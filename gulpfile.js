var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
var paths = {
	'sweetalert': 'node_modules/sweetalert/dist/',
};

elixir(function(mix) {
    // mix.sass('app.scss');
    mix.copy(paths.sweetalert+'sweetalert-dev.js', 'public/plugins/sweetalert/sweetalert-dev.js')
	   .copy(paths.sweetalert+'sweetalert.min.js', 'public/plugins/sweetalert/sweetalert.min.js')
	   .copy(paths.sweetalert+'sweetalert.css', 'public/plugins/sweetalert/sweetalert.css')
	   .browserify('iapms.js');
});
