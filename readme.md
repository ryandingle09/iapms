Routing Naming Convention

"resources.index" => Return view for listing of resources / return (json B) of list of resources
"resources.create" => Return a form to create resources  
"resources.store" => Handle the storing process of new resources (json A)
"resources.show" => Return view of to diplay single resources / Return sigle resources in (json B) format
"resources.edit" => Form to edit resources
"resources.update" => Handle updating of old resources and return (json A)
"resources.destroy" => Handle delete process of resources and return (json A)
"resources.save" => Handle store process if resources not existing otherwise handle update and return (json A)
"resources.list" => Return list of reources in datatable format like (json D)

JSON Return Format

json A
    return response()->json([ 
    	'success' => true , 
    	'message' => 'Resource has been created.' ,
    	'data' => $data // Optional
    ]);

json B
	return response()->json([ 
    	'success' => true , 
    	'data' => $data // object or array of object
    ]);

json C
	return response()->json([ 
    	'success' => true , 
    	'message' => 'DAR has been sent.' 
    ]);

json D
	return Datatables::of($scopes)->make(true);



Cheat Sheet
php artisan make:controller SampleController    // Singular Name and 'Controller' suffix
php artisan make:request SampleRequest    // Singular Name and 'Request' suffix
php artisan make:repository Sample    // Singular Name
php artisan make:bindings Sample     // To bind repository
php artisan make:migration create_sample_table --create=sample  // creating migrate of 'sample' table

Notes :
- Don't create model without repository
- No need to create model for a pivot table

Date Format : Dec-1-2017
For JS : "M-dd-yyyy",
For PHP : "M-d-Y", configuration can be found on config/app.php look for app.date_display

Random Token mismatch
- "https://github.com/laravel/framework/issues/8172"
