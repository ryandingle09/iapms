<?php

/**
 * IAPMS Configuration file
 */
return [
    /*
    |--------------------------------------------------------------------------
    | Application Name
    |--------------------------------------------------------------------------
    |
    | Application name here
    |
    */
    'app_name' => "Internal Audit Project Management System",
    /**
     * Lookup codes config
     */
    'lookups' => [
        'user_tag' => "User Tag",
        'master_company' =>  'Master Company',
        'company' => 'Company Code',
        'gender' => 'Gender',
        'holiday' => 'Holiday Types',
        'leave' => 'Leave Types',
        'proficiency_rate' => 'Proficiency Rate',
        'group_type' => 'Group Type',
        'entity_code' => 'Entity Code',
        'dept_code' => 'Department Code',
        'business_type' => 'Business Type',
        'business_process' => 'Business Process Code',
        'additional_info_attr' => 'Additional Info Attributes',
        'audit_type' => 'Audit Type',
        'source_type' => 'Source Type',
        'primary_keys' => 'Table primary key',
        'head_office' => 'Head office',
        'main_bp' => 'Main Business Process',
        'person_project_role' => "Person Project Role",
        'additional_information' => [
            'tables' => 'Additional Info Tables',
        ],
        'auditee' => [
            'group' => 'Auditee Group Code',
            'position' => 'Auditee Position',
            'distribution_type' => 'Auditee Distribution Type',
        ],
        'auditor' => [
            'position' => 'Auditor Position',
            'proficiency' => 'Auditor Proficiency Code',
            'lcm' => 'Auditor LCM Type',
            'training' => 'Auditor Training Type',
            'type' => 'Auditor Type',
        ],
        'branch' => [
            'code' => 'Branch Code',
            'store' => [
                'size' => 'Store Size',
                'class' => 'Store Class',
            ],
            'zone' => 'Branch Zone',
            'class' => 'Branch Class',
            'entity_class' => 'Entity Class',
        ],
        'control' => [
            'controls' => 'Controls',
            'component' => 'Control Component',
            'principle' => 'Control Principle',
            'focus' => 'Control Focus',
            'assertions' => 'Control Assertions',
            'types' => 'Control Types',
            'application' => 'Control Application',
            'frequency' => 'Control Frequency',
        ],
        'risk' => [
            'risks' => 'Risks',
            'type' => 'Risk Type',
            'source' => 'Risk Source Type',
            'response' => 'Risk Response Type',
            'response_status' => 'Risk Response Status',
            'ent_code' => 'Enterprise Risk Code',
            'ent_type' => 'Enterprise Risk Type',
            'ent_class' => 'Enterprise Risk Class',
            'ent_area' => 'Enterprise Risk Area',
        ],
        'objective' => [
            'type' => 'Objective Type',
            'category' => 'Objective Category',
        ],
        'questionnaire' => [
            'type' => 'Questionnaire Type',
            'response' => 'Questionnaire Response Type',
            'choices' => 'Questionnaire Choices Name',
            'datatype' => 'Questionnaire Data Type',
        ],
        'plan' => [
            'group_by' => 'Plan Group By'
        ]
    ],
    'custom_measure' => [
        'count' => 10
    ],
    'inquiry' => [
        'sequence' => [
            'entity' => ['entity', 'mainBp', 'subBp', 'objective', 'objectiveStep', 'risk', 'control'],
            'branch' => ['branch', 'mainBp', 'subBp', 'objective', 'objectiveStep', 'risk', 'control'],
            'dept' => ['mainBp', 'subBp', 'objective', 'objectiveStep', 'risk', 'control'],
            'mainbp' => ['entity', 'subBp', 'objective', 'objectiveStep', 'risk', 'control'],
            'subbp' => ['entity', 'mainBp', 'objective', 'objectiveStep', 'risk', 'control'],
            'objective' => ['entity', 'mainBp', 'subBp', 'objectiveStep', 'risk', 'control'],
            'risk' => ['entity', 'mainBp', 'subBp', 'objective', 'objectiveStep', 'control'],
            'control' => ['entity', 'mainBp', 'subBp', 'objective', 'objectiveStep', 'risk'],
        ]
    ]
];
