SET YEAR(CURDATE()) = 2017;
SELECT 
  ras.auditable_entity_id ae_id,
  ras.ae_mbp_id mbp,
  ras.inherent_impact_value,
  ras.inherent_likelihood_value,
  (
    ras.inherent_impact_value * ras.inherent_likelihood_value
  ) risk_score,
  md_ri.matrix_value risk_index,
  md_ri.color risk_index_color,
  md_sw.matrix_value severity_weight,
  (
    (
      ras.inherent_impact_value * ras.inherent_likelihood_value
    ) * md_sw.matrix_value
  ) severity_value,
  IF (
    YEAR(CURDATE()) - CAST(
      YEAR(
        STR_TO_DATE(psm.last_audit_date, "%Y-%m-%d")
      ) AS UNSIGNED
    ) > 3,
    3,
    YEAR(CURDATE()) - CAST(
      YEAR(
        STR_TO_DATE(psm.last_audit_date, "%Y-%m-%d")
      ) AS UNSIGNED
    )
  ) tla,
  (md_pai.matrix_value * 100) arp,
  (
    (
      (
        ras.inherent_impact_value * ras.inherent_likelihood_value
      ) * md_sw.matrix_value
    ) * (md_pai.matrix_value)
  ) AS adjusted_severity_value 
FROM
  ia_risk_assessment_scale AS ras 
  LEFT JOIN ia_matrix m_ri 
    ON m_ri.matrix_name = "Risk Index" 
  LEFT JOIN ia_matrix_details AS md_ri 
    ON md_ri.x_sequence = ras.inherent_impact_value 
    AND md_ri.y_sequence = ras.inherent_likelihood_value 
    AND md_ri.matrix_id = m_ri.matrix_id 
  LEFT JOIN ia_matrix AS m_sw 
    ON m_sw.matrix_name = "Severity Weight" 
  LEFT JOIN ia_matrix_details AS md_sw 
    ON md_sw.color = md_ri.color 
    AND m_sw.matrix_id = md_sw.matrix_id 
  LEFT JOIN 
    (SELECT 
      grade,
      planned_start_date AS last_audit_date,
      auditable_entity_id,
      ae_mbp_id 
    FROM
      ia_project_scope_summary t1 
    WHERE planned_start_date = 
      (SELECT 
        MAX(t2.planned_start_date) 
      FROM
        ia_project_scope_summary t2 
      WHERE t2.auditable_entity_id = t1.auditable_entity_id 
        AND t2.ae_mbp_id = t1.ae_mbp_id) 
    GROUP BY auditable_entity_id,
      ae_mbp_id) psm 
    ON psm.auditable_entity_id = ras.auditable_entity_id 
    AND psm.ae_mbp_id = ras.ae_mbp_id 
  LEFT JOIN ia_matrix AS m_pai 
    ON m_pai.matrix_name = "Past Audit Index" 
  LEFT JOIN ia_matrix_details AS md_pai 
    ON md_pai.matrix_id = m_pai.matrix_id 
    AND md_pai.x_value = psm.grade 
    AND md_pai.y_sequence = (
      IF (
        YEAR(CURDATE()) - CAST(
          YEAR(
            STR_TO_DATE(psm.last_audit_date, "%Y-%m-%d")
          ) AS UNSIGNED
        ) > 3,
        3,
        YEAR(CURDATE()) - CAST(
          YEAR(
            STR_TO_DATE(psm.last_audit_date, "%Y-%m-%d")
          ) AS UNSIGNED
        )
      )
    )









    -- with curdate YEAR(CURDATE()) 


    SELECT 
  ras.auditable_entity_id ae_id,
  ras.ae_mbp_id mbp,
  ras.inherent_impact_value,
  ras.inherent_likelihood_value,
  (
    ras.inherent_impact_value * ras.inherent_likelihood_value
  ) risk_score,
  md_ri.matrix_value risk_index,
  md_ri.color risk_index_color,
  md_sw.matrix_value severity_weight,
  (
    (
      ras.inherent_impact_value * ras.inherent_likelihood_value
    ) * md_sw.matrix_value
  ) severity_value,
  IF (
    YEAR(CURDATE()) - CAST(
      YEAR(
        STR_TO_DATE(psm.last_audit_date, "%Y-%m-%d")
      ) AS UNSIGNED
    ) > 3,
    3,
    YEAR(CURDATE()) - CAST(
      YEAR(
        STR_TO_DATE(psm.last_audit_date, "%Y-%m-%d")
      ) AS UNSIGNED
    )
  ) tla,
  (md_pai.matrix_value * 100) arp,
  (
    (
      (
        ras.inherent_impact_value * ras.inherent_likelihood_value
      ) * md_sw.matrix_value
    ) * (md_pai.matrix_value)
  ) AS adjusted_severity_value 
FROM
  ia_risk_assessment_scale AS ras 
  LEFT JOIN ia_matrix m_ri 
    ON m_ri.matrix_name = "Risk Index" 
  LEFT JOIN ia_matrix_details AS md_ri 
    ON md_ri.x_sequence = ras.inherent_impact_value 
    AND md_ri.y_sequence = ras.inherent_likelihood_value 
    AND md_ri.matrix_id = m_ri.matrix_id 
  LEFT JOIN ia_matrix AS m_sw 
    ON m_sw.matrix_name = "Severity Weight" 
  LEFT JOIN ia_matrix_details AS md_sw 
    ON md_sw.color = md_ri.color 
    AND m_sw.matrix_id = md_sw.matrix_id 
  LEFT JOIN 
    (SELECT 
      grade,
      planned_start_date AS last_audit_date,
      auditable_entity_id,
      ae_mbp_id 
    FROM
      ia_project_scope_summary t1 
    WHERE planned_start_date = 
      (SELECT 
        MAX(t2.planned_start_date) 
      FROM
        ia_project_scope_summary t2 
      WHERE t2.auditable_entity_id = t1.auditable_entity_id 
        AND t2.ae_mbp_id = t1.ae_mbp_id) 
    GROUP BY auditable_entity_id,
      ae_mbp_id) psm 
    ON psm.auditable_entity_id = ras.auditable_entity_id 
    AND psm.ae_mbp_id = ras.ae_mbp_id 
  LEFT JOIN ia_matrix AS m_pai 
    ON m_pai.matrix_name = "Past Audit Index" 
  LEFT JOIN ia_matrix_details AS md_pai 
    ON md_pai.matrix_id = m_pai.matrix_id 
    AND md_pai.x_value = psm.grade 
    AND md_pai.y_sequence = (
      IF (
        YEAR(CURDATE()) - CAST(
          YEAR(
            STR_TO_DATE(psm.last_audit_date, "%Y-%m-%d")
          ) AS UNSIGNED
        ) > 3,
        3,
        YEAR(CURDATE()) - CAST(
          YEAR(
            STR_TO_DATE(psm.last_audit_date, "%Y-%m-%d")
          ) AS UNSIGNED
        )
      )
    )