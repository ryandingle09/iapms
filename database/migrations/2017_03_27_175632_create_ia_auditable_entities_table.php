<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaAuditableEntitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('auditable_entities', function(Blueprint $table)
		{
			$table->increments('auditable_entity_id');
			$table->string('auditable_entity_name', 240)->index('auditable_entities_auditable_entity_name_index');
			$table->string('company_code', 100)->index('auditable_entities_company_code_index');
			$table->string('group_type', 100)->index('auditable_entities_group_type_index');
			$table->string('branch_code', 100)->index('auditable_entities_branch_code_index')->nullable();
			$table->string('entity_class', 100)->index('auditable_entities_entity_class_index');
			$table->string('department_code', 100)->index('auditable_entities_department_code_index')->nullable();
			$table->string('business_type', 100)->index('auditable_entities_business_type_index');
			$table->string('parent_entity', 1);
			$table->integer('parent_ae_id')->unsigned()->nullable();
			$table->string('contact_name', 240)->index('contact_name');
			$table->string('entity_head_name', 240)->index('entity_head_name');
			$table->string('template', 1)->default('N');
			$table->string('attribute1', 240)->nullable();
			$table->string('attribute2', 240)->nullable();
			$table->string('attribute3', 240)->nullable();
			$table->string('attribute4', 240)->nullable();
			$table->string('attribute5', 240)->nullable();
			$table->string('attribute6', 240)->nullable();
			$table->string('attribute7', 240)->nullable();
			$table->string('attribute8', 240)->nullable();
			$table->string('attribute9', 240)->nullable();
			$table->string('attribute10', 240)->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('auditable_entities');
	}

}
