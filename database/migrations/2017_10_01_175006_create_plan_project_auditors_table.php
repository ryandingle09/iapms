<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanProjectAuditorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plan_project_auditors', function(Blueprint $table)
		{
			$table->increments('plan_project_auditor_id');
			$table->integer('plan_project_id')->unsigned();
			$table->integer('auditor_id')->unsigned();
			// $table->string('auditor_type', 100);
			// $table->integer('allotted_mandays');
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');

			$table->foreign('plan_project_id','plan_project_auditors_plan_project_id_foreign')
				->references('plan_project_id')
				->on('plan_projects')
				// ->onUpdate('RESTRICT')
				->onDelete('CASCADE');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plan_project_auditors');
	}

}
