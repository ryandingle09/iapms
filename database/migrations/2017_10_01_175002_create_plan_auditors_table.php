<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanAuditorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plan_auditors', function(Blueprint $table)
		{
			$table->increments('plan_auditor_id');
			$table->integer('plan_id')->unsigned();
			$table->integer('auditor_id')->unsigned();
			// $table->string('auditor_type', 100);
			$table->integer('annual_mandays');
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');

			$table->foreign('plan_id')
				->references('plan_id')
				->on('plan')
				->onUpdate('RESTRICT')
				->onDelete('CASCADE');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plan_auditors');
	}

}
