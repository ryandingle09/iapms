<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalSetupMastersTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('approval_setup_masters', function(Blueprint $table) {
            $table->increments('approval_setup_master_id');
            $table->string('source_category', 50 )->nullable();
            $table->string('source_subcategory', 50 )->nullable();
            $table->string('description', 200 )->nullable();
            $table->string('source_table', 50 )->nullable();
            $table->string('start_status', 50 )->nullable();
            $table->string('finish_status', 50 )->nullable();
            $table->integer('supersede_allowed_flag')->default(0);
            $table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('approval_setup_masters');
	}

}
