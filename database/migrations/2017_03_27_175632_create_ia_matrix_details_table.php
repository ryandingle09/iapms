<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaMatrixDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('matrix_details', function(Blueprint $table)
		{
			$table->integer('matrix_id')->unsigned()->index('matrix_details_matrix_id_foreign');
			$table->string('x_value', 20);
			$table->integer('x_sequence')->unsigned();
			$table->string('y_value', 20);
			$table->integer('y_sequence')->unsigned();
			$table->integer('matrix_value')->unsigned();
			$table->string('color', 20);
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('matrix_details');
	}

}
