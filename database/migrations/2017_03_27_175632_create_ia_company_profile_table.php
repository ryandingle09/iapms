<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaCompanyProfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('company_profile', function(Blueprint $table)
		{
			$table->increments('company_id');
			$table->string('company_name', 100)->index('company_profile_company_name_index');
			$table->string('company_code', 100)->index('company_profile_company_code_index');
			$table->string('description', 240)->nullable();
			$table->text('business_desc')->nullable();
			$table->text('industry_desc')->nullable();
			$table->text('com_env_desc')->nullable();
			$table->text('eco_stab_desc')->nullable();
			$table->text('tech_dev_desc')->nullable();
			$table->text('growth_pot_desc')->nullable();
			$table->text('line_of_bus_desc')->nullable();
			$table->text('products_desc')->nullable();
			$table->text('vol_of_op_desc')->nullable();
			$table->text('cust_and_market_desc')->nullable();
			$table->text('suppliers_desc')->nullable();
			$table->text('competitors_desc')->nullable();
			$table->text('fin_rep_and_acct_desc')->nullable();
			$table->text('taxation_desc')->nullable();
			$table->text('gov_pol_desc')->nullable();
			$table->text('legal_req_desc')->nullable();
			$table->text('key_people_desc')->nullable();
			$table->text('board_of_dir_desc')->nullable();
			$table->text('consultants_desc')->nullable();
			$table->text('third_party_rel_desc')->nullable();
			$table->text('stockholders_desc')->nullable();
			$table->text('company_fin_desc')->nullable();
			$table->text('investment_desc')->nullable();
			$table->text('obj_and_strat_desc')->nullable();
			$table->text('fin_aspect_desc')->nullable();
			$table->text('op_aspect_desc')->nullable();
			$table->text('it_profile_desc')->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('company_profile');
	}

}
