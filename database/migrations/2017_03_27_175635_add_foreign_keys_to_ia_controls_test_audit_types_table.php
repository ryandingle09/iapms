<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaControlsTestAuditTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('controls_test_audit_types', function(Blueprint $table)
		{
			$table->foreign('control_test_id', 'controls_test_audit_types_control_test_id_foreign')->references('control_test_id')->on('controls_test_proc')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('controls_test_audit_types', function(Blueprint $table)
		{
			$table->dropForeign('controls_test_audit_types_control_test_id_foreign');
		});
	}

}
