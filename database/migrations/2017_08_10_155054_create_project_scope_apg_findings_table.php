<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectScopeApgFindingsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_scope_apg_findings', function(Blueprint $table) {
            $table->increments('project_scope_apg_finding_id');
            $table->integer('project_scope_apg_id')->unsigned();
            $table->string('non_issue',1);
            $table->string('highlight',1);
            $table->text('issues');
            $table->text('conclusions');
            $table->text('recommendations');
            $table->text('auditee_actions_needed');
            $table->date('auditee_action_due_date');
            $table->text('auditee_actions_taken');
            $table->text('auditor_remarks');
            $table->integer('auditor_id');
            $table->integer('auditee_id');
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('last_update_by');
            $table->date('last_update_date');

            $table->foreign('project_scope_apg_id')->references('project_scope_apg_id')->on('project_scope_apg')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_scope_apg_findings');
	}

}
