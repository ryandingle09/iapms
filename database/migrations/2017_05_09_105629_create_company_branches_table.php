<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_branches', function (Blueprint $table) {
            $table->unsignedInteger('company_id');
            $table->string('branch_code', 50)->indexed();
            $table->string('active', 1)->default('Y');
            $table->string('branch_store_size', 100)->nullable();
            $table->string('branch_address', 240)->nullable();
            $table->string('branch_zone', 100)->nullable();
            $table->string('branch_store_class', 100)->nullable();
            $table->string('branch_class', 100)->nullable();
            $table->string('entity_class', 100)->indexed();
            $table->date('branch_opening_date')->nullable();
            $table->unsignedInteger('created_by');
            $table->date('created_date');
            $table->unsignedInteger('last_update_by');
            $table->date('last_update_date');

            $table->foreign('company_id')
                  ->references('company_id')
                  ->on('company_profile')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_branches');
    }
}
