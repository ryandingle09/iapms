<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaUserRespAssignmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_resp_assignments', function(Blueprint $table)
		{
			$table->foreign('responsibility_id', 'responsibility_FK')->references('responsibility_id')->on('responsibilities')->onUpdate('RESTRICT')->onDelete('SET NULL');
			$table->foreign('user_id', 'users_FK')->references('user_id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_resp_assignments', function(Blueprint $table)
		{
			$table->dropForeign('responsibility_FK');
			$table->dropForeign('users_FK');
		});
	}

}
