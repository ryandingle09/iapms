<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterControlsMasterTableChangeComponentPrincipleFocusSize extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('controls_master', function (Blueprint $table) {
            $table->string('component_code', 250)->change();
            $table->string('principle_code', 250)->change();
            $table->string('focus_code', 250)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('controls_master', function (Blueprint $table) {
            $table->string('component_code', 100)->change();
            $table->string('principle_code', 100)->change();
            $table->string('focus_code', 100)->change();
        });
    }
}
