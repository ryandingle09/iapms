<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEnterpriseRisksTableAddAeMbpId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enterprise_risks', function (Blueprint $table) {
            $table->foreign('auditable_entity_id')
                  ->references('auditable_entity_id')
                  ->on('auditable_entities_actual')
                  ->onDelete('CASCADE');
            $table->foreign('bp_id')
                  ->references('bp_id')
                  ->on('business_processes')
                  ->onDelete('CASCADE');
            // new field
            $table->unsignedInteger('ae_mbp_id')->after('auditable_entity_id');
            $table->foreign('ae_mbp_id')
                  ->references('ae_mbp_id')
                  ->on('ae_mbp')
                  ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enterprise_risks', function (Blueprint $table) {
            $table->dropForeign('enterprise_risks_ae_mbp_id_foreign');
            $table->dropColumn('ae_mbp_id');
        });
    }
}
