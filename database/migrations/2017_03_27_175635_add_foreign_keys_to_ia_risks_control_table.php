<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaRisksControlTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('risks_control', function(Blueprint $table)
		{
			$table->foreign('risk_id', 'risks_control_risk_idFK')->references('risk_id')->on('risks')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('risks_control', function(Blueprint $table)
		{
			$table->dropForeign('risks_control_risk_idFK');
		});
	}

}
