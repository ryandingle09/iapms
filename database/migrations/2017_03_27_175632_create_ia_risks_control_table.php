<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaRisksControlTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('risks_control', function(Blueprint $table)
		{
			$table->integer('risk_id')->unsigned()->index('risks_control_risk_idFK');
			$table->increments('risk_control_id');
			$table->integer('risk_control_seq')->unsigned();
			$table->integer('control_id')->unsigned();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('risks_control');
	}

}
