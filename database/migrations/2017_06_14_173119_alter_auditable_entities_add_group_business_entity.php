<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAuditableEntitiesAddGroupBusinessEntity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auditable_entities', function (Blueprint $table) {
            $table->string('entity_class', 100)->index()->after('department_code');
            $table->string('business_type', 100)->index()->after('department_code');
            $table->string('group_type', 100)->index()->after('department_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auditable_entities', function (Blueprint $table) {
            $table->dropColumn('group_type');
            $table->dropColumn('business_type');
            $table->dropColumn('entity_class');
        });
    }
}
