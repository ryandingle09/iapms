<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaAuditorProficiencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('auditor_proficiency', function(Blueprint $table)
		{
			$table->foreign('auditor_id', 'auditor_proficiency_auditor_id_foreign')->references('auditor_id')->on('auditors')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('auditor_proficiency', function(Blueprint $table)
		{
			$table->dropForeign('auditor_proficiency_auditor_id_foreign');
		});
	}

}
