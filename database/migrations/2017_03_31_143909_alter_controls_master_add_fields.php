<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterControlsMasterAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('controls_master', function (Blueprint $table) {
            $table->longText('assertions')->after('focus_code');
            $table->longText('control_types')->after('assertions');
            $table->string('application', 100)->indexed()->after('control_types');
            $table->longText('control_source')->after('application');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('controls_master', function (Blueprint $table) {
            $table->dropColumn('assertions');
            $table->dropColumn('control_types');
            $table->dropColumn('application');
            $table->dropColumn('control_source');
        });
    }
}
