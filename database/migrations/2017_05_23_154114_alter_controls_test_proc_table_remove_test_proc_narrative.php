<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterControlsTestProcTableRemoveTestProcNarrative extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('controls_test_proc', function (Blueprint $table) {
            $table->dropColumn('test_proc_narrative');
            $table->unsignedInteger('test_proc_id')->after('control_test_seq');

            $table->foreign('test_proc_id')
                  ->references('test_proc_id')
                  ->on('test_proc')
                  ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('controls_test_proc', function (Blueprint $table) {
            $table->dropForeign('controls_test_proc_test_proc_id_foreign');
            $table->dropColumn('test_proc_id');
            $table->longText('test_proc_narrative')->after('control_test_seq');
        });
    }
}
