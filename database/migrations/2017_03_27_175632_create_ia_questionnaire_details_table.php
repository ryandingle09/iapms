<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaQuestionnaireDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('questionnaire_details', function(Blueprint $table)
		{
			$table->integer('questionnaire_id')->unsigned();
			$table->integer('question_sequence')->unsigned();
			$table->string('question', 240);
			$table->string('response_type', 100);
			$table->string('response_required', 1)->default('Y');
			$table->string('choices_name', 20)->nullable();
			$table->string('range_from', 20)->nullable();
			$table->string('range_to', 20)->nullable();
			$table->string('data_type', 20);
			$table->string('page_break_after', 1)->default('Y');
			$table->string('optional_remarks', 1)->default('Y');
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('questionnaire_details');
	}

}
