<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaBpObjectivesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bp_objectives', function(Blueprint $table)
		{
			$table->foreign('bp_id', 'bp_objectives_bp_id_foreign')->references('bp_id')->on('business_processes')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bp_objectives', function(Blueprint $table)
		{
			$table->dropForeign('bp_objectives_bp_id_foreign');
		});
	}

}
