<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaAdditionalInfoDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('additional_info_details', function(Blueprint $table)
		{
			$table->foreign('additional_info_id', 'additional_info_details_additional_info_id_foreign')->references('additional_info_id')->on('additional_info_master')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('additional_info_details', function(Blueprint $table)
		{
			$table->dropForeign('additional_info_details_additional_info_id_foreign');
		});
	}

}
