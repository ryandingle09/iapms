<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaMenuEntriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menu_entries', function(Blueprint $table)
		{
			$table->integer('menu_id')->unsigned()->index('menu_id_fk_idx');
			$table->integer('permission_id')->unsigned()->index('permission_idFK_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menu_entries');
	}

}
