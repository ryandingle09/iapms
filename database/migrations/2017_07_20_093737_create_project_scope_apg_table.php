<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectScopeApgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_scope_apg', function (Blueprint $table) {
            $table->increments('project_scope_apg_id');
            $table->integer('project_scope_id')->unsigned();
            $table->integer('apg_seq');
            $table->string('main_bp_name',100);
            $table->integer('mbp_bp_seq');
            $table->string('bp_name',240);
            $table->string('objective_name',100);
            $table->integer('bp_steps_seq');
            $table->text('activity_narrative');
            $table->integer('bp_step_risk_seq');
            $table->string('risk_code',100);
            $table->integer('control_seq')->nullable();
            $table->string('control_name',100);
            $table->integer('control_test_seq')->nullable();
            $table->integer('test_proc_id');
            $table->text('test_proc_narrative')->nullable();
            $table->text('audit_objective')->nullable();
            $table->integer('allotted_mandays');
            $table->integer('auditor_id');
            $table->string('status',20);
            $table->string('enabled_flag',1);
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('last_update_by');
            $table->date('last_update_date');

            $table->foreign('project_scope_id')->references('project_scope_id')->on('project_scope')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_scope_apg');
    }
}
