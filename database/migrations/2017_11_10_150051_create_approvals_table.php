<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('approvals', function(Blueprint $table) {
            $table->increments('approval_id');
            $table->string('source_category',50);
            $table->string('source_subcategory',50);
            $table->integer('approver_id');
            $table->string('status_from',50);
            $table->string('status_to',50);
            $table->integer('approval_ref_id');
            $table->string('auditor_type',20);
            $table->date('approval_date');
            $table->text('remarks')->nullable();
            $table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('approvals');
	}

}
