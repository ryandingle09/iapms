<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAuditorProficiencyTableRemoveStartEndDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auditor_proficiency', function (Blueprint $table) {
            $table->dropColumn('effective_start_date');
            $table->dropColumn('effective_end_date');
            $table->dropColumn('proficiency_code');

            $table->increments('auditor_proficiency_id');
            $table->string('proficiency_type', 20)->indexed()->after('auditor_id');
            $table->longText('remarks')->nullable()->after('proficiency_rate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auditor_proficiency', function (Blueprint $table) {
            $table->dropColumn('auditor_proficiency_id');
            $table->dropColumn('proficiency_type');
            $table->dropColumn('remarks');

            $table->string('proficiency_code', 100)->indexed()->after('auditor_id');
            $table->date('effective_end_date')->after('proficiency_rate');
            $table->date('effective_start_date')->after('proficiency_rate');
        });
    }
}
