<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaAuditorProficiencyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('auditor_proficiency', function(Blueprint $table)
		{
			$table->integer('auditor_id')->unsigned()->index('auditor_proficiency_auditor_id_foreign');
			$table->string('proficiency_code', 100)->index('auditor_proficiency_proficiency_code_index');
			$table->integer('proficiency_rate');
			$table->date('effective_start_date');
			$table->date('effective_end_date');
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('auditor_proficiency');
	}

}
