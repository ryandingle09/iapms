<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaAuditorGradingMatrixTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('auditor_grading_matrix', function(Blueprint $table)
		{
			$table->integer('variance_from')->unsigned();
			$table->integer('variance_to')->unsigned();
			$table->integer('rating')->unsigned();
			$table->integer('quality_max')->unsigned();
			$table->integer('documentation_max')->unsigned();
			$table->string('remarks', 240)->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('auditor_grading_matrix');
	}

}
