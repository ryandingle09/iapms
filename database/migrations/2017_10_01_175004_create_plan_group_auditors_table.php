<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanGroupAuditorsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plan_group_auditors', function(Blueprint $table) {
            $table->increments('plan_group_auditor_id');
            $table->integer('plan_group_id')->unsigned();
            $table->integer('auditor_id')->unsigned();
            $table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');

			$table->foreign('plan_group_id')
				->references('plan_group_id')
				->on('plan_groups')
				->onUpdate('RESTRICT')
				->onDelete('CASCADE');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plan_group_auditors');
	}

}
