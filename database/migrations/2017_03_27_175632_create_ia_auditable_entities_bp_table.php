<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaAuditableEntitiesBpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('auditable_entities_bp', function(Blueprint $table)
		{
			$table->integer('bp_id')->unsigned();
			$table->integer('auditable_entity_id')->unsigned()->index('auditable_entities_bp_auditable_entity_id_foreign');
			$table->integer('cm_1_value')->nullable();
			$table->integer('cm_2_value')->nullable();
			$table->integer('cm_3_value')->nullable();
			$table->integer('cm_4_value')->nullable();
			$table->integer('cm_5_value')->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('auditable_entities_bp');
	}

}
