<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaLookupValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lookup_values', function(Blueprint $table)
		{
			$table->integer('lookup_type_id')->unsigned();
			$table->string('lookup_code', 100)->index('lookup_code');
			$table->string('meaning', 100)->nullable();
			$table->text('description')->nullable();
			$table->date('effective_start_date');
			$table->date('effective_end_date')->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
			$table->unique(['lookup_type_id','lookup_code'], 'lookup_type_id_lookup_code');
			$table->index(['effective_start_date','effective_end_date'], 'effective_start_date_effective_end_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lookup_values');
	}

}
