<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIaWbsHeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wbs_header', function(Blueprint $table) {
            $table->increments('wbs_header_id');
            $table->integer('audit_year');
            $table->integer('auditor_id');
            $table->string('template_name', 255);
            $table->integer('created_by')->unsigned();
            $table->date('created_date');
            $table->integer('last_update_by')->unsigned();
            $table->date('last_update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wbs_header');
    }
}
