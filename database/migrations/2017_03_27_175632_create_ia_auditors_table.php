<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaAuditorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('auditors', function(Blueprint $table)
		{
			$table->increments('auditor_id');
			$table->string('full_name', 240)->index('auditors_full_name_index');
			$table->string('gender', 1);
			$table->string('position_code', 100)->index('auditors_position_code_index');
			$table->integer('supervisor_id')->unsigned()->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
			$table->date('effective_start_date');
			$table->date('effective_end_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('auditors');
	}

}
