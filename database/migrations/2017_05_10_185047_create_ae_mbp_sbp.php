<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAeMbpSbp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ae_mbp_sbp', function (Blueprint $table) {
            $table->increments('ae_mbp_bp_id');
            $table->unsignedInteger('ae_mbp_id');
            $table->integer('mbp_bp_seq');
            $table->unsignedInteger('bp_id');
            $table->unsignedInteger('created_by');
            $table->date('created_date');
            $table->unsignedInteger('last_update_by');
            $table->date('last_update_date');

            $table->foreign('ae_mbp_id')
                  ->references('ae_mbp_id')
                  ->on('ae_mbp')
                  ->onDelete('cascade');

            $table->foreign('bp_id')
                  ->references('bp_id')
                  ->on('business_processes')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ae_mbp_sbp');
    }
}
