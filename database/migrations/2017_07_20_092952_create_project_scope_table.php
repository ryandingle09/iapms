<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectScopeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_scope', function (Blueprint $table) {
            $table->increments('project_scope_id');
            $table->integer('project_id')->unsigned();
            $table->integer('scope_sequence');
            $table->integer('auditable_entity_id');
            $table->integer('ae_mbp_id');
            $table->string('audit_location',80)->nullable();;
            $table->date('target_start_date');
            $table->date('target_end_date');
            $table->date('actual_start_date')->nullable();
            $table->date('actual_end_date')->nullable();
            $table->string('project_scope_status',20);
            $table->string('freeze_status',20)->nullable();
            $table->integer('plan_project_scope_id')->nullable();
            $table->integer('default_misc_mandays')->nullable();
            $table->integer('default_apg_mandays')->nullable();
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('last_update_by');
            $table->date('last_update_date');
            $table->foreign('project_id')->references('project_id')->on('projects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_scope');
    }
}
