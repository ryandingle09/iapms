<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaControlsMasterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('controls_master', function(Blueprint $table)
		{
			$table->increments('control_id');
			$table->string('control_name', 80)->index('controls_master_control_name_index');
			$table->string('control_code', 80)->index('controls_master_control_code_index');
			$table->string('component_code', 100);
			$table->string('principle_code', 100);
			$table->string('focus_code', 100);
			$table->string('attribute1', 20)->nullable();
			$table->string('attribute2', 20)->nullable();
			$table->string('attribute3', 20)->nullable();
			$table->string('attribute4', 20)->nullable();
			$table->string('attribute5', 20)->nullable();
			$table->string('attribute6', 20)->nullable();
			$table->string('attribute7', 20)->nullable();
			$table->string('attribute8', 20)->nullable();
			$table->string('attribute9', 20)->nullable();
			$table->string('attribute10', 20)->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
			$table->index(['component_code','principle_code','focus_code'], 'controls_masters_component_code_principle_code_focus_code_index');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('controls_master');
	}

}
