<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaEnterpriseRiskDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('enterprise_risk_details', function(Blueprint $table)
		{
			$table->foreign('enterprise_risk_id', 'enterprise_risk_details_enterprise_risk_id_foreign')->references('enterprise_risk_id')->on('enterprise_risks')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('enterprise_risk_details', function(Blueprint $table)
		{
			$table->dropForeign('enterprise_risk_details_enterprise_risk_id_foreign');
		});
	}

}
