<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRisksTableAddEnterpriseRiskId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('risks', function (Blueprint $table) {
            $table->dropColumn('enterprise_risk_code');
            $table->dropColumn('enterprise_risk_type');
            $table->dropColumn('enterprise_risk_class');
            $table->dropColumn('enterprise_risk_area');

            $table->unsignedInteger('enterprise_risk_id')->after('rating_comment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('risks', function (Blueprint $table) {
            $table->string('enterprise_risk_code', 100);
            $table->string('enterprise_risk_type', 100);
            $table->string('enterprise_risk_class', 100);
            $table->string('enterprise_risk_area', 100);

            $table->dropColumn('enterprise_risk_id');
        });
    }
}
