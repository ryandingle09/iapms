<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaAuditableEntitiesBpTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('auditable_entities_bp', function(Blueprint $table)
		{
			$table->foreign('auditable_entity_id', 'auditable_entities_bp_auditable_entity_id_foreign')->references('auditable_entity_id')->on('auditable_entities')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('auditable_entities_bp', function(Blueprint $table)
		{
			$table->dropForeign('auditable_entities_bp_auditable_entity_id_foreign');
		});
	}

}
