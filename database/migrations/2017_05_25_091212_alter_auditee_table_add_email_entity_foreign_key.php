<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAuditeeTableAddEmailEntityForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auditees', function (Blueprint $table) {
            $table->string('email_address', 100)->indexed()->after('supervisor_id');
            $table->date('effective_end_date')->nullable()->change();
            $table->foreign('auditable_entity_id')
                  ->references('auditable_entity_id')
                  ->on('auditable_entities_actual');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auditees', function (Blueprint $table) {
            $table->dropColumn('email_address');
            $table->date('effective_end_date')->change();
            $table->dropForeign('auditees_auditable_entity_id_foreign');
        });
    }
}
