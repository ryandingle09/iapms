<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaAdditionalInfoDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('additional_info_details', function(Blueprint $table)
		{
			$table->integer('additional_info_id')->unsigned()->index('additional_info_details_additional_info_id_foreign');
			$table->integer('additional_info_seq');
			$table->string('prompt', 80);
			$table->string('data_type', 100);
			$table->string('attribute_assignment', 20);
			$table->integer('value_set_id')->unsigned()->nullable();
			$table->string('required', 1)->default('Y');
			$table->string('remarks', 240)->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('additional_info_details');
	}

}
