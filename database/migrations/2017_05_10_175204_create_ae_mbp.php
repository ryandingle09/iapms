<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAeMbp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ae_mbp', function (Blueprint $table) {
            $table->increments('ae_mbp_id');
            $table->unsignedInteger('auditable_entity_id');
            $table->string('main_bp_name', 20)->index();
            $cm_count = range(1,10);
            for ($i=0; $i < count($cm_count); $i++) {
                $table->string('cm_'.$cm_count[$i].'_name', 40)->nullable();
                $table->string('cm_'.$cm_count[$i].'_high_score_type', 20)->nullable();
            }
            $table->decimal('walk_thru_mandays', 3, 1)->nullable();
            $table->decimal('planning_mandays', 3, 1)->nullable();
            $table->decimal('followup_mandays', 3, 1)->nullable();
            $table->decimal('discussion_mandays', 3, 1)->nullable();
            $table->decimal('draft_dar_mandays', 3, 1)->nullable();
            $table->decimal('final_report_mandays', 3, 1)->nullable();
            $table->decimal('wrap_up_mandays', 3, 1)->nullable();
            $table->decimal('reviewer_mandays', 3, 1)->nullable();
            $table->unsignedInteger('created_by');
            $table->date('created_date');
            $table->unsignedInteger('last_update_by');
            $table->date('last_update_date');

            $table->foreign('auditable_entity_id')
                  ->references('auditable_entity_id')
                  ->on('auditable_entities')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ae_mbp');
    }
}
