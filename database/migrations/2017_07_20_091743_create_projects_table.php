<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('project_id');
            $table->string('project_name',80);
            $table->string('audit_type',100);
            $table->string('project_status',20);
            $table->date('target_start_date');
            $table->date('target_end_date');
            $table->string('project_remarks',1000)->nullable();
            $table->integer('project_head')->nullable();
            $table->integer('plan_project_id');
            $table->integer('audit_year');
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('last_update_by');
            $table->date('last_update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
    }
}
