<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAuditorsTableAddEmailAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auditors', function (Blueprint $table) {
            $table->string('nickname', 20)->nullable()->after('full_name');
            $table->string('employee_number', 20)->indexed()->after('nickname');
            $table->string('email_address', 100)->indexed()->after('position_code');
            $table->longText('provincial_address')->nullable()->after('supervisor_id');
            $table->string('mobile_number', 50)->nullable()->after('supervisor_id');
            $table->longText('present_address')->nullable()->after('supervisor_id');
            $table->date('effective_end_date')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auditors', function (Blueprint $table) {
            $table->dropColumn('nickname');
            $table->dropColumn('employee_number');
            $table->dropColumn('email_address');
            $table->dropColumn('provincial_address');
            $table->dropColumn('mobile_number');
        });
    }
}
