<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaHolidaysTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('holidays', function(Blueprint $table)
		{
			$table->date('holiday_date');
			$table->string('holiday_type', 100)->index('holidays_holiday_type_index');
			$table->string('description', 240)->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('holidays');
	}

}
