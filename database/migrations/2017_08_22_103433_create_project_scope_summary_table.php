<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectScopeSummaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_scope_summary', function (Blueprint $table) {
            $table->increments('project_scope_summary_id');
            $table->integer('project_id');
            $table->integer('scope_id');
            $table->integer('auditable_entity_id');
            $table->integer('ae_mbp_id');
            $table->date('planned_start_date');
            $table->date('planned_end_date');
            $table->date('actual_start_date');
            $table->date('actual_end_date');
            $table->string('grade');
            $table->text('remarks')->nullable();
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('last_update_by');
            $table->date('last_update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_scope_summary');
    }
}
