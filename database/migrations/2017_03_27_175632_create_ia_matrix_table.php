<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaMatrixTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('matrix', function(Blueprint $table)
		{
			$table->increments('matrix_id');
			$table->string('matrix_name', 20);
			$table->string('description', 240);
			$table->integer('x_dimension')->unsigned();
			$table->integer('y_dimension')->unsigned();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('matrix');
	}

}
