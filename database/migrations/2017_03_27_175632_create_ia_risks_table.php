<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaRisksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('risks', function(Blueprint $table)
		{
			$table->increments('risk_id');
			$table->string('risk_code', 100)->index('risks_risk_code_index');
			$table->string('risk_type', 100)->index('risks_risk_type_index');
			$table->text('risk_remarks')->nullable();
			$table->string('risk_source_type', 100)->index('risks_risk_source_type_index');
			$table->string('risk_response_type', 100)->index('risks_risk_response_type_index');
			$table->integer('def_inherent_impact_rate');
			$table->integer('def_inherent_likelihood_rate');
			$table->integer('def_residual_impact_rate');
			$table->integer('def_residual_likelihood_rate');
			$table->text('rating_comment')->nullable();
			$table->string('enterprise_risk_code', 100);
			$table->string('enterprise_risk_type', 100)->index('risks_enterprise_risk_type_index');
			$table->string('enterprise_risk_class', 100)->index('risks_enterprise_risk_class_index');
			$table->string('enterprise_risk_area', 100)->index('risks_enterprise_risk_area_index');
			$table->string('attribute1', 240)->nullable();
			$table->string('attribute2', 240)->nullable();
			$table->string('attribute3', 240)->nullable();
			$table->string('attribute4', 240)->nullable();
			$table->string('attribute5', 240)->nullable();
			$table->string('attribute6', 240)->nullable();
			$table->string('attribute7', 240)->nullable();
			$table->string('attribute8', 240)->nullable();
			$table->string('attribute9', 240)->nullable();
			$table->string('attribute10', 240)->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('risks');
	}

}
