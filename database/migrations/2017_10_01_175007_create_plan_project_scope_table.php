<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanProjectScopeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plan_project_scope', function(Blueprint $table)
		{
			$table->increments('plan_project_scope_id');
			$table->integer('plan_project_id')->unsigned();
			$table->integer('scope_sequence');
			$table->integer('auditable_entity_id')->unsigned();
			$table->integer('mbp_id')->unsigned();
			$table->string('audit_location', 80)->nullable();
			$table->decimal('budgeted_mandays', 5, 1)->nullable();
			$table->date('target_start_date')->nullable();
			$table->date('target_end_date')->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');

			$table->foreign('plan_project_id')
				->references('plan_project_id')
				->on('plan_projects')
				->onUpdate('RESTRICT')
				->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plan_project_scope');
	}

}
