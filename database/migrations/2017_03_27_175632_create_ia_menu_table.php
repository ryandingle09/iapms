<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaMenuTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menu', function(Blueprint $table)
		{
			$table->increments('menu_id');
			$table->string('menu_name', 100);
			$table->string('description', 240)->nullable();
			$table->date('effective_start_date');
			$table->date('effective_end_date')->nullable();
			$table->string('created_by', 15);
			$table->date('created_date');
			$table->string('last_update_by', 15);
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menu');
	}

}
