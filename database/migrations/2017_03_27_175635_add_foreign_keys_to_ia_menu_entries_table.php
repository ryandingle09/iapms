<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaMenuEntriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('menu_entries', function(Blueprint $table)
		{
			$table->foreign('menu_id', 'menu_id_fk')->references('id')->on('menus')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('permission_id', 'permission_id')->references('id')->on('permissions')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('menu_entries', function(Blueprint $table)
		{
			$table->dropForeign('menu_id_fk');
			$table->dropForeign('permission_id');
		});
	}

}
