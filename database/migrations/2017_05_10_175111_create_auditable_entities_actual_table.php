<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditableEntitiesActualTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditable_entities_actual', function (Blueprint $table) {
            $table->increments('auditable_entity_id');
            $table->string('auditable_entity_name', 240)->index();
            $table->string('company_code', 100)->index();
            $table->string('branch_code', 100)->index();
            $table->string('department_code', 100)->index()->nullable();
            $table->string('attribute1', 240)->nullable();
            $table->string('attribute2', 240)->nullable();
            $table->string('attribute3', 240)->nullable();
            $table->string('attribute4', 240)->nullable();
            $table->string('attribute5', 240)->nullable();
            $table->string('attribute6', 240)->nullable();
            $table->string('attribute7', 240)->nullable();
            $table->string('attribute8', 240)->nullable();
            $table->string('attribute9', 240)->nullable();
            $table->string('attribute10', 240)->nullable();
            $table->unsignedInteger('master_auditable_entity_id');
            $table->unsignedInteger('created_by');
            $table->date('created_date');
            $table->unsignedInteger('last_update_by');
            $table->date('last_update_date');

            $table->foreign('master_auditable_entity_id')
                  ->references('auditable_entity_id')
                  ->on('auditable_entities')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('auditable_entities_actual');
    }
}
