<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaMenuEntriesBackupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('menu_entries_backup', function(Blueprint $table)
		{
			$table->foreign('function_id', 'function_id1')->references('function_id')->on('functions')->onUpdate('NO ACTION')->onDelete('SET NULL');
			$table->foreign('menu_id', 'menu_id_FK1')->references('menu_id')->on('menu')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('menu_entries_backup', function(Blueprint $table)
		{
			$table->dropForeign('function_id1');
			$table->dropForeign('menu_id_FK1');
		});
	}

}
