<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaFunctionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('functions', function(Blueprint $table)
		{
			$table->increments('function_id');
			$table->string('function_name', 100)->index('function_name');
			$table->string('description', 240)->nullable();
			$table->string('executable_name', 30);
			$table->string('executable_type', 15);
			$table->date('effective_start_date');
			$table->date('effective_end_date')->nullable();
			$table->string('created_by', 15);
			$table->date('created_date');
			$table->string('last_update_by', 15);
			$table->date('last_update_date');
			$table->index(['executable_name','executable_type'], 'executable_name_executable_type');
			$table->index(['effective_start_date','effective_end_date'], 'effective_start_date_effective_end_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('functions');
	}

}
