<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectScopeFreezesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_scope_freeze', function(Blueprint $table) {
            $table->increments('project_scope_freeze_id');
            $table->integer('project_scope_id')->unsigned();
            $table->date('freeze_date');
            $table->date('effective_date_from');
            $table->date('effective_date_to')->nullable();
            $table->text('remarks')->nullable();
            $table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
			$table->foreign('project_scope_id')
					->references('project_scope_id')
					->on('project_scope')
					->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_scope_freeze');
	}

}
