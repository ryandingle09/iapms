<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaBpStepsRisksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bp_steps_risks', function(Blueprint $table)
		{
			$table->integer('bp_step_risk_seq')->unsigned();
			$table->integer('bp_step_id')->unsigned()->index('bp_steps_risks_bp_step_id_foreign');
			$table->integer('risk_id')->unsigned()->index('bp_steps_risks_bp_risk_idFK');
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bp_steps_risks');
	}

}
