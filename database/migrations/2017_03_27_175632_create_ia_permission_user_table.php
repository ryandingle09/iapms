<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaPermissionUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('permission_user', function(Blueprint $table)
		{
			$table->integer('user_id')->unsigned()->index('permission_user_user_id_index');
			$table->integer('permission_id')->unsigned()->index('permission_user_permission_id_index');
			$table->boolean('value')->default(-1);
			$table->dateTime('expires')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('permission_user');
	}

}
