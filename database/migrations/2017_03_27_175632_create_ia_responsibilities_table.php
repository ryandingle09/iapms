<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaResponsibilitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('responsibilities', function(Blueprint $table)
		{
			$table->increments('responsibility_id');
			$table->string('responsibility_name', 100)->index('responsibility_name');
			$table->string('description', 240)->nullable();
			$table->integer('menu_id')->unsigned()->nullable()->index('menu_FK');
			$table->date('effective_start_date');
			$table->date('effective_end_date')->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
			$table->index(['effective_start_date','effective_end_date'], 'effective_start_date_effective_end_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('responsibilities');
	}

}
