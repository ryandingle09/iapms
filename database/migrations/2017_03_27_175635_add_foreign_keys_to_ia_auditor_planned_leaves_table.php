<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaAuditorPlannedLeavesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('auditor_planned_leaves', function(Blueprint $table)
		{
			$table->foreign('auditor_id', 'auditor_planned_leaves_auditor_idFK')->references('auditor_id')->on('auditors')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('auditor_planned_leaves', function(Blueprint $table)
		{
			$table->dropForeign('auditor_planned_leaves_auditor_idFK');
		});
	}

}
