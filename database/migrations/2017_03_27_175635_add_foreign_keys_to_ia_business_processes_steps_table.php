<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaBusinessProcessesStepsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('business_processes_steps', function(Blueprint $table)
		{
			$table->foreign('bp_objective_id', 'business_processes_steps_bp_objective_id_foreign')->references('bp_objective_id')->on('bp_objectives')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('business_processes_steps', function(Blueprint $table)
		{
			$table->dropForeign('business_processes_steps_bp_objective_id_foreign');
		});
	}

}
