<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaControlsTestProcTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('controls_test_proc', function(Blueprint $table)
		{
			$table->foreign('control_id', 'controls_test_proc_control_id_foreign')->references('control_id')->on('controls_master')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('controls_test_proc', function(Blueprint $table)
		{
			$table->dropForeign('controls_test_proc_control_id_foreign');
		});
	}

}
