<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectScopeApgApprovalsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_scope_apg_approvals', function(Blueprint $table) {
            $table->increments('project_scope_apg_approval_id');
            $table->integer('apg_id')->unsigned();
            $table->integer('approval_seq')->nullable();
            $table->string('status_from');
            $table->string('status_to');
            $table->integer('auditor_id');
            $table->text('remarks');
            $table->integer('role');
            $table->integer('status_date');
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('last_update_by');
            $table->date('last_update_date');

            $table->foreign('apg_id')->references('project_scope_apg_id')->on('project_scope_apg')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_scope_apg_approvals');
	}

}
