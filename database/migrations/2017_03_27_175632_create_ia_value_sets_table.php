<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaValueSetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('value_sets', function(Blueprint $table)
		{
			$table->increments('value_set_id');
			$table->string('value_set_name', 20)->index('value_sets_value_set_name_index');
			$table->string('description', 240)->nullable();
			$table->string('code', 20)->index('value_sets_code_index');
			$table->string('meaning', 240);
			$table->string('from_clause', 240);
			$table->string('where_clause', 240);
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('value_sets');
	}

}
