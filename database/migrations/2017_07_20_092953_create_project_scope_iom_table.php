<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectScopeIomTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_scope_iom', function(Blueprint $table) {
            $table->integer('project_scope_id')->unsigned();
            $table->string('ref_no',100);
            $table->date('start_date');
            $table->longText('subject');
            $table->longText('introduction');
            $table->longText('background');
            $table->longText('objectives');
            $table->longText('auditee_assistance');
            $table->longText('staffing');
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('last_update_by');
            $table->date('last_update_date');

            $table->foreign('project_scope_id')->references('project_scope_id')->on('project_scope')->onDelete('cascade');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_scope_iom');
	}

}
