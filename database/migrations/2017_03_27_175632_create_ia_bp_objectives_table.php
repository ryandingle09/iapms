<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaBpObjectivesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bp_objectives', function(Blueprint $table)
		{
			$table->increments('bp_objective_id');
			$table->integer('bp_id')->unsigned()->index('bp_objectives_bp_id_foreign');
			$table->text('objective_narrative');
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bp_objectives');
	}

}
