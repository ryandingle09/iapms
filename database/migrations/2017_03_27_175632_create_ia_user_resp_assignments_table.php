<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaUserRespAssignmentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_resp_assignments', function(Blueprint $table)
		{
			$table->integer('user_id')->unsigned()->index('users_FK');
			$table->integer('responsibility_id')->unsigned()->nullable()->index('responsibility_FK');
			$table->date('effective_start_date');
			$table->date('effective_end_date')->nullable();
			$table->string('created_by', 15);
			$table->date('created_date');
			$table->string('last_update_by', 15);
			$table->date('last_update_date');
			$table->index(['effective_start_date','effective_end_date'], 'effective_start_date_effective_end_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_resp_assignments');
	}

}
