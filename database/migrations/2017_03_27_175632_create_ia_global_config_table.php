<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaGlobalConfigTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('global_config', function(Blueprint $table)
		{
			$table->integer('auditor_mandays_factor_rate');
			$table->integer('auditor_timeliness');
			$table->integer('auditor_quality');
			$table->integer('auditor_documentation');
			$table->integer('auditor_caf');
			$table->integer('reviewer_timeliness');
			$table->integer('reviewer_quality');
			$table->integer('reviewer_documentation');
			$table->text('iom_auditee_assist_narrative')->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('global_config');
	}

}
