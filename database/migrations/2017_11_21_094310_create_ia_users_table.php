<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('user_id');
			$table->string('user_name', 100)->index('user_name');
			$table->string('password', 60);
			$table->string('description', 240)->nullable();
			$table->string('display_name', 225)->nullable();
			$table->integer('employee_id')->nullable();
			$table->string('user_tag', 100)->nullable();
			$table->integer('user_tag_ref_id')->nullable();
			$table->date('effective_start_date')->nullable();
			$table->date('effective_end_date')->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->date('last_login_date')->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
