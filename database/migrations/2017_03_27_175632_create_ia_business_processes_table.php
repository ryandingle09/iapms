<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaBusinessProcessesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('business_processes', function(Blueprint $table)
		{
			$table->increments('bp_id');
			$table->string('bp_name', 240)->index('business_processes_bp_name_index');
			$table->string('bp_code', 100)->index('business_processes_bp_code_index');
			$table->string('objective_category', 100)->index('business_processes_objective_category_index');
			$table->string('source_type', 100)->index('business_processes_source_type_index');
			$table->string('source_ref', 100);
			$table->text('bp_remarks');
			$table->string('cm_1_name', 40)->nullable();
			$table->string('cm_1_high_score_type', 20)->nullable();
			$table->string('cm_2_name', 40)->nullable();
			$table->string('cm_2_high_score_type', 20)->nullable();
			$table->string('cm_3_name', 40)->nullable();
			$table->string('cm_3_high_score_type', 20)->nullable();
			$table->string('cm_4_name', 40)->nullable();
			$table->string('cm_4_high_score_type', 20)->nullable();
			$table->string('cm_5_name', 40)->nullable();
			$table->string('cm_5_high_score_type', 20)->nullable();
			$table->string('attribute1', 240)->nullable();
			$table->string('attribute2', 240)->nullable();
			$table->string('attribute3', 240)->nullable();
			$table->string('attribute4', 240)->nullable();
			$table->string('attribute5', 240)->nullable();
			$table->string('attribute6', 240)->nullable();
			$table->string('attribute7', 240)->nullable();
			$table->string('attribute8', 240)->nullable();
			$table->string('attribute9', 240)->nullable();
			$table->string('attribute10', 240)->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('business_processes');
	}

}
