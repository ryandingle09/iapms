<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAuditableEntitiesBpRemoveCmAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auditable_entities_bp', function (Blueprint $table) {
            $table->increments('ae_bp_id')->first();

            $cm_count = range(1,5);
            for ($i=0; $i < count($cm_count); $i++) {
                $table->dropColumn('cm_'.$cm_count[$i].'_value');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auditable_entities_bp', function (Blueprint $table) {
            $table->dropColumn('ae_bp_id');
            $cm_count = range(1,5);
            for ($i=0; $i < count($cm_count); $i++) {
                $table->integer('cm_'.$cm_count[$i].'_value');
            }
        });
    }
}
