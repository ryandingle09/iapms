<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaAuditorPlannedLeavesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('auditor_planned_leaves', function(Blueprint $table)
		{
			$table->integer('auditor_id')->unsigned()->index('auditor_planned_leaves_auditor_idFK_idx');
			$table->date('leave_date_from');
			$table->date('leave_date_to');
			$table->string('leave_type', 100)->index('auditor_planned_leaves_leave_type_index');
			$table->string('remarks', 240)->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
			$table->index(['leave_date_from','leave_date_to'], 'leave_date_from_leave_date_to');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('auditor_planned_leaves');
	}

}
