<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaLookupValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('lookup_values', function(Blueprint $table)
		{
			$table->foreign('lookup_type_id', 'lookup_types_fk')->references('id')->on('lookup_types')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('lookup_values', function(Blueprint $table)
		{
			$table->dropForeign('lookup_types_fk');
		});
	}

}
