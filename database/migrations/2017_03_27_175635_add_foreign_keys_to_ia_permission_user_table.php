<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaPermissionUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('permission_user', function(Blueprint $table)
		{
			$table->foreign('permission_id', 'permission_user_permission_id_foreign')->references('id')->on('permissions')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('user_id', 'permission_user_user_id_foreign')->references('user_id')->on('users')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('permission_user', function(Blueprint $table)
		{
			$table->dropForeign('permission_user_permission_id_foreign');
			$table->dropForeign('permission_user_user_id_foreign');
		});
	}

}
