<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIomDistListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('iom_dist_list', function (Blueprint $table) {
            $table->increments('iom_dist_list_id');
            $table->integer('project_scope_id')->unsigned();
            $table->integer('auditee_id');
            $table->string('distribution_type',20);
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('last_update_by');
            $table->date('last_update_date');
            $table->foreign('project_scope_id')->references('project_scope_id')->on('project_scope')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('iom_dist_list');
    }
}
