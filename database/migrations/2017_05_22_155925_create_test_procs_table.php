<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestProcsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_proc', function(Blueprint $table) {
            $table->increments('test_proc_id');
            $table->string('test_proc_name', 240)->indexed();
            $table->longText('test_proc_narrative');
            $table->text('audit_objective');
            $table->decimal('budgeted_mandays', 3, 1);
            $table->unsignedInteger('created_by');
            $table->date('created_date');
            $table->unsignedInteger('last_update_by');
            $table->date('last_update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('test_proc');
    }

}
