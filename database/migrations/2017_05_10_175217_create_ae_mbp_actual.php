<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAeMbpActual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ae_mbp_actual', function (Blueprint $table) {
            $table->increments('ae_mbp_id');
            $table->unsignedInteger('auditable_entity_id');
            $table->unsignedInteger('master_ae_mbp_id');
            $table->string('main_bp_name', 20)->index();

            $cm_count = range(1,10);
            for ($i=0; $i < count($cm_count); $i++) {
                $table->decimal('cm_'.$cm_count[$i].'_value',15,2)->nullable();
            }

            $table->decimal('walk_thru_mandays', 3, 1)->nullable();
            $table->decimal('planning_mandays', 3, 1)->nullable();
            $table->decimal('followup_mandays', 3, 1)->nullable();
            $table->decimal('discussion_mandays', 3, 1)->nullable();
            $table->decimal('draft_dar_mandays', 3, 1)->nullable();
            $table->decimal('final_report_mandays', 3, 1)->nullable();
            $table->decimal('wrap_up_mandays', 3, 1)->nullable();
            $table->decimal('reviewer_mandays', 3, 1)->nullable();
            $table->unsignedInteger('created_by');
            $table->date('created_date');
            $table->unsignedInteger('last_update_by');
            $table->date('last_update_date');

            $table->foreign('auditable_entity_id')
                  ->references('auditable_entity_id')
                  ->on('auditable_entities')
                  ->onDelete('cascade');

            $table->foreign('master_ae_mbp_id')
                  ->references('ae_mbp_id')
                  ->on('ae_mbp')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ae_mbp_actual');
    }
}
