<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaControlsTestAuditTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('controls_test_audit_types', function(Blueprint $table)
		{
			$table->integer('control_test_id')->unsigned()->index('controls_test_audit_types_control_test_id_foreign');
			$table->string('audit_type', 100);
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('controls_test_audit_types');
	}

}
