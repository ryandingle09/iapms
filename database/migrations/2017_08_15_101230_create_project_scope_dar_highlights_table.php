<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectScopeDarHighlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_scope_dar_highlights', function (Blueprint $table) {
            $table->increments('project_scope_dar_highlight_id');
            $table->integer('project_scope_id')->unsigned();
            $table->integer('highlights_seq')->nullable();
            $table->text('findings')->nullable();
            $table->text('risk')->nullable();
            $table->text('auditee_actions_taken')->nullable();
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('last_update_by');
            $table->date('last_update_date');
            $table->foreign('project_scope_id')->references('project_scope_id')->on('project_scope')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_scope_dar_highlights');
    }
}
