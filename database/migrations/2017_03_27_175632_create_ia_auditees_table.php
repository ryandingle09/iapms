<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaAuditeesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('auditees', function(Blueprint $table)
		{
			$table->increments('auditee_id');
			$table->string('full_name', 240)->index('auditees_full_name_index');
			$table->integer('auditable_entity_id')->unsigned();
			$table->string('position_code', 100)->index('auditees_position_code_index');
			$table->integer('supervisor_id')->unsigned()->nullable();
			$table->date('effective_start_date');
			$table->date('effective_end_date');
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('auditees');
	}

}
