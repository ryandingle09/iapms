<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBusinessProcessesTableRemovedObjectCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_processes', function (Blueprint $table) {
            $table->dropColumn('objective_category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_processes', function (Blueprint $table) {
            $table->string('objective_category', 100);
        });
    }
}
