<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalFromStatusesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('approval_from_status', function(Blueprint $table) {
            $table->increments('approval_from_status_id');
            $table->integer('approval_setup_detail_id')->unsigned();
            $table->string('status',50);
            $table->string('description',255);
            $table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');

			$table->foreign('approval_setup_detail_id','approval_from_status_approval_setup_detail_id_foreign')
				->references('approval_setup_detail_id')
				->on('approval_setup_details')
				// ->onUpdate('RESTRICT')
				->onDelete('CASCADE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('approval_from_status');
	}

}
