<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaAdditionalInfoMasterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('additional_info_master', function(Blueprint $table)
		{
			$table->increments('additional_info_id');
			$table->string('additional_info_name', 100)->index('additional_info_master_additional_info_name_index');
			$table->string('description', 240)->nullable();
			$table->string('table_name', 80)->index('additional_info_master_table_name_index');
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('additional_info_master');
	}

}
