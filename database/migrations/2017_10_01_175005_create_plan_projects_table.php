<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanProjectsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plan_projects', function(Blueprint $table)
		{
			$table->increments('plan_project_id');
			$table->integer('plan_group_id')->unsigned();
			$table->integer('project_sequence');
			$table->string('plan_project_name', 80)->index('plan_projects_plan_project_name_index');
			$table->string('audit_type', 100)->index('plan_projects_audit_type_index');
			$table->string('plan_project_status', 50)->index('plan_projects_plan_project_status_index');
			$table->date('target_start_date')->nullable();
			$table->date('target_end_date')->nullable();
			$table->text('plan_project_remarks')->nullable();
			$table->integer('reviewed_by')->unsigned()->nullable();
			$table->date('reviewed_date')->nullable();
			$table->text('reviewer_remarks')->nullable();
			$table->integer('approved_by')->unsigned()->nullable();
			$table->date('approved_date')->nullable();
			$table->text('approver_remarks')->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');

			$table->foreign('plan_group_id')
				->references('plan_group_id')
				->on('plan_groups')
				->onUpdate('RESTRICT')
				->onDelete('CASCADE');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plan_projects');
	}

}
