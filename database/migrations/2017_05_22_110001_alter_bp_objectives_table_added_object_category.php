<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBpObjectivesTableAddedObjectCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bp_objectives', function (Blueprint $table) {
            $table->string('objective_category', 100)->after('bp_id');
            $table->string('objective_name', 100)->indexed()->after('bp_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bp_objectives', function (Blueprint $table) {
            $table->dropColumn('objective_category');
            $table->dropColumn('narrative_name');
        });
    }
}
