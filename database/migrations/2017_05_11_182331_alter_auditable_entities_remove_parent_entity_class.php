<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAuditableEntitiesRemoveParentEntityClass extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('auditable_entities', function (Blueprint $table) {
            $table->dropColumn('group_type');
            $table->dropColumn('business_type');
            $table->dropColumn('entity_class');
            $table->dropColumn('parent_entity');
            $table->dropColumn('parent_ae_id');
            $table->dropColumn('template');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('auditable_entities', function (Blueprint $table) {
            $table->string('group_type', 100)->index();
            $table->string('business_type', 100)->index();
            $table->string('entity_class', 100)->index();
            $table->string('parent_entity', 1);
            $table->integer('parent_ae_id')->unsigned()->nullable();
            $table->string('template', 1)->default('N');
        });
    }
}
