<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaResponsibilitiesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('responsibilities', function(Blueprint $table)
		{
			$table->foreign('menu_id', 'menu_FK')->references('menu_id')->on('menu')->onUpdate('RESTRICT')->onDelete('SET NULL');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('responsibilities', function(Blueprint $table)
		{
			$table->dropForeign('menu_FK');
		});
	}

}
