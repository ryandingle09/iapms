<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiskAssessmentScaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_assessment_scale', function(Blueprint $table)
        {
            $table->increments('risk_assessment_scale_id');
            $table->integer('auditable_entity_id')->unsigned();
            $table->integer('ae_mbp_id')->unsigned();
            $table->integer('bp_id')->unsigned();
            $table->integer('bp_objective_id')->unsigned();
            $table->integer('bp_steps_id')->unsigned();
            $table->integer('risk_id')->unsigned();
            $table->integer('inherent_impact_value')->default(0);
            $table->integer('inherent_likelihood_value')->default(0);
            $table->text('inherent_remarks')->nullable();
            $table->integer('residual_impact_value')->default(0);
            $table->integer('residual_likelihood_value')->default(0);
            $table->text('residual_remarks')->nullable();
            $table->string('type');
            $table->integer('created_by')->unsigned();
            $table->date('created_date');
            $table->integer('last_update_by')->unsigned();
            $table->date('last_update_date');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('risk_assessment_scale');
    }
}
