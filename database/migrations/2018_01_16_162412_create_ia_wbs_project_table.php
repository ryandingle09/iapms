<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIaWbsProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wbs_project', function(Blueprint $table) {
            $table->increments('wbs_project_id');
            $table->integer('wbs_header_id')->unsigned();
            $table->integer('project_id');
            $table->string('project_name', 255);
            $table->string('audit_type', 255);
            $table->integer('project_head_id');
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('status', 20)->nullable();
            $table->integer('created_by')->unsigned();
            $table->date('created_date');
            $table->integer('last_update_by')->unsigned();
            $table->date('last_update_date');

            $table->foreign('wbs_header_id')
                ->references('wbs_header_id')
                ->on('wbs_header')
                ->onUpdate('RESTRICT')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wbs_project');
    }
}
