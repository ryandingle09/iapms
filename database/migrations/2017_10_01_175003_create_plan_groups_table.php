<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanGroupsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plan_groups', function(Blueprint $table) {
            $table->increments('plan_group_id');
            $table->integer('plan_id')->unsigned();
            $table->string('plan_group_name', 80);
            $table->text('description')->nullable();
            $table->integer('group_head')->unsigned();
            $table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');

			$table->foreign('plan_id')
				->references('plan_id')
				->on('plan')
				->onUpdate('RESTRICT')
				->onDelete('CASCADE');
				
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plan_groups');
	}

}
