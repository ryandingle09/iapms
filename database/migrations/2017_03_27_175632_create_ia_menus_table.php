<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaMenusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menus', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('parent_id')->unsigned()->index('parent_id_FK_idx');
			$table->string('name', 100)->index('permission_id_FK_idx');
			$table->boolean('menu_seq')->default(0);
			$table->string('route', 200)->nullable()->index('route_index');
			$table->integer('created_by')->unsigned()->nullable();
			$table->date('created_date')->nullable();
			$table->date('last_update_date')->nullable();
			$table->integer('last_update_by')->unsigned()->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menus');
	}

}
