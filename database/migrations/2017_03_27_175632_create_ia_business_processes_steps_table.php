<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaBusinessProcessesStepsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('business_processes_steps', function(Blueprint $table)
		{
			$table->increments('bp_steps_id');
			$table->integer('bp_objective_id')->unsigned()->index('business_processes_steps_bp_objective_id_foreign');
			$table->integer('bp_steps_seq');
			$table->text('activity_narrative');
			$table->string('attribute1', 240)->nullable();
			$table->string('attribute2', 240)->nullable();
			$table->string('attribute3', 240)->nullable();
			$table->string('attribute4', 240)->nullable();
			$table->string('attribute5', 240)->nullable();
			$table->string('attribute6', 240)->nullable();
			$table->string('attribute7', 240)->nullable();
			$table->string('attribute8', 240)->nullable();
			$table->string('attribute9', 240)->nullable();
			$table->string('attribute10', 240)->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('business_processes_steps');
	}

}
