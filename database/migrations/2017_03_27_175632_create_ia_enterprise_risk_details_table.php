<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaEnterpriseRiskDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('enterprise_risk_details', function(Blueprint $table)
		{
			$table->increments('enterprise_risk_det_id');
			$table->integer('enterprise_risk_id')->unsigned()->index('enterprise_risk_details_enterprise_risk_id_foreign');
			$table->string('risk_name', 20)->index('enterprise_risk_details_risk_name_index');
			$table->string('risk_desc', 240);
			$table->text('risk_remarks');
			$table->string('risk_type', 100)->index('enterprise_risk_details_risk_type_index');
			$table->string('risk_class', 100)->index('enterprise_risk_details_risk_class_index');
			$table->string('risk_area', 100)->index('enterprise_risk_details_risk_area_index');
			$table->string('response_type', 100)->index('enterprise_risk_details_response_type_index');
			$table->string('response_status', 100)->index('enterprise_risk_details_response_status_index');
			$table->integer('inherent_impact_rate');
			$table->integer('inherent_likelihood_rate');
			$table->integer('residual_impact_rate');
			$table->integer('residual_likelihood_rate');
			$table->text('rating_comment');
			$table->string('supporting_docs_name')->nullable();
			$table->string('supporting_docs_file')->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('enterprise_risk_details');
	}

}
