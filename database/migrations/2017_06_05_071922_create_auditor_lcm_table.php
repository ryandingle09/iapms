<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditorLcmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditor_lcm', function (Blueprint $table) {
            $table->increments('auditor_lcm_id');
            $table->integer('auditor_id')->unsigned();
            $table->string('lcm_type', 20)->index();
            $table->string('ref_no', 40)->index();
            $table->longText('remarks')->nullable();
            $table->date('issued_date');
            $table->date('expiry_date');
            $table->integer('created_by')->unsigned();
            $table->date('created_date');
            $table->integer('last_update_by')->unsigned();
            $table->date('last_update_date');

            $table->foreign('auditor_id')
            ->references('auditor_id')
            ->on('auditors')
            ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('auditor_lcm');
    }
}
