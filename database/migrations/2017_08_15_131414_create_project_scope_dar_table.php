<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectScopeDarTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('project_scope_dar', function(Blueprint $table) {
            $table->increments('project_scope_dar_id');
            $table->integer('project_scope_id')->unsigned();
            $table->text('subject')->nullable();
            $table->text('scope')->nullable();
            $table->text('objectives')->nullable();
            $table->text('opinion')->nullable();
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('last_update_by');
            $table->date('last_update_date');
            $table->foreign('project_scope_id')->references('project_scope_id')->on('project_scope')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('project_scope_dar');
	}

}
