<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovalSetupDetailsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('approval_setup_details', function(Blueprint $table) {
            $table->increments('approval_setup_detail_id');
            $table->integer('approval_setup_master_id')->unsigned();
            $table->integer('approval_sequence');
            $table->string('description', 200 )->nullable();
            $table->string('auditor_type',255);
            $table->integer('approver_flag')->default(0);
            $table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');

			$table->foreign('approval_setup_master_id','approval_setup_details_approval_setup_master_id_foreign')
				->references('approval_setup_master_id')
				->on('approval_setup_masters')
				// ->onUpdate('RESTRICT')
				->onDelete('CASCADE');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('approval_setup_details');
	}

}
