<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaMatrixDetailsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('matrix_details', function(Blueprint $table)
		{
			$table->foreign('matrix_id', 'matrix_details_matrix_id_foreign')->references('matrix_id')->on('matrix')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('matrix_details', function(Blueprint $table)
		{
			$table->dropForeign('matrix_details_matrix_id_foreign');
		});
	}

}
