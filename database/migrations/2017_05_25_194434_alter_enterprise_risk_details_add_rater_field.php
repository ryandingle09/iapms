<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEnterpriseRiskDetailsAddRaterField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('enterprise_risk_details', function (Blueprint $table) {
            $table->date('rated_date')->nullable()->after('supporting_docs_file');
            $table->unsignedInteger('rated_by')->after('supporting_docs_file');
            $table->longText('response_status_comment')->after('response_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enterprise_risk_details', function (Blueprint $table) {
            $table->dropColumn('rated_by');
            $table->dropColumn('rated_date');
            $table->dropColumn('response_status_comment');
        });
    }
}
