<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaMenuEntriesBackupTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menu_entries_backup', function(Blueprint $table)
		{
			$table->integer('menu_id')->unsigned()->index('menu_id_FK_idx1');
			$table->integer('function_id')->unsigned()->nullable()->index('function_id_idx1');
			$table->integer('parent_menu')->unsigned()->index('parent_menu_idx1');
			$table->boolean('menu_seq')->default(0);
			$table->date('effective_start_date');
			$table->date('effective_end_date')->nullable();
			$table->string('created_by', 15);
			$table->date('created_date');
			$table->string('last_update_by', 15);
			$table->date('last_update_date');
			$table->increments('id');
			$table->integer('permission_id')->unsigned()->index('permission_idFK_idx1');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menu_entries_backup');
	}

}
