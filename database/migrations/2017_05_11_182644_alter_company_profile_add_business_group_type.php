<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCompanyProfileAddBusinessGroupType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_profile', function (Blueprint $table) {
            $table->string('group_type', 100)->index()->after('description');
            $table->string('business_type', 100)->index()->after('description');
            $table->string('head_office', 100)->index()->after('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_profile', function (Blueprint $table) {
            $table->dropColumn('group_type');
            $table->dropColumn('business_type');
            $table->dropColumn('head_office');
        });
    }
}
