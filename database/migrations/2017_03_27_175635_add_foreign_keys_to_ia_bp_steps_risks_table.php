<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToIaBpStepsRisksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bp_steps_risks', function(Blueprint $table)
		{
			$table->foreign('risk_id', 'bp_steps_risks_bp_risk_idFK')->references('risk_id')->on('risks')->onUpdate('RESTRICT')->onDelete('CASCADE');
			$table->foreign('bp_step_id', 'bp_steps_risks_bp_step_idFK')->references('bp_steps_id')->on('business_processes_steps')->onUpdate('RESTRICT')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bp_steps_risks', function(Blueprint $table)
		{
			$table->dropForeign('bp_steps_risks_bp_risk_idFK');
			$table->dropForeign('bp_steps_risks_bp_step_idFK');
		});
	}

}
