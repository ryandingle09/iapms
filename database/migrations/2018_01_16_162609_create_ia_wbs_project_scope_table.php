<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIaWbsProjectScopeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wbs_project_scope', function(Blueprint $table) {
            $table->increments('wbs_project_scope_id');
            $table->integer('wbs_project_id')->unsigned();
            $table->integer('wbs_sequence');
            $table->integer('project_scope_id');
            $table->integer('iom_number');
            $table->integer('auditable_entity_id');
            $table->integer('ae_mbp_id');
            $table->integer('budgeted_mandays');
            $table->integer('task_mode')->nullable();
            $table->integer('predecessor')->nullable();
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->string('status', 20)->nullable();
            $table->integer('created_by')->unsigned();
            $table->date('created_date');
            $table->integer('last_update_by')->unsigned();
            $table->date('last_update_date');   

            $table->foreign('wbs_project_id')
                ->references('wbs_project_id')
                ->on('wbs_project')
                ->onUpdate('RESTRICT')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wbs_project_scope');
    }
}
