<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiskAssessmentScaleAnnualAsvTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('risk_assessment_scale_annual_asv', function(Blueprint $table) {
			$table->increments('risk_assessment_scale_annual_asv_id');
        	$table->integer('year');
        	$table->integer('auditable_entity_id');
        	$table->integer('ae_mbp_id');
        	$table->integer('bp_id')->nullable();
        	$table->integer('bp_objective_id')->nullable();
        	$table->integer('bp_steps_id')->nullable();
        	$table->integer('risk_id');
        	$table->integer('adjusted_severity_value');
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('last_update_by');
            $table->date('last_update_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('risk_assessment_scale_annual_asv');
	}

}
