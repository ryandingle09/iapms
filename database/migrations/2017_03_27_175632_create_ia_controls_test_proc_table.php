<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateIaControlsTestProcTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('controls_test_proc', function(Blueprint $table)
		{
			$table->increments('control_test_id');
			$table->integer('control_id')->unsigned()->index('controls_test_proc_control_id_foreign');
			$table->integer('control_test_seq');
			$table->string('test_proc_narrative', 100);
			$table->integer('budgeted_mandays');
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('controls_test_proc');
	}

}
