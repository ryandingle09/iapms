<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectScopeAuditorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_scope_auditors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_scope_id')->unsigned();;
            $table->integer('auditor_id');
            $table->string('auditor_type',20);
            $table->integer('allotted_mandays_misc');
            $table->integer('created_by');
            $table->date('created_date');
            $table->integer('last_update_by');
            $table->date('last_update_date');
            $table->foreign('project_scope_id')->references('project_scope_id')->on('project_scope')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_scope_auditors');
    }
}
