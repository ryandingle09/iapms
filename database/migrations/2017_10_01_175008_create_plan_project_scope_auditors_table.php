<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlanProjectScopeAuditorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('plan_project_scope_auditors', function(Blueprint $table)
		{
			$table->increments('plan_project_scope_auditor_id');
			$table->integer('plan_project_scope_id')->unsigned();
			$table->integer('auditor_id')->unsigned();
			$table->string('auditor_type', 100)->nullable();
			$table->decimal('allotted_mandays', 5, 1)->nullable();
			$table->integer('created_by')->unsigned();
			$table->date('created_date');
			$table->integer('last_update_by')->unsigned();
			$table->date('last_update_date');

			$table->foreign('plan_project_scope_id')
				->references('plan_project_scope_id')
				->on('plan_project_scope')
				->onUpdate('RESTRICT')
				->onDelete('CASCADE');

		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('plan_project_scope_auditors');
	}

}
