<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('users')->delete();

        \DB::table('users')->insert(array (
            0 =>
            array (
                'user_id' => 1,
                'user_name' => 'mgarcega',
                'password' => '$2y$10$rIm6ODeX6JXRazlBQaQgnuAufb6WdEbC6cogNgBx3e4hCi6cIGG.S',
                'description' => 'test admin',
                'employee_id' => 1000,
                'effective_start_date' => '2016-11-01',
                'effective_end_date' => '2017-01-31',
                'remember_token' => 'Rmxp8bbEZc1vcn9AiuyVCQFX4J80xudDiedtYo6AeJmDp1o9DeyGJAHMeHcm',
                'last_login_date' => '2017-01-19',
                'created_by' => 1,
                'created_date' => '2016-10-20',
                'last_update_by' => 1,
                'last_update_date' => '2017-01-19',
            ),
        ));


    }
}