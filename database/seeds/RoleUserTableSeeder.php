<?php

use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('role_user')->delete();
        
        \DB::table('role_user')->insert(array (
            0 => 
            array (
                'user_id' => 1,
                'role_id' => 1,
            ),
            1 => 
            array (
                'user_id' => 11,
                'role_id' => 3,
            ),
            2 => 
            array (
                'user_id' => 10,
                'role_id' => 2,
            ),
            3 => 
            array (
                'user_id' => 15,
                'role_id' => 3,
            ),
            4 => 
            array (
                'user_id' => 1,
                'role_id' => 3,
            ),
            5 => 
            array (
                'user_id' => 21,
                'role_id' => 1,
            ),
            6 => 
            array (
                'user_id' => 21,
                'role_id' => 3,
            ),
            7 => 
            array (
                'user_id' => 21,
                'role_id' => 2,
            ),
            8 => 
            array (
                'user_id' => 23,
                'role_id' => 1,
            ),
            9 => 
            array (
                'user_id' => 23,
                'role_id' => 2,
            ),
            10 => 
            array (
                'user_id' => 23,
                'role_id' => 3,
            ),
            11 => 
            array (
                'user_id' => 24,
                'role_id' => 1,
            ),
            12 => 
            array (
                'user_id' => 24,
                'role_id' => 2,
            ),
            13 => 
            array (
                'user_id' => 24,
                'role_id' => 3,
            ),
            14 => 
            array (
                'user_id' => 25,
                'role_id' => 1,
            ),
            15 => 
            array (
                'user_id' => 25,
                'role_id' => 2,
            ),
            16 => 
            array (
                'user_id' => 25,
                'role_id' => 3,
            ),
            17 => 
            array (
                'user_id' => 26,
                'role_id' => 1,
            ),
            18 => 
            array (
                'user_id' => 26,
                'role_id' => 2,
            ),
            19 => 
            array (
                'user_id' => 26,
                'role_id' => 3,
            ),
            20 => 
            array (
                'user_id' => 27,
                'role_id' => 1,
            ),
            21 => 
            array (
                'user_id' => 27,
                'role_id' => 2,
            ),
            22 => 
            array (
                'user_id' => 27,
                'role_id' => 3,
            ),
            23 => 
            array (
                'user_id' => 28,
                'role_id' => 1,
            ),
            24 => 
            array (
                'user_id' => 28,
                'role_id' => 2,
            ),
            25 => 
            array (
                'user_id' => 28,
                'role_id' => 3,
            ),
            26 => 
            array (
                'user_id' => 29,
                'role_id' => 1,
            ),
            27 => 
            array (
                'user_id' => 29,
                'role_id' => 2,
            ),
            28 => 
            array (
                'user_id' => 29,
                'role_id' => 3,
            ),
            29 => 
            array (
                'user_id' => 30,
                'role_id' => 1,
            ),
            30 => 
            array (
                'user_id' => 31,
                'role_id' => 1,
            ),
            31 => 
            array (
                'user_id' => 32,
                'role_id' => 2,
            ),
            32 => 
            array (
                'user_id' => 33,
                'role_id' => 3,
            ),
        ));
        
        
    }
}