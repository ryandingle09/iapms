<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'administrator',
                'created_at' => '2016-11-07 04:53:35',
                'updated_at' => '2016-11-07 04:53:35',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'auditor',
                'created_at' => '2016-11-07 05:15:02',
                'updated_at' => '2016-11-07 05:15:02',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'auditee',
                'created_at' => '2016-11-07 05:15:13',
                'updated_at' => '2016-11-07 05:15:13',
            ),
        ));
        
        
    }
}