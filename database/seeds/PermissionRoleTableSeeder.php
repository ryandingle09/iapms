<?php

use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('permission_role')->delete();
        
        \DB::table('permission_role')->insert(array (
            0 => 
            array (
                'permission_id' => 10,
                'role_id' => 2,
                'value' => -1,
                'expires' => NULL,
            ),
            1 => 
            array (
                'permission_id' => 12,
                'role_id' => 2,
                'value' => -1,
                'expires' => NULL,
            ),
            2 => 
            array (
                'permission_id' => 11,
                'role_id' => 2,
                'value' => -1,
                'expires' => NULL,
            ),
            3 => 
            array (
                'permission_id' => 10,
                'role_id' => 3,
                'value' => -1,
                'expires' => NULL,
            ),
            4 => 
            array (
                'permission_id' => 11,
                'role_id' => 3,
                'value' => -1,
                'expires' => NULL,
            ),
            5 => 
            array (
                'permission_id' => 27,
                'role_id' => 2,
                'value' => -1,
                'expires' => NULL,
            ),
            6 => 
            array (
                'permission_id' => 63,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            7 => 
            array (
                'permission_id' => 66,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            8 => 
            array (
                'permission_id' => 68,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            9 => 
            array (
                'permission_id' => 72,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            10 => 
            array (
                'permission_id' => 73,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            11 => 
            array (
                'permission_id' => 74,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            12 => 
            array (
                'permission_id' => 77,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            13 => 
            array (
                'permission_id' => 77,
                'role_id' => 2,
                'value' => -1,
                'expires' => NULL,
            ),
            14 => 
            array (
                'permission_id' => 76,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            15 => 
            array (
                'permission_id' => 62,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            16 => 
            array (
                'permission_id' => 33,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            17 => 
            array (
                'permission_id' => 35,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            18 => 
            array (
                'permission_id' => 34,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            19 => 
            array (
                'permission_id' => 4,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            20 => 
            array (
                'permission_id' => 6,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            21 => 
            array (
                'permission_id' => 5,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            22 => 
            array (
                'permission_id' => 30,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            23 => 
            array (
                'permission_id' => 32,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            24 => 
            array (
                'permission_id' => 31,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            25 => 
            array (
                'permission_id' => 1,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            26 => 
            array (
                'permission_id' => 3,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            27 => 
            array (
                'permission_id' => 2,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            28 => 
            array (
                'permission_id' => 7,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            29 => 
            array (
                'permission_id' => 9,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            30 => 
            array (
                'permission_id' => 8,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            31 => 
            array (
                'permission_id' => 27,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            32 => 
            array (
                'permission_id' => 29,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            33 => 
            array (
                'permission_id' => 28,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            34 => 
            array (
                'permission_id' => 69,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            35 => 
            array (
                'permission_id' => 67,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            36 => 
            array (
                'permission_id' => 42,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            37 => 
            array (
                'permission_id' => 70,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            38 => 
            array (
                'permission_id' => 10,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            39 => 
            array (
                'permission_id' => 12,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            40 => 
            array (
                'permission_id' => 11,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            41 => 
            array (
                'permission_id' => 51,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            42 => 
            array (
                'permission_id' => 55,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            43 => 
            array (
                'permission_id' => 52,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            44 => 
            array (
                'permission_id' => 57,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            45 => 
            array (
                'permission_id' => 59,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            46 => 
            array (
                'permission_id' => 61,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            47 => 
            array (
                'permission_id' => 71,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            48 => 
            array (
                'permission_id' => 65,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
            49 => 
            array (
                'permission_id' => 75,
                'role_id' => 1,
                'value' => -1,
                'expires' => NULL,
            ),
        ));
        
        
    }
}