<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('permissions')->delete();

        \DB::table('permissions')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'administrator.responsibility.create',
                'readable_name' => 'Create responsibilities',
                'created_at' => '2016-11-07 10:35:49',
                'updated_at' => '2016-11-07 10:56:53',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'administrator.responsibility.edit',
                'readable_name' => 'Edit responsibilities',
                'created_at' => '2016-11-08 10:44:00',
                'updated_at' => '2016-11-08 10:44:00',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'administrator.responsibility.delete',
                'readable_name' => 'Delete responsibilities',
                'created_at' => '2016-11-08 10:44:28',
                'updated_at' => '2016-11-08 10:44:28',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'administrator.menu.create',
                'readable_name' => 'Create menu',
                'created_at' => '2016-11-08 10:47:19',
                'updated_at' => '2016-11-08 10:47:19',
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'administrator.menu.edit',
                'readable_name' => 'Edit menu',
                'created_at' => '2016-11-08 17:32:21',
                'updated_at' => '2016-11-08 17:32:21',
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'administrator.menu.delete',
                'readable_name' => 'Delete menu',
                'created_at' => '2016-11-08 17:32:32',
                'updated_at' => '2016-11-08 17:32:32',
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'administrator.roles.create',
                'readable_name' => 'Create role',
                'created_at' => '2016-11-08 19:02:15',
                'updated_at' => '2016-11-08 19:02:15',
            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'administrator.roles.edit',
                'readable_name' => 'Edit roles',
                'created_at' => '2016-11-08 19:02:29',
                'updated_at' => '2016-11-08 19:02:29',
            ),
            8 =>
            array (
                'id' => 9,
                'name' => 'administrator.roles.delete',
                'readable_name' => 'Delete role',
                'created_at' => '2016-11-08 19:02:40',
                'updated_at' => '2016-11-08 19:02:40',
            ),
            9 =>
            array (
                'id' => 10,
                'name' => 'auditor.list.create',
                'readable_name' => 'Create audit',
                'created_at' => '2016-11-10 17:24:27',
                'updated_at' => '2016-11-25 16:31:41',
            ),
            10 =>
            array (
                'id' => 11,
                'name' => 'auditor.list.edit',
                'readable_name' => 'Edit audit',
                'created_at' => '2016-11-10 17:24:40',
                'updated_at' => '2016-11-25 16:31:59',
            ),
            11 =>
            array (
                'id' => 12,
                'name' => 'auditor.list.delete',
                'readable_name' => 'Delete audit',
                'created_at' => '2016-11-10 17:24:53',
                'updated_at' => '2016-11-25 16:32:15',
            ),
            12 =>
            array (
                'id' => 27,
                'name' => 'administrator.users.create',
                'readable_name' => 'Create users',
                'created_at' => '2016-11-14 17:30:24',
                'updated_at' => '2016-11-14 17:30:24',
            ),
            13 =>
            array (
                'id' => 28,
                'name' => 'administrator.users.edit',
                'readable_name' => 'Edit users',
                'created_at' => '2016-11-14 17:47:16',
                'updated_at' => '2016-11-14 17:47:16',
            ),
            14 =>
            array (
                'id' => 29,
                'name' => 'administrator.users.delete',
                'readable_name' => 'Delete users',
                'created_at' => '2016-11-14 17:47:54',
                'updated_at' => '2016-11-14 17:47:54',
            ),
            15 =>
            array (
                'id' => 30,
                'name' => 'administrator.permissions.create',
                'readable_name' => 'Create permissions',
                'created_at' => '2016-11-14 18:24:08',
                'updated_at' => '2016-11-14 18:24:08',
            ),
            16 =>
            array (
                'id' => 31,
                'name' => 'administrator.permissions.edit',
                'readable_name' => 'Edit permissions',
                'created_at' => '2016-11-14 18:24:19',
                'updated_at' => '2016-11-14 18:24:19',
            ),
            17 =>
            array (
                'id' => 32,
                'name' => 'administrator.permissions.delete',
                'readable_name' => 'Delete permissions',
                'created_at' => '2016-11-14 18:24:30',
                'updated_at' => '2016-11-14 18:24:30',
            ),
            18 =>
            array (
                'id' => 33,
                'name' => 'administrator.lookup.create',
                'readable_name' => 'Create lookup',
                'created_at' => '2016-11-15 19:16:01',
                'updated_at' => '2016-11-15 19:16:01',
            ),
            19 =>
            array (
                'id' => 34,
                'name' => 'administrator.lookup.edit',
                'readable_name' => 'Edit lookup',
                'created_at' => '2016-11-15 19:16:21',
                'updated_at' => '2016-11-15 19:16:21',
            ),
            20 =>
            array (
                'id' => 35,
                'name' => 'administrator.lookup.delete',
                'readable_name' => 'Delete lookup',
                'created_at' => '2016-11-15 19:16:35',
                'updated_at' => '2016-11-15 19:16:35',
            ),
            21 =>
            array (
                'id' => 42,
                'name' => 'auditee.create',
                'readable_name' => 'Create auditee',
                'created_at' => '2016-11-23 14:41:44',
                'updated_at' => '2016-11-23 14:41:44',
            ),
            22 =>
            array (
                'id' => 51,
                'name' => 'auditor.setup.holiday.create',
                'readable_name' => 'Create auditor holiday',
                'created_at' => '2016-11-28 18:27:14',
                'updated_at' => '2016-11-28 18:27:14',
            ),
            23 =>
            array (
                'id' => 52,
                'name' => 'auditor.setup.holiday.edit',
                'readable_name' => 'Edit auditor holiday setup',
                'created_at' => '2016-11-28 18:30:41',
                'updated_at' => '2016-11-28 18:30:41',
            ),
            24 =>
            array (
                'id' => 55,
                'name' => 'auditor.setup.holiday.delete',
                'readable_name' => 'Delete auditor holiday setup',
                'created_at' => '2016-11-28 18:42:10',
                'updated_at' => '2016-11-28 18:42:10',
            ),
            25 =>
            array (
                'id' => 57,
                'name' => 'auditor.setup.leave.create',
                'readable_name' => 'Create auditor leave setup',
                'created_at' => '2016-11-28 18:45:30',
                'updated_at' => '2016-11-28 18:45:30',
            ),
            26 =>
            array (
                'id' => 59,
                'name' => 'auditor.setup.leave.delete',
                'readable_name' => 'Delete auditor leave setup',
                'created_at' => '2016-11-28 18:46:18',
                'updated_at' => '2016-11-28 18:46:18',
            ),
            27 =>
            array (
                'id' => 61,
                'name' => 'auditor.setup.leave.edit',
                'readable_name' => 'Edit auditor leave setup',
                'created_at' => '2016-11-28 19:12:15',
                'updated_at' => '2016-11-28 19:12:15',
            ),
            28 =>
            array (
                'id' => 62,
                'name' => 'administrator.global_config.update',
                'readable_name' => 'Update global config',
                'created_at' => '2016-12-02 11:26:29',
                'updated_at' => '2016-12-28 10:22:59',
            ),
            29 =>
            array (
                'id' => 63,
                'name' => 'controls_registry.create',
                'readable_name' => 'Create controls registry',
                'created_at' => '2016-12-02 19:12:34',
                'updated_at' => '2016-12-05 18:53:32',
            ),
            30 =>
            array (
                'id' => 65,
                'name' => 'risks.store',
                'readable_name' => 'Create risk',
                'created_at' => '2016-12-07 09:57:19',
                'updated_at' => '2016-12-07 13:29:09',
            ),
            31 =>
            array (
                'id' => 66,
                'name' => 'business_process.create',
                'readable_name' => 'Create business process',
                'created_at' => '2016-12-16 19:05:06',
                'updated_at' => '2016-12-16 19:05:06',
            ),
            32 =>
            array (
                'id' => 67,
                'name' => 'auditable_entities.list.create',
                'readable_name' => 'Create auditable entities',
                'created_at' => '2016-12-19 10:12:53',
                'updated_at' => '2016-12-27 15:06:03',
            ),
            33 =>
            array (
                'id' => 68,
                'name' => 'company.create',
                'readable_name' => 'Create company',
                'created_at' => '2016-12-27 14:49:45',
                'updated_at' => '2016-12-27 14:49:45',
            ),
            34 =>
            array (
                'id' => 69,
                'name' => 'auditable_entities.inquiry.create',
                'readable_name' => 'Auditable entities inquiries',
                'created_at' => '2016-12-27 15:13:33',
                'updated_at' => '2016-12-27 15:15:18',
            ),
            35 =>
            array (
                'id' => 70,
                'name' => 'auditor.grading_factors.create',
                'readable_name' => 'Create grading factors',
                'created_at' => '2017-01-04 11:40:36',
                'updated_at' => '2017-01-04 11:40:36',
            ),
            36 =>
            array (
                'id' => 71,
                'name' => 'risk_assessment_scale.create',
                'readable_name' => 'Create risk assessment scale',
                'created_at' => '2017-01-10 08:26:46',
                'updated_at' => '2017-01-10 08:26:46',
            ),
            37 =>
            array (
                'id' => 72,
                'name' => 'matrix_definition.create',
                'readable_name' => 'Create matrix definition',
                'created_at' => '2017-01-10 08:27:15',
                'updated_at' => '2017-01-10 08:27:15',
            ),
            38 =>
            array (
                'id' => 73,
                'name' => 'questionnaire.create',
                'readable_name' => 'Create questionnaire',
                'created_at' => '2017-01-10 13:01:50',
                'updated_at' => '2017-01-10 13:01:50',
            ),
            39 =>
            array (
                'id' => 74,
                'name' => 'enterprise_risk.create',
                'readable_name' => 'Create enterprise risk',
                'created_at' => '2017-01-13 13:58:07',
                'updated_at' => '2017-01-13 13:58:07',
            ),
            40 =>
            array (
                'id' => 75,
                'name' => 'value_set.create',
                'readable_name' => 'Create value set',
                'created_at' => '2017-01-16 16:01:44',
                'updated_at' => '2017-01-16 16:01:44',
            ),
            41 =>
            array (
                'id' => 76,
                'name' => 'additional_information.create',
                'readable_name' => 'Create additional information',
                'created_at' => '2017-01-17 09:36:55',
                'updated_at' => '2017-01-17 09:36:55',
            ),
            42 =>
            array (
                'id' => 77,
                'name' => 'planning.create',
                'readable_name' => 'Create planning',
                'created_at' => '2017-02-14 09:45:51',
                'updated_at' => '2017-02-14 09:45:51',
            ),
            43=>
            array (
                'id' => 78,
                'name' => 'audit_universe',
                'readable_name' => 'Audit universe',
                'created_at' => '2017-02-14 09:45:51',
                'updated_at' => '2017-02-14 09:45:51',
            ),
            44 =>
            array (
                'id' => 79,
                'name' => 'coso',
                'readable_name' => 'C.O.S.O',
                'created_at' => '2017-02-14 09:45:51',
                'updated_at' => '2017-02-14 09:45:51',
            ),
        ));


    }
}