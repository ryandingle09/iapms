<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'user_name' => $faker->username,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\LookupType::class, function (Faker\Generator $faker) {
    return [
        'lookup_type' => $faker->lookup_type,
        'meaning' => $faker->meaning,
        'description' => $faker->description,
        'created_by' => $faker->created_by,
        'created_date' => $faker->created_date,
        'last_update_by' => $faker->last_update_by,
        'last_update_date' => $faker->last_update_date,
    ];
});

$factory->define(App\Models\UserRole::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
    ];
});

$factory->define(App\Models\Auditee::class, function (Faker\Generator $faker) {
    return [
        'full_name' => $faker->name,
        'company_code' => $faker->company,
        'group_code' => $faker->domainWord,
        'position_code' => $faker->tld,
        'supervisor_id' => $faker->randomDigit,
        'effective_start_date' => $faker->date,
        'effective_end_date' => $faker->date,
    ];
});

$factory->define(App\Models\Auditor::class, function (Faker\Generator $faker) {
    return [
        'full_name' => $faker->name,
        'position_code' => $faker->tld,
        'supervisor_id' => $faker->randomDigit,
        'effective_start_date' => $faker->date,
        'effective_end_date' => $faker->date,
        'created_by' => $faker->randomDigit,
        'last_update_by' => $faker->randomDigit,
    ];
});

$factory->define(App\Models\AuditorProficiency::class, function (Faker\Generator $faker) {
    return [
        'auditor_id' => 1,
        'proficiency_code' => $faker->tld,
    ];
});

$factory->define(App\Models\Holiday::class, function (Faker\Generator $faker) {
    return [
        'holiday_date' => $faker->date,
        'holiday_type' => $faker->word,
        'description' => $faker->sentence,
    ];
});

$factory->define(App\Models\AuditorPlannedLeave::class, function (Faker\Generator $faker) {
    return [
        'leave_date' => $faker->date,
        'leave_type' => $faker->word,
        'remarks' => $faker->sentence,
        'effective_start_date' => $faker->date,
        'effective_end_date' => $faker->date,
    ];
});

$factory->define(App\Models\ControlsMaster::class, function (Faker\Generator $faker) {
    return [
        'component_code' => $faker->word,
        'principle_code' => $faker->word,
        'focus_code' => $faker->word,
    ];
});