/*
* This plugin is just an extention of chosen jquery so it is dependent to that plugin
* Source : https://learn.jquery.com/plugins/basic-plugin-creation/
*/
(function ( $ ) {

    $.fn.makeSelectize = function(options){
        var settings = $.extend({
            selectedTemplate : "id_text",
            valueField: 'id',
            labelField: 'text',
            searchField: 'text',
            lookup : null,
            url : null,
            preload : true,
            resultLimit : false,
        }, options );
        if(settings.lookup != null){
            var newUrl = base_url+'/administrator/lookup/type/'+settings.lookup;
            settings = $.extend(settings,{
                create: false,
                render: {
                    option: function(item, escape) {
                        return $.fn.makeSelectize.getTemplate(settings.selectedTemplate , item, escape);
                    }
                },
                load: function(query, callback) {
                    // if (!query.length) return callback();
                    var limit = settings.resultLimit ? '&limit='+settings.resultLimit : '';
                    var search = query != '' ? '?search='+query : '';
                    $.ajax({
                        url: newUrl+search+limit,
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(results) {
                            callback(results.data); 
                        }
                    });
                }
            });
        }else if(settings.url != null){
            settings = $.extend(settings,{
                create: false,
                render: {
                    option: function(item, escape) {

                        return $.fn.makeSelectize.getTemplate(settings.selectedTemplate , item, escape);
                    }
                },
                load: function(query, callback) {
                    // if (!query.length) return callback();
                    $.ajax({
                        url: settings.url,
                        type: 'GET',
                        data : {
                            search : query
                        },
                        dataType : "json",
                        error: function() {
                            callback();
                        },
                        success: function(results) {
                            if(results.data == undefined){
                                callback();
                            }else{
                                callback(results.data); 
                            }
                        }
                    });
                }
            });
        }
        return this.each(function(){
            var element = $(this);
            var $select = element.selectize(settings);
        })
    }

    $.fn.makeSelectize.getTemplate = function(selectedTemplate, item, escape){
        if(typeof selectedTemplate == 'function'){
            return selectedTemplate(item, escape);
        }else{
            switch(selectedTemplate){
                case 'id_text_meaning_description' :
                    return '<div> <b> '+item.text+'</b> | '+item.meaning+' | '+item.description+'</div>';
                    break;
                case 'id_code_meaning' :
                    return '<div> <b> '+item.code+'</b> | '+item.meaning+'</div>';
                    break;
                case 'id_text_description' :
                    return '<div> <b>'+item.text+'</b> | '+item.description+' </div>';
                    break;
                case 'id_text' :
                    return '<div> '+item.text+'</div>';
                    break;
                case 'id_text_meaning' :
                    return '<div> '+item.text+'</b> | '+item.meaning+'</div>';
                    break;
                default :
                    return '<div> '+item.text+'</div>';
            }
        }
    }

    $.reloadURL = function(selectize, url){
        selectize.clear();
        selectize.clearOptions();
        selectize.settings.load =  function(query, callback) {
            $.ajax({
                url: url,
                type: 'GET',
                data : {
                    search : query
                },
                dataType : "json",
                error: function() {
                    callback();
                },
                success: function(results) {
                    if(results.data == undefined){
                        callback();
                    }else{
                        callback(results.data); 
                    }
                }
            });
        }

        selectize.load( function(callback) {
            $.ajax({
                url: url,
                success: function(response) {
                    callback(response.data);
                },
                error: function() {
                    callback();
                }
            });
        } );
    }

}( jQuery ));