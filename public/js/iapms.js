if (typeof jQuery === 'undefined') { throw new Error('JavaScript requires jQuery') }


var iapms = {
    /**
     * generate select2 component
     * @param  element element
     * @param  string url
     * @param  string searchFields
     * @return
     */
    generateSelect2 : function( element, url, searchFields, selection_callback, result_callback ) {
        $(element).select2({
            ajax: {
                url: url,
                dataType: 'json',
                allowClear: true,
                data: function (params) {
                    return {
                            search: params.term, // search term
                            searchFields: searchFields, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.data
                        };
                    },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            templateSelection: selection_callback,
            templateResult: result_callback
        });
    },
    /**
     * generate lookup values to select2 component
     * @param  element element
     * @param  string type
     * @return
     */
    LookupCodeSelect2 : function( element, type ) {
        $url = base_url+'/administrator/lookup/type/'+type;
        this.generateSelect2(element, $url, 'lookup_code:like', this.LookupCodeTemplate, this.LookupCodeFormat);
    },
    /**
     * Set return data and set meaning label
     * @param  array data
     * @param  html container
     * @return string
     */
    LookupCodeTemplate : function(data, container) {
        if( container !== undefined ) {
            var description_container = container.prevObject.parent().parent().parent().parent().next();
            // check if label exist as description/meaning container
            if( description_container.find('.label').length > 0 ) {
                description_container.find('.label').html(data.description);
                description_container.find('input[type="hidden"]').val(data.meaning);
            }
            // if label does not exist, use the next attribute as description/meaning container
            else {
                description_container = container.prevObject.parent().parent().parent().next();
                description_container.html(data.meaning);
            }
        }
        return data.text;
    },

    /**
     * Format the dropdown result of lookup data
     * @param  array data
     * @return string|html
     */
    LookupCodeFormat : function (data) {
        if (data.loading) return data.text;
        var markup = '';

        // markup += '<dt>'+data.id+'</dt>';
        // markup += '<dd><strong>Meaning:</strong> '+data.meaning+'</dd>';
        // markup += '<dd><strong>Description:</strong> '+data.description+'</dd>';
        // markup += '</dl>';

        markup = data.id+' | '+data.meaning;

        return markup;
    },

    GenerateDatatables : function(resource, columns, searchable) {
        return true;
    },

    getAddInfoForm: function(id, table) {
        return $.ajax({
            url: base_url+'/additional_information/table/'+id+'/'+table,
            method: "GET",
            data: { _token : _token }, // serializes all the form data
            beforeSend : function() {
                toggle_loading();
            }
        });
    },

    setAddInfoForm: function(modal, url, method) {
        var _method = typeof method == undefined ? 'post' : method;

        modal.find('form').attr('action', url);
        modal.find('[name="_method"]').val(_method);
    },

    transformSelect2Data: function(data, id, name) {
        var ret = $.map(data, function (obj) {
            obj.id = obj.id || obj[id];

            return obj;
        });
        ret = $.map(ret, function (obj) {
                obj.text = obj.text || obj[name];

                return obj;
        });

        return ret;
    }
};
