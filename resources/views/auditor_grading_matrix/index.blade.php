@extends('layouts.app')
@section('styles')
    
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')
        <div class="page-content">
            @include('includes.content_header')
            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" id="show-auditor-grading-matrix-modal">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Auditor Grading Matrix
                    </div>
                    <table id="grading-matrix-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Variance From</th>
                                <th>Variance To</th>
                                <th>Timeliness Max</th>
                                <th>Quality Max</th>
                                <th>Documentation Max</th>
                                <th>Remarks</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@include('auditor_grading_matrix.includes.auditor_grading_matrix_modal')
@endsection
@section('footer_script')
<script type="text/javascript">
    var grading_matrix_datatable = $('#grading-matrix-table').DataTable( {
        ajax: "{{route('auditor_grading_matrix.list')}}",
        "processing": true,
        "serverSide": true,
        orderCellsTop: true,
        "searching": false,
        columns: [
            { data: "variance_from" },
            { data: "variance_to" },
            { data: "rating" },
            { data: "quality_max" },
            { data: "documentation_max" },
            { data: "remarks", defaultContent : 'N/A', orderable: false, searchable: false },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="edit" title="Edit" rel="tooltip"><i class="fa fa-pencil"></i></a> &nbsp; <a href="{{ url('auditor-grading-matrix')}}/'+data.id+'" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    $("#show-auditor-grading-matrix-modal").click(function(){
        $("#auditor-grading-matrix-form").attr('action',"{{route('auditor_grading_matrix.store')}}");
        $("#auditor-grading-matrix-form").attr('method', "POST" );
        $('#auditor-grading-matrix-modal').modal('show');
        return false;
    })

    $(document).on("click","#grading-matrix-table .delete",function(){
        $(this).deleteItemFrom(grading_matrix_datatable);
        return false;
    });

    $(document).on("click","#grading-matrix-table .edit",function(){
        var row = $(this).parent().parent();
        var data = grading_matrix_datatable.row( row ).data();
        $("#auditor-grading-matrix-form").attr('action',"{{ url('auditor-grading-matrix')}}/" + data.id)
                              .attr('method', "PUT" )
                              .supply(data);
        $("#auditor-grading-matrix-modal").modal('show');
        return false;
    });

</script>
@yield('auditor_grading_matrix_modal_scripts')
@endsection
