<!-- Modal form -->
<div class="modal fade" data-backdrop="static" id="auditor-grading-matrix-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Grading Matrix Form</h4>
            </div>
            <div class="modal-body">
                <div id="auditor-grading-matrix-alert"></div>
                <form class="form-horizontal" id="auditor-grading-matrix-form" >
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="variance_from"> Variance From </label>
                        <div class="col-sm-3">
                            <input type="number" name="variance_from" class="form-control input-sm basic-info" value="">
                        </div>

                        <label class="col-sm-2 control-label" for="variance_to"> Variance To </label>
                        <div class="col-sm-3">
                            <input type="number" name="variance_to" class="form-control input-sm basic-info" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="rating"> Timeliness Max </label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control input-sm" name="rating" step=".25">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="quality_max"> Quality Max </label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control input-sm" name="quality_max" step=".5">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="documentation_max"> Documentation Max </label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control input-sm" name="documentation_max" step=".5">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="remarks"> Remarks </label>
                        <div class="col-sm-9">
                            <textarea class="form-control input-sm noresize" name="remarks" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="form-row align-right" >
                        <button class="btn btn-info btn-sm">
                            <i class="ace-icon fa fa-save bigger-120"></i>
                            Apply
                        </button>
                        <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Modal form -->
@section('auditor_grading_matrix_modal_scripts')
<script type="text/javascript">
    $("#auditor-grading-matrix-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#auditor-grading-matrix-form")[0].reset();
                $("#auditor-grading-matrix-modal").modal('hide');
                grading_matrix_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#auditor-grading-matrix-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });
</script>
@endsection