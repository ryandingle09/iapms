<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>IAPMS | Vue</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="/css/bootstrap.css" />
    <link rel="stylesheet" href="/css/font-awesome.css" />
    <link rel="stylesheet" href="/css/jquery-ui.custom.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <script type="text/javascript">
        var base_url = '{{url('/')}}';
        var _token = '{{csrf_token()}}';
    </script>
</head>
<body>
    @yield('content')
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/js/vue/vue.js"></script>
    <script src="/js/vue/vue-resource.min.js"></script>
    <script src="/js/planning.vue.js"></script>
</body>
</html>
