<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>IAPMS | {{ ucwords( str_replace(['_', '-'], ' ', Request::segment(1)) ) }}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="/css/bootstrap.css" />
    <link rel="stylesheet" href="/css/font-awesome.css" />
    <link rel="stylesheet" href="/css/jquery-ui.custom.css" />
    <link rel="stylesheet" href="/css/dropzone.css" />

    <!-- text fonts -->
    <link rel="stylesheet" href="/css/ace-fonts.css" />

    <!-- ace styles -->
    <link rel="stylesheet" href="/css/ace.css" />

    <!--[if lte IE 9]>
        <link rel="stylesheet" href="/css/ace-part2.css" />
    <![endif]-->
    <link rel="stylesheet" href="/css/ace-rtl.css" />

    <!--[if lte IE 9]>
      <link rel="stylesheet" href="/css/ace-ie.css" />
    <![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    <script src="/js/html5shiv.js"></script>
    <script src="/js/respond.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="/css/override.css" />

    <style type="text/css">
        .mask{
            display: none; /*This hides the mask*/
        }

        .mask .loading {
            font-size: 40px;
            color: #ffffff;
            margin: auto 0;
            text-align: center;
            position: fixed;
            top: 40%;
            left: 37%;
            background-color: transparent;
        }

        .ajax{
            display: block !important;
            width: 100%;
            height: 100%;
            position: absolute; /*required for z-index*/
            z-index: 1000; /*puts on top of everything*/
            background-color: #000;
            opacity: 0.7;
        }
    </style>
    <script type="text/javascript">var base_url = '{{url('/')}}';</script>
    <!-- basic scripts -->
        <!--[if !IE]> -->
        <script type="text/javascript">
            window.jQuery || document.write("<script src='/js/jquery.js'>"+"<"+"/script>");
        </script>

        <!-- <![endif]-->

        <!--[if IE]>
    <script type="text/javascript">
     window.jQuery || document.write("<script src='/js/jquery1x.js'>"+"<"+"/script>");
    </script>
    <![endif]-->
</head>
<body class="no-skin">
    <div class="main-content">
        <div class="main-content-inner">
            <div class="page-content">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->
                        <div>
                            <form action="{{route('migration.execute')}}" method="post" enctype="multipart/formdata"  class="dropzone" id="dropzone">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group">
                                    <div class="fallback">
                                        <input name="file" type="file" multiple="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-primary btn-upload">Upload</button>
                                </div>
                            </form>
                        </div><!-- PAGE CONTENT ENDS -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    <div class="footer">
        <div class="footer-inner">
            <!-- #section:basics/footer -->
            <div class="footer-content">
                <span class="bigger-120">
                    <span class="blue bolder">SM Retail</span>
                    IAPMS
                </span>
            </div>

            <!-- /section:basics/footer -->
        </div>
    </div>

    <!-- to up -->
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>

    <script src="/js/bootstrap.js"></script>

    <!-- page specific plugin scripts -->
    <script src="/js/dropzone.js"></script>

    <!--[if lte IE 8]>
      <script src="/js/excanvas.js"></script>
    <![endif]-->

    <!-- ace scripts -->
    <script src="/js/ace/ace.js"></script>
    <script src="/js/ace/ace.ajax-content.js"></script>
    <script src="/js/ace/ace.sidebar.js"></script>
    <script src="/js/ace/ace.sidebar-scroll-1.js"></script>
    <script src="/js/ace/ace.submenu-hover.js"></script>

    <!-- inline scripts related to this page -->
    <script type="text/javascript">
        jQuery(function($){
            try {
                Dropzone.autoDiscover = false;
                var myDropzone = new Dropzone("#dropzone" , {
                    paramName: "file", // The name that will be used to transfer the file
                    // maxFilesize: 0.5, // MB
                    autoProcessQueue: false,
                    acceptedFiles: '.csv',
                    addRemoveLinks : true,
                    dictDefaultMessage :
                    '<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload \
                    <span class="smaller-80 grey">(or click)</span> <br /> \
                    <i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',
                    dictResponseError: 'Error while uploading file!',

                    //change the previewTemplate to use Bootstrap progress bars
                    previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-details\">\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n    <div class=\"dz-size\" data-dz-size></div>\n    <img data-dz-thumbnail />\n  </div>\n  <div class=\"progress progress-small progress-striped active\"><div class=\"progress-bar progress-bar-success\" data-dz-uploadprogress></div></div>\n  <div class=\"dz-success-mark\"><span></span></div>\n  <div class=\"dz-error-mark\"><span></span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n</div>"
                });

                $(document).one('ajaxloadstart.page', function(e) {
                    try {
                        myDropzone.destroy();
                    } catch(e) {}
                });

                $('.btn-upload').on('click', function(){
                    myDropzone.processQueue();
                    $('.dz-remove').remove();
                });
                myDropzone.on("success", myDropzone.processQueue.bind(myDropzone));
            } catch(e) {
              alert('Dropzone.js does not support older browsers!');
            }
        });
    </script>
</body>
</html>