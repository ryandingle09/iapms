@extends('layouts.app')
@section('styles')
<style type="text/css">
    .wbs_project{
        max-height: 400px;
        overflow-x: auto;
        overflow-y: auto;
    }
</style>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')
        <div class="page-content">
            @include('includes.content_header', [ 'heading' => "Auditor Management Input"])

        	<div class="col-xm-12">
                <div class="row">
                    <div class="col-xs-12 mrgB15" id="auditor-management-tools">
                        <div class="table-header mrgB15">
                            Auditor Management Tool
                        </div>
                    </div>
                    <div class="col-xs-12 align-left">
                        <a class="btn btn-primary btn-sm " data-toggle="modal" data-target="#create-auditor-modal" >
                            <i class="ace-icon fa fa-plus bigger-120"></i>
                            Create
                        </a>
                    </div>
                    <div class="col-xs-12 mrgB15">
                        <table id="auditor-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Audit Year</th>
                                    <th>Auditor Name</th>
                                    <th>Template Name</th>
                                    <th class="text-center" width="10%">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
        	</div>

            <div class="col-xm-12">
                <div class="row" id="wbs-management">

                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-xs-12 mrgB15">
                                <div class="table-header mrgB15">
                                    Work Breakdown Structure
                                </div>
                            </div>
                            <div class="col-xs-12 align-left">
                                <a class="btn btn-primary btn-sm create-project-btn" data-toggle="modal" data-target="#create-project-modal" >
                                    <i class="ace-icon fa fa-plus bigger-120"></i>
                                    Create
                                </a>
                            </div>
                            <div class="col-xs-12 mrgB15">
                            </div>
                            <div class="col-xs-12 mrgB15 wbs_project">
                                <!-- Projects and scopes will be loaded here serverside -->
                                <h3 class="text-center" class="page-header">No Projects Found.</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-xs-12 mrgB15" id="plan-details">
                                <div class="table-header mrgB15">
                                    Gantt Chart
                                </div>
                            </div>

                            <div class="col-xs-12 mrgB15 wbs_gunttchart">
                            </div>
                        </div>
                    </div>

                </div>
            </div>

		</div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@include('auditor_management.management_input.includes.auditor_modal')
@include('auditor_management.management_input.includes.project_modal')
@include('auditor_management.management_input.includes.project_scope_modal') 
@endsection

@section('footer_script')

<script src="/js/date-time/bootstrap-datepicker.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script src="/plugins/tinymce/tinymce.min.js"></script>
<script src="/plugins/tinymce/jquery.tinymce.min.js"></script>

<script type="text/javascript">
    var selected_auditor    = '';
    var selected_year       = '{{ date('Y') }}';
    var selected_project    = '';
    var pmMinDate           = new Date(selected_year,0,01);
    var pmMaxDate           = new Date(selected_year,11,31);

    if(selected_auditor == '') $('.create-project-btn').attr("disabled", "disabled");

    var project_auditor_datatable = $('#auditor-table').DataTable({
            bDestroy: true,
            ajax: "{{ route('auditor_management_input.list')}}",
            processing: true,
            order: [[ 1, 'asc' ]],
            columns: [
                { data: "audit_year" },
                { data: "auditor.full_name" },
                { data: "template_name" },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        var btn = '<a href="{{ url('auditor-management')}}/management-input/'+data.wbs_header_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                        return btn;
                    },
                    orderable: false,
                    searchable: false,
                    className: "text-center"
                }
            ]
        });

    $(document).on('click','#auditor-table tbody tr',function(){
        $(".selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        var data = project_auditor_datatable.row( $(this) ).data();
        selected_auditor = data.wbs_header_id;
        selected_year = data.audit_year;
        pmMinDate       = new Date(selected_year,0,01);
        pmMaxDate       = new Date(selected_year,11,31);

        $('.pm-datepicker').datepicker({
            format: "M-dd-yyyy",
            startDate : pmMinDate,
            endDate : pmMaxDate,
            keyboardNavigation: false,
            autoclose : true
        });

        $.ajax({
            url : "{{url('auditor-management')}}/management-input/"+selected_auditor+"/project/list",
            method : 'GET',
            data : $(this).serialize(),
            beforeSend : function(){
                //sLoading()
                var loader = '<div class="text-center"><img src="{{ url('images/loading_bar.gif') }}" width="200px"></div>';
                    loader += '<h4 class="text-center page-header">Loading</h4>';
                $('.wbs_project').html(loader);
            },
            success : function(response){
                $('.wbs_project').html(response);
                $('.create-project-btn').removeAttr("disabled");
                swal.close();
            },
            error : function(xhr){
                $("#create-auditor-form").modalFormErrorHandler(xhr);
            }
        }); 
    });

    $(document).on("click","#auditor-table .delete",function(){
        $(this).deleteItemFrom(project_auditor_datatable);
        return false;
    });

    $(document).on("click",".create-scope-modal",function(){
        var dis                 = $(this)
        var target              = dis.data('modal');
        var selected_project    = dis.data('projectId');
        $(target).modal('show');

        cpsm_entity_select.clear();
        cpsm_mbp_select.clear();
        cpsm_mbp_select.disable();
        $("#create-project-scope-form")[0].reset();
        $("#create-project-scope-form").clearForm();
        $("#create-project-scope-form").attr('action',"{{ url('auditor-management/management-input/')}}/"+selected_project+"/scope").attr('method','POST');
        $("#create-project-scope-modal").modal('show');
        return false;
    });

    $(document).on("click","#accordion .delete",function(){
        var url = $(this).attr("href");
        $.sWarning("Are you sure you want to delete this item?",function(){
            $.ajax({
                url : url,
                method : "delete",
                success : function(response){
                    $.sSuccess(response.message);
                    $('#auditor-table tbody tr.selected-row').trigger('click');
                }
            });
        });
        return false;
    });

</script>

@yield('auditor_modal_scripts')
@yield('project_modal_scripts')
@yield('project_scope_modal_scripts')
@endsection