@if($projects)
@if(sizeof($projects) == 0)
<h3 class="text-center page-header">No Projects Found.</h3>
@else
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	@foreach($projects as $key => $project)
    <div class="widget-box" role="tab" id="heading_{{$key}}">
        <div class="widget-header">
            <h4 class="widget-title">
                <a {{ ($key == 0) ? 'class="collapsed"' : '' }} role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{$key}}" aria-expanded="{{ ($key == 0) ? 'true' : 'false' }}" aria-controls="collapse_{{$key}}">
                    {{ $project->project_name }}
                </a>
            </h4>
        </div>
        <div id="collapse_{{$key}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_{{$key}}">
            <div class="panel-body">
                <!-- {{ json_encode($projects) }} -->
                <!-- Table Projects -->
                <table class="table table-bordered table-responsive project-table">
                    <thead>
                        <tr>
                            <td>Project Name</td>
                            <td>Project Head</td>
                            <td>Audit Type</td>
                            <td>Status</td>
                            <td>Budgeted Mandays</td>
                            <td>Start Date</td>
                            <td>End Date</td>
                            <td>%</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $project->project_name }}</td>
                            <td>{{ $project->auditor['full_name'] }}</td>
                            <td>{{ $project->audit_type }}</td>
                            <td>{{ $project->status }}</td>
                            <td>100</td>
                            <td>{{ ymd($project->start_date) }}</td>
                            <td>{{ ymd($project->end_date) }}</td>
                            <td>%1000</td>
                            <td class="text-center" style="width: 10%">
                                <a href="{{ url('auditor-management')}}/management-input/{{ $project->wbs_project_id }}/project" data-modal="#view-project-modal"  data-method="get" data-type="view" class="action-project"><i class="fa fa-eye text-info" title="View Details" rel="tooltip"></i></a>
                                <a href="{{ url('auditor-management')}}/management-input/{{ $project->wbs_project_id }}/project" data-modal="#edit-project-modal"  data-method="get" data-type="edit" class="action-project"><i class="fa fa-edit text-success" title="Edit" rel="tooltip"></i></a>
                                <a href="{{ url('auditor-management')}}/management-input/{{ $project->wbs_project_id }}/project" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="col-xs-12 align-left">
                    <a class="btn btn-primary btn-sm create-scope-btn create-scope-modal" data-projectId="{{ $project->wbs_project_id }}" data-modal="#create-project-scope-modal" >
                        <i class="ace-icon fa fa-plus bigger-120"></i>
                        Create
                    </a>
                </div>
                <!-- Table Scopes -->
                @if(sizeof($project->scope) == 0)
                    <h4 class="text-center page-header">No Project Scopes Found.</h4>
                @else
                <table class="table table-bordered table-responsive scope-table">
                    <thead>
                        <tr>
                            <td>Seq</td>
                            <td>IOM Number</td>
                            <td>Auditable Entity IDL</td>
                            <td>Main Business Process</td>
                            <td>Status</td>
                            <td>Budgeted Mandays</td>
                            <td>Start Date</td>
                            <td>End Date</td>
                            <td>%</td>
                            <td>TM</td>
                            <td>Pre</td>
                            <td class="text-center">Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($project->scope as $key => $scope)
                        <tr>
                            <td>{{ $scope['wbs_sequence'] }}</td>
                            <td>{{ $scope['wbs_iom_number'] }}</td>
                            <td>{{ $scope['auditable_entity_id'] }}</td>
                            <td>{{ $scope['ae_mbp_id'] }}</td>
                            <td>{{ $scope['status'] }}</td>
                            <td>{{ $scope['budgeted_mandays'] }}</td>
                            <td>{{ ymd($scope['start_date']) }}</td>
                            <td>{{ ymd($scope['end_date']) }}</td>
                            <td>%</td>
                            <td>{{ $scope['task_mod'] }}</td>
                            <td>{{ $scope['predecessor'] }}</td>
                            <td class="text-center" style="width: 10%">
                                <a href="{{ url('auditor-management')}}/management-input/{{ $scope->wbs_project_scope_id }}/scope" data-auditType="{{ $project->audit_type }}" data-method="get" data-type="view" data-modal="#view-scope-modal" class="action-scope"><i class="fa fa-eye text-info" title="View Details" rel="tooltip"></i></a>
                                <a href="{{ url('auditor-management')}}/management-input/{{ $scope->wbs_project_scope_id }}/scope" data-auditType="{{ $project->audit_type }}" data-method="put" data-type="edit" data-modal="#edit-scope-modal" class="action-scope"><i class="fa fa-edit text-success" title="Edit" rel="tooltip"></i></a>
                                <a href="{{ url('auditor-management')}}/management-input/{{ $scope->wbs_project_scope_id }}/scope" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                @endif
            </div>
        </div>
    </div>
    @endforeach
</div>
@endif
@endif