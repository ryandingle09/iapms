<!-- Create Project Modal -->
<div class="modal fade " data-backdrop="static" id="create-project-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Project</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-project-alert"></div>
                        <form class="form-horizontal" id="create-project-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="project_name"> Project Name </label>
                                <div class="col-sm-10">
                                    <input type="text" name="project_name" id="project_name" class="form-control input-sm" value="">
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="project_head"> Project Head </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm project_head" placeholder="Select Project Head" name="project_head"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_type"> Audit Type </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm makelabel audit_type" placeholder="Select Audit Type" name="audit_type"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="status"> Status </label>
                                <div class="col-sm-10">
                                    <select name="status" class="form-control input-sm status">
                                        <option value="active">Active</option>
                                        <option value="disabled">Disabled</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-10">
                                    <div class="input-daterange input-group">
                                        <input type="text" name="start_date" class="form-control input-sm form-control pm-datepicker" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" name="end_date" class="form-control input-sm form-control pm-datepicker" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Project Modal -->

<!-- Edit Project Modal -->
<div class="modal fade " data-backdrop="static" id="edit-project-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Project</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div id="edit-project-alert"></div>
                        <form class="form-horizontal" id="edit-project-form" action="" method="PUT">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="project_name"> Project Name </label>
                                <div class="col-sm-10">
                                    <input type="text" name="project_name" id="project_name" class="form-control input-sm" value="">
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="project_head"> Project Head </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm project_head" placeholder="Select Project Head" name="project_head"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_type"> Audit Type </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm makelabel audit_type" placeholder="Select Audit Type" name="audit_type"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="status"> Status </label>
                                <div class="col-sm-10">
                                    <select name="status" class="form-control input-sm status">
                                        <option value="active">Active</option>
                                        <option value="disabled">Disabled</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-10">
                                    <div class="input-daterange input-group">
                                        <input type="text" name="start_date" class="form-control input-sm form-control pm-datepicker" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" name="end_date" class="form-control input-sm form-control pm-datepicker" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Project Modal -->

<!-- View Project Modal -->
<div class="modal fade " data-backdrop="static" id="view-project-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Project Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div id="edit-project-alert"></div>
                        <form class="form-horizontal" id="view-project-form" action="" method="PUT">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left" for="project_name"> Project Name </label>
                                <div class="col-sm-10">
                                    <input type="text" name="project_name" id="project_name" class="form-control input-sm makelabel">
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="project_head"> Project Head </label>
                                <div class="col-sm-10">
                                    <input type="text" name="project_head" id="project_head" class="form-control input-sm makelabel">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_type"> Audit Type </label>
                                <div class="col-sm-10">
                                    <input type="text" name="audit_type" id="audit_type" class="form-control input-sm makelabel">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="status"> Status </label>
                                <div class="col-sm-10">
                                    <input type="text" name="status" id="status" class="form-control input-sm makelabel" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="start_date"> Start Date </label>
                                <div class="col-sm-10">
                                    <div class="input-daterange input-group">
                                        <input type="text" name="start_date" class="form-control input-sm form-control pm-datepicker makelabel" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="end_date"> End Date </label>
                                <div class="col-sm-10">
                                    <div class="input-daterange input-group">
                                        <input type="text" name="end_date" class="form-control input-sm form-control pm-datepicker makelabel" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Close
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Project Modal -->

@section('project_modal_scripts')
<script type="text/javascript">

    // init selectize in status select
    var status_type_select = $(".status").makeSelectize()[0].selectize;

    //audit type
    var cpm_audit_type_select = $(".audit_type").makeSelectize({
        lookup : '{{ config('iapms.lookups.audit_type') }}'
    })[0].selectize;

    // project head
    var pam_auditor_select = $(".project_head").makeSelectize({
        url: "{{ url('auditor/show')}}",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    })[0].selectize;

    $('.pm-datepicker').datepicker({
        format: "M-dd-yyyy",
        startDate : pmMinDate,
        endDate : pmMaxDate,
        keyboardNavigation: false,
        autoclose : true
    });

    $(document).on("click","#create-project-form", function(){
        $(this).find('.has-error').removeClass('has-error');
        $.ajax({
            url : "{{ url('auditor-management/management-input') }}/"+selected_auditor+"/project",
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                cpm_audit_type_select.clear();
                $("#create-project-form")[0].reset();
                $("#create-project-modal").modal('hide');
                $.sSuccess(response.message);
                $('#auditor-table tbody tr.selected-row').trigger('click');
            },
            error : function(xhr){
                $("#create-project-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on("click",".action-project", function(e){
        e.preventDefault();

        var dis     = $(this)
        var method  = dis.data('method');
        var url     = dis.attr('href');
        var type    = dis.data('type');
        var target  = dis.data('modal'); 

        //set form action url
        $(target+' form').attr('action', url);

        $.ajax({
            url : url,
            method : method,
            beforeSend : sLoading(),
            success : function(response){
                swal.close();

                // create new instance for selectize inputs
                if(target == '#edit-project-modal'){ //if edit modal
                    var head_select = $(target+' select[name="project_head"]')[0].selectize;
                    var audit_select = $(target+' select[name="audit_type"]')[0].selectize;

                    //set value of selectize input instances
                    head_select.addItem(response[0].project_head_id, false);
                    audit_select.addItem(response[0].audit_type);
                    status_type_select.addItem(response[0].status);
                    $(target+' select[name="status"]').val(response[0].status);
                }

                $(target+' input[name="project_name"]').val(response[0].project_name);
                $(target+' input[name="status"]').val(response[0].status);
                $(target+' input[name="project_head"]').val(response[0].auditor.full_name);
                $(target+' input[name="audit_type"]').val(response[0].audit_type);
                $(target+' input[name="start_date"]').datepicker('setDate', new Date(response[0].start_date));
                $(target+' input[name="end_date"]').datepicker('setDate', new Date(response[0].end_date));

                // for view
                $("#view-project-form .makelabel").makeLabel();

                $(target).modal('show');
            }
        });
        return false;
    });

    $(document).on("submit","#edit-project-form", function(){
        $(".alerts").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#edit-project-modal").modal('hide');
                $('#auditor-table tbody tr.selected-row').trigger('click');
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#edit-project-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

</script>
@endsection