<!-- Create Projects Auditor Modal -->
<div class="modal fade " data-backdrop="static" id="create-auditor-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Auditor</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-auditor-alert"></div>
                        <form class="form-horizontal" id="create-auditor-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_year"> Year </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control input-sm audit_year" name="audit_year" />
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="auditor"> Auditor </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm auditor" placeholder="Select Auditor" name="auditor"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="template_name"> Template Name </label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control input-sm template_name" name="template_name" />
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Close
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Projects Auditor Modal -->
@section('auditor_modal_scripts')
<script type="text/javascript">
    // project_auditor
    var pam_auditor_select = $("#create-auditor-form .auditor").makeSelectize({
        url: "{{ url('auditor/show')}}",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    })[0].selectize;

    $("#create-auditor-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#create-auditor-form")[0].reset();
                $("#create-auditor-modal").modal('hide');
                pam_auditor_select.clear();
                project_auditor_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#create-auditor-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });
</script>
@endsection