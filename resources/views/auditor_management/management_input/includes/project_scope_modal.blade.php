<!-- Create Projects Scope Modal -->
<div class="modal fade " data-backdrop="static" id="create-project-scope-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Project Scope</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-project-scope-alert"></div>
                        <form class="form-horizontal" id="create-project-scope-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="auditable_entity"> Auditable Entity </label>
                                <div class="col-sm-8">
                                    <select class="form-control input-sm auditable_entity" name="auditable_entity"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="main_business_process"> Main Business Process </label>
                                <div class="col-sm-8">
                                    <select class="form-control input-sm main_business_process" name="main_business_process"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="budgeted_mandays"> Budgeted Mandays </label>
                                <div class="col-sm-8">
                                    <input type="text" name="budgeted_mandays" class="form-control input-sm budgeted_mandays">
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="predecessor"> Predecessor </label>
                                <div class="col-sm-8">
                                    <input type="text" name="predecessor" id="predecessor" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="task_mode"> Task Mode </label>
                                <div class="col-sm-8">
                                    <select name="task_mode" class="form-control input-sm task_mode">
                                        <option value="0">Automatic</option>
                                        <option value="1">Manual</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="status"> Status </label>
                                <div class="col-sm-8">
                                    <select name="status" class="form-control input-sm status">
                                        <option value="active">Active</option>
                                        <option value="disabled">Disabled</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-8">
                                    <div class="input-daterange input-group">
                                        <input type="text" name="start_date" class="form-control input-sm form-control pm-datepicker" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" name="end_date" class="form-control input-sm form-control pm-datepicker" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Projects Scope Modal -->
@section('project_scope_modal_scripts')
<script type="text/javascript">

    // init selectize in task_mode select
    var task_mode_select = $(".task_mode").makeSelectize()[0].selectize;

    var cpsm_entity_select = $("#create-project-scope-form .auditable_entity").selectize({
        valueField: 'auditable_entity_id',
        labelField: 'auditable_entity_name',
        searchField: 'auditable_entity_name',
        options: [],
        create: false,
        render: {
            option: function (item, escape) {
                return " <div>" +escape(item.auditable_entity_name) + "</div>";
            }
        },
        load: function (query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '{{ url( 'auditable_entities/lists' )}}?search=' + encodeURIComponent(query) + '&searchFields=auditable_entity_name%3Alike&entity_type=actual',
                type: 'GET',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback();
                }
            });
        },
        onChange: function(value) {
            if (!value.length) return;
            cpsm_mbp_select.clear();
            cpsm_mbp_select.disable();
            cpsm_mbp_select.load(function(callback) {
                $.ajax({
                    url: '{{ url( 'auditable_entities/master' )}}/'+value+'/business_processes/main?type=actual&json=true',
                    success: function(results) {
                        cpsm_mbp_select.enable();
                        callback(results);
                    },
                    error: function() {
                        callback();
                    }
                })
            });
        }
    })[0].selectize;

    var cpsm_mbp_select = $("#create-project-scope-form .main_business_process").selectize({
        valueField: 'ae_mbp_id',
        labelField: 'main_bp_name',
        searchField: 'main_bp_name',
        options: [],
        create: false,
        render: {
            option: function (item, escape) {
                return '<div>'+escape(item.main_bp_name) + '<div>';
            }
        }
    })[0].selectize;
    
    cpsm_mbp_select.disable();

    $('#create-project-scope-modal .input-daterange').datepicker({
        format: "dd-M-yyyy",
        startDate: 'today',
        keyboardNavigation: false,
        autoclose : true,
    });

    $("#create-project-scope-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#create-project-scope-form")[0].reset();
                $("#create-project-scope-modal").modal('hide');
                cpsm_entity_select.clear();
                cpsm_mbp_select.clear();
                project_scope_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                switch(parseInt(xhr.status)){
                    case 422:
                        swal.close();
                        alertWrapper = $("#create-project-scope-alert");
                        $("#create-project-scope-form").showError(xhr.responseJSON,alertWrapper);
                        $("#create-project-scope-modal").resetModalHeight();
                        break;
                    case 500:
                        $.sError("Error "+xhr.status+" : "+xhr.statusText);
                    break;
                }
            }
        }); 
        return false;
    });

    $(document).on("click",".action-scope", function(e){
        e.preventDefault();

        var dis     = $(this)
        var method  = dis.data('method');
        var url     = dis.attr('href');
        var type    = dis.data('type');
        var target  = dis.data('modal'); 

        //set form action url
        $(target+' form').attr('action', url);

        $.ajax({
            url : url,
            method : method,
            beforeSend : sLoading(),
            success : function(response){
                swal.close();

                // create new instance for selectize inputs
                /*if(target == '#edit-project-modal'){ //if edit modal
                    var cpsm_entity_select = $(target+' select[name="project_head"]')[0].selectize;
                    var cpsm_mbp_select = $(target+' select[name="audit_type"]')[0].selectize;

                    //set value of selectize input instances
                    cpsm_entity_select.addItem(response[0].project_head_id, false);
                    cpsm_mbp_select.addItem(response[0].audit_type);
                    status_type_select.addItem(response[0].status);
                    $(target+' select[name="status"]').val(response[0].status);
                }*/

                $(target+' input[name="project_name"]').val(response[0].project_name);
                $(target+' input[name="status"]').val(response[0].status);
                $(target+' input[name="project_head"]').val(response[0].auditor.full_name);
                $(target+' input[name="audit_type"]').val(response[0].audit_type);
                $(target+' input[name="start_date"]').datepicker('setDate', new Date(response[0].start_date));
                $(target+' input[name="end_date"]').datepicker('setDate', new Date(response[0].end_date));

                // for view
                $("#view-project-form .makelabel").makeLabel();

                $(target).modal('show');
            }
        });
        return false;
    });

    /*$("#show-create-project-scope-modal").click(function(){
        alert('clear form');
        cpsm_entity_select.clear();
        cpsm_mbp_select.clear();
        cpsm_mbp_select.disable();
        $("#create-project-scope-form")[0].reset();
        $("#create-project-scope-form").clearForm();
        $("#create-project-scope-form").attr('action',"{{ url('plan-project')}}/"+selectedProject+"/scope").attr('method','POST');
        $("#create-project-scope-modal").modal('show');
        return false;
    });*/
</script>
@endsection