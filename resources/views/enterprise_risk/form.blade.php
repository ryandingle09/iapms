@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="/plugins/selectize/selectize.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.bootstrap3.css" />
    <link rel="stylesheet" href="/css/selectize-custom.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <link rel="stylesheet" href="/css/datepicker.css" />
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Enterprise Risk Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <a href="{{route('enterprise_risk')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        @if (Session::has('message'))
                                        <div class="alert alert-success">
                                            <button type="button" class="close" data-dismiss="alert">
                                                <i class="ace-icon fa fa-times"></i>
                                            </button>

                                            <strong>
                                                <i class="ace-icon fa fa-check"></i>

                                            </strong>

                                            {!! session('message') !!}
                                            <br>
                                        </div>
                                        @endif
                                        <div class="table-header"> Auditable Entity - Business Process </div>
                                        <table class="table table-striped table-bordered table-details">
                                            <tbody>
                                                <tr>
                                                    <th width="20%">Auditable Entity</th>
                                                    <td>{{ $details->auditableEntity->auditable_entity_name }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Main Business Process</th>
                                                    <td colspan="2">{{ $details->mainBp->main_bp_name }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Business Process</th>
                                                    <td colspan="2">{{ $details->businessProcess->bp_name }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="clearfix">
                                            <a class="btn btn-primary btn-sm" data-toggle="modal" href='#enterprise-risk-modal'>
                                                <i class="ace-icon fa fa-plus"></i>
                                                Create
                                            </a>
                                        </div>
                                        <div class="table-header">
                                            Risk Details
                                        </div>
                                        <table id="risks-table" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Risk Name</th>
                                                    <th>Risk Description</th>
                                                    <th>Risk Type</th>
                                                    <th>Risk Remarks</th>
                                                    <th></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <a href="{{route('enterprise_risk')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<!-- Modal Form -->
<div class="modal fade" data-backdrop="static" id="enterprise-risk-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Enterprise Risk Details</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning alert-modal"></div>
                <form method="POST" action="{{route('enterprise_risk.details.store', ['id' => $details->enterprise_risk_id])}}" accept-charset="UTF-8" class="form-horizontal" id="enterprise-risk-form" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="post">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="risk"> Risk Title </label>
                        <div class="col-sm-8">
                            <input type="text" id="risk" name="risk" class="form-control input-sm basic-info">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="description"> Description </label>
                        <div class="col-sm-8">
                            <textarea id="description" name="description" class="form-control" rows="3" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="risk_remarks"> Risk Remarks </label>
                        <div class="col-sm-8">
                            <textarea id="risk_remarks" name="risk_remarks" class="form-control" rows="3" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="risk_type"> Risk Type </label>
                        <div class="col-sm-8">
                            <select id="risk_type" name="risk_type" class="form-control input-sm basic-info" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="risk_class"> Risk Class </label>
                        <div class="col-sm-8">
                            <select id="risk_class" name="risk_class" class="form-control input-sm basic-info" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="risk_area"> Risk Area </label>
                        <div class="col-sm-8">
                            <select id="risk_area" name="risk_area" class="form-control input-sm basic-info" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="response_type"> Response Type </label>
                        <div class="col-sm-8">
                            <select id="response_type" name="response_type" class="form-control input-sm basic-info" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="response_status"> Response Status </label>
                        <div class="col-sm-8">
                            <select id="response_status" name="response_status" class="form-control input-sm basic-info" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="response_status_comment"> Response Status Comment </label>
                        <div class="col-sm-8 desc-wrapper">
                            <textarea id="response_status_comment" name="response_status_comment" class="form-control" rows="3" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="form-group" style="display: none;">
                        <label class="label"> &nbsp; </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for=""> &nbsp; </label>
                        <div class="col-sm-4"  style="text-align: center;">
                            <span class="label label-info arrowed-in arrowed-in-right">Impact</span>
                        </div>
                        <div class="col-sm-4"  style="text-align: center;">
                            <span class="label label-info arrowed-in arrowed-in-right">Likelihood</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inherent_impact_rate"> Inherent Rating </label>
                        <div class="col-sm-4">
                            <input type="hidden" id="inherent_impact_rate" name="inherent_impact_rate" value="">
                            <div class="rating" data-id="inherent_impact_rate"></div>
                        </div>
                        <div class="col-sm-4">
                            <input type="hidden" id="inherent_likelihood_rate" name="inherent_likelihood_rate" value="">
                            <div class="rating" data-id="inherent_likelihood_rate"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="residual_impact_rate"> Residual Rating </label>
                        <div class="col-sm-4">
                            <input type="hidden" id="residual_impact_rate" name="residual_impact_rate" value="">
                            <div class="rating" data-id="residual_impact_rate"></div>
                        </div>
                        <div class="col-sm-4">
                            <input type="hidden" id="residual_likelihood_rate" name="residual_likelihood_rate" value="">
                            <div class="rating" data-id="residual_likelihood_rate"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="rating_comment"> Rating Remarks </label>
                        <div class="col-sm-8">
                            <textarea id="rating_comment" name="rating_comment" class="form-control" rows="3" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="rated_by"> Rated by </label>
                        <div class="col-sm-8">
                            <input type="text" id="rated_by" name="rated_by" class="form-control input-sm basic-info" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="rater_company"> Rater Company </label>
                        <div class="col-sm-8">
                            <input type="text" id="rater_company" name="rater_company" class="form-control input-sm basic-info" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="rater_department"> Rater Department </label>
                        <div class="col-sm-8">
                            <input type="text" id="rater_department" name="rater_department" class="form-control input-sm basic-info" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="lv_position_code_desc"> Rater Position </label>
                        <div class="col-sm-8">
                            <input type="text" id="lv_position_code_desc" name="lv_position_code_desc" class="form-control input-sm basic-info" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="rated_date"> Rated Date </label>
                        <div class="col-sm-4">
                            <input type="text" id="rated_date" name="rated_date" class="form-control input-sm basic-info input-date">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-11">
                            <input type="file" id="supporting_docs" name="supporting_docs">
                            <input type="hidden" name="supporting_docs_delete" value="">
                        </div>
                    </div>
                    <div id="download-docs" class="form-group" style="display: none;">
                        <label class="col-sm-3 control-label" for="">&nbsp;</label>
                        <div class="col-sm-9">
                            <a href="#" class="btn btn-info btn-sm"><i class="fa fa-download"></i> Download supporting document</a>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Modal Form -->
@endsection

@section('footer_script')
    <script src="/js/ace-elements.js"></script>
    <script src="/js/jquery.raty.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/plugins/selectize/selectize.js"></script>
    <script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
    <script src="/js/date-time/bootstrap-datepicker.js"></script>

    <script type="text/javascript">
        var form        = $('#enterprise-risk-form'),
            $alert      = $('.alert-modal'),
            $modal      = $('#enterprise-risk-modal'),
            risk_type   = $('#risk_type'),
            risk_class   = $('#risk_class'),
            risk_area   = $('#risk_area'),
            response_status = $('#response_status'),
            response_type = $('#response_type');
        var xhr,
            select_risk_type, $select_risk_type,
            select_risk_class, $select_risk_class,
            select_risk_area, $select_risk_area,
            select_response_type, $select_response_type,
            select_response_status, $select_response_status;
        var lookup_url = '{{url('/administrator/lookup/type')}}';
        var datatable= $('#risks-table').DataTable( {
            ajax: "{{route('enterprise_risk.details.list', ['id' => $details->enterprise_risk_id])}}",
            processing: true,
            serverSide: true,
            orderCellsTop: true,
            columns: [
                { data: "risk_name" },
                { data: "risk_desc", searchable: false },
                { data: "risk_type", searchable: false },
                { data: "risk_remarks", searchable: false },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn-edit"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.enterprise_risk_det_id+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        } );

        $(function(){
            $('.input-date').datepicker({
                autoclose : true,
                todayHighlight : true,
                format : 'dd-M-yyyy'
            });

            // rating by raty
            $('.rating').raty({
                'cancel' : true,
                'half': false,
                'starType' : 'i',
                'starOn' : rate_class,
                'starOff' : 'star-off-png',
                hints       : ['1', '2', '3', '4', '5'],
                'mouseover': function(e) {
                    setRatingColors.call(this, e, true);
                },
                'mouseout': function(e) {
                    setRatingColors.call(this, e, false);
                },
                click: function(score, evt) {
                    var id = $(this).attr('data-id');
                    $(this).raty('set', {starOn : 'star-on-png '+rates[score]});
                    $('#'+id).val(score);
                }
            });
            $('[data-id="inherent_impact_rate"]').raty('score', $('#inherent_impact_rate').val());
            $('[data-id="inherent_likelihood_rate"]').raty('score', $('#inherent_likelihood_rate').val());
            $('[data-id="residual_impact_rate"]').raty('score', $('#residual_impact_rate').val());
            $('[data-id="residual_likelihood_rate"]').raty('score', $('#residual_likelihood_rate').val());
            $('.rating').find('i').attr('rel', 'tooltip');
            // selectize widget initialization start
            $select_risk_type = risk_type.selectize({
                valueField: 'text',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="department_code">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.risk.ent_type') }}',
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(res) {
                            callback(res.data);
                        }
                    });
                }
            });
            select_risk_type = $select_risk_type[0].selectize;

            $select_risk_class = risk_class.selectize({
                valueField: 'text',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="department_code">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.risk.ent_class') }}',
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(res) {
                            callback(res.data);
                        }
                    });
                }
            });
            select_risk_class = $select_risk_class[0].selectize;

            $select_risk_area = risk_area.selectize({
                valueField: 'text',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="department_code">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.risk.ent_area') }}',
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(res) {
                            callback(res.data);
                        }
                    });
                }
            });
            select_risk_area = $select_risk_area[0].selectize;

            $select_response_type = response_type.selectize({
                valueField: 'text',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="department_code">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.risk.response') }}',
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(res) {
                            callback(res.data);
                        }
                    });
                }
            });
            select_response_type = $select_response_type[0].selectize;

            $select_response_status = response_status.selectize({
                valueField: 'text',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="department_code">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.risk.response_status') }}',
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(res) {
                            callback(res.data);
                        }
                    });
                }
            });
            select_response_status = $select_response_status[0].selectize;

            $('#supporting_docs').ace_file_input({
                no_file:'No supporting document',
                btn_choose:'Choose',
                btn_change:'Change',
                droppable:false,
                onchange:null,
                before_remove: function(){
                    var ans = confirm("Delete supporting document?");
                    if( ans ) {
                        $('input[name="supporting_docs_delete"]').val('1');
                        return true;
                    }

                    return false;
                },
                thumbnail:true //| true | large
            });
        });

        // stops the page from reloading/closing if the form is edited
        $('input').change(function() {
            if( $(this).val() != "" ) {
                window.onbeforeunload = function() {
                    return 'Are you sure you want to navigate away from this page?';
                };
            }
        });

        // save the form through AJAX call
        $(document).on('click', '.btn-save', function() {
            swal({
                title: "Continue?",
                text: "",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, save it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: form.attr('action'), // url based on the form action
                    method: "POST",
                    headers : {
                        'X-CSRF-Token' : '{{csrf_token()}}'
                    },
                    data: new FormData($("#enterprise-risk-form")[0]), // serializes all the form data
                    processData: false,
                    contentType: false,
                    success : function(data) {
                        if( data.success ) {
                            swal("Success!", "Risk details has been created.", "success");
                            window.onbeforeunload = null;
                            location.reload();
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal.close();
                        if( xhr.status == 500 ) {
                            $alert.removeClass('alert-warning').addClass('alert-danger');
                            $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                        }
                        else {
                            // displays the validation error
                            var unique_message = [];
                            var msg = '<ul class="list-unstyled">';

                            $('.basic-info').parent().parent().removeClass('has-error');

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    // set error class to fields
                                    $('#'+key).parent().parent().addClass('has-error');

                                    // shows error message
                                    if( unique_message.indexOf(val[i]) === -1 ) {
                                        msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                        unique_message.push(val[i]);
                                    }
                                }
                            });
                            msg += '</ul>';

                            $alert.removeClass('alert-danger').addClass('alert-warning');
                            $alert.html(msg).show();
                        }
                    }
                });
            });
        });

        // edit
        $(document).on('click', '.btn-edit', function() {
            var row = $(this).parent().parent();
            var data = datatable.row( row ).data();
            var action = '{{url('enterprise_risk/details')}}/'+data.enterprise_risk_det_id+'/update';

            form.find('#risk').val(data.risk_name);
            form.find('#description').val(data.risk_desc);
            form.find('#risk_remarks').val(data.risk_remarks);
            form.find('#rating_comment').val(data.rating_comment);

            select_risk_type.setValue(data.risk_type);
            select_risk_class.setValue(data.risk_class);
            select_risk_area.setValue(data.risk_area);
            select_response_status.setValue(data.response_status);
            select_response_type.setValue(data.response_type);

            $('#response_status_comment').val(data.response_status_comment);
            $('#rated_date').val(data.rated_date);
            $('#inherent_impact_rate').val(data.inherent_impact_rate);
            $('#inherent_likelihood_rate').val(data.inherent_likelihood_rate);
            $('#residual_impact_rate').val(data.residual_impact_rate);
            $('#residual_likelihood_rate').val(data.residual_likelihood_rate);
            $('[data-id="inherent_impact_rate"]').raty('score', $('#inherent_impact_rate').val());
            $('[data-id="inherent_impact_rate"]').find('.star-on-png').addClass(rates[data.inherent_impact_rate]);
            $('[data-id="inherent_likelihood_rate"]').raty('score', $('#inherent_likelihood_rate').val());
            $('[data-id="inherent_likelihood_rate"]').find('.star-on-png').addClass(rates[data.inherent_likelihood_rate]);
            $('[data-id="residual_impact_rate"]').raty('score', $('#residual_impact_rate').val());
            $('[data-id="residual_impact_rate"]').find('.star-on-png').addClass(rates[data.residual_impact_rate]);
            $('[data-id="residual_likelihood_rate"]').raty('score', $('#residual_likelihood_rate').val());
            $('[data-id="residual_likelihood_rate"]').find('.star-on-png').addClass(rates[data.residual_likelihood_rate]);

            if( data.supporting_docs_name != null ) {
                $('#supporting_docs').ace_file_input('show_file_list', [data.supporting_docs_name]);
                $('#download-docs').show()
                .find('a')
                .attr('href', '{{url('/')}}/'+data.supporting_docs_file)
                .attr('target', '_blank');
            }

            form.find('input[name="_method"]').val('put');
            form.prop('action', action);

            $modal.modal('show');

            return false;
        });

        $('.select2-drop').on('select2:select', function(evt){
            var data = evt.params.data;
            var target = evt.currentTarget.id;

            $('#'+target+'_desc').find('.label').html(data.meaning);
        });

        $modal.on('show.bs.modal', function (e) {
            if( form.find('input[name="_method"]').val() == 'post') {
                $('#rated_date').val('{{date('d-M-Y')}}');
            }
        });

        $modal.on('hidden.bs.modal', function (e) {
            var action = '{{route('enterprise_risk.details.store', ['id' => $details->enterprise_risk_id])}}';

            form.find('input, select, textarea').val('');
            $('#inherent_impact_rate').val('')
            $('#inherent_likelihood_rate').val('')
            $('#residual_impact_rate').val('')
            $('#residual_likelihood_rate').val('')
            $('[data-id="inherent_impact_rate"]').raty('score', $('#inherent_impact_rate').val());
            $('[data-id="inherent_likelihood_rate"]').raty('score', $('#inherent_likelihood_rate').val());
            $('[data-id="residual_impact_rate"]').raty('score', $('#residual_impact_rate').val());
            $('[data-id="residual_likelihood_rate"]').raty('score', $('#residual_likelihood_rate').val());

            $('#supporting_docs').ace_file_input('show_file_list', ['']);
            $('input[name="supporting_docs_delete"]').val('');
            $('#download-docs').hide()
            .find('a')
            .attr('href','#')
            .attr('target', '');

            select_risk_type.clear();
            select_risk_class.clear();
            select_risk_area.clear();
            select_response_status.clear();
            select_response_type.clear();
            form.prop('action', action);
            form.find('input[name="_method"]').val('post');
            $('.desc-wrapper span').html('');
        });

        // remove
        $(document).on('click', '.btn-delete', function() {
            var remove_id = $(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "You are about to remove a enterprise risk details.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: '{{url('enterprise_risk/details/delete')}}/'+remove_id,
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}' },
                    success : function(data) {
                        if( data.success ) {
                            swal("Deleted!", "Enterprise risk details has been deleted.", "success");
                            datatable.ajax.reload( null, false );
                            $modal.modal('hide');
                        }
                    }
                });
            });

            return false;
        });
    </script>
@endsection
