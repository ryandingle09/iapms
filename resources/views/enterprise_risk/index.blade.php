@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/plugins/selectize/selectize.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.bootstrap3.css" />
    <link rel="stylesheet" href="/css/selectize-custom.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <style type="text/css">
        .row-details {
            padding-bottom: 5px;
        }
        .selected-row {
            background-color: #ffe99e !important;
        }
        .table-hover tbody > tr:hover {
            cursor: pointer;
        }
        .dataTables_filter { display: none !important; }
    </style>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="ace-icon fa fa-times"></i>
                            </button>

                            <strong>
                                <i class="ace-icon fa fa-check"></i>

                            </strong>

                            {!! session('message') !!}
                            <br>
                        </div>
                    @endif
                    <div class="table-header">
                        Search
                    </div>
                    {!! Form::open(['class' => 'form-horizontal', 'id' => 'search-form']) !!}
                        <div class="form-group">
                            <label for="search_entity" class="col-sm-2 control-label">Auditable Entity</label>
                            <div class="col-sm-6">
                                <select class="basic-info form-control lookup filter" id="search_auditable_entity" name="search_auditable_entity" placeholder="Select a entity"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="search_bp"> Main Business Process </label>
                            <div class="col-sm-6">
                                <select class="basic-info form-control lookup" id="search_main_business_process" name="search_main_business_process" placeholder="Select a main process" required></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label" for="search_sub_bp"> Sub Business Process </label>
                            <div class="col-sm-6">
                                <select class="basic-info form-control lookup" id="search_business_process" name="search_business_process" placeholder="Select a sub process" required></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10 pull-right">
                                <button class="btn btn-primary btn-search" type="button">Go</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" data-toggle="modal" href='#ent-risk-modal'>
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Enterprise Risk
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="enterprise_risk-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Auditable Entity Name</th>
                                <th>Business Process Main</th>
                                <th>Business Process Name</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="table-header">
                        Auditable Entity Details
                    </div>
                    <table class="table table-striped table-bordered table-details">
                        <tbody>
                            <tr>
                                <th width="25%">IDL</th>
                                <td colspan="2" class="label-auditable_entity_name"></td>
                            </tr>
                            <tr>
                                <th>Company</th>
                                <td colspan="2" class="label-lv_company_code_desc"></td>
                            </tr>
                            <tr>
                                <th>Group Type</th>
                                <td colspan="2" class="label-group_type"></td>
                            </tr>
                            <tr>
                                <th>Business Type</th>
                                <td colspan="2" class="label-business_type"></td>
                            </tr>
                            <tr>
                                <th>Entity Class</th>
                                <td colspan="2" class="label-entity_class"></td>
                            </tr>
                            <tr>
                                <th>Branch Code</th>
                                <td colspan="2" class="label-branch_code"></td>
                            </tr>
                            <tr>
                                <th>Department Code</th>
                                <td colspan="2" class="label-department_code"></td>
                            </tr>
                            <tr>
                                <th>Contact Name</th>
                                <td colspan="2" class="label-contact_name"></td>
                            </tr>
                            <tr>
                                <th>Entity Head Name</th>
                                <td colspan="2" class="label-entity_head_name"></td>
                            </tr>
                        </tbody>
                    </table>
                    <div id="au-buttons" class="row row-details" style="display: none;">
                        <div class="col-sm-12" style="text-align: center;margin-top: 10px;">
                            <button type="button" class="btn btn-primary btn-add-info"><i class="fa fa-list-ul"></i> Additional Info</button>
                            <a href="#" class="btn btn-success btn-company-profile" target="_blank"><i class="fa fa-institution"></i> Company Profile</a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="table-header">
                        Main Business Process Details
                    </div>
                    <table class="table table-striped table-bordered table-details">
                        <tbody>
                            <tr>
                                <th width="23%">Name</th>
                                <td colspan="2" class="label-main_bp_name"></td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td colspan="2" class="label-lv_main_bp_name_desc"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-xs-6">
                    <div class="table-header">
                        Business Process Details
                    </div>
                    <table class="table table-striped table-bordered table-details">
                        <tbody>
                            <tr>
                                <th width="23%">Name</th>
                                <td colspan="2" class="label-bp_name"></td>
                            </tr>
                            <tr>
                                <th>Business Process</th>
                                <td colspan="2" class="label-bp_code"></td>
                            </tr>
                            <tr>
                                <th>Source Type</th>
                                <td colspan="2" class="label-source_type"></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <button type="button" class="btn btn-primary btn-add-info-bp" style="display: none;"><i class="fa fa-list-ul"></i> Additional Info</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<!-- Modal Form -->
<div class="modal fade" data-backdrop="static" id="ent-risk-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Enterprise Risk Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning alert-ra"></div>

                {!! Form::open(['class' => 'form-horizontal', 'id' => 'ent-risk-form']) !!}
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="auditable_entity"> Auditable Entity </label>
                        <div class="col-sm-6">
                            <select class="basic-info form-control lookup" id="auditable_entity" name="auditable_entity" placeholder="Select a entity" required></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="main_business_process"> Business Process Main </label>
                        <div class="col-sm-6">
                            <select class="basic-info form-control lookup" id="main_business_process" name="main_business_process" placeholder="Select a main process" required></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="business_process"> Sub Business Process </label>
                        <div class="col-sm-6">
                            <select class="basic-info form-control lookup" id="business_process" name="business_process" placeholder="Select a sub process" required></select>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Modal Form -->
@include('includes.modals.additional_information')
@endsection

@section('footer_script')
<script src="/js/iapms.js"></script>
<script src="/plugins/selectize/selectize.js"></script>
<script src="/js/jquery.validate.js"></script>
<script src="/js/date-time/bootstrap-datepicker.js"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var business_process = {};
    var $alert  = $('.alert-ra'),
        form    = $('#ent-risk-form'),
        $modal  = $('#ent-risk-modal'),
        $main_bp = $('#main_business_process'),
        $bp = $('#business_process'),
        $auditable_entity  = $('#auditable_entity');
    var ai_modal = $('#additional-information-modal');
    var xhr;
    var select_entity, $select_entity, select_filter_entity, $select_filter_entity;
    var select_main_bp, $select_main_bp, select_filter_main_bp, $select_filter_main_bp;
    var select_bp, $select_bp, select_filter_bp, $select_filter_bp;
    var datatable= $('#enterprise_risk-table').DataTable( {
        ajax: "{{route('enterprise_risk.list')}}",
        "processing": true,
        "serverSide": true,
        columns: [
            { data: "auditable_entity_name" },
            { data: "main_bp_name", defaultContent: 'N/A' },
            { data: "bp_name", defaultContent: 'N/A' },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('enterprise_risk/edit')}}/'+data.enterprise_risk_id+'"><i class="fa fa-pencil edit" title="edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.enterprise_risk_id+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    $(document).ready(function() {
        // selectize
        $select_entity = $auditable_entity.selectize({
            valueField: 'auditable_entity_id',
            labelField: 'auditable_entity_name',
            searchField: 'auditable_entity_name',
            options: [],
            preload: true,
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item['auditable_entity_name']) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            load: function (query, callback) {
                $.ajax({
                    url: '{{ route('enterprise_risk.available.entities') }}',
                    type: 'GET',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback();
                    }
                });
            },
            onChange: function(value) {
                if (!value.length) return;
                select_main_bp.clearOptions();
                select_main_bp.disable();
                select_bp.clearOptions();
                select_bp.disable();

                select_main_bp.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ url('auditable_entities/master') }}/'+value+'/business_processes/main?type=actual&json=true',
                        success: function(results) {
                            select_main_bp.enable();
                            callback(results);
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            }
        });
        select_entity = $select_entity[0].selectize;

        $select_main_bp = $main_bp.selectize({
            valueField: 'ae_mbp_id',
            labelField: 'main_bp_name',
            searchField: ['main_bp_name'],
            options: [],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.main_bp_name) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            onChange: function(value) {
                if (!value.length) return;
                var sub_bps = select_main_bp.options[value].sub_bp_details;

                select_bp.clearOptions();
                select_bp.enable();
                select_bp.addOption(sub_bps);
            }
        });
        select_main_bp = $select_main_bp[0].selectize;
        select_main_bp.disable();

        $select_bp = $bp.selectize({
            valueField: 'bp_id',
            labelField: 'bp_name',
            searchField: ['bp_name'],
            options: [],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.bp_name) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            }
        });
        select_bp = $select_bp[0].selectize;
        select_bp.disable();

        $select_filter_entity = $('#search_auditable_entity').selectize({
            valueField: 'auditable_entity_id',
            labelField: 'auditable_entity_name',
            searchField: 'auditable_entity_name',
            options: [],
            preload: true,
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item['auditable_entity_name']) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            load: function (query, callback) {
                $.ajax({
                    url: '{{ route('enterprise_risk.available.entities') }}',
                    type: 'GET',
                    success: function (res) {
                        callback(res);
                    },
                    error: function () {
                        callback();
                    }
                });
            },
            onChange: function(value) {
                if (!value.length) return;
                select_filter_main_bp.clearOptions();
                select_filter_main_bp.disable();
                select_filter_bp.clearOptions();
                select_filter_bp.disable();

                select_filter_main_bp.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ url('auditable_entities/master') }}/'+value+'/business_processes/main?type=actual&json=true',
                        success: function(results) {
                            select_filter_main_bp.enable();
                            callback(results);
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            }
        });
        select_filter_entity = $select_filter_entity[0].selectize;

        $select_filter_main_bp = $('#search_main_business_process').selectize({
            valueField: 'ae_mbp_id',
            labelField: 'main_bp_name',
            searchField: ['main_bp_name'],
            options: [],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.main_bp_name) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            onChange: function(value) {
                if (!value.length) return;
                var sub_filter_bps = select_filter_main_bp.options[value].sub_bp_details;

                select_filter_bp.clearOptions();
                select_filter_bp.enable();
                select_filter_bp.addOption(sub_filter_bps);
            }
        });
        select_filter_main_bp = $select_filter_main_bp[0].selectize;
        select_filter_main_bp.disable();

        $select_filter_bp = $('#search_business_process').selectize({
            valueField: 'bp_id',
            labelField: 'bp_name',
            searchField: ['bp_name'],
            options: [],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.bp_name) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            }
        });
        select_filter_bp = $select_filter_bp[0].selectize;
        select_filter_bp.disable();
        // end selectize

        /**
         * Set  Auditable Entity and Business Process details on designated labels
         */
        $('#enterprise_risk-table tbody').on('click', 'tr', function () {
            if( $('td', this).prop('colspan') == '1' ) {
                $('#enterprise_risk-table tbody > tr').removeClass('selected-row'); // reset tr highlights
                var data = datatable.row( this ).data();

                $('#au-buttons').show();
                $(this).addClass('selected-row');
                $('.btn-add-info').attr('data-id', data['auditable_entity_id']);
                $('.btn-add-info-bp').attr('data-id', data['bp_id']).show();
                setLabel(data);
            }
        } );
    } );

    function setLabel(data) {
        $.each(data, function(i, v){
            var label = '.label-'+i;
            if( $(label).length ) {
                if( i =='parent' && v != null ) {
                    $(label).html(v);
                }
                else {
                    var value = v != null ? v : 'N/A';
                    $(label).html(value);
                }
                if( i =='company_code' ) {
                    $('.btn-company-profile').prop('href', '{{ url('company_profile/details') }}/'+v);
                }
            }
        });
    }

    $( '#auditable_entity_name, #bp_name' ).on( 'keyup change', function () {
        var index = $(this).attr('data-index');
        if ( datatable.column(index).search() !== $(this).val() ) {
            datatable
            .column( index )
            .search( $(this).val() )
            .draw();
        }
    } );

    $modal.on('hidden.bs.modal', function(){
        form.find('select').val('');
        $alert.html('').hide();
        $('.basic-info').parent().parent().removeClass('has-error');
    });

    $(document).on('click', '.btn-save', function() {
        swal({
            title: "Continue?",
            text: "You are about to create new enterprise risk.",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        location.reload();
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }
                }
            });
        });
    });

    // remove
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        console.log(remove_id);
        swal({
            title: "Are you sure?",
            text: "You are about to remove a enterprise risk.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(){
            $.ajax({
                url: '{{url('enterprise_risk/delete')}}/'+remove_id,
                method: "POST",
                data: { _method : 'delete', _token : '{{ csrf_token() }}' },
                success : function(data) {
                    if( data.success ) {
                        swal("Deleted!", "Enterprise risk has been deleted.", "success");
                        datatable.ajax.reload( null, false );
                        $modal.modal('hide');
                    }
                }
            });
        });

        return false;
    });

    $(document).on('click', '.btn-add-info', function() {
        var id = $(this).attr('data-id');
        iapms.getAddInfoForm(id, 'auditable_entities').success(function(data) {
            if( !data.success ) {
                ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
            }
            else {
                ai_modal.find('#ai_wrapper').html(data.data);
            }
            ai_modal.find('#additional-information-form input').attr('disabled', true);
            ai_modal.find('#additional-information-form select').attr('disabled', true);
            ai_modal.find('.btn-save-ai').hide();
            ai_modal.modal('show');
            iapms.setAddInfoForm(ai_modal, '{{url('auditable_entities')}}/'+id+'/additional_information/update', 'put');

            toggle_loading();
        });

        return false;
    });

    $(document).on('click', '.btn-add-info-bp', function() {
        var id = $(this).attr('data-id');
        iapms.getAddInfoForm(id, 'business_processes').success(function(data) {
            if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                }
            ai_modal.find('#additional-information-form input').attr('disabled', true);
            ai_modal.find('#additional-information-form select').attr('disabled', true);
            ai_modal.find('.btn-save-ai').hide();
            ai_modal.modal('show');
            iapms.setAddInfoForm(ai_modal, '{{url('business_process')}}/'+id+'/additional_information/update', 'put');

            toggle_loading();
        });

        return false;
    });

    $(function(){
        ai_modal.find('form').validate();
    });

    ai_modal.find('form').on('submit', function(e){
        e.preventDefault();
        if($(this).valid()) {
            swal({
                title: "Continue?",
                text: "You are about to update additional information.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, save it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: ai_modal.find('form').attr('action'),
                    method: "POST",
                    data: ai_modal.find('form').serialize(), // serializes all the form data
                    success : function(data) {
                        if(data.success) {
                            swal("Success!", "Additional information has been updated.", "success");
                            location.reload();
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal.close();
                        // displays the validation error
                        var msg = '<ul class="list-unstyled">';

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                            }
                        });
                        msg += '</ul>';

                        ai_modal.find('.alert-modal').html(msg).show();
                    }
                });
            });
        }

        return false;
    });

    ai_modal.on('click', '.btn-save-ai', function() {
        ai_modal.find('form').submit();
    });

    $(document).on('click', '#advance_search', function () {
        if( $(this).is(':checked') ) {
            $('.basic-search').val('').attr('disabled', true);
            $('#search_advance').attr('disabled', false).focus();
        }
        else {
            $('.basic-search').attr('disabled', false).eq(0).focus();
            $('#search_advance').attr('disabled', true);
        }
    });

    $('.btn-search').on('click', function () {
        var search_data = $('#search-form').serialize();
        datatable.ajax.url( '{{route('enterprise_risk.list')}}?'+search_data ).load();
    });
</script>
@endsection
