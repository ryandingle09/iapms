@extends('layouts.app')
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">

            @include('includes.content_header', [ 'heading' => 'Risk Assessment - '.$type ])

            <div class="row">
                <div class="col-xs-12">
                    @if( $type == "Actual" )
                        @include('risk_assessment.includes.widget_filter_actual')
                    @else
                	   @include('risk_assessment.includes.widget_filter_master')
                    @endif
                	@include('risk_assessment.includes.widget_results')

                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection
@section('footer_script')
<script src="/js/iapms.js"></script>
<script type="text/javascript">
    // aea = SMCo.Pozorrubio.TREAS
	var xhr = null;
    var ras_datatable = null;
</script>
@yield('widget_filter_scripts')
@yield('widget_results_scripts')
@endsection