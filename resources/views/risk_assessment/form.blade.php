@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <style type="text/css">
        .label-default {
            width: 100%;
        }
        .row-details {
            padding-bottom: 5px;
        }
        .selected-row {
            background-color: #ffe99e !important;
        }
        .table-hover tbody > tr:hover {
            cursor: pointer;
        }
        .dt-editable {
            cursor:cell;
        }
    </style>
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Risk Assessment Scale Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <a href="{{route('risk_assessment_scale')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="table-header"> Auditable Entity - Business Process </div>
                                        <table class="table table-striped table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th width="15%">Auditable Entity</th>
                                                    <td>{{ $details->auditableEntity->auditable_entity_name }}</td>
                                                </tr>
                                                <tr>
                                                    <th>Business Process</th>
                                                    <td colspan="2">{{ $details->businessProcess->bp_name }}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="table-header"> Risk Assessment Scale </div>
                                        <table id="risk-table" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="10%">Risk Name</th>
                                                    <th width="13%">Risk Description</th>
                                                    <th width="5%">Impact</th>
                                                    <th width="5%">Likelihood</th>
                                                    <th width="5%">Risk Score</th>
                                                    <th width="5%">Risk Index</th>
                                                    <th width="5%">Severity Weight</th>
                                                    <th width="5%">Severity Value</th>
                                                    <th width="15%">Time Last Audit &amp; Audit Result %</th>
                                                    <th width="10%">Adjusted Severity</th>
                                                    <th width="15%">Remarks</th>
                                                    <th width="7%"></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="table-header"> Risk Details </div>
                                        <table class="table table-striped table-bordered table-details">
                                            <tbody>
                                                <tr>
                                                    <th width="20%">Risk Name</th>
                                                    <td colspan="2" class="label-risk_code"></td>
                                                </tr>
                                                <tr>
                                                    <th>Description</th>
                                                    <td colspan="2" class="label-lv_risk_code_desc"></td>
                                                </tr>
                                                <tr>
                                                    <th>Risk Type</th>
                                                    <td colspan="2" class="label-risk_type"></td>
                                                </tr>
                                                <tr>
                                                    <th>Risk Type Description</th>
                                                    <td colspan="2" class="label-lv_risk_type_desc"></td>
                                                </tr>
                                                <tr>
                                                    <th>Risk Remarks</th>
                                                    <td colspan="2" class="label-risk_remarks"></td>
                                                </tr>
                                                <tr>
                                                    <th>Source Type</th>
                                                    <td colspan="2" class="label-risk_source_type"></td>
                                                </tr>
                                                <tr>
                                                    <th>Source Type Description</th>
                                                    <td colspan="2" class="label-lv_source_type_desc"></td>
                                                </tr>
                                                <tr>
                                                    <th>Response Type</th>
                                                    <td colspan="2" class="label-risk_response_type"></td>
                                                </tr>
                                                <tr>
                                                    <th>Response Type Description</th>
                                                    <td colspan="2" class="label-lv_response_type_desc"></td>
                                                </tr>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th width="12%">Impact</th>
                                                    <th>Likelihood</th>
                                                </tr>
                                                <tr>
                                                    <th>Default Inherent Rating</th>
                                                    <td class="rating-row"><div class="rating label-def_inherent_impact_rate"></div></td>
                                                    <td class="rating-row"><div class="rating label-def_inherent_likelihood_rate"></div></td>
                                                </tr>
                                                <tr>
                                                    <th>Default Residual Rating</th>
                                                    <td class="rating-row"><div class="rating label-def_residual_impact_rate"></div></td>
                                                    <td class="rating-row"><div class="rating label-def_residual_likelihood_rate"></div></td>
                                                </tr>
                                                <tr>
                                                    <th>Default Rating Remarks</th>
                                                    <td colspan="2" class="label-rating_comment"></td>
                                                </tr>
                                                <tr>
                                                    <th>Enterprise Risk Code</th>
                                                    <td colspan="2" class="label-enterprise_risk_code"></td>
                                                </tr>
                                                <tr>
                                                    <th>Enterprise Risk Code Description</th>
                                                    <td colspan="2" class="label-lv_enterprise_risk_code_desc"></td>
                                                </tr>
                                                <tr>
                                                    <th>Enterprise Risk Type</th>
                                                    <td colspan="2" class="label-enterprise_risk_type"></td>
                                                </tr>
                                                <tr>
                                                    <th>Enterprise Risk Type Description</th>
                                                    <td colspan="2" class="label-lv_enterprise_risk_type_desc"></td>
                                                </tr>
                                                <tr>
                                                    <th>Enterprise Risk Class</th>
                                                    <td colspan="2" class="label-enterprise_risk_class"></td>
                                                </tr>
                                                <tr>
                                                    <th>Enterprise Risk Class Description</th>
                                                    <td colspan="2" class="label-lv_enterprise_risk_class_desc"></td>
                                                </tr>
                                                <tr>
                                                    <th>Enterprise Risk Area</th>
                                                    <td colspan="2" class="label-enterprise_risk_area"></td>
                                                </tr>
                                                <tr>
                                                    <th>Enterprise Risk Area Description</th>
                                                    <td colspan="2" class="label-lv_enterprise_risk_area_desc"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3"><button type="button" class="btn btn-primary btn-sm btn-add-info" style="display: none;"><i class="fa fa-list-ul"></i> Additional Info</button></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <a href="{{route('risk_assessment_scale')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<script type="text/template" id="rating-template">
    {!! Form::selectRange('rating', 1, 5, null, ['class' => 'form-control']) !!}
</script>
<script type="text/template" id="remarks-template">
    <textarea class="form-control" name="remarks" style="resize: none;" rows="2"></textarea>
</script>
@include('includes.modals.additional_information')
@endsection

@section('footer_script')
    <script src="/js/iapms.js"></script>
    <script src="/js/jquery.validate.js"></script>
    <script src="/js/select2.full.js"></script>
    <script src="/js/date-time/bootstrap-datepicker.js"></script>
    <script src="/js/jquery.raty.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
        var form        = $('#bp-form'),
            code        = $('#bp_code'),
            obj_type    = $('#obj_type'),
            obj_cat     = $('#obj_cat'),
            $alert      = $('.alert-main'),
            $modal      = $('#bpstep-modal');
        var ai_modal = $('#additional-information-modal');
        var impact,
            likelihood;

        var $datatable = $('#risk-table').DataTable({
            "lengthMenu": [ 50, 75, 100 ],
            "processing": true,
            ajax: "{{route('risk_assessment_scale.risk.list', ['ae_id' => $details->auditable_entity_id, 'bp_id' => $details->bp_id])}}",
            "serverSide": true,
            createdRow: function( row, data, dataIndex ) {
                var remarks = data.ra_remarks != '' ? data.ra_remarks : data.risk_remarks;

                // Set the data-status attribute, and add a class
                $( row ).find('td:eq(2)').attr('data-value', impact)
                .attr('data-type', 'impact_value');
                $( row ).find('td:eq(3)').attr('data-value', likelihood)
                .attr('data-type', 'likelihood_value');
                $( row ).find('td:eq(10)').attr('data-value', remarks)
                .attr('data-type', 'remarks');
            },
            columns: [
                { data: "risk_code" },
                { data: "lv_risk_code_desc", orderable: false },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        impact = parseInt(data.impact_value) > 0 ? data.impact_value : data.def_inherent_impact_rate; // store impact in global variable
                        return impact;
                    },
                    className: 'dt-editable',
                    searchable: false
                },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        likelihood = parseInt(data.likelihood_value) > 0 ? data.likelihood_value : data.def_inherent_likelihood_rate; // store likelihood in global variable
                        return likelihood;
                    },
                    className: 'dt-editable',
                    searchable: false},
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return parseInt(impact) * parseInt(likelihood);
                    },
                    searchable: false
                },
                {
                    data: 'matrix_value',
                    defaultContent : '-',
                    searchable: false
                },
                {
                    data: 'severity_weight',
                    defaultContent : '0',
                    searchable: false
                },
                {
                    data: null,
                    defaultContent : '0',
                    "render": function ( data, type, full, row, meta ) {
                        return data.severity_weight != null ? parseInt(data.severity_weight) * (parseInt(impact) * parseInt(likelihood)) : 0;
                    },
                    searchable: false
                },
                {
                    data: null,
                    defaultContent : '-',
                    // "render": function ( data, type, full, meta ) {
                    //     return parseInt(data.def_inherent_impact_rate) * parseInt(data.def_inherent_impact_rate);
                    // },
                    searchable: false
                },
                {
                    data: null,
                    defaultContent : '-',
                    // "render": function ( data, type, full, meta ) {
                    //     return parseInt(data.def_inherent_impact_rate) * parseInt(data.def_inherent_impact_rate);
                    // },
                    searchable: false
                },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        remarks = data.ra_remarks ? data.ra_remarks : data.risk_remarks;
                        return remarks;
                    },
                    className: 'dt-editable',
                    orderable: false,
                    searchable: false
                },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn-delete delete btn-remove" data-id="'+data.risk_id+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    orderable: false
                }
            ]
        });

        function setRaty(element, cancel, read_only, color) {
            element = element != undefined ? element : '.rating';
            cancel = cancel != undefined ? cancel : false;
            read_only = read_only != undefined ? read_only : true;
            color = color != undefined ? color : 'orange2';

            // rating by raty
            $(element).raty({
                score: function() {
                    return $(this).attr('data-rating');
                },
                readOnly: read_only,
                'cancel' : cancel,
                'half': false,
                'starType' : 'i',
                'starOn' : rate_class,
                'starOff' : 'star-off-png',
                hints       : ['1', '2', '3', '4', '5'],
                click: function(score, evt) {
                    var id = $(this).attr('data-id');
                    $(this).raty('set', {starOn : 'star-on-png '+rates[score]});
                    $('#'+id).val(score);
                },
                'mouseover': function(e) {
                    setRatingColors.call(this, e, true);
                },
                'mouseout': function(e) {
                    setRatingColors.call(this, e, false);
                },
            });
            $(element).find('i').attr('rel', 'tooltip');
        }

        $(document).ready(function() {
            setRaty();

            $('#risk-table tbody').on('click', 'td', function () {
                if( $(this).hasClass('dt-editable') && $(this).find('.form-control').length < 1 ) {
                    var td = $(this);
                    var data = $datatable.row( td.parent() ).data();
                    var data_type = td.attr('data-type');
                    var data_value = td.attr('data-value');
                    var input = data_type == 'remarks' ? $('#remarks-template').html() : $('#rating-template').html();

                    td.html('').append(input);
                    td.find('.form-control')
                    .prop('name', data_type)
                    .attr('data-orig', data_value)
                    .val(data_value)
                    .focus();

                    td.find('.form-control').on('blur', function(){
                        var orig_val = $(this).attr('data-orig');
                        var new_val = $(this).val();

                        if( orig_val != new_val ) {
                            td.html(new_val);
                            // save changes and update the value on the table
                            $.ajax({
                                url: '{{url('risk_assessment_scale/risks/update')}}/{{ $details->auditable_entity_id }}/{{ $details->bp_id }}/'+data.risk_id, // url based on the form action
                                method: "POST",
                                data: { _token : '{{csrf_token()}}', _method : 'put', data_type : data_type, data_value : new_val }, // serializes all the form data
                                beforeSend : function() {
                                    toggle_loading();
                                },
                                success : function(data) {
                                    swal("Saved!", "Risk details is successfully updated.", "success");
                                    $datatable.ajax.reload( null, false );
                                    $('.table-details td').not('.rating-row').html('');
                                    setRaty();
                                    toggle_loading();
                                }
                            });
                        }
                        else {
                            // show original value if there's no changes made
                            td.html(orig_val);
                        }
                        // remove input element
                        $(this).remove();
                    });
                }
            });

            /**
             * Set  Auditable Entity and Business Process details on designated labels
             */
            $('#risk-table tbody').on('click', 'tr', function () {
                if( $('td', this).prop('colspan') == '1' ) {
                    $('#risk-table tbody > tr').removeClass('selected-row'); // reset tr highlights
                    var data = $datatable.row( this ).data();
                    $('#au-buttons').show();
                    $(this).addClass('selected-row');
                    $('.btn-add-info').attr('data-id', data['risk_id']).show();

                    $.each(data, function(i, v){
                        var label = '.label-'+i;
                        if( $(label).length ) {
                            if( i =='parent' && v != null ) {
                                $(label).html(v);
                            }
                            else if(i.indexOf('_rate') != -1) {
                                $(label).raty('set', {score: v});
                            }
                            else {
                                var value = v != null ? v : 'N/A';
                                $(label).html(value);
                            }
                            if( i =='company_code' ) {
                                $('.btn-company-profile').prop('href', '{{ url('company_profile/details') }}/'+v);
                            }
                        }
                    });
                }
            } );
        } );

         @if (Session::has('message'))
            $alert.html('{!! session('message') !!}').show();
        @endif

        // stops the page from reloading/closing if the form is edited
        $('input').change(function() {
            if( $(this).val() != "" ) {
                window.onbeforeunload = function() {
                    return 'Are you sure you want to navigate away from this page?';
                };
            }
        });

        // save the form through AJAX call
        function save() {
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        window.onbeforeunload = null;
                        if( data.redirect !== undefined ) window.location = data.redirect;
                        else window.location = '{{ route('business_process') }}';
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        }

        // initialize modal fields
        function init_modal( sequence, activity_narrative, id ) {
            if( id != '' ) {
                // update setting
                $modal.find('input[name="id"]').val(id);
                $modal.find('form').attr('action', '{{ url('business_process/steps/update') }}/'+id)
                                      .find('input[name="_method"]').val('put');
            }
            else {
                // create setting
                $modal.find('input[name="id"]').val('');
                $modal.find('form').attr('action', '{{route('business_process.steps.store', ['bp_id' => $details->bp_id])}}')
                                      .find('input[name="_method"]').val('post');
            }
            $modal.find('input#sequence').val(sequence);
            $modal.find('#activity_narrative').val(activity_narrative);

            $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
            $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');
        }

        $modal.on('hidden.bs.modal', function (e) {
            init_modal('', '', '');
            $modal.find('.label-component').html('N/A');
            $modal.find('.label-principle').html('N/A');
            $modal.find('.label-focus').html('N/A');
            $modal.find('.modal-alert').html('').hide();
        });

        function updateDatatable() {
            $datatable.ajax.reload( null, false );
            $modal.modal('hide');
        }

        $(document).on('click', '.btn-controlsmodal-save', function(){
            $('#bpsteps-modal-form').submit();
        });

        $('#bpsteps-modal-form').on('submit', function(){
            swal({
                title: "Continue saving?",
                text: "You are about to add a new business process area.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, save it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: $('#bpsteps-modal-form').attr('action'), // url based on the form action
                    method: "POST",
                    data: $('#bpsteps-modal-form').serialize(), // serializes all the form data
                    success : function(data) {
                        if( data.success ) {
                            swal("Saved!", "New step is successfully created.", "success");
                            updateDatatable();
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal.close();
                        $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');

                        if( xhr.status == 500 ) {
                            $('.modal-alert').removeClass('alert-warning').addClass('alert-danger');
                            $('.modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                        }
                        else {
                            // displays the validation error
                            var unique_message = [];
                            var msg = '<ul class="list-unstyled">';

                            $('.modal-input').parent().parent().removeClass('has-error');

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    // set error class to fields
                                    $('#'+key).parent().parent().addClass('has-error');

                                    // shows error message
                                    if( unique_message.indexOf(val[i]) === -1 ) {
                                        msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                        unique_message.push(val[i]);
                                    }
                                }
                            });
                            msg += '</ul>';

                            $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
                            $('.modal-alert').html(msg).show();
                        }
                    }
                });
            });

            return false;
        });

        // show update form
        $(document).on('click', '.btn-update', function(){
            var activity_narrative = $(this).attr('data-activity_narrative'),
                id = $(this).attr('data-id'),
                seq = $(this).attr('data-seq');

            // set values
            init_modal(seq, activity_narrative, id);

            $modal.modal('show');

            return false;
        });

        // remove existing control
        $(document).on('click', '.btn-remove', function() {
            var remove_id = $(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "You are about to remove a business process area.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: '{{url('risk_assessment_scale/delete')}}/{{ $details->auditable_entity_id }}/{{ $details->bp_id }}/risk/'+remove_id,
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}' },
                    success : function(data) {
                        if( data.success ) {
                            swal("Deleted!", "Risk has been deleted.", "success");
                            updateDatatable();
                        }
                    }
                });
            });

            return false;
        });

        $(document).on('click', '.btn-add-info', function() {
            var id = $(this).attr('data-id');
            iapms.getAddInfoForm(id, 'risks').success(function(data) {
                if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                    ai_modal.find('.btn-save-ai').hide();
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                    ai_modal.find('.btn-save-ai').show();
                }
                ai_modal.modal('show');
                iapms.setAddInfoForm(ai_modal, '{{url('risks')}}/'+id+'/additional_information/update', 'put');

                toggle_loading();
            });

            return false;
        });

        $(function(){
            ai_modal.find('form').validate();
        });

        ai_modal.find('form').on('submit', function(e){
            e.preventDefault();
            if($(this).valid()) {
                swal({
                    title: "Continue?",
                    text: "You are about to update additional information.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, save it!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function(){
                    $.ajax({
                        url: ai_modal.find('form').attr('action'),
                        method: "POST",
                        data: ai_modal.find('form').serialize(), // serializes all the form data
                        success : function(data) {
                            if(data.success) {
                                swal("Success!", "Additional information has been updated.", "success");
                                location.reload();
                            }
                        },
                        error: function(xhr, textStatus, errorThrown){
                            swal.close();
                            // displays the validation error
                            var msg = '<ul class="list-unstyled">';

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                }
                            });
                            msg += '</ul>';

                            ai_modal.find('.alert-modal').html(msg).show();
                        }
                    });
                });
            }

            return false;
        });

        ai_modal.on('click', '.btn-save-ai', function() {
            ai_modal.find('form').submit();
        });
    </script>
@endsection
