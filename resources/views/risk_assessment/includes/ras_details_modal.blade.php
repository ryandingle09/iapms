<div class="modal fade" id="ras-details" style="overflow-y:scroll;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-6">

                        <div class="table-header">
                            Auditable Entity Details
                        </div>
                        <table class="table table-striped table-bordered table-details">
                            <tbody>
                                <tr>
                                    <th width="25%">IDL</th>
                                    <td colspan="2" class="label-auditable_entity_actual_auditable_entity_name label-auditable_entity_auditable_entity_name"> - </td>
                                </tr>
                                <tr>
                                    <th>Company</th>
                                    <td colspan="2" class="label-auditable_entity_actual_company_code"> - </td>
                                </tr>
                                <!--<tr>
                                    <th>Group Type</th>
                                    <td colspan="2" class="label-group_type"> - </td>
                                </tr>
                                <tr>
                                    <th>Business Type</th>
                                    <td colspan="2" class="label-business_type"> - </td>
                                </tr>
                                <tr>
                                    <th>Entity Class</th>
                                    <td colspan="2" class="label-entity_class"> - </td>
                                </tr>-->
                                <tr>
                                    <th>Branch Code</th>
                                    <td colspan="2" class="label-auditable_entity_actual_department_code"> - </td>
                                </tr>
                                <!--<tr>
                                    <th>Department Code</th>
                                    <td colspan="2" class="label-department_code"> - </td>
                                </tr>
                                <tr>
                                    <th>Contact Name</th>
                                    <td colspan="2" class="label-contact_name"> - </td>
                                </tr>
                                <tr>
                                    <th>Entity Head Name</th>
                                    <td colspan="2" class="label-entity_head_name"> - </td>
                                </tr>
                                <tr>
                                    <th>Active?</th>
                                    <td colspan="2" class="label-active"> - </td>
                                </tr>-->
                            </tbody>
                        </table>
                        <div id="au-buttons" class="row row-details">
                            <div class="col-sm-12" style="text-align: center;margin-top: 10px;">
                                <button type="button" class="btn btn-sm btn-primary btn-add-info"><i class="fa fa-list-ul"></i> Additional Info</button>
                                &nbsp;
                                <a href="#" class="btn btn-sm btn-success btn-company-profile" target="_blank"><i class="fa fa-institution"></i> Company Profile</a>
                            </div>
                        </div>

                        <div class="table-header">
                            Main Business Process Details
                        </div>
                        <table class="table table-striped table-bordered table-details">
                            <tbody>
                                <tr>
                                    <th width="23%">Name</th>
                                    <td colspan="2" class="label-mbpa_main_bp_name"> - </td>
                                </tr>
                                <tr>
                                    <th>Description</th>
                                    <td colspan="2" class="label-lv_main_bp_name_desc"> N/A</td>
                                </tr>
                            </tbody>
                        </table>

                        <div class="table-header">
                            Sub Business Process Details
                        </div>
                        <table class="table table-striped table-bordered table-details">
                            <tbody>
                            <tr>
                                <th width="23%">IDL</th>
                                <td colspan="2" class="label-business_process_bp_name"> - </td>
                            </tr>
                            <tr>
                                <th>Business Process</th>
                                <td colspan="2" class="label-business_process_bp_code"> - </td>
                            </tr>
                            <!--<tr>
                                <th>Business Process Description</th>
                                <td colspan="2" class="label-lv_bp_code_desc"> - </td>
                            </tr>-->
                            <tr>
                                <th>Source Type</th>
                                <td colspan="2" class="label-business_process_source_type "> - </td>
                            </tr>
                            <tr>
                                <th>Source Reference</th>
                                <td colspan="2" class="label-business_process_source_ref "> - </td>
                            </tr>
                            <tr>
                                <th>Remarks</th>
                                <td colspan="2" class="label-business_process_bp_remarks "> - </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <button type="button" class="btn btn-sm btn-primary btn-add-info-bp"><i class="fa fa-list-ul"></i> Additional Info</button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        
                        
                        
                        
                    </div>
                    <div class="col-xs-6">
                        

                        <div class="table-header">
                            Business Process Objectives
                        </div>
                        <table class="table table-striped table-bordered table-details">
                            <tbody>
                                <tr>
                                    <th width="23%">Short Name</th>
                                    <td colspan="2" class="label-bp_objective_objective_name "> - </td>
                                </tr>
                                <tr>
                                    <th>Objective Narrative</th>
                                    <td colspan="2" class="label-bp_objective_objective_narrative "> - </td>
                                </tr>
                                <tr>
                                    <th>Objective Category</th>
                                    <td colspan="2" class="label-bp_objective_objective_category "> - </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="table-header"> Risk Details </div>
                        <table class="table table-striped table-bordered table-details">
                            <tbody>
                            <tr>
                                <th width="35%">Risk Name</th>
                                <td colspan="2" class="label-risk_risk_code_meaning"> - </td>
                            </tr>
                            <tr>
                                <th>Risk Code</th>
                                <td colspan="2" class="label-risk_risk_code"> - </td>
                            </tr>
                            <tr>
                                <th>Risk Type</th>
                                <td colspan="2" class="label-risk_risk_type"> - </td>
                            </tr>
                            <tr>
                                <th>Risk Remarks</th>
                                <td colspan="2" class="label-risk_risk_remarks"> - </td>
                            </tr>
                            <tr>
                                <th>Source Type</th>
                                <td colspan="2" class="label-risk_risk_source_type"> - </td>
                            </tr>
                            <tr>
                                <th>Response Type</th>
                                <td colspan="2" class="label-risk_risk_response_type"> - </td>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <th width="12%">Impact</th>
                                <th>Likelihood</th>
                            </tr>
                            <tr>
                                <th>Default Inherent Rating</th>
                                <td class="rating-row"><div class="rating label-risk_def_inherent_impact_rate"></div></td>
                                <td class="rating-row"><div class="rating label-risk_def_inherent_likelihood_rate"></div></td>
                            </tr>
                            <tr>
                                <th>Default Residual Rating</th>
                                <td class="rating-row"><div class="rating label-risk_def_residual_impact_rate"></div></td>
                                <td class="rating-row"><div class="rating label-risk_def_residual_likelihood_rate"></div></td>
                            </tr>
                            <tr>
                                <th>Default Rating Remarks</th>
                                <td colspan="2" class="label-risk_rating_comment"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Enterprise Risk Title</th>
                                <td colspan="2" class="label-risk_name"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Description</th>
                                <td colspan="2" class="label-risk_desc"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Risk Remarks</th>
                                <td colspan="2" class="label-lv_enterprise_risk_code_desc"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Risk Type</th>
                                <td colspan="2" class="label-risk_type"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Risk Class</th>
                                <td colspan="2" class="label-risk_class"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Risk Area</th>
                                <td colspan="2" class="label-risk_area"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Response Type</th>
                                <td colspan="2" class="label-response_type"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Response Status</th>
                                <td colspan="2" class="label-response_status"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Response Status Comment</th>
                                <td colspan="2" class="label-response_status_comment"> - </td>
                            </tr>

                            <tr class="ent_risk_details">
                                <th>&nbsp;</th>
                                <th width="12%">Impact</th>
                                <th>Likelihood</th>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Inherent Rating</th>
                                <td class="rating-row"><div class="rating label-inherent_impact_rate"></div> - </td>
                                <td class="rating-row"><div class="rating label-inherent_likelihood_rate"></div> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Residual Rating</th>
                                <td class="rating-row"><div class="rating label-residual_impact_rate"></div> - </td>
                                <td class="rating-row"><div class="rating label-residual_likelihood_rate"></div> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Rating Remarks</th>
                                <td colspan="2" class="label-ent_rating_comment"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Rated By</th>
                                <td colspan="2" class="label-rated_by"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Rater Company</th>
                                <td colspan="2" class="label-"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Rater Department</th>
                                <td colspan="2" class="label-"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Rater Position</th>
                                <td colspan="2" class="label-"> - </td>
                            </tr>
                            <tr class="ent_risk_details">
                                <th>Rated Date</th>
                                <td colspan="2" class="label-rated_date"> - </td>
                            </tr>
                            <tr>
                                <td colspan="3"><button type="button" class="btn btn-primary btn-sm btn-add-info"><i class="fa fa-list-ul"></i> Additional Info</button></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@section('ras_details_modal_scripts')
@include('includes.modals.additional_information')
<script type="text/javascript">
    $(document).on('click', '.btn-details', function () {
        var row = $(this).parent().parent();
        selected_object = ras_datatable.row( row ).data();
        data = Object.flatten(selected_object);
        $.each(data, function(i, v){
            var label = '.label-'+i;

            if($(label).length){
                
                var value = v != null || v != "" ? v : 'N/A';
                $(label).html(value);

                if( i == 'auditable_entity_actual_company_code' ) {
                    $('.btn-company-profile').prop('href', '{{ url('company_profile/details') }}/'+v);
                }
            }else{

                // console.log(label +" : "+ $(label).length );
            }
        });

        if(data.risk_source_type === 'Enterprise') $('.ent_risk_details').show()
        else $('.ent_risk_details').hide()
        
        $('.btn-add-info-bp').attr('data-id', data.bp_id).show();
        $('.btn-add-info-ae').attr('data-id', data['auditable_entity_id']);
        $('#ras-details').modal('show');

        // if( $('td', this).prop('colspan') == '1' ) {
        //     $('#risk_assessment_scale-table tbody > tr').removeClass('selected-row'); // reset tr highlights
        //     var data = ras_datatable.row( this ).data();
        //     $('#au-buttons').show();

        //     $(this).addClass('selected-row');
        // }
        return false;
    } );

    $(document).on('click', '.btn-add-info-bp', function() {
        var id = $(this).attr('data-id');
        var ai_modal = $('#additional-information-modal');
        iapms.getAddInfoForm(id, 'business_processes').success(function(data) {
            ai_modal.find('#ai_wrapper').html(data.data);
            ai_modal.find('#additional-information-form input').attr('disabled', true);
            ai_modal.find('#additional-information-form select').attr('disabled', true);
            ai_modal.find('.btn-save-ai').hide();
            ai_modal.modal('show');
            iapms.setAddInfoForm(ai_modal, '{{url('business_process')}}/'+id+'/additional_information/update', 'put');

            toggle_loading();
        });

        return false;
    });
</script>
@endsection