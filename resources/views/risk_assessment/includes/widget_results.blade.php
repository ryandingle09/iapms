<div class="widget-box widget-color-blue2 widget-container-col">

    <div class="widget-header">
        <h4 class="widget-title lighter smaller"><i class="fa fa-filter"></i> Results</h4>
    </div>

    <div class="widget-body">
        <div class="widget-main padding-8">
            <table id="ras-table" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th > Main Business Proccess</th>
                    <th > Risk Name</th>
                    <th > Inherent Impact</th>
                    <th > Inherent Likelihood</th>
                    <th > Inherent Remarks</th>
                    <th > Residual Impact</th>
                    <th > Residual Likelihood</th>
                    <th > Residual Remarks</th>
                    <th class="text-center" width="10%"> Actions</th>
                </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>
@include('risk_assessment.includes.ras_details_modal')
@include('risk_assessment.includes.edit_ras_modal')
@section('widget_results_scripts')
<script type="text/javascript">
    ras_datatable = $('#ras-table').DataTable( {
        bDestroy: true,
        processing: true,
        order: [[ 1, 'asc' ]],
        createdRow: function( row, data, dataIndex ) {

        },
        columns: [
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return data.type == 'Actual' ? data.mbpa.main_bp_name : data.mbp.main_bp_name ;
                }
            },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return data.risk.risk_code ;
                }
            },
            { data: "inherent_impact_value"},
            { data: "inherent_likelihood_value"},
            { data: "inherent_remarks" ,  defaultContent: 'N/A'},
            { data: "residual_impact_value"},
            { data: "residual_likelihood_value"},
            { data: "residual_remarks" ,  defaultContent: 'N/A'},
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('risk_assessment')}}/'+data.risk_assessment_scale_id+'" class="btn-edit"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a>    <a href="#" class="btn-details"><i class="fa fa-eye" title="Details" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className : "text-center"
            }
        ]
    });

</script>
@yield('ras_details_modal_scripts')
@yield('edit_ras_modal_scripts')
@endsection
