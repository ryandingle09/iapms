<!-- Edit Projects Scopes Auditor Modal -->
<div class="modal fade " data-backdrop="static" id="edit-ras-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Risk Assessment Scale</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="edit-ras-alert"></div>
                        <form class="form-horizontal" id="edit-ras-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-3 control-label align-left " for="inherent_impact_value"> Inherent Impact Value </label>
                                <div class="col-sm-9">
                                    <input type="text" name="inherent_impact_value" class="form-control input-sm" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="inherent_likelihood_value"> Inherent Likelihood Value </label>
                                <div class="col-sm-9">
                                    <input type="text" name="inherent_likelihood_value" class="form-control input-sm " value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="inherent_remarks"> Inherent Remarks </label>
                                <div class="col-sm-9">
                                    <textarea class="form-control input-sm" name="inherent_remarks" rows=3></textarea>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-3 control-label align-left " for="residual_impact_value"> Residual Impact Value </label>
                                <div class="col-sm-9">
                                    <input type="text" name="residual_impact_value" class="form-control input-sm" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="residual_likelihood_value"> Residual Likelihood Value </label>
                                <div class="col-sm-9">
                                    <input type="text" name="residual_likelihood_value" class="form-control input-sm " value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="residual_remarks"> Residual Remarks </label>
                                <div class="col-sm-9">
                                    <textarea class="form-control input-sm" name="residual_remarks" rows=3></textarea>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Projects Scopes Auditor Modal -->


@section('edit_ras_modal_scripts')
<script type="text/javascript">
    $(document).on('click','#ras-table .btn-edit', function(){
        var row = $(this).parent().parent();
        data = ras_datatable.row( row ).data();
        var edit_ras_url = "{{ url('risk_assessment') }}/"+data.risk_assessment_scale_id;
        $("#edit-ras-form").attr('action',edit_ras_url).supply(data);
        $("#edit-ras-modal").modal('show');
        return false;
    });

    $("#edit-ras-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : "PUT",
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#edit-ras-form")[0].reset();
                $("#edit-ras-modal").modal('hide');
                ras_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#edit-ras-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    }); 
</script>
@endsection