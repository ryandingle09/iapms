<div class="widget-box widget-color-blue2 widget-container-col">

    <div class="widget-header">
        <h4 class="widget-title lighter smaller"><i class="fa fa-filter"></i> Filter</h4>
        <div class="widget-toolbar">
            <a href="#" data-toggle="collapse" data-target="#filter-body">
                <i class="ace-icon fa fa-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body collapse in" id="filter-body">
        <div class="widget-main padding-8">
            <div id="filter-alert" class="mrgB15"></div>
            <form class="form-horizontal" id="filter-form">

                <input type="hidden" name="type" value="Actual">

                <div class="form-group">
                    <label for="search_entity" class="col-sm-2 control-label">Auditable Entity</label>
                    <div class="col-sm-9">
                        <select class="basic-info form-control lookup filter" id="auditable_entity_id" name="auditable_entity_id" placeholder="Select a entity"></select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="search_bp"> Main Business Process </label>
                    <div class="col-sm-9">
                        <select class="basic-info form-control lookup" id="ae_mbp_id" name="ae_mbp_id" placeholder="Select a main process" ></select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="search_sub_bp"> Sub Business Process </label>
                    <div class="col-sm-9">
                        <select class="basic-info form-control lookup" id="bp_id" name="bp_id" placeholder="Select a sub process" ></select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="bp_objective_id"> Business Process Objective </label>
                    <div class="col-sm-9">
                        <select class="basic-info form-control lookup" id="bp_objective_id" name="bp_objective_id" placeholder="Select a objective"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="risk_id"> Risk </label>
                    <div class="col-sm-9">
                        <select class="basic-info form-control lookup" id="risk_id" name="risk_id" placeholder="Select a risk"></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="pull-right">
                            <button class="btn btn-primary btn-filter"><i class="fa fa-bolt"></i> Go</button>
                            <a href="#" class="btn btn-warning btn-clear-filter" ><i class="fa fa-times-circle"></i> Clear</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>
@section('widget_filter_scripts')
<script type="text/javascript">
    $("#filter-form").submit(function(){
        $(".alert").remove();
        $(".has-error").removeClass('has-error');
        if(!$("#auditable_entity_id").val()){
            $("#auditable_entity_id").parents(".form-group").addClass('has-error');
            $("#filter-alert").aError("Auditable Entity is required.");
            return false;
        }
        var query = "";
        $.each($(this).serializeArray() , function(key,value){
            if(value.value.length) query += value.name + ":" + value.value + ";";   
        });
        ras_datatable.ajax.url( '{{route('risk_assessment.list')}}?searchJoin=and&search=' + query.slice(0,-1) ).load();
        // $('#filter-body').collapse('hide');    
        return false;
    });

    var aea_select = $("#auditable_entity_id").selectize({
            valueField: 'auditable_entity_id',
            labelField: 'auditable_entity_name',
            searchField: ['auditable_entity_name'],
            options: [],
            create: false,
            preload: true,
            loadThrottle: 800,
            render: {
                option: function (item, escape) {
                    return '<div>' + escape(item.auditable_entity_name) + '</div>';
                }
            },
            load: function (query, callback) {
                $.ajax({
                    url: "{{ route('aea.index') }}?search=auditable_entity_name:"+encodeURIComponent(query)+"&searchFields=auditable_entity_name:like",
                    type: 'GET',
                    success: function (response) {
                        // aea_select.clearOptions();
                        callback(response.data);
                    },
                    error: function () {
                        callback();
                    }
                });
            },
            onChange: function(value) {
                if (!value.length) return;
                mbp_select.clearOptions();
                mbp_select.disable();

                sbp_select.clearOptions();
                sbp_select.disable();

                bpo_select.clearOptions();
                bpo_select.disable();

                risk_select.clearOptions();
                risk_select.disable();

                mbp_select.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ url('aea') }}/'+value+'/mbpa',
                        success: function(response) {
                            mbp_select.enable();
                            callback(response.data);
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            }
        })[0].selectize;

    var mbp_select = $("#ae_mbp_id").selectize({
            valueField: 'ae_mbp_id',
            labelField: 'main_bp_name',
            searchField: ['main_bp_name'],
            options: [],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' + escape(item.main_bp_name) + '</div>';
                }
            },
            onChange: function(value) {
                if (!value.length) return;
                sbp_select.clearOptions();
                sbp_select.disable();

                bpo_select.clearOptions();
                bpo_select.disable();

                risk_select.clearOptions();
                risk_select.disable();

                sbp_select.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ url('aea-mbpa') }}/'+value+'/sbp',
                        success: function(response) {
                            sbp_select.enable();
                            callback(response.data);
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            }
        })[0].selectize;

    mbp_select.disable();

    var sbp_select = $("#bp_id").selectize({
            valueField: 'id',
            labelField: 'text',
            searchField: ['text'],
            options: [],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' + escape(item.text) + '</div>';
                }
            },
            onChange: function(value) {
                if (!value.length) return;
                bpo_select.clearOptions();
                bpo_select.disable();

                risk_select.clearOptions();
                risk_select.disable();
            
                bpo_select.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ url('business_process/objectives') }}/'+value,
                        success: function(results) {
                            bpo_select.enable();
                            callback(results);
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            }
        })[0].selectize;

    sbp_select.disable();
     

    var bpo_select = $("#bp_objective_id").selectize({
            valueField: 'bp_objective_id',
            labelField: 'objective_name',
            searchField: ['objective_name'],
            options: [],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' + escape(item.objective_name) + '</div>';
                }
            },
            onChange: function(value) {
                if (!value.length) return;
                risk_select.clearOptions();
                risk_select.disable();
                risk_select.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ url('business_process/objectives') }}/'+value+'/risks',
                        success: function(results) {
                            risk_select.enable();
                            callback(results);
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            }
        })[0].selectize;

    bpo_select.disable();

    var risk_select = $("#risk_id").selectize({
            valueField: 'risk_id',
            labelField: 'lv_risk_code_meaning',
            searchField: ['lv_risk_code_meaning'],
            options: [],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' + escape(item.lv_risk_code_meaning) + '</div>';
                }
            }
        })[0].selectize;

    risk_select.disable();

</script>
@endsection

