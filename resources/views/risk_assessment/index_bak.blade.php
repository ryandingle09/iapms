@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/plugins/selectize/selectize.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.bootstrap3.css" />
    <link rel="stylesheet" href="/css/selectize-custom.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <style type="text/css">
        .dataTables_filter { display: none !important; }
        .selectize-control.filter::before {
            -moz-transition: opacity 0.2s;
            -webkit-transition: opacity 0.2s;
            transition: opacity 0.2s;
            content: ' ';
            z-index: 2;
            position: absolute;
            display: block;
            top: 12px;
            right: 34px;
            width: 16px;
            height: 16px;
            background: url(/img/loading.gif);
            background-size: 16px 16px;
            opacity: 0;
        }
        .selectize-control.filter.loading::before {
            opacity: 0.4;
        }
        .dt-editable {
            cursor:cell;
        }
        .align-center {
            text-align: center;
        }
    </style>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header', ['heading' => 'Risk Assessment - '.(\Request::get('type') == 'actual' ? 'Actual' : 'Master')])

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="widget-box widget-color-blue2 widget-container-col">
                        <div class="widget-header">
                            <h4 class="widget-title lighter smaller"><i class="fa fa-filter"></i> Filter</h4>
                            <div class="widget-toolbar">
                                <a href="#" data-action="collapse">
                                    <i class="ace-icon fa fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main padding-8">
                                <form class="form-horizontal" id="filter-form">
                                    <input type="hidden" name="type" value="{{\Request::get('type')}}">
                                    <div class="form-group">
                                        <label for="search_entity" class="col-sm-2 control-label">Auditable Entity</label>
                                        <div class="col-sm-9">
                                            <select class="basic-info form-control lookup filter" id="auditable_entity" name="auditable_entity" placeholder="Select a entity"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="search_bp"> Main Business Process </label>
                                        <div class="col-sm-9">
                                            <select class="basic-info form-control lookup" id="main_business_process" name="main_business_process" placeholder="Select a main process" required></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="search_sub_bp"> Sub Business Process </label>
                                        <div class="col-sm-9">
                                            <select class="basic-info form-control lookup" id="business_process" name="business_process" placeholder="Select a sub process" required></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="objective"> Business Process Objective </label>
                                        <div class="col-sm-9">
                                            <select class="basic-info form-control lookup" id="objective" name="objective" placeholder="Select a objective"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="risk"> Risk </label>
                                        <div class="col-sm-9">
                                            <select class="basic-info form-control lookup" id="risk" name="risk" placeholder="Select a risk"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-primary btn-filter"><i class="fa fa-bolt"></i> Go</button>
                                                <button type="button" class="btn btn-warning btn-clear-filter"><i class="fa fa-times-circle"></i> Clear</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="widget-container-col">
                        <div class="widget-box">
                            <div class="widget-body">
                                <div class="widget-main padding-6">
                                    <table id="risk_assessment_scale-table" class="table table-striped table-bordered table-hover">
                                        <thead>
                                        <tr>
                                            <th width="9.5%">Auditable Entity Name</th>
                                            <th width="15%">Sub Business Process Name</th>
                                            <th width="15%">Business Process Objective</th>
                                            <th width="9.5%">Risk Name</th>
                                            <th width="6%">Inherent Impact</th>
                                            <th width="6%">Inherent Likelihood</th>
                                            <th width="9.5%">Inherent Remarks</th>
                                            <th width="6%">Residual Impact</th>
                                            <th width="6%">Residual Likelihood</th>
                                            <th width="9.5%">Residual Remarks</th>
                                            <th></th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

@include('risk_assessment.details')
@include('includes.modals.additional_information')
<script type="text/template" id="rating-template">
    {!! Form::selectRange('rating', 1, 5, null, ['class' => 'form-control']) !!}
</script>
<script type="text/template" id="remarks-template">
    <textarea class="form-control" name="remarks" style="resize: none;" rows="2"></textarea>
</script>
@endsection

@section('footer_script')
<script src="/js/iapms.js"></script>
<script src="/plugins/selectize/selectize.js"></script>
<script src="/js/jquery.validate.js"></script>
<script src="/js/date-time/bootstrap-datepicker.js"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var $alert  = $('.alert-ras'),
        form    = $('#risk-scale-form'),
        $modal  = $('#risk-scale-modal'),
        $business_process  = $('#business_process'),
        $auditable_entity  = $('#auditable_entity'),
        $main_bp  = $('#main_business_process'),
        $bp = $('#business_process');
    var xhr, select_entity, $select_entity;
    var select_main_bp, $select_main_bp;
    var select_bp, $select_bp;
    var select_objective, $select_objective;
    var select_risk, $select_risk;
    var ai_modal = $('#additional-information-modal');
    var inherent_impact,
        inherent_likelihood,
        inherent_remarks,
        residual_impact,
        residual_likelihood,
        residual_remarks;
    var select_entity_url = '{!! \Request::get('type') == 'actual' ? route('auditable_entities.lists').'?ra=true&limit=10&entity_type=actual&entity_search=' : route('auditable_entities.master.lists').'?searchFields=auditable_entity_name:like&search=' !!}';
    var type = '{{ \Request::get('type') == 'actual' ? 'actual' : 'master' }}';
    var datatable= $('#risk_assessment_scale-table').DataTable( {
        {{--ajax: "{{route('risk_assessment.list')}}",--}}
        "processing": true,
        data: [],
        createdRow: function( row, data, dataIndex ) {
            // Set the data-status attribute, and add a class
            $( row ).find('td:eq(4)').attr('data-value', inherent_impact)
                .attr('data-type', 'inherent_impact_value');
            $( row ).find('td:eq(5)').attr('data-value', inherent_likelihood)
                .attr('data-type', 'inherent_likelihood_value');
            $( row ).find('td:eq(6)').attr('data-value', inherent_remarks)
                .attr('data-type', 'inherent_remarks');
            $( row ).find('td:eq(7)').attr('data-value', residual_impact)
                .attr('data-type', 'residual_impact_value');
            $( row ).find('td:eq(8)').attr('data-value', residual_likelihood)
                .attr('data-type', 'residual_likelihood_value');
            $( row ).find('td:eq(9)').attr('data-value', residual_remarks)
                .attr('data-type', 'residual_remarks');
        },
        columns: [
            { data: "auditable_entity_name" },
            { data: "bp_name", defaultContent: 'N/A' },
            { data: "objective_name", defaultContent: 'N/A' },
            { data: "lv_risk_code_meaning", defaultContent: 'N/A' },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    inherent_impact = parseInt(data.inherent_impact_value) > 0 ? data.inherent_impact_value : data.def_inherent_impact_rate; // store inherent_impact in global variable
                    return inherent_impact;
                },
                className: 'dt-editable align-center',
                searchable: false
            },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    inherent_likelihood = parseInt(data.inherent_likelihood_value) > 0 ? data.inherent_likelihood_value : data.def_inherent_likelihood_rate;

                    return inherent_likelihood;
                },
                className: 'dt-editable align-center',
                searchable: false
            },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    inherent_remarks = data.inherent_remarks != '' ? data.inherent_remarks : '';

                    return inherent_remarks;
                },
                className: 'dt-editable',
                searchable: false
            },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    residual_impact = parseInt(data.residual_impact_value) > 0 ? data.residual_impact_value : data.def_residual_impact_rate; // store inherent_impact in global variable
                    return residual_impact;
                },
                className: 'dt-editable align-center',
                searchable: false
            },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    residual_likelihood = parseInt(data.residual_likelihood_value) > 0 ? data.residual_likelihood_value : data.def_residual_likelihood_rate;

                    return residual_likelihood;
                },
                className: 'dt-editable align-center',
                searchable: false
            },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    residual_remarks = data.residual_remarks != '' ? data.residual_remarks : '';

                    return residual_remarks;
                },
                className: 'dt-editable',
                searchable: false
            },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="btn-details"><i class="fa fa-eye" title="Details" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    $('#risk_assessment_scale-table tbody').on('click', 'td', function () {
        if( $(this).hasClass('dt-editable') && $(this).find('.form-control').length < 1 ) {
            var td = $(this);
            var data = datatable.row( td.parent() ).data();
            var data_type = td.attr('data-type');
            var data_value = td.attr('data-value');
            var input = data_type.indexOf('remarks') !== -1 ? $('#remarks-template').html() : $('#rating-template').html();

            td.html('').append(input);
            td.find('.form-control')
                .prop('name', data_type)
                .attr('data-orig', data_value)
                .val(data_value)
                .focus();

            td.find('.form-control').on('blur', function(){
                var orig_val = $(this).attr('data-orig');
                var new_val = $(this).val();

                if( (new_val != '' || new_val != '0') && orig_val != new_val ) {
                    td.html(new_val);
                    // save changes and update the value on the table
                    $.ajax({
                        url: '{{url('risk_assessment/risks/update')}}/'+data.auditable_entity_id+'/'+data.bp_id+'/'+data.risk_id,
                        method: "POST",
                        data: { _token : _token, _method : 'put', data_type : data_type, data_value : new_val }, // serializes all the form data
                        beforeSend : function() {
                            toggle_loading();
                        },
                        success : function(data) {
                            swal("Saved!", "Risk details is successfully updated.", "success");
                            datatable.ajax.reload( null, false );
                            $('.table-details td').not('.rating-row').html('');
//                            setRaty();
                            toggle_loading();
                        }
                    });
                }
                else {
                    // show original value if there's no changes made
                    td.html(orig_val);
                }
                // remove input element
                $(this).remove();
            });
        }
    });

    $(document).ready(function() {
        /**
         * Set  Auditable Entity and Business Process details on designated labels
         */
        $(document).on('click', '.btn-details', function () {
            var row = $(this).parent().parent();
            var data = datatable.row( row ).data();

            $.each(data, function(i, v){
                var label = '.label-'+i;
                if( $(label).length ) {
                    var value = v != null ? v : 'N/A';
                    $(label).html(value);

                    if( i =='company_code' ) {
                        $('.btn-company-profile').prop('href', '{{ url('company_profile/details') }}/'+v);
                    }
                }
            });

            if(data.risk_source_type === 'Enterprise') $('.ent_risk_details').show()
            else $('.ent_risk_details').hide()

            $('#ras-details').modal('show');
            return false;

            if( $('td', this).prop('colspan') == '1' ) {
                $('#risk_assessment_scale-table tbody > tr').removeClass('selected-row'); // reset tr highlights
                var data = datatable.row( this ).data();
                $('#au-buttons').show();
//                ras-details

                $(this).addClass('selected-row');
                $('.btn-add-info').attr('data-id', data['auditable_entity_id']);
                $('.btn-add-info-bp').attr('data-id', data['bp_id']).show();
            }
        } );

        // selectize
        $select_entity = $auditable_entity.selectize({
            valueField: 'auditable_entity_id',
            labelField: 'auditable_entity_name',
            searchField: ['auditable_entity_name'],
            options: [],
            create: false,
            preload: true,
            loadThrottle: 800,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.auditable_entity_name) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            load: function (query, callback) {
                $.ajax({
                    url: select_entity_url+encodeURIComponent(query),
                    type: 'GET',
                    success: function (res) {
                        select_entity.clearOptions();
                        callback(res);
                    },
                    error: function () {
                        callback();
                    }
                });
            },
            onChange: function(value) {
                if (!value.length) return;
                select_main_bp.clearOptions();
                select_main_bp.disable();
                select_bp.clearOptions();
                select_bp.disable();

                select_main_bp.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ url('auditable_entities/master') }}/'+value+'/business_processes/main?type='+type+'&json=true',
                        success: function(results) {
                            select_main_bp.enable();
                            callback(results);
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            }
        });
        select_entity = $select_entity[0].selectize;

        $select_main_bp = $main_bp.selectize({
            valueField: 'ae_mbp_id',
            labelField: 'main_bp_name',
            searchField: ['main_bp_name'],
            options: [],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.main_bp_name) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            onChange: function(value) {
                if (!value.length) return;
                var sub_bps = select_main_bp.options[value].sub_bp_details;

                select_bp.clearOptions();
                select_bp.enable();
                select_bp.addOption(sub_bps);
            }
        });
        select_main_bp = $select_main_bp[0].selectize;
        select_main_bp.disable();

        $select_bp = $bp.selectize({
            valueField: 'bp_id',
            labelField: 'bp_name',
            searchField: ['bp_name'],
            options: [],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.bp_name) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            onChange: function(value) {
                if (!value.length) return;

                select_objective.clearOptions();
                select_objective.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ url('business_process/objectives') }}/'+value,
                        success: function(results) {
                            select_objective.enable();
                            callback(results);
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            }
        });
        select_bp = $select_bp[0].selectize;
        select_bp.disable();

        $select_objective = $('#objective').selectize({
            valueField: 'bp_objective_id',
            labelField: 'objective_name',
            searchField: ['objective_name'],
            options: [],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.objective_name) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            onChange: function(value) {
                if (!value.length) return;

                select_risk.clearOptions();
                select_risk.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{ url('business_process/objectives') }}/'+value+'/risks',
                        success: function(results) {
                            select_risk.enable();
                            callback(results);
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            }
        });
        select_objective = $select_objective[0].selectize;
        select_objective.disable();

        $select_risk = $('#risk').selectize({
            valueField: 'risk_id',
            labelField: 'lv_risk_code_meaning',
            searchField: ['lv_risk_code_meaning'],
            options: [],
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.lv_risk_code_meaning) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            }
        });
        select_risk = $select_risk[0].selectize;
        select_risk.disable();
        // end selectize
    } );

    $modal.on('hidden.bs.modal', function(){
        select_entity.clear();
        select_bp.clear();
        select_bp.disable();
        $alert.html('').hide();
        $('.basic-info').parent().parent().removeClass('has-error');
    });

    $('.btn-clear-filter').on('click', function(){
        window.location = '{{ route('risk_assessment') }}';
    });

    $('.btn-filter').on('click', function(){
        datatable.ajax.url( '{{route('risk_assessment.list')}}?'+$('#filter-form').serialize() ).load();
    });

    $(document).on('click', '.btn-save', function() {
        swal({
            title: "Continue?",
            text: "You are about to create new risk assesment.",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        location.reload();
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }
                }
            });
        });
    });

    // remove
    $(document).on('click', '.btn-delete', function() {
        var remove_auid = $(this).attr('data-au_id');
        var remove_bpid = $(this).attr('data-bp_id');

        swal({
                title: "Are you sure?",
                text: "You are about to remove a risk assignment.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: '{{url('risk_assessment/delete')}}/'+remove_auid+'/'+remove_bpid,
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}' },
                    success : function(data) {
                        if( data.success ) {
                            swal("Deleted!", "Risk assignment has been deleted.", "success");
                            datatable.ajax.reload( null, false );
                            $modal.modal('hide');
                        }
                    }
                });
            });

        return false;
    });

    $(document).on('click', '.btn-add-info', function() {
        var id = $(this).attr('data-id');
        iapms.getAddInfoForm(id, 'auditable_entities').success(function(data) {
            if( !data.success ) {
                ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
            }
            else {
                ai_modal.find('#ai_wrapper').html(data.data);
            }
            ai_modal.find('#additional-information-form input').attr('disabled', true);
            ai_modal.find('#additional-information-form select').attr('disabled', true);
            ai_modal.find('.btn-save-ai').hide();
            ai_modal.modal('show');
            iapms.setAddInfoForm(ai_modal, '{{url('auditable_entities')}}/'+id+'/additional_information/update', 'put');

            toggle_loading();
        });

        return false;
    });

    $(document).on('click', '.btn-add-info-bp', function() {
        var id = $(this).attr('data-id');
        iapms.getAddInfoForm(id, 'business_processes').success(function(data) {
            ai_modal.find('#ai_wrapper').html(data.data);
            ai_modal.find('#additional-information-form input').attr('disabled', true);
            ai_modal.find('#additional-information-form select').attr('disabled', true);
            ai_modal.find('.btn-save-ai').hide();
            ai_modal.modal('show');
            iapms.setAddInfoForm(ai_modal, '{{url('business_process')}}/'+id+'/additional_information/update', 'put');

            toggle_loading();
        });

        return false;
    });

    $(function(){
        ai_modal.find('form').validate();
    });

    ai_modal.find('form').on('submit', function(e){
        e.preventDefault();
        if($(this).valid()) {
            swal({
                    title: "Continue?",
                    text: "You are about to update additional information.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, save it!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function(){
                    $.ajax({
                        url: ai_modal.find('form').attr('action'),
                        method: "POST",
                        data: ai_modal.find('form').serialize(), // serializes all the form data
                        success : function(data) {
                            if(data.success) {
                                swal("Success!", "Additional information has been updated.", "success");
                                location.reload();
                            }
                        },
                        error: function(xhr, textStatus, errorThrown){
                            swal.close();
                            // displays the validation error
                            var msg = '<ul class="list-unstyled">';

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                }
                            });
                            msg += '</ul>';

                            ai_modal.find('.alert-modal').html(msg).show();
                        }
                    });
                });
        }

        return false;
    });

    ai_modal.on('click', '.btn-save-ai', function() {
        ai_modal.find('form').submit();
    });
</script>
@endsection