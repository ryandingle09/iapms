<div class="col-xs-12">
    <div class="table-header"> Project - Scopes </div>
</div>
<div class="col-xs-12 mrgB15">
    <table id="scope-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Project Name</th>
                <th >Audit Type</th>
                <th >Auditable Entity Name</th>
                <th >Main Business Process Name</th>
                <th >IOM No.</th>
                <th >Target Start Date</th>
                <th >Target End Date</th>

                <th >Allotted Miscellaneous Mandays</th>
                <th >Allotted APG Mandays</th>
                <th >Allotted Total Mandays</th>

                <th >Approval Status</th>
                <th >Freeze Status</th>
                <th >Actions</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@section('tab_scope_scripts')
<script type="text/javascript">
    //selectedScope coming from fieldwork.index
    scope_datatable = $("#scope-table").DataTable( {
        "bDestroy": true,
        ajax: "{{ url('project/0/scope/list')}}?userfilter=true",
        "processing": true,
        order: [[ 1, 'asc' ]],
        createdRow: function( row, data, dataIndex ) {
            if(selectedScope == data.project_scope_id){
                $( row ).addClass('selected-row');
            }
            $( row ).attr('data-id', data.project_scope_id);
        },
        columns: [
            { data: "project.project_name" },
            { data: "project.audit_type" },
            { data: "auditable_entity.auditable_entity_name" },
            { data: "main_bp.main_bp_name" },
            { data: "iom_ref_no" },
            { data: "target_start_date" },
            { data: "target_end_date" },
            { data: "allotted_miscellaneous_mandays" },
            { data: "allotted_apg_mandays" },
            { data: "allotted_total_mandays" },
            { data: "project_scope_status" },
            { data: "freeze_status" ,}, // TODO
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="" class="btn-mbpa-details" data-aeid="'+data.auditable_entity.auditable_entity_id+'" data-mbpaid="'+data.main_bp.ae_mbp_id+'" ><i class="fa fa-sitemap text-primary" title="Details" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            },
        ]
    });

    $(document).on('click','#scope-table tbody tr',function(){
        $("#main_tab").find("a[href='#apg']").attr('data-toggle','tab');
        selectedScope = $(this).data('id');
        $(".selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
    });
</script>
@endsection