@extends('layouts.app')
@section('styles')

@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')
        <div class="page-content">
            @include('includes.content_header')
            <div id="alert-wrapper">
                @include('includes.alerts')
            </div>
        	<div class="col-xm-12">
            	<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="main_tab">
                    <li class="active">
                        <a href="#scope" data-toggle="tab" aria-expanded="true">Project - Scope</a>
                    </li>
                	<li >
                        <a href="#apg" data-toggle="" aria-expanded="false">APG</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- Scope -->
                    <div id="scope" class="tab-pane active">
                        <div class="row">
                            @include('project_engagement_update.includes.tab_scope')
                        </div>
                    </div>
                    <!-- /Scope -->
                    <!-- APG -->
                    <div id="apg" class="tab-pane">
                        <div class="row">
                            @include('project_engagement_update.includes.tab_apg')
                        </div>
                    </div>
                    <!-- /APG -->
                </div>
        	</div>
		</div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection
@section('footer_script')
<script src="/plugins/tinymce/tinymce.min.js"></script>
<script src="/plugins/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript">
    
</script>
@yield('tab_scope_scripts')
@yield('tab_apg_scripts')
@endsection
