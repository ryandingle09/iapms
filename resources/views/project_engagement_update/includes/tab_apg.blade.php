<div class="col-xs-12 mrgB15">
    <div class="table-header"> Audit Program Guide </div>
</div>
<div class="col-xs-12">
    <a class="btn btn-primary btn-sm btn-create" data-toggle="modal" data-target='#create-project-scope-apg-modal'>
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>
</div>
<div class="col-xs-12 mrgB15">
    <table id="project-scope-apg-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Control Sequence</th>
                <th >Control Name</th>
                <th >Test Sequence</th>
                <th >Test Procedure Narrative</th>
                <th >Audit Objectives</th>
                <th >Auditor Name</th>
                <th >Budgeted Mandays</th>
                <th >Status</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@include('engagement_planning.includes.project_scope_apg_modal')
@section('tab_apg_scripts')
<script type="text/javascript">
    // selectedScope variables can from tab_scope.blade.php
    var project_scope_apg_datatable = null;
    $(document).on('click',"#main_tab a[href='#apg']",function(){
        if(selectedScope){
            var url = "{{ url('project-scope') }}/" + selectedScope + "/apg";
            $("#create-project-scope-apg-form").attr('action',url);
            project_scope_apg_datatable = $("#project-scope-apg-table").DataTable( {
                "bDestroy": true,
                ajax: url + "/list",
                "processing": true,
                order: [[ 1, 'asc' ]],
                columns: [
                    {
                        data: null,
                        render : function ( data, type, full, meta ) {

                            return data.control_seq ? data.control_seq : 'n/a';
                        }
                    },
                    { 
                        data: null,
                        render : function(data, type, full, meta){
                            return data.control_master ? data.control_master.control_name : "n/a";
                        }

                    },
                    { 
                        data: null,
                        render : function(data, type, full, meta){
                            return data.control_test_seq ? data.control_test_seq : "n/a";
                        }

                    },
                    { 
                        data: null,
                        render : function(data, type, full, meta){
                            return data.test_proc ? data.test_proc_narrative : "n/a";
                        }

                    },
                    { 
                        data: null,
                        render : function(data, type, full, meta){
                            return data.test_proc ? data.audit_objective : "n/a";
                        }

                    },
                    {
                        data: null,
                        render : function ( data, type, full, meta ) {

                            return data.auditor ? data.auditor.first_name+' '+data.auditor.middle_name+' '+data.auditor.last_name : 'n/a';
                        }
                    },
                    { data: "budgeted_mandays" },
                    { data: "status" },
                    {
                        data: null,
                        "render": function ( data, type, full, meta ) {
                            return '<a href="'+url+'/'+data.project_scope_apg_id+'" class="edit" data-id="" rel="tooltip" data-original-title="Edit"><i class="fa fa-pencil"></i></a>    <a href="'+url+'/'+data.project_scope_apg_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                        },
                        orderable: false,
                        searchable: false,
                        className: "text-center"
                    }
                ]
            });
        }else{
            $.sInfo("You have to select project scope to open this tab.");
        }
    });

    $(document).on("click","#project-scope-apg-table .delete",function(){
        $(this).deleteItemFrom(project_scope_apg_datatable);
        return false;
    });

</script>
@yield('project_scope_apg_modal_scripts')
@endsection