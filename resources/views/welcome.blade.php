@extends('layouts.vue')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Welcome To Vue JS</div>

                <div class="panel-body">
                    <div id="app">
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" name="search" id="search" class="form-control input-sm year-picker" placeholder="Audit Year" required v-model="search">
                            </div>
                            <div class="col-sm-2">
                                <button class="btn btn-default btn-sm" type="button" v-on:click="searchPlan">
                                    <i class="ace-icon fa fa-search"></i>
                                    Go
                                </button>
                            </div>
                        </div>
                        <div style="padding-top: 10px;">
                            <button class="btn btn-success btn-sm btn-create-plan" type="button" v-on:click="toggleCreate(true)" v-show="!create">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </button>
                            <button class="btn btn-primary btn-sm btn-create-plan" v-on:click="toggleCreate(false)" type="button" v-show="create">
                                <i class="ace-icon fa fa-save"></i>
                                Save
                            </button>
                            <button class="btn btn-warning btn-sm btn-create-plan" type="button" v-on:click="toggleCreate(false)" v-show="create">
                                <i class="ace-icon fa fa-times"></i>
                                Cancel
                            </button>
                            <button class="btn btn-default btn-sm" type="button">
                                <i class="ace-icon fa fa-list-ol"></i>
                                Guide
                            </button>
                        </div>
                        {!! Form::open(['url' => '','class' => 'form-horizontal', 'id' => 'planning-form']) !!}
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="audit_year"> Audit Annual Year </label>
                                <div class="col-sm-2">
                                    <input type="text" name="audit_year" id="audit_year" class="form-control input-sm basic-info" placeholder="Audit Year" v-model="plan.year" required :disabled="inputToggle">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="name"> Plan Name </label>
                                <div class="col-sm-6">
                                    <input type="text" name="name" id="name" class="form-control input-sm basic-info" v-model="plan.name" required :disabled="inputToggle">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="status"> Status </label>
                                <div class="col-sm-2">
                                    <?php $status = config('iapms.lookups.planning.status'); ?>
                                    <select id="status" name="status" v-model="plan.status" class="form-control input-sm" required disabled>
                                        @if( count($status) )
                                            @foreach($status as $key => $val)
                                                <option value="{{ $val['text'] }}" {{ !empty($details['data']) ? ($details['data']['status'] == $val['text'] ? 'selected' : '') : '' }}>{{ $val['text'] }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="remarks"> Remarks </label>
                                <div class="col-sm-6">
                                    <textarea name="remarks" id="remarks" class="form-control basic-info" rows="3" style="resize: none;" disabled  v-model="plan.remarks" :disabled="inputToggle"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="reviewed_by"> Reviewed By </label>
                                <div class="col-sm-3">
                                    <input type="text" name="reviewed_by" id="reviewed_by" class="form-control input-sm" v-model="plan.reviewer" disabled>
                                </div>
                                <label class="col-sm-1 control-label" for="reviewed_date"> Date </label>
                                <div class="col-sm-2">
                                    <input type="text" name="reviewed_date" id="reviewed_date" class="form-control input-sm" v-model="plan.review_date" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="approved_by"> Approved By </label>
                                <div class="col-sm-3">
                                    <input type="text" name="approved_by" id="approved_by" class="form-control input-sm" v-model="plan.approver" disabled>
                                </div>
                                <label class="col-sm-1 control-label" for="approved_date"> Date </label>
                                <div class="col-sm-2">
                                    <input type="text" name="approved_date" id="approved_date" class="form-control input-sm" v-model="plan.approve_date" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="au_annual_mandays"> Auditor's Annual Mandays </label>
                                <div class="col-sm-2">
                                    <input type="text" name="au_annual_mandays" id="au_annual_mandays" class="form-control input-sm" value="" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="au_running_mandays"> Auditor's Running Mandays </label>
                                <div class="col-sm-2">
                                    <input type="text" name="au_running_mandays" id="au_running_mandays" class="form-control input-sm" value="" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="au_available_mandays"> Auditor's Available Mandays </label>
                                <div class="col-sm-2">
                                    <input type="text" name="au_available_mandays" id="au_available_mandays" class="form-control input-sm" value="" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="reviewer_remarks"> Reviewer Remarks </label>
                                <div class="col-sm-6">
                                    <textarea name="reviewer_remarks" id="reviewer_remarks" class="form-control" rows="3" style="resize: none;" :disabled="plan.status != 'For Review' ? true : false"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="approver_remarks"> Approver Remarks </label>
                                <div class="col-sm-6">
                                    <textarea name="approver_remarks" id="approver_remarks" class="form-control" rows="3" style="resize: none;" :disabled="plan.status != 'For Approval' ? true : false"></textarea>
                                </div>
                            </div>
                            <div class="form-group" id="approval" class="edit-components">
                                <div class="col-sm-12">
                                    <button type="button" class="btn btn-info btn-sm"><i class="fa fa-thumbs-up"></i> Approval</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
