@extends('layouts.app')
@section('styles')
    <ink rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')
            @include('includes.alerts')
            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('inter_office_memo.create')}}">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Additional Informations List
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="memo-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="20%">Subject</th>
                                <th width="50%">Start Date</th>
                                <th width="20%">Introduction</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@include('engagement_planning.includes.annual_audit_plan_projects_modal')
@endsection
@section('footer_script')
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script>
    $('#memo-table').DataTable( {
        ajax: "{{route('inter_office_memo.lists')}}",
        "processing": true,
        columns: [
            { data: "subject" },
            { data: "start_date" },
            { data: "introduction" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('inter-office-memo/edit')}}/'+data.project_scope_id+'"><i class="fa fa-pencil edit" title="edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-project_scope_id="'+data.project_scope_id+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    $(document).on('click', '.btn-delete', function() {
        var id = $(this).attr('data-project_scope_id');
        sWarning('Are you sure you want to remove this item?',function(){
            $.ajax({
                url: '{{url('inter-office-memo/delete')}}/'+id,
                method: "delete",
                data: { _token : _token, _method : 'delete' }, // serializes all the form data
                success : function(data) {
                    location.reload();
                }
            });
        })
        return false;
    });
</script>
@endsection