@extends('layouts.app')
@section('styles')
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')
        <div class="page-content">
            @include('includes.content_header')
            @include('includes.alerts')
            <form class="form-horizontal" id="form-memo" method="POST" action="{{ isset($details) ? route('inter_office_memo.update',[ 'id' => $details->project_scope_id ]) : route('inter_office_memo.store')}}">
                {{ csrf_field() }}
                <div class="row">
                	<div class="col-xs-12 mrgB15">
                        <div class="table-header"> Inter Office Memo </div>
                    </div>

                    <div class="col-xs-6 text-left" for="description"> 
                        @if(isset($details))
                        <input type="hidden" name="_method" value="PUT">
                        <a href="#" class="btn btn-info btn-sm">
                            <i class="ace-icon fa fa-eye bigger-120"></i>
                            View
                        </a>
                        <a href="#" class="btn btn-success btn-sm">
                            <i class="ace-icon fa fa-send bigger-120"></i>
                            Send
                        </a>
                        @endif
                    </div>
                    <div class="col-xs-6 text-right" for="description"> 
                        @if(isset($details))
                        <a href="{{ route('inter_office_memo.create')}}" class="btn btn-success btn-sm">
                            <i class="ace-icon fa fa-plus bigger-120"></i>
                            New
                        </a>
                        @endif
                        <button class="btn btn-info btn-sm"><i class="ace-icon fa fa-save bigger-120"></i>
                            Apply</button>
                        <a href="{{ route('inter_office_memo')}}" class="btn btn-warning btn-sm">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Cancel
                        </a>
                    </div>
                    <div class="col-xs-12 ">
                        <div class="form-group" >
                            <label class="col-sm-2 control-label align-left" for="start_date"> Audit Start Date </label>
                            <div class="col-sm-4">
                                <input type="text" name="start_date" id="start_date" class="form-control input-sm date-picker" value="{{ isset($details) ? $details->start_date : '' }}" data-date-format="dd-M-yyyy">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label align-left" for="subject"> Subject </label>
                            <div class="col-sm-4">
                                <input type="text" name="subject" id="subject" class="form-control input-sm" value="{{ isset($details) ? $details->subject : '' }}" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label align-left" for="introduction"> Introduction </label>
                            <div class="col-sm-4">
                                <input type="text" name="introduction" id="introduction" class="form-control input-sm" value="{{ isset($details) ? $details->introduction : '' }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label align-left" for="background"> Background </label>
                            <div class="col-sm-10">
                                <textarea name="background" id="background" class="form-control input-sm h-only" rows="3" >{{ isset($details) ? $details->background : '' }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label align-left" for="objectives"> Objectives </label>
                            <div class="col-sm-10">
                                <textarea name="objectives" id="objectives" class="form-control input-sm h-only" rows="3" >{{ isset($details) ? $details->objectives : '' }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label align-left" for="auditee_assistance"> Auditee Assistance </label>
                            <div class="col-sm-10">
                                <textarea name="auditee_assistance" id="auditee_assistance" class="form-control input-sm h-only" rows="3" >{{ isset($details) ? $details->auditee_assistance : '' }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label align-left" for="staffing"> Staffing </label>
                            <div class="col-sm-10">
                                <textarea name="staffing" id="staffing" class="form-control input-sm h-only" rows="3" >{{ isset($details) ? $details->staffing : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                    @if(isset($details))
                    <div class="col-xs-12 mrgB15">
                        <div class="table-header"> Auditee Distribution List </div>
                    </div>
                    <div class="col-xs-12">
                        <a class="btn btn-primary btn-sm btn-create" data-toggle="modal" href='#additional-info-modal'>
                            <i class="ace-icon fa fa-plus bigger-120"></i>
                            Create
                        </a>
                    </div>
                    <div class="col-xs-12 mrgB15">
                        <table id="additional-info-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th >Auditee Name</th>
                                    <th >Position</th>
                                    <th >Distribution Type</th>
                                    <th class="text-center" width="10%">Delete</th>
                                </tr>
                            </thead>

                            <tbody>
                                @for($i = 0; $i < 10;$i++)
                                <tr>
                                    <td> Sample Data</td>
                                    <td> Sample Data</td>
                                    <td> Sample Data</td>
                                    <td class="text-center">
                                        <a href="#" class="btn-delete delete" title="delete" rel="tooltip">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12 mrgB15">
                        <div class="table-header"> Auditors </div>
                    </div>
                    <div class="col-xs-6 mrgB15">
                        <table id="additional-info-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th >Auditor Name</th>
                                    <th >Auditor Type</th>
                                </tr>
                            </thead>

                            <tbody>
                                @for($i = 0; $i < 10;$i++)
                                <tr>
                                    <td> Sample Data</td>
                                    <td> Sample Data</td>
                                </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                    @endif
                    <div class="col-xs-12 text-right" for="description"> 
                        <button class="btn btn-info btn-sm"><i class="ace-icon fa fa-save bigger-120"></i>
                            Apply</button>
                        <a href="{{ route('inter_office_memo')}}" class="btn btn-warning btn-sm">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Cancel
                        </a>
                    </div>
                </div>
            </form>
		</div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@include('engagement_planning.includes.annual_audit_plan_projects_modal')
@endsection
@section('footer_script')
<script src="/js/date-time/bootstrap-datepicker.js"></script>
<script>

    $("#form-memo").submit(function(){
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                // console.log(response);
                // location.href = response.link;
            }
        });
        return false;
    });
</script>
@endsection