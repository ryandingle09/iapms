<!DOCTYPE html>
<html>
<head>
	<title>&nbsp;</title>
	<link rel="stylesheet" href="{{ asset('css/pdf.css') }}" />
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="text-center box col-xs-12">INTERNAL AUDIT DIVISION</div>
			<div class="box col-xs-12">
				<table class="table1">
					<tr>
						<td style="width:11%;"> 
							<div class="labelbox"> MEMO TO</div>	
						</td>
						<td style="width:55%;">
							<div class="box">
							@foreach($dist_list as $dist)
							{{ $dist->auditee->first_name }} {{ $dist->auditee->last_name }},
							@endforeach
						</div>
						</td>
						<td style="width:10%;">
							<div class="labelbox">REF. NO</div>	
						</td>
						<td style="width:15%;">
							<div class="box">
							{{ $iom->ref_no }}
						</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="labelbox"> SUBJECT</div>
						</td>
						<td>
							<div class="box">
								{{ $iom->subject }}
							</div>
						</td>
						<td>
							<div class="labelbox">DATE:</div>
						</td>
						<td>
							<div class="box">
								{{ $iom->start_date }}
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div class="box col-xs-12">
				<table>
					<tr >
						<td colspan="2">
							<div class="box minh100">
								{{ $iom->introduction }}
							</div>
						</td>
					</tr>
					<tr>
						<td style="width:20%;">
							<div class="labelbox">BACKGROUND</div>
						</td>
						<td style="width:80%;">
							<div class="box minh100">
								{{ $iom->background }}
							</div>
						</td>
					</tr>
					<tr>
						<td style="width:20%;">
							<div class="labelbox">OBJECTIVES</div>
						</td>
						<td style="width:80%;">
							<div class="box minh100">
								{{ $iom->objectives }}
							</div>
						</td>
					</tr>
					<tr>
						<td style="width:20%;">
							<div class="labelbox">AUDITEE ASSISTANCE</div>
						</td>
						<td style="width:80%;">
							<div class="box minh100">
								{{ $iom->auditee_assistance }}
							</div>
						</td>
					</tr>
					<tr>
						<td style="width:20%;">
							<div class="labelbox">STAFFING</div>
						</td>
						<td style="width:80%;">
							<div class="box minh100">
								{{ $iom->staffing }}
							</div>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td style="width:35%;">
							<div class="box"> 
								@if(isset($head))
								{{ $head->first_name }} {{ $head->last_name }}<br>
								{{ $head->position_code  }}
								@endif
							</div>
						</td>
						<td style="width:65%;"></td>
					</tr>
				</table>
				<table>
					<tr> 
						<td><div class="labelbox">  Distribution List :</div></td>
						<td><div class="labelbox"> CC:</div>  </td>
					</tr>
					<tr>
						<td>
							<div class="box minh100">
								@foreach($dist_list as $dist)
								{{ $dist->auditee->first_name }} {{ $dist->auditee->last_name }} <br>
								@endforeach
							</div>
						</td>
						<td>
							<div class="box minh100">
								@foreach($dist_list as $dist)
								{{ $dist->auditee->first_name }} {{ $dist->auditee->last_name }} <br>
								@endforeach
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
		
	</div>

</body>
</html>