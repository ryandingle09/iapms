<!DOCTYPE html>
<html>
<head>
	<title>&nbsp;</title>
	<link rel="stylesheet" href="{{ asset('css/bootstrap-grid.css') }}" />
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="text-center box col-xs-12">INTERNAL AUDIT DIVISION</div>
			<div class="box col-xs-12">
				<div class="col-xs-12">
					<div class="col-xs-2 labelbox"> MEMO TO</div>
					<div class="col-xs-5 box">
						@foreach($dist_list as $dist)
						{{ $dist->auditee->full_name }},
						@endforeach
					</div>
					<div class="col-xs-2 labelbox">REF. NO:</div>
					<div class="col-xs-3 box">
						{{ $iom->ref_no }}
					</div>
				</div>
				<div class="col-xs-12">
					<div class="col-xs-2 labelbox"> SUBJECT</div>
					<div class="col-xs-5 box">
						{{ $iom->subject }}
					</div>
					<div class="col-xs-2 labelbox">DATE:</div>
					<div class="col-xs-3 box">
						{{ $iom->start_date }}
					</div>
				</div>
			</div>
			<div class="box col-xs-12">
				<div class="col-xs-12 box minh80 mgrB15">
					{{ $iom->introduction }}
				</div>
				<div class="mgrB15 clearfix">
					<div class="col-xs-2 labelbox">BACKGROUND</div>
					<div class="col-xs-10 box minh80">
						{{ $iom->background }}
					</div>
				</div>
				<div class="mgrB15 clearfix">
					<div class="col-xs-2 labelbox">OBJECTIVES</div>
					<div class="col-xs-10 box minh80">
						{{ $iom->objectives }}
					</div>
				</div>
				<div class="mgrB15 clearfix">
					<div class="col-xs-2 labelbox">AUDITEE ASSISTANCE</div>
					<div class="col-xs-10 box minh80">
						{{ $iom->auditee_assistance }}
					</div>
				</div>
				<div class="mgrB15 clearfix">
					<div class="col-xs-2 labelbox">STAFFING</div>
					<div class="col-xs-10 box minh80">
						{{ $iom->staffing }}
					</div>
				</div>
				<div class="col-xs-12 mgrB15">
					<div class="row">
						<div class="box col-xs-4"> 
							@if(isset($head))
							{{ $head->full_name  }}
							@endif
						</div>
					</div>
					<div class="row">
						<div class="box col-xs-4"> 
							@if(isset($head))
							{{ $head->position_code  }}
							@endif
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-6">Distribution List :</div>
						<div class="col-xs-6">CC: </div>
					</div>
					<div class="row">
						<div class="box col-xs-6 minh80">
							@foreach($dist_list as $dist)
							{{ $dist->auditee->full_name }} <br>
							@endforeach
						</div>
						<div class="box col-xs-6 minh80">
							@foreach($dist_list as $dist)
							{{ $dist->auditee->full_name }} <br>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>

</body>
</html>