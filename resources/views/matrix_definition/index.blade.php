@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('matrix_definition.create') }}">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Matrix
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="matrix-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="25%">Matrix Name</th>
                                <th width="35%">Description</th>
                                <th width="15%">X Dimension</th>
                                <th width="15%">Y Dimension</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var $alert  = $('.alert'),
        form    = $('#matrix-form'),
        $modal  = $('#matrix-modal');
    var datatable= $('#matrix-table').DataTable( {
        ajax: "{{route('matrix_definition.list')}}",
        "processing": true,
        orderCellsTop: true,
        columns: [
            { data: "matrix_name" },
            { data: "description", defaultContent: 'N/A', orderable: false },
            { data: "x_dimension" },
            { data: "y_dimension" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('matrix_definition/edit')}}/'+data.matrix_id+'"><i class="fa fa-pencil edit" title="Edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.matrix_id+'" data-name="'+data.matrix_name+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    // remove auditee
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var remove_name = $(this).attr('data-name');

        swal({
            title: "Continue?",
            text: "You are about to delete "+remove_name+" matrix definition.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function() {
            $.ajax({
                url: '{{ url('matrix_definition/delete') }}/'+remove_id,
                method: "POST",
                data: { _token : _token, _method : 'delete' }, // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        swal({
                            title: "Success!",
                            text: "Redirecting. Please wait...",
                            type: 'success',
                            showConfirmButton: false
                        });
                        location.reload();
                    }
                }
            });
        });

        return false;
    });
</script>
@endsection
