@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <style type="text/css">
        .editable {
            cursor:cell;
        }
        .matrix-graph, .matrix-graph-title {
            width:100%;
            cursor:cell;
        }
        .matrix-graph:hover, .matrix-graph-title:hover {
            background-color: #76c8ff;
        }
    </style>
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Matrix Definition Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm btn-save" type="button">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button>
                                            <a href="{{route('matrix_definition')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="table-header"> Matrix Details </div>
                                        <div style="display:none;" class="alert alert-warning alert-main"></div>
                                        {!! Form::open(['url' => !isset($details) ? route('matrix_definition.store') : route('matrix_definition.update', ['id' => $details->matrix_id]), 'class' => 'form-horizontal', 'id' => 'matrix-form']) !!}
                                            @if(isset($details))
                                                <input type="hidden" name="_method" value="put">
                                                <input type="hidden" name="id" value="{{$details->matrix_id}}">
                                            @endif
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="matrix_name"> Matrix Name </label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="basic-info form-control" id="matrix_name" name="matrix_name" value="{{ isset($details) ? $details->matrix_name : ''}}" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="description"> Description </label>
                                                <div class="col-sm-6">
                                                    <textarea class="basic-info form-control" id="description" name="description" rows="3" style="resize: none;">{{ isset($details) ? $details->description : ''}}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="x"> X Dimensions </label>
                                                <div class="col-sm-2">
                                                    <input type="number" step="1" min="1" class="basic-info form-control" id="x" name="x" value="{{ isset($details) ? $details->x_dimension : ''}}" required>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="y"> Y Dimensions </label>
                                                <div class="col-sm-2">
                                                    <input type="number" step="1" min="1" class="basic-info form-control" id="y" name="y" value="{{ isset($details) ? $details->y_dimension : ''}}" required>
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                    @if(isset($details))
                                    <?php
                                        $col_default = 3;
                                        $col_max = 12;
                                        $col = $details->x_dimension - 5;
                                        if( $col < 2 ) $col = $col_default;
                                        if( $col > $col_max ) $col = $col_max;
                                    ?>
                                    <div class="col-xs-{{$col}}">
                                        <div class="table-header"> Matrix Graph </div>
                                        <div style="width: 100%;overflow-x: auto;">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td valign="middle" align="center" width="8%" style="font-size: 20px;">Y</td>
                                                    <td width="92%">
                                                        <table id="matrix-graph-table" class="table table-striped table-bordered table-hover" style="margin-bottom: 5px !important;">
                                                            <tbody>
                                                                <?php krsort($matrix_table); $x_axis = []; ?>
                                                                @foreach($matrix_table as $y => $x)
                                                                    <?php
                                                                        $label_y = $x[1]['label_y'] != '' ? $x[1]['label_y'] : $y;
                                                                        $width = (100 / ($details->x_dimension + 1));
                                                                    ?>
                                                                    <tr align="center">
                                                                        <th style="text-align: center;width:{{$width}}%;" class="editable-graph inline-edit" data-type="y-value" data-value="{{$label_y}}" id="y-value_{{$y}}">
                                                                            <div class="matrix-graph-title">
                                                                                <span class="matrix-span">{{ $label_y }}</span>
                                                                            </div>
                                                                        </th>
                                                                    @foreach($x as $i => $axis)
                                                                        <?php $x_axis[] = $axis['label_x'] != '' ? $axis['label_x'] : $i; ?>
                                                                        <td id="graph_{{$y}}_{{$i}}" class="editable-graph" data-type="matrix-value" style="text-align: center;{!! !is_null($axis['color']) ? 'background-color: '.$axis['color'].';' : 'inherit;' !!}width:{{$width}}%;">
                                                                            <div class="matrix-graph" rel="popover" data-axis="{{$y}}_{{$i}}" data-value="{{$axis['value']}}" data-color="{{$axis['color']}}">
                                                                                <span class="matrix-span">{!! !is_null($axis['value']) ? $axis['value'] : '&nbsp;' !!}</span>
                                                                            </div>
                                                                        </td>
                                                                    @endforeach
                                                                    </tr>
                                                                @endforeach
                                                                <tr>
                                                                    <th  style="text-align: center;"></th>
                                                                    <?php
                                                                        $x_axis = array_unique($x_axis);
                                                                        sort($x_axis);
                                                                    ?>
                                                                    @for($i=0; $i < count($x_axis); $i++)
                                                                        <th  style="text-align: center;" class="editable-graph inline-edit" data-type="x-value" data-value="{{$x_axis[$i]}}" id="x-value_{{$x_axis[$i]}}">
                                                                            <div class="matrix-graph-title">
                                                                                <span class="matrix-span">{{ $x_axis[$i] }}</span>
                                                                            </div>
                                                                        </th>
                                                                    @endfor
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center" style="font-size: 20px;">X</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <br/>
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm btn-save" type="button">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button>
                                            <a href="{{route('matrix_definition')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<script type="text/template" id="text-template">
    <input type="text" name="" class="form-control input-sm" value="">
</script>
<script type="text/template" id="color-template">
    {!! Form::select('size', ['#00ca00' => 'Green', 'yellow' => 'Yellow', 'orange' => 'Orange', '#ff6666' => 'Red'], null, ['class' => 'form-control']) !!}
</script>
<script type="text/template" id="loader-template">
    <span style="color: #3691ec;"><i class="fa fa-spin fa-cog bigger-120"></i></span>
</script>
<script type="text/template" id="form-template">
    {!! Form::open(['url' => '', 'class' => 'form-horizontal', 'id' => 'matrix-details-form', 'onsubmit' => 'javascript:void(0);']) !!}
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="x" value="">
        <input type="hidden" name="y" value="">
        <div class="form-group">
            <label class="col-sm-5 control-label" for="description"> Matrix Value </label>
            <div class="col-sm-6">
                <input type="number" name="matrix" class="form-control input-sm" value="">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label" for="description"> Color </label>
            <div class="col-sm-6">
                {!! Form::select('color', ['' => '------', '#47dc47' => 'Green', 'yellow' => 'Yellow', 'orange' => 'Orange', '#ff6666' => 'Red'], null, ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12" style="text-align: center;">
                <button type="button" class="btn btn-primary btn-md btn-save-matrix">Ok</button>
                <button type="button" class="btn btn-warning btn-md btn-cancel-matrix">Cancel</button>
            </div>
        </div>
    </form>
</script>
@endsection

@section('footer_script')
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
        var form        = $('#matrix-form'),
            code        = $('#bp_code'),
            obj_type    = $('#obj_type'),
            obj_cat     = $('#obj_cat'),
            $alert      = $('.alert-main'),
            $modal      = $('#bpstep-modal');

        var impact,
            likelihood,
            curr_coord;
        @if(isset($details))
        $(document).ready(function() {
            $(".matrix-graph").popover({
                html: true,
                content: $('#form-template').html(),
                placement: "auto"
            });
        });

        $('body').on('click', function (e) {
            $('[rel="popover"]').each(function () {
                //the 'is' for buttons that trigger popups
                //the 'has' for icons within a button that triggers a popup
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    $(this).popover('hide');
                }
            });
        });

        $('body').on('click', '.editable-graph', function (e) {
            var td = $(this);

            if( $(this).hasClass('inline-edit') ) {
                var input = $('#text-template').html();
                var data_type = td.attr('data-type');
                var data_value = td.attr('data-value');

                td.html('').append(input);
                td.find('.form-control')
                .prop('name', data_type)
                .attr('data-orig', data_value)
                .val(data_value)
                .focus();

                td.find('.form-control').on('blur keyup', function(e){
                    var orig_val = $(this).attr('data-orig');
                    if (e.keyCode === 27) {
                        td.html(orig_val);
                        $(this).remove();

                        return false;
                    }

                    if (e.type == 'blur' || e.keyCode == '13') {
                        td.html($('#loader-template').html());
                        var new_val = $(this).val();

                        if( orig_val != new_val ) {
                            td.html(new_val);
                            $.ajax({
                                url: '{{url('matrix_definition/update')}}/{{$details->matrix_id}}/details',
                                method: "POST",
                                data: { _token : '{{csrf_token()}}', _method : 'put', axis : td.attr('data-value'), type : td.attr('data-type'), value : new_val }, // serializes all the form data
                                success : function(data) {
                                    swal("Saved!", "Matrix details is successfully updated.", "success");
                                }
                            });
                        }
                        else {
                            // show original value if there's no changes made
                            td.html(orig_val);
                        }
                        // remove input element
                        $(this).remove();

                        // Set td background color based on the user input
                        if( data_type == 'color' && td.html() != '' ) {
                            td.css('background-color', td.html());
                        }
                    }
                });

                td.find('select').on('change', function(){
                    $(this).blur();
                });
            }
        });

        function initTables(coordinates, matrix, color, x_val, y_val) {
            var ids = coordinates.split('_'); // [0] => Y | [1] => X
            if(y_val != undefined && y_val != '' ) $('#y-value_'+ids[0]).html(y_val);
            if( x_val != undefined && x_val != '' ) $('#x-value_'+ids[1]).html(x_val);

            $('#graph_'+coordinates+' div.matrix-graph').attr('data-value', matrix);
            $('#graph_'+coordinates+' div.matrix-graph').attr('data-color', color);
            $('#graph_'+coordinates).css('background-color', color);
            $('#graph_'+coordinates+' .matrix-span').html(matrix == '' ? 0 : matrix);

            $('[rel="popover"]').popover('hide');
        }

        function save() {
            $.ajax({
                url: $('#matrix-details-form').attr('action'),
                method: "POST",
                data: $('#matrix-details-form').serialize(), // serializes all the form data
                success : function(data) {
                    swal("Saved!", "Risk details is successfully updated.", "success");
                }
            });
        }

        $(document).on('click', '.btn-save-matrix', function() {
            var form = $(this).closest('form');
            var matrix = form.find('input[name="matrix"]').val();
            var color = form.find('select[name="color"]').val();

            $('#graph_'+curr_coord).css('background-color', color);
            $('#graph_'+curr_coord+' .matrix-span').html(matrix);

            save();

            initTables(curr_coord, matrix, color);
        });

        // Set current coordinates
        $(document).on('click', '[rel="popover"]', function() {
            curr_coord = $(this).attr('data-axis');
            var val = $(this).attr('data-value');
            var color = $(this).attr('data-color');
            var x_y = curr_coord.split('_'); // [0] => Y | [1] => X
            $('#matrix-details-form').find('input[name="matrix"]').val(val);
            $('#matrix-details-form').find('select[name="color"]').val(color);
            $('#matrix-details-form').find('input[name="x"]').val(x_y[1]);
            $('#matrix-details-form').find('input[name="y"]').val(x_y[0]);
            $('#matrix-details-form').attr('action', '{{url('matrix_definition/update')}}/{{$details->matrix_id}}/details');
        });

        $(document).on('click', '.btn-cancel-matrix', function() {
            $('[rel="popover"]').popover('hide');
        });
        @endif
        // save the form through AJAX call
        $(document).on('click', '.btn-save', function() {
            swal({
                title: "Continue saving?",
                text: "You are about to add a matrix definition.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, save it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function() {
                $.ajax({
                    url: form.attr('action'), // url based on the form action
                    method: "POST",
                    data: form.serialize(), // serializes all the form data
                    success : function(data) {
                        if( data.success ) {
                            swal({
                                title: "Success!",
                                text: "Redirecting. Please wait...",
                                type: 'success',
                                showConfirmButton: false
                            });
                            if( data.redirect != undefined ) window.location = data.redirect;
                            else location.reload();
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal.close();
                        if( xhr.status == 500 ) {
                            $alert.removeClass('alert-warning').addClass('alert-danger');
                            $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                        }
                        else {
                            // displays the validation error
                            var unique_message = [];
                            var msg = '<ul class="list-unstyled">';

                            $('.basic-info').parent().parent().removeClass('has-error');

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    // set error class to fields
                                    $('#'+key).parent().parent().addClass('has-error');

                                    // shows error message
                                    if( unique_message.indexOf(val[i]) === -1 ) {
                                        msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                        unique_message.push(val[i]);
                                    }
                                }
                            });
                            msg += '</ul>';

                            $alert.removeClass('alert-danger').addClass('alert-warning');
                            $alert.html(msg).show();
                        }
                    }
                });
            });
        });

        // initialize modal fields
        function init_modal( sequence, activity_narrative, id ) {
            if( id != '' ) {
                // update setting
                $modal.find('input[name="id"]').val(id);
                $modal.find('form').attr('action', '{{ url('business_process/steps/update') }}/'+id)
                                      .find('input[name="_method"]').val('put');
            }
            else {
                // create setting
                $modal.find('input[name="id"]').val('');
            }
            $modal.find('input#sequence').val(sequence);
            $modal.find('#activity_narrative').val(activity_narrative);

            $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
            $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');
        }

    </script>
@endsection
