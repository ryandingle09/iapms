@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="/plugins/fancytree/skin-xp/ui.fancytree.min.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.bootstrap3.css" />
    <link rel="stylesheet" href="/css/selectize-custom.css" />
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Risks
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                {{ (isset($details)) ? 'Save' : 'Save &amp; proceed to controls' }}
                                            </button>
                                            <a href="{{route('risks')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="table-header"> Risks </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div style="display:none;" class="alert alert-warning alert-main"></div>
                                        {!! Form::open(['url' => isset($details) ? route('risks.update', ['risk_id' => $details->risk_id]) : route('risks.store'), 'class' => 'form-horizontal', 'id' => 'controls-form', 'style' => 'margin-top: 12px;']) !!}
                                            @if(isset($details))
                                                <input type="hidden" name="_method" value="PUT">
                                                <input type="hidden" name="risk" value="{{ $details->risk_code }}">
                                            @endif
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="risk"> Risk Name </label>
                                                    <div class="col-sm-8">
                                                        @if(isset($details))
                                                            <span class="label label-primary">{{ $details->lv_risk_code_meaning }}</span>
                                                        @else
                                                            <select id="risk" name="risk" class="form-control input-sm basic-info lookup" placeholder="Select a risk"></select>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="risk"> Risk Description </label>
                                                    <div class="col-xs-8" id="risk_desc">
                                                        <span class="label label-info label-risk label-wrap">{{ (isset($details)) ? $details->lv_risk_code_desc : 'N/A' }}</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="risk_type"> Risk Type </label>
                                                    <div class="col-sm-8">
                                                        <select id="risk_type" name="risk_type" class="form-control input-sm basic-info select2-drop" data-placeholder="Click to Choose...">
                                                            @if(isset($details))
                                                            <option value="{{ $details->risk_type }}" selected>{{ $details->risk_type }}</option>
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="risk_remarks"> Risk Remarks </label>
                                                    <div class="col-xs-8">
                                                        <textarea id="risk_remarks" name="risk_remarks" class="form-control" rows="3" style="resize: none;">{{ (isset($details)) ? $details->risk_remarks : '' }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="source_type"> Source Type </label>
                                                    <div class="col-sm-8">
                                                        <select id="source_type" name="source_type" class="form-control input-sm basic-info select2-drop" placeholder="Select a source type"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="response_type"> Response Type </label>
                                                    <div class="col-sm-8">
                                                        <select id="response_type" name="response_type" class="form-control input-sm basic-info select2-drop" placeholder="Select a response type"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for=""> &nbsp; </label>
                                                    <div class="col-sm-3"  style="text-align: center;">
                                                        <span class="label label-info arrowed-in arrowed-in-right">Impact</span>
                                                    </div>
                                                    <div class="col-sm-4"  style="text-align: center;">
                                                        <span class="label label-info arrowed-in arrowed-in-right">Likelihood</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="risk_type"> Default Inherent Rating </label>
                                                    <div class="col-sm-4">
                                                        <input type="hidden" id="def_in_rating_impact" name="def_in_rating_impact" value="{{ (isset($details)) ? $details->def_inherent_impact_rate : 0 }}">
                                                        <div class="rating" data-id="def_in_rating_impact"></div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="hidden" id="def_in_rating_likelihood" name="def_in_rating_likelihood" value="{{ (isset($details)) ? $details->def_inherent_likelihood_rate : 0 }}">
                                                        <div class="rating" data-id="def_in_rating_likelihood"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="risk_type"> Default Residual Rating </label>
                                                    <div class="col-sm-4">
                                                        <input type="hidden" id="def_res_rating_impact" name="def_res_rating_impact" value="{{ (isset($details)) ? $details->def_residual_impact_rate : 0 }}">
                                                        <div class="rating" data-id="def_res_rating_impact"></div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="hidden" id="def_res_rating_likelihood" name="def_res_rating_likelihood" value="{{ (isset($details)) ? $details->def_residual_likelihood_rate : 0 }}">
                                                        <div class="rating" data-id="def_res_rating_likelihood"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label" for="rating_comment"> Default Rating Remarks </label>
                                                    <div class="col-sm-8">
                                                        <textarea id="rating_comment" name="rating_comment" class="form-control" rows="3" style="resize: none;">{{ (isset($details)) ? $details->rating_comment : '' }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="enterprise_risk_wrapper" style="{{ isset($details) ? ($details->risk_source_type != 'Enterprise' ? 'display: none;' : '') : 'display: none;' }}">
                                                @include('risks.enterprise_risk')
                                            </div>

                                            @if(isset($details))
                                            <div class="col-xs-12">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-primary btn-sm btn-add-info" data-id="{{$details->risk_id}}"><i class="fa fa-list-ul"></i> Additional Info</button>
                                                </div>
                                            </div>
                                            @endif
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                @if(isset($details))
                                <br/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#riskscontrol-form">
                                            <i class="ace-icon fa fa-plus bigger-120"></i>
                                            Add
                                        </button>
                                        <div class="table-header"> Risk - Control </div>
                                        <table id="risks-table" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="8%">Sequence</th>
                                                    <th width="15%">Control Code</th>
                                                    <th width="20%">Control Name</th>
                                                    <th>Control Description</th>
                                                    <th width="8%"></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @endif
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                {{ (isset($details)) ? 'Save' : 'Save &amp; proceed to controls' }}
                                            </button>
                                            <a href="{{route('risks')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<!-- Control modal -->
@if(isset($details))
<div class="modal fade" data-backdrop="static" id="riskscontrol-form">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Risks Control Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning modal-alert"></div>
                {!! Form::open(['method' => 'post', 'url' => route('risks.control.store', ['risk_id' => $details->risk_id]), 'class' => 'form-horizontal', 'id' => 'controlsmodal-form']) !!}
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="id" value="">
                    <input type="hidden" name="risk_id" value="{{ isset($details) ? $details->risk_id : '' }}">
                    <div class="form-group">
                        <label class="col-sm-4 control-label modal-input" for="control_seq"> Sequence </label>
                        <div class="col-sm-3">
                            <input type="number" required class="form-control input-sm" id="control_seq" name="control_seq" value="" min="1" step="1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label modal-input" for="controls"> Controls </label>

                        <div class="col-xs-8">
                            <select id="control_code" name="controls" required class="form-control input-sm basic-info" placeholder="Select a control"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" for="risk"> Control Description </label>
                        <div class="col-xs-8">
                            <span class="label label-info label-control label-wrap" id="control-code-desc">N/A</span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-controlsmodal-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
{{--COSO Modal--}}
@include('controls.coso_tree_modal')
@include('includes.modals.additional_information')
@endif
<!-- /Control modal -->
@endsection

@section('footer_script')
    <script src="/js/iapms.js"></script>
    <script src="/js/jquery.raty.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="{{asset('plugins/selectize/selectize.js')}}"></script>
    <script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
    <script src="/js/date-time/bootstrap-datepicker.js"></script>
    <script src="/js/jquery.validate.js"></script>

    <script type="text/javascript">
        var form        = $('#controls-form'),
            controls    = $('#controls'),
            risks       = $('#risk'),
            risk_type   = $('#risk_type'),
            source_type = $('#source_type'),
            response_type = $('#response_type'),
            ent_control_code = $('#ent_control_code'),
            ent_risk_type = $('#ent_risk_type'),
            ent_risk_class = $('#ent_risk_class'),
            ent_risk_area = $('#ent_risk_area'),
            $alert       = $('.alert-main'),
            $modal      = $('#riskscontrol-form');

        var $datatable,
            dataSet = [];
        var lookup_url = '{{url('/administrator/lookup/type')}}';
        var $select_risk, select_risk;
        var $select_risk_type, select_risk_type;
        var $select_source_type, select_source_type;
        var $select_response_type, select_response_type;
        var $select_ent_risk, select_ent_risk;
        var select_control, $select_control;
        var coso_components = [];
        var coso_data = {!! isset($coso_components) && !is_null($coso_components['data']) ? json_encode($coso_components['data']) : json_encode([]) !!};

        $(function(){
            // selectize
            @if(!isset($details))
            $select_risk = risks.selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: 'description',
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.text) + '</span>' +
                            '<span class="by"> ' + escape(item.description) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.risk.risks') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    $('#risk_desc span').html(select_risk.options[value].description);
                }
            });
            select_risk = $select_risk[0].selectize;
            @endif
            $select_risk_type = risk_type.selectize({
                valueField: 'meaning',
                labelField: 'meaning',
                searchField: 'meaning',
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.risk.type') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                }
            });
            select_risk_type = $select_risk_type[0].selectize;

            $select_source_type = source_type.selectize({
                valueField: 'meaning',
                labelField: 'meaning',
                searchField: 'meaning',
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.risk.source') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    if( value == 'Enterprise' )
                        $('#enterprise_risk_wrapper').show();
                    else {
                        $('#enterprise_risk_wrapper').hide();
                        $('.ent-risk-info').html('');
                        $('#ent_risk_doc').find('span').html('');
                        $('#ent_risk_doc').hide().attr('href', '#');
                        $('[data-id="ent_def_in_rating_impact"]').raty('set', { score: 0 });
                        $('[data-id="ent_def_in_rating_likelihood"]').raty('set', { score: 0 });
                        $('[data-id="ent_def_res_rating_impact"]').raty('set', { score: 0 });
                        $('[data-id="ent_def_res_rating_likelihood"]').raty('set', { score: 0 });
                        select_ent_risk.clearOptions();
                    }
                },
                onLoad: function(data) {
                    select_source_type.setValue('{{ isset($details) ? $details->risk_source_type : 'Risk' }}');
                }
            });
            select_source_type = $select_source_type[0].selectize;

            $select_response_type = response_type.selectize({
                valueField: 'meaning',
                labelField: 'meaning',
                searchField: 'meaning',
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.risk.response') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                }
                @if(isset($details))
                ,onLoad: function(data) {
                    select_response_type.setValue('{{ $details->risk_response_type }}');
                }
                @endif
            });
            select_response_type = $select_response_type[0].selectize;

            $select_ent_risk = $('#enterprise_risk_id').selectize({
                valueField: 'enterprise_risk_det_id',
                labelField: 'risk_name',
                searchField: ['risk_name'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.risk_name) + '</span>' +
                            '<span class="by"> ' + (item.risk_desc ? escape(item.risk_desc) : 'N/A') + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: '{{ route('enterprise_risk.details.list', ['id' => 'all']) }}?format=json&exclude_assigned=true&current={{isset($details) && !is_null($details->enterpriseRisk) ? $details->enterpriseRisk->risk_name : ''}}',
                        type: 'GET',
                        success: function(res) {
                            callback(res);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    var ent_risk_data = select_ent_risk.options[value];

                    $('#enterprise_risk_desc span').html(ent_risk_data.risk_desc);
                    $('#enterprise_risk_remarks span').html(ent_risk_data.risk_remarks);
                    $('#ent_risk_type span').html(ent_risk_data.risk_type);
                    $('#ent_risk_type_desc span').html(ent_risk_data.lv_risk_type_desc);
                    $('#ent_risk_class span').html(ent_risk_data.risk_class);
                    $('#ent_risk_class_desc span').html(ent_risk_data.lv_risk_class_desc);
                    $('#ent_risk_area span').html(ent_risk_data.risk_area);
                    $('#ent_risk_area_desc span').html(ent_risk_data.lv_risk_area_desc);
                    $('#ent_risk_response_type span').html(ent_risk_data.response_type);
                    $('#ent_risk_response_type_desc span').html(ent_risk_data.lv_response_type_desc);
                    $('#ent_risk_response_status span').html(ent_risk_data.response_status);
                    $('#ent_risk_response_status_desc span').html(ent_risk_data.lv_response_status_desc);
                    $('#ent_risk_response_comment span').html(ent_risk_data.response_status_comment);
                    $('#ent_risk_rating_remarks span').html(ent_risk_data.rating_comment);
                    if( ent_risk_data.supporting_docs_name != null ) {
                        $('#ent_risk_doc').find('span').html(' - '+ent_risk_data.supporting_docs_name);
                        $('#ent_risk_doc').show()
                        .attr('href', '{{url('/')}}/'+ent_risk_data.supporting_docs_file)
                        .attr('target', '_blank');
                    }

                    $('[data-id="ent_def_in_rating_impact"]').raty('set', { score: ent_risk_data.inherent_impact_rate });
                    $('[data-id="ent_def_in_rating_impact"]').find('.star-on-png').addClass(rates[ent_risk_data.inherent_impact_rate]);
                    $('[data-id="ent_def_in_rating_likelihood"]').raty('set', { score: ent_risk_data.inherent_likelihood_rate });
                    $('[data-id="ent_def_in_rating_likelihood"]').find('.star-on-png').addClass(rates[ent_risk_data.inherent_likelihood_rate]);
                    $('[data-id="ent_def_res_rating_impact"]').raty('set', { score: ent_risk_data.residual_impact_rate });
                    $('[data-id="ent_def_res_rating_impact"]').find('.star-on-png').addClass(rates[ent_risk_data.residual_impact_rate]);
                    $('[data-id="ent_def_res_rating_likelihood"]').raty('set', { score: ent_risk_data.residual_likelihood_rate });
                    $('[data-id="ent_def_res_rating_likelihood"]').find('.star-on-png').addClass(rates[ent_risk_data.residual_likelihood_rate]);

                    $('#ent_risk_rated_by span').html(ent_risk_data.updater.auditee.full_name);
                    $('#ent_risk_rater_company span').html(ent_risk_data.updater.auditee.auditable_entity.auditable_entity_name);
                    $('#ent_risk_rater_company_desc span').html(ent_risk_data.updater.auditee.auditable_entity.lv_company_code_desc);
                    $('#ent_risk_rater_dept span').html(ent_risk_data.updater.auditee.auditable_entity.department_code);
                    $('#ent_risk_rater_dept_desc span').html(ent_risk_data.updater.auditee.auditable_entity.lv_department_code_desc);
                    $('#ent_risk_rater_position span').html(ent_risk_data.updater.auditee.position_code);
                    $('#ent_risk_rater_position_desc span').html(ent_risk_data.updater.auditee.lv_position_code_desc);
                    $('#ent_risk_rated_date span').html(ent_risk_data.rated_date);
                }
                @if(isset($details))
                ,onLoad: function() {
                    @if( !is_null($details->enterpriseRisk) )
                    select_ent_risk.setValue({{$details->enterprise_risk_id}});
                    @endif
                }

                @endif
            });
            select_ent_risk = $select_ent_risk[0].selectize;

            @if(isset($details))
                $select_control = $('#control_code').selectize({
                    valueField: 'control_id',
                    labelField: 'control_name',
                    searchField: 'control_name',
                    options: [],
                    create: false,
                    preload: true,
                    render: {
                        option: function(item, escape) {
                            return '<div>' +
                                '<span class="title">' +
                                '<span class="name">' + escape(item.control_code) + ' </span>' +
                                '<span class="by">' + escape(item.control_name) + '</span>' +
                                '</span>' +
                                '</div>';
                        }
                    },
                    load: function(query, callback) {
                        $.ajax({
                            url: '{{ route('controls_registry.list') }}?response=json',
                            type: 'GET',
                            success: function(res) {
                                callback(res);
                            },
                            error: function() {
                                callback();
                            }
                        });
                    },
                    onChange: function(value) {
                        if (!value.length) return;
                        var sel_control = select_control.options[value];
                        $('#control-code-desc').html(sel_control.lv_control_code_desc || 'N/A');
                    }
                });
                select_control = $select_control[0].selectize;
            @endif
            // end selectize

            // rating by raty
            $('.ent-rating').raty({
                'readOnly' : true,
                'half': false,
                'starType' : 'i',
                hints       : ['1', '2', '3', '4', '5'],
            });
            $('.rating').raty({
                'cancel' : true,
                'half': false,
                'starType' : 'i',
                'starOn' : rate_class,
                'starOff' : 'star-off-png',
                hints       : ['1', '2', '3', '4', '5'],
                'click': function() {
                    setRatingColors.call(this);
                },
                'mouseover': function(e) {
                    setRatingColors.call(this, e, true);
                },
                'mouseout': function(e) {
                    setRatingColors.call(this, e, false);
                },
                click: function(score, evt) {
                    var id = $(this).attr('data-id');
                    $(this).raty('set', {starOn : 'star-on-png '+rates[score]});
                    $('#'+id).val(score);
                }
            });
            $('[data-id="def_in_rating_impact"]').raty('score', $('#def_in_rating_impact').val());
            $('[data-id="def_in_rating_impact"]').find('.star-on-png').addClass(rates[$('#def_in_rating_likelihood').val()]);
            $('[data-id="def_in_rating_likelihood"]').raty('score', $('#def_in_rating_likelihood').val());
            $('[data-id="def_in_rating_likelihood"]').find('.star-on-png').addClass(rates[$('#def_in_rating_likelihood').val()]);
            $('[data-id="def_res_rating_impact"]').raty('score', $('#def_res_rating_impact').val());
            $('[data-id="def_res_rating_impact"]').find('.star-on-png').addClass(rates[$('#def_res_rating_impact').val()]);
            $('[data-id="def_res_rating_likelihood"]').raty('score', $('#def_res_rating_likelihood').val());
            $('[data-id="def_res_rating_likelihood"]').find('.star-on-png').addClass(rates[$('#def_res_rating_likelihood').val()]);

            $('.ent-rating').find('i').attr('rel', 'tooltip');
            $('.rating').find('i').attr('rel', 'tooltip');
        });

        // save the form through AJAX call
        function save() {
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        window.onbeforeunload = null;
                        if( data.redirect !== undefined ) window.location = data.redirect;
                        else window.location = '{{ route('risks') }}';
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        }

        @if(isset($details))
        $datatable = $('#risks-table').DataTable({
            "lengthMenu": [ 25, 50, 75, 100 ],
            "processing": true,
            ajax: "{{route('risks.control', ['risk_id' => $details->risk_id])}}",
            "serverSide": true,
            columns: [
                { data: "risk_control_seq", searchable: false },
                { data: "control_code" },
                { data: "control_name" },
                { data: "lv_control_code_desc", searchable: false },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn-view-coso" data-id="'+data.risk_control_id+'" title="COSO components" rel="tooltip"><i class="fa fa-cubes"></i></a> &nbsp; <a href="#" class="btn-delete delete btn-remove" data-id="'+data.risk_control_id+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        });

        // initialize modal fields
        function init_modal( sequence, control, id ) {
            if( id != '' ) {
                // update setting
                $modal.find('input[name="id"]').val(id);
                $('#controlsmodal-form').attr('action', '{{ url('risks/control/update') }}/'+id)
                                      .find('input[name="_method"]').val('put');
            }
            else {
                // create setting
                $modal.find('input[name="id"]').val('');
                $('#controlsmodal-form').attr('action', '{{route('risks.control.store', ['risk_id' => $details->risk_id])}}')
                                      .find('input[name="_method"]').val('post');
            }
            $modal.find('input#control_seq').val(sequence);
            select_control.clear();
            $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
            $('#controlsmodal-form .form-group').removeClass('has-error').removeClass('has-success');
        }

        $modal.on('hidden.bs.modal', function (e) {
            init_modal('', '', '');
            $modal.find('.label-control').html('N/A');
            $modal.find('.label-component').html('N/A');
            $modal.find('.label-principle').html('N/A');
            $modal.find('.label-focus').html('N/A');
            $modal.find('.modal-alert').html('').hide();
        });

        $modal.on('show.bs.modal', function () {
            $modal.find('#control_seq').val($datatable.rows().data().length + 1);
        });

        function updateDatatable() {
            $datatable.ajax.reload( null, false );
            $modal.modal('hide');
        }

        $(document).on('click', '.btn-controlsmodal-save', function(){
            $('#controlsmodal-form').submit();
        });

        $('#controlsmodal-form').on('submit', function(){
            swal({
                title: "Continue saving?",
                text: "You are about to add a new risk control.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, save it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: $('#controlsmodal-form').attr('action'), // url based on the form action
                    method: "POST",
                    data: $('#controlsmodal-form').serialize(), // serializes all the form data
                    success : function(data) {
                        if( data.success ) {
                            swal("Saved!", "New risk control is successfully created.", "success");
                            updateDatatable();
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal.close();
                        if( xhr.status == 500 ) {
                            $('.modal-alert').removeClass('alert-warning').addClass('alert-danger');
                            $('.modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                        }
                        else {
                            // displays the validation error
                            var unique_message = [];
                            var msg = '<ul class="list-unstyled">';

                            $('.modal-input').parent().parent().removeClass('has-error');

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    // set error class to fields
                                    $('#'+key).parent().parent().addClass('has-error');

                                    // shows error message
                                    if( unique_message.indexOf(val[i]) === -1 ) {
                                        msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                        unique_message.push(val[i]);
                                    }
                                }
                            });
                            msg += '</ul>';

                            $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
                            $('.modal-alert').html(msg).show();
                        }
                    }
                });
            });

            return false;
        });

        // show update form
        $(document).on('click', '.btn-update', function(){
            var control = $(this).attr('data-control_id'),
                id = $(this).attr('data-id'),
                seq = $(this).attr('data-seq');

            // set values
            init_modal(seq, control, id);

            $modal.modal('show');

            return false;
        });

        // remove existing control
        $(document).on('click', '.btn-remove', function() {
            var remove_id = $(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "You are about to remove a risk control.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: '{{url('risks/control/delete')}}/'+remove_id,
                    method: "POST",
                    data: { _method : 'delete', _token : _token },
                    success : function(data) {
                        swal("Deleted!", "Risk control has been deleted.", "success");
                        updateDatatable();
                    }
                });
            });

            return false;
        });

        var ai_modal = $('#additional-information-modal');
        $(document).on('click', '.btn-add-info', function() {
            var id = $(this).attr('data-id');
            iapms.getAddInfoForm(id, 'risks').success(function(data) {
                if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                    ai_modal.find('.btn-save-ai').hide();
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                    ai_modal.find('.btn-save-ai').show();
                }
                ai_modal.modal('show');
                iapms.setAddInfoForm(ai_modal, '{{url('risks')}}/'+id+'/additional_information/update', 'put');

                toggle_loading();
            });

            return false;
        });

        $(function(){
            ai_modal.find('form').validate();
        });

        ai_modal.find('form').on('submit', function(e){
            e.preventDefault();
            if($(this).valid()) {
                swal({
                    title: "Continue?",
                    text: "You are about to update additional information.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, save it!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function(){
                    $.ajax({
                        url: ai_modal.find('form').attr('action'),
                        method: "POST",
                        data: ai_modal.find('form').serialize(), // serializes all the form data
                        success : function(data) {
                            if(data.success) {
                                swal("Success!", "Additional information has been updated.", "success");
                                location.reload();
                            }
                        },
                        error: function(xhr, textStatus, errorThrown){
                            swal.close();
                            // displays the validation error
                            var msg = '<ul class="list-unstyled">';

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                }
                            });
                            msg += '</ul>';

                            ai_modal.find('.alert-modal').html(msg).show();
                        }
                    });
                });
            }

            return false;
        });

        ai_modal.on('click', '.btn-save-ai', function() {
            ai_modal.find('form').submit();
        });
        @endif
    </script>
    @stack('coso_script')
@endsection