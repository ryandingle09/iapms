@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href='{{ route('risks.create') }}'>
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        List
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="risks-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="14%">Risk Name</th>
                                <th width="40%">Description</th>
                                <th width="10%">Risk Type</th>
                                <th width="30%">Remarks</th>
                                <th width="6%"></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@include('includes.modals.additional_information')
@endsection

@section('footer_script')
<script src="/js/iapms.js"></script>
<script src="/js/jquery.validate.js"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script src="/js/date-time/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    var ai_modal = $('#additional-information-modal');
    var datatable= $('#risks-table').DataTable( {
        ajax: "{{route('risks.list')}}",
        processing: true,
        serverSide: true,
        orderCellsTop: true,
        columns: [
            { data: "lv_risk_code_meaning" },
            { data: "lv_risk_code_desc", searchable: false },
            { data: "risk_type" },
            { data: "risk_remarks" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('risks')}}/edit/'+data.risk_id+'" title="Edit" rel="tooltip"><i class="fa fa-pencil edit"></i></a> &nbsp; <a href="javascript:void(0);" title="Additional Info" class="btn-add-info" data-id="'+data.risk_id+'" rel="tooltip"><i class="fa fa-list-ul"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.risk_id+'" data-risk_code="'+data.risk_code+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    // remove auditee
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var remove_name = $(this).attr('data-risk_code');
        swal({
            title: "Are you sure you want to remove?",
            text: "You are about to remove "+remove_name+" risk.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(){
            $.ajax({
                url: '{{ url('risks/delete') }}/'+remove_id,
                method: "POST",
                data: { _token : _token, _method : 'delete' }, // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        swal("Deleted!", "Risk control has been deleted.", "success");
                        datatable.ajax.reload( null, false );
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();
                    // displays the validation error
                    var msg = '<ul class="list-unstyled">';

                    $.each(xhr.responseJSON, function(key, val){
                        for(var i=0; i < val.length; i++) {
                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                        }
                    });
                    msg += '</ul>';

                    $('.alert-warning').html(msg).show();
                }
            });
        });

        return false;
    });

    $(document).on('click', '.btn-add-info', function() {
        var id = $(this).attr('data-id');
        iapms.getAddInfoForm(id, 'risks').success(function(data) {
            if( !data.success ) {
                ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
            }
            else {
                ai_modal.find('#ai_wrapper').html(data.data);
            }
            ai_modal.find('#additional-information-form input').attr('disabled', true);
            ai_modal.find('#additional-information-form select').attr('disabled', true);
            ai_modal.find('.btn-save-ai').hide();
            ai_modal.modal('show');
            iapms.setAddInfoForm(ai_modal, '{{url('risks')}}/'+id+'/additional_information/update', 'put');

            toggle_loading();
        });

        return false;
    });

    $(function(){
        ai_modal.find('form').validate();
    });

    ai_modal.find('form').on('submit', function(e){
        e.preventDefault();
        if($(this).valid()) {
            swal({
                title: "Continue?",
                text: "You are about to update additional information.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, save it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: ai_modal.find('form').attr('action'),
                    method: "POST",
                    data: ai_modal.find('form').serialize(), // serializes all the form data
                    success : function(data) {
                        if(data.success) {
                            swal("Success!", "Additional information has been updated.", "success");
                            location.reload();
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal.close();
                        // displays the validation error
                        var msg = '<ul class="list-unstyled">';

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                            }
                        });
                        msg += '</ul>';

                        ai_modal.find('.alert-modal').html(msg).show();
                    }
                });
            });
        }

        return false;
    });

    ai_modal.on('click', '.btn-save-ai', function() {
        ai_modal.find('form').submit();
    });
</script>
@endsection