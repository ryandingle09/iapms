<div class="col-xs-12">
    <div class="table-header"> Enterprise Risk Details </div>
</div>
<div class="col-xs-12">
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_control_code"> Enterprise Risk Title </label>
        <div class="col-sm-8">
            <select id="enterprise_risk_id" name="enterprise_risk_id" class="form-control input-sm basic-info lookup" placeholder="Select a enterprise risk"></select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_control_code"> Description </label>
        <div class="col-xs-7" id="enterprise_risk_desc">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_control_code"> Remarks </label>
        <div class="col-xs-7" id="enterprise_risk_remarks">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_type"> Risk Type </label>
        <div class="col-xs-7" id="ent_risk_type">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_type_desc"> Risk Type Description </label>
        <div class="col-xs-7" id="ent_risk_type_desc">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_class"> Risk Class </label>
        <div class="col-xs-7" id="ent_risk_class">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_class"> Risk Class Description </label>
        <div class="col-xs-7" id="ent_risk_class_desc">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_area"> Risk Area </label>
        <div class="col-xs-7" id="ent_risk_area">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_area_desc"> Risk Area Description </label>
        <div class="col-xs-7" id="ent_risk_area_desc">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_response_type"> Response Type </label>
        <div class="col-xs-7" id="ent_risk_response_type">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_response_type_desc"> Response Type Description </label>
        <div class="col-xs-7" id="ent_risk_response_type_desc">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_response_status"> Response Status </label>
        <div class="col-xs-7" id="ent_risk_response_status">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_response_status_desc"> Response Status Description </label>
        <div class="col-xs-7" id="ent_risk_response_status_desc">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_response_comment"> Response Comment </label>
        <div class="col-xs-7" id="ent_risk_response_comment">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for=""> &nbsp; </label>
        <div class="col-sm-3"  style="text-align: center;">
            <span class="label label-info arrowed-in arrowed-in-right">Impact</span>
        </div>
        <div class="col-sm-4"  style="text-align: center;">
            <span class="label label-info arrowed-in arrowed-in-right">Likelihood</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="risk_type"> Inherent Rating </label>
        <div class="col-sm-4">
            <div class="ent-rating" data-id="ent_def_in_rating_impact"></div>
        </div>
        <div class="col-sm-4">
            <div class="ent-rating" data-id="ent_def_in_rating_likelihood"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="risk_type"> Residual Rating </label>
        <div class="col-sm-4">
            <div class="ent-rating" data-id="ent_def_res_rating_impact"></div>
        </div>
        <div class="col-sm-4">
            <div class="ent-rating" data-id="ent_def_res_rating_likelihood"></div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_rating_remarks"> Rating Remarks </label>
        <div class="col-xs-7" id="ent_risk_rating_remarks">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_rated_by"> Rated By </label>
        <div class="col-xs-7" id="ent_risk_rated_by">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_rater_company"> Rater Company </label>
        <div class="col-xs-7" id="ent_risk_rater_company">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_rater_company_desc"> Company Code Description </label>
        <div class="col-xs-7" id="ent_risk_rater_company_desc">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_rater_dept"> Rater Department </label>
        <div class="col-xs-7" id="ent_risk_rater_dept">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_rater_dept_desc"> Department Code Description </label>
        <div class="col-xs-7" id="ent_risk_rater_dept_desc">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_rater_position"> Rater Position </label>
        <div class="col-xs-7" id="ent_risk_rater_position">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_rater_position_desc"> Rater Position Description </label>
        <div class="col-xs-7" id="ent_risk_rater_position_desc">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_rated_date"> Rated Date </label>
        <div class="col-xs-7" id="ent_risk_rated_date">
            <span class="label label-info label-wrap ent-risk-info"></span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="ent_risk_doc"> Supporting Documents </label>
        <div class="col-xs-7">
            <a class="btn btn-primary btn-sm" id="ent_risk_doc" style="display: none;"><i class="fa fa-download"></i> Download<span></span></a>
        </div>
    </div>
</div>