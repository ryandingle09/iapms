@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href='{{route('value_set.create')}}'>
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Value Sets
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="risk_assessment_scale-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="30%">Value Set Name</th>
                                <th width="60%">Description</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var $alert  = $('.alert'),
        form    = $('#risk-scale-form'),
        $modal  = $('#risk-scale-modal'),
        $business_process  = $('#business_process'),
        $auditable_entity  = $('#auditable_entity');
    var datatable= $('#risk_assessment_scale-table').DataTable( {
        ajax: "{{route('value_set.list')}}",
        "processing": true,
        orderCellsTop: true,
        columns: [
            { data: "value_set_name" },
            { data: "description", defaultContent: 'N/A', orderable: false },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('value_set/edit')}}/'+data.value_set_id+'"><i class="fa fa-pencil edit" title="edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.value_set_id+'" data-name="'+data.value_set_name+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    $(document).on('click', '.btn-save', function() {
        swal({
            title: "Continue?",
            text: "You are about to create new risk assesment scale.",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        location.reload();
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }
                }
            });
        });
    });

    // remove auditee
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var remove_name = $(this).attr('data-name');

        swal({
            title: "Continue?",
            text: "You are about to delete "+remove_name+" value set?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $.ajax({
                url: '{{ url('value_set/delete') }}/'+remove_id,
                method: "POST",
                data: { _token : _token, _method : 'delete' }, // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        location.reload();
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    // displays the validation error
                    var msg = '<ul class="list-unstyled">';

                    $.each(xhr.responseJSON, function(key, val){
                        for(var i=0; i < val.length; i++) {
                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                        }
                    });
                    msg += '</ul>';

                    $('.alert-warning').html(msg).show();
                }
            });
        });

        return false;
    });
</script>
@endsection
