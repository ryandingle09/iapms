@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Value Sets Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button>
                                            <a href="{{route('value_set')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                {!! Form::open(['url' => isset($details) ? route('value_set.update', ['id' => $details->value_set_id]) : route('value_set.store'), 'class' => 'form-horizontal', 'id' => 'value-set-form', 'style' => 'margin-top: 12px;']) !!}
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="table-header"> Value Set Definition </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div style="display:none;" class="alert alert-warning alert-main"></div>
                                        @if(isset($details))
                                            <input type="hidden" name="_method" value="PUT">
                                            <input type="hidden" name="id" value="{{ $details->value_set_id }}">
                                        @endif
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="name"> Value Set Name </label>
                                            <div class="col-sm-6">
                                                <input type="text" name="name" id="name" class="form-control input-sm" value="{{ isset($details) ? $details->value_set_name : '' }}" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="description"> Description </label>
                                            <div class="col-sm-6">
                                                <textarea name="description" id="description" class="form-control" rows="3" style="resize: none;">{{ isset($details) ? $details->description : '' }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12">
                                        <div class="table-header"> Value Set Query </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="code"> Code </label>
                                            <div class="col-sm-6">
                                                <input type="text" name="code" id="code" class="form-control input-sm" value="{{ isset($details) ? $details->code : '' }}" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="meaning"> Meaning </label>
                                            <div class="col-sm-6">
                                                <input type="text" name="meaning" id="meaning" class="form-control input-sm" value="{{ isset($details) ? $details->meaning : '' }}" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="from_clause"> From Clause </label>
                                            <div class="col-sm-6">
                                                <input type="text" name="from_clause" id="from_clause" class="form-control input-sm" value="{{ isset($details) ? $details->from_clause : '' }}" required="required">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for="where_clause"> Where Clause </label>
                                            <div class="col-sm-6">
                                                <textarea type="text" name="where_clause" id="where_clause" class="form-control" rows="10" style="resize: vertical;">{{ isset($details) ? $details->where_clause : '' }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label" for=""> &nbsp; </label>
                                            <div class="col-sm-6">
                                                <button type="button" class="btn btn-danger btn-test-query"><i class="fa fa-play"></i> Test query</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button>
                                            <a href="{{route('value_set')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
        var form        = $('#value-set-form'),
            $alert      = $('.alert-main');

        var $datatable;

         @if (Session::has('message'))
            $alert.html('{!! session('message') !!}').show();
        @endif

        // stops the page from reloading/closing if the form is edited
        $('input').change(function() {
            if( $(this).val() != "" ) {
                window.onbeforeunload = function() {
                    return 'Are you sure you want to navigate away from this page?';
                };
            }
        });

        // save the form through AJAX call
        function save() {
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    toggle_loading();

                    if( data.success ) {
                        window.onbeforeunload = null;
                        window.location = '{{ route('value_set') }}';
                    }
                    else
                    {
                        swal('Failed!', 'You have an error in you query. Please check.', 'warning');
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        }

        $('body').on('click', '.btn-test-query', function(){
             $.ajax({
                url: '{{route('value_set.test_query')}}',
                method: "POST",
                data: { _token:'{{csrf_token()}}', code:$('#code').val(), meaning:$('#meaning').val(), from_clause:$('#from_clause').val(), where_clause:$('#where_clause').val() },
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    var title = 'Success!';
                    var type = 'success';
                    var message = '';

                    if( !data.success ) {
                        title = 'Failed!';
                        type = 'warning';
                        message = data.message;
                    }
                    else {
                        message = 'The query run without an error. Results count: '+ data.message;
                    }

                    toggle_loading();
                    swal(title, message, type);
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        });
    </script>
@endsection
