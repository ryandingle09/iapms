<div class="col-xs-12 mrgB15">
    <div class="table-header"> Projects </div>
</div>
<div class="col-xs-12 mrgB15">
    <table id="project-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Project Name</th>
                <th >Audit Type</th>
                <th >Status</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-12 mrgB15">
    <div class="table-header"> Project - Auditors </div>
</div>
<div class="col-xs-12 mrgB15">
    <table id="project-auditor-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Auditor Name</th>
                <th >Role</th>
                <th >Plan Budgeted Mandays</th>
                <th >Allotted Miscellaneous Mandays</th>
                <th >Allotted APG MandaysMandays</th>
                <th >Allotted Total Mandays</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@include('engagement_planning_inquiry.includes.project_modal')
@section('tab_projects_scripts')
<script type="text/javascript">
    //selectedProject came from engagement_planning_inquiry.index
    var project_datatable = $('#project-table').DataTable( {
        ajax: "{{route('project.list')}}",
        "processing": true,
        order: [[ 1, 'asc' ]],
        createdRow: function( row, data, dataIndex ) {
            if(selectedProject == data.project_id){
                $( row ).addClass('selected-row');
            }
            
            $( row ).attr('data-id', data.project_id);
            $( row ).attr('data-status', data.project_status);
        },

        columns: [
            { data: "project_name" , className : "inline-edit" },
            { data: "audit_type" },
            { data: "project_status" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('project')}}/'+data.project_id+'" class="edit"><i class="fa fa-eye" title="Fullview" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });

    var project_auditor_datatable = project_auditor_datatable = $('#project-auditor-table').DataTable( {
            "bDestroy": true,
            "processing": true,
            order: [[ 1, 'asc' ]],
            columns: [
                {
                    data: null,
                    render : function ( data, type, full, meta ) {
                        return data.auditor.first_name+' '+data.auditor.middle_name+' '+data.auditor.last_name;
                    }
                },
                { data: "auditor_type" },
                { data: "plan_budgeted_mandays" },
                { data: "allotted_miscellaneous_mandays" },
                { data: "allotted_apg_mandays" },
                { data: "alotted_total_mandays" },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="proficiency" data-id="'+data.auditor_id+'"><i class="fa fa-user text-primary" title="Proficiency" rel="tooltip"></i></a>';
                    },
                    orderable: false,
                    searchable: false,
                    className: "text-center"
                }
            ]
        });


    $(document).on('click','#project-table tbody tr',function(){
        $(".selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        selectedProject = $( this ).data('id');
        selectedProjectStatus = $( this ).data('status');
        var url = "{{url('project')}}/"+selectedProject+"/auditor";
        $("#create-project-auditor-form").attr('action',url);
        $("#_engagement_tab").find("a[href='#scope']").attr('data-toggle','tab');
        $("a[data-target='#create-project-auditor-modal']").removeAttr('disabled');
        project_auditor_datatable.ajax.url( url + "/list" ).load();
        return false;
    });

    $(document).on("click","#project-auditor-table .delete",function(){
        $(this).deleteItemFrom(project_auditor_datatable);
        return false;
    });

</script>
@yield('project_modal_scripts')
@endsection
