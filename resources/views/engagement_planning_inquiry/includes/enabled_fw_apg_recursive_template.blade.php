@if($group_by != "test_proc_table")
	@foreach($data as $item)
	<div class="apg-item">
	    <div class="content">
	        {{ $item->title }}
	    </div>
	    <div class="child on" >
	        @if($item->children)
	            @include("engagement_planning.includes.enabled_fw_apg_recursive_template", [ 
	            	'data' => $item->children , 
	            	'group_by' => $item->next_field
	            ])
	        @endif
	    </div>
	</div>
	@endforeach
@else
<div class="apg-table">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Test Seq.</th>
                <th >Test Procedure Narrative</th>
                <th >Audit Objective</th>
                <th >Auditor</th>
                <th >Mandays</th>
                <th >Approval Status </th>
                <th >Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $item)
            <tr>
                <td>{{ $item->control_test_seq }}</td>
                <td>{{ $item->test_proc_narrative }}</td>
                <td>{{ $item->audit_objective}}</td>
                <td>{{ $item->auditor_full_name}}</td>
                <td>{{ $item->allotted_mandays}}</td>
                <td> n/a </td>
                <td> 
                    <a class="fieldwork" href="{{ route('project_scope_apg_findings.save',[ 'apg_id' => $item->project_scope_apg_id ]) }}">
                        <i class="ace-icon fa fa-tasks bigger-130" rel="tooltip" data-original-title="Fieldwork" ></i>
                    </a> 
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

