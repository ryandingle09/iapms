

<!-- Edit Projects Scope Modal -->
<div class="modal fade " data-backdrop="static" id="edit-project-scope-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Project Scope</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div id="edit-project-scope-alert"></div>
                        <form class="form-horizontal" id="edit-project-scope-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="auditable_entity_auditable_entity_name"> Auditable Entity </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control makelabel" name="auditable_entity_auditable_entity_name" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="main_bp_main_bp_name"> Main Business Process </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control makelabel" name="main_bp_main_bp_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="auditable_entity_entity_head_name"> Auditee Head </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control makelabel" name="auditable_entity_entity_head_name" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="auditable_entity_contact_name"> Auditee Contact Person </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control makelabel" name="auditable_entity_contact_name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="plan_budgeted_mandays"> Plan Budgeted Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="plan_budgeted_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="default_misc_mandays"> Default Miscellaneous Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="default_misc_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="default_apg_mandays"> Default APG Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="default_apg_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="default_total_mandays"> Default Total Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="default_total_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="allotted_miscellaneous_mandays"> Allotted Miscellaneous Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="allotted_miscellaneous_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="allotted_apg_mandays"> Allotted APG Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="allotted_apg_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="allotted_total_mandays"> Allotted Total Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="allotted_total_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="adjusted_severity_value"> Adjusted Severity Value </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="adjusted_severity_value" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-3">
                                    <input type="text" name="target_start_date" class="form-control input-sm form-control makelabel" >
                                </div>
                                <label class="col-sm-1 control-label align-center " for="name"> To </label>
                                <div class="col-sm-3">
                                    <input type="text" name="target_end_date" class="form-control input-sm form-control makelabel" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="project_scope_status"> Status </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control makelabel" name="project_scope_status" value="n/a">
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Projects Scope Modal -->

@section('project_scope_modal_scripts')
<script type="text/javascript">
    // project_scope_datatable from tab_scope

    $(document).on('click',"#project-scope-table .edit",function(){
        var url = $(this).attr('href');
        $.ajax({
            url : url,
            method : "GET",
            dataType : "json",
            success : function(response){
                $("#edit-project-scope-form").supply(response.data).attr('action',url );
                $("#edit-project-scope-form .makelabel").makeLabel();
                $("#edit-project-scope-modal").modal('show');
            }
        })
        return false;
    });

</script>
@endsection