<div class="modal fade"  id="create-iom-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Generate IOM</h4>
            </div>
            <div class="modal-body">
                <div id="create-iom-alert"></div>
                <form class="form-horizontal scrollable" id="create-iom-form" action="" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="project_scope_id" value="">
                    <div class="form-group" > 
                        <div class="col-xs-6 text-left" for="description"> 
                            <a href="#" class="btn btn-info btn-sm btn-iom-view">
                                <i class="ace-icon fa fa-eye bigger-120"></i>
                                View
                            </a>
                            <a href="#" class="btn btn-success btn-sm btn-iom-send">
                                <i class="ace-icon fa fa-send bigger-120"></i>
                                Send
                            </a>
                        </div>
                        <div class="col-sm-6 text-right">
                            <button class="btn btn-info btn-sm"><i class="ace-icon fa fa-save bigger-120"></i>
                            Apply</button>
                            <a href="#" class="btn btn-warning btn-sm" data-dismiss="modal">
                                <i class="ace-icon fa fa-times bigger-120"></i>
                                Cancel
                            </a>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-2 control-label align-left" for="start_date"> Audit Start Date </label>
                        <div class="col-sm-4">
                            <input type="text" name="start_date" class="start_date form-control input-sm date-picker" value="" data-date-format="dd-M-yyyy">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label align-left" for="subject"> Subject </label>
                        <div class="col-sm-4">
                            <input type="text" name="subject" class="subject form-control input-sm" value="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label align-left" for="introduction"> Introduction </label>
                        <div class="col-sm-10">
                            <textarea name="introduction" class="introduction form-control input-sm h-only" rows="3" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label align-left" for="background"> Background </label>
                        <div class="col-sm-10">
                            <textarea name="background" class="background form-control input-sm h-only" rows="3" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label align-left" for="objectives"> Objectives </label>
                        <div class="col-sm-10">
                            <textarea name="objectives" class="objectives form-control input-sm h-only" rows="3" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label align-left" for="auditee_assistance"> Auditee Assistance </label>
                        <div class="col-sm-10">
                            <textarea name="auditee_assistance" id="auditee_assistance" class="form-control input-sm h-only" rows="3" ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label align-left" for="staffing"> Staffing </label>
                        <div class="col-sm-10">
                            <textarea name="staffing" class="staffing form-control input-sm h-only" rows="3" ></textarea>
                        </div>
                    </div>
                    <div class="form-group mrgB15" style="    position: relative;">
                        <div class="col-xs-12 align-left">
                            <a class="btn btn-primary btn-sm btn-create-dist" data-target="create-dist-form">
                                <i class="ace-icon fa fa-plus bigger-120"></i>
                                Create
                            </a>
                        </div>
                        <div id="create-dist-form" class="create-dist col-md-12">
                            <div class="table-header mrgB15" > Create Auditee Distribution </div>
                            <div id="create-dist-alert"></div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="dist_auditee"> Auditee </label>
                                <div class="col-sm-9">
                                    <select class="form-control dist_auditee" name="dist_auditee"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="dist_type"> Distribution Type </label>
                                <div class="col-sm-9">
                                    <select class="form-control dist_type" name="dist_type">
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" > 
                                <div class="col-sm-12 text-right">
                                    <a href="#" class="btn btn-info btn-sm btn-save-dist">
                                        <i class="ace-icon fa fa-save bigger-120"></i>
                                        Save
                                    </a>
                                    <a href="#" class="btn btn-warning btn-sm btn-close-dist" >
                                        <i class="ace-icon fa fa-times bigger-120"></i>
                                        Cancel
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="table-header reseth" > Auditee Distribution List </div>
                            <table id="auditee-distribution-table" class=" table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th >Auditee Name</th>
                                        <th >Position</th>
                                        <th >Distribution Type</th>
                                        <th >Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group mrgB15">
                        <div class="col-sm-12">
                            <div class="table-header"> Auditors </div>
                            <table id="iom-scope-auditor-table" class="col-sm-6 table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th >Auditor Name</th>
                                        <th >Auditor Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="form-group" > 
                        <div class="col-sm-12 text-right">
                            <button class="btn btn-info btn-sm"><i class="ace-icon fa fa-save bigger-120"></i>
                            Apply</button>
                            <a href="#" class="btn btn-warning btn-sm" data-dismiss="modal">
                                <i class="ace-icon fa fa-times bigger-120"></i>
                                Cancel
                            </a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
<div class="modal fade"  id="create-dist-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" data-dismiss="modal" class="close" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Distribution</h4>
            </div>
            <div class="modal-body">
                
            </div>
        </div>
    </div>
</div>

@section('project_scope_iom_modal_scripts')
<script type="text/javascript" src="../js/jQuery.print.js"></script>
<script type="text/javascript">
    // selectedScope initialized in index 
    var iom_scope_auditor_datatable = null;
    var iom_scope_dist_datatable = null;
    var has_iom = 0;
    var iom_scope = 0;
    $(document).on("click",".btn-generate-iom",function(){
        $("#create-iom-form")[0].reset();
        has_iom = 0;
        link = $(this).attr('href');
        iom_scope = $(this).data('id');
        iom_scope_dist_datatable = $("#auditee-distribution-table").DataTable( {
            "bDestroy": true,
            ajax: "{{ url('project-scope')}}/"+ iom_scope+ "/dist/list",
            processing : true,
            bLengthChange : false,
            bFilter : false,
            order: [[ 1, 'asc' ]],
            columns: [
                {
                    data: null,
                    render : function ( data, type, full, meta ) {
                        return data.auditee.first_name+' '+data.auditee.middle_name+' '+data.auditee.last_name;
                    }
                },
                { data: "auditee.lv_position_code_meaning" },
                { data: "distribution_type" },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="{{ url('project-scope') }}/' + iom_scope +'/dist/'+data.iom_dist_list_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                    },
                    orderable: false,
                    searchable: false,
                    className: "text-center"
                }
            ],
            initComplete : function(){
                $("#create-iom-modal").resetModalHeight();
            }
        });

        iom_scope_auditor_datatable = $("#iom-scope-auditor-table").DataTable( {
            bDestroy: true,
            ajax: "{{ url('project-scope')}}/" + iom_scope + "/auditor/list",
            processing : true,
            bLengthChange : false,
            bFilter : false,
            order: [[ 1, 'asc' ]],
            columns: [
                {
                    data: null,
                    render : function ( data, type, full, meta ) {
                        return data.auditor.first_name+' '+data.auditor.middle_name+' '+data.auditor.last_name;
                    }
                },
                { data: "auditor_type" }
            ],
            initComplete : function(){
                $("#create-iom-modal").resetModalHeight();
            }
        });

        if(isNaN(link)){
            has_iom = 1;
            $.ajax({
                url : link,
                method : "GET",
                beforeSend : sLoading(),
                success : function(response){
                    swal.close();
                    $("#create-iom-modal").modal({backdrop: 'static', keyboard: false});
                    if(response.data != null){
                        $("#create-iom-form").supply(response.data);
                    }
                }
            });
        }else{
            $.ajax({
                url : "{{ url('global_config/0') }}",
                method : "GET",
                beforeSend : sLoading(),
                success : function(response){
                    swal.close();
                    var data = {
                        subject : response.data.iom_subject,
                        introduction : response.data.iom_introduction,
                        background : response.data.iom_background,
                        objectives : response.data.iom_objectives,
                        auditee_assistance : response.data.iom_auditee_assistance,
                        staffing : response.data.iom_staffing,
                    }
                    $("#create-iom-form").supply(data);
                    $("#create-iom-modal").modal({backdrop: 'static', keyboard: false});
                }
            });
           
        }
        
        return false;
    });

    $(document).on("submit","#create-iom-form",function(){
        $('.alert').remove();
        $.ajax({
            url : "{{ url('project-scope') }}/" + iom_scope + "/iom/save",
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#create-iom-modal").resetModalHeight();
                has_iom = 1;
                project_scope_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#create-iom-form").modalFormErrorHandler(xhr);
            }
        });
        return false;
    });

    $(document).on('click',"#create-dist-modal .close",function(){
        $("#create-iom-modal").resetModalHeight();
        return false;
    });

    $(document).on('click',".btn-create-dist",function(){
        if(has_iom){
            $("#create-iom-modal").resetModalHeight();
            $("#create-dist-form").show();
        }else{
            sAlert("Oops!","Please save IOM before creating auditee distribution.")
        }
    });

    $(document).on('click',".btn-iom-view",function(){
        if(has_iom){
            $.ajax({
                url : "{{ url('project-scope') }}/" + iom_scope + "/iom/layout",
                method : "POST",
                data : { _token : _token },
                beforeSend : sLoading(),
                success : function(response){
                    swal.close();
                    $.print(response.html);
                }
            });
        }else{
            sAlert("Oops!","Please save IOM before viewing the layout.")
        }
    });

    $(document).on('click',".btn-iom-send",function(){
        if(has_iom){
            $.ajax({
                url : "{{ url('project-scope') }}/" + iom_scope + "/iom/send",
                method : "POST",
                data : { _token : _token },
                beforeSend : sLoading(),
                success : function(response){
                    $.sSuccess(response.message);
                }
            });
        }else{
            sAlert("Oops!","Please save IOM before sending.")
        }
    });

    $(document).on("click",'.btn-close-dist',function(){
        $("#create-dist-form").hide();
    });

    $(document).mouseup(function(e){
        var container = $("#create-dist-form");
        if (!container.is(e.target) && container.has(e.target).length === 0){
            container.hide();
        }
    });

    var dist_type_select = $(".dist_type").makeSelectize({
        lookup  : '{{ config('iapms.lookups.auditee.distribution_type') }}'
    })[0].selectize;

    var dist_auditee_select = $(".dist_auditee").makeSelectize({
        valueField: 'auditee_id',
        labelField: 'first_name',
        searchField: ['first_name','last_name'],
        url  : '{{ url('auditee/list?format=json') }}',
        selectedTemplate : function(item,scape){
            return '<div> '+item.first_name+' '+item.last_name+'</div>';
        }
    })[0].selectize;

    $(document).on('click',".btn-save-dist",function(){
        $('.alert').remove();
        $('.has-error').removeClass('has-error');
        $.ajax({
            url : "{{ url('project-scope') }}/"+ iom_scope + "/dist",
            method : "POST",
            data : { 
                _token : _token,
                dist_auditee : $(".dist_auditee").val(),
                dist_type : $(".dist_type").val()
            },
            beforeSend : sLoading(),
            success : function(response){
                iom_scope_dist_datatable.ajax.reload( null, false );
                dist_type_select.clear();
                dist_auditee_select.clear();
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#create-dist-form").modalFormErrorHandler(xhr);
            }
        });
        return false;
    });

    $(document).on("click","#auditee-distribution-table .delete",function(){
        $(this).deleteItemFrom(iom_scope_dist_datatable);
        return false;
    });

</script>
@endsection