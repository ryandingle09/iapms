
<!-- Edit Project Modal -->
<div class="modal fade " data-backdrop="static" id="edit-project-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">View Project</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div id="edit-project-alert"></div>
                        <form class="form-horizontal" id="edit-project-form" action="" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="project_name"> Project Name </label>
                                <div class="col-sm-4">
                                    <input type="text" name="project_name" id="project_name" class="form-control input-sm makelabel" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_type"> Audit Type </label>
                                <div class="col-sm-4">
                                    <input type="text" name="audit_type" id="audit_type" class="form-control input-sm makelabel" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="project_status"> Status </label>
                                <div class="col-sm-4">
                                    <input type="text" name="project_status" class="form-control input-sm project_status makelabel">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-2">
                                    <input type="text" name="target_start_date" class="form-control input-sm form-control makelabel" >
                                </div>
                                <label class="col-sm-1 control-label align-center " for="name"> To </label>
                                <div class="col-sm-2">
                                    <input type="text" name="target_end_date" class="form-control input-sm form-control makelabel" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="project_head"> Project Head </label>
                                <div class="col-sm-4">
                                    <input type="text" name="project_head" class="form-control input-sm form-control makelabel" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="project_remarks"> Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm makelabel" name="project_remarks" id="project_remarks" rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="approver_fullname"> Approved By </label>
                                <div class="col-sm-4">
                                    <input type="text" name="approver_fullname" class="form-control input-sm makelabel" value="n/a">
                                </div>
                                <label class="col-sm-2 control-label align-left" for="approved_date"> Approved Date </label>
                                <div class="col-sm-4">
                                    <input type="text" name="approved_date" class="form-control input-sm makelabel" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="approver_remarks"> Approver Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm approver_remarks makelabel" name="approver_remarks" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="plan_budgeted_mandays"> Plan Budgeted Mandays </label>
                                <div class="col-sm-3">
                                    <input type="text" name="plan_budgeted_mandays" class="form-control input-sm makelabel">
                                </div>
             
                                <label class="col-sm-3 control-label align-left" for="allotted_miscellaneous_mandays"> Allotted Miscellaneous Mandays </label>
                                <div class="col-sm-3">
                                    <input type="text" name="allotted_miscellaneous_mandays" class="form-control input-sm makelabel">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="allotted_apg_mandays"> Allotted APG Mandays </label>
                                <div class="col-sm-3">
                                    <input type="text" name="allotted_apg_mandays" class="form-control input-sm makelabel">
                                </div>
               
                                <label class="col-sm-3 control-label align-left" for="allotted_total_mandays"> Allotted Total Mandays </label>
                                <div class="col-sm-3">
                                    <input type="text" name="allotted_total_mandays" class="form-control input-sm makelabel">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="annual_plan_plan_project_name"> Annual Audit Plan Project Name </label>
                                <div class="col-sm-3">
                                    <input type="text" name="annual_plan_plan_project_name" class="form-control input-sm makelabel" >
                                </div>
                                <label class="col-sm-3 control-label align-left" for="annual_plan_scopes_total_budgeted_mandays"> Annual Plan Budgeted Mandays </label>
                                <div class="col-sm-3">
                                    <input type="text" name="annual_plan_scopes_total_budgeted_mandays" class="form-control input-sm makelabel">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="annual_plan_target_start_date"> Annual Audit Plan Target Date Start </label>
                                <div class="col-sm-3">
                                    <input type="text" name="annual_plan_target_start_date" class="form-control input-sm makelabel">
                                </div>
                                <label class="col-sm-3 control-label align-left" for="annual_plan_target_end_date"> Annual Audit Plan Target Date End </label>
                                <div class="col-sm-3">
                                    <input type="text" name="annual_plan_target_end_date" class="form-control input-sm makelabel">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_year"> Audit Year </label>
                                <div class="col-sm-4">
                                    <input type="text" name="audit_year"  class="form-control input-sm makelabel">
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Close
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Project Modal -->

@section('project_modal_scripts')
<script type="text/javascript">

    $(document).on("click","#project-table .edit",function(){
        var url = $(this).attr('href');
        $.ajax({
            url : url,
            method : "GET",
            beforeSend : sLoading(),
            success : function(response){
                data = response.data;
                swal.close();
                $("#edit-project-form").supply(data);
                $("#edit-project-form .makelabel").makeLabel();
                $("#edit-project-modal").modal('show');
            }
        });
        return false;
    });

</script>
@endsection