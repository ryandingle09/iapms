

<div class="col-xs-12 align-left" id="main-apg-wrapper">

    <div class="apg-wrapper-header"> Audit Program Guide </div>
    <div class="apg-wrapper scrollable" id="apg-wrapper" data-groupBy="bp_name" data-keyword="">
        
    </div>
</div>

@include('engagement_planning.includes.project_scope_apg_modal')
@section('tab_apg_scripts')
<script type="text/javascript">
    var project_scope_apg_datatable = null;
    $(document).on('click',"#_engagement_tab a[href='#apg']",function(){
        if(selectedScope){
            $.loadFullView('enabled_apg');
        }else{
            $.sInfo("You have to select project scope to open this tab.");
        }
    });

    $.loadFullView = function(type){
        $("#main-apg-wrapper").LoadingOverlay("show");
        $.ajax({
            url : "{{ url('project-scope') }}/" + selectedScope + "/apg/full-view",
            method : "GET",
            data : { type : type },
            success : function(response){
                var html = '<div class="alert alert-info"> <strong> <i class="ace-icon fa fa-info"></i> </strong> No APG Found. <br> </div>';
                if(response.data.length){
                    html = response.html;
                }
                $("#apg-wrapper").html(html);    
                $("#main-apg-wrapper").LoadingOverlay("hide");
            }
        });
    };

</script>
@yield('project_scope_apg_modal_scripts')
@endsection
