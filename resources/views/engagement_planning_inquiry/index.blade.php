@extends('layouts.app')
@section('styles')
<link rel="stylesheet" href="/css/datepicker.css" />
<link rel="stylesheet" href="/css/datatables.bootstrap.css" />
<style type="text/css">
    .row-details {
        padding-bottom: 5px;
    }
    .selected-row {
        background-color: #ffe99e !important;
    }
    .table-hover tbody > tr:hover {
        cursor: pointer;
    }
</style>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')
        <div class="page-content">
            @include('includes.content_header')
            <div id="alert-wrapper">
                @include('includes.alerts')
            </div>
        	<div class="col-xm-12">
            	<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="_engagement_tab">
                    <li class="active">
                        <a href="#projects" data-toggle="tab" aria-expanded="true">Projects</a>
                    </li>
                    <li>
                        <a href="#scope" data-toggle="" aria-expanded="false">Scope</a>
                    </li>
                	<li >
                        <a href="#apg" data-toggle="" aria-expanded="false">APG</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- Projects -->
                    <div id="projects" class="tab-pane active">
                        <div class="row">
                            @include('engagement_planning_inquiry.includes.tab_projects')
                        </div>
                    </div>
                    <!-- /Projects -->
                    <!-- Scope -->
                    <div id="scope" class="tab-pane">
                        <div class="row">
                            @include('engagement_planning_inquiry.includes.tab_scope')
                        </div>
                    </div>
                    <!-- /Scope -->
                    <!-- APG -->
                    <div id="apg" class="tab-pane">
                        <div class="row">
                            @include('engagement_planning_inquiry.includes.tab_apg')
                        </div>
                    </div>
                    <!-- /APG -->
                </div>
        	</div>
		</div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@include('includes.modals.ae_details_modal')
@include('includes.modals.proficiency_modal')
@include('includes.modals.mbpa_details_modal')
@endsection
@section('footer_script')
<script src="/js/date-time/bootstrap-datepicker.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script src="/plugins/tinymce/tinymce.min.js"></script>
<script src="/plugins/tinymce/jquery.tinymce.min.js"></script>
<script type="text/javascript">
    var selectedProject = 0;
    var selectedProjectStatus = 0;
    var selectedScope = 0;


</script>
<script type="text/javascript">
    // $(document).on('click','.btn-ae-details',function(){
        //$("#ae-details-modal").modal('show');
        // return false;
    // })
</script>
@yield('tab_projects_scripts')
@yield('tab_scope_scripts')
@yield('tab_apg_scripts')
@yield('proficiency_scripts')
@yield('mbpa_details_modal_scripts')

@endsection