@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Leave Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                {!! Form::open(['url' => isset($details) ? route('leave.update', [ 'id' => $details->id ]) : route('leave.store'), 'class' => 'form-horizontal', 'id' => 'leave-form']) !!}
                                    @if( isset($details) )
                                    <input type="hidden" name="_method" value="put">
                                    @endif
                                    <div class="form-row align-right">
                                        <button class="btn btn-primary btn-sm">
                                            <i class="ace-icon fa fa-save bigger-120"></i>
                                            Save
                                        </button>
                                        <a href="{{route('leave.index')}}" class="btn btn-warning btn-sm">
                                            <i class="ace-icon fa fa-times bigger-120"></i>
                                            Cancel
                                        </a>
                                    </div>
                                    <hr>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="auditor"> Auditor </label>
                                        <div class="col-sm-6 col-xs-10">
                                            @if(isset($details))
                                            <span class="label label-info" style="margin-top: 5px;">{{ $details->auditor->first_name." ".$details->auditor->last_name }}</span>
                                            <input type="hidden" name="auditor" value="{{$details->auditor_id}}">
                                            @else
                                            <select id="auditor" name="auditor" class="form-control input-sm basic-info" data-placeholder="Click to Choose..." {{ isset($details) ? 'disabled' : '' }}></select>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="leave_date_from leave_date_to"> Leave Date </label>

                                        <div class="col-sm-6">
                                            <div class="input-daterange input-group">
                                                <input type="text" id="leave_date_from" name="leave_date_from" class="form-control input-sm form-control" placeholder="Start Date" value="{{ isset($details) ? $details->leave_date_from : '' }}"/>
                                                <span class="input-group-addon">to</span>
                                                <input type="text" id="leave_date_to" name="leave_date_to" class="form-control input-sm form-control" placeholder="End Date" value="{{ isset($details) ? $details->leave_date_to : '' }}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="leave_type"> Leave Type </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <select id="leave_type" name="leave_type" class="form-control input-sm basic-info" data-placeholder="Click to Choose...">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="remarks"> Remarks </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <textarea id="remarks" name="remarks" row="5" style="resize: none;" class="form-control input-sm basic-info">{{ isset($details) ? $details->remarks : '' }}</textarea>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-row align-right">
                                        <button class="btn btn-primary btn-sm">
                                            <i class="ace-icon fa fa-save bigger-120"></i>
                                            Save
                                        </button>
                                        <a href="{{route('leave.index')}}" class="btn btn-warning btn-sm">
                                            <i class="ace-icon fa fa-times bigger-120"></i>
                                            Cancel
                                        </a>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script type="text/javascript">
    var leave_type_select = $('#leave_type').makeSelectize({
        lookup : '{{ config('iapms.lookups.leave') }}',
        selectedTemplate : 'id_text_meaning_description',
        onLoad : function(){
            @if(isset($details))
            this.setValue("{{ $details->leave_type}}");
            @endif
        },
    });

    var auditor_select = $('#auditor').makeSelectize({
        url: "{{route('auditor.list.show')}}",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    });

    $('.input-daterange').datepicker({
        format: "dd-M-yyyy",
        startDate: 'today',
        keyboardNavigation: false,
        autoclose : true,
    });

    $("#leave-form").submit(function(){
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response) {
                $.sSuccess(response.messsage,function(){
                    location.href = "{{route('leave.index')}}";
                });
            }
        });
        return false;
    });

</script>
@endsection