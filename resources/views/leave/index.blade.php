@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')
            @include('includes.alerts')
            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{route('leave.create')}}">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Leaves
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="auditor-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Auditor</th>
                                <th>Leave Date From</th>
                                <th>Leave Date To</th>
                                <th>Leave Type</th>
                                <th>Remarks</th>
                                <th width="5%"></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var auditor_datatable = $('#auditor-table').DataTable( {
        ajax: "{{route('leave.list')}}",
        "processing": true,
        order: [[ 1, 'asc' ]],
        columns: [
            {
                data: null,
                render : function (data, type, full, meta) {
                    return data.auditor.first_name+' '+ data.auditor.middle_name+' '+ data.auditor.last_name;
                }
            },
            { data: "leave_date_from" },
            { data: "leave_date_to" },
            { data: "leave_type" },
            { data: "remarks" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('leave/edit')}}/'+data.id+'"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a> &nbsp; <a href="{{ url('leave') }}/'+data.id+'" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    // remove auditor
    $(document).on('click', "#auditor-table .delete" ,function(){
        $(this).deleteItemFrom(auditor_datatable);  
        return false;
    });

</script>
@endsection
