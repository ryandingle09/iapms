<!-- Edit Project Modal -->
<div class="modal fade " data-backdrop="static" id="edit-project-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">View Project</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div id="edit-project-alert"></div>
                        <form class="form-horizontal" id="edit-project-form" action="" method="POST">
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="plan_project_name"> Project Name </label>
                                <div class="col-sm-5">
                                    <input type="text" name="plan_project_name" id="plan_project_name" class="form-control input-sm makelabel" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_type"> Audit Type </label>
                                <div class="col-sm-5">
                                    <input type="text" name="audit_type" id="audit_type" class="form-control input-sm makelabel" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="plan_project_status"> Status </label>
                                <div class="col-sm-5">
                                    <input type="text" name="plan_project_status" class="form-control input-sm plan_project_status" value="" readonly="">

                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-2">
                                    <input type="text" id="target_start_date" name="target_start_date" class="form-control input-sm form-control makelabel" />
                                </div>
                                <label class="col-sm-1 control-label align-center " for="name"> To </label>
                                <div class="col-sm-2">
                                     <input type="text" id="target_end_date" name="target_end_date" class="form-control input-sm form-control makelabel" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="project_head_fullname"> Project Head </label>
                                <div class="col-sm-5">
                                    <input type="text" id="project_head_fullname" name="project_head_fullname" class="form-control input-sm form-control makelabel" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="plan_project_remarks"> Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm makelabel" name="plan_project_remarks" id="plan_project_remarks" rows="3" ></textarea>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="approver_fullname"> Approved By </label>
                                <div class="col-sm-4">
                                    <input type="text" name="approver_fullname" class="form-control input-sm makelabel" value="n/a">
                                </div>
                                <label class="col-sm-2 control-label align-left" for="approved_date"> Approved Date </label>
                                <div class="col-sm-4">
                                    <input type="text" name="approved_date" class="form-control input-sm makelabel" value="n/a">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="running_allotted_mandays"> Running Allotted Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" name="running_allotted_mandays" class="form-control input-sm makelabel" value="n/a" readonly="">
                                </div>
                            </div>
               
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="approver_remarks"> Approver Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm approver_remarks makelabel" name="approver_remarks" rows="3" readonly="">n/a</textarea>
                                </div>
                            </div>

                            <div class="form-row row" >
                                <div class="col-sm-6 align-left">
                                    <a href="#" class="btn btn-success btn-sm btn-approval" 
                                        data-key="plan_project" 
                                        data-status="" 
                                        data-ref-id="" 
                                        data-cat="Annual Audit Plan"
                                        data-subcat="Project"
                                    >
                                        <i class="ace-icon fa fa-check bigger-120"></i>
                                        Approval
                                    </a>  
                                </div>
                                <div class="col-sm-6 align-right">
                                    <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                        <i class="ace-icon fa fa-times bigger-120"></i>
                                        Close
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Project Modal -->

@section('project_modal_scripts')
<script type="text/javascript">
    //project_datatable came from engagement_planning.tab_projects

    $(document).on("click","#project-table .edit",function(){
        $("#edit-project-form [name='reviewer_remarks']").attr('readonly','readonly');
        $("#edit-project-form [name='approver_remarks']").attr('readonly','readonly');
        url = $(this).attr('href');
        $.ajax({
            url : url,
            method : "GET",
            beforeSend : sLoading(),
            success : function(response){
                data = response.data;
                swal.close();
                $("#edit-project-modal").modal('show');
                data['project_head_fullname'] = data.project_head_details == null ? 'n/a' : data.project_head_details.full_name;
                data['approver_fullname'] = data.end_approval == null ? 'n/a' : data.end_approval.approver.full_name;
                data['approved_date'] = data.end_approval == null ? 'n/a' : data.end_approval.approval_date;
                data['approver_remarks'] = data.end_approval == null ? 'n/a' : data.end_approval.remarks;
                $("#edit-project-form").supply(data);
                $("#edit-project-form").attr('action',url);
                $("#edit-project-form .makelabel").makeLabel();
                $("#edit-project-form .btn-approval").attr('data-status', data.plan_project_status );
                $("#edit-project-form .btn-approval").attr('data-ref-id', data.plan_project_id );
            }
        });
        return false;
    });

    $("#edit-project-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : "PUT",
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#edit-project-modal").modal('hide');
                $.sSuccess(response.message);
                project_datatable.ajax.reload( null, false );

            },
            error : function(xhr){
                $("#edit-project-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on("submit","#plan_project-approval-form",function(){
        $(".alert").remove();
        $(".has-error").removeClass('has-error');
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                console.log(response);
                $("#plan_project-approval-form").parents('#approval-modal').modal('hide');
                project_datatable.ajax.reload( null, false );
                $("#edit-project-modal").modal('hide');
                $.sSuccess(response.message);
            },
            error: function (xhr) {
                switch(parseInt(xhr.status)){
                    case 422:
                        swal.close();
                        html = '<div class="alert alert-warning"> <button type="button" class="close" data-dismiss="alert"> <i class="ace-icon fa fa-times"></i> </button>';
                        $.each(xhr.responseJSON,function(k,v){
                            html += '<strong> <i class="ace-icon fa fa-times bigger-110 red"></i> </strong>'+v[0]+'<br>';
                            $('[name="'+k+'"]').parent().addClass('has-error');
                        });
                        html += '</div>';
                        $('#plan_project-approval-form').before(html);
                        break;
                    default:
                        $.sError("Error "+xhr.status+" : "+xhr.statusText);
                    break;
                }
            }
        });
        return false;
    });

</script>
@endsection