<div class="col-xs-12">
    <div class="table-header"> Project Scopes </div>
</div>
<div class="col-xs-12 mrgB15">
    <table id="project-scope-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Auditable Entity Name</th>
                <th >Main Business Process Name</th>
                <th >Budgeted Mandays</th>
                <th >Adjusted Severity Value</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>

        <tbody></tbody>
    </table>
</div>
<div class="col-xs-12">
    <div class="table-header"> Project Scope - Auditors </div>
</div>
<div class="col-xs-12 mrgB15">
    <table id="project-scope-auditor-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Auditor Name</th>
                <th >Role</th>
                <th >Allotted Mandays</th>
                <th >Annual Mandays</th>
                <th >Running Mandays</th>
                <th >Available Mandays</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
</div>

@include('plan.includes.project_scope_modal')
@include('plan.includes.project_scope_auditor_modal')
@include('plan.includes.project_scope_iom_modal')
@include('includes.modals.custom_measures')

@section('tab_scope_scripts')
<script type="text/javascript">
    // selectedProject initialized in index
    // selectedScope initialized in index 
    var project_scope_datatable = null
    var project_scope_auditor_datatable = null
    $(document).on('click',"#main_tab a[href='#scope']",function(){
        if(selectedProject){
            scope_url = "{{ url('plan-project')}}/"+selectedProject+"/scope";
            project_scope_datatable = $("#project-scope-table").DataTable( {
                "bDestroy": true,
                ajax: scope_url +"/list",
                "processing": true,
                order: [[ 1, 'asc' ]],
                createdRow: function( row, data, dataIndex ) {
                    if(selectedScope == data.plan_project_scope_id){
                        $( row ).addClass('selected-row');
                    }
                    $( row ).attr('data-id', data.plan_project_scope_id);
                },
                columns: [
                    { data: "auditable_entity.auditable_entity_name" },
                    { data: "main_bp.main_bp_name" },
                    { data: "budgeted_mandays" },
                    { data: "adjusted_severity_value" },
                    {
                        data: null,
                        "render": function ( data, type, full, meta ) {
                            return '<a href="#" class="btn-custom-measure" data-ae_id="'+data.auditable_entity_id+'" data-id="'+data.main_bp.master_ae_mbp_id+'" ><i class="fa fa-dashboard text-primary" title="Custom Measures" rel="tooltip"></i></a>     <a href="" class="btn-mbpa-details" data-aeid="'+data.auditable_entity_id+'" data-mbpaid="'+data.mbp_id+'" ><i class="fa fa-sitemap text-primary" title="Details" rel="tooltip"></i></a>';
                        },
                        orderable: false,
                        searchable: false,
                        className: "text-center"
                    }
                ]
            });

        }else{
            $.sInfo("You have to select project to open this tab.");
        }
    });

    $(document).on('click','#project-scope-table tbody tr',function(){
        selectedScope = $(this).data('id');
        $(".selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        var url = "{{ url('plan-project-scope')}}/" + selectedScope + "/auditor";
        project_scope_auditor_datatable = $("#project-scope-auditor-table").DataTable( {
            "bDestroy": true,
            ajax: url + "/list",
            "processing": true,
            order: [[ 1, 'asc' ]],
            columns: [
                {
                    data: null,
                    render : function ( data, type, full, meta ) {
                        return data.auditor.first_name+' '+data.auditor.middle_name+' '+data.auditor.last_name;
                    }
                },
                { data: "auditor_type" },
                { data: "allotted_mandays" },
                { data: "annual_mandays" },
                { data: "running_mandays" },
                { data: "available_mandays" },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="proficiency" data-id="'+data.auditor_id+'"><i class="fa fa-user text-primary" title="Proficiency" rel="tooltip"></i></a>';
                    },
                    orderable: false,
                    searchable: false,
                    className: "text-center"
                }
            ]
        });
    });


</script>
@yield('project_scope_modal_scripts')
@yield('project_scope_auditor_modal_scripts')
@yield('project_scope_iom_modal_scripts')
@yield('ae_details_modal_scripts')
@yield('cm_scripts')
@endsection