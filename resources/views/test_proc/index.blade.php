@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="{{asset('plugins/sweetalert/sweetalert.css')}}" />
    <link rel="stylesheet" href="{{asset('css/datatables.bootstrap.css')}}" />
@endsection
@section('content')
    <div class="main-content">
        <div class="main-content-inner">
            @include('includes.breadcrumb')

            <div class="page-content">
                @include('includes.content_header')

                <div class="row">
                    <div class="col-xs-12">
                        @if (Session::has('message'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">
                                    <i class="ace-icon fa fa-times"></i>
                                </button>

                                <strong>
                                    <i class="ace-icon fa fa-check"></i>

                                </strong>

                                {!! session('message') !!}
                                <br>
                            </div>
                        @endif
                        <div class="clearfix">
                            <div class="pull-right">
                                <a class="btn btn-primary" href="#test-procedures-modal" data-toggle="modal">
                                    <i class="ace-icon fa fa-plus"></i>
                                    Create
                                </a>
                            </div>
                        </div>
                        <div class="table-header">
                            Test Procedures
                        </div>

                        <!-- div.table-responsive -->

                        <!-- div.dataTables_borderWrap -->
                        <table id="test-procedures-table" class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="10%">Test Procedure Name</th>
                                <th width="40%">Test Procedure Description</th>
                                <th width="35%">Audit Objective</th>
                                <th>Budgeted Mandays</th>
                                <th width="10%"></th>
                            </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    <!-- Test procedures modal -->
    <div class="modal fade" data-backdrop="static" id="test-procedures-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Test Procedures Form</h4>
                </div>
                <div class="modal-body">
                    <div style="display:none;" class="alert alert-warning modal-alert"></div>
                    {!! Form::open(['url' => route('test_procedures.store'), 'class' => 'form-horizontal', 'id' => 'test-procedures-form']) !!}
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="id" value="">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="test_proc_name"> Test Procedure Name </label>
                        <div class="col-xs-9">
                            <input type="text" id="test_proc_name" name="test_proc_name" class="form-control input-sm basic-info" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="test_proc_narrative"> Test Procedure Narrative </label>
                        <div class="col-xs-9">
                            <textarea id="test_proc_narrative" name="test_proc_narrative" class="form-control basic-info tinymce-editor"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="audit_objective"> Audit Objective </label>
                        <div class="col-xs-9">
                            <textarea id="audit_objective" name="audit_objective" class="form-control basic-info" rows="5" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="budgeted_mandays"> Budgeted Mandays </label>
                        <div class="col-xs-2">
                            <input type="number" name="budgeted_mandays" id="budgeted_mandays" class="form-control basic-info input-sm" required>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-testproc-save">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- /Test procedures modal -->
@endsection
@section('footer_script')
    <script src="{{ asset('js/iapms.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.js') }}"></script>
    <script src="{{ asset('plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/dataTables/1.10.12/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables/1.10.12/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('plugins/tinymce/jquery.tinymce.min.js') }}"></script>
    <script type="text/javascript">
        var $modal = $('#test-procedures-modal');
        var form = $('#test-procedures-form');
        var datatable= $('#test-procedures-table').DataTable( {
            ajax: "{{route('test_procedures.lists')}}",
            processing: true,
            orderCellsTop: true,
            columns: [
                { data: "test_proc_name" },
                { data: "test_proc_narrative", orderable: false},
                { data: "audit_objective", orderable: false },
                { data: "budgeted_mandays"  },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn-edit edit" data-id="'+data.test_proc_id+'" title="edit" rel="tooltip"><i class="fa fa-pencil"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.test_proc_id+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        } );

        $(function () {
            tinymce.init({
                selector: '.tinymce-editor',
                height: 300,
                menubar: false,
                plugins: [
                    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code fullscreen",
                    "insertdatetime nonbreaking save table contextmenu directionality",
                    "emoticons template paste textcolor colorpicker textpattern table"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | bullist numlist outdent indent | link | paste | table",
                paste_data_images: true,
            });
        });

        $(document).on('click', '.btn-testproc-save', function() {
            swal({
                    title: "Are you sure?",
                    text: "You are about to add a test procedure.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, save it!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function(){
                    tinyMCE.triggerSave();
                    $.ajax({
                        url: form.attr('action'),
                        method: "POST",
                        data: form.serialize(), // serializes all the form data
                        success : function(data) {
                            if( data.success ) {
                                swal("Saved!", "Test procedure is successfully saved.", "success");
                                location.reload();
                            }
                        },
                        error: function(xhr, textStatus, errorThrown){
                            swal.close();
                            $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');

                            if( xhr.status == 500 ) {
                                $('.modal-alert').removeClass('alert-warning').addClass('alert-danger');
                                $('.modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                            }
                            else {
                                // displays the validation error
                                var unique_message = [];
                                var msg = '<ul class="list-unstyled">';

                                $('.modal-input').parent().parent().removeClass('has-error');

                                $.each(xhr.responseJSON, function(key, val){
                                    for(var i=0; i < val.length; i++) {
                                        // set error class to fields
                                        $('#'+key).parent().parent().addClass('has-error');

                                        // shows error message
                                        if( unique_message.indexOf(val[i]) === -1 ) {
                                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                            unique_message.push(val[i]);
                                        }
                                    }
                                });
                                msg += '</ul>';

                                $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
                                $('.modal-alert').html(msg).show();
                            }
                        }
                    });
                });

            return false;
        });

        $(document).on('click', '.btn-edit', function() {
            var id = $(this).attr('data-id');
            var row = $(this).parent().parent();
            var data = datatable.row( row ).data();
            var action = '{{route('test_procedures.update')}}';

            form.find('#test_proc_name').val(data.test_proc_name);
            tinymce.get('test_proc_narrative').setContent(data.test_proc_narrative);
            form.find('#audit_objective').val(data.audit_objective);
            form.find('#budgeted_mandays').val(data.budgeted_mandays);
            form.find('input[name="_method"]').val('put');
            form.find('input[name="id"]').val(id);
            form.prop('action', action);

            $modal.modal('show');
            return false;
        });

        $modal.on('hidden.bs.modal', function (e) {
            form.find('#test_proc_name').val('');
            tinymce.get('test_proc_narrative').setContent('');
            form.find('#audit_objective').val('');
            form.find('#budgeted_mandays').val('');
            form.find('input[name="_method"]').val('post');
            form.find('input[name="id"]').val('');
            form.prop('action', '{{route('test_procedures.store')}}');
        });

        $(document).on('click', '.btn-delete', function() {
            var id = $(this).attr('data-id');
            swal({
                    title: "Are you sure?",
                    text: "You are about to delete a test procedure.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function(){
                    $.ajax({
                        url: '{{route('test_procedures.delete')}}',
                        method: "POST",
                        data: { _method : 'delete', _token : _token, id : id },
                        success: function (data) {
                            if (data.success) {
                                swal("Saved!", "Test procedure is successfully deleted.", "success");
                                location.reload();
                            }
                        }
                    });
                });

            return false;
        });
    </script>
@endsection