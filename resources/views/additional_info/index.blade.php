@extends('layouts.app')
@section('styles')

@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')
        <div class="page-content">
            @include('includes.content_header')
            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" data-toggle="modal" href='#additional-info-modal'>
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Additional Informations List
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="additional-info-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="20%">Additional Information Name</th>
                                <th width="50%">Description</th>
                                <th width="20%">Table</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@include('additional_info.additional_info_modal')
@endsection
@section('footer_script')
<script type="text/javascript">
    var  form    = $('#additional-information-form');

    var additional_info_table = $('#additional-info-table').DataTable( {
        ajax: "{{route('additional_information.list')}}",
        "processing": true,
        orderCellsTop: true,
        columns: [
            { data: "additional_info_name" },
            { data: "description", defaultContent: 'N/A', orderable: false },
            { data: "table_name" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('additional_information/edit')}}/'+data.additional_info_id+'"><i class="fa fa-pencil edit" title="Edit" rel="tooltip"></i></a> &nbsp; <a href="{{url('additional_information')}}/'+data.additional_info_id+'" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );    
    
    $(document).on('click', "#additional-info-table .delete" ,function(){
        $(this).deleteItemFrom(additional_info_table);  
        return false;
    });

</script>
@yield('additional_info_modal_scripts')
@endsection
