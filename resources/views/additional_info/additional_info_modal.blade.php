<!-- modal -->
<div class="modal fade" data-backdrop="static" id="additional-info-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Additional Information Form</h4>
            </div>
            <div class="modal-body">
                <div id="additional-info-alert"></div>
                <form action="{{ route('additional_information.store')}}" id="additional-info-form" class="form-horizontal" method="POST">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="name"> Additional Information Name </label>
                        <div class="col-sm-9">
                            <input type="text" name="name" id="name" class="form-control input-sm" value="{{ isset($details) ? $details->additional_info_name : '' }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="description"> Description </label>
                        <div class="col-sm-9">
                            <textarea name="description" id="description" class="form-control" rows="3" style="resize: none;">{{ isset($details) ? $details->description : '' }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="table"> Table Name </label>
                        <div class="col-sm-9">
                            <select type="text" name="table" id="table" class="form-control input-sm" placeholder="Please select">
                            </select>
                        </div>
                    </div>
                    <div class="form-row align-right" >
                        <button class="btn btn-info btn-sm">
                            <i class="ace-icon fa fa-save bigger-120"></i>
                            Apply
                        </button>
                        <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /modal -->
@section('additional_info_modal_scripts')
<script type="text/javascript">
    var table_select = $("#table").makeSelectize({ 
        lookup : '{{ config('iapms.lookups.additional_information.tables') }}' 
    })[0].selectize;

    $("#additional-info-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#additional-info-form")[0].reset();
                $("#additional-info-modal").modal('hide');
                table_select.clear();
                additional_info_table.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#additional-info-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

</script>
@endsection