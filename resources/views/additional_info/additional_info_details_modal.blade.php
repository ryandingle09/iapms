<!-- Additional Info Details modal -->
<div class="modal fade" data-backdrop="static" id="additional-info-details-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Additional Information Form</h4>
            </div>
            <div class="modal-body">
                <div id="additional-info-details-alert"></div>
                <form class="form-horizontal" method="{{ route('additional_information_details.store', ['id' => $details->additional_info_id])}}" id="additional-info-details-form"> 
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="prompt"> Prompt </label>
                        <div class="col-sm-8">
                            <input type="text" name="prompt" id="prompt" class="form-control" value="" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="attribute"> Attribute Assignment </label>
                        <div class="col-sm-8">
                            <select id="attribute" name="attribute" class="form-control" data-placeholder="Click to Choose...">
                                
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="data_type"> Data Type </label>
                        <div class="col-sm-5">
                            <select name="data_type" id="data_type" class="form-control basic-info" >
                                <option value="">---</option>
                                <option value="integer">Integer</option>
                                <option value="string">String</option>
                                <option value="date">Date</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="value_set"> Value Set </label>
                        <div class="col-sm-8">
                            <select id="value_set" name="value_set" class="form-control basic-info" data-placeholder="Click to Choose...">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="required"> Required </label>
                        <div class="col-sm-3">
                            <select name="required" required class="form-control basic-info">
                                <option value="Y">Yes</option>
                                <option value="N">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="remarks"> Remarks </label>
                        <div class="col-sm-8">
                            <textarea name="remarks" id="remarks" class="form-control" rows="3" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="form-row align-right" >
                        <button class="btn btn-info btn-sm">
                            <i class="ace-icon fa fa-save bigger-120"></i>
                            Apply
                        </button>
                        <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Additional Info Details modal -->
@section('additional_info_details_modal_scripts')
<script type="text/javascript">
    var attribute_select = $("#attribute").makeSelectize({ 
        lookup : '{{ config('iapms.lookups.additional_info_attr') }}'
    })[0].selectize;

    var value_set_select = $("#value_set").makeSelectize({
        url : '{{ route('value_set.list')}}' + '?format=json'
    })[0].selectize;

    $("#additional-info-details-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#additional-info-details-form")[0].reset();
                $("#additional-info-details-modal").modal('hide');
                attribute_select.clear();
                table_select.clear();
                value_set_select.clear();
                info_details_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#additional-info-details-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on('click', "#info-details-table .edit" ,function(){
        var url = $(this).attr('href');
        var row = $(this).parent().parent();
        var data = info_details_datatable.row( row ).data();
        console.log(data);
        $("#additional-info-details-form").attr('action',url);
        $("#additional-info-details-form").attr('method','PUT').supply(data);
        attribute_select.setValue(data.attribute_assignment);
        value_set_select.setValue(data.value_set_id);
        $("#additional-info-details-modal").modal('show');
        return false;
    });


</script>
@endsection