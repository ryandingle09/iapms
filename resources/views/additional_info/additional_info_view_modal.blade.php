<!-- View modal -->
<div class="modal fade" data-backdrop="static" id="view-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">View: Additional Information</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /View modal -->
@section('additional_info_view_modal_scripts')
<script type="text/javascript">
    $('.btn-view-info').on('click', function() {
        $.ajax({
            url: '{{route('additional_information.show', ['id' => $details->additional_info_id])}}', // url based on the form action
            method: "GET",
            type: "html",
            data: {_token : '{{csrf_token()}}'},
            beforeSend : function() {
                swal({
                    title: "Loading",
                    text: "Please wait...",
                    showConfirmButton: false
                });
            },
            success : function(data) {
                swal.close();
                $('#view-modal').find('.modal-body').html(data);
                $('#view-modal').modal('show');
            }
        });
    });
</script>
@endsection