@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')
            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Additional Information Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <form action="{{ route('additional_information.update', ['id' => $details->additional_info_id ]) }}" class="form-horizontal" id="additional-info-form">
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="name"> Additional Information Name </label>
                                        <div class="col-sm-6">
                                            <input type="text" name="name" id="name" class="form-control input-sm" value="{{ isset($details) ? $details->additional_info_name : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="description"> Description </label>
                                        <div class="col-sm-6">
                                            <textarea name="description" id="description" class="form-control" rows="3" style="resize: none;">{{ isset($details) ? $details->description : '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="table"> Table Name </label>
                                        <div class="col-sm-6">
                                            <select type="text" name="table" id="table" class="form-control input-sm" placeholder="Please select">
                                                <option></option>
                                                @if(isset($details))
                                                <option value="{{$details->table_name}}" selected>{{$details->table_name}}</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-row align-right">
                                        <a href="#" class="btn btn-info btn-sm btn-view-info">
                                            <i class="ace-icon fa fa-eye bigger-120"></i>
                                            View
                                        </a>
                                        <button class="btn btn-primary btn-sm">
                                            <i class="ace-icon fa fa-save bigger-120"></i>
                                            Save
                                        </button>
                                        <a href="{{ route('additional_information.index') }}" class="btn btn-warning btn-sm">
                                            <i class="ace-icon fa fa-times bigger-120"></i>
                                            Cancel
                                        </a>
                                    </div>
                                </form>
                                
                                <div class="row">
                                    <div class="col-xs-12">
                                        
                                    </div>

                                    <div class="col-xs-12">
                                        <a class="btn btn-primary btn-sm" id="show-additional-info-details-modal">
                                            <i class="ace-icon fa fa-plus bigger-120"></i>
                                            Create
                                        </a>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="table-header"> Additional Informations </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <table id="info-details-table" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="10%">Sequence</th>
                                                    <th width="15%">Prompt</th>
                                                    <th width="15%">Attribute Assignment</th>
                                                    <th width="10%">Date Type</th>
                                                    <th width="15%">Value Set</th>
                                                    <th width="10%">Required</th>
                                                    <th width="20%">Remarks</th>
                                                    <th width="5%"></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@include('additional_info.additional_info_details_modal')
@include('additional_info.additional_info_view_modal')
@endsection

@section('footer_script')
<script type="text/javascript">
    var table_select = $("#table").makeSelectize({ 
        lookup : '{{ config('iapms.lookups.additional_information.tables') }}'
    })[0].selectize;

    $("#additional-info-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : "PUT",
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response) {
                $.sSuccess(response.message);
            }
        });
        return false;
    });

    var details_url = "{{ url('additional_information/'.$details->additional_info_id.'/details') }}";

    var info_details_datatable = $('#info-details-table').DataTable( {
        ajax: details_url + "/list",
        "searching": false,
        "processing": true,
        "serverSide": true,
        paging: false,
        info: false,
        orderCellsTop: true,
        columns: [
            {
                data: null,
                searchable : false,
                render : function(data, type, full, meta) {
                    return meta.row + 1;
                }
            },
            { data: "prompt" },
            { data: "attribute_assignment" },
            { data: "data_type" },
            { data: "value_set.value_set_name", defaultContent: 'N/A' },
            { data: "required" },
            { data: "remarks", defaultContent: 'N/A', orderable: false },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+details_url+'/'+data.id+'" class="edit" title="Edit" rel="tooltip"><i class="fa fa-pencil"></i></a> &nbsp; <a href="'+details_url+'/'+data.id+'" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    });


    $(document).on('click', "#show-additional-info-details-modal" ,function(){
        $("#additional-info-details-form").attr('action',details_url);
        $("#additional-info-details-form").attr('method','POST');
        $("#additional-info-details-modal").modal('show');
        return false;
    });

    $(document).on('click', "#info-details-table .delete" ,function(){
        $(this).deleteItemFrom(additional_info_table);  
        return false;
    });

</script>
@yield('additional_info_details_modal_scripts')
@yield('additional_info_view_modal_scripts')
@endsection
