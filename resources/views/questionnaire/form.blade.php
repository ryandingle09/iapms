@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/js/jquery-ui-1.11.4.sortable/jquery-ui.min.css" />
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Questionnaire
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm btn-save" type="button">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button>
                                            <a href="{{route('questionnaire.index')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div style="display:none;" class="alert alert-warning alert-main"></div>
                                {!! Form::open(['url' => isset($questionnaire) ? route('questionnaire.update', ['id' => $questionnaire->questionnaire_id]) : route('questionnaire.store'), 'class' => 'form-horizontal', 'id' => 'questionnaire-form']) !!}
                                    <input type="hidden" name="details" value="{{ isset($questionnaire) ? ( $questionnaire->details->count() ? '1' : '' ) : '1' }}">
                                    @if(isset($questionnaire))
                                        <input type="hidden" name="_method" value="put">
                                        <input type="hidden" name="id" value="{{$questionnaire->questionnaire_id}}">
                                    @endif
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label modal-input" for="control_seq"> Questionnaire Name </label>
                                                <div class="col-sm-6">
                                                    <input type="text" required class="form-control input-sm" id="name" name="name" value="{{ isset($questionnaire) ? $questionnaire->questionnaire_name : '' }}" min="1" step="1">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label" for="description"> Description </label>
                                                <div class="col-xs-6">
                                                    <textarea id="description" name="description" class="form-control" rows="5" style="resize: none;">{{ isset($questionnaire) ? $questionnaire->description : '' }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label modal-input" for="question_type"> 
                                                    <a href="{{ lookup_url('iapms.lookups.questionnaire.type') }}" class="shortcut" title="Create Lookup" target="_blank" rel="tooltip"> Type </a>
                                                </label>
                                                <div class="col-xs-6">
                                                    <select id="question_type" name="question_type" required class="form-control input-sm basic-info">
                                                        <option value="">---</option>
                                                        @if( isset($config['question_type']) )
                                                            @foreach($config['question_type']->lookupValue as $question_type)
                                                                <option value="{{$question_type->lookup_code}}" {!! isset($questionnaire) ? ($questionnaire->questionnaire_type == $question_type->lookup_code ? 'selected="selected"' : '') : '' !!} data-desc="{{$question_type->description}}">{{$question_type->meaning}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label modal-input" for="question_type"> Type Description </label>
                                                <div class="col-xs-8">
                                                    <span class="label label-primary"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12">
                                            <div class="table-header"> Questions </div>
                                            <div class="widget-container-col">
                                                @if( isset($questionnaire) )
                                                    @if( $questionnaire->details->count() )
                                                        @foreach( $questionnaire->details as $q )
                                                            @include('questionnaire.template.question', ['q' => $q])
                                                        @endforeach
                                                    @endif
                                                @else
                                                    @include('questionnaire.template.question')
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <button class="btn btn-success btn-sm btn-add-question" type="button">
                                                        <i class="ace-icon fa fa-plus bigger-120"></i>
                                                        Add Question
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="pull-right">
                                                <button class="btn btn-primary btn-sm btn-save" type="button">
                                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                                    Save
                                                </button>
                                                <a href="{{route('questionnaire.index')}}" class="btn btn-warning btn-sm">
                                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                                    Cancel
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<script type="text/template" id="question-details">
    @include('questionnaire.template.question', ['config' => $config])
</script>
<script type="text/template" id="question-response-choices">
    <div class="form-group">
        <label class="col-xs-2 control-label modal-input"> 
            <a href="{{ lookup_url('iapms.lookups.questionnaire.choices') }}" class="shortcut" title="Create Lookup" target="_blank" rel="tooltip">Choices Name </a>
        </label>
        <div class="col-xs-4">
            <select name="questionnaire[__seq__][choices]" required class="form-control input-sm basic-info multiple-choice">
                <option value="">--- Select Type ---</option>
                @if( isset($config['choices']) )
                    @foreach($config['choices']->lookupValue as $choices)
                        <option value="{{$choices->lookup_code}}">{{$choices->meaning}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-2 control-label modal-input">&nbsp;</label>
        <div class="choices-container col-sm-6"></div>
    </div>
</script>
<script type="text/template" id="question-response-range">
    <div class="form-group">
        <label class="col-xs-2 control-label modal-input"> Range </label>
        <div class="col-xs-3">
            <input type="number" name="questionnaire[__seq__][range][from]" id="input" step="1" min="0" class="form-control range-from" required="required">
        </div>
        <div class="col-xs-3">
            <input type="number" name="questionnaire[__seq__][range][to]" id="input" step="1" min="1" class="form-control range-to" required="required">
        </div>
    </div>
</script>
@endsection

@section('footer_script')
    <script src="/js/jquery-ui-1.11.4.sortable/jquery-ui.min.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>

    <script type="text/javascript">
        var form               = $('#questionnaire-form'),
            question_container = $('.widget-container-col'),
            question_template = $('#question-details').html(),
            risk_type   = $('#risk_type'),
            source_type = $('#source_type'),
            response_type = $('#response_type'),
            ent_risk_type = $('#ent_risk_type'),
            ent_risk_class = $('#ent_risk_class'),
            ent_risk_area = $('#ent_risk_area'),
            $alert       = $('.alert-main'),
            $modal      = $('#riskscontrol-form');

        var $sortable,
            dataSet = [];

         @if (Session::has('message'))
            $alert.html('{!! session('message') !!}').show();
        @endif

        $(function(){
            $( ".widget-container-col" ).sortable({
                cancel: '.fullscreen',
                opacity:0.8,
                revert:true,
                forceHelperSize:true,
                placeholder: 'widget-placeholder',
                forcePlaceholderSize:true,
                handle: '.handle'
            });

            $( ".widget-container-col" ).on( "sortstop", function( e, ui ) {
                $(this).find('.widget-question').each( function(q) {
                    var seq = $(this).index();
                    var number = seq + 1;
                    $(this).find('.question-number').html(number);
                    $(this).find('.form-control').each(function(){
                        var name = $(this).attr('name');
                        var regex = /(questionnaire)(\[)[0-9]|__seq__(\])/; //questionnaire name regex
                        $(this).attr('name', name.replace(regex, '$1$2'+seq+'$3'));
                    });
                });
            } );

            $( ".widget-container-col" ).trigger('sortstop');
            $( ".response-type" ).trigger('change');
            @if(isset($questionnaire))
            $('#question_type').trigger('change');
            @endif
        });

        $('body').on('change', '.response-type', function(){

            var response = $(this).parent().parent().parent().find('.response-containter');
            var seq = $(this).parent().parent().parent().parent().parent().index();
            var selected = $(this).val();
            var choice = $(':selected', this).attr('data-choice');
            var range_from = $(':selected', this).attr('data-from');
            var range_to = $(':selected', this).attr('data-to');
            var html = '';

            if( selected == 'Choices' ) {
                html = $('#question-response-choices').html();
            }
            if( selected == 'Range' ) {
                html = $('#question-response-range').html();
            }
            response.html( html.replace(/__seq__/g, seq) );

            // auto-select multiple choice if not undefined
            if( choice != undefined ) {
                response.find('.multiple-choice').val(choice).trigger('change');
            }
            // auto-populate range if not undefined
            if( range_from != undefined ) {
                response.find('.range-from').val(range_from);
                response.find('.range-to').val(range_to);
            }
        });

        $('body').on('change', '.multiple-choice', function(){
            var choices_container = $(this).parent().parent().next().find('.choices-container');
            var selected = $(this).val();
            $.get( "{{url('administrator/lookup/type')}}/"+selected, function( data ) {
                var html = '<ul class="list-unstyled spaced">';
                if( data.data.length ) {
                    $.each(data.data, function(i, v){
                        html += '<li><i class="ace-icon fa fa-circle green"></i> '+v.meaning+'</li>';
                    });
                }
                else {
                    var url ="{{ url('administrator/lookup/edit') }}/"+selected;
                    html += '<li><i class="ace-icon fa fa-info-circle blue"></i> No choices available. <a href="'+url+'" target="_blank">Click here to create.</a></li>';
                }
                html += '</ul>'
                choices_container.html(html);
            });

        });

        $('.btn-add-question').on('click', function(){
            question_container.append(question_template);
            question_container.find('.widget-box:last').find('.form-control').val('');
            question_container.find('.widget-box:last').find('.widget-toolbar a').removeAttr('data-seq');
            var question_number = question_container.find('.widget-question').length;
            question_container.find('.widget-question:last').find('.question-number').html(question_number);
            question_container.find('.widget-question:last').find('.resp_required').val('Y');
            question_container.find('.widget-question:last').find('.page_break_after').val('N');
            question_container.find('.widget-question:last').find('.optional_remarks').val('N');
            $( ".widget-container-col" ).trigger('sortstop');
            $('input[name="details"]').val('1');
        });

        $('body').on('click', '.remove-question', function(){
            var widget_question = $(this).parent().parent().parent();
            var q_number = widget_question.find('.question-number').html();
            swal({
                title: "Continue?",
                text: "You are about to remove question #"+q_number+".",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, remove it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function() {
                @if(isset($questionnaire))
                var q_seq = widget_question.find('.widget-toolbar a').attr('data-seq');
                if( q_seq != undefined ) {
                    $.ajax({
                        url: "{{url('questionnaire').'/'.$questionnaire->questionnaire_id}}/delete/question/"+q_seq, // url based on the form action
                        method: "POST",
                        data: { _token : '{{csrf_token()}}', _method : 'delete' }, // serializes all the form data
                        success : function(data) {
                            if( data.success ) {
                                swal("Success!", "Question is successfully removed.", "success");
                                location.reload();
                            }
                        }
                    });
                }
                @endif
                widget_question.remove();
                $( ".widget-container-col" ).trigger('sortstop');
                swal.close();
                $('input[name="details"]').val($('.widget-question').length);
            });
            return false;
        });

        $('.btn-save').on('click', function(){
            $(".alert").remove();
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : sLoading(),
                success : function(data) {
                    if( data.success ) {
                        $.sSuccess(data.message,function(){
                            window.location = '{{ route('questionnaire.index') }}';
                        });
                    }
                },
                error: function (xhr) {
                    switch(parseInt(xhr.status)){
                        case 422:
                            swal.close();
                            // displays the validation error
                            var unique_message = [];
                            var firstKey = null;
                            var msg = '<ul class="list-unstyled">';var test = 0;
                            $.each(xhr.responseJSON, function(key, val){
                                
                                for(var i=0; i < val.length; i++) {
                                    console.log(key)
                                    // set error class to fields
                                    var elemName = key;
                                    if(key.indexOf("questionnaire.") !== -1){
                                        var parts = key.split(".");
                                        var elemName = parts[0] + "[" + parts[1] + "]["+ parts[2] + "]";
                                        if(parts.length == 4){
                                            elemName = parts[0] + "[" + parts[1] + "]["+ parts[2] + "]["+ parts[3] + "]";
                                        }   
                                    }

                                    $("[name='" +elemName+"']").parents('.form-group').addClass("has-error");
                                    if(i == 0) firstKey = elemName;

                                    // shows error message
                                    if( unique_message.indexOf(val[i]) === -1 ) {
                                        msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                        unique_message.push(val[i]);
                                    }
                                }
                            });
                                
                            msg += '</ul>';

                            $alert.removeClass('alert-danger').addClass('alert-warning');
                            $alert.html(msg).show();
                            $("[name='" +firstKey+"']").scroll({ adjustment : -100 });
                            break;
                        case 500:
                            sError("Error "+xhr.status+" : "+xhr.statusText);
                        break;
                    }
                }
            });
        });

        $('#question_type').on('change', function(){
            var desc = $('option:selected', this).attr('data-desc');
            var parent = $(this).parent().parent();
            parent.next().find('.label').html(desc);
        });
    </script>
@endsection