<div class="widget-box widget-color-orange ui-sortable-handle widget-question">
    <!-- #section:custom/widget-box.options.collapsed -->
    <div class="widget-header widget-header-small handle">
        <h6 class="widget-title">
            <i class="ace-icon fa fa-hashtag"></i>
            <span class="question-number">{{ isset( $q ) ? $q->question_sequence : '1' }}</span>
        </h6>

        <div class="widget-toolbar">
            <a href="#" data-action="close" class="remove-question" {{ isset( $q ) ? 'data-seq='.$q->question_sequence : '' }}>
                <i class="ace-icon fa fa-times"></i>
            </a>
        </div>
    </div>

    <!-- /section:custom/widget-box.options.collapsed -->
    <div class="widget-body">
        <div class="widget-main">
            <input type="hidden" class="form-control" name="questionnaire[__seq__][seq]" value="{{ isset( $q ) ? $q->question_sequence : '' }}">
            <div class="form-group">
                <label class="col-xs-2 control-label modal-input"> Question </label>
                <div class="col-sm-6">
                    <textarea name="questionnaire[__seq__][question]" class="form-control" rows="5" style="resize: none;">{{ isset($q) ? $q->question : '' }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label modal-input"> Response Required? </label>
                <div class="col-xs-1">
                    <select name="questionnaire[__seq__][required]" required class="form-control input-sm basic-info resp_required">
                        <option value="Y" {!! isset($q) ? ($q->response_required == 'Y' ? 'selected="selected"' : '') : '' !!}>Yes</option>
                        <option value="N" {!! isset($q) ? ($q->response_required == 'N' ? 'selected="selected"' : '') : '' !!}>No</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label modal-input"> Page Break After </label>
                <div class="col-xs-1">
                    <select name="questionnaire[__seq__][page_break_after]" required class="form-control input-sm basic-info page_break_after">
                        <option value="N" {!! isset($q) ? ($q->page_break_after == 'N' ? 'selected="selected"' : '') : '' !!}>No</option>
                        <option value="Y" {!! isset($q) ? ($q->page_break_after == 'Y' ? 'selected="selected"' : '') : '' !!}>Yes</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label modal-input"> Optional Remarks </label>
                <div class="col-xs-1">
                    <select name="questionnaire[__seq__][optional_remarks]" required class="form-control input-sm basic-info optional_remarks">
                        <option value="N" {!! isset($q) ? ($q->optional_remarks == 'N' ? 'selected="selected"' : '') : '' !!}>No</option>
                        <option value="Y" {!! isset($q) ? ($q->optional_remarks == 'Y' ? 'selected="selected"' : '') : '' !!}>Yes</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label"> Data Type </label>
                <div class="col-xs-4">
                    <select name="questionnaire[__seq__][datatype]" required class="form-control input-sm basic-info">
                        <option value="">---</option>
                        @if( isset($config['datatype']) )
                            @foreach($config['datatype']->lookupValue as $datatype)
                                <option value="{{$datatype->lookup_code}}" {!! isset($q) ? ($q->data_type == $datatype->lookup_code ? 'selected="selected"' : '') : '' !!}>{{$datatype->meaning}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-xs-2 control-label"> Response Type </label>
                <div class="col-xs-4">
                    <select name="questionnaire[__seq__][response]" required class="form-control input-sm basic-info response-type">
                        <option value="">---</option>
                        @if( isset($config['response_type']) )
                            @foreach($config['response_type']->lookupValue as $response_type)
                                <option value="{{$response_type->lookup_code}}" {!! isset($q) ? ($q->response_type == $response_type->lookup_code ? 'selected="selected" data-choice="'.$q->choices_name.'" data-from="'.$q->range_from.'" data-to="'.$q->range_to.'"' : '') : '' !!}>{{$response_type->meaning}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="response-containter"></div>
        </div>
    </div>
</div>