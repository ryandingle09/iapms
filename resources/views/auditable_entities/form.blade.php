@extends('layouts.app')

@section('styles')
    <style type="text/css">
        #custom-measure-modal table th { text-align: center; }
        .selected-row {
            background-color: #ffe99e !important;
        }
        .table-selectable tbody > tr:hover {
            cursor: pointer;
        }
    </style>
    <link rel="stylesheet" href="/plugins/selectize/selectize.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.bootstrap3.css" />
    <link rel="stylesheet" href="/css/selectize-custom.css" />
    <link rel="stylesheet" href="/css/bootstrap-duallistbox.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Auditable Entity Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                {{ (isset($details)) ? 'Save' : 'Save &amp; proceed to business process' }}
                                            </button>
                                            <a href="{{ Request::is('auditable_entities/master/*') ? route('auditable_entities.master') : route('auditable_entities') }}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        @if (Session::has('message'))
                                        <div class="alert alert-success alert-message">
                                            <button type="button" class="close" data-dismiss="alert">
                                                <i class="ace-icon fa fa-times"></i>
                                            </button>

                                            <strong>
                                                <i class="ace-icon fa fa-check"></i>

                                            </strong>

                                            {!! session('message') !!}
                                            <br>
                                        </div>
                                        @endif
                                        <div class="table-header"> Auditable Entity </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div style="display:none;" class="alert alert-warning alert-main alert-form-warning"></div>
                                        <?php
                                            if( Request::is('auditable_entities/master/*') )
                                                $form_action = isset($details) ? route('auditable_entities.master.update', ['id' => $details->auditable_entity_id]) : route('auditable_entities.master.store');
                                            else
                                                $form_action = isset($details) ? route('auditable_entities.update', ['id' => $details->auditable_entity_id]) : route('auditable_entities.store');
                                        ?>
                                        {!! Form::open(['url' => $form_action, 'class' => 'form-horizontal', 'id' => 'auditable-entities-form', 'style' => 'margin-top: 12px;']) !!}
                                            <input type="hidden" name="id" value="{{ isset($details) ? $details->auditable_entity_id : '' }}">
                                            <input type="hidden" name="_type" value="{{ Request::is('auditable_entities/master/*') ? 'master' : 'actual' }}">
                                            <input type="hidden" id="company_id" name="company_id" value="{{ isset($details) ? $details->company_id : '' }}">
                                            @if(isset($details))
                                                <input type="hidden" name="_method" value="PUT">
                                            @endif
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="name"> IDL </label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="name" id="name" class="form-control input-sm" {!! isset($details) ? 'value="'.$details->auditable_entity_name.'"' : '' !!} readonly>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="company"> Company </label>
                                                <div class="col-sm-6">
                                                    @if(isset($details))
                                                        <input type="hidden" name="company" value="{{$details->company_code}}">
                                                        <span class="label label-info label-wrap">{{$details->lv_company_code_meaning}}</span>
                                                    @else
                                                        <select id="company" name="company" class="form-control input-sm basic-info lookup" placeholder="Select a company..." {{ isset($details) ? 'disabled' : 'required' }}>
                                                            @if(isset($details))
                                                                <option value="{{ $details->company_code }}" selected>{{ $details->company_code }}</option>
                                                            @endif
                                                        </select>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="company_desc"> Company Code Description </label>
                                                <div class="col-sm-6" id="company_desc">
                                                    <span class="label label-info label-wrap">{{ isset($details) ? $details->lv_company_code_desc : '' }}</span>
                                                </div>
                                            </div>
                                            @if(!Request::is('auditable_entities/master/*'))
                                                @include('auditable_entities.actual', [isset($details) ? 'details' : '' => isset($details) ? $details : ''])
                                            @else
                                                @include('auditable_entities.master', [isset($details) ? 'details' : '' => isset($details) ? $details : ''])
                                            @endif
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="department_code"> Department </label>
                                                <div class="col-sm-6">
                                                    @if(isset($details))
                                                        <input type="hidden" name="department_code" value="{{$details->department_code}}">
                                                        <span class="label label-info">{{ $details->department_code != '' ? $details->department_code : 'Default' }}</span>
                                                    @else
                                                        <select id="department_code" name="department_code" class="form-control input-sm basic-info lookup" placeholder="Select a department..." required></select>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="department_desc"> Department Description </label>
                                                <div class="col-sm-8" id="department_desc">
                                                    <span class="label label-info">{{ isset($details) ? $details->lv_department_code_desc : '' }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="contact_name"> Contact Name </label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="contact_name" id="contact_name" class="form-control input-sm" value="{{ isset($details) ? $details->contact_name : '' }}" required="required">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="head_name"> Entity Head Name </label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="head_name" id="head_name" class="form-control input-sm" value="{{ isset($details) ? $details->entity_head_name : '' }}" required="required">
                                                </div>
                                            </div>
                                            @yield('entity_branch_details')
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                @if(isset($details))
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button type="button" class="btn btn-primary btn-sm btn-add-info" data-id="{{$details->auditable_entity_id}}"><i class="fa fa-list-ul"></i> Additional Info</button>
                                        @if($details->branch_code == '' && $details->department_code == '' )
                                        <button type="button" class="btn btn-success btn-sm btn-create-details" data-toggle="modal" data-target="#branch-codes-modal"><i class="fa fa-list"></i> Add Branch Entities</button>
                                        @endif
                                    </div>
                                </div>
                                    @if($details->branch_code != '' && $details->department_code != '' )
                                    <br/>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="widget-container-col">
                                                <div class="widget-box">
                                                    <div class="widget-header widget-header-small">
                                                        <h5 class="widget-title smaller">Business Process</h5>

                                                        <!-- #section:custom/widget-box.tabbed -->
                                                        <div class="widget-toolbar no-border">
                                                            <ul class="nav nav-tabs" id="businessProcessTab">
                                                                <li class="active">
                                                                    <a data-toggle="tab" href="#sub" aria-expanded="true">Sub-BP</a>
                                                                </li>
                                                                <li class="">
                                                                    <a data-toggle="tab" href="#main" aria-expanded="false">Main BP</a>
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        <!-- /section:custom/widget-box.tabbed -->
                                                    </div>

                                                    <div class="widget-body">
                                                        <div class="widget-main padding-6">
                                                            <div class="tab-content">
                                                                <div id="sub" class="tab-pane active">
                                                                    @if( in_array($details->company_code, $master_companies) )
                                                                        <button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#bp-modal">
                                                                            <i class="ace-icon fa fa-plus bigger-120"></i>
                                                                            Add
                                                                        </button>
                                                                    @endif
                                                                    <table id="bp-table" class="table table-striped table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Business Process IDL</th>
                                                                                <th>Business Process Code</th>
                                                                                <th>Business Process Description</th>
                                                                                <th>Source Type</th>
                                                                                <th>Source Type Description</th>
                                                                                @if( in_array($details->company_code, $master_companies) )
                                                                                <th width="5%"> </th>
                                                                                @endif
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                                <div id="main" class="tab-pane">
                                                                    @if( in_array($details->company_code, $master_companies) )
                                                                        <button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#bp-main-modal">
                                                                            <i class="ace-icon fa fa-plus bigger-120"></i>
                                                                            Add
                                                                        </button>
                                                                    @endif
                                                                    <!-- Main process -->
                                                                    <div class="table-header"> Main Business Process </div>
                                                                    <table id="bp-main-table" class="table table-striped table-bordered table-hover table-selectable">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Main Process Code</th>
                                                                                <th>Main Process Name</th>
                                                                                <th width="5%"></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                    <!-- /Main process -->
                                                                    <!-- Sub process -->

                                                                    @if( in_array($details->company_code, $master_companies) )
                                                                        <button class="btn btn-default btn-sm btn-sub-bp" type="button" data-toggle="modal" data-target="#sub-bp-modal" style="display: none;">
                                                                            <i class="ace-icon fa fa-plus bigger-120"></i>
                                                                            Add
                                                                        </button>
                                                                    @endif
                                                                    <div class="table-header"> Sub Process </div>
                                                                    <table id="bp-sub-table" class="table table-striped table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>Sequence</th>
                                                                                <th>Business Process IDL</th>
                                                                                <th>Business Process Code</th>
                                                                                <th>Business Process Description</th>
                                                                                <th>Source Type</th>
                                                                                <th>Source Type Description</th>
                                                                                <th width="5%"></th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        </tbody>
                                                                    </table>
                                                                    <!-- /Sub process -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                @endif
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                {{ (isset($details)) ? 'Save' : 'Save &amp; proceed to business process' }}
                                            </button>
                                            <a href="{{ Request::is('auditable_entities/master/*') ? route('auditable_entities.master') : route('auditable_entities') }}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@if(isset($details))
    @include('auditable_entities.includes.modal', ['details' => $details])
    @include('includes.modals.custom_measures', ['ae_id' => $details->auditable_entity_id])
    @include('includes.modals.additional_information')

    <script type="text/template" id="branch-codes-template">
        <tr>
            <td>
                <select name="branch_code[]" class="form-control input-sm basic-info branch_code" data-placeholder="Click to Choose..."></select>
            </td>
            <td></td>
        </tr>
    </script>
@endif
@endsection

@section('footer_script')
    <script src="/js/iapms.js"></script>
    <script src="/js/jquery.validate.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/plugins/selectize/selectize.js"></script>
    <script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
    <script src="/js/jquery.bootstrap-duallistbox.js"></script>
    <script type="text/javascript">
        var xhr;
        var select_company, $select_company;
        var select_branch, $select_branch;
        var select_branch_ca, $select_branch_ca;
        var select_dept, $select_dept;
        var select_parent, $select_parent;
        var select_sub_bp, $select_sub_bp;
        var select_main_bp, $select_main_bp;
        var select_group_type, $select_group_type;
        var select_entity_class, $select_entity_class;
        var ai_modal = $('#additional-information-modal');
        var form        = $('#auditable-entities-form'),
            company     = $('#company'),
            branch      = {!! Request::is('auditable_entities/master/*') ? "$('input[name=\"branch\"]')" : "$('#branch')" !!},
            department = $('#department_code'),
            group_type = $('#group_type_st'),
            entity_class = $('#entity_class_st'),
            $alert      = $('.alert-main'),
            $modal      = $('#bp-modal'),
            $bc_modal    = $('#branch-codes-modal');
        var $datatable, bpmain_datatable, bpsub_datatable;
        var bps;
        var master_companies = {!! json_encode($master_companies) !!};
        var company_code = '', company_name = '', branch_name = '', dept_name = '';
        var lookup_url = '{{url('/administrator/lookup/type')}}';
        var branches = [];

        function clearCompanyBranchDept() {
            @if(!Request::is('auditable_entities/master/*'))
            @if(!isset($details))
            select_branch.disable();
            select_dept.disable();
            select_branch.clearOptions();
            select_dept.clear();
            @endif

            $('#group_type span').html('');
            $('#business_type span').html('');
            @endif
            $('#company_desc span').html('');
            $('#branch_desc span').html('');
            $('#department_desc span').html('');
        }

        function generateEntityName() {
            var idl;
            @if(!Request::is('auditable_entities/master/*'))
                idl = typeof select_company.options[company.val()] != "undefined" ? select_company.options[company.val()].meaning : '';
                idl += branch.val() ? '.'+(typeof select_branch.options[branch.val()] != "undefined" ? select_branch.options[branch.val()].meaning : '.Default') : '.Default';
            @else
                idl = typeof select_company.options[company.val()] != "undefined" ? select_company.options[company.val()].text : '';
                idl += branch.val() ? '.'+branch.val() : '.Default';
            @endif
            idl += department.val() ? '.'+department.val() : '.Default';
            return idl;
        }

        $('#bp-main-table').find('tbody').on('click', 'tr', function () {
            var data = bpmain_datatable.row( this ).data();
            var sub_data = [];
            if( typeof data != "object" ) return false;
            if( typeof data.sub_bp_details != "undefined" ) {
                sub_data = data.sub_bp_details;
            }
            else {
                for (var sub in data.sub_bp) {
                    sub_data.push(data.sub_bp[sub].business_process);
                }
            }
            $('.main_bp_name span').html(data.main_bp_name);
            $('#sub-bp-modal').find('#sp_seq').val(sub_data.length + 1);
            $('input[name="mbp_id"]').val(data.ae_mbp_id);
            $('.btn-sub-bp').show();

            $('#bp-main-table').find('tbody > tr').removeClass('selected-row'); // reset tr highlights
            $(this).addClass('selected-row');

            bpsub_datatable.clear();
            bpsub_datatable.rows.add(sub_data).draw();
        } );

        $(function(){
            // selectize
            @if(!isset($details))
            $select_company = company.selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: ['text'],
                options: [],
                @if(!isset($details))
                preload : true,
                @endif
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.text) + '</span>' +
                            '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ Request::is('auditable_entities/master/*') ? config('iapms.lookups.master_company') : config('iapms.lookups.company') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onChange: function(value) {
                    clearCompanyBranchDept();
                    if (!value.length) return;
                    var company = select_company.options[value];
                    var company_code = company.id;

                    @if(!Request::is('auditable_entities/master/*'))
                    if( select_branch.getValue() == '' && select_dept.getValue() == '' ) {
                        $('#entity_class span').html('Company');
                    }
                    select_branch.load(function(callback) {
                        xhr && xhr.abort();
                        xhr = $.ajax({
                            url: '{{url('company_profile')}}/'+encodeURIComponent(company_code)+'/branches',
                            success: function(results) {
                                branches = results.data || results;
                                select_branch.enable();
                                callback(branches);
                            },
                            error: function() {
                                callback();
                            }
                        })
                    });

                    $.ajax({
                        url: '{{ url('company_profile') }}/'+encodeURIComponent(company_code)+'?filter=company_id;business_type;group_type',
                        type: 'GET',
                        beforeSend: function() {
                            toggle_loading();
                        },
                        success: function(res) {
                            $('#group_type span').html(res.group_type);
                            $('#business_type span').html(res.business_type);

                            $('#company_id').val(res.company_id);
                            toggle_loading();
                        }

                    });
                    @else
                        select_entity_class.setValue('Company');
                        select_dept.disable();
                        $('#business_type span').html(company.text);
                        $('input[name="business_type"]').val(company.text);
                    @endif
                    $('#name').val(generateEntityName());
                    $('#company_desc span').html(select_company.options[value].description);
                }
            });
            select_company = $select_company[0].selectize;
            @endif

            @if(!Request::is('auditable_entities/master/*') && !isset($details))
            $select_branch = branch.selectize({
                valueField: 'branch_code',
                labelField: 'meaning',
                searchField: ['description'],
                options: [],
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.branch_code) + '</span>' +
                            '<span class="by">' + escape(item.description) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                onDelete: function() {
                    $('#branch_desc span').html('');
                    $('#entity_class span').html('Company');
                    $('.branch-details').hide();
                    $('#branch_store_class span').html('');
                    $('#branch_store_size span').html('');
                    $('#branch_address span').html('');
                    $('#branch_class span').html('');
                    $('#branch_opening_date span').html('');
                    $('#branch_zone span').html('');
                },
                onChange: function(value) {
                    if (!value.length) return;
                    var branch_details = select_branch.options[value];

                    $('#branch_desc span').html(branch_details.description);
                    $('#entity_class span').html(branch_details.entity_class);
                    $('#name').val(generateEntityName());

                    select_dept.enable();

                    $('.branch-details').show();
                    $('#branch_store_class span').html(branch_details.branch_store_class);
                    $('#branch_store_size span').html(branch_details.branch_store_size);
                    $('#branch_address span').html(branch_details.branch_address);
                    $('#branch_class span').html(branch_details.branch_class);
                    $('#branch_opening_date span').html(branch_details.branch_opening_date);
                    $('#branch_zone span').html(branch_details.branch_zone);

                    if( branch_details.entity_class == 'Company' ) return;
                }
            });
            select_branch  = $select_branch[0].selectize;
            @endif

            @if(!isset($details))
            $select_dept = department.selectize({
                valueField: 'text',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.text) + '</span>' +
                            '<span class="by">' + escape(item.description) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                onChange: function(value) {
                    $('#name').val(generateEntityName());

                    if (!value.length) {
                        $('#department_desc span').html('');
                        if( branch.val() != 'default' ) $('.branch-details').show();

                        return;
                    }

                    $('.branch-details').hide(); // hide branch details

                    $('#department_desc span').html(select_dept.options[value].description);
                },
                preload: true,
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.dept_code') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                }
            });
            select_dept = $select_dept[0].selectize;
            @endif

            @if(!Request::is('auditable_entities/master/*') && !isset($details))
            select_company.on("changed", function () {
                var company_code = select_company.getValue();

                select_branch.load(function(callback) {
                    xhr && xhr.abort();
                    xhr = $.ajax({
                        url: '{{url('company_profile')}}/'+encodeURIComponent(company_code)+'/branches',
                        success: function(results) {
                            branches = results.data || results;
{{--                            @if( (isset($details) && ($details->branch_code != "" && $details->department_code != "")) )--}}
                            select_branch.enable();
                            select_branch_ca.addOption(branches);
                            {{--@endif--}}
                            callback(branches);
                        },
                        error: function() {
                            callback();
                        }
                    })
                });
            });

            select_branch.disable();
            select_dept.disable();
            @endif

            @if(!isset($details) && Request::is('auditable_entities/master/*'))
            $select_group_type = group_type.selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: ['text'],
                options: [],
                @if(!isset($details))
                preload : true,
                @endif
                create: false,
                render: {
                    option: function (item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.text) + '</span>' +
                            '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url + '/{{ config('iapms.lookups.group_type') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                }
            });
            $select_entity_class = entity_class.selectize({
                valueField: 'meaning',
                labelField: 'meaning',
                searchField: ['text'],
                options: [],
                create: false,
                preload : true,
                render: {
                    option: function (item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.text) + '</span>' +
                            '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function (query, callback) {
                    $.ajax({
                        url: lookup_url + '/{{ config('iapms.lookups.branch.entity_class') }}',
                        type: 'GET',
                        success: function (res) {
                            callback(res.data);
                        },
                        error: function () {
                            callback();
                        }
                    });
                }
                @if(Request::is('auditable_entities/master/*'))
                ,onChange: function(value) {
                    var val = value == '' ? value : select_entity_class.options[value].meaning;
                    $('#branch').html(val);
                    if( value != 'Company' ) {
                        $('input[name="branch"]').val(val);
                    }
                    $('#name').val(generateEntityName());
                    if( value == 'Company' ) {
                        select_dept.clear();
                        select_dept.disable();
                    }
                    else {
                        select_dept.enable();
                    }
                }
                @endif
            });
            select_entity_class  = $select_entity_class[0].selectize;
            @endif

            @if(isset($details))
            $select_main_bp = $('#main_bp').selectize({
                valueField: 'meaning',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.text) + '</span>' +
                            '<span class="by">' + escape(item.meaning)  + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.main_bp') }}?search='+encodeURIComponent(query)+'&searchFields=description:like',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    $('#main_bp_desc span').html(select_main_bp.options[value].description);
                }
            });
            select_main_bp = $select_main_bp[0].selectize;
            $select_sub_bp = $('#sub_bp').selectize({
                valueField: 'bp_id',
                labelField: 'bp_code',
                searchField: ['bp_name'],
                options: $datatable.rows().data(),
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.bp_code) + '</span>' +
                            '<span class="by">' + escape(item.bp_name) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                onChange: function(value) {
                    if (!value.length) return;
                    $('#sub_bp_desc span').html(select_sub_bp.options[value].bp_name);
                }
            });
            select_sub_bp = $select_sub_bp[0].selectize;

            $select_branch_ca = $('.branch_code').selectize({
                persist: false,
                maxItems: null,
                valueField: 'branch_code',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                create: false,
                preload: true,
                render: {
                    item: function(item, escape) {
                        return '<div>' +
                            (item.meaning ? '<span class="label label-sm label-primary arrowed arrowed-right">' + escape(item.meaning) + '</span>' : '') +
                            '</div>';
                    },
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.branch_code) + ' - </span>' +
                            '<span class="by">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: '{{ route('company.branches', ['code' => $details->company_code]) }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res);
                        },
                        error: function() {
                            callback();
                        }
                    });
                }
            });
            select_branch_ca  = $select_branch_ca[0].selectize;
            // /seletize

                @if(in_array($details->company_code, $master_companies))
                getBusinessProcessList();
                @endif
            @endif
        });

        // save the form through AJAX call
        function save() {
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        window.onbeforeunload = null;
                        @if(!isset($details))
                            if( data.redirect !== undefined ) window.location = data.redirect;
                            else window.location = '{{ route('auditable_entities') }}';
                        @else
                            location.reload();
                        @endif
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        }

        $(document).on('click', '.btn-sub-bp-save', function(){
            $('#sub-bp-modal-form').submit();
        });

        $('#sub-bp-modal-form').on('submit', function(){
            swal({
                    title: "Continue saving?",
                    text: "You are about to add sub business process.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, save it!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function(){
                    $.ajax({
                        url: '{{ url('auditable_entities/master/business_processes') }}/'+$('input[name="mbp_id"]').val()+'/sub/store', // url based on the form action
                        method: "POST",
                        data: $('#sub-bp-modal-form').serialize(), // serializes all the form data
                        success : function(data) {
                            if( data.success ) {
                                swal("Saved!", "Sub business process is successfully added.", "success");
                                bpmain_datatable.ajax.reload( null, false );
                                $('#sub-bp-modal').modal('hide');
                                setTimeout(function(){
                                    $('#bp-main-table tr#'+$('input[name="mbp_id"]').val()).trigger('click');
                                }, 1000);
                            }
                        },
                        error: function(xhr, textStatus, errorThrown){
                            swal.close();
                            $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');

                            if( xhr.status == 500 ) {
                                $('.modal-alert').removeClass('alert-warning').addClass('alert-danger');
                                $('.modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                            }
                            else {
                                // displays the validation error
                                var unique_message = [];
                                var msg = '<ul class="list-unstyled">';

                                $('.modal-input').parent().parent().removeClass('has-error');

                                $.each(xhr.responseJSON, function(key, val){
                                    for(var i=0; i < val.length; i++) {
                                        // set error class to fields
                                        $('#'+key).parent().parent().addClass('has-error');

                                        // shows error message
                                        if( unique_message.indexOf(val[i]) === -1 ) {
                                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                            unique_message.push(val[i]);
                                        }
                                    }
                                });
                                msg += '</ul>';

                                $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
                                $('.modal-alert').html(msg).show();
                            }
                        }
                    });
                });

            return false;
        });

        $(document).on('click', '.btn-bp-main-save', function(){
            $('#bp-main-modal-form').submit();
        });

        $('#bp-main-modal-form').on('submit', function(){
            $.ajax({
                url: $('#bp-main-modal-form').attr('action'), // url based on the form action
                method: "POST",
                data: $('#bp-main-modal-form').serialize()+'&_token='+_token, // serializes all the form data
                beforeSend : sLoading(),
                success : function(data) {
                    if( data.success ) {
                        bpmain_datatable.ajax.reload( null, false );
                        $('#bp-main-modal').modal('hide');
                        swal("Saved!", "Main business process is successfully added.", "success");
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();
                    $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');

                    if( xhr.status == 500 ) {
                        $('.modal-alert').removeClass('alert-warning').addClass('alert-danger');
                        $('.modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.modal-input').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
                        $('.modal-alert').html(msg).show();
                    }
                }
            });

            return false;
        });

        @if(isset($details))
        var $datatable = $('#bp-table').DataTable({
            "searching": false,
            "lengthMenu": [ 50, 75, 100 ],
            "processing": true,
            data : {!! json_encode($bp) !!},
            columns: [
                { data: "bp_name" },
                { data: "bp_code", searchable: false, defaultContent: 'N/A' },
                { data: 'lv_bp_code_desc', searchable: false, orderable: false, defaultContent: 'N/A' },
                { data: "source_type", defaultContent: '-', searchable: false, defaultContent: 'N/A' },
                { data: 'lv_source_type_desc', defaultContent: 'N/A', searchable: false, orderable: false },
                @if( in_array($details->company_code, $master_companies) )
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn-delete delete btn-remove" data-id="'+data.bp_id+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    orderable: false
                }
                @endif
            ]
        });

        bpmain_datatable = $('#bp-main-table').DataTable({
            "searching": false,
            "lengthMenu": [ 50, 75, 100 ],
            "processing": true,
            ajax: "{{route('auditable_entities.master.bp.main', ['entity_id' => $details->auditable_entity_id])}}?type={{!Request::is('auditable_entities/master/*') ? 'actual' : 'master'}}",
            rowId: 'ae_mbp_id',
            columns: [
                { data: "lv_main_bp_name_code" },
                { data: "main_bp_name" },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        var controls = '<a href="#" class="btn-masterbp-mandays" data-id="'+data.ae_mbp_id+'" title="Mandays" rel="tooltip"><i class="fa fa-calendar"></i></a> &nbsp; ';
                        @if( in_array($details->company_code, $master_companies) )
                            controls += '<a href="#" class="btn-update-mainbp" data-id="'+data.ae_mbp_id+'" title="Edit" rel="tooltip"><i class="fa fa-pencil"></i></a> &nbsp; <a href="#" class="btn-delete delete btn-remove-mainbp" data-id="'+data.ae_mbp_id+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                        @else
                            controls += '<a href="#" class="btn-custom-measure" data-id="'+data.ae_mbp_id+'" title="Custom Measure" rel="tooltip"><i class="fa fa-tachometer"></i></a>';
                        @endif

                        return controls;
                    },
                    orderable: false
                }
            ]
        });

        $(document).on('click', '.btn-masterbp-mandays', function(){
            var id = $(this).attr('data-id');
            var bpmain_data = bpmain_datatable.row('#'+id).data();

            $('#mainbp-mandays-modal').find('form').attr('action', '{{ url('auditable_entities/business_process/main') }}/'+id+'/mandays/update');
            $('#mainbp-mandays-modal').modal('show');
            $('#mainbp-mandays-modal-form').find('input[type="number"]').each(function(i, el){
                var input = $(el);
                if( typeof bpmain_data[input.attr('id')] != "undefined" && bpmain_data[input.attr('id')] > 0) {
                    input.val(bpmain_data[input.attr('id')]);
                }
            });
            return false;
        });
        $(document).on('click', '.btn-mainbp-mandays-save', function() {
            $('#mainbp-mandays-modal-form').submit();
        });
        $('#mainbp-mandays-modal-form').on('submit', function(){
            swal({
                    title: "Continue saving?",
                    text: "You are about to update business process mandays.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, save it!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function(){
                    $.ajax({
                        url: $('#mainbp-mandays-modal-form').attr('action'), // url based on the form action
                        method: "POST",
                        data: $('#mainbp-mandays-modal-form').serialize(), // serializes all the form data
                        success : function(data) {
                            if( data.success ) {
                                swal("Saved!", "Business process mandays is successfully updated.", "success");
                                bpmain_datatable.ajax.reload( null, false );
                                $('#mainbp-mandays-modal').modal('hide');
                            }
                        },
                        error: function(xhr, textStatus, errorThrown){
                            swal.close();
                            $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');

                            if( xhr.status == 500 ) {
                                $('.modal-alert').removeClass('alert-warning').addClass('alert-danger');
                                $('.modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                            }
                            else {
                                // displays the validation error
                                var unique_message = [];
                                var msg = '<ul class="list-unstyled">';

                                $('.modal-input').parent().parent().removeClass('has-error');

                                $.each(xhr.responseJSON, function(key, val){
                                    for(var i=0; i < val.length; i++) {
                                        // set error class to fields
                                        $('#'+key).parent().parent().addClass('has-error');

                                        // shows error message
                                        if( unique_message.indexOf(val[i]) === -1 ) {
                                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                            unique_message.push(val[i]);
                                        }
                                    }
                                });
                                msg += '</ul>';

                                $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
                                $('.modal-alert').html(msg).show();
                            }
                        }
                    });
                });

            return false;
        });

        bpsub_datatable = $('#bp-sub-table').DataTable({
            "searching": false,
            "lengthMenu": [ 50, 75, 100 ],
            "processing": true,
            data : [],
            columns: [
                { data: "pivot.mbp_bp_seq", searchable: false },
                { data: "bp_name" },
                { data: "bp_code", searchable: false },
                { data: 'lv_bp_code_desc', searchable: false, orderable: false },
                { data: "source_type", defaultContent: '-', searchable: false },
                { data: 'lv_source_type_desc', defaultContent: '-', searchable: false, orderable: false },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        @if( in_array($details->company_code, $master_companies) )
                            return '<a href="#" class="btn-delete delete btn-remove-subbp" data-id="'+data.pivot.ae_mbp_bp_id+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                        @else
                            return '';
                        @endif
                    },
                    orderable: false
                }
            ]
        });

        @if( in_array($details->company_code, $master_companies) )
        // initialize modal fields
        function init_modal( sequence, activity_narrative, id ) {
            $modal.find('input#sequence').val(sequence);
            $modal.find('#activity_narrative').val(activity_narrative);

            $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
            $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');
        }

        $modal.on('hidden.bs.modal', function (e) {
            $('#duallist option').removeAttr('selected');
            $('#duallist').bootstrapDualListbox('refresh', true);
            $modal.find('.modal-alert').html('').hide();
        });

        function updateDatatable() {
            $datatable.ajax.reload( null, false );
            getBusinessProcessList();
            $modal.modal('hide');
        }

        $(document).on('click', '.btn-auditable-bp-save', function(){
            $('select[name="bp[]_helper2"]').removeAttr('name');
            $('#auditable-bp-modal-form').submit();
        });

        $('#auditable-bp-modal-form').on('submit', function(){
            swal({
                title: "Continue saving?",
                text: "You are about to add a new auditable entity business process.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, save it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: $('#auditable-bp-modal-form').attr('action'), // url based on the form action
                    method: "POST",
                    data: $('#auditable-bp-modal-form').serialize(), // serializes all the form data
                    success : function(data) {
                        if( data.success ) {
                            swal("Saved!", "Business process is successfully added.", "success");
                            location.reload();
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal.close();
                        $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');

                        if( xhr.status == 500 ) {
                            $('.modal-alert').removeClass('alert-warning').addClass('alert-danger');
                            $('.modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                        }
                        else {
                            // displays the validation error
                            var unique_message = [];
                            var msg = '<ul class="list-unstyled">';

                            $('.modal-input').parent().parent().removeClass('has-error');

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    // set error class to fields
                                    $('#'+key).parent().parent().addClass('has-error');

                                    // shows error message
                                    if( unique_message.indexOf(val[i]) === -1 ) {
                                        msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                        unique_message.push(val[i]);
                                    }
                                }
                            });
                            msg += '</ul>';

                            $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
                            $('.modal-alert').html(msg).show();
                        }
                    }
                });
            });

            return false;
        });

        // show update form
        $(document).on('click', '.btn-update', function(){
            var activity_narrative = $(this).attr('data-activity_narrative'),
                id = $(this).attr('data-id'),
                seq = $(this).attr('data-seq');

            // set values
            init_modal(seq, activity_narrative, id);

            $modal.modal('show');

            return false;
        });

        // remove existing control
        $(document).on('click', '.btn-remove', function() {
            var remove_id = $(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "You are about to remove a auditable entity business process.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: '{{route('auditable_entities.business_process.delete', ['auditable_ent_id' => $details->auditable_entity_id])}}',
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}', business_process : remove_id },
                    success : function(data) {
                        if( data.success ) {
                            swal("Deleted!", "Business process area has been deleted.", "success");
                            location.reload();
                        }
                    }
                });
            });

            return false;
        });

        $(document).on('click', '.btn-remove-subbp', function() {
            var remove_id = $(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "You are about to remove a sub business process.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: '{{route('auditable_entities.bp.sub.delete')}}',
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}', id : remove_id },
                    success : function(data) {
                        if( data.success ) {
                            bpmain_datatable.ajax.reload( null, false );
                            setTimeout(function(){
                                $('#bp-main-table tr#'+$('input[name="mbp_id"]').val()).trigger('click');
                            }, 1000);
                            swal("Deleted!", "Sub business process has been deleted.", "success");
                        }
                    }
                });
            });

            return false;
        });

        $(document).on('click', '.btn-update-mainbp', function(){
            var id = $(this).attr('data-id');
            var bpmain_data = bpmain_datatable.row('#'+id).data();

            $('#bp-main-modal-form').attr('action', '{{url('auditable_entities/master/business_processes')}}/'+id+'/main/update');
            $('#bp-main-modal-form').find('input[name="_method"]').val('put');
            $('#bp-main-modal').modal('show');

            select_main_bp.setValue(bpmain_data.main_bp_name);
            select_main_bp.disable();

            for(var cm = 1; cm < 11; cm++) {
                if( bpmain_data['cm_'+cm+'_name'] && bpmain_data['cm_'+cm+'_high_score_type'] ) {
                    createCM(bpmain_data['cm_'+cm+'_name'], bpmain_data['cm_'+cm+'_high_score_type']);
                }
            }

            $('.mainbp-cm').each(function(i, element){
                var id = $(element).prop('id');

                if( typeof bpmain_data[id] != "undefined" ) {
                    $(element).val(bpmain_data[id]);
                }
            });

            return false;
        });

        $(document).on('click', '.btn-remove-mainbp', function() {
            var remove_id = $(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "You are about to remove a main business process.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: '{{route('auditable_entities.bp.main.delete') }}',
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}', id : remove_id },
                    success : function(data) {
                        if( data.success ) {
                            bpmain_datatable.ajax.reload( null, false );
                            bpsub_datatable.rows().clear().draw();
                            $('.btn-sub-bp').hide();
                            swal("Deleted!", "Business process area has been deleted.", "success");
                        }
                    }
                });
            });

            return false;
        });

        @endif
        $bc_modal.on('hidden.bs.modal', function (e) {
            $(this).find('table > tbody > tr').remove();
            $('.btn-add-branch').trigger('click');
        });
        $('#sub-bp-modal').on('hidden.bs.modal', function (e) {
            select_sub_bp.clear();
            $('#sub_bp_desc span').html('');
        });

        $('#bp-main-modal').on('hidden.bs.modal', function (e) {
            $('input', this).val('');
            $('select', this).val('');
            $('#main_bp_desc span', this).html('');
            select_main_bp.clear();
            select_main_bp.enable();
            $('.cm-group').remove();
            $('#btn-add-cm').show();
            $('#bp-main-modal-form').attr('action', '{{route('auditable_entities.bp.main.store', ['auditable_ent_id' => $details->auditable_entity_id])}}');
            $('#bp-main-modal-form').find('input[name="_method"]').val('post');
        });
        @endif

        $('.btn-apply-to-all').on('click', function(){
            swal({
                title: "Continue?",
                text: "You are about to apply this template to selected branches.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, apply it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: '{{route('auditable_entities.template.apply')}}',
                    method: "POST",
                    data: $bc_modal.find('form').serialize(),
                    success : function(data) {
                        if( data.success ) {
                            window.location = data.redirect;
                        }
                    }
                });
            });
        });

        function getBusinessProcessList() {
            $.ajax({
                url: "{{ route('business_process.processes') }}",
                dataType: 'json',
                success: function (data) {
                    var options = '';
                    var existing = $datatable.rows().data();
                    $('#duallist').find('option').remove();
                    $.each(data, function(i, v){
                        if(!existing.filter(function (existing) { return existing.bp_name == v.bp_code+' - '+v.bp_name }).length)
                            options += '<option value="'+v.bp_id+'">'+v.bp_code+' - '+v.bp_name+'</option>';
                    });
                    $('#duallist').append(options);

                    // DualLIstBox init
                    bps = $('#duallist').bootstrapDualListbox({
                        nonSelectedListLabel: 'Non-selected',
                        selectedListLabel: 'Selected',
                        preserveSelectionOnMove: 'moved',
                        moveOnSelect: false,
                        refresh: true
                    });
                    var bps_container = bps.bootstrapDualListbox('getContainer');
                    bps_container.find('.btn').addClass('btn-info btn-bold').removeClass('btn-white');
                }
            });
        }

        function getSubBusinessProcesses() {
            var data = $datatable.rows().data();
            var options = '';
            var sub_bps;

            $('#sub_bps').find('option').remove();
            $.each(data, function(i, v){
                options += '<option value="'+v.bp_id+'">'+v.bp_name+'</option>';
            });
            $('#sub_bps').append(options);

            // DualLIstBox init
            sub_bps = $('#sub_bps').bootstrapDualListbox({
                nonSelectedListLabel: 'Non-selected',
                selectedListLabel: 'Selected',
                preserveSelectionOnMove: 'moved',
                moveOnSelect: false,
                refresh: true
            });
            var bps_container = sub_bps.bootstrapDualListbox('getContainer');
            bps_container.find('.btn').addClass('btn-info btn-bold').removeClass('btn-white');
        }

        $(document).on('click', '.btn-add-info', function() {
            var id = $(this).attr('data-id');
            var e_type = "{{!Request::is('auditable_entities/master/*') ? 'actual' : 'master'}}";
            var table = 'auditable_entities';
            table += (e_type == 'actual' ? '_'+e_type : '');
            iapms.getAddInfoForm(id, table, e_type).success(function(data) {
                if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                    ai_modal.find('.btn-save-ai').hide();
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                    ai_modal.find('.btn-save-ai').show();
                }
                ai_modal.modal('show');
                iapms.setAddInfoForm(ai_modal, '{{url('auditable_entities')}}/'+id+'/additional_information/update?type='+e_type, 'put');

                toggle_loading();
            });

            return false;
        });

        $(function(){
            ai_modal.find('form').validate();
        });

        ai_modal.find('form').on('submit', function(e){
            e.preventDefault();
            if($(this).valid()) {
                swal({
                    title: "Continue?",
                    text: "You are about to update additional information.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, save it!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function(){
                    $.ajax({
                        url: ai_modal.find('form').attr('action'),
                        method: "POST",
                        data: ai_modal.find('form').serialize(), // serializes all the form data
                        success : function(data) {
                            if(data.success) {
                                swal("Success!", "Additional information has been updated.", "success");
                                location.reload();
                            }
                        },
                        error: function(xhr, textStatus, errorThrown){
                            swal.close();
                            // displays the validation error
                            var msg = '<ul class="list-unstyled">';

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                }
                            });
                            msg += '</ul>';

                            ai_modal.find('.alert-modal').html(msg).show();
                        }
                    });
                });
            }

            return false;
        });

        ai_modal.on('click', '.btn-save-ai', function() {
            ai_modal.find('form').submit();
        });
    </script>
    @yield('cm_scripts')
@endsection
