@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header', ['heading' => 'Auditable Entities - '.$type ])

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href='{{ Request::is('auditable_entities/master') ? route('auditable_entities.master.create') : route('auditable_entities.create') }}'>
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Auditable Entities
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="entity-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Auditable Entity IDL</th>
                                <th>Company Short Name</th>
                                <th>Business Type</th>
                                <th>Entity Class</th>
                                <th>Branch Code</th>
                                <th>Branch Short Name</th>
                                <th>Department Short Name</th>
                                <th width="7%"></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@include('includes.modals.additional_information')

@endsection

@section('footer_script')
<script src="/js/iapms.js"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var ai_modal = $('#additional-information-modal');
    var $searchButton;
    var datatable= $('#entity-table').DataTable( {
        initComplete : function() {
            var input = $('.dataTables_filter input').unbind();
                $searchButton = $('<button class="btn btn-info btn-small">')
                .text('Go!')
                .click(function() {
                    var search = input.val();

                    if( search == '' ) return false;
                    $.ajax({
                        url: '{{ Request::is('auditable_entities/master') ? route('auditable_entities.master.lists') : route('auditable_entities.lists').'?entity_type=actual' }}',
                        method: "GET",
                        data: { entity_search : search },
                        beforeSend: function () {
                            toggle_loading();
                        },
                        success: function (data) {
                            datatable.clear();
                            datatable.rows.add(data).draw();
                            input.val(search);
                            toggle_loading();
                        }
                    });
                }),
                $clearButton = $('<button class="btn btn-warning btn-small">')
                .text('Clear')
                .click(function() {
                    input.val('');
                    $searchButton.click();
                });
            $('.dataTables_filter label').append($searchButton, $clearButton);
            input.keypress(function (e) {
                if (e.which == 13) {
                    if( $(this).val() != '' )
                        $searchButton.trigger('click');

                    return false;
                }
            });
        },
        data: [],
        processing: true,
        columns: [
            { data: "auditable_entity_name" },
            { data: "{{ Request::is('auditable_entities/master') ? 'company_code' : 'company_name' }}", defaultContent : 'N/A' },
            { data: "business_type", defaultContent : 'N/A'},
            { data: "entity_class", defaultContent : 'Company'},
            { data: "branch_code" },
            { data: "{{ Request::is('auditable_entities/master') ? 'branch_code' : 'lv_branch_code_desc' }}"},
            { data: "department_code"},
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="btn-add-info" data-id="'+data.auditable_entity_id+'" title="additional info" rel="tooltip"><i class="fa fa-list-ul edit"></i></a> &nbsp; <a href="{{ Request::is('auditable_entities/master') ? url('auditable_entities/master') : url('auditable_entities') }}/edit/'+data.auditable_entity_id+'"><i class="fa fa-pencil edit" title="Edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.auditable_entity_id+'" data-name="'+data.auditable_entity_name+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    @if(Request::is('auditable_entities/master'))
        datatable.columns(4).visible(false);
    @endif

    $(document).on('click', '.btn-add-info', function() {
        var id = $(this).attr('data-id');
        var e_type = "{{!Request::is('auditable_entities/master/*') ? 'actual' : 'master'}}";
        var table = 'auditable_entities';
        table += (e_type == 'actual' ? '_'+e_type : '');
        iapms.getAddInfoForm(id, table).success(function(data) {
            if( !data.success ) {
                ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
            }
            else {
                ai_modal.find('#ai_wrapper').html(data.data);
            }
            ai_modal.find('#additional-information-form input').attr('disabled', true);
            ai_modal.find('#additional-information-form select').attr('disabled', true);
            ai_modal.find('.btn-save-ai').hide();
            ai_modal.modal('show');
            iapms.setAddInfoForm(ai_modal, '{{url('auditable_entities')}}/'+id+'/additional_information/update', 'put');

            toggle_loading();
        });

        return false;
    });

    // remove auditee
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var remove_name = $(this).attr('data-name');
        var row = $(this).parent().parent();
        swal({
            title: "Are you sure?",
            text: "You are about to remove "+remove_name+" auditable entity.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(){
            $.ajax({
                url: '{{ !Request::is('auditable_entities/master') ? url('auditable_entities/delete') : url('auditable_entities/master/delete') }}/'+remove_id,
                method: "POST",
                data: { _token : _token, _method : 'delete' }, // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        datatable.row(row).remove().draw();
                        swal("Deleted!", "Auditable Entity has been deleted.", "success");
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();
                    // displays the validation error
                    var msg = '<ul class="list-unstyled">';

                    $.each(xhr.responseJSON, function(key, val){
                        for(var i=0; i < val.length; i++) {
                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                        }
                    });
                    msg += '</ul>';

                    $('.alert-warning').html(msg).show();
                }
            });
        });

        return false;
    });
</script>
@endsection