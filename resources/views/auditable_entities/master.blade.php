    <div class="form-group">
        <label class="col-sm-2 control-label" for="group_type"> Group Type </label>
        <div class="col-sm-6" id="group_type">
            @if(isset($details))
                <span class="label label-info">{{ $details->group_type }}</span>
            @else
                <select id="group_type_st" name="group_type" class="form-control input-sm basic-info lookup" placeholder="Select a group type..." required></select>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="business_type"> Business Type </label>
        <div class="col-sm-6" id="business_type">
            <input type="hidden" name="business_type" value="{{ isset($details) ? $details->business_type : '' }}">
            <span class="label label-info">{{ isset($details) ? $details->business_type : '' }}</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="branch"> Branch </label>
        <div class="col-sm-6">
            <input type="hidden" name="branch" value="{{ isset($details) ? $details->branch_code : '' }}">
            <span class="label label-info" id="branch" >{{ isset($details) ? ($details->branch_code != '' ? $details->branch_code : 'Default') : '' }}</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="entity_class"> Entity Class </label>
        <div class="col-sm-6" id="entity_class">
            @if(isset($details))
                <span class="label label-info">{{ $details->branch_code != '' && $details->department_code != '' ? $details->entity_class : 'Company' }}</span>
            @else
                <select id="entity_class_st" name="entity_class" class="form-control input-sm basic-info lookup" placeholder="Select a entity class..." required></select>
            @endif
        </div>
    </div>