    <div class="form-group">
        <label class="col-sm-2 control-label" for="group_type"> Group Type </label>
        <div class="col-sm-6" id="group_type">
            @if(Request::is('auditable_entities/master/*'))
                <select id="group_type_st" name="group_type" class="form-control input-sm basic-info lookup" placeholder="Select a group type..." {{ isset($details) ? 'disabled' : 'required' }}>
                    @if(isset($details))
                        <option value="{{ $details->group_type }}" selected>{{ $details->group_type }}</option>
                    @endif
                </select>
            @else
                <span class="label label-info">{{ isset($details) ? $details->group_type : '' }}</span>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="business_type"> Business Type </label>
        <div class="col-sm-6" id="business_type">
            @if(Request::is('auditable_entities/master/*'))
                <input type="hidden" name="business_type" value="{{ isset($details) ? $details->business_type : '' }}">
            @endif
            <span class="label label-info">{{ isset($details) ? $details->business_type : '' }}</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="branch"> Branch </label>
        <div class="col-sm-6">
            @if(isset($details))
                <input type="hidden" name="branch" value="{{$details->branch_code}}">
                <span class="label label-info">{{ $details->branch_code != '' ? $details->branch_code : 'Default' }}</span>
            @else
                <select id="branch" name="branch" class="form-control input-sm basic-info lookup" placeholder="Select a branch..."></select>
            @endif
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="branch_desc"> Branch Description </label>
        <div class="col-sm-8" id="branch_desc">
            <span class="label label-info">{{ isset($details) ? $details->lv_branch_code_desc : '' }}</span>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="entity_class"> Entity Class </label>
        <div class="col-sm-6" id="entity_class">
            <span class="label label-info">{{ isset($details) ? ($details->branch_code != '' ? $details->entity_class : 'Company') : '' }}</span>
        </div>
    </div>

    @section('entity_branch_details')
        <div class="branch-details" style="{{ isset($details) && ($details->branch_code != '' && $details->department_code != '' ) ? '' : 'display: none;' }}">
            <div class="form-group">
                <div class="col-sm-12">
                    <div class="table-header"> Branch Details </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="branch_store_size" style="padding-top: 1px;"> Store Size </label>
                <div class="col-sm-6" id="branch_store_size">
                    <span class="label label-info">{{ isset($details) ? $details->branch_store_size : '' }}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="branch_address" style="padding-top: 1px;"> Address </label>
                <div class="col-sm-6" id="branch_address">
                    <span class="label label-info">{{ isset($details) ? $details->branch_address : '' }}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="branch_zone" style="padding-top: 1px;"> Zone </label>
                <div class="col-sm-6" id="branch_zone">
                    <span class="label label-info">{{ isset($details) ? $details->lv_branch_zone_desc : '' }}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="branch_store_class" style="padding-top: 1px;"> Store Class </label>
                <div class="col-sm-6" id="branch_store_class">
                    <span class="label label-info">{{ isset($details) ? $details->branch_store_class : '' }}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="branch_class" style="padding-top: 1px;"> Branch Class </label>
                <div class="col-sm-6" id="branch_class">
                    <span class="label label-info">{{ isset($details) ? $details->branch_class : '' }}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="branch_opening_date" style="padding-top: 1px;"> Opening Date </label>
                <div class="col-sm-6" id="branch_opening_date">
                    <span class="label label-info">{{ isset($details) ? date('d-M-Y', strtotime($details->branch_opening_date)) : '' }}</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="active"> Active? </label>
            <div class="col-sm-2">
                <select id="active" name="active" class="form-control input-sm basic-info">
                    <option value="Y" {!! isset($details) ? ($details->active == 'Y' ? 'selected="selected"' : '') : '' !!}>Yes</option>
                    <option value="N" {!! isset($details) ? ($details->active == 'N' ? 'selected="selected"' : '') : '' !!}>No</option>
                </select>
            </div>
        </div>
    @endsection