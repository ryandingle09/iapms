<div class="col-xs-12">
    <div class="table-header"> Control - Control Test Procedure </div>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th width="15%">Component Code</th>
                <th width="15%">Principle Code</th>
                <th width="15%">Focus Code</th>
                <th>Control Sequence</th>
                <th>Control Code</th>
                <th>Control Description</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="label-component_code"></td>
                <td class="label-principle_code"></td>
                <td class="label-focus_code"></td>
                <td class="label-risk_control_seq"></td>
                <td class="label-control_code"></td>
                <td class="label-lv_control_code_desc"></td>
            </tr>
        </tbody>
    </table>
    <div class="table-header"> Control Test Procedures </div>
    <table id="datatable-ctrl_proc" class="table table-striped table-bordered table-hover datatable-details">
        <thead>
            <tr>
                <th width="10%">Sequence</th>
                <th width="90%">Test Procedure Narrative</th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-12" style="padding-top: 10px;">
    <div class="table-header">
        Control Test Procedure Details
    </div>
    <table class="table table-striped table-bordered table-details">
        <tbody>
            <tr>
                <th width="15%">Sequence</th>
                <td class="label-control_test_seq"></td>
            </tr>
            <tr>
                <th>Test Procedure Narrative</th>
                <td class="label-test_proc_narrative"></td>
            </tr>
        </tbody>
    </table>
    <div class="row row-details">
        <div class="col-sm-12" style="text-align: center;margin-top: 10px;">
            <button type="button" class="btn btn-primary btn-add-info-proc" style="display: none;"><i class="fa fa-list-ul"></i> Additional Info</button>
        </div>
    </div>
</div>

@section('ctrl_proc_js')
<script type="text/javascript">
    var table_ctrl_proc = $('#datatable-ctrl_proc');
    var datatable_ctrl_proc = table_ctrl_proc.DataTable( {
        data: [],
        processing: true,
        orderCellsTop: true,
        columns: [
            { data: "control_test_seq" },
            { data: "test_proc_narrative", orderable : false, searchable : false }
        ]
    } );

    table_ctrl_proc.find('tbody').on('click', 'tr', function () {
        table_ctrl_proc.find('tbody > tr').removeClass('selected-row'); // reset tr highlights
        var data = datatable_ctrl_proc.row( this ).data();
        $('.btn-add-info-proc').attr('data-id', data['control_id']).show();
        $(this).addClass('selected-row');
        $.each(data, function(i, v){
            var label = '.label-'+i;
            if( $(label).length ) {
                if( i =='parent' && v != null ) {
                    $(label).html(v.auditable_entity_name);
                }
                else {
                    var value = v != null ? v : 'N/A';
                    $(label).html(value);
                }
            }
        });
    } );

    function setControlProcData(data) {
        datatable_ctrl_proc.clear();
        datatable_ctrl_proc.rows.add(data).draw();
    }

    $('.btn-add-info-proc').on('click', function(){
        var table = 'controls_master';
        var id = $(this).attr('data-id');
        $('.btn-save-ai').hide();
        $.ajax({
            url: '{{ url('additional_information/table') }}/'+id+'/'+table,
            method: "GET",
            data: { _token : '{{ csrf_token() }}' }, // serializes all the form data
            beforeSend : function() {
                toggle_loading();
            },
            success : function(data) {
                if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                }
                ai_modal.find('#additional-information-form input').attr('disabled', true);
                ai_modal.find('#additional-information-form select').attr('disabled', true);
                ai_modal.find('.btn-save-ai').hide();
                ai_modal.modal('show');

                toggle_loading();
            }
        });
    });
</script>
@endsection