<div class="col-xs-12">
    <div class="table-header">
        Auditable Entities
    </div>

    <!-- div.table-responsive -->

    <!-- div.dataTables_borderWrap -->
    <table id="datatable-ae" class="table table-striped table-bordered table-hover datatable">
        <thead>
            <tr>
                <th>Auditable Entity Name</th>
                <th>Company Code</th>
                <th>Group Type</th>
                <th>Branch Code</th>
                <th>Department Code</th>
                <th>Entity Class</th>
                <th>Business Type</th>
                <th>Parent Entity?</th>
                <th>Parent Name</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-12" style="padding-top: 10px;">
    <div class="table-header">
        Auditable Entity Details
    </div>
    <table class="table table-striped table-bordered table-details">
        <tbody>
            <tr>
                <th width="15%">Name</th>
                <td colspan="2" class="label-auditable_entity_name"></td>
            </tr>
            <tr>
                <th>Company Code</th>
                <td colspan="2" class="label-company_code"></td>
            </tr>
            <tr>
                <th>Company Code Description</th>
                <td colspan="2" class="label-lv_company_code_desc"></td>
            </tr>
            <tr>
                <th>Group Type</th>
                <td colspan="2" class="label-group_type"></td>
            </tr>
            <tr>
                <th>Group Type Description</th>
                <td colspan="2" class="label-lv_group_type_desc"></td>
            </tr>
            <tr>
                <th>Branch Code</th>
                <td colspan="2" class="label-branch_code"></td>
            </tr>
            <tr>
                <th>Branch Code Description</th>
                <td colspan="2" class="label-lv_branch_code_desc"></td>
            </tr>
            <tr>
                <th>Department Code</th>
                <td colspan="2" class="label-department_code"></td>
            </tr>
            <tr>
                <th>Department Code Description</th>
                <td colspan="2" class="label-lv_department_code_desc"></td>
            </tr>
            <tr>
                <th>Entity Class</th>
                <td colspan="2" class="label-entity_class"></td>
            </tr>
            <tr>
                <th>Entity Class Description</th>
                <td colspan="2" class="label-lv_entity_class_desc"></td>
            </tr>
            <tr>
                <th>Business Type</th>
                <td colspan="2" class="label-business_type"></td>
            </tr>
            <tr>
                <th>Business Type Description</th>
                <td colspan="2" class="label-lv_business_type_desc"></td>
            </tr>
            <tr>
                <th>Parent Entity?</th>
                <td colspan="2" class="label-parent_entity"></td>
            </tr>
            <tr>
                <th>Parent Entity Name</th>
                <td colspan="2" class="label-parent"></td>
            </tr>
            <tr>
                <th>Contact Name</th>
                <td colspan="2" class="label-contact_name"></td>
            </tr>
            <tr>
                <th>Entity Head Name</th>
                <td colspan="2" class="label-entity_head_name"></td>
            </tr>
        </tbody>
    </table>
    <div id="au-buttons" class="row row-details" style="display: none;">
        <div class="col-sm-12" style="text-align: center;margin-top: 10px;">
            <button type="button" class="btn btn-primary btn-add-info btn-addinfo"><i class="fa fa-list-ul"></i> Additional Info</button>
            &nbsp;
            <a href="#" class="btn btn-success btn-company-profile" target="_blank"><i class="fa fa-institution"></i> Company Profile</a>
        </div>
    </div>
</div>
@section('ae_js')
<script type="text/javascript">
    var table_ae = $('#datatable-ae');
    var ai_modal = $('#additional-information-modal');
    var datatable_ae = table_ae.DataTable( {
        ajax: "{{route('auditable_entities.list')}}?with=parent&descriptions=true",
        processing: true,
        serverSide: true,
        orderCellsTop: true,
        columns: [
            { data: "auditable_entity_name" },
            { data: "company_code", searchable: false },
            { data: "group_type", searchable: false },
            { data: "branch_code", searchable: false },
            { data: "department_code", searchable: false },
            { data: "entity_class", searchable: false },
            { data: "business_type", searchable: false },
            { data: "parent_entity", searchable: false },
            { data: "parent.auditable_entity_name", defaultContent : 'N/A', searchable: false }
        ]
    } );
    dtClickable(table_ae, datatable_ae, 'auditable_entity_id', 'datatable_bp');

    $('.btn-add-info').on('click', function(){
        var table = 'auditable_entities';
        var id = $(this).attr('data-id');
        $('.btn-save-ai').hide();
        $.ajax({
            url: '{{ url('additional_information/table') }}/'+id+'/'+table,
            method: "GET",
            data: { _token : '{{ csrf_token() }}' }, // serializes all the form data
            beforeSend : function() {
                toggle_loading();
            },
            success : function(data) {
                if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                }
                ai_modal.find('#additional-information-form input').attr('disabled', true);
                ai_modal.find('#additional-information-form select').attr('disabled', true);
                ai_modal.find('.btn-save-ai').hide();
                ai_modal.modal('show');

                toggle_loading();
            }
        });
    });
</script>
@endsection