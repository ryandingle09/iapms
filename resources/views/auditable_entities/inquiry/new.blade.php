@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/plugins/fancytree/skin-xp/ui.fancytree.min.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.bootstrap3.css" />
    <link rel="stylesheet" href="/css/selectize-custom.css" />
    <style type="text/css">
        .popover {
            min-width: 200px !important;
            max-width: 550px !important;
        }
        .tree-details {
            cursor: pointer;
        }
        .fancytree-ext-childcounter span.fancytree-childcounter {
            background: #3d94d6 !important;
            border: 1px solid #3d94d6 !important;
            top: -3px !important;
            right: -5px !important;
            min-width: 13px !important;
            height: 13px !important;
        }
        .profile-info-name {
            min-width: 110px !important;
            max-width: 180px !important;
        }
        .fancytree-title {
            white-space: normal;
            padding-right: 10px;
        }
    </style>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header', ['heading' => 'Inquiry'])
            <div class="row">
                <div class="col-xs-12">
                    <div class="alert alert-info">
                        <i class="fa fa-info-circle"></i> Select desired starting point and enter your filter.
                    </div>
                    <div class="widget-box widget-color-blue2 widget-container-col">
                        <div class="widget-header">
                            <h4 class="widget-title lighter smaller"><i class="fa fa-filter"></i> Filter</h4>
                            <div class="widget-toolbar">
                                <a href="#" data-action="collapse">
                                    <i class="ace-icon fa fa-chevron-down"></i>
                                </a>
                            </div>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main padding-8">
                                <form method="GET" action="{{route('auditable_entities.inquiry')}}" class="form-horizontal" id="filter-form">
                                    <input type="hidden" name="_type" value="{{\Request::get('_type')}}">
                                    <div class="form-group">
                                        <label for="company" class="col-sm-2 control-label">Starting Point</label>
                                        <div class="col-sm-10">
                                            <div class="control-group">
                                                <div class="radio">
                                                    <label>
                                                        <input name="subtreeview" type="radio" class="ace" value="all" {{ \Request::get('subtreeview') == '' || \Request::get('subtreeview') == 'all' ? 'checked' : '' }}>
                                                        <span class="lbl"> <i class="fa fa-building"></i> All</span>
                                                    </label>
                                                    <label>
                                                        <input name="subtreeview" type="radio" class="ace" value="branch" {{ \Request::get('subtreeview') == 'branch' ? 'checked' : '' }}>
                                                        <span class="lbl"><i class="fa fa-building"></i> Branch</span>
                                                    </label>
                                                    <label>
                                                        <input name="subtreeview" type="radio" class="ace" value="dept" {{ \Request::get('subtreeview') == 'dept' ? 'checked' : '' }}>
                                                        <span class="lbl"> <i class="fa fa-building"></i> Department</span>
                                                    </label>
                                                    <label>
                                                        <input name="subtreeview" type="radio" class="ace" value="mainbp" {{ \Request::get('subtreeview') == 'mainbp' ? 'checked' : '' }}>
                                                        <span class="lbl"> <i class="fa fa-cog"></i> Main Process</span>
                                                    </label>
                                                    <label>
                                                        <input name="subtreeview" type="radio" class="ace" value="subbp" {{ \Request::get('subtreeview') == 'subbp' ? 'checked' : '' }}>
                                                        <span class="lbl"><i class="fa fa-cog"></i> Sub Process</span>
                                                    </label>
                                                    <label>
                                                        <input name="subtreeview" type="radio" class="ace" value="objective" {{ \Request::get('subtreeview') == 'objective' ? 'checked' : '' }}>
                                                        <span class="lbl"> <i class="fa fa-crosshairs"></i> Objectives</span>
                                                    </label>
                                                    <label>
                                                        <input name="subtreeview" type="radio" class="ace" value="risk" {{ \Request::get('subtreeview') == 'risk' ? 'checked' : '' }}>
                                                        <span class="lbl"> <i class="fa fa-exclamation-triangle"></i> Risks</span>
                                                    </label>
                                                    <label>
                                                        <input name="subtreeview" type="radio" class="ace" value="control" {{ \Request::get('subtreeview') == 'control' ? 'checked' : '' }}>
                                                        <span class="lbl"> <i class="fa fa-cogs"></i> Controls</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group company-filter filter-wrapper" style="display:none;">
                                        <label for="company" class="col-sm-2 control-label">Company</label>
                                        <div class="col-sm-8">
                                            <select id="company" name="filter[company]" class="form-control input-sm filter filter-group" placeholder="Select a company..." required></select>
                                        </div>
                                    </div>
                                    <div class="form-group company-filter filter-wrapper" style="display:none;">
                                        <label for="branch" class="col-sm-2 control-label">Branch</label>
                                        <div class="col-sm-8">
                                            <select id="branch" name="filter[branch]" class="form-control input-sm filter" placeholder="Select a branch..."></select>
                                        </div>
                                    </div>
                                    <div class="form-group company-filter filter-wrapper" style="display:none;">
                                        <label for="department" class="col-sm-2 control-label">Department</label>
                                        <div class="col-sm-8">
                                            <select id="department" name="filter[department]" class="form-control input-sm filter" placeholder="Select a department..."></select>
                                        </div>
                                    </div>
                                    <div class="form-group mainbp-filter filter-wrapper" style="display:none;">
                                        <label for="main_bp" class="col-sm-2 control-label">Main Process</label>
                                        <div class="col-sm-8">
                                            <select id="main_bp" name="filter[main_bp]" class="form-control input-sm filter" placeholder="Select a main process..."></select>
                                        </div>
                                    </div>
                                    <div class="form-group subbp-filter filter-wrapper" style="display:none;">
                                        <label for="sub_bp" class="col-sm-2 control-label">Sub Process</label>
                                        <div class="col-sm-8">
                                            <select id="sub_bp" name="filter[sub_bp]" class="form-control input-sm filter" placeholder="Select a sub process..."></select>
                                        </div>
                                    </div>
                                    <div class="form-group objective-filter filter-wrapper" style="display:none;">
                                        <label for="objective" class="col-sm-2 control-label">Objective</label>
                                        <div class="col-sm-10">
                                            <select id="objective" name="filter[objective]" class="form-control input-sm filter" placeholder="Select a objective..."></select>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <span id="objective_narrative" class="label label-info"></span>
                                        </div>
                                    </div>
                                    <div class="form-group risk-filter filter-wrapper" style="display:none;">
                                        <label for="risk" class="col-sm-2 control-label">Risk</label>
                                        <div class="col-sm-8">
                                            <select id="risk" name="filter[risk]" class="form-control input-sm filter" placeholder="Select a risk..."></select>
                                        </div>
                                    </div>
                                    <div class="form-group control-filter filter-wrapper" style="display:none;">
                                        <label for="control" class="col-sm-2 control-label">Control</label>
                                        <div class="col-sm-10">
                                            <select id="control" name="filter[control]" class="form-control input-sm filter" placeholder="Select a control..."></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-primary btn-filter"><i class="fa fa-bolt"></i> Go</button>
                                                <a href="{{route('auditable_entities.inquiry')}}" class="btn btn-warning btn-clear-filter"><i class="fa fa-times-circle"></i> Clear</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="widget-box widget-color-blue2">
                        <div class="widget-header">
                            <h4 class="widget-title lighter smaller"><i class="fa fa-list-alt"></i> Inquiry Result</h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main padding-8">
                                <div id="entities_tree"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
{{--COSO Modal--}}
@include('controls.coso_tree_modal')
@include('includes.modals.additional_information')
<script type="text/template" id="details-row-temp"><div class="profile-info-row"><div class="profile-info-name"></div><div class="profile-info-value"></div></div></script>
@endsection

@section('footer_script')
    <script src="/js/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/plugins/fancytree/jquery.fancytree-all.min.js"></script>
    <script src="/plugins/selectize/selectize.js"></script>
    <script type="text/javascript">
        var xhr;
        var select_company, $select_company;
        var select_branch, $select_branch;
        var select_dept, $select_dept;
        var select_main_bp, $select_main_bp;
        var select_sub_bp, $select_sub_bp;
        var select_obj, $select_obj;
        var select_risk, $select_risk;
        var select_control, $select_control;
        var starting_point = $('input[name=subtreeview]');
        var _type = $('input[name=_type]');
        var subtreeview = $('input[name="subtreeview"]:checked').val();
        var tree;
        var tree_source = '{{ route('auditable_entities.inquiry.tree') }}?type=entity{!! Request::getQueryString() ? '&'.Request::getQueryString() : '' !!}';
        var children = [];
        var search = false;
        var query = '';
        var filter = $('.filter-wrapper');
        var company_filter = $('.company-filter');
        var lookup_url = '{{url('/administrator/lookup/type')}}';
        var coso_components = [];
        var coso_data = {!! !is_null($coso_components['data']) ? json_encode($coso_components['data']) : [] !!};

        $('.btn-filter').on('click', function(){
            $( "#filter-form" ).submit();
            return false;
        });

        $(document).ready(function () {
            $select_company = $('#company').selectize({
                valueField: 'company_code',
                labelField: 'auditable_entity_name',
                searchField: ['auditable_entity_name'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                                '<span class="auditable_entity_name">' + escape(item.auditable_entity_name) + '</span>' +
                            '</span>' +
                        '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: '{{route('auditable_entities.inquiry.filter')}}?search='+escape(query)+'&searchFields=auditable_entity_name:like{{isset(\Request::get('filter')['branch']) ? '&branch_code='.\Request::get('filter')['branch'] : ''}}',
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(res) {
                            callback(res);
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    select_branch.clear();
                    select_branch.clearOptions();
                    select_branch.load(function(callback) {
                        xhr && xhr.abort();
                        xhr = $.ajax({
                            url: '{{route('auditable_entities.inquiry.filter')}}?company_code=' + encodeURIComponent(value),
                            success: function(results) {
                                select_branch.settings.searchField = ['lv_branch_code_desc'];
                                select_branch.settings.valueField = 'branch_code';

                                callback(results);
                            },
                            error: function() {
                                callback();
                            }
                        })
                    });
                },
                onLoad: function() {
                    @if(isset(\Request::get('filter')['company']))
                    select_company.setValue('{{\Request::get('filter')['company']}}');
                    @endif
                }
            });
            $select_branch = $('#branch').selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: ['description'],
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function(item, escape) {
                        var text = item.hasOwnProperty('text') ? item.text : item.branch_code;
                        var desc = item.hasOwnProperty('description') ? item.description : item.lv_branch_code_desc;
                        return '<div>' +
                            '<span class="title">' +
                                '<span class="branch_code">' + escape(text) + ' | ' + escape(item.meaning) +  ' | ' + escape(desc) + '</span>' +
                            '</span>' +
                        '</div>';
                    }
                },
                load: function(query, callback) {
                    var search = encodeURIComponent(query || '{{isset($tree) && count($tree) && isset($tree[0]['details']['Branch']) ? $tree[0]['details']['Branch'] : '' }}');
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.branch.code') }}?search='+ search +'&searchFields=description:like&limit=10',
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(res) {
                            callback(res.data);
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    select_dept.load(function(callback) {
                        xhr && xhr.abort();
                        xhr = $.ajax({
                            url: '{{route('auditable_entities.inquiry.filter')}}?branch_code=' + encodeURIComponent(value),
                            beforeSend : function(){
                                if(select_company.getValue() == '') {
                                    load_company(true, value);
                                }
                            },
                            success: function(results) {
                                callback(results);
                            },
                            error: function() {
                                callback();
                            }
                        })
                    });
                },
                onLoad: function() {
                    @if(isset(\Request::get('filter')['branch']))
                    select_branch.setValue('{{\Request::get('filter')['branch']}}');
                    @endif
                }
            });
            $select_dept = $('#department').selectize({
                valueField: 'text',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                                '<span class="department_code">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                        '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.dept_code') }}?limit=10',
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(res) {
                            callback(res.data);
                        }
                    });
                },
                onLoad: function() {
                    @if(isset(\Request::get('filter')['department']))
                    select_dept.setValue('{{\Request::get('filter')['department']}}');
                    @endif
                }
            });
            select_branch  = $select_branch[0].selectize;
            select_company = $select_company[0].selectize;
            select_dept = $select_dept[0].selectize;

            $select_main_bp = $('#main_bp').selectize({
                valueField: 'meaning',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.id) + ' - </span>' +
                            '<span class="by">' + escape(item.meaning) +'</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.main_bp') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onLoad: function() {
                    @if(isset(\Request::get('filter')['main_bp']))
                    select_main_bp.setValue('{{\Request::get('filter')['main_bp']}}');
                    @endif
                }
            });
            select_main_bp = $select_main_bp[0].selectize;

            $select_sub_bp = $('#sub_bp').selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.id) + ' - </span>' +
                            '<span class="by">' + escape(item.meaning) +'</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.business_process') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onLoad: function() {
                    @if(isset(\Request::get('filter')['sub_bp']))
                    select_sub_bp.setValue('{{\Request::get('filter')['sub_bp']}}');
                    @endif
                }
            });
            select_sub_bp = $select_sub_bp[0].selectize;

            $select_risk = $('#risk').selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.id) + ' - </span>' +
                            '<span class="by">' + escape(item.meaning) +'</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.risk.risks') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onLoad: function() {
                    @if(isset(\Request::get('filter')['risk']))
                    select_risk.setValue('{{\Request::get('filter')['risk']}}');
                    @endif
                }
            });
            select_risk = $select_risk[0].selectize;

            $select_obj = $('#objective').selectize({
                valueField: 'bp_objective_id',
                labelField: 'objective_name',
                searchField: ['objective_name', 'objective_narrative'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.objective_name) + ' - </span>' +
                            '<span class="name">' + escape(item.objective_narrative) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: '{{ route('business_process.objectives') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    var obj_narrative = select_obj.options[value].objective_narrative;
                    $('span#objective_narrative').html(obj_narrative);
                },
                onLoad: function() {
                    @if(isset(\Request::get('filter')['objective']))
                    select_obj.setValue('{{\Request::get('filter')['objective']}}');
                    @endif
                }
            });
            select_obj = $select_obj[0].selectize;

            $select_control = $('#control').selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.id) + ' - </span>' +
                            '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) +'</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.control.controls') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onLoad: function() {
                    @if(isset(\Request::get('filter')['control']))
                    select_control.setValue('{{\Request::get('filter')['control']}}');
                    @endif
                }
            });
            select_control = $select_control[0].selectize;

            tree = $('#entities_tree').fancytree({
                debugLevel: 0,
                source : {!! json_encode(!is_null($tree) ? $tree : [['title' => 'No data to display.', 'icon' => 'fa fa-exclamation-circle']]) !!},
                lazyLoad: function(event, data) {
                    var node = data.node;
                    var meta = node.data.meta;
                    var uri_company = typeof node.data.company !== undefined ? '&company='+node.data.company : '';
                    var view = meta != null && meta['treeview'] != "undefined" ? meta['treeview'] : subtreeview;
                    var query = '';
                    if(typeof node.data.branch != "undefined") query += "&filter[branch]="+node.data.branch;
                    if(typeof meta['root_id'] != "undefined") query += "&filter[root_id]="+meta['root_id'];
                    if(typeof meta['root_name'] != "undefined") query += "&filter[root_name]="+meta['root_name'];

                    data.result = {
                        url: '{{ route('auditable_entities.inquiry') }}?json=true&id='+node.key+'&_type='+node.data.type+'&subtreeview='+view+query+'&level='+node.data.meta.level,
                        data: {
                            key: node.id,
                            toggle: 'popover'
                        }
                    }
                },
                postProcess: function(event, data) {
                    if( subtreeview != "all" && data.node.data.type == 'bp' ) {
                        var resp = data.response;
                        if( typeof resp != "undefined") {
                            for (var i = resp.length - 1; i >= 0; i--) {
                                resp[i]['children'] = getChildren(resp[i]);
                            }
                        }
                        data.result = resp;
                    }
                },
                renderNode  : function(event, data) {
                    var node = data.node;
                    var node_li = $(node.li);
                    var $nodeSpan = $(node.span);
                    if (!$nodeSpan.data('rendered')) {
                        if( typeof node.data.type != "undefined" ) {
                            var temp_row = $('#details-row-temp').html();
                            var html = '';
                            var meta = '';

                            if( typeof node.data.details == "object" ) {
                                for(var label in node.data.details) {
                                    var row = $(temp_row).find('.profile-info-name').html(label).parent();
                                    row = row.find('.profile-info-value').html(node.data.details[label] ? node.data.details[label] : '--').parent();
                                    html += $('<div />').append(row.clone()).html();
                                }
                            }

                            if( node.data.meta != null ) {
                                if( typeof node.data.meta.modal != "undefined" ) {
                                    meta = '<a href="#" class="btn-view-coso" data-id="'+node.key+'" title="COSO components" rel="tooltip"><i class="fa fa-cubes"></i></a> &nbsp; ';
                                    node_li.find('span.fancytree-node').append(meta);
                                }
                                if( typeof node.data.meta.coso_data != "undefined" && node.data.meta.coso_data.component_code != "" ) {
                                    node.data.meta.coso_data.component_code = JSON.parse(node.data.meta.coso_data.component_code);
                                    coso_components[node.key] = node.data.meta.coso_data;
                                }
                            }

                            if( html != '' ) {
                                html = "<div class='profile-user-info profile-user-info-striped'>"+html+"</div>";
                                node_li.find('span.fancytree-node').append('<span class="tree-details" rel="popover"><i class="fa fa-question-circle"></i></span>');

                                $('.tree-details', node_li).popover({
                                    trigger : 'click',
                                    html : true,
                                    placement : 'auto left',
                                    title : 'Details',
                                    content : html
                                });
                            }
                            $nodeSpan.data('rendered', true);
                        }
                    }
                }
            });
            starting_point.trigger('change');
        });

        function getChildren(data) {
            var childs = ['steps', 'risks', 'controls'];
            var index = childs.indexOf(subtreeview);
            var temp = [];
            var resp = typeof data['objectives'] != "undefined" ? data['objectives']['data'] : data; // assign default value to response

            if( index != -1 ) {
                for (var x = resp.length - 1; x >= 0; x--) {
                    if( typeof resp[x][ childs[0] ]['data'] == "object" ) {
                        var first_data = resp[x][ childs[0] ]['data'];
                        for (var d = first_data.length - 1; d >= 0; d--) {
                            if( typeof first_data[d][ childs[1] ] != "undefined" ) {
                                var second_data = first_data[d][ childs[1] ]['data'];
                                for (var e = second_data.length - 1; e >= 0; e--) {
                                    if( typeof second_data[e][ childs[2] ] != "undefined" ) {
                                        var third_data = second_data[e][ childs[2] ]['data'];
                                        for (var f = third_data.length - 1; f >= 0; f--) {
                                            temp.push(third_data[f]);
                                        }
                                    }
                                    else {
                                        temp.push(second_data[e]);
                                    }
                                }
                            }
                            else {
                                temp.push(first_data[d]);
                            }
                        }
                    }
                }
                resp = temp;
            }
            return resp
        }

        $(document).on('click', function (e) {
            $('.tree-details').each(function () {
                //the 'is' for buttons that trigger popups
                //the 'has' for icons within a button that triggers a popup
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
                }
            });
        });
        starting_point.on('change', function(){
            filter.hide();

            switch ($('input[name="subtreeview"]:checked').val()) {
                case 'branch':
                    load_company(false);
                    _type.val('entity');
                    company_filter.hide();
                    company_filter.not(':eq(2)').show();
                    break;
                case 'dept':
                    load_company(false);
                    _type.val('entity');
                    company_filter.show();
                    break;
                case 'mainbp':
                    _type.val('mainBp');
                    $('.mainbp-filter').show();
                    break;
                case 'subbp':
                    _type.val('subBp');
                    filter.hide();
                    $('.subbp-filter').show();
                    break;
                case 'objective':
                    _type.val('Objective');
                    $('.objective-filter').show();
                    break;
                case 'risk':
                    _type.val('Risk');
                    $('.risk-filter').show();
                    break;
                case 'control':
                    _type.val('Control');
                    $('.control-filter').show();
                    break;
                default:
                    // all
                    _type.val('entity');
                    company_filter.eq(0).show();
                    break;
            }
        });

        function load_company(by_branch, branch) {
            var url = '{{route('auditable_entities.inquiry.filter')}}?search=&searchFields=auditable_entity_name:like';
            url += by_branch ? '&branch_code='+encodeURIComponent(branch)+'&by_branch=true' : '';
            select_company.clearOptions();

            select_company.load(function(callback) {
                xhr && xhr.abort();
                xhr = $.ajax({
                    url: url,
                    success: function (results) {
                        callback(results);
                    },
                    error: function () {
                        callback();
                    }
                });
            });
            return false;
        }
    </script>
    @stack('coso_script')
@endsection
