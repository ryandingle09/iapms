<div class="col-xs-12">
    <div class="table-header"> Risk Details </div>
    <table class="table table-striped table-bordered table-details">
        <thead>
            <tr>
                <th>Risk Name</th>
                <th>Risk Description</th>
                <th>Risk Type</th>
                <th>Remarks</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="label-risk_code"></td>
                <td class="label-lv_risk_code_desc"></td>
                <td class="label-risk_type"></td>
                <td class="label-risk_remarks"></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="col-xs-12">
    <div class="table-header"> Risk Control Category </div>
    <table id="datatable-risk_ctrl" class="table table-striped table-bordered table-hover datatable-details">
        <thead>
            <tr>
                <th width="5%">Control Sequence</th>
                <th>Control Name</th>
                <th>Control Code</th>
                <th width="15%">Control Description</th>
                <th>Component Code</th>
                <th>Principle Code</th>
                <th>Focus Code</th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-12" style="padding-top: 10px;">
    <div class="table-header">
        Control Details
    </div>
    <table class="table table-striped table-bordered table-details">
        <tbody>
            <tr>
                <th width="15%">Control Sequence</th>
                <td colspan="2" class="label-pivot label-risk_control_seq"></td>
            </tr>
            <tr>
                <th width="15%">Control Name</th>
                <td colspan="2" class="label-control_name"></td>
            </tr>
            <tr>
                <th width="15%">Control Code</th>
                <td colspan="2" class="label-control_code"></td>
            </tr>
            <tr>
                <th width="15%">Control Description</th>
                <td colspan="2" class="label-lv_control_code_desc"></td>
            </tr>
            <tr>
                <th width="15%">Component Code</th>
                <td colspan="2" class="label-component_code"></td>
            </tr>
            <tr>
                <th width="15%">Component Code Description</th>
                <td colspan="2" class="label-lv_component_code_desc"></td>
            </tr>
            <tr>
                <th width="15%">Principle Code</th>
                <td colspan="2" class="label-principle_code"></td>
            </tr>
            <tr>
                <th width="15%">Principle Code Description</th>
                <td colspan="2" class="label-lv_principle_code_desc"></td>
            </tr>
            <tr>
                <th width="15%">Focus Code</th>
                <td colspan="2" class="label-focus_code"></td>
            </tr>
            <tr>
                <th width="15%">Focus Code Description</th>
                <td colspan="2" class="label-lv_focus_code_desc"></td>
            </tr>
            <tr>
                <th width="15%">Assertions</th>
                <td colspan="2" class="label-assertions"></td>
            </tr>
            <tr>
                <th width="15%">Control Types</th>
                <td colspan="2" class="label-control_types"></td>
            </tr>
            <tr>
                <th width="15%">Application</th>
                <td colspan="2" class="label-application"></td>
            </tr>
            <tr>
                <th width="15%">Application Description</th>
                <td colspan="2" class="label-lv_application_desc"></td>
            </tr>
            <tr>
                <th width="15%">Control Source</th>
                <td colspan="2" class="label-control_source"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <button type="button" class="btn btn-primary btn-sm btn-add-info-control" style="display: none;"><i class="fa fa-list-ul"></i> Additional Info</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>

@section('risk_ctrl_js')
<script type="text/javascript">
    var table_risk_ctrl = $('#datatable-risk_ctrl');
    var assertions = {!! !is_null($assertions) ? ($assertions->lookupValue->count() ? json_encode($assertions->lookupValue->pluck('description', 'lookup_code')) : '[]' ) : '[]' !!};
    var control_types = {!! !is_null($types) ? ($types->lookupValue->count() ? json_encode($types->lookupValue->pluck('description', 'lookup_code')) : '[]' ) : '[]' !!};
    var datatable_risk_ctrl = table_risk_ctrl.DataTable( {
        data: [],
        processing: true,
        orderCellsTop: true,
        columns: [
            { data: "pivot.risk_control_seq", searchable : false },
            { data: "control_name" },
            { data: "control_code" },
            { data: "lv_control_code_desc", orderable : false, searchable : false },
            { data: "component_code" },
            { data: "principle_code" },
            { data: "focus_code" }
        ]
    } );
    // dtClickable(table_risk_ctrl, datatable_risk_ctrl, 'control_det_id', 'datatable_ctrl_proc');

    function setRiskControlData(data) {
        datatable_risk_ctrl.clear();
        datatable_risk_ctrl.rows.add(data).draw();
    }

    table_risk_ctrl.find('tbody').on('click', 'tr', function () {
        table_risk_ctrl.find('tbody > tr').removeClass('selected-row'); // reset tr highlights
        var data = datatable_risk_ctrl.row( this ).data();

        $(this).addClass('selected-row');
        setControlProcData( data.test_procedures );
        $('.btn-add-info-control').attr('data-id', data['control_id']).show();
        clearDetails();
        $.each(data, function(i, v){
            var label = '.label-'+i;
            if( $(label).length ) {
                if(i == 'pivot') {
                    $('.label-risk_control_seq').html(v.risk_control_seq);
                }
                else if(i == 'assertions' || i == 'control_types' ) {
                    var htm = '';
                    if( v.length ) {
                        htm = '<ul class="list-unstyled spaced">';
                        for(var x = 0; x < v.length; x++) {
                            if( i == 'assertions' )
                                var desc = typeof assertions[ v[x] ] != "undefined" ? assertions[ v[x] ] : 'N/A';
                            else
                                var desc = typeof control_types[ v[x] ] != "undefined" ? control_types[ v[x] ] : 'N/A';

                            htm += '<li><i class="ace-icon fa fa-angle-right bigger-110"></i> '+v[x]+' - '+desc+'</li>';
                        }
                    }
                    htm += '</ul>';
                    $('.label-'+i).html(htm);
                }
                else {
                    var value = v != null ? v : 'N/A';
                    $(label).html(value);
                }
            }
        });
        if( !data.test_procedures.length ) {
            swal("Empty!", "There's no data available for the selected item.", "info");
        }
    } );

    $('.btn-add-info-control').on('click', function(){
        var table = 'controls_master';
        var id = $(this).attr('data-id');
        $('.btn-save-ai').hide();
        $.ajax({
            url: '{{ url('additional_information/table') }}/'+id+'/'+table,
            method: "GET",
            data: { _token : '{{ csrf_token() }}' }, // serializes all the form data
            beforeSend : function() {
                toggle_loading();
            },
            success : function(data) {
                if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                }
                ai_modal.find('#additional-information-form input').attr('disabled', true);
                ai_modal.find('#additional-information-form select').attr('disabled', true);
                ai_modal.find('.btn-save-ai').hide();
                ai_modal.modal('show');

                toggle_loading();
            }
        });
    });
</script>
@endsection