@extends('layouts.app')
@section('styles')

    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <style type="text/css">
        .label-default {
            width: 100%;
            text-align: right;
            background-color: #1f7d74;
        }
        .row-details {
            padding-bottom: 5px;
        }
        .selected-row {
            background-color: #ffe99e !important;
        }
        .table-hover tbody > tr:hover {
            cursor: pointer;
        }
    </style>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')
            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif

                    <div class="tabbable">
                        <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="inquiries">
                            <?php
                                $q = explode('|', Request::get('q'));
                                $back_q = count($q) > 0 ? array_splice($q, 0, -1) : ''; // Back url | (count($q) - 2) = last tab id or code
                                $back_q = implode('|', $back_q);
                            ?>
                            <li class="active">
                                <a href="#entities" data-toggle="tab" aria-expanded="false">Auditable Entities</a>
                            </li>

                            <li style="display:none;">
                                <a href="#bp" data-toggle="tab" aria-expanded="false">Business Process</a>
                            </li>

                            <li style="display:none;">
                                <a href="#bp_objectives" data-toggle="tab" aria-expanded="true">Business Process Objective - Steps</a>
                            </li>

                            <li style="display:none;" class="bp_objectives_show">
                                <a href="#risk" data-toggle="tab" aria-expanded="true">Risk</a>
                            </li>

                            <li style="display:none;" class="bp_objectives_show">
                                <a  href="#risk_control" data-toggle="tab" aria-expanded="true">Control</a>
                            </li>

                            <li style="display:none;" class="bp_objectives_show">
                                <a href="#control_proc" data-toggle="tab" aria-expanded="true">Control Procedure</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <!-- Entities -->
                            <div id="entities" class="tab-pane active">
                                <div class="row">
                                    @include('auditable_entities.inquiry.auditable_ent')
                                </div>
                            </div>
                            <!-- /Entities -->

                            <!-- Business Process -->
                            <div id="bp" class="tab-pane">
                                <div class="row">
                                    @include('auditable_entities.inquiry.bp')
                                </div>
                            </div>
                            <!-- /Business Process -->

                            <!-- Business Process Area -->
                            <div id="bp_objectives" class="tab-pane">
                                <div class="row">
                                    @include('auditable_entities.inquiry.bp_objectives')
                                </div>
                            </div>
                            <!-- /Business Process Area -->

                            <!-- Risks -->
                            <div id="risk" class="tab-pane">
                                <div class="row">
                                    @include('auditable_entities.inquiry.risk')
                                </div>
                            </div>
                            <!-- /Risks -->

                            <!-- Risk Controls -->
                            <div id="risk_control" class="tab-pane">
                                <div class="row">
                                    @include('auditable_entities.inquiry.risk_control')
                                </div>
                            </div>
                            <!-- /Risk Controls -->

                            <!-- Control Test Procedure -->
                            <div id="control_proc" class="tab-pane">
                                <div class="row">
                                    @include('auditable_entities.inquiry.control_proc')
                                </div>
                            </div>
                            <!-- /Control Test Procedure -->
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@include('includes.modals.additional_information')
@endsection

@section('footer_script')
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var tab;
    var current_tab;
    var inquiry = $('ul#inquiries');

    $(document).ready(function() {
        current_tab = inquiry.find('li.active');
        $('#au-buttons').hide();
    } );

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        current_tab = $(e.target).parent();
    })

    /**
     * Set  Auditable Entity and Business Process details on designated labels
     */
    function dtClickable( table, datatable, id, next, data_id ) {
        table.find('tbody').on('click', 'tr', function () {
            var data = datatable.row( this ).data();
            var next_tab = current_tab.next(); // get next tab element
            var next_tab_id = next_tab.find('a').attr('href');
            var tab_container = current_tab.find('a').attr('href');

            if( typeof data != "object" ) return false;

            table.find('tbody > tr').removeClass('selected-row'); // reset tr highlights

            next_tab.show();
            $('#au-buttons').show();
            $(this).addClass('selected-row');
            $('.next-display').show();
            $(tab_container).find('button.btn-addinfo').attr('data-id', data[id]).show();
            // populate next datatable
            if( next !== undefined ) getTabData(next_tab_id.replace('#', ''), data[id], next, data_id);

            // additional info button
            $(current_tab.find('a').attr('href')).find('.btn-additional-info').attr('data-id', data[id]);

            clearDetails(tab_container);

            if( tab_container == '#bp' ) datatable_audit_type.clear().draw();

            $.each(data, function(i, v){
                var label = '.label-'+i;
                if( $(tab_container).find(label).length ) {
                    if( i =='parent' && v != null ) {
                        $(tab_container).find(label).html(v.auditable_entity_name);
                    }
                    else if(i.indexOf('_rate') != -1) {
                        $(tab_container).find(label).raty('set', {score: v});
                    }
                    else {
                        var value = v != null ? v : 'N/A';
                        $(tab_container).find(label).html(value);
                    }

                    if( i =='company_code' ) {
                        $('.btn-company-profile').prop('href', '{{ url('company_profile/details') }}/'+v);
                    }
                }
            });
        } );
    }

    /**
     * Get next tab datatable data
     * @param  string tab   tab ID
     * @param  string q     criteria for the next tab data
     * @return void
     */
    function getTabData( tab, q, next, data_id ) {
        $.ajax({
            url: '{{ url('auditable_entities/inquiry') }}/'+tab,
            method: "POST",
            data: { q : q, _token : '{{ csrf_token() }}' }, // serializes all the form data
            beforeSend : function() {
                toggle_loading();
            },
            success : function(data) {
                if( data.success ) {
                    var dt = eval(next);
                    var dt_data = data_id !== undefined ? data.data[data_id] : data.data;
                    dt.clear();
                    dt.rows.add(dt_data).draw();

                    init_tabs(dt_data.length);

                    if( dt_data.length < 1 ) {
                        swal("Empty!", "There's no data available for the selected item.", "info");
                    }
                }
                toggle_loading();
            }
        });
    }

    // Clear all details including next tab
    function clearDetails(tab_container) {
        var current = current_tab;
        var next_tab = current.length;
        while( next_tab > 0 ) {
            if( current.is(':visible') ) {
                var tab_container = current.find('a').attr('href');
                $(tab_container).find('.table-details td').each(function(){
                    if( $(this).attr('class') != undefined ) {
                        $(this).html('');
                    }
                });
            }
            current = current.next();
            next_tab = current.length;
        }
        $('.rating').raty('set', {score: 0});
    }

    /**
     * Hide remaining tabs if selected item has no data
     * @param  integer data_count
     * @return void
     */
    function init_tabs(data_count) {
        if( data_count < 1 ) {
            var tabs = inquiry.find('li:visible').length;
            var curr_index = current_tab.index();
            var except = curr_index + 1; // exclude the current tab and the next tab to be hidden when there's no data available on selected item

            for (var i = except; i < tabs; i++) {
                inquiry.find('li:eq('+i+')').hide();
            }
        }
    }
</script>
@yield('ae_js')
@yield('bp_js')
@yield('bp_step_js')
@yield('risk_js')
@yield('risk_ctrl_js')
@yield('ctrl_proc_js')
@endsection
