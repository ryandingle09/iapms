<div class="col-xs-12">
    <div class="table-header"> Business Process Area - Risks  </div>
    <table class="table table-striped table-bordered">
        <thead>
            <tr>
                <th width="10%">Sequence</th>
                <th width="90%">Activity Narrative</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td align="center">#<span class="label-bp_steps_seq"></span></td>
                <td class="label-activity_narrative"></td>
            </tr>
        </tbody>
    </table>
</div>
<div class="col-xs-12">
    <div class="table-header"> Risks </div>
    <table id="datatable-risk" class="table table-striped table-bordered table-hover datatable-details">
        <thead>
            <tr>
                <th>Risk Name</th>
                <th>Risk Description</th>
                <th>Risk Type</th>
                <th>Risk Remarks</th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-12" style="padding-top: 10px;">
    <div class="table-header">
        Risk Details
    </div>
    <table class="table table-striped table-bordered table-details">
        <tbody>
            <tr>
                <th width="20%">Risk Name</th>
                <td colspan="2" class="label-risk_code"></td>
            </tr>
            <tr>
                <th>Description</th>
                <td colspan="2" class="label-lv_risk_code_desc"></td>
            </tr>
            <tr>
                <th>Risk Type</th>
                <td colspan="2" class="label-risk_type"></td>
            </tr>
            <tr>
                <th>Risk Type Description</th>
                <td colspan="2" class="label-lv_risk_type_desc"></td>
            </tr>
            <tr>
                <th>Risk Remarks</th>
                <td colspan="2" class="label-risk_remarks"></td>
            </tr>
            <tr>
                <th>Source Type</th>
                <td colspan="2" class="label-risk_source_type"></td>
            </tr>
            <tr>
                <th>Source Type Description</th>
                <td colspan="2" class="label-lv_source_type_desc"></td>
            </tr>
            <tr>
                <th>Response Type</th>
                <td colspan="2" class="label-risk_response_type"></td>
            </tr>
            <tr>
                <th>Response Type Description</th>
                <td colspan="2" class="label-lv_response_type_desc"></td>
            </tr>
            <tr>
                <th>&nbsp;</th>
                <th width="12%">Impact</th>
                <th>Likelihood</th>
            </tr>
            <tr>
                <th>Default Inherent Rating</th>
                <td><div class="rating label-def_inherent_impact_rate"></div></td>
                <td><div class="rating label-def_inherent_likelihood_rate"></div></td>
            </tr>
            <tr>
                <th>Default Residual Rating</th>
                <td><div class="rating label-def_residual_impact_rate"></div></td>
                <td><div class="rating label-def_residual_likelihood_rate"></div></td>
            </tr>
            <tr>
                <th>Default Rating Remarks</th>
                <td colspan="2" class="label-rating_comment"></td>
            </tr>
            <tr>
                <th>Enterprise Risk Code</th>
                <td colspan="2" class="label-enterprise_risk_code"></td>
            </tr>
            <tr>
                <th>Enterprise Risk Code Description</th>
                <td colspan="2" class="label-lv_enterprise_risk_code_desc"></td>
            </tr>
            <tr>
                <th>Enterprise Risk Type</th>
                <td colspan="2" class="label-enterprise_risk_type"></td>
            </tr>
            <tr>
                <th>Enterprise Risk Type Description</th>
                <td colspan="2" class="label-lv_enterprise_risk_type_desc"></td>
            </tr>
            <tr>
                <th>Enterprise Risk Class</th>
                <td colspan="2" class="label-enterprise_risk_class"></td>
            </tr>
            <tr>
                <th>Enterprise Risk Class Description</th>
                <td colspan="2" class="label-lv_enterprise_risk_class_desc"></td>
            </tr>
            <tr>
                <th>Enterprise Risk Area</th>
                <td colspan="2" class="label-enterprise_risk_area"></td>
            </tr>
            <tr>
                <th>Enterprise Risk Area Description</th>
                <td colspan="2" class="label-lv_enterprise_risk_area_desc"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <button type="button" class="btn btn-primary btn-sm btn-add-info-risk" data-id="" style="display: none;"><i class="fa fa-list-ul"></i> Additional Info</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>

@section('risk_js')
<script src="/js/jquery.raty.js"></script>
<script type="text/javascript">
    var table_risk = $('#datatable-risk');
    var datatable_risk = table_risk.DataTable( {
        data: [],
        processing: true,
        orderCellsTop: true,
        columns: [
            { data: "risk_code" },
            { data: "lv_risk_code_desc", searchable: false, orderable: false },
            { data: "risk_type", searchable: false },
            { data: "risk_remarks", searchable: false, orderable: false }
        ]
    } );

    function setRaty(element, cancel, read_only, color) {
        element = element != undefined ? element : '.rating';
        cancel = cancel != undefined ? cancel : false;
        read_only = read_only != undefined ? read_only : true;
        color = color != undefined ? color : 'orange2';

        // raty
        $(element).raty({
            score: function() {
                return $(this).attr('data-rating');
            },
            readOnly: read_only,
            'cancel' : cancel,
            'half': false,
            'starType' : 'i',
            'starOn' : rate_class,
            'starOff' : 'star-off-png',
            hints       : ['1', '2', '3', '4', '5'],
            'mouseover': function(e) {
                setRatingColors.call(this, e, true);
            },
            'mouseout': function(e) {
                setRatingColors.call(this, e, false);
            },
        });
    }

    $(document).ready(function() {
        setRaty();
    } );

    // dtClickable(table_risk, datatable_risk, 'risk_code');

    function setRiskData(data) {
        datatable_risk.clear();
        datatable_risk.rows.add(data).draw();
    }

    table_risk.find('tbody').on('click', 'tr', function () {
        table_risk.find('tbody > tr').removeClass('selected-row'); // reset tr highlights
        var data = datatable_risk.row( this ).data();

        $(this).addClass('selected-row');
        setRiskControlData( data.controls );
        $('.btn-add-info-risk').attr('data-id', data['risk_id']).show();
        clearDetails();
        $.each(data, function(i, v){
            var label = '.label-'+i;
            if( $(label).length ) {
                if( i =='parent' && v != null ) {
                    $(label).html(v.auditable_entity_name);
                }
                else if(i.indexOf('_rate') != -1) {
                    $(label).raty('set', {score: v});
                }
                else {
                    var value = v != null ? v : 'N/A';
                    $(label).html(value);
                }

                if( i =='company_code' ) {
                    $('.btn-company-profile').prop('href', '{{ url('company_profile/details') }}/'+v);
                }
            }
        });

        if( !data.controls.length ) {
            swal("Empty!", "There's no data available for the selected item.", "info");
        }
    } );

    $('.btn-add-info-risk').on('click', function(){
        var table = 'risks';
        var id = $(this).attr('data-id');
        $('.btn-save-ai').hide();
        $.ajax({
            url: '{{ url('additional_information/table') }}/'+id+'/'+table,
            method: "GET",
            data: { _token : '{{ csrf_token() }}' }, // serializes all the form data
            beforeSend : function() {
                toggle_loading();
            },
            success : function(data) {
                if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                }
                ai_modal.find('#additional-information-form input').attr('disabled', true);
                ai_modal.find('#additional-information-form select').attr('disabled', true);
                ai_modal.find('.btn-save-ai').hide();
                ai_modal.modal('show');

                toggle_loading();
            }
        });
    });
</script>
@endsection