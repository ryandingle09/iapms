<div class="col-xs-12">
    <div class="table-header"> Objectives </div>
    <table id="datatable-objective" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>Objective Narrative</th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-12">
    <div class="table-header"> Audit Types </div>
    <table id="datatable-audit-types" class="table table-striped table-bordered datatable-static">
        <thead>
            <tr>
                <th width="30%">Audit Type</th>
                <th width="70%">Description</th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-12">
    <div class="table-header"> Business Process Area </div>
    <table id="datatable-bps" class="table table-striped table-bordered table-hover datatable-details">
        <thead>
            <tr>
                <th width="10%">Sequence</th>
                <th width="90%">Activity Narrative</th>
                <th></th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
</div>

@section('bp_step_js')
<script type="text/javascript">
    var table_objective = $('#datatable-objective');
    var table_bps = $('#datatable-bps');
    var table_audit_type = $('#datatable-audit-types');
    var datatable_objective= table_objective.DataTable( {
        data: [],
        processing: true,
        orderCellsTop: true,
        searching: false,
        columns: [
            { data: "objective_narrative", orderable: false, searchable : false }
        ]
    } );
    var datatable_bps = table_bps.DataTable( {
        data: [],
        processing: true,
        orderCellsTop: true,
        searching: false,
        columns: [
            { data: "bp_steps_seq" },
            { data: "activity_narrative", orderable: false, searchable : false },
            {
                data: null,
                className: 'tbl-controls',
                render: function(data) {
                    return '<a href="#" class="btn-add-info-bps" data-id="'+data.bp_steps_id+'" title="additional info" rel="tooltip"><i class="fa fa-list-ul"></i></a>';
                },
                orderable: false, searchable : false
            }
        ]
    } );
    // dtClickable(table_bps, datatable_bps, 'bp_steps_id');

    var datatable_audit_type = table_audit_type.DataTable( {
        data: [],
        processing: true,
        orderCellsTop: true,
        searching: false,
        columns: [
            { data: "audit_type" },
            { data: "lv_audit_type_desc", orderable: false, searchable : false }
        ]
    } );
    table_objective.find('tbody').on('click', 'tr', function () {
        table_objective.find('tbody > tr').removeClass('selected-row'); // reset tr highlights
        var data = datatable_objective.row( this ).data();

        $(this).addClass('selected-row');
        datatable_bps.clear();
        datatable_bps.rows.add(data.bp_steps).draw();

        var risk_data = processData(data.bp_steps, 'bp_step_risk', 'risk_id');
        var risk_control_data = processData(risk_data, 'controls', 'control_id');
        var control_proc_data = processData(risk_control_data, 'test_procedures', 'control_test_id');
        var audit_types_data = processData(control_proc_data, 'audit_types', 'audit_type');

        setRiskData( risk_data );
        setRiskControlData(risk_control_data);
        setControlProcData(control_proc_data);
        setAuditTypesData(audit_types_data);
        $('#risk, #risk_control, #control_proc').find('.table-details td').each(function(){
            if( $(this).attr('class') != undefined ) {
                $(this).html('');
            }
        });
        $('#control_proc').find('.ctrl_proc-details td').each(function(){
            if( $(this).attr('class') != undefined ) {
                $(this).html('');
            }
        });

        if( data.bp_steps.length ) {
            $('.bp_objectives_show').show();
        }
        else {
            swal("Empty!", "There's no data available for the selected item.", "info");
        }
    } );
    table_bps.find('tbody').on('click', 'tr', function (e) {
        if($(e.target).hasClass('tbl-controls') || $(e.target).hasClass('fa')) return true;

        table_bps.find('tbody > tr').removeClass('selected-row'); // reset tr highlights
        var data = datatable_bps.row( this ).data();

        $(this).addClass('selected-row');
        setRiskData( data.bp_step_risk );

        clearDetails();
        if( !data.bp_step_risk.length ) {
            swal("Empty!", "There's no data available for the selected item.", "info");
        }
        else {
            $.each(data, function(i, v){
                var label = '.label-'+i;
                if( $('#risk').find(label).length ) {
                    var value = v != null ? v : 'N/A';
                    $('#risk').find(label).html(value);
                }
            });
        }
    } );

    function processData(data, key, id) {
        var $return = [];
        var ids = [];
        for (var i = data.length - 1; i >= 0; i--) {
            if( data[i][key].length ) {
                for (var x = data[i][key].length - 1; x >= 0; x--) {
                    var curr_id = data[i][key][x][id];
                    if( ids.indexOf(curr_id) === -1 ) {
                        $return.push(data[i][key][x]);
                        ids.push(curr_id);
                    }
                }
            }
        }
        return $return;
    }

    function setAuditTypesData(data) {
        datatable_audit_type.clear();
        datatable_audit_type.rows.add(data).draw();
    }

    $('body').on('click', '.btn-add-info-bps', function(){
        var table = 'business_processes_steps';
        var id = $(this).attr('data-id');
        $('.btn-save-ai').hide();
        $.ajax({
            url: '{{ url('additional_information/table') }}/'+id+'/'+table,
            method: "GET",
            data: { _token : '{{ csrf_token() }}' }, // serializes all the form data
            beforeSend : function() {
                toggle_loading();
            },
            success : function(data) {
                if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                }
                ai_modal.find('#additional-information-form input').attr('disabled', true);
                ai_modal.find('#additional-information-form select').attr('disabled', true);
                ai_modal.find('.btn-save-ai').hide();
                ai_modal.modal('show');

                toggle_loading();
            }
        });

        return false;
    });
</script>
@endsection