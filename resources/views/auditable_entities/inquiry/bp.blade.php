<div class="col-xs-12">
    <div class="table-header">
        Business Process
    </div>

    <!-- div.table-responsive -->

    <!-- div.dataTables_borderWrap -->
    <table id="datatable-bp" class="table table-striped table-bordered table-hover datatable">
        <thead>
            <tr>
                <th>Business Process Name</th>
                <th>Business Process Code</th>
                <th>Business Process Description</th>
                <th>Objective Category</th>
                <th>Objective Category Description</th>
                <th>Source Type</th>
                <th>Source Type Description</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-12" style="padding-top: 10px;">
    <div class="table-header">
        Business Process Details
    </div>
    <table class="table table-striped table-bordered table-details">
        <tbody>
            <tr>
                <th width="15%">Name</th>
                <td colspan="2" class="label-bp_name"></td>
            </tr>
            <tr>
                <th>Business Process</th>
                <td colspan="2" class="label-bp_code"></td>
            </tr>
            <tr>
                <th>Business Process Description</th>
                <td colspan="2" class="label-bp_code_desc"></td>
            </tr>
            <tr>
                <th>Objective Category</th>
                <td colspan="2" class="label-objective_category"></td>
            </tr>
            <tr>
                <th>Objective Category Description</th>
                <td colspan="2" class="label-objective_category_desc"></td>
            </tr>
            <tr>
                <th>Source Type</th>
                <td colspan="2" class="label-source_type"></td>
            </tr>
            <tr>
                <th>Source Type Description</th>
                <td colspan="2" class="label-source_type_desc"></td>
            </tr>
            <tr>
                <th>Source Reference</th>
                <td colspan="2" class="label-source_ref"></td>
            </tr>
            <tr>
                <th>Remarks</th>
                <td colspan="2" class="label-bp_remarks"></td>
            </tr>
        </tbody>
    </table>
    <table class="table table-striped table-bordered table-details">
        <tbody>
            <tr>
                <th width="15%">Custom Measure1</th>
                <td class="label-cm_1_name"></td>
                <th width="15%">High Score Type</th>
                <td class="label-cm_1_high_score_type"></td>
            </tr>
            <tr>
                <th width="15%">Custom Measure2</th>
                <td class="label-cm_2_name"></td>
                <th width="15%">High Score Type</th>
                <td class="label-cm_2_high_score_type"></td>
            </tr>
            <tr>
                <th width="15%">Custom Measure3</th>
                <td class="label-cm_3_name"></td>
                <th width="15%">High Score Type</th>
                <td class="label-cm_3_high_score_type"></td>
            </tr>
            <tr>
                <th width="15%">Custom Measure4</th>
                <td class="label-cm_4_name"></td>
                <th width="15%">High Score Type</th>
                <td class="label-cm_4_high_score_type"></td>
            </tr>
            <tr>
                <th width="15%">Custom Measure5</th>
                <td class="label-cm_5_name"></td>
                <th width="15%">High Score Type</th>
                <td class="label-cm_5_high_score_type"></td>
            </tr>
            <tr>
                <td colspan="4">
                    <button type="button" class="btn btn-primary btn-sm btn-add-info-bp btn-addinfo" data-id="" style="display: none;"><i class="fa fa-list-ul"></i> Additional Info</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>

@section('bp_js')
<script type="text/javascript">
    var table_bp = $('#datatable-bp');
    var datatable_bp = table_bp.DataTable( {
        data: [],
        processing: true,
        orderCellsTop: true,
        columns: [
            { data: "bp_name" },
            { data: "bp_code", searchable: false },
            { data: "bp_code_desc", searchable: false, orderable: false },
            { data: "objective_category", searchable: false },
            { data: "objective_category_desc", searchable: false, orderable: false },
            { data: "source_type", searchable: false },
            { data: "source_type_desc", searchable: false, orderable: false }
        ]
    } );
    dtClickable(table_bp, datatable_bp, 'bp_id', 'datatable_objective');

    $('.btn-add-info-bp').on('click', function(){
        var table = 'business_processes';
        var id = $(this).attr('data-id');
        $('.btn-save-ai').hide();
        $.ajax({
            url: '{{ url('additional_information/table') }}/'+id+'/'+table,
            method: "GET",
            data: { _token : '{{ csrf_token() }}' }, // serializes all the form data
            beforeSend : function() {
                toggle_loading();
            },
            success : function(data) {
                if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                }
                ai_modal.find('#additional-information-form input').attr('disabled', true);
                ai_modal.find('#additional-information-form select').attr('disabled', true);
                ai_modal.find('.btn-save-ai').hide();
                ai_modal.modal('show');

                toggle_loading();
            }
        });
    });
</script>
@endsection