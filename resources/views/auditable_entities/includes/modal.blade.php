<!-- bp modal -->
<div class="modal fade" data-backdrop="static" id="bp-modal">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Business Process Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning modal-alert"></div>
                {!! Form::open(['method' => 'post', 'url' => route('auditable_entities.business_process.store', ['auditable_ent_id' => $details->auditable_entity_id]), 'class' => 'form-horizontal', 'id' => 'auditable-bp-modal-form']) !!}
                <h5>Business Processes</h5>
                <div class="form-group">
                    <div class="col-sm-12">
                        <select multiple="multiple" size="10" name="bps[]" id="duallist" style="height: 350px;"></select>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-auditable-bp-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /bp modal -->

<!-- bp main modal -->
<div class="modal fade" data-backdrop="static" id="bp-main-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Main Business Process Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning modal-alert"></div>
                {!! Form::open(['method' => 'post', 'url' => route('auditable_entities.bp.main.store', ['auditable_ent_id' => $details->auditable_entity_id]), 'class' => 'form-horizontal', 'id' => 'bp-main-modal-form']) !!}
                <input type="hidden" name="entity_id" value="{{ $details->auditable_entity_id}}">
                <input type="hidden" name="_method" value="post">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="main_bp"> Main Business Process </label>
                    <div class="col-sm-8">
                        <select id="main_bp" name="main_bp" class="form-control input-sm basic-info lookup" placeholder="Select a main process..." required></select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="custom_measure"> Custom Measures </label>
                    <div class="col-sm-4">
                        <button class="btn btn-primary btn-sm" type="button" id="btn-add-cm"><i class="fa fa-plus"></i> Add</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-bp-main-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script type="text/template" id="cm-template">
    <div class="form-group cm-group">
        <label class="col-sm-4 control-label text-right" for="custom_measure">#<span class="cm-number">||</span></label>
        <div class="col-sm-4">
            <input type="text" id="cm_||_name" name="custom_measure[||][name]" class="form-control input-sm mainbp-cm" value="" placeholder="Name">
        </div>
        <div class="col-sm-3">
            <select id="cm_||_high_score_type" name="custom_measure[||][value]" class="form-control input-sm mainbp-cm">
                <option value="">High score type</option>
                <option value="Floor">Floor</option>
                <option value="Ceiling">Ceiling</option>
            </select>
        </div>
        <div class="col-sm-1">
            <a href="#" class="delete btn-delete-cm" rel="tooltip" title="Remove"><i class="fa fa-times-circle-o"></i></a>
        </div>
    </div>
</script>
<script type="text/javascript">
    var cm_template = $('#cm-template').html();

    function createCM(value, hstype) {
        var cm_group = $('.cm-group');
        var cm_count = cm_group.length + 1;
        var template = cm_template;

        if( cm_count > 10 ) return false;
        template = template.replace(/\|\|/g, cm_count);

        $('#bp-main-modal-form').append(template);

        if( typeof value !== "undefined" && typeof hstype !== "undefined") {
            var last_cm = $('#bp-main-modal-form div.cm-group:last');
            last_cm.find('input[type="text"]').val(value);
            last_cm.find('select').val(hstype);
        }

        if(cm_count > 9) $(this).hide();
    }

    $('#btn-add-cm').on('click', function () {
        createCM();
    });

    $(document).on('click', '.btn-delete-cm', function(){
        var cm_item = $(this).parent().parent();
        var cm_number = cm_item.find('.cm-number').html();

        sWarning('Remove custome measure #'+cm_number+'?',function(){
            cm_item.remove();
            var cm_group = $('.cm-group');
            $.each(cm_group, function(i, e){
                var cnt = i+1;
                $(e).find('span.cm-number').html(cnt);
                $(e).find('input[type="text"]').attr('name', 'custom_measure['+cnt+'][name]');
                $(e).find('select').attr('name', 'custom_measure['+cnt+'][value]');
            });
            swal.close();
        });
    });
</script>
<!-- /bp main modal -->

<!-- sub bp modal -->
<div class="modal fade" data-backdrop="static" id="sub-bp-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Sub Business Process Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning modal-alert"></div>
                {!! Form::open(['method' => 'post', 'class' => 'form-horizontal', 'id' => 'sub-bp-modal-form']) !!}
                <input type="hidden" name="mbp_id" value="">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="main_bp_name" style="padding-top: 0px;"> Main Process Name </label>
                    <div class="col-sm-8 main_bp_name">
                        <span class="label label-info label-wrap"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="sp_seq"> Sequence </label>
                    <div class="col-sm-2">
                        <input type="number" id="sp_seq" name="sp_seq" class="form-control input-sm" step="1" min="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="sub_bp"> Sub Process </label>
                    <div class="col-sm-8">
                        <select id="sub_bp" name="sub_bp" class="form-control input-sm basic-info lookup" placeholder="Select a sub process..." required></select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="sub_bp_desc"> Sub Process Description </label>
                    <div class="col-sm-8" id="sub_bp_desc">
                        <span class="label label-info label-wrap"></span>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-sub-bp-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /sub bp modal -->

<!-- mainbp mandays modal -->
<div class="modal fade" data-backdrop="static" id="mainbp-mandays-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Main Business Process Mandays Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning modal-alert"></div>
                {!! Form::open(['method' => 'post', 'class' => 'form-horizontal', 'id' => 'mainbp-mandays-modal-form']) !!}
                <input type="hidden" name="_method" value="put">
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="main_bp_name" style="padding-top: 0px;"> Main Process Name </label>
                    <div class="col-sm-8 main_bp_name">
                        <span class="label label-info label-wrap"></span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="walk_thru_mandays"> Walk Thru </label>
                    <div class="col-sm-2">
                        <input type="number" id="walk_thru_mandays" name="walk_thru_mandays" class="form-control input-sm" step="1" min="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="planning_mandays"> Planning </label>
                    <div class="col-sm-2">
                        <input type="number" id="planning_mandays" name="planning_mandays" class="form-control input-sm" step="1" min="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="followup_mandays"> Follw up </label>
                    <div class="col-sm-2">
                        <input type="number" id="followup_mandays" name="followup_mandays" class="form-control input-sm" step="1" min="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="discussion_mandays"> Discussion </label>
                    <div class="col-sm-2">
                        <input type="number" id="discussion_mandays" name="discussion_mandays" class="form-control input-sm" step="1" min="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="draft_dar_mandays"> Draft DAR </label>
                    <div class="col-sm-2">
                        <input type="number" id="draft_dar_mandays" name="draft_dar_mandays" class="form-control input-sm" step="1" min="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="final_report_mandays"> Final Report </label>
                    <div class="col-sm-2">
                        <input type="number" id="final_report_mandays" name="final_report_mandays" class="form-control input-sm" step="1" min="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="wrap_up_mandays"> Wrap Up </label>
                    <div class="col-sm-2">
                        <input type="number" id="wrap_up_mandays" name="wrap_up_mandays" class="form-control input-sm" step="1" min="1">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label" for="reviewer_mandays"> Reviewer </label>
                    <div class="col-sm-2">
                        <input type="number" id="reviewer_mandays" name="reviewer_mandays" class="form-control input-sm" step="1" min="1">
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-mainbp-mandays-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /mainbp mandays modal -->

<!-- branch codes modal -->
<div class="modal fade" data-backdrop="static" id="branch-codes-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Auto Create Auditable Entities</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method' => 'post', 'url' => route('auditable_entities.business_process.store', ['auditable_ent_id' => $details->auditable_entity_id]), 'class' => 'form-horizontal']) !!}
                <input type="hidden" name="ae_id" value="{{ isset($details) ? $details->auditable_entity_id : '' }}">
                <input type="hidden" name="ae_name" value="{{ isset($details) ? $details->auditable_entity_name : '' }}">
                @if(!Request::is('auditable_entities/master/*'))
                    <input type="hidden" name="group_type" value="{{ isset($details) ? $details->group_type : '' }}">
                    <input type="hidden" name="business_type" value="{{ isset($details) ? $details->business_type : '' }}">
                @endif
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name"> Branches </label>
                    <div class="col-sm-10">
                        <select name="branch_code[]" class="form-control input-sm basic-info branch_code" placeholder="Select branches"></select>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-apply-to-all"><i class="fa fa-flash"></i> Apply To All</button>
            </div>
        </div>
    </div>
</div>
<!-- /branch codes modal -->