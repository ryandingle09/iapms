@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                User Form
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div id="user-alert" class="mrgB15"></div>
                                <form action="{{ isset($details) ? route('user.update', [ 'id' => $details->user_id ]) : route('user.store') }}" class="form-horizontal" id="user-form" method="POST">
                                    @if( isset($details) )
                                    <input type="hidden" name="_method" value="put">
                                    @endif
                                    <div class="form-row align-right">
                                        <button class="btn btn-primary btn-sm">
                                            <i class="ace-icon fa fa-save bigger-120"></i>
                                            Save
                                        </button>
                                        <a href="{{route('user.index')}}" class="btn btn-warning btn-sm">
                                            <i class="ace-icon fa fa-times bigger-120"></i>
                                            Cancel
                                        </a>
                                    </div>
                                    <hr>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="user_tag"> Get user from </label>
                                        <div class="col-sm-6">
                                            <select class="form-control input-sm user_tag" name="user_tag">
                                                @if(isset($details))
                                                <option value="{{ $details->user_tag }}" selected> {{ $details->user_tag }}</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group tag-ref" style="display: none;" id="tag-ref-Auditor">
                                        <label class="col-sm-2 control-label" for="auditor"> Auditor </label>
                                        <div class="col-sm-6 col-xs-10">
                                            <select name="auditor" class="form-control input-sm">
                                                @if(isset($details) && $details->user_tag == "Auditor")
                                                <option value="{{ $details->user_tag_ref_id }}" selected> {{ $details->full_name }}</option>
                                                @endif
                                            </select>
                                        </div> 
                                    </div>

                                    <div class="form-group tag-ref" style="display: none;" id="tag-ref-Auditee">
                                        <label class="col-sm-2 control-label" for="auditee"> Auditee </label>
                                        <div class="col-sm-6 col-xs-10">
                                            <select name="auditee" class="form-control input-sm">
                                                @if(isset($details) && $details->user_tag == "Auditee")
                                                <option value="{{ $details->user_tag_ref_id }}" selected> {{ $details->full_name }}</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="user_name"> Username </label>

                                        <div class="col-sm-6 col-xs-10">
                                            @if(isset($details))
                                            <input type="text" name="user_name" class="form-control input-sm makelabel" value="{{ $details->user_name }}">
                                            @else
                                            <input type="text" name="user_name" class="form-control input-sm" value="">
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="remarks"> Password </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <input type="password" name="password" class="form-control input-sm" value="">
                                            @if(isset($details))
                                            <span class="text-helper"> Just leave it a blank if you dont want to change the password. </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="display_name"> Display name </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control input-sm" name="display_name" value="{{ isset($details) ? $details->display_name : "" }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="description"> Description </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <textarea id="remarks" name="description" row="5" class="form-control input-sm noresize">{{ isset($details) ? $details->description : '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="effective_start_date effective_end_date"> Effective Date </label>

                                        <div class="col-sm-6">
                                            <div class="input-daterange input-group">
                                                <input type="text" id="effective_start_date" name="effective_start_date" class="form-control input-sm form-control" placeholder="Start Date" value="{{ isset($details) ? $details->effective_start_date : '' }}"/>
                                                <span class="input-group-addon">to</span>
                                                <input type="text" id="effective_end_date" name="effective_end_date" class="form-control input-sm form-control" placeholder="End Date" value="{{ isset($details) ? $details->effective_end_date : '' }}"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="description"> Application Roles </label>

                                        <div class="col-sm-6">
                                            @php
                                                $old = isset($details) ? $details->roles->pluck('id')->toArray() : [] ;

                                            @endphp
                                            @foreach($roles as $role)
                                                @if(in_array($role->id, $old))
                                                <input class="ace cb-permission" type="checkbox" name="roles[]" value="{{ $role->id}}" checked="checked">
                                                @else
                                                <input class="ace cb-permission" type="checkbox" name="roles[]" value="{{ $role->id}}" >
                                                @endif
                                                <span class="lbl"> {{ ucfirst( $role->name ) }}</span><br>
                                            @endforeach
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-row align-right">
                                        <button class="btn btn-primary btn-sm">
                                            <i class="ace-icon fa fa-save bigger-120"></i>
                                            Save
                                        </button>
                                        <a href="{{route('user.index')}}" class="btn btn-warning btn-sm">
                                            <i class="ace-icon fa fa-times bigger-120"></i>
                                            Cancel
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script type="text/javascript">
    var user_tag_select = $("#user-form [name='user_tag']").makeSelectize({
        lookup : '{{ config('iapms.lookups.user_tag') }}',
        onChange: function(value) {
            if (!value.length) return;
            $(".tag-ref").hide();
            $("#tag-ref-" + value).show();
        },
    })[0].selectize;

    var auditor_select = $("#user-form [name='auditor']").makeSelectize({
        url : "{{route('auditor.list.show')}}",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        },
        onChange: function(value) {
            if (!value.length) return;
            var option = this.options[ value ];
            $("[name='display_name']").val(option.text);
        }
    })[0].selectize;

    var auditee_select = $("#user-form [name='auditee']").makeSelectize({
        url : "{{ route('auditee.index')}}",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        },
        onChange: function(value) {
            if (!value.length) return;
            var option = this.options[ value ];
            $("[name='display_name']").val(option.text);
        }
    })[0].selectize;

    $('#user-form .input-daterange').datepicker({
        format: "M-dd-yyyy",
        // startDate: 'today',
        keyboardNavigation: false,
        autoclose : true,
    });

    $("#user-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response) {
                $.sSuccess(response.message,function(){
                    location.href = "{{ url('user/edit/')}}/" + response.data.user_id;
                });
            },
            error : function(xhr){
                $("#user-form").formErrorHandler(xhr);
            }
        });
        return false;
    });

    @if(isset($details))
    $("#tag-ref-{{ $details->user_tag }}").show();
    $(".makelabel").makeLabel();
    @endif
</script>
@endsection