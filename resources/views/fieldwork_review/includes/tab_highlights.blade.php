<div class="col-xs-12 mrgB15">
    <div class="table-header"> Fieldwork Audit Highlights </div>
</div>
<div class="col-xs-12 mrgB15">
    <table id="highlights-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Sequence</th>
                <th >Findings</th>
                <th >Action Taken</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@include('fieldwork_review.includes.create_highlights_modal')
@section('tab_highlights_scripts')
<script type="text/javascript">
    // selectedScope coming from fieldwork_review.index;

    var highlights_datatable = $("#highlights-table").DataTable( {
        "bDestroy": true,
        "processing": true,
        order: [[ 0, 'asc' ]],
        columns: [
            {data: "highlights_seq"},
            {data: "findings"},
            {data: "auditee_actions_taken"}
        ]
    });

    $(document).on('click',"#main_tab a[href='#highlights']",function(){
        if(selectedScope){
            var url = "{{ url('project-scope')}}/"+selectedScope + "/highlight";
            $("#create-highlights-form").attr('action', url);
            highlights_datatable.ajax.url( url + "/list" ).load();
        }else{
            $.sInfo("You have to select project scope to open this tab.");
        }
    });

</script>
@yield('create_highlights_modal_scripts')
@endsection