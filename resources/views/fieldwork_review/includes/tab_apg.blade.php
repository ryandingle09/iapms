<div class="col-xs-12 align-left" id="main-apg-wrapper">
    <div class="apg-wrapper-header"> Audit Program Guide </div>
    <div class="apg-wrapper scrollable" id="apg-wrapper" data-groupBy="bp_name" data-keyword="">
        
    </div>
</div>

@include('fieldwork_review.includes.create_apg_findings_modal')
@section('tab_apg_scripts')
<script type="text/javascript">
    // selectedScope variables can coming tab_scope.blade.php
    var selectedApg = 0;
    $(document).on('click',"#main_tab a[href='#apg']",function(){
        if(selectedScope){
            $.loadFullView('fw_enabled_apg');  
        }else{
            $.sInfo("You have to select project scope to open this tab.");
        }
    });

    $.loadFullView = function(type){
        $("#main-apg-wrapper").LoadingOverlay("show");
        $.ajax({
            url : "{{ url('project-scope') }}/" + selectedScope + "/apg/full-view",
            method : "GET",
            data : { type : type },
            success : function(response){
                var html = '<div class="alert alert-info"> <strong> <i class="ace-icon fa fa-info"></i> </strong> No APG Found. <br> </div>';
                if(response.data.length){
                    html = response.html;
                }
                $("#apg-wrapper").html(html);    
                $("#main-apg-wrapper").LoadingOverlay("hide");
            }
        });
    };

    $(document).on('click',".apg-table .fieldwork",function(){
        $('.alert').remove();
        var url = $(this).attr('href');
        $("#create-apg-findings-form")[0].reset();
        // $('#create-apg-findings-form input:checkbox').removeAttr('checked');
        $.ajax({
            url : url,
            method : "GET",
            beforeSend : sLoading(),
            success : function(response){
                swal.close();
                if(response.data){
                    $("#create-apg-findings-form").supply(response.data);
                }
                $("#create-apg-findings-form").attr('action',url);
                $("#create-apg-findings-modal").modal('show');
            }
        }); 
        return false;
    });

</script>
@yield('create_apg_findings_modal_scripts')
@endsection