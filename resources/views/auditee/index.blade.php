@extends('layouts.app')
@section('styles')
    <style type="text/css">
        /*.dataTables_filter { display: none !important; }*/
    </style>
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{route('auditee.create')}}">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Auditees
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="auditee-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Auditable Entity IDL</th>
                                <th>Position</th>
                                <th>Email Address</th>
                                <th>Immediate Head</th>
                                <th>Effective Start</th>
                                <th>Effective End</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var datatable= $('#auditee-table').DataTable( {
        ajax: "{{route('auditee.list')}}",
        "processing": true,
        orderCellsTop: true,
        columns: [
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    return data.first_name+' '+data.middle_name+' '+data.last_name;
                }
            },
            { data: "auditable_entity.auditable_entity_name", defaultContent : 'N/A' },
            { data: "lv_position_code_desc" },
            { data: "email_address" },
            { data: "supervisor", "render": function ( data, type, full, meta ) {
                    return data ? data.first_name+' '+data.last_name : 'N/A';
                }
            },
            { data: "effective_start_date" },
            { data: "effective_end_date", defaultContent : 'N/A' },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('auditee/edit')}}/'+data.auditee_id+'"><i class="fa fa-pencil edit" title="edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.auditee_id+'" data-name="'+data.full_name+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    $( '#full_name, #company_code' ).on( 'keyup change', function () {
        var index = $(this).attr('data-index');
        if ( datatable.column(index).search() !== $(this).val() ) {
            datatable
            .column( index )
            .search( $(this).val() )
            .draw();
        }
    } );

    // remove auditee
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var remove_name = $(this).attr('data-name');
        var ans = confirm('Are you sure you want to remove '+remove_name+'?');

        if( ans ) {
            $.ajax({
                url: '{{ url('auditee/delete') }}/'+remove_id,
                method: "POST",
                data: { _token : _token, _method : 'delete' }, // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        location.reload();
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    // displays the validation error
                    var msg = '<ul class="list-unstyled">';

                    $.each(xhr.responseJSON, function(key, val){
                        for(var i=0; i < val.length; i++) {
                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                        }
                    });
                    msg += '</ul>';

                    $('.alert-warning').html(msg).show();
                }
            });
        }

        return false;
    });
</script>
@endsection
