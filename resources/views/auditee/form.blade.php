@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.bootstrap3.css" />
    <link rel="stylesheet" href="/css/selectize-custom.css" />
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Auditee Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div style="display:none;" class="alert alert-warning"></div>
                                {!! Form::open(['url' => isset($details) ? route('auditee.update', ['id' => $details->auditee_id]) : route('auditee.store'), 'class' => 'form-horizontal', 'id' => 'auditee-form']) !!}
                                    @if( isset($details) )
                                    <input type="hidden" name="_method" value="put">
                                    <input type="hidden" name="id" value="{{$details->auditee_id}}">
                                    @endif
                                    <div class="form-group">
                                        <div class="pull-right btn-group">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button>
                                            <a href="{{route('auditee')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="full_name"> Full name </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <input type="text" class="form-control input-sm basic-info" id="first_name" name="first_name" value="{{ isset($details) ? trim($details->first_name) : '' }}" placeholder="first name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="full_name"> &nbsp; </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <input type="text" class="form-control input-sm basic-info" id="middle_name" name="middle_name" value="{{ isset($details) ? trim($details->middle_name) : '' }}" placeholder="middle name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="full_name"> &nbsp; </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <input type="text" class="form-control input-sm basic-info" id="last_name" name="last_name" value="{{ isset($details) ? trim($details->last_name) : '' }}" placeholder="last name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="employee_id"> Employee ID </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <input type="number" name="employee_id" id="employee_id" value="{{isset($details) ? $details->employee_id : ''}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="email_address"> Email Address </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <input type="email" id="email_address" name="email_address" class="form-control input-sm basic-info" value="{{ isset($details) ? $details->email_address : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="auditable_entity"> Auditable Entity </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <select id="auditable_entity" name="auditable_entity" class="form-control input-sm basic-info lookup" placeholder="Select a entity">
                                                <option value=""></option>
                                                @if( isset($details) && !is_null($details->auditableEntity) )
                                                <option value="{{$details->auditable_entity_id}}" selected="selected">{{$details->auditableEntity->auditable_entity_name}}</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="position"> Position </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <select id="position" name="position" class="form-control input-sm basic-info" placeholder="Select a position">
                                                @if( isset($details) )
                                                <option value="{{$details->position_code}}" selected="selected">{{$details->lv_position_code_desc}}</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="supervisor"> Immediate Head </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <select id="supervisor" name="supervisor" class="form-control input-sm basic-info" placeholder="Select a Immediate Head">
                                                @if( isset($details) )
                                                    @if( !is_null($details->supervisor) )
                                                    <option value="{{$details->supervisor_id}}" selected="selected">{{$details->supervisor->first_name.' '.$details->supervisor->middle_name.' '.$details->supervisor->last_name}}</option>
                                                    @endif
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="department_code"> Department </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <select id="department_code" name="department_code" class="form-control input-sm basic-info" placeholder="Select a department"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="effectivity"> Effectivity Date </label>

                                        <div class="col-sm-3 col-xs-10">
                                            <input type="text" class="form-control input-sm date-picker basic-info" id="start_date" name="start_date" placeholder="Start Date" data-date-format="dd-MM-yyyy" value="{{ isset($details) ? date('d-F-Y', strtotime($details->effective_start_date)) : date('d-F-Y') }}">
                                        </div>

                                        <div class="col-sm-3 col-xs-10">
                                            <input type="text" class="form-control input-sm date-picker basic-info" id="end_date" name="end_date" placeholder="End Date" data-date-format="dd-MM-yyyy" value="{{ isset($details) ? ($details->effective_end_date != '' ? date('d-F-Y', strtotime($details->effective_end_date)) : '') : '' }}">
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                        <div class="pull-right btn-group">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button>
                                            <a href="{{route('auditee')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
    <script src="/js/iapms.js"></script>
    <script src="/js/date-time/bootstrap-datepicker.js"></script>
    <script src="/plugins/selectize/selectize.js"></script>

    <script type="text/javascript">
        var form        = $('#auditee-form'),
            auditable_entity = $('#auditable_entity'),
            position    = $('#position'),
            supervisor  = $('#supervisor'),
            department_code  = $('#department_code'),
            $alert       = $('.alert');

        var select_entity, $select_entity;
        var select_position, $select_position;
        var select_sup, $select_sup;
        var select_department_code, $select_department_code;
        var lookup_url = '{{url('/administrator/lookup/type')}}';

        $(function(){
            // selectize
            $select_entity = auditable_entity.selectize({
                valueField: 'auditable_entity_id',
                labelField: 'auditable_entity_name',
                searchField: ['auditable_entity_name'],
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function (item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.auditable_entity_name) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function (query, callback) {
                    $.ajax({
                        url: '{{ route('auditable_entities.lists') }}?type=actual&search=' + encodeURIComponent(query) + '&searchFields=auditable_entity_name:like&filter=auditable_entity_id;auditable_entity_name&is_parent=true',
                        type: 'GET',
                        success: function (res) {
                            callback(res);
                        },
                        error: function () {
                            callback();
                        }
                    });
                }
            });
            select_entity = $select_entity[0].selectize;

            $select_position = position.selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function (query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.auditee.position') }}?search='+encodeURIComponent(query)+'&searchFields=meaning:like',
                        type: 'GET',
                        success: function (res) {
                            callback(res.data);
                        },
                        error: function () {
                            callback();
                        }
                    });
                }
            });
            select_position = $select_position[0].selectize;

            $select_sup = supervisor.selectize({
                valueField: 'id',
                labelField: 'text',
                searchField: ['text'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.text) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function (query, callback) {
                    $.ajax({
                        url: '{{route('auditee.show')}}?except={{ isset($details) ? $details->first_name.','.$details->middle_name.','.$details->last_name : '' }}&company={{ isset($details) ? $details->auditableEntity->company_code : '' }}',
                        type: 'GET',
                        success: function (res) {
                            callback(res.data);
                        },
                        error: function () {
                            callback();
                        }
                    });
                }
            });
            select_sup = $select_sup[0].selectize;

            $select_department_code = department_code.selectize({
                valueField: 'meaning',
                labelField: 'meaning',
                searchField: ['meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="department_code">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.dept_code') }}',
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(res) {
                            callback(res.data);
                        }
                    });
                },
                onLoad: function() {
                    @if(isset($details))
                    select_department_code.setValue('{{$details->department_code}}');
                    @endif
                }
            });
            select_department_code = $select_department_code[0].selectize;
            // end selectize
        });

        // stops the page from reloading/closing if the form is edited
        $('input').change(function() {
            if( $(this).val() != "" ) {
                window.onbeforeunload = function() {
                    return 'Are you sure you want to navigate away from this page?';
                };
            }
        });

        // save the form through AJAX call
        function save() {
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        window.onbeforeunload = null;
                        window.location = '{{ route('auditee') }}';
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    $('.basic-info').parent().parent().removeClass('has-error');
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        }
    </script>
@endsection