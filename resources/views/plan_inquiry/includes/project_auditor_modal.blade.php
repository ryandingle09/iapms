<!-- Create Projects Auditor Modal -->
<div class="modal fade " data-backdrop="static" id="create-project-auditor-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Project Auditor</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-project-auditor-alert"></div>
                        <form class="form-horizontal" id="create-project-auditor-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="auditor"> Auditor </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm auditor" name="auditor"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="auditor_type"> Auditor Type </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm auditor_type" name="auditor_type"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="allotted_mandays"> Allotted Mandays </label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control input-sm allotted_mandays" name="allotted_mandays">
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Projects Auditor Modal -->
@section('project_auditor_modal_scripts')
<script type="text/javascript">
    // project_auditor_datatable came form tab_projects

    var pam_auditor_type_select = $("#create-project-auditor-form .auditor_type").makeSelectize({
        lookup : '{{ config('iapms.lookups.auditor.type') }}'
    })[0].selectize;

    var pam_auditor_select = $("#create-project-auditor-form .auditor").makeSelectize({
        url: "{{ url('auditor/show')}}",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    })[0].selectize;

    $("#create-project-auditor-form").submit(function(){
        $(".alerts").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#create-project-auditor-form")[0].reset();
                $("#create-project-auditor-modal").modal('hide');
                pam_auditor_type_select.clear();
                pam_auditor_select.clear();
                project_auditor_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#create-project-auditor-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });
</script>
@endsection