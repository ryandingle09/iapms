<div class="col-xs-12">
    <form method="GET" action="" class="form-horizontal" id="plan-search">
        <div class="form-group">
            <div class="col-sm-5">
                <div class="input-group">
                    <input type="number"  name="search_year" class="form-control" placeholder="Please type year here (ex. 2017 or 2018)" value="{{ $search_year }}">
                    <span class="input-group-btn">
                        <button class="btn btn-primary">
                            <i class="ace-icon fa fa-search bigger-110"></i>
                            Go!
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="col-xs-12 mrgB15" id="plan-details">
    <div class="table-header mrgB15">
        Annual Audit Plan {{ isset($plan) ? 'for '.$plan->plan_year : '' }}
    </div>
    @if(isset($plan))
    <div id="edit-plan-alert"></div>
    <form class="form-horizontal" id="edit-plan-form">
        <div class="form-group" >
            <label class="col-sm-2 control-label align-left " for="plan_year"> Annual Audit Year </label>
            <div class="col-sm-4">
                <input type="text" name="plan_year" class="form-control input-sm makelabel" value="{{ $plan->plan_year }}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="plan_name"> Plan Name </label>
            <div class="col-sm-4">
                <input type="text" name="plan_name" class="form-control input-sm makelabel" value="{{ $plan->plan_name }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="status"> Status </label>
            <div class="col-sm-4">
                <input type="text" name="status" class="form-control input-sm makelabel" value="{{ $plan->plan_status }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="remarks"> Remarks </label>
            <div class="col-sm-10">
                <textarea name="remarks" class="form-control makelabel" rows="4">{{ $plan->remarks }}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="approver_fullname"> Approved By </label>
            <div class="col-sm-4">
                <input type="text" name="approver_fullname" class="form-control input-sm makelabel" value="{{ isset($plan->end_approval) ? $plan->end_approval->approver->full_name  : 'n/a' }}">
            </div>
            <label class="col-sm-2 control-label align-left" for="approved_date"> Approved Date </label>
            <div class="col-sm-4">
                <input type="text" name="approved_date" class="form-control input-sm makelabel" value="{{ $plan->end_approval  ? $plan->end_approval->approval_date : 'n/a' }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="status"> Auditor's Allotted Mandays </label>
            <div class="col-sm-4">
                <input type="text" name="status" class="form-control input-sm makelabel" value="{{ $plan->allotted_mandays }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="status"> Auditor's Annual Mandays </label>
            <div class="col-sm-4">
                <input type="text" name="status" class="form-control input-sm makelabel" value="{{ $plan->annual_mandays ? : 'n/a' }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="status"> Auditor's Available Mandays </label>
            <div class="col-sm-4">
                <input type="text" name="status" class="form-control input-sm makelabel" value="{{ $plan->available_mandays ? : 'n/a' }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="approver_remarks"> Approver Remarks </label>
            <div class="col-sm-10">
                <textarea name="approver_remarks" class="form-control makelabel" rows="4" readonly="">{{ $plan->end_approval  ? $plan->end_approval->remarks : 'n/a' }}</textarea>
            </div>
        </div>
        
    </form>
    @else
        @if(strlen($search_year))
            <div id="create-plan-alert">
                <div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <strong> <i class="ace-icon fa fa-warning"></i> </strong> Plan doesn't exist. Please search other year.
                    <br>
                </div>
            </div>
        @else
        <div class="alert alert-info">
             <strong> <i class="ace-icon fa fa-info"></i> </strong> Please type the year above to search or create an annual plan.
            <br>
        </div>
        @endif
    @endif
</div>
@if($plan)
<div class="col-xs-12 mrgB15" id="plan-details">
    <div class="table-header">
        Annual Audit Plan - Auditors
    </div>
</div>
<div class="col-xs-12 mrgB15">
    <table id="plan-auditor-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Auditor Name</th>
                <th >Allotted Mandays</th>
                <th >Annual Mandays</th>
                <th >Running Mandays</th>
                <th >Available Mandays</th>
                <th >Action</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@include('plan.includes.plan_auditor_modal')
@endif
@section('tab_plan_scripts')
<script type="text/javascript">
    plan_auditor_datatable = $('#plan-auditor-table').DataTable( {
        "bDestroy": true,
        ajax: "{{ url('plan')}}/" + selectedPlan +"/auditor/list",
        "processing": true,
        order: [[ 1, 'asc' ]],
        columns: [
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    return data.auditor.first_name+' '+data.auditor.middle_name+' '+data.auditor.last_name;
                }
            },
            { data: "allotted_mandays" },
            { data: "annual_mandays" },
            { data: "running_mandays" },
            { data: "available_mandays" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="proficiency" data-id="'+data.auditor_id+'"><i class="fa fa-user text-primary" title="Proficiency" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });

    $(document).on("click","#plan-auditor-table .delete",function(){
        $(this).deleteItemFrom(plan_auditor_datatable);
        return false;
    });

    $("#edit-plan-form .makelabel").makeLabel();

    $("#edit-plan-form").submit(function(){
        $.ajax({
            url : $(this).attr('action'),
            method : "PUT",
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                swal.close();
                $("#edit-plan-alert").aSuccess(response.message);
            }
        }); 
        return false;
    });

</script>
@yield('plan_auditor_modal_scripts')
@endsection