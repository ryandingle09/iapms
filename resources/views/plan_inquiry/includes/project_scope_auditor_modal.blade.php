<!-- Create Projects Scopes Auditor Modal -->
<div class="modal fade " data-backdrop="static" id="create-project-scope-auditor-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Project Scope Auditor</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-project-scope-auditor-alert"></div>
                        <form class="form-horizontal" id="create-project-scope-auditor-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="auditor"> Auditor </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm" name="auditor"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="auditor_type"> Auditor Type </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm" name="auditor_type"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="allotted_mandays"> Allotted Mandays </label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control input-sm" name="allotted_mandays">
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Projects Scopes Auditor Modal -->
@section('project_scope_auditor_modal_scripts')
<script type="text/javascript">
    // project_scope_datatable came from tab_scope

    var psam_auditor_type_select = $("#create-project-scope-auditor-form [name=auditor_type]").makeSelectize({
        lookup : '{{ config('iapms.lookups.auditor.type') }}'
    })[0].selectize;

    var psam_auditor_select = $("#create-project-scope-auditor-form [name=auditor]").makeSelectize({
        url: "{{ url('auditor/show')}}",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    })[0].selectize;

    $("#create-project-scope-auditor-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#create-project-scope-auditor-form")[0].reset();
                $("#create-project-scope-auditor-modal").modal('hide');
                psam_auditor_type_select.clear();
                psam_auditor_select.clear();
                project_scope_auditor_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#create-project-scope-auditor-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    })
</script>
@endsection