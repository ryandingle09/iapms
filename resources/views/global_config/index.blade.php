@extends('layouts.app')
@section('styles')
<link rel="stylesheet" href="/css/jquery-ui.css" />
<script src="/plugins/tinymce/tinymce.min.js"></script>
<script src="/plugins/tinymce/jquery.tinymce.min.js"></script>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')
        <div class="page-content">
            <div class="alert-wrapper">
            @include('includes.content_header')
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Global Configuration
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main clearfix">
                                @include('includes.alerts')
                                <form class="form-horizontal col-md-12" id="form-config">
                                    <input type="hidden" name="_method" value="put">
                                    {{ csrf_field() }}
                                    <div class="form-group align-right">
                                        <button class="btn btn-primary btn-sm">
                                            <i class="ace-icon fa fa-save bigger-120"></i> Save
                                        </button>
                                        <a href="" class="btn btn-warning btn-sm">
                                            <i class="ace-icon fa fa-times bigger-120"></i>
                                            Cancel
                                        </a>
                                    </div>
                                    <div id="tabs">
                                        <ul>
                                            <li>
                                                <a href="#tabs-1">Inter Office Memo</a>
                                            </li>
                                            <li>
                                                <a href="#tabs-2">Auditor Grading Factor </a>
                                            </li>
                                            <li>
                                                <a href="#tabs-3">Reviewer Grading Factor </a>
                                            </li>
                                            <li>
                                                <a href="#tabs-4">Others</a>
                                            </li>
                                        </ul>
                                        <div id="tabs-1" >
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="iom_subject"> Subject </label>
                                                <div class="col-sm-10">
                                                    <input type="text" class="form-control input-sm" name="iom_subject" id="iom_subject" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="iom_introduction"> Introduction </label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control input-sm" name="iom_introduction" id="iom_introduction" ></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="iom_background"> Background </label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control input-sm" name="iom_background" id="iom_background" ></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="iom_objectives"> Objective </label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control input-sm" name="iom_objectives" id="iom_objectives" ></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="iom_auditee_assistance"> Auditee Assistance </label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control input-sm" name="iom_auditee_assistance" id="iom_auditee_assistance" ></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="iom_staffing"> Staffing </label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control input-sm" name="iom_staffing" id="iom_staffing" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="tabs-2" class="cal1" >
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="auditor_timeliness"> Timeliness % </label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control input-sm" name="auditor_timeliness" id="auditor_timeliness" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="auditor_quality"> Quality % </label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control input-sm" name="auditor_quality" id="auditor_quality" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="auditor_documentation"> Documentation % </label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control input-sm" name="auditor_documentation" id="auditor_documentation" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="auditor_caf"> CAF % </label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control input-sm" name="auditor_caf" id="auditor_caf" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="value"> Total </label>
                                                <div class="col-sm-5">
                                                    <span class="label label-xlg label-primary arrowed total" id="auditor_total"> 100 % </span> </div>
                                            </div>
                                        </div>
                                        <div id="tabs-3" class="cal2" >
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="reviewer_timeliness"> Timeliness % </label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control input-sm" name="reviewer_timeliness" id="reviewer_timeliness" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="reviewer_quality"> Quality % </label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control input-sm" name="reviewer_quality" id="reviewer_quality" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="reviewer_documentation"> Documentation % </label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control input-sm" name="reviewer_documentation" id="reviewer_documentation" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="value"> Total </label>
                                                <div class="col-sm-5">
                                                    <span class="label label-xlg label-primary arrowed total" id="auditor_total"> 100 % </span> </div>
                                            </div>
                                        </div>
                                        <div id="tabs-4" >
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="auditor_mandays_factor_rate"> Auditor Mandays Factor Rate </label>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control input-sm" name="auditor_mandays_factor_rate" id="auditor_mandays_factor_rate" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-group align-right">
                                        <button class="btn btn-primary btn-sm">
                                            <i class="ace-icon fa fa-save bigger-120"></i> Save
                                        </button>
                                        <a href="" class="btn btn-warning btn-sm">
                                            <i class="ace-icon fa fa-times bigger-120"></i>
                                            Cancel
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/js/jquery-ui.js"></script>
<script type="text/javascript">
    $( "#tabs" ).tabs();

    $.ajax({
        url : "{{ route('global_config.show',[ 'id' => 0 ]) }}",
        method : "GET",
        data : { _token : _token },
        beforeSend : sLoading(),
        success : function(response){
            swal.close();
            $("#form-config").supply(response.data);
            calculatePercentage($(".cal1"));
            calculatePercentage($(".cal2"));
        }
    });

    $("#form-config").submit(function(){
        $(".alert").remove();
        $(".has-error").removeClass('has-error');
        $.ajax({
            url : "{{ route('global_config.save') }}",
            method : "PUT",
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                swal.close();
                $(".alert-wrapper").showSuccess(response.message);
                $("body").scroll();
            }
        });
        return false;
    });

    $(document).on('keyup',".cal1 input",function(){
        calculatePercentage($(".cal1"));
    });

    $(document).on('keyup',".cal2 input",function(){
        calculatePercentage($(".cal2"));
    });

    function calculatePercentage(elem){
        var total = 0;
        $(elem).find('input').each(function(){
            total = total + parseInt($(this).val()) ;
        });
        if(isNaN(total)){
            $(elem).find('.total').html("Invalid");
        }else{
            $(elem).find('.total').html(total + " %");
        }

    }

    tinymce.init({
        path_absolute : path_absolute,
        selector: '.tinymce-editor',
        height: 300,
        menubar: false,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = path_absolute + 'filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    });
</script>
@endsection
