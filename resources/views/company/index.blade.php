@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.bootstrap3.css" />
    <link rel="stylesheet" href="/css/selectize-custom.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header', array('heading' => 'Company Profile'))

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="#companyModal" data-toggle="modal">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Companies
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="company-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Company Name</th>
                                <th>Company Code</th>
                                <th>Description</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<!-- Modal -->
<div class="modal fade" id="companyModal" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Company Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning alert-modal"></div>
                {!! Form::open(['class' => 'form-horizontal', 'id' => 'company-form']) !!}
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="id" value="">
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="company_code"> Code </label>

                        <div class="col-xs-10">
                            <select id="company_code" name="company_code" class="form-control input-sm basic-info lookup" placeholder="Select a company..."></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="company_name"> Name </label>

                        <div class="col-xs-10">
                            <span id="company_name" class="label label-info label-wrap"></span>
                            <input type="hidden" name="company_name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label" for="description"> Description </label>

                        <div class="col-xs-10">
                            <span id="description" class="label label-info label-wrap"></span>
                            <input type="hidden" name="description">
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_script')
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/selectize/selectize.js"></script>
<script src="/js/iapms.js"></script>
<script type="text/javascript">
    var $modal  = $('#companyModal'),
        form    = $('#company-form'),
        company    = $('#company_code'),
        $alert  = $('.alert-modal');
    var select_company, $select_company;
    var lookup_url = '{{url('/administrator/lookup/type')}}';
    var $datatable= $('#company-table').DataTable( {
        ajax: "{{route('company.list')}}",
        "processing": true,
        "serverSide": true,
        orderCellsTop: true,
        columns: [
            { data: "company_name" },
            { data: "company_code" },
            { data: "description", orderable: false, defaultContent: 'N/A' },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('company_profile/details')}}/'+data.company_code+'"><i class="fa fa-file-text details" title="Details" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.company_id+'" data-name="'+data.company_name+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    $(function(){
        $select_company = company.selectize({
            valueField: 'id',
            labelField: 'text',
            searchField: ['text'],
            options: [],
            preload : true,
            create: false,
            render: {
                option: function(item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.text) + '</span>' +
                        '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            load: function(query, callback) {
                $.ajax({
                    url: lookup_url+'/{{ config('iapms.lookups.company') }}',
                    type: 'GET',
                    success: function(res) {
                        callback(res.data);
                    },
                    error: function() {
                        callback();
                    }
                });
            },
            onChange: function(value) {
                if (!value.length) return;
                var meaning = select_company.options[value].meaning;
                var desc = select_company.options[value].description;
                $('#company_name').html(meaning);
                $('#description').html(desc);

                $('input[name="company_name"]').val(meaning)
                $('input[name="description"]').val(desc)
            }
        });
        select_company = $select_company[0].selectize;
    });

    function init_modal() {
        $modal.find('input[name="_method"]').val('post');
        $modal.find('input[name="id"]').val('');
        $modal.find('#company_name').html('');
        $modal.find('#description').html('');
        select_company.clear();
        $('input[name="company_name"]').val('')
        $('input[name="description"]').val('')
        $modal.find('input.basic-info').val('');
        $modal.find('.basic-info').parent().parent().removeClass('has-error');
        $alert.html('').hide();
    }

    $modal.on('hidden.bs.modal', function(){
        init_modal();
    });

    // save company via Ajax
    $(document).on('click', '.btn-save', function() {
        swal({
            title: "Continue saving?",
            text: "You are about to add a new company.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, save it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(){
            $.ajax({
                url: form.attr('action'),
                method: "POST",
                data: form.serialize(),
                success : function(data) {
                    $modal.modal('hide');
                    swal("Saved!", "New company is successfully created.", "success");
                    window.location = data.redirect;
                },
                error: function(xhr, textStatus, errorThrown){
                    // displays the validation error
                    var unique_message = [];
                    var msg = '<ul class="list-unstyled">';

                    $('.basic-info').parent().parent().removeClass('has-error');

                    $.each(xhr.responseJSON, function(key, val){
                        for(var i=0; i < val.length; i++) {
                            // set error class to fields
                            $('#'+key).parent().parent().addClass('has-error');

                            // shows error message
                            if( unique_message.indexOf(val[i]) === -1 ) {
                                msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                unique_message.push(val[i]);
                            }
                        }
                    });
                    msg += '</ul>';

                    $alert.removeClass('alert-danger').addClass('alert-warning');
                    $alert.html(msg).show();
                    swal.close();
                }
            });
        });
    });

    // remove auditee
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var remove_name = $(this).attr('data-name');

        swal({
            title: "Continue?",
            text: 'You are about to remove "'+remove_name+'" company.',
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(){
            $.ajax({
                url: '{{ url('company_profile/delete') }}/'+remove_id,
                method: "POST",
                data: { _token : _token, _method : 'delete' },
                success : function(data) {
                    if( data.success ) {
                        swal("Deleted!", remove_name+" is successfully deleted.", "success");
                        $datatable.ajax.reload( null, false );
                    }
                }
            });
        });

        return false;
    });
</script>
@endsection
