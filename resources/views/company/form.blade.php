@extends('layouts.app')
@section('styles')
    <style type="text/css">
        .tab-content {
            min-height: 409px;
        }
    </style>
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />

    <script src="/plugins/tinymce/tinymce.min.js"></script>
    <script src="/plugins/tinymce/jquery.tinymce.min.js"></script>
    <link rel="stylesheet" href="/plugins/selectize/selectize.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.bootstrap3.css" />
    <link rel="stylesheet" href="/css/selectize-custom.css" />
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="table-header">
                        {{ $details->company_code }} | {{ ucwords($details->company_name) }}
                    </div>
                    <div class="tabbable tabs-left">
                        <ul class="nav nav-tabs" id="company_profile_tab" style="width: 220px;">
                            <li {!! Request::get('tab') == 'basic' || Request::get('tab') == ''  ? 'class="active"' : '' !!}>
                                <a href="?tab=basic">
                                    <i class="green ace-icon fa fa-certificate bigger-110"></i>
                                    Company Details
                                </a>
                            </li>
                            <li {!! Request::get('tab') == 'business'  ? 'class="active"' : '' !!}>
                                <a href="?tab=business">
                                    <i class="green ace-icon fa fa-certificate bigger-110"></i>
                                    Company Business
                                </a>
                            </li>

                            <li {!! Request::get('tab') == 'industry' ? 'class="active"' : '' !!}>
                                <a href="?tab=industry">
                                    <i class="green ace-icon fa fa-certificate bigger-110"></i>
                                    Company Industry
                                </a>
                            </li>

                            <li {!! Request::get('tab') == 'business_operation' ? 'class="active"' : '' !!}>
                                <a href="?tab=business_operation">
                                    <i class="green ace-icon fa fa-certificate bigger-110"></i>
                                    Line of Business and Operation
                                </a>
                            </li>

                            <li {!! Request::get('tab') == 'legal' ? 'class="active"' : '' !!}>
                                <a href="?tab=legal">
                                    <i class="green ace-icon fa fa-certificate bigger-110"></i>
                                    Legal &amp; Regulatory Framework
                                </a>
                            </li>

                            <li {!! Request::get('tab') == 'ownership' ? 'class="active"' : '' !!}>
                                <a href="?tab=ownership">
                                    <i class="green ace-icon fa fa-certificate bigger-110"></i>
                                    Ownership, Governance and Organizational Structure
                                </a>
                            </li>

                            <li {!! Request::get('tab') == 'financing' ? 'class="active"' : '' !!}>
                                <a href="?tab=financing">
                                    <i class="green ace-icon fa fa-certificate bigger-110"></i>
                                    Sources of Financing
                                </a>
                            </li>

                            <li {!! Request::get('tab') == 'investment' ? 'class="active"' : '' !!}>
                                <a href="?tab=investment">
                                    <i class="green ace-icon fa fa-certificate bigger-110"></i>
                                    Nature of Investment
                                </a>
                            </li>

                            <li {!! Request::get('tab') == 'objectives' ? 'class="active"' : '' !!}>
                                <a href="?tab=objectives">
                                    <i class="green ace-icon fa fa-certificate bigger-110"></i>
                                    Objectives and Strategies
                                </a>
                            </li>

                            <li {!! Request::get('tab') == 'performance_measure' ? 'class="active"' : '' !!}>
                                <a href="?tab=performance_measure">
                                    <i class="green ace-icon fa fa-certificate bigger-110"></i>
                                    Understand Relevant Performance Measures
                                </a>
                            </li>

                            <li {!! Request::get('tab') == 'audit_info' ? 'class="active"' : '' !!}>
                                <a href="?tab=audit_info">
                                    <i class="green ace-icon fa fa-certificate bigger-110"></i>
                                    Audit Information
                                </a>
                            </li>

                            <li {!! Request::get('tab') == 'branches' ? 'class="active"' : '' !!}>
                                <a href="?tab=branches">
                                    <i class="green ace-icon fa fa-certificate bigger-110"></i>
                                    Branches
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div id="details" class="tab-pane in active">
                                {{-- Check if tab query exists --}}
                                @if( Request::get('tab') != '' )
                                    @include('company.details.'.Request::get('tab'), ['details' => $details])
                                @else
                                    {{-- Default details sub-view --}}
                                    @include('company.details.basic', ['details' => $details])
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script src="/plugins/selectize/selectize.js"></script>
<script src="/js/date-time/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    var $alert  = $('.alert-modal'),
        form    = $('.update-form');
    var path_absolute = '/';

    tinymce.init({
        path_absolute : path_absolute,
        selector: '.tinymce-editor',
        height: 300,
        menubar: false,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | paste",
        paste_data_images: true,
        relative_urls: false,
        file_browser_callback : function(field_name, url, type, win) {
            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

            var cmsURL = path_absolute + 'filemanager?field_name=' + field_name;
            if (type == 'image') {
                cmsURL = cmsURL + "&type=Images";
            } else {
                cmsURL = cmsURL + "&type=Files";
            }

            tinyMCE.activeEditor.windowManager.open({
                file : cmsURL,
                title : 'Filemanager',
                width : x * 0.8,
                height : y * 0.8,
                resizable : "yes",
                close_previous : "no"
            });
        }
    });

    // save company via Ajax
    form.on('submit', function(e) {
        e.preventDefault();
        swal({
            title: "Continue saving?",
            text: "You are about to update company details.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, save it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(){
            $.ajax({
                url: form.attr('action'),
                method: "POST",
                data: form.serialize()+'&_method=put',
                success : function(data) {
                    swal("Saved!", "Company details is successfully udpated.", "success");
                }
            });
        });
    });
</script>
@yield('details_script')
@endsection