<div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#branch-modal">
                    <i class="fa fa-plus-square"></i> Add
                </button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-chevron-circle-left"></i> Back</a>
            </div>
        </div>
</div>
<div class="widget-box transparent">
    <div class="widget-header">
        <h4 class="widget-title lighter">Branches</h4>
    </div>

    <div class="widget-body" style="display: block; padding-top: 10px;">
        <table id="branch-table" class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th>Branch Code</th>
                <th>Description</th>
                <th>Active</th>
                <th>Store Size</th>
                <th>Address</th>
                <th>Zone</th>
                <th>Store Class</th>
                <th>Branch Class</th>
                <th>Entity Class</th>
                <th>Opening Date</th>
                <th></th>
            </tr>
            </thead>

            <tbody>
            </tbody>
        </table>
    </div>
</div>
<!-- Branch modal -->
<div class="modal fade" data-backdrop="static" id="branch-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Branch Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning alert-modal"></div>
                {!! Form::open(['method' => 'post', 'url' => route('company.branch.store', ['auditable_ent_id' => $details->company_id]), 'class' => 'form-horizontal', 'id' => 'branch-modal-form']) !!}
                <input type="hidden" name="company_id" value="{{$details->company_id}}">
                <input type="hidden" name="_method" value="post">
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="branch_code"> Branch </label>
                    <div class="col-sm-6">
                        <select id="branch_code" name="branch_code" class="form-control input-sm basic-info lookup" placeholder="Select a branch..." required>
                            <option value="{{ $details->branch_code }}" selected>{{ $details->branch_code }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="branch_address"> Address </label>
                    <div class="col-sm-8">
                        <textarea name="branch_address" id="branch_address" class="form-control basic-info" rows="5" style="resize: none;"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="branch_store_size"> Store Size </label>
                    <div class="col-sm-6">
                        <select id="branch_store_size" name="branch_store_size" class="form-control input-sm basic-info lookup" placeholder="Select a store size..." required>
                            <option value="{{ $details->branch_store_size }}" selected>{{ $details->branch_store_size }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="branch_zone"> Zone </label>
                    <div class="col-sm-6">
                        <select id="branch_zone" name="branch_zone" class="form-control input-sm basic-info lookup" placeholder="Select a zone..." required>
                            <option value="{{ $details->branch_zone }}" selected>{{ $details->branch_zone }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="branch_store_class"> Store Class </label>
                    <div class="col-sm-6">
                        <select id="branch_store_class" name="branch_store_class" class="form-control input-sm basic-info lookup" placeholder="Select a store class..." required>
                            <option value="{{ $details->branch_store_class }}" selected>{{ $details->branch_store_class }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="branch_class"> Branch Class </label>
                    <div class="col-sm-6">
                        <select id="branch_class" name="branch_class" class="form-control input-sm basic-info lookup" placeholder="Select a branch class..." required>
                            <option value="{{ $details->branch_class }}" selected>{{ $details->branch_class }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="entity_class"> Entity Class </label>
                    <div class="col-sm-6">
                        <select id="entity_class" name="entity_class" class="form-control input-sm basic-info lookup" placeholder="Select a entity class..." required>
                            <option value="{{ $details->entity_class }}" selected>{{ $details->entity_class }}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="branch_opening_date"> Opening Date </label>
                    <div class="col-sm-4">
                        <input type="text" name="branch_opening_date" id="branch_opening_date" class="form-control input-sm date-picker" value="" data-date-format="dd-M-yyyy">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="active"> Active? </label>
                    <div class="col-sm-3">
                        <select id="active" name="active" class="form-control input-sm basic-info" required>
                            <option value="Y" {!! ($details->active == 'Y' ? 'selected="selected"' : '') !!}>Yes</option>
                            <option value="N" {!! ($details->parent_entity == 'N' ? 'selected="selected"' : '') !!}>No</option>
                        </select>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-branch-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Branch modal -->
@section('details_script')
<script type="text/javascript">
    var data = {!! json_encode($branches) !!};
    var select_branch, $select_branch;
    var select_entity_class, $select_entity_class;
    var select_store_size, $select_store_size;
    var select_zone, $select_zone;
    var select_store_class, $select_store_class;
    var select_branch_class, $select_branch_class;
    var branch = $('#branch_code'),
        entity_class = $('#entity_class'),
        store_size = $('#branch_store_size'),
        zone = $('#branch_zone'),
        store_class = $('#branch_store_class'),
        branch_class = $('#branch_class');
    var datatable,
        $alert = $('.alert-modal'),
        $modal = $('#branch-modal');
    var lookup_url = '{{url('/administrator/lookup/type')}}';

    $('body').on('focus', ".date-picker", function(){
        $(this).datepicker({
            autoclose: true,
            todayHighlight: true
        });
    });

    $(function(){
        datatable = $('#branch-table').DataTable( {
            data: data,
            "processing": true,
            columns: [
                { data: "branch_code" },
                { data: "lv_branch_code_desc" },
                {
                    data: "active",
                    render: function (data, type, full) {
                       return data == 'Y' ? 'Yes' : 'No';
                    },
                    searchable: false
                },
                { data: "branch_store_size", defaultContent: '-', searchable: false },
                { data: "branch_address", defaultContent: '-', searchable: false },
                { data: "lv_branch_zone_desc", defaultContent: '-', searchable: false },
                { data: "branch_store_class", defaultContent: '-', searchable: false },
                { data: "branch_class", defaultContent: '-', searchable: false },
                { data: "entity_class", defaultContent: '-', searchable: false },
                { data: "branch_opening_date", defaultContent: '-', searchable: false },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        if( data.lv_branch_code_desc == 'default' ) return '';

                        return '<a href="#" class="btn-edit-branch edit" data-id="'+data.company_id+'" title="Edit" rel="tooltip"><i class="fa fa-pencil"></i></a> &nbsp; <a href="#" class="btn-delete-branch delete" data-id="'+data.company_id+'" data-branch="'+data.branch_code+'" data-name="'+data.lv_branch_code_desc+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    searchable : false,
                    orderable : false
                }
            ]
        } );
        $('#branch-table').parent().css('padding','0px');
        $select_branch = branch.selectize({
            valueField: 'text',
            labelField: 'meaning',
            searchField: ['text'],
            options: [],
            preload: true,
            create: false,
            load: function(query, callback) {
                $.ajax({
                    url: lookup_url+'/{{ config('iapms.lookups.branch.code') }}?search='+encodeURIComponent(query)+'&searchFields=description:like',
                    success: function(results) {
                        callback(results.data);
                    },
                    error: function() {
                        callback();
                    }
                });
            },
            render: {
                option: function(item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.text) + '</span>' +
                        '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            }
        });
        select_branch  = $select_branch[0].selectize;

        $select_entity_class = entity_class.selectize({
            valueField: 'meaning',
            labelField: 'meaning',
            searchField: ['text'],
            options: [],
            preload: true,
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.text) + '</span>' +
                        '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            load: function (query, callback) {
                $.ajax({
                    url: lookup_url + '/{{ config('iapms.lookups.branch.entity_class') }}?exclude=meaning|Company',
                    type: 'GET',
                    success: function (res) {
                        callback(res.data);
                    },
                    error: function () {
                        callback();
                    }
                });
            }
        });
        select_entity_class  = $select_entity_class[0].selectize;

        $select_store_size = store_size.selectize({
            valueField: 'meaning',
            labelField: 'meaning',
            searchField: ['text'],
            options: [],
            preload: true,
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.text) + '</span>' +
                        '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            load: function (query, callback) {
                $.ajax({
                    url: lookup_url + '/{{ config('iapms.lookups.branch.store.size') }}',
                    type: 'GET',
                    success: function (res) {
                        callback(res.data);
                    },
                    error: function () {
                        callback();
                    }
                });
            }
        });
        select_store_size  = $select_store_size[0].selectize;

        $select_zone = zone.selectize({
            valueField: 'text',
            labelField: 'meaning',
            searchField: ['text'],
            options: [],
            preload: true,
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.text) + '</span>' +
                        '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            load: function (query, callback) {
                $.ajax({
                    url: lookup_url + '/{{ config('iapms.lookups.branch.zone') }}',
                    type: 'GET',
                    success: function (res) {
                        callback(res.data);
                    },
                    error: function () {
                        callback();
                    }
                });
            }
        });
        select_zone  = $select_zone[0].selectize;

        $select_store_class = store_class.selectize({
            valueField: 'meaning',
            labelField: 'meaning',
            searchField: ['text'],
            options: [],
            preload: true,
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.text) + '</span>' +
                        '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            load: function (query, callback) {
                $.ajax({
                    url: lookup_url + '/{{ config('iapms.lookups.branch.store.class') }}',
                    type: 'GET',
                    success: function (res) {
                        callback(res.data);
                    },
                    error: function () {
                        callback();
                    }
                });
            }
        });
        select_store_class  = $select_store_class[0].selectize;

        $select_branch_class = branch_class.selectize({
            valueField: 'meaning',
            labelField: 'meaning',
            searchField: ['text'],
            options: [],
            preload: true,
            create: false,
            render: {
                option: function (item, escape) {
                    return '<div>' +
                        '<span class="title">' +
                        '<span class="name">' + escape(item.text) + '</span>' +
                        '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                        '</span>' +
                        '</div>';
                }
            },
            load: function (query, callback) {
                $.ajax({
                    url: lookup_url + '/{{ config('iapms.lookups.branch.class') }}',
                    type: 'GET',
                    success: function (res) {
                        callback(res.data);
                    },
                    error: function () {
                        callback();
                    }
                });
            }
        });
        select_branch_class  = $select_branch_class[0].selectize;
    });

    $modal.on('hidden.bs.modal', function (e) {
        init_modal();
    });

    $('.btn-branch-save').on('click', function () {
        swal({
            title: "Continue saving?",
            text: "You are about to add branch to this company.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, save it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(){
            $.ajax({
                url: $('#branch-modal-form').attr('action'),
                method: "POST",
                data: $('#branch-modal-form').serialize(), // serializes all the form data
                beforeSend : function () {
                    swal({
                        title: "Loading",
                        text: "Please wait...",
                        type: "info",
                        showConfirmButton: false
                    });
                },
                success : function(data) {
                    if( data.success ) {
                        swal("Success!", "Company branch successfully added.", "success");
                        location.reload();
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }
                }
            });
        });
    });

    $(document).on('click', '.btn-delete-branch', function() {
        var company = $(this).attr('data-id');
        var branch = $(this).attr('data-branch');
        var branch_name = $(this).attr('data-name');

        swal({
            title: "Continue?",
            text: "You are about to delete "+branch_name+" branch.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(){
            $.ajax({
                url: '{{ route('company.branch.delete', ['id' => $details->company_id]) }}',
                method: "POST",
                data: { _method : 'delete', _token : _token, branch : branch },
                success : function(data) {
                    swal("Saved!", branch_name+" is successfully deleted.", "success");
                    if( data.success ) {
                        location.reload();
                    }
                }
            });
        });

        return false;
    });

    $(document).on('click', '.btn-edit-branch', function() {
        var id = $(this).attr('data-id');
        var row = $(this).parent().parent();
        var data = datatable.row( row ).data();

        select_branch.setValue(data.branch_code);
        select_branch.disable();
        $('#branch_address').val(data.branch_address);
        select_store_size.setValue(data.branch_store_size);
        select_zone.setValue(data.branch_zone);
        select_store_class.setValue(data.branch_store_class);
        select_branch_class.setValue(data.branch_class);
        select_entity_class.setValue(data.entity_class);
        $('#branch_opening_date').val(data.branch_opening_date);
        $('#active').val(data.active);

        $('input[name="_method"]').val('put');
        $('#branch-modal-form').attr('action', '{{route('company.branch.update')}}?branch_code='+data.branch_code);

        $modal.modal('show');

        return false;
    });

    function init_modal() {
        select_branch.enable();
        select_branch.clear();
        $('#branch_address').val('');
        select_store_size.clear();
        select_zone.clear();
        select_store_class.clear();
        select_branch_class.clear();
        select_entity_class.clear();
        $('#branch_opening_date').val('');
        $('#active').val('');
        $('#branch-modal-form').attr('action', '{{route('company.branch.store', ['auditable_ent_id' => $details->company_id])}}');
        $('input[name="_method"]').val('post');

        $modal.find('.alert-modal').html('').hide();
        $modal.find('.basic-info').parent().parent().removeClass('has-error');
    }
    // Test
</script>
@endsection