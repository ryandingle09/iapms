{!! Form::open(['class' => 'form-horizontal update-form']) !!}
    <div class="form-group">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Cancel</a>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Audit Engagements / Projects</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <table id="engagement-table" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Engagement Type</th>
                        <th>IOM Number</th>
                        <th>Subject</th>
                        <th>Covered Location</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Risk Library</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <table id="risks-table" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Risk Name</th>
                        <th>Source</th>
                        <th>Impact</th>
                        <th>Likelihood</th>
                        <th>Risk Index</th>
                        <th>Status</th>
                    </tr>
                </thead>

                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Information Technology Profile</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">
                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="it_profile_desc" name="it_profile_desc">{{$details->it_profile_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Cancel</a>
            </div>
        </div>
    </div>
{!! Form::close() !!}
@section('details_script')
<script type="text/javascript">
    var data = {!! json_encode($risks) !!};
    var datatable;

    $(function(){
        datatable = $('#risks-table').DataTable( {
            data: data,
            "processing": true,
            columns: [
                { data: "risk_code" },
                { data: "risk_source_type", searchable:false },
                { data: "impact", searchable:false },
                { data: "likelihood", searchable:false },
                { data: "matrix_value", searchable:false },
                { data: "risk_response_type", searchable:false }
            ]
        } );
    })
</script>
@endsection