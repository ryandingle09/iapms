{!! Form::open(['class' => 'form-horizontal update-form']) !!}
    <div class="form-group">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Cancel</a>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Nature and Types of Investments</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="investment_desc" name="investment_desc">{{$details->investment_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Cancel</a>
            </div>
        </div>
    </div>
{!! Form::close() !!}