{!! Form::open(['class' => 'form-horizontal update-form']) !!}
    <div class="form-group">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Cancel</a>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Company Details</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="company"> Description </label>
                <div class="col-sm-8">
                    <textarea name="description" class="form-control" rows="5" style="resize: none;">{{ $details->description }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="group_type"> Group Type </label>
                <div class="col-sm-8">
                    <select id="group_type" name="group_type" class="form-control input-sm basic-info lookup" placeholder="Select a group type..." required></select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="group_type_desc"> Group Type Description </label>
                <div class="col-sm-8" id="group_type_desc">
                    <span class="label label-info label-wrap">{{ $details->lv_group_type_desc }}</span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="business_type"> Business Type </label>
                <div class="col-sm-8">
                    <select id="business_type" name="business_type" class="form-control input-sm basic-info lookup" placeholder="Select a business type..."  required></select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="business_type_desc"> Business Type Description </label>
                <div class="col-sm-8" id="business_type_desc">
                    <span class="label label-info label-wrap">{{ isset($details) ? $details->lv_business_type_desc : '' }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Cancel</a>
            </div>
        </div>
    </div>
{!! Form::close() !!}

@section('details_script')
<script type="text/javascript">
    var select_business_type, $select_business_type;
    var select_group_type, $select_group_type;
    var group_type = $('#group_type'),
        business_type = $('#business_type');
    var lookup_url = '{{url('/administrator/lookup/type')}}';

    $select_group_type = group_type.selectize({
        valueField: 'meaning',
        labelField: 'meaning',
        searchField: ['text'],
        options: [],
        create: false,
        preload : true,
        render: {
            option: function (item, escape) {
                return '<div>' +
                    '<span class="title">' +
                    '<span class="name">' + escape(item.text) + '</span>' +
                    '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                    '</span>' +
                    '</div>';
            }
        },
        load: function (query, callback) {
            $.ajax({
                url: lookup_url + '/{{ config('iapms.lookups.group_type') }}',
                type: 'GET',
                success: function (res) {
                    callback(res.data);
                    select_group_type.setValue('{{$details->group_type}}');
                },
                error: function () {
                    callback();
                }
            });
        },
        onChange: function(value) {
            if (!value.length) return;
            $('#group_type_desc span').html(select_group_type.options[value].description);
        }
    });
    select_group_type  = $select_group_type[0].selectize;

    $select_business_type = business_type.selectize({
        valueField: 'text',
        labelField: 'meaning',
        searchField: ['text'],
        options: [],
        create: false,
        preload : true,
        render: {
            option: function (item, escape) {
                return '<div>' +
                    '<span class="title">' +
                    '<span class="name">' + escape(item.text) + '</span>' +
                    '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                    '</span>' +
                    '</div>';
            }
        },
        load: function (query, callback) {
            $.ajax({
                url: lookup_url + '/{{ config('iapms.lookups.business_type') }}',
                type: 'GET',
                success: function (res) {
                    callback(res.data);
                    select_business_type.setValue('{{$details->business_type}}');
                },
                error: function () {
                    callback();
                }
            });
        },
        onChange: function(value) {
            if (!value.length) return;
            $('#business_type_desc span').html(select_business_type.options[value].description);
        }
    });
    select_business_type  = $select_business_type[0].selectize;
</script>
@endsection