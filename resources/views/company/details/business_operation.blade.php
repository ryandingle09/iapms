{!! Form::open(['class' => 'form-horizontal update-form']) !!}
    <div class="form-group">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Cancel</a>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Line of Business</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="line_of_bus_desc" name="line_of_bus_desc">{{$details->line_of_bus_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Products</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="products_desc" name="products_desc">{{$details->products_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Volume of Operations</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="vol_of_op_desc" name="vol_of_op_desc">{{$details->vol_of_op_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Customers and Market</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="cust_and_market_desc" name="cust_and_market_desc">{{$details->cust_and_market_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Suppliers</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="suppliers_desc" name="suppliers_desc">{{$details->suppliers_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Competitors</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="competitors_desc" name="competitors_desc">{{$details->competitors_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Cancel</a>
            </div>
        </div>
    </div>
{!! Form::close() !!}