{!! Form::open(['class' => 'form-horizontal update-form']) !!}
    <div class="form-group">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Cancel</a>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Financial Reporting / Standards Accounting Policies and Framework</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="fin_rep_and_acct_desc" name="fin_rep_and_acct_desc">{{$details->fin_rep_and_acct_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Taxation</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="taxation_desc" name="taxation_desc">{{$details->taxation_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Government Policies</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="gov_pol_desc" name="gov_pol_desc">{{$details->gov_pol_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Legal Requirements</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="legal_req_desc" name="legal_req_desc">{{$details->legal_req_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Cancel</a>
            </div>
        </div>
    </div>
{!! Form::close() !!}