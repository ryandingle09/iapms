{!! Form::open(['class' => 'form-horizontal update-form']) !!}
    <div class="form-group">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Cancel</a>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Key People Charged with Governance</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="key_people_desc" name="key_people_desc">{{$details->key_people_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Board of Directors</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="board_of_dir_desc" name="board_of_dir_desc">{{$details->board_of_dir_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Consultants</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="consultants_desc" name="consultants_desc">{{$details->consultants_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Third Party Relationship of the Company</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="third_party_rel_desc" name="third_party_rel_desc">{{$details->third_party_rel_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="widget-box transparent">
        <div class="widget-header">
            <h4 class="widget-title lighter">Stockholders Information</h4>
        </div>

        <div class="widget-body" style="display: block; padding-top: 10px;">
            <div class="form-group">

                <div class="col-xs-12">
                    <textarea class="form-control input-sm basic-info tinymce-editor" id="stockholders_desc" name="stockholders_desc">{{$details->stockholders_desc}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-xs-12">
            <div class="pull-right">
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i> Save</button>
                <a href="{{ route('company') }}" class="btn btn-warning btn-sm"><i class="fa fa-times"></i> Cancel</a>
            </div>
        </div>
    </div>
{!! Form::close() !!}