@extends('layouts.app')
@section('styles')
<style type="text/css">
    .row-details {
        padding-bottom: 5px;
    }
    .selected-row {
        background-color: #ffe99e !important;
    }
    .table-hover tbody > tr:hover {
        cursor: pointer;
    }
</style>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')
        <div class="page-content">
            @include('includes.content_header')
        	<div class="col-xm-12">
                
                @include('plan_guide.widget_filter')
                @include('plan_guide.widget_auditable_entity')
                @include('plan_guide.widget_main_business_proccess')
                @include('plan_guide.widget_risk')
                @include('plan_guide.widget_project')
        	</div>
		</div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@include('includes.modals.custom_measures')
@include('plan_guide.includes.project_scope_modal')
@endsection
@section('footer_script')
<script type="text/javascript">
    var cartData = [];
    var audit_year = "";
    var selectedPlan = 0;
    var aeId = 0;
    var mbpaId = 0;
    var mbpName = 0;
    var rId = 0;
    var project_datatable = null;
    var selectedProject = 0;
    var selectedAuditType = "";
    var selectedPivot = "";

    $.checkCart = function(itemObject){
        var i;
        for (i = 0; i < cartData.length; i++) {
            if (cartData[i].mbpa_id === itemObject.mbpa_id && cartData[i].audit_type === itemObject.audit_type ) {
                return true;
            }
        }
        return false;
    }

    $.removeToCart = function(itemObject){
        cartData = $.grep( cartData,
            function(value,key) { 
                return ( value.audit_type === itemObject.audit_type 
                    && value.mbpa_id ===  itemObject.mbpa_id ) ; 
            },
            true);
    }

    $(document).on('change','.mbp',function(){
        var object = {
            mbpa_id : $(this).data('mbpa-id'),
            audit_type : $(this).data('audit-type'),
        };

        if( $(this).is(':checked') ){

            if( $.checkCart(object) ){
                $.gSuccess("MBP already in the cart.");
            }else{
                cartData.push(object);
                $.gSuccess("MBP has been added to cart.");
            }

        }else{
            $.removeToCart(object);
            $.gSuccess("MBP has been removed to cart.");   
        }
        console.log(cartData)
    });

    $(document).on('click','.btn-restart',function(){
        $.sWarning("Restarting the smart guide will wipe out all the selected scopes and inputs. Are you sure you want to continue?",function(){
            location.reload();
        });
        return false;
    });
</script>
@yield('widget_filter_scripts')
@yield('widget_auditable_entity_scripts')
@yield('widget_main_business_proccess')
@yield('widget_risk_scripts')
@yield('widget_project_scripts')
<script type="text/javascript">
            
    $(document).on('click',".btn-project",function(){
        if(cartData.length){
            $(".shopping-view").fadeOut("fast");
            $("#widget-project").fadeIn('slow');
            var jsonCartData = JSON.stringify(cartData);
            project_datatable.ajax.url( "{{ url('plan-guide/project-list') }}").load();
            scope_summary.ajax.url( "{{ url('plan-guide/cart-data') }}?cart_data=" + jsonCartData ).load();
            selectedProject = 0;
            selectedAuditType = "";
            $("#btn-add-to-project").attr('disabled','disabled');

        }else{
            $.sInfo("You need to select at least 1 AE/MBP combinations.")
        }
        return false;
    });
</script>
@yield('cm_scripts')
@yield('project_scope_modal_scripts')
@endsection