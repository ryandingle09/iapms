<div class="widget-box widget-color-blue2 widget-container-col shopping-view" id="by-auditable_entity" style="display: none;" >
    <div class="widget-header">
        <h4 class="widget-title lighter smaller"><i class="fa fa-filter"></i> By Auditable Entity</h4>
    </div>
    <div class="widget-body">
        <div class="widget-main padding-8">
            <div class="row">
                <div class="col-sm-4" >
                    <div id="bae-ae-wrapper">
                        <form id="bae-ae-search-form">
                            <div class="input-group input-group-sm">
                                <input type="text"  name="search" class="form-control" value="" placeholder="Please type any character to search">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary">
                                        <i class="ace-icon fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                        <div class="shopping-box clearfix">
                            <table class="table table-bordered search-table table-hover" id="bae-ae-table">
                                <thead>
                                    <tr>
                                        <th>Auditable Entity</th>
                                        <th width="50">Severity Value</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div id="bae-mbp-wrapper">
                        <form id="bae-mbp-search-form">
                            <div class="input-group input-group-sm">
                                <input type="text"  name="search" class="form-control" value="" placeholder="Please type any character to search">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary">
                                        <i class="ace-icon fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                        <div class="shopping-box clearfix">
                            <table class="table table-bordered search-table table-hover" id="bae-mbp-table">
                                <thead>
                                    <tr>
                                        <tr>
                                            <th>Main Business Proccess</th>
                                            <th width="50">Severity Value</th>
                                            <th width="50">CM</th>
                                            <th width="50">Audit Type</th>
                                            <td width="10">&nbsp;</td>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div id="bae-risk-wrapper">
                        <form id="bae-risk-search-form">
                            <div class="input-group input-group-sm">
                                <input type="text"  name="search" class="form-control" value="" placeholder="Please type any character to search">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary">
                                        <i class="ace-icon fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                        <div class="shopping-box clearfix">
                            <table class="table table-bordered search-table table-hover" id="bae-risk-table">
                                <thead>
                                    <tr>
                                        <tr>
                                            <th>Risk</th>
                                            <th width="50">Severity Value</th>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-4 control-label align-left" for="status"> Auditor's Allotted Mandays </label>
                        <div class="col-sm-6">
                            <span class="label label-xlg label-primary label-wrap arrowed plan_alloted_mandays">0</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label align-left" for="status"> Auditor's Annual Mandays </label>
                        <div class="col-sm-6">
                            <span class="label label-xlg label-primary label-wrap arrowed plan_annual_mandays">0</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label align-left" for="status"> Auditor's Available Mandays </label>
                        <div class="col-sm-6">
                            <span class="label label-xlg label-primary label-wrap arrowed plan_available_mandays">0</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-sm-12">
                    <div class="pull-right">
                        <a href="" class="btn btn-primary btn-restart"> 
                            <i class="fa fa-refresh"></i> Restart </a>
                        <button class="btn btn-primary btn-project" >
                            <i class="fa fa-angle-double-right"></i> Next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('widget_auditable_entity_scripts')
<script type="text/javascript">
    // aeId came from index.blade.php;
    // mbpaId came from index.blade.php;

    bae_ae_datatable = $("#bae-ae-table").DataTable({ 
        dom : '<<t>p>' , 
        pagingType : 'simple_numbers',
        order: [[ 1, 'desc' ]],
        createdRow: function( row, data, dataIndex ) {
            $( row ).attr('data-ae-id', data.auditable_entity_id);
        },
        columns: [
            { data: "auditable_entity_name" },
            { data: "severity_value" }
        ]
    });

    bae_mbp_datatable = $("#bae-mbp-table").DataTable({ 
        dom : '<<t>p>' , 
        pagingType : 'simple_numbers',
        order: [[ 1, 'desc' ]],
        createdRow: function( row, data, dataIndex ) {
            $( row ).attr('data-mbpa-id', data.ae_mbp_id);
        },
        columns: [
            { data: "main_bp_name" },
            { data: "severity_value" },
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    return '<a href="#" class="btn-custom-measure" data-ae_id="'+data.auditable_entity_id+'" data-id="'+data.master_ae_mbp_id+'" ><i class="fa fa-dashboard text-primary" title="Custom Measures" rel="tooltip"></i></a>';
                },
                orderable: false,
                className: "text-center"
            },
            { data: "audit_type" },
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    if( $.checkCart({ mbpa_id : data.ae_mbp_id , audit_type : data.audit_type }) ){
                        return '<input type="checkbox" class="mbp" data-mbpa-id="'+data.ae_mbp_id+'"  data-audit-type="'+data.audit_type+'" checked>';
                    }else{
                        return '<input type="checkbox" class="mbp" data-mbpa-id="'+data.ae_mbp_id+'" data-audit-type="'+data.audit_type+'" >';
                    }
                },
                orderable: false,
                className: "text-center"
            },
        ]
    });

    bae_risk_datatable = $("#bae-risk-table").DataTable({ 
        dom : '<<t>p>' , 
        pagingType : 'simple_numbers',
        order: [[ 1, 'desc' ]],
        createdRow: function( row, data, dataIndex ) {
            // $( row ).attr('data-ae-id', data.auditable_entity_id);
        },
        columns: [
            { data: "risk_code" },
            { data: "severity_value" }
        ]
    });

    $("#bae-ae-search-form").submit(function(){
        var keyword = $(this).find('[name="search"]').val().trim();   
        $("#bae-ae-wrapper").LoadingOverlay("show");
        bae_mbp_datatable.clear();
        bae_risk_datatable.clear();
        $.ajax({
            url: "{{ route('plan_guide.search_data') }}",
            method: "GET",
            data: { 
                fsearch : "auditable_entity_name:"+keyword,
                fgroupBy : "auditable_entity_id",
                ffilter : "auditable_entity_name;auditable_entity_id;"
            },
            success: function (response) {
                bae_ae_datatable.clear();
                bae_ae_datatable.rows.add(response.data).draw();
                $("#bae-ae-wrapper").LoadingOverlay("hide");
            }
        });
        return false;
    });

    $(document).on("click","#bae-ae-table tbody tr",function(){
        $("#bae-ae-table tbody tr.selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        aeId = $(this).data('ae-id');
        $("#bae-mbp-wrapper").LoadingOverlay("show");
        $.ajax({
            url: "{{ route('plan_guide.search_data') }}",
            method: "GET",
            data: { 
                fsearch : "auditable_entity_id:"+aeId,
                fgroupBy : "audit_type;mbpa_id",
                ffilter : "mbpa_id;mbpa_name;auditable_entity_id;master_ae_mbp_id;audit_type"
            },
            success: function (response) {
                bae_mbp_datatable.clear();
                bae_risk_datatable.clear();
                bae_mbp_datatable.rows.add(response.data).draw();
                $("#bae-mbp-wrapper").LoadingOverlay("hide");
            }
        });
    });


    $("#bae-mbp-search-form").submit(function(){
        var keyword = $(this).find('[name="search"]').val().trim();    
        $("#bae-mbp-wrapper").LoadingOverlay("show");
        $.ajax({
            url: "{{ route('plan_guide.search_data') }}",
            method: "GET",
            data: { 
                fsearch : "auditable_entity_id:" +aeId + ";mbpa_name:" + keyword,
                fgroupBy : "audit_type;mbpa_id",
                ffilter : "mbpa_id;mbpa_name;auditable_entity_id;master_ae_mbp_id;audit_type"
            },
            success: function (response) {
                bae_mbp_datatable.clear();
                bae_mbp_datatable.rows.add(response.data).draw();
                $("#bae-mbp-wrapper").LoadingOverlay("hide");
            }
        });
        return false;
    });

    $(document).on("click","#bae-mbp-table tbody tr",function(){
        $("#bae-mbp-table tbody tr.selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        mbpaId = $(this).data('mbpa-id');
        $("#bae-risk-wrapper").LoadingOverlay("show");
        $.ajax({
            url: "{{ route('plan_guide.search_data') }}",
            method: "GET",
            data: { 
                fsearch : "auditable_entity_id:" +aeId + ";mbpa_id:"+mbpaId,
                fgroupBy : "risk_id",
                ffilter : "risk_id;risk_code;mbpa_id"
            },
            success: function (response) {
                bae_risk_datatable.clear();
                bae_risk_datatable.rows.add(response.data).draw();
                $("#bae-risk-wrapper").LoadingOverlay("hide");
            }
        });
    });

    $("#bae-risk-search-form").submit(function(){
        var keyword = $(this).find('[name="search"]').val().trim();     
        $("#bae-risk-wrapper").LoadingOverlay("show");
        $.ajax({
            url: "{{ route('plan_guide.search_data') }}",
            method: "GET",
            data: { 
                fsearch : "auditable_entity_id:" +aeId + ";mbpa_id:"+mbpaId+";risk_code:" + keyword,
                fgroupBy : "risk_id",
                ffilter : "risk_id;risk_code;mbpa_id"
            },
            success: function (response) {
                bae_risk_datatable.clear();
                bae_risk_datatable.rows.add(response.data).draw();
                $("#bae-risk-wrapper").LoadingOverlay("hide");
            }
        });
        return false;
    });
   
</script>
@endsection