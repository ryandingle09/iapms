<div class="widget-box widget-color-blue2 widget-container-col shopping-view" id="by-main_business_proccess" style="display: none;">
    <div class="widget-header">
        <h4 class="widget-title lighter smaller"><i class="fa fa-filter"></i> By Main Business Proccess</h4>
    </div>
    <div class="widget-body">
        <div class="widget-main padding-8">
            <div class="row">
                <div class="col-sm-3">
                    <div id="bmbp-mbp-wrapper">
                        <form id="bmbp-mbp-search-form">
                            <div class="input-group input-group-sm">
                                <input type="text"  name="search" class="form-control" value=""  placeholder="Please type any character to search">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary">
                                        <i class="ace-icon fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                        <div class="shopping-box clearfix">
                            <table class="table table-bordered search-table table-hover" id="bmbp-mbp-table">
                                <thead>
                                    <tr>
                                        <tr>
                                            <th>Main Business Proccess</th>
                                            <th width="50">Severity Value</th>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5" >
                    <div id="bmbp-ae-wrapper">
                        <form id="bmbp-ae-search-form">
                            <div class="input-group input-group-sm">
                                <input type="text"  name="search" class="form-control" value=""  placeholder="Please type any character to search">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary">
                                        <i class="ace-icon fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                        <div class="shopping-box clearfix">
                            <table class="table table-bordered search-table table-hover" id="bmbp-ae-table">
                                <thead>
                                    <tr>
                                        <th>Auditable Entity</th>
                                        <th width="50">Severity Value</th>
                                        <th width="50">CM</th>
                                        <th width="50">Audit Type</th>
                                        <td width="10">&nbsp;</td>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div id="bmbp-risk-wrapper">
                        <form id="bmbp-risk-search-form">
                            <div class="input-group input-group-sm">
                                <input type="text"  name="search" class="form-control" value=""  placeholder="Please type any character to search">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary">
                                        <i class="ace-icon fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                        <div class="shopping-box clearfix">
                            <table class="table table-bordered search-table table-hover" id="bmbp-risk-table">
                                <thead>
                                    <tr>
                                        <tr>
                                            <th>Risk</th>
                                            <th width="50">Severity Value</th>
                                        </tr>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-4 control-label align-left" for="status"> Auditor's Allotted Mandays </label>
                        <div class="col-sm-6">
                            <span class="label label-xlg label-primary label-wrap arrowed plan_alloted_mandays">0</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label align-left" for="status"> Auditor's Annual Mandays </label>
                        <div class="col-sm-6">
                            <span class="label label-xlg label-primary label-wrap arrowed plan_annual_mandays">0</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label align-left" for="status"> Auditor's Available Mandays </label>
                        <div class="col-sm-6">
                            <span class="label label-xlg label-primary label-wrap arrowed plan_available_mandays">0</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="pull-right">
                            <a href="" class="btn btn-primary btn-restart"> 
                                <i class="fa fa-refresh"></i> Restart </a>
                            <button class="btn btn-primary btn-project" >
                                <i class="fa fa-angle-double-right"></i> Next</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('widget_main_business_proccess')
<script type="text/javascript">
    // aeId came from index.blade.php;
    // mmbpId came from index.blade.php;
    // mbpName came from index.blade.php;
    
    bmbp_mbp_datatable = $("#bmbp-mbp-table").DataTable({ 
        dom : '<<t>p>' , 
        pagingType : 'simple_numbers',
        createdRow: function( row, data, dataIndex ) {
            $( row ).attr('data-mbp-name', data.main_bp_name);
        },
        order: [[ 1, 'desc' ]],
        columns: [
            { data: "main_bp_name" },
            { data: "severity_value" }
        ]
    });

    bmbp_ae_datatable = $("#bmbp-ae-table").DataTable({ 
        dom : '<<t>p>' , 
        pagingType : 'simple_numbers',
        createdRow: function( row, data, dataIndex ) {
            $( row ).attr('data-ae-id', data.auditable_entity_id);
        },
        order: [[ 1, 'desc' ]],
        columns: [
            { data: "auditable_entity_name" },
            { data: "severity_value" },
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    return '<a href="#" class="btn-custom-measure" data-ae_id="'+data.auditable_entity_id+'" data-id="'+data.master_ae_mbp_id+'" ><i class="fa fa-dashboard text-primary" title="Custom Measures" rel="tooltip"></i></a>';
                },
                orderable: false,
                className: "text-center"
            },
            { data: "audit_type" },
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    if( $.checkCart({ mbpa_id : data.ae_mbp_id , audit_type : data.audit_type }) ){
                        return '<input type="checkbox" class="mbp" data-mbpa-id="'+data.ae_mbp_id+'"  data-audit-type="'+data.audit_type+'" checked>';
                    }else{
                        return '<input type="checkbox" class="mbp" data-mbpa-id="'+data.ae_mbp_id+'" data-audit-type="'+data.audit_type+'" >';
                    }
                },
                orderable: false,
                className: "text-center"
            },
        ]
    });

    bmbp_risk_datatable = $("#bmbp-risk-table").DataTable({ 
        dom : '<<t>p>' , 
        pagingType : 'simple_numbers',
        order: [[ 1, 'desc' ]],
        createdRow: function( row, data, dataIndex ) {
            // $( row ).attr('data-ae-id', data.auditable_entity_id);
        },
        columns: [
            { data: "risk_code" },
            { data: "severity_value" }
        ]
    });

    $("#bmbp-mbp-search-form").submit(function(){
        var keyword = $(this).find('[name="search"]').val().trim();   
        $("#bmbp-mbp-wrapper").LoadingOverlay("show");
        $.ajax({
            url: "{{ route('plan_guide.search_data') }}",
            method: "GET",
            data: { 
                fsearch : "mbpa_name:"+keyword,
                fgroupBy : "mbpa_name",
                ffilter : "mbpa_name;"
            },
            success: function (response) {
                bmbp_mbp_datatable.clear();
                bmbp_mbp_datatable.rows.add(response.data).draw();
                $("#bmbp-mbp-wrapper").LoadingOverlay("hide");
            }
        });
        return false;
    });

    $(document).on("click","#bmbp-mbp-table tbody tr",function(){
        $("#bmbp-mbp-table tbody tr.selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        mbpName = $(this).data('mbp-name');
        $("#bmbp-ae-wrapper").LoadingOverlay("show");
        $.ajax({
            url: "{{ route('plan_guide.search_data') }}",
            method: "GET",
            data: { 
                fsearch : "mbpa_name:"+mbpName,
                fgroupBy : "audit_type;auditable_entity_id",
                ffilter : "auditable_entity_name;auditable_entity_id;master_ae_mbp_id;audit_type;mbpa_id"
            },
            success: function (response) {
                bmbp_ae_datatable.clear();
                bmbp_ae_datatable.rows.add(response.data).draw();
                $("#bmbp-ae-wrapper").LoadingOverlay("hide");
            }
        });
    });

    $("#bmbp-ae-search-form").submit(function(){
        var keyword = $(this).find('[name="search"]').val().trim();
        $("#bmbp-ae-wrapper").LoadingOverlay("show");
        bmbp_risk_datatable.clear();
        $.ajax({
            url: "{{ route('plan_guide.search_data') }}",
            method: "GET",
            data: { 
                fsearch : "mbpa_name:"+mbpName+";auditable_entity_name:"+keyword,
                fgroupBy : "audit_type;auditable_entity_id",
                ffilter : "auditable_entity_name;auditable_entity_id;master_ae_mbp_id;audit_type;mbpa_id"
            },
            success: function (response) {
                bmbp_ae_datatable.clear();
                bmbp_ae_datatable.rows.add(response.data).draw();
                $("#bmbp-ae-wrapper").LoadingOverlay("hide");
            }
        });
        return false;
    });

    $(document).on("click","#bmbp-ae-table tbody tr",function(){
        $("#bmbp-ae-table tbody tr.selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        aeId = $(this).data('ae-id');
        $("#bmbp-risk-wrapper").LoadingOverlay("show");
        $.ajax({
            url: "{{ route('plan_guide.search_data') }}",
            method: "GET",
            data: { 
                fsearch : "mbpa_name:"+mbpName+";auditable_entity_id:"+aeId,
                fgroupBy : "risk_id",
                ffilter : "risk_id;risk_code"
            },
            success: function (response) {
                bmbp_risk_datatable.clear();
                bmbp_risk_datatable.rows.add(response.data).draw();
                $("#bmbp-risk-wrapper").LoadingOverlay("hide");
            }
        });
    });

    $("#bmbp-risk-search-form").submit(function(){
        var keyword = $(this).find('[name="search"]').val().trim();    
        $("#bmbp-risk-wrapper").LoadingOverlay("show");
        $.ajax({
            url: "{{ route('plan_guide.search_data') }}",
            method: "GET",
            data: { 
                fsearch : "mbpa_name:"+mbpName+";auditable_entity_id:"+aeId+";risk_code:" + keyword,
                fgroupBy : "risk_id",
                ffilter : "risk_id;risk_code"
            },
            success: function (response) {
                bmbp_risk_datatable.clear();
                bmbp_risk_datatable.rows.add(response.data).draw();
                $("#bmbp-risk-wrapper").LoadingOverlay("hide");
            }
        });
        return false;
    });
   
</script>
@endsection