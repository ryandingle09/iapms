<div class="widget-box widget-color-blue2 widget-container-col"  id="widget-project" style="display: none;" >
    <div class="widget-header">
        <h4 class="widget-title lighter smaller"><i class="fa fa-filter"></i> Add to Project</h4>
    </div>
    <div class="widget-body">
        <div class="widget-main padding-8">
            <div class="row mrgB15">
                <div class="col-xs-12">
                    <div class="table-header" > Summary </div>
                </div>

                <div class="col-sm-12">
                    <table class="table table-bordered table-hover" id="scope-summary">
                        <thead>
                            <tr>
                                <th width="20" class="text-center" style="padding: 5px!important;"> <input type="checkbox" class="summary-cball" > </th>
                                <th>Auditable Entity IDL</th>
                                <th>Main Business Proccess</th>
                                <th>Audit Type</th>
                                <th>Severity Value</th>
                                <th>Budgeted Mandays</th>
                                <th width="40">Delete</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="row mrgB15">
                <div class="col-xs-12 align-left">
                    <a class="btn btn-primary btn-sm " id="show-create-project-modal" >
                        <i class="ace-icon fa fa-plus bigger-120"></i>
                        Create
                    </a>
                </div>
                <div class="col-xs-12">
                    <div class="table-header" > Plan Projects </div>
                </div>

                <div class="col-sm-12">
                    <table class="table table-bordered table-hover" id="project-table">
                        <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Audit Type</th>
                                <th>Running Allotted Mandays</th>
                                <th>Mandays to Add</th>
                                <th>Total Mandays</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="pull-right">
                            <a href="" class="btn btn-primary btn-back-pivot"> 
                                <i class="fa fa-angle-double-left"></i> Back 
                            </a>
                            <button class="btn btn-primary" id="btn-add-to-project" disabled="disabled"><i class="fa fa-link"></i> Link to Project</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('plan_guide.includes.project_modal')
@section('widget_project_scripts')
<script type="text/javascript">

    scope_summary = $('#scope-summary').DataTable( {
        bDestroy: true,
        processing: true,
        order: [[ 4, 'asc' ]],
        columns: [

            {
                data: null,
                render : function ( data, type, full, meta ) {
                    return '<input type="checkbox" class="summary-cb" data-mbpa-id="'+data.mbpa_id+'"  data-audit-type="'+data.audit_type+'">';
                },
                orderable: false,
                className: "text-center"
            },
            { data: "auditable_entity_name" },
            { data: "main_bp_name" },
            { data: "audit_type" },
            { data: "adjusted_severity_value" },
            { data: "budgeted_mandays" },
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    return '<a href="#" class="delete" data-mbpa-id="'+data.mbpa_id+'"  data-audit-type="'+data.audit_type+'"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                className: "text-center"
            },

        ]
    });

    project_datatable = $('#project-table').DataTable( {
        bDestroy: true,
        processing: true,
        order: [[ 1, 'asc' ]],

        createdRow: function( row, data, dataIndex ) {
            if(selectedProject == data.plan_project_id){
                $( row ).addClass('selected-row');
            }

            $( row ).attr('data-pp-id', data.plan_project_id);
            $( row ).attr('data-pp-audit-type', data.audit_type);
        },
        columns: [
            { data: "plan_project_name" },
            { data: "audit_type" },
            { data: "running_allotted_mandays" },
            { data: "mandays_to_add" },
            { data: "total_mandays" }
        ]
    });

    $(document).on('click',"#project-table tr",function(){
        $("#project-table tr.selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        selectedProject = $(this).data('pp-id');
        selectedAuditType = $(this).data('pp-audit-type');
        $("#btn-add-to-project").removeAttr('disabled');
    });

    $(document).on("click","#btn-add-to-project",function(){
        var temp = [];
        var tempAuditType = "";
        var filteredCartData = [];
        var typeError = 0;
        $(".summary-cb:checked").each(function(){
            var thisMbpaId = $(this).data('mbpa-id');
            var thisType = $(this).data('audit-type');
            if( tempAuditType != ""){
                if(tempAuditType != thisType) {
                    typeError++;   
                }else{
                    tempAuditType = thisType;
                    filteredCartData.push({
                        mbpa_id : thisMbpaId,
                        audit_type : thisType,
                    });    
                }
            }else{
                tempAuditType = thisType;
                filteredCartData.push({
                    mbpa_id : thisMbpaId,
                    audit_type : thisType,
                });
            }
        });

        if( typeError ){
            $.sInfo("You cannot select multiple audit type on summary.");
                    return false;
        }

        if(filteredCartData.length <= 0){
            $.sInfo("Please select scope from the summary.");
            return false;
        }

        if( tempAuditType != selectedAuditType ){
            $.sInfo("Selected project's audit type does not match with the selected scope's audit type.");
            return false;
        }

        $.sWarning('Are you sure you want to add selected scopes to this project?',function(){
            $.ajax({
                url : "{{ route('plan_guide.add_to_project') }}",
                method : "POST",
                data : { 
                    plan_project_id : selectedProject,
                    cart_data : JSON.stringify(filteredCartData)
                },
                success : function(response){
                    project_scope_datatable.ajax.url( "{{ url('plan-project')}}/"+selectedProject+"/scope/list" ).load();
                    $.sSuccess( response.message ,function(){
                        $("#project-scope-modal").modal('show');
                        $.each(filteredCartData , function(key, value){
                            console.log(value);
                            $.removeToCart({
                                mbpa_id : value.mbpa_id,
                                audit_type : value.audit_type,
                            });
                        });
                        var jsonCartData = JSON.stringify(cartData);
                        project_datatable.ajax.url( "{{ url('plan-guide/project-list') }}").load();
                        scope_summary.ajax.url( "{{ url('plan-guide/cart-data') }}?cart_data=" + jsonCartData ).load();
                        selectedProject = 0;
                        selectedAuditType = "";
                        $("#btn-add-to-project").attr('disabled','disabled');
                    });
                }
            });
        });
        return false;
    });

    $(document).on("click",'.btn-back-pivot',function(){
        $("#widget-project").fadeOut('fast');
        $(selectedPivot).fadeIn("fast");
        return false;
    });

    $(document).on("click",".summary-cball" , function(){
        $('.summary-cb').prop('checked', this.checked );    
    });

    $(document).on("click",".summary-cb" , function(event){
        
    });

    $(document).on('click','#scope-summary .delete' , function(){
        var object = {
            mbpa_id : $(this).data('mbpa-id'),
            audit_type : $(this).data('audit-type'),
        };
        $.sWarning("Are you sure you want to delete this item?", function(){
            swal.close()
            $.removeToCart(object);
            var jsonCartData = JSON.stringify(cartData);
            scope_summary.ajax.url( "{{ url('plan-guide/cart-data') }}?cart_data=" + jsonCartData ).load();
            $.gSuccess("MBP has been removed to cart.");   
        });
        return false;
    })
</script>
@yield('project_modal_scripts')
@endsection