<div class="widget-box widget-color-blue2 widget-container-col" id="filter-widget">
    <div class="widget-header">
        <h4 class="widget-title lighter smaller"><i class="fa fa-filter"></i> Filter</h4>
    </div>
    <div class="widget-body collapse in" id="filter-body">
        <div class="widget-main padding-8">

            <form class="form-horizontal" id="filter-form">
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="pull-right">
                            <a href="#" class="btn btn-primary btn-sm" id="pasv"><i class="fa fa-refresh"></i> PASV</a>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="audit_year"> Annual Audit Year </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm" name="audit_year" value="{{ Request::get('dyear') ?  Request::get('dyear') : date('Y') }}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="group_by"> Pivot By </label>
                    <div class="col-sm-8">
                        <select class="form-control input-sm" name="group_by"></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="pull-right">
                            @if(Request::get('dyear'))
                            <a href="{{ url('plan?search_year='.Request::get('dyear'))}}" class="btn btn-primary btn-sm"> 
                                <i class="fa fa-angle-double-left"></i> Back to Plan Details </a>
                            @endif
                            <button class="btn btn-primary btn-sm" ><i class="fa fa-angle-double-right"></i> Next</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@section('widget_filter_scripts')
<script type="text/javascript">
    var ff_group_by_select = $("#filter-form [name='group_by']").makeSelectize({
        lookup : '{{ config('iapms.lookups.plan.group_by') }}'
    })[0].selectize;

    $("#filter-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url: "{{ route('plan_guide.check_plan') }}",
            method: "PUT",
            data: $(this).serialize(),
            beforeSend : sLoading(),
            success: function (response) {
                swal.close();
                audit_year = $("#filter-form [name='audit_year']").val();
                selectedPlan = response.data.plan_id;
                selectedPivot = "#by-"+ $("#filter-form [name='group_by']").val();
                $("#filter-widget").fadeOut('fast');
                $( selectedPivot ).fadeIn('slow');
                $(".plan_alloted_mandays").html(response.data.allotted_mandays);
                $(".plan_annual_mandays").html(response.data.annual_mandays);
                $(".plan_available_mandays").html(response.data.available_mandays);
            }
        });
        return false;
    });

    $("#pasv").click(function(){
        $.ajax({
            url: "{{ route('plan_guide.pasv') }}",
            method: "GET",
            data: $(this).serialize(),
            beforeSend : sLoading(),
            success: function (response) {
                $.sSuccess(response.message);
            }
        });
    })
</script>
@endsection