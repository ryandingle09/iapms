<!-- Create Project Scope Modal -->
<div class="modal fade " data-backdrop="static" id="project-scope-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Project Scope List</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 mrgB15">
                        <div class="table-header"> Project Scopes </div>
                        <table id="project-scope-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th >Auditable Entity Name</th>
					                <th >Main Business Process Name</th>
					                <th >Budgeted Mandays</th>
					                <th >Adjusted Severity Value</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">              
                    <div class="col-xs-12 ">
                        <div class="form-row align-right" >
                            <a href="#" class="btn btn-primary btn-sm btn-restart"> 
                                <i class="fa fa-refresh"></i> Restart 
                            </a>
                            <a href="#" class="btn btn-warning btn-sm" data-dismiss="modal">
                                <i class="ace-icon fa fa-times bigger-120"></i>
                                Close
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Project Scope Modal -->
@section('project_scope_modal_scripts')
<script type="text/javascript">

	var project_scope_datatable = $("#project-scope-table").DataTable( {
        "bDestroy": true,
        "processing": true,
        order: [[ 1, 'asc' ]],
        columns: [
            { data: "auditable_entity.auditable_entity_name" },
            { data: "main_bp.main_bp_name" },
            { data: "budgeted_mandays" },
            { data: "adjusted_severity_value" }
        ]
    });

</script>
@endsection