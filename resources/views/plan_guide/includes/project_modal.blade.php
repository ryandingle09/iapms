<!-- Create Project Modal -->
<div class="modal fade " data-backdrop="static" id="create-project-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Project</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-project-alert"></div>
                        <form class="form-horizontal" id="create-project-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="group"> Plan Group </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm group" name="group"></select>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="plan_project_name"> Project Name </label>
                                <div class="col-sm-10">
                                    <input type="text" name="plan_project_name" class="form-control input-sm plan_project_name" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_type"> Audit Type </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm group" name="audit_type"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="plan_project_status"> Status </label>
                                <div class="col-sm-10">
                                    <input type="text" name="plan_project_status" class="form-control input-sm plan_project_status" value="New" readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-10">
                                    <div class="input-daterange input-group">
                                        <input type="text" name="target_start_date" class="form-control input-sm form-control target_start_date" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" name="target_end_date" class="form-control input-sm form-control target_end_date" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="plan_project_remarks"> Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm" name="plan_project_remarks" id="plan_project_remarks" rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Project Modal -->

@section('project_modal_scripts')
<script type="text/javascript">

    var cpm_audit_type_select = $("#create-project-form [name='audit_type']").makeSelectize({
        lookup : '{{ config('iapms.lookups.audit_type') }}'
    })[0].selectize;

    var cpm_group_select = $("#create-project-form [name='group']").makeSelectize({
        url: "{{ url('plan')}}/" +  selectedPlan + "/group",
        valueField: 'plan_group_id',
        labelField: 'plan_group_name',
        searchField: 'plan_group_name',
        selectedTemplate : function(item, escape){
            return '<div> '+item.plan_group_name +'</div>';
        }
    })[0].selectize;

    $('#create-project-form .input-daterange').datepicker({
        format: "M-dd-yyyy",
        // startDate: 'today',
        keyboardNavigation: false,
        autoclose : true,
    });

    $("#create-project-form").submit(function(){
        $(this).find('.has-error').removeClass('has-error');
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#create-project-form")[0].reset();
                $("#create-project-modal").modal('hide');
                project_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#create-project-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $("#show-create-project-modal").click(function(){
        $("#create-project-form").clearForm();
        $("#create-project-form").attr('action', "{{ url( 'plan-group/0/project')}}" );
        $.reloadURL( cpm_group_select, "{{ url('plan')}}/" +  selectedPlan + "/group" );
        $.sDelay(function(){
            $("#create-project-modal").modal('show');
        });
        return false;
    });

</script>
@endsection