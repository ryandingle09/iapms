<!-- Create Findings Modal -->
<div class="modal fade " data-backdrop="static" id="create-findings-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Fieldwork Audit Findings</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div id="create-findings-alert"></div>
                        <form class="form-horizontal" id="create-findings-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="business_process_step"> Business Process Step </label>
                                <div class="col-sm-8">
                                    <textarea name="business_process_step" class="business_process_step form-control v-only"></textarea>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="risk"> Risk </label>
                                <div class="col-sm-8">
                                    <textarea name="risk" class="risk form-control v-only"></textarea>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="recommendations"> Recommendation </label>
                                <div class="col-sm-8">
                                    <textarea name="recommendations" class="recommendations form-control v-only"></textarea>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="auditee_actions_taken"> Auditee Actions Taken </label>
                                <div class="col-sm-8">
                                    <textarea name="auditee_actions_taken" class="auditee_actions_taken form-control v-only"></textarea>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Findings Modal -->
@section('create_findings_modal_scripts')
<script type="text/javascript">
    //selectedScope came from views.dar.index
    //findings_datatable
    $(document).on("submit","#create-findings-form",function(){
        $(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            // beforeSend : sLoading(),
            success : function(response){
                $("#create-findings-modal").modal('hide');
                $("#create-findings-form")[0].reset();
                findings_datatable.ajax.reload( null, false );
                sSuccess(response.message);
            },
            error : function(xhr){
                $("#create-findings-form").modalFormErrorHandler(xhr);
            }
        });
        return false;
    });
</script>
@endsection