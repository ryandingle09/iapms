<div class="col-xs-12 mrgB15">
    <div class="table-header"> Detailed Audit Report </div>
</div>
<div class="col-xs-12 ">
	<div id="create-dar-form-alert"></div>
	<form class="form-horizontal scrollable" id="create-dar-form" action="" method="POST">
	    {{ csrf_field() }}
	    <input type="hidden" name="project_scope_id" value="">
	    <div class="form-group" > 
	        <div class="col-xs-6 text-left" for="description"> 
	            <a href="#" class="btn btn-info btn-sm btn-dar-view">
	                <i class="ace-icon fa fa-eye bigger-120"></i>
	                View
	            </a>
	            <a href="#" class="btn btn-success btn-sm btn-dar-send">
	                <i class="ace-icon fa fa-send bigger-120"></i>
	                Send
	            </a>
	        </div>
	        <div class="col-sm-6 text-right">
	            <button class="btn btn-info btn-sm">
	            	<i class="ace-icon fa fa-save bigger-120"></i>Apply
	            </button>
	            <a href="{{ url('fieldwork')}}" class="btn btn-warning btn-sm" data-dismiss="modal">
	                <i class="ace-icon fa fa-times bigger-120"></i>
	                Cancel
	            </a>
	        </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label align-left" for="subject"> Subject </label>
	        <div class="col-sm-4">
	            <input type="text" name="subject" class="subject form-control input-sm" value="{{ isset($details) ? $details->subject : '' }}" >
	        </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label align-left" for="scope"> Scope </label>
	        <div class="col-sm-10">
	            <textarea name="scope" class="scope form-control input-sm h-only" rows="3" >{{ isset($details) ? $details->scope : '' }}</textarea>
	        </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label align-left" for="objectives"> Objectives </label>
	        <div class="col-sm-10">
	            <textarea name="objectives" class="objectives form-control input-sm h-only" rows="3" >{{ isset($details) ? $details->objectives : '' }}</textarea>
	        </div>
	    </div>
	    <div class="form-group">
	        <label class="col-sm-2 control-label align-left" for="opinion"> Opinion/Suggestion </label>
	        <div class="col-sm-10">
	            <textarea name="opinion" class="opinion form-control input-sm h-only" rows="3" >{{ isset($details) ? $details->opinion : '' }}</textarea>
	        </div>
	    </div>
	    <div class="form-group" > 
	        <div class="col-sm-12 text-right">
	            <button class="btn btn-info btn-sm"><i class="ace-icon fa fa-save bigger-120"></i>
	            Apply</button>
	            <a href="{{ url('fieldwork')}}" class="btn btn-warning btn-sm" data-dismiss="modal">
	                <i class="ace-icon fa fa-times bigger-120"></i>
	                Cancel
	            </a>
	        </div>
	    </div>
	</form>
</div>
@section('tab_main_scripts')
<script type="text/javascript" src="../../js/jQuery.print.js"></script>
<script type="text/javascript">
    //selectedScope came from views.dar.index
    var dar_url = "{{ url('project-scope') }}/" + selectedScope + "/dar";

	$(document).on('submit','#create-dar-form',function(){
		$(".alert").remove();
        $.ajax({
            url : dar_url ,
            method : "POST",
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $.sSuccess(response.message);
            }
        }); 
		return false;
	});

	$(document).on('click','.btn-dar-view',function(){
		$.ajax({
            url : dar_url + "/layout",
            method : "GET",
            data : { _token : _token },
            beforeSend : sLoading(),
            success : function(response){
                swal.close();
                $.print(response.html);
            }
        });
		return false;
	});

	$(document).on('click','.btn-dar-send',function(){
		$.ajax({
	        url : dar_url + "/send",
	        method : "POST",
	        data : { _token : _token },
	        beforeSend : sLoading(),
	        success : function(response){
	            $.sSuccess(response.message);
	        }
	    });
		return false;		
	});
</script>
@endsection