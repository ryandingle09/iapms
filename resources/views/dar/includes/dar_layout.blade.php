<!DOCTYPE html>
<html>
<head>
	<title>&nbsp;</title>
	<link rel="stylesheet" href="{{ asset('css/pdf.css') }}" />
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="text-center box col-xs-12">INTERNAL AUDIT DIVISION</div>
			<div class="box col-xs-12">
				<table class="table1">
					<tr>
						<td>
							<div class="labelbox"> SUBJECT</div>
						</td>
						<td>
							<div class="box">
								{{ $dar->subject }}
							</div>
						</td>
						<td>
							<div class="labelbox">DATE:</div>
						</td>
						<td>
							<div class="box">
								{{ $dar->created_date }}
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div class="box col-xs-12">
				<table>
					<tr>
						<td style="width:20%;">
							<div class="labelbox">SCOPE</div>
						</td>
						<td style="width:80%;">
							<div class="box minh100">
								{{ $dar->scope }}
							</div>
						</td>
					</tr>
					<tr>
						<td style="width:20%;">
							<div class="labelbox">OBJECTIVES</div>
						</td>
						<td style="width:80%;">
							<div class="box minh100">
								{{ $dar->objectives }}
							</div>
						</td>
					</tr>
					<tr>
						<td style="width:20%;">
							<div class="labelbox">Opinion/Suggestion</div>
						</td>
						<td style="width:80%;">
							<div class="box minh100">
								{{ $dar->opinion }}
							</div>
						</td>
					</tr>
				</table>
				
			</div>
		</div>
		
	</div>

</body>
</html>