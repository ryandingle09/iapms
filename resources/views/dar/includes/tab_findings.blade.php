<div class="col-xs-12">
    <a class="btn btn-primary btn-sm btn-create" data-toggle="modal" href='#create-findings-modal'>
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>
</div>
<div class="col-xs-12">
    <div class="table-header"> Fieldwork Audit Findings </div>
</div>
<div class="col-xs-12 mrgB15">
    <table id="findings-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Sequence</th>
                <th >Business Process Step</th>
                <th >Risk</th>
                <th >Recommendation</th>
                <th >Auditee Actions Taken</th>
                <th >Delete</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@include('dar.includes.create_findings_modal')
@section('tab_findings_scripts')
<script type="text/javascript">

    //selectedScope came from views.dar.index
    var findings_url = "{{ url('project-scope') }}/" + selectedScope + "/finding";
    $("#create-findings-form").attr('action',findings_url);

    var findings_datatable = $("#findings-table").DataTable( {
        "bDestroy": true,
        ajax: findings_url + "/list",
        "processing": true,
        order: [[ 0, 'asc' ]],
        columns: [
            {data: "findings_seq"},
            {data: "business_process_step"},
            {data: "risk"},
            {data: "recommendations"},
            {data: "auditee_actions_taken"},
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+findings_url+'/'+data.project_scope_dar_sof_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });  

    $(document).on('click','#findings-table .delete',function(){
        $(this).deleteItemFrom(findings_datatable);
        return false;
    });
</script>
@yield('create_findings_modal_scripts')
@endsection