<div class="col-xs-12">
    <a class="btn btn-primary btn-sm btn-create" data-toggle="modal" href='#create-highlights-modal'>
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>
</div>
<div class="col-xs-12">
    <div class="table-header"> Fieldwork Audit Highlights </div>
</div>
<div class="col-xs-12 mrgB15">
    <table id="highlights-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Sequence</th>
                <th >Findings</th>
                <th >Risk/Effect</th>
                <th >Auditee Actions Taken</th>
                <th >Delete</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@include('dar.includes.create_highlights_modal')
@section('tab_highlights_scripts')
<script type="text/javascript">
    //selectedScope came from views.dar.index
    var hlUrl = "{{ url('project-scope')}}/"+selectedScope + "/highlight";
    $("#create-highlights-form").attr('action',hlUrl);
    var highlights_datatable = $("#highlights-table").DataTable( {
        "bDestroy": true,
        ajax: hlUrl + "/list",
        "processing": true,
        order: [[ 0, 'asc' ]],
        columns: [
            {data: "highlights_seq"},
            {data: "findings"},
            {data: "risk"},
            {data: "auditee_actions_taken"},
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+hlUrl+'/'+data.project_scope_dar_highlight_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });  

    $(document).on('click','#highlights-table .delete',function(){
        $(this).deleteItemFrom(highlights_datatable);
        return false;
    });

</script>
@yield('create_highlights_modal_scripts')
@endsection