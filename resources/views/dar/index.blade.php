@extends('layouts.app')
@section('styles')
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')
        <div class="page-content">
            @include('includes.content_header')
            <div id="alert-wrapper">
                @include('includes.alerts')
            </div>
        	<div class="col-xm-12">
            	<ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="main_tab">
                    <li class="active">
                        <a href="#main" data-toggle="tab" aria-expanded="true">Main</a>
                    </li>
                	<li >
                        <a href="#highlights" data-toggle="tab" aria-expanded="false">Highlights</a>
                    </li>
                    <li >
                        <a href="#findings" data-toggle="tab" aria-expanded="false">Findings</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- Main -->
                    <div id="main" class="tab-pane active">
                        <div class="row">
                           @include('dar.includes.tab_main')
                        </div>
                    </div>
                    <!-- /Main -->
                    <!-- Highlights -->
                    <div id="highlights" class="tab-pane">
                        <div class="row">
                           @include('dar.includes.tab_highlights')
                        </div>
                    </div>
                    <!-- /Highlights -->
                     <!-- Findings -->
                    <div id="findings" class="tab-pane">
                        <div class="row">
                           @include('dar.includes.tab_findings')
                        </div>
                    </div>
                    <!-- /Findings -->
                </div>
        	</div>
		</div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection
@section('footer_script')
<script type="text/javascript">
    var selectedScope = "{{ $scopeId }}";
</script>
@yield('tab_main_scripts')
@yield('tab_highlights_scripts')
@yield('tab_findings_scripts')
@endsection