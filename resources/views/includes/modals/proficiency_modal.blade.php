<!-- Create Proficiency Modal -->
<div class="modal fade " data-backdrop="static" id="proficiency-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Auditor's proficiency</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 mrgB15">
                        <div class="table-header"> Proficiency </div>
                        <table id="proficiency-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th >Proficiency Type</th>
                                    <th >Rate</th>
                                    <th >Remarks</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">              
                    <div class="col-xs-12 ">
                        <div class="form-row align-right" >
                            <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                <i class="ace-icon fa fa-times bigger-120"></i>
                                Close
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Proficiency Modal -->
@section('proficiency_scripts')
<script type="text/javascript">
    var proficiency_table = null;
    $(document).on('click',".proficiency",function(){
        auditor_id = $(this).data('id');
        proficiency_table = $("#proficiency-table").DataTable( {
            "bDestroy": true,
            ajax: "{{ url('auditor')}}/"+auditor_id+"/proficiency/list",
            "processing": true,
            order: [[ 1, 'asc' ]],
            columns: [
                { data: 'proficiency_type' },
                { data: 'proficiency_rate' },
                { data: 'remarks' }
            ]
        });
        $("#proficiency-modal").modal('show');
        return false;
    });
</script>
@endsection