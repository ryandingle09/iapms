<!-- Show MBPA details Modal -->
<div class="modal fade " data-backdrop="static" id="mbpa-details-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Main Business Proccess Details</h4>
            </div>
            <div class="modal-body">
                <form id="mbpa-details-form" class="form-horizontal">
                    <div class="form-group" >
                        <label class="col-sm-4 control-label align-left " for="main_bp_name"> MBP Name </label>
                        <div class="col-sm-8">
                            <input type="text" name="main_bp_name" class="form-control makelabel" value="">
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-4 control-label align-left " for="walk_thru_mandays"> WalkThru Mandays </label>
                        <div class="col-sm-8">
                            <input type="text" name="walk_thru_mandays" class="form-control makelabel" value="">
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-4 control-label align-left " for="planning_mandays"> Planning Mandays </label>
                        <div class="col-sm-8">
                            <input type="text" name="planning_mandays" class="form-control makelabel" value="">
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-4 control-label align-left " for="followup_mandays"> Followup Mandays </label>
                        <div class="col-sm-8">
                            <input type="text" name="followup_mandays" class="form-control makelabel" value="">
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-4 control-label align-left " for="discussion_mandays"> Discussion Mandays </label>
                        <div class="col-sm-8">
                            <input type="text" name="discussion_mandays" class="form-control makelabel" value="">
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-4 control-label align-left " for="draft_dar_mandays"> Draft Dar Mandays </label>
                        <div class="col-sm-8">
                            <input type="text" name="draft_dar_mandays" class="form-control makelabel" value="">
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-4 control-label align-left " for="final_report_mandays"> Final Report Mandays </label>
                        <div class="col-sm-8">
                            <input type="text" name="final_report_mandays" class="form-control makelabel" value="">
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-4 control-label align-left " for="wrap_up_mandays"> Wrap Up Mandays </label>
                        <div class="col-sm-8">
                            <input type="text" name="wrap_up_mandays" class="form-control makelabel" value="">
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="col-sm-4 control-label align-left " for="reviewer_mandays"> Reviewer Mandays </label>
                        <div class="col-sm-8">
                            <input type="text" name="reviewer_mandays" class="form-control makelabel" value="">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /Show MBPA details Modal -->
@section('mbpa_details_modal_scripts')
<script type="text/javascript">
    $(document).on('click','.btn-mbpa-details',function(){
        var mbpaid = $(this).data('mbpaid');
        var aeid = $(this).data('aeid');
        $.ajax({
            url  : "{{ url('aea') }}/" +aeid+ "/mbpa/" + mbpaid,
            method : "GET",
            success : function(response){;
                $("#mbpa-details-form").supply(response.data);
                $("#mbpa-details-modal").modal('show');
                $("#mbpa-details-form .makelabel").makeLabel();
            }
        })
        return false;
    })
</script>
@endsection