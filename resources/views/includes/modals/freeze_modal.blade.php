<div class="modal fade" data-backdrop="static" id="freeze-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Project Scope Freeze Details</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 mrgB15 left-panel">
                        <div class="table-header"> History </div>
                        <a href="" class="btn btn-success btn-sm" id="create-freeze">
                            <i class="ace-icon fa fa-plus"></i>
                            Create
                        </a>
                        <div class="freeze-history-wrapper scrollable">
                            
                        </div>
                    </div>
                    <div class="col-xs-4 mrgB15 right-panel" style="display: none;">
                        <div class="table-header mrgB15 "> Freeze Form </div>
                    	<form class="form-horizontal clearfix" action="" method="POST" id="freeze-form">
                            <div class="form-group">
                                <label class="col-sm-12 control-label align-left " for="effective_date_from"> Start Date </label>
                                <div class="col-sm-12">
                                    <input type="text" name="effective_date_from" class="form-control date-picker">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 control-label align-left " for="effective_date_to"> End Date </label>
                                <div class="col-sm-12">
                                    <input type="text" name="effective_date_to" class="form-control date-picker">
                                </div>
                            </div>
    		                <div class="form-group">
                                <label class="col-sm-12 control-label align-left " for="remarks"> Remarks </label>
    		                    <div class="col-sm-12">
    		                        <textarea name="remarks" class="form-control input-sm noresize scrollable" rows="4" placeholder="Remarks"></textarea>
    		                    </div>
    		                </div>
    		                <div class="form-group">
    		                	<div class="col-sm-12">
    		                        <div class="text-right">
    		                            <button class="btn btn-info btn-sm">
    		                                <i class="ace-icon fa fa-save bigger-120"></i>
    		                                Apply
    		                            </button>
    		                            <a href="#" class="btn btn-warning btn-sm" id="close-freeze-form">
    		                                <i class="ace-icon fa fa-times bigger-120"></i>
    		                                Cancel
    		                            </a>  
    		                        </div>
    		                    </div>
    		                </div>
    		            </form>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
@section('freeze_modal_scripts')
<script type="text/javascript">

    $('#freeze-form .input-daterange').datepicker({
        format: "M-dd-yyyy",
        keyboardNavigation: false,
        autoclose : true,
    });


    
    $(document).on("click",'.btn-freeze',function(){
        var id = $(this).data('id');
        $.loadFreezeHistory( id , function(){
            $("#freeze-modal").modal('show');
        })
        return false;
    });

    $("#create-freeze").click(function(){
        var id = $(this).data('id');
        var url = "{{ url('project-scope') }}/" + id + "/freeze";
        $(".alert").remove();
        $(".has-error").removeClass('has-error');
        $("#freeze-form")[0].reset();
        $("#freeze-form").attr('action', url ).attr('method', "POST");
        $.showFreezeForm(id);
        return false;
    })

    $(document).on('click','.freeze-history-wrapper .edit',function(){
        var id = $(this).data('id');
        var scopeId = $(this).data('scope-id');
        $(".alert").remove();
        $(".has-error").removeClass('has-error');
        $("#freeze-form")[0].reset();
        var url = "{{ url('project-scope') }}/" + scopeId + "/freeze/" + id;
        $.ajax({
            url : url,
            method : "GET",
            dataType: 'JSON',
            beforeSend : sLoading(),
            success : function(response){
                swal.close();
                // $.hideFreezeForm();
                // $.loadFreezeHistory( response.data.project_scope_id );
                // $.sSuccess(response.message);
                $("#freeze-form").attr('action', url )
                            .attr('method', "PUT")
                            .supply(response.data);
                $.showFreezeForm();
            }
        });

        return false;
    })

    $("#close-freeze-form").click(function(){
        $.hideFreezeForm();
        return false;
    });

    $.showFreezeForm = function(scopeId){
        var modal = $("#freeze-modal");
        modal.find(".modal-dialog").removeClass('modal-md').addClass('modal-lg');
        modal.find(".left-panel").removeClass('col-xs-12').addClass('col-xs-8');
        modal.find(".right-panel").show();
    }

    $.hideFreezeForm = function(scopeId){
        var modal = $("#freeze-modal");
        modal.find(".modal-dialog").removeClass('modal-lg').addClass('modal-md');
        modal.find(".left-panel").removeClass('col-xs-8').addClass('col-xs-12');
        modal.find(".right-panel").hide();
    }

    $.loadFreezeHistory = function(scopeId, callback){
        $('#create-freeze').data('id',scopeId);
        $.ajax({
            url : "{{ url('project-scope') }}/" + scopeId + "/freeze",
            method : "GET",
            data : {
                orderBy : "effective_date_from",
                sortedBy : "desc"
            },
            success : function(response){
                var html = '<div class="alert alert-info"> <strong> <i class="ace-icon fa fa-info"></i> </strong> No history found. <br> </div';
                if(response.data.length){
                    html = "";
                    $.each(response.data, function(k, v){
                        var btn = v.editable ? '<a href="#" class="edit" data-id="'+v.project_scope_freeze_id+'" data-scope-id="'+v.project_scope_id+'"> <i class="fa fa-edit"></i> </a>' : '' ;

                        html += '<div class="item"> '+btn+'  <b> Froze By </b> '+v.frozen_by+' <b> on</b>  '+v.freeze_date+'<br> <b> Date From </b> '+v.effective_date_from+'  <b> until </b> '+v.effective_date_to_alt+' <br> <b> Remarks :</b>'+v.remarks+'</div>';
                    });
                }
                $(".freeze-history-wrapper").html(html);

                if(callback != undefined ) callback(response);
                
            }
        });
    }

    $("#freeze-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            dataType: 'JSON',
            beforeSend : sLoading(),
            success : function(response){
                $.hideFreezeForm();
                $.loadFreezeHistory( response.data.project_scope_id );
                $.sSuccess(response.message);
            }
        });
        return false;
    })
</script>
@endsection