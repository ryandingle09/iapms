<div class="modal fade" data-backdrop="static" id="ae-details-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Auditable Entity Details</h4>
            </div>
            <div class="modal-body">
                <div class="profile-user-info profile-user-info-striped" style="text-align: center!important;">
                    <div class="profile-info-row">
                        <div class="profile-info-name">Group Type</div>
                        <div class="profile-info-name">Business Type</div>
                        <div class="profile-info-name">Entity Class</div>
                        <div class="profile-info-name">Branch</div>
                        <div class="profile-info-name">Department</div>
                    </div>
                    <div class="profile-info-row">
                        <div class="profile-info-value">--</div>
                        <div class="profile-info-value">--</div>
                        <div class="profile-info-value">--</div>
                        <div class="profile-info-value">--</div>
                        <div class="profile-info-value">--</div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@section('ae_details_modal_scripts')

@endsection