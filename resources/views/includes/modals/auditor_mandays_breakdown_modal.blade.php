<!-- Auditor Mandays Breakdown Modal -->
<div class="modal fade " data-backdrop="static" id="auditor-mandays-breakdown-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Auditor Mandays Breakdown</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div id="auditor-mandays-breakdown-alert"></div>
                        <form class="form-horizontal" id="auditor-mandays-breakdown-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div id="breakdown-wrapper">
                                
                            </div>
                            <div class="form-row align-right" >
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Close
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Auditor Mandays Breakdown Modal -->

@section('auditor_mandays_breakdown_modal_scripts')
<script type="text/javascript">
	$(document).on("click",".mandays_breakdown",function(){
        var mbAuditorId = $(this).data('id') ;
        var mbPlanId = $(this).data('plan_id') ;
		
		$.ajax({
            url : "{{ url('plan') }}/" + mbPlanId + "/auditor/" + mbAuditorId + "/mandays-breakdown",
            method : "GET",
            beforeSend : sLoading(),
            success : function(response){
                swal.close();
                var html = '<div class="alert alert-info"> <strong> <i class="ace-icon fa fa-info"></i> </strong> No alloted mandays found. <br> </div>'; 

                if(response.data.length > 0){
                    html = "";
                    $.each(response.data,function(k,v){
                        console.log();
                        html += '<div class="form-group" > <label class="col-sm-4 control-label align-left " for="main_bp_name"> '+v.auditor_type+' </label> <div class="col-sm-8"> <input type="text" name="main_bp_name" class="form-control makelabel" value="'+v.allotted_mandays+'"> </div> </div>'; 
                    });
                }
                
                $("#breakdown-wrapper").html(html);
                $("#auditor-mandays-breakdown-form .makelabel").makeLabel();
		        $("#auditor-mandays-breakdown-modal").modal('show');
            }
        })
		return false;
	})

</script>
@endsection