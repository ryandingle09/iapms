<div class="modal fade" data-backdrop="static" id="custom-measure-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Custom Measures</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method' => 'post', 'url' => isset($ae_id) ? route('auditable_entities.business_process.store', ['auditable_ent_id' => $ae_id]) : '', 'class' => 'form-horizontal', 'id' => 'frm-custom-measures']) !!}
                <input type="hidden" name="_method" value="put">
                <input type="hidden" name="bp_id" value="">
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Custom Measure</th>
                            <th>Value</th>
                            <th>Percentile Score</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                @if(isset($ae_id))
                <button type="button" class="btn btn-primary btn-custom-measure-save">Save changes</button>
                @endif
            </div>
        </div>
    </div>
</div>
<script type="text/template" id="custom-measure-template">
    <tr>
        <th class="cm-name"></th>
        <td class="cm-value">
            @if(isset($ae_id))
            <input type="number" name="cm_value[]" class="form-control cm-value-input" value="" step="1" required="required" >
            @else
            <input type="number" name="cm_value[]" class="form-control cm-value-input" value="" step="1" readonly="" >
            @endif
        </td>
        <th class="cm-score"></th>
    </tr>
</script>

@section('cm_scripts')
<script type="text/javascript">
    var $cm_modal    = $('#custom-measure-modal');
    var ae_id = '{{ isset($ae_id) ? $ae_id : '' }}';

    function getPercetileScores(ae_id, bp_id) {
        var cm_count = $('#frm-custom-measures').find('input').length;
        if( !cm_count ) return false;

        $.ajax({
            url: '{{url('auditable_entities')}}/'+ae_id+'/business_process/'+bp_id+'/custom_measures/percentile',
            method: "GET",
            data: $('#frm-custom-measures').find('input:not([name="_method"])').serialize(), // serializes all the form data
            beforeSend: function(){
                toggle_loading();
            },
            success : function(response) {
                if( response.success ) {
                    for (var cm in response.data) {
                        $('.cm_'+cm+'_percentile').html(response.data[cm]);
                    }
                }
                toggle_loading();
            }
        });
    }

    @if(isset($ae_id))
    // save custom measure
    $(document).on('click', '.btn-custom-measure-save', function() {
        swal({
            title: "Continue?",
            text: "You are about to update custom measures.",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, save it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(){
            $.ajax({
                url: '{{route('auditable_entities.business_process.cm.update', ['auditable_ent_id' => $ae_id])}}',
                method: "POST",
                data: $cm_modal.find('form').serialize(),
                success : function(data) {
                    if( data.success ) {
                        location.reload();
                    }
                }
            });
        });
        return false;
    });
    @endif

    function getCustomMeasures(entity_id, bp_id) {
        $.get('{{url('auditable_entities/master/custom_measures')}}/'+bp_id+'/'+entity_id, function(response){
            generateCustomMeasures(response.data, entity_id, bp_id);
        });
    }

    // custom measure
    $(document).on('click', '.btn-custom-measure', function() {
        var entity_id = ae_id || $(this).attr('data-ae_id');

        var bp_id = $(this).attr('data-id');
        var index = $(this).parent().parent();
        
        if( typeof $cm_datatable != "undefined" ) {
            var data =  $cm_datatable.row(index).data();
            console.log('cm', data);
            generateCustomMeasures(data, entity_id, bp_id);
        }
        else {
            getCustomMeasures(entity_id, bp_id);
        }

        $cm_modal.find('input[name="bp_id"]').val(bp_id);

        $cm_modal.modal('show');
        return false;
    });

    function generateCustomMeasures(data, ae_bp, bp_id) {
        var template = $('#custom-measure-template').html();
        var table = $cm_modal.find('table tbody');

        if( data != null ) {
            var actual_cm = data.actual['data'][0];
            for(var i=1; i <= {{config('iapms.custom_measure.count')}}; i++) {
                var name = 'cm_'+i+'_name';
                var value = 'cm_'+i+'_value';
                var score = 'cm_'+i+'_percentile';

                if( data[name]) {
                    table.append( template );
                    table.find('.cm-name:last').html(data[name]);
                    table.find('.cm-value:last > input').val(actual_cm[value]);
                    table.find('.cm-score:last').addClass(score);
                }
            }
        }

        if( table.html() == '' ) {
            table.append('<tr><td colspan="3">No custom measure available.</td></tr>'); // show message if no custom measure available
            $cm_modal.find('.btn-custom-measure-save').hide();
        }

        getPercetileScores(ae_bp, bp_id);
    }

    $cm_modal.on('hidden.bs.modal', function (e) {
        $cm_modal.find('table tbody').html('');
        $cm_modal.find('input[name="bp_id"]').val('');
        $cm_modal.find('.btn-custom-measure-save').show();
    });
</script>
@endsection