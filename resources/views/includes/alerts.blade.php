@if(!$errors->isEmpty())
<div class="alert alert-warning">
	<button type="button" class="close" data-dismiss="alert">
		<i class="ace-icon fa fa-times"></i>
	</button>
	
	@foreach ($errors->all() as $error)
    <strong> <i class="ace-icon fa fa-exclamation-triangle"></i> </strong>{{ $error }}<br/> 
    @endforeach
</div>
@endif
@if(Session::has('warning_message'))
<div class="alert alert-warning">
	<button type="button" class="close" data-dismiss="alert">
		<i class="ace-icon fa fa-times"></i>
	</button>
	 <strong> <i class="ace-icon fa fa fa-exclamation-triangle"></i> </strong>{{ Session::get('warning_message')}}
	<br>
</div>
@endif
@if(Session::has('danger_message'))
<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">
		<i class="ace-icon fa fa-times"></i>
	</button>
	 <strong> <i class="ace-icon fa fa-times"></i> </strong>{{ Session::get('danger_message')}}
	<br>
</div>
@endif
@if(Session::has('success_message'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">
		<i class="ace-icon fa fa-times"></i>
	</button>
	 <strong> <i class="ace-icon fa fa-check"></i> </strong>{{ Session::get('success_message')}}
	<br>
</div>
@endif
@if(Session::has('info_message'))
<div class="alert alert-info">
	<button type="button" class="close" data-dismiss="alert">
		<i class="ace-icon fa fa-times"></i>
	</button>
	 <strong> <i class="ace-icon fa fa-info"></i> </strong>{{ Session::get('info_message')}}
	<br>
</div>
@endif
<!--Set true to test the layout-->
@if(false) 
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">
		<i class="ace-icon fa fa-times"></i>
	</button>
	 <strong> <i class="ace-icon fa fa-check"></i> </strong>
	 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
	 tempor incididunt ut labore et dolore magna aliqua.
	<br>
</div>
@endif
<!--Test layout end-->

