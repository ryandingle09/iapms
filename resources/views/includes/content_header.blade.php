<div class="page-header">
    <h1>
        {{ isset($heading) ? $heading : ucwords( str_replace(['_', '-'], ' ', Request::segment(1)) ) }}
    </h1>
</div><!-- /.page-header -->