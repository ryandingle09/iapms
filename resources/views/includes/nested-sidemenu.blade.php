@foreach($menus as $item)
<li class="">
	@if($item->is_parent)
	<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa {{$item->icon}}"></i>
		<span class="menu-text">
			{{$item->name}}
		</span>

		<b class="arrow fa fa-angle-down"></b>
	</a>

	<b class="arrow"></b>

	<ul class="submenu">
		{!! menu()->getMenuNestableTemplate('includes.nested-sidemenu',$item->children,[],$allowed) !!}
	</ul>
	@else
	<a href="{{ url($item->url)}}">
		<i class="menu-icon fa {{$item->icon}}"></i>
		<span class="menu-text">
			{{$item->name}}
		</span>
	</a>

	<b class="arrow"></b>
	@endif
</li>
@endforeach