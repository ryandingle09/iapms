@section('styles')
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection

@section('footer_script')
    <script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
@endsection
