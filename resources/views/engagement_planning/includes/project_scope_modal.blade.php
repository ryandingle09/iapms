<!-- Create Projects Scope Modal -->
<div class="modal fade " data-backdrop="static" id="create-project-scope-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Project Scope</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-project-scope-alert"></div>
                        <form class="form-horizontal" id="create-project-scope-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="auditable_entity"> Auditable Entity </label>
                                <div class="col-sm-8">
                                    <select class="form-control input-sm auditable_entity" name="auditable_entity"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="main_business_process"> Main Business Process </label>
                                <div class="col-sm-8">
                                    <select class="form-control input-sm main_business_process" name="main_business_process"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-8">
                                    <div class="input-daterange input-group">
                                        <input type="text" name="target_start_date" class="form-control input-sm form-control target_start_date" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" name="target_end_date" class="form-control input-sm form-control target_end_date" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="location"> Audit Location </label>
                                <div class="col-sm-8">
                                    <input type="text" id="location" name="location" class="form-control input-sm basic-info-modal">
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Projects Scope Modal -->

<!-- Edit Projects Scope Modal -->
<div class="modal fade " data-backdrop="static" id="edit-project-scope-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Project Scope</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div id="edit-project-scope-alert"></div>
                        <form class="form-horizontal" id="edit-project-scope-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="auditable_entity_auditable_entity_name"> Auditable Entity </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control makelabel" name="auditable_entity_auditable_entity_name" >
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="main_bp_main_bp_name"> Main Business Process </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control makelabel" name="main_bp_main_bp_name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="auditable_entity_entity_head_name"> Auditee Head </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control makelabel" name="auditable_entity_entity_head_name" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="auditable_entity_contact_name"> Auditee Contact Person </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control makelabel" name="auditable_entity_contact_name">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="plan_budgeted_mandays"> Plan Budgeted Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="plan_budgeted_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="default_misc_mandays"> Default Miscellaneous Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="default_misc_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="default_apg_mandays"> Default APG Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="default_apg_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="default_total_mandays"> Default Total Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="default_total_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="allotted_miscellaneous_mandays"> Allotted Miscellaneous Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="allotted_miscellaneous_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="allotted_apg_mandays"> Allotted APG Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="allotted_apg_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="allotted_total_mandays"> Allotted Total Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="allotted_total_mandays" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="adjusted_severity_value"> Adjusted Severity Value </label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control makelabel" name="adjusted_severity_value" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-8">
                                    <div class="input-daterange input-group">
                                        <input type="text" name="target_start_date" class="form-control input-sm form-control target_start_date" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" name="target_end_date" class="form-control input-sm form-control target_end_date" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="project_scope_status"> Status </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control makelabel" name="project_scope_status" value="n/a">
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Projects Scope Modal -->

@section('project_scope_modal_scripts')
<script type="text/javascript">
    // project_scope_datatable from tab_scope
    


    var cpsm_entity_select = $("#create-project-scope-form .auditable_entity").selectize({
        valueField: 'auditable_entity_id',
        labelField: 'auditable_entity_name',
        searchField: 'auditable_entity_name',
        options: [],
        create: false,
        render: {
            option: function (item, escape) {
                return " <div>" +escape(item.auditable_entity_name) + "</div>";
            }
        },
        load: function (query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '{{ url( 'auditable_entities/lists' )}}?search=' + encodeURIComponent(query) + '&searchFields=auditable_entity_name%3Alike&entity_type=actual',
                type: 'GET',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback();
                }
            });
        },
        onChange: function(value) {
            if (!value.length) return;
            cpsm_mbp_select.clear();
            cpsm_mbp_select.disable();
            cpsm_mbp_select.load(function(callback) {
                $.ajax({
                    url: '{{ url( 'auditable_entities/master' )}}/'+value+'/business_processes/main?type=actual&json=true',
                    success: function(results) {
                        cpsm_mbp_select.enable();
                        callback(results);
                    },
                    error: function() {
                        callback();
                    }
                })
            });
        }
    })[0].selectize;

    

    var cpsm_mbp_select = $("#create-project-scope-form .main_business_process").selectize({
        valueField: 'ae_mbp_id',
        labelField: 'main_bp_name',
        searchField: 'main_bp_name',
        options: [],
        create: false,
        render: {
            option: function (item, escape) {
                return '<div>'+escape(item.main_bp_name) + '<div>';
            }
        }
    })[0].selectize;
    
    cpsm_mbp_select.disable();

    $('#create-project-scope-modal .input-daterange').datepicker({
        format: "M-dd-yyyy",
        startDate: 'today',
        keyboardNavigation: false,
        autoclose : true,
    });

    $('#edit-project-scope-modal .input-daterange').datepicker({
        format: "M-dd-yyyy",
        startDate: 'today',
        keyboardNavigation: false,
        autoclose : true,
    });

    $("#create-project-scope-form").submit(function(){
        $(this).find('.has-error').removeClass('has-error');
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#create-project-scope-form")[0].reset();
                $("#create-project-scope-modal").modal('hide');
                cpsm_entity_select.clear();
                cpsm_mbp_select.clear();
                project_scope_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#create-project-scope-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });


    $(document).on('click',"#project-scope-table .edit",function(){
        var url = $(this).attr('href');
        $.ajax({
            url : url,
            method : "GET",
            dataType : "json",
            success : function(response){
                $("#edit-project-scope-form").supply(response.data).attr('action',url );
                $("#edit-project-scope-form .makelabel").makeLabel();
                $("#edit-project-scope-modal").modal('show');
            }
        })
        return false;
    });

    $("#edit-project-scope-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : "PUT",
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#edit-project-scope-modal").modal('hide');
                project_scope_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#edit-project-scope-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });
    
</script>
@endsection