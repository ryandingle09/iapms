@if($group_by != "test_proc_table")
    @foreach($data as $item)
    <div class="apg-item">
        <div class="content">
            <a class="apg-toggle" href="" data-groupBy="{{ $item->next_field }}" data-keyword="{{ $item->title }}">
                @if($item->children)
                    <i class="ace-icon fa fa-chevron-up bigger-130"></i>
                @else
                    <i class="ace-icon fa fa-chevron-down bigger-130"></i>
                @endif
            </a>
            <span class="editable-title"
                data-id="{{ $item->project_scope_apg_id }}" 
                data-field="{{ $item->field }}"
            >{{ $item->title }}</span>
            <div class="pull-right action-buttons">
                @if($item->ycount > 0 )
                <a class="enabled_flag" href="#" data-id="{{ $item->project_scope_apg_id }}" data-field="{{ $item->field }}">
                    <i class="ace-icon fa fa-check-square bigger-130"></i>
                </a>
                @else
                <a class="enabled_flag" href="#" data-id="{{ $item->project_scope_apg_id }}" data-field="{{ $item->field }}">
                    <i class="ace-icon fa fa-check-square-o bigger-130"></i>
                </a>
                @endif
                <a class="create-apg" href="#" data-id="{{ $item->project_scope_apg_id }}" data-field="{{ $item->field }}">
                    <i class="ace-icon fa fa-plus bigger-130"></i>
                </a>
                <a class="red delete" href="" data-id="{{ $item->project_scope_apg_id }}" data-field="{{ $item->field }}">
                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                </a>
            </div>
        </div>
        @if($item->children)
        <div class="child on" >
            
                @include("engagement_planning.includes.editable_apg_recursive_template", [ 
                    'data' => $item->children , 
                    'group_by' => $item->next_field
                ])
        </div>
        @else
        <div class="child" ></div>
        @endif
    </div>
    @endforeach
@else
<div class="apg-table">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Test Seq.</th>
                <th >Test Procedure Narrative</th>
                <th >Audit Objective</th>
                <th >Auditor</th>
                <th >Mandays</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $item)
            <tr>
                <td>{{ $item->control_test_seq }}</td>
                <td>{{ $item->test_proc_narrative }}</td>
                <td>{{ $item->audit_objective}}</td>
                <td>{{ $item->auditor_full_name}}</td>
                <td>{{ $item->allotted_mandays}}</td>
                <td>
                    @if($item->enabled_flag == "Y")
                    <a class="enabled_flag" href="#" data-id="{{ $item->project_scope_apg_id }}" data-field="{{ $item->field }}">
                        <i class="ace-icon fa fa-check-square bigger-130"></i>
                    </a>
                    @else
                    <a class="enabled_flag" href="#" data-id="{{ $item->project_scope_apg_id }}" data-field="{{ $item->field }}">
                        <i class="ace-icon fa fa-check-square-o bigger-130"></i>
                    </a>
                    @endif
                    <a class="edit" href="#" data-id="{{ $item->project_scope_apg_id }}">
                        <i class="ace-icon fa fa-pencil bigger-130"></i>
                    </a>
                    <a class="red delete" href="#" data-id="{{ $item->project_scope_apg_id }}" data-field="{{ $item->field }}">
                        <i class="ace-icon fa fa-trash-o bigger-130"></i>
                    </a>

                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif


