<!--  APG Modal -->
<div class="modal fade " data-backdrop="static" id="project-scope-apg-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Audit Program Guide</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="project-scope-apg-alert"></div>
                        <form class="form-horizontal" id="project-scope-apg-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="bp_name"> Sub-Business Process Name </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-sm" name="bp_name"/>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="objective_name"> Sub-Business Process Objective </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-sm" name="objective_name"/>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="activity_narrative"> Sub-Business Process Step </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-sm" name="activity_narrative"/>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="risk_code"> Risk Name </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-sm" name="risk_code"/>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="control_name"> Control Name </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-sm" name="control_name"/>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="test_proc_narrative"> Test Proc Narrative </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-sm" name="test_proc_narrative"/>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="audit_objective"> Audit Objective </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-sm" name="audit_objective"/>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="auditor_id"> Auditor Name </label>
                                <div class="col-sm-8">
                                    <select class="form-control input-sm" name="auditor_id"></select>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="allotted_mandays"> Mandays </label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control input-sm" name="allotted_mandays"/>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- / APG Modal -->

@section('project_scope_apg_modal_scripts')
<script type="text/javascript">
    
    var psa_auditor_select = $("#project-scope-apg-form [name='auditor_id']").makeSelectize({
        url: "{{ url('auditor/show')}}",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    })[0].selectize;

    var selectedAPGTools = null;

    $(document).on('click','.create-apg',function(){

        psa_auditor_select.clear();
        psa_auditor_select.clearOptions();
        psa_auditor_select.load(function(callback) {
            $.ajax({
                url: '{{ url( 'project-scope/' )}}/'+selectedScope+'/auditor',
                success: function(response) {
                    callback(response.data);
                },
                error: function() {
                    callback();
                }
            })
        });


        selectedAPGTools = $(this);
        var id = $(this).data('id');
        var field = $(this).data('field');
        var url = "{{ url('project-scope') }}/" + selectedScope +"/apg";
        $("#project-scope-apg-form")[0].reset();
        $("#project-scope-apg-form").attr('action',url).attr('method','POST');
        if(id == undefined){
            $("#project-scope-apg-modal").modal('show');
        }else{
            $.ajax({
                url : url + "/" + id,
                method : "GET",
                beforeSend : sLoading(),
                data : {
                    field : field
                },
                success : function(response){
                    swal.close();
                    $("#project-scope-apg-form").supply(response.data);
                    $("#project-scope-apg-modal").modal('show');
                }
            })
        }
        return false;
    });

    $("#project-scope-apg-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#project-scope-apg-form")[0].reset();
                $("#project-scope-apg-modal").modal('hide');
                // cpsam_select.clear();
                $.loadFullView('editor_apg');
                
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#project-scope-apg-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });


    $(document).on('click','.apg-table .edit',function(){
        sLoading();
        var id = $(this).data('id');
        psa_auditor_select.clear();
        psa_auditor_select.clearOptions();
        psa_auditor_select.load(function(callback) {
            $.ajax({
                url: '{{ url( 'project-scope/' )}}/'+selectedScope+'/auditor',
                success: function(response) {
                    callback(response.data);
                },
                error: function() {
                    callback();
                }
            })
        });

        setTimeout(function(){
            $(".alert").remove();
            var url = "{{ url('project-scope') }}/" + selectedScope + "/apg/" + id;
            $("#project-scope-apg-form")[0].reset();
            $("#project-scope-apg-form").attr('action',url).attr('method','PUT');
            $.ajax({
                url : url,
                method : "GET",
                success : function(response){
                    swal.close();
                    var data = response.data;
                    psa_auditor_select.setValue(data.auditor_id);
                    delete data['auditor_id'];
                    $("#project-scope-apg-form").supply(response.data);
                    $("#project-scope-apg-modal").modal('show');
                }
            });
        },1000);
        return false;
    });

</script>
@endsection