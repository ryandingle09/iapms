
<div class="col-xs-12 mrgB15">
    <div class="table-header"> Audit Program Guide </div>
</div>

<div class="col-xs-12 align-left" id="main-apg-wrapper">

    <a class="btn btn-primary btn-sm create-apg" >
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>

    <a class="btn btn-primary btn-sm" id="enabled-toggle" >
        <i class="ace-icon fa fa-check-square bigger-120"></i>
        Enabled Toggle
    </a>

    <a class="btn btn-success btn-sm" id="apg-refresh" >
        <i class="ace-icon fa fa-refresh bigger-120"></i>
        Refresh
    </a>
    
    <div class="apg-wrapper-header"> Audit Program Guide </div>
    <div class="apg-wrapper scrollable" id="apg-wrapper" data-groupBy="bp_name" data-keyword="">
        
    </div>
</div>

@include('engagement_planning.includes.project_scope_apg_modal')
@section('tab_apg_scripts')
<script type="text/javascript">
    var project_scope_apg_datatable = null;
    $(document).on('click',"#_engagement_tab a[href='#apg']",function(){
        if(selectedScope){
            $.loadFullView('editor_apg');
        }else{
            $.sInfo("You have to select project scope to open this tab.");
        }
    });

    $.loadFullView = function(type){
        $("#main-apg-wrapper").LoadingOverlay("show");
        $.ajax({
            url : "{{ url('project-scope') }}/" + selectedScope + "/apg/full-view",
            method : "GET",
            data : { type : type },
            success : function(response){
                var html = '<div class="alert alert-info"> <strong> <i class="ace-icon fa fa-info"></i> </strong> No APG Found. <br> </div>';
                if(response.data.length){
                    html = response.html;
                }
                $("#apg-wrapper").html(html);    
                $("#main-apg-wrapper").LoadingOverlay("hide");
            }
        });
    };

    $("#apg-refresh").click(function(){
        if( $("#enabled-toggle").hasClass('btn-primary') ){
            $.loadFullView('editor_apg');
        }else{
            $.loadFullView('enabled_apg');
        }
        return false;
    })

    $("#enabled-toggle").click(function(){
        if( $(this).hasClass('btn-primary') ){
            $.loadFullView('enabled_apg');
            $(".apg-wrapper-header").html("Enabled APG List");
            $(this).removeClass('btn-primary').addClass('btn-success');
        }else{
            $.loadFullView('editor_apg');
            $(".apg-wrapper-header").html("APG in Editor Mode");
            $(this).removeClass('btn-success').addClass('btn-primary');
        }
    });

    $.loadRootEnabled = function(){
        $("#main-apg-wrapper").LoadingOverlay("show");
        $("#apg-wrapper").loadEnabledApg(function(response){
            $("#apg-wrapper").html(response.html);
            $("#main-apg-wrapper").LoadingOverlay("hide");
        });
    };

    $.fn.loadEditorPartial = function( callback ){
        var groupBy = $(this).attr('data-groupBy');
        var keyword = $(this).attr('data-keyword');
        $.ajax({
            url : "{{ url('project-scope') }}/" + selectedScope + "/apg/partial-view",
            method : "GET",
            data : {
                groupBy : groupBy,
                keyword : keyword
            },
            success : function(response){
                callback(response);
            }
        });
    };

    $(document).on('click',".apg-toggle",function(){
        if( $(this).children('.ace-icon').hasClass('fa-chevron-down') ){
            var elem = $(this);
            $("#main-apg-wrapper").LoadingOverlay("show");
            elem.loadEditorPartial(function(response){
                elem.parent().next().html(response.html).addClass('on');
                elem.children('.ace-icon').removeClass('fa-chevron-down').addClass('fa-chevron-up');
                $("#main-apg-wrapper").LoadingOverlay("hide");
            });
        }else{
            $(this).parent().next().removeClass('on').html("");
            $(this).children('.ace-icon').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        }
        return false;
    });

    $(document).on('click','.apg-item .delete',function(){
        var elem = $(this);
        var id = elem.data('id');
        var field = elem.data('field');
        $.sWarning("Are you sure you want to delete this item? It will be cascaded to all of its child.",function(){
            $.ajax({
                url : "{{ url('project-scope') }}/" + selectedScope +"/apg/recursive-destroy",
                method : "DELETE",
                data : {
                    id : id,
                    field : field,
                },
                success : function(response){
                    $.loadFullView('editor_apg');
                    $.sSuccess(response.message);
                }
            });
        });
        return false;
    });

    $(document).on('click','.apg-table .delete',function(){
        var elem = $(this);
        var id = elem.data('id');
        $.sWarning("Are you sure you want to delete this APG?",function(){
            $.ajax({
                url : "{{ url('project-scope') }}/" + selectedScope +"/apg/" + id,
                method : "DELETE",
                success : function(response){
                    $.loadFullView('editor_apg');
                    $.sSuccess(response.message);
                }
            });
        });
        return false;
    });

    $(document).on('click','.enabled_flag',function(){
        var elem = $(this);
        var id = $(this).data('id');
        var field = $(this).data('field');
        var status = $(this).find('.ace-icon').hasClass('fa-check-square') ? "N" : "Y";
        $.ajax({
            url :  "{{ url('project-scope') }}/" + selectedScope +"/apg/" + id +"/enabled-flag",
            method : "PUT",
            beforeSend : sLoading(),
            data : {
                field : field,
                status : status
            },
            success : function(response){
                swal.close();
                if(status == "Y"){
                    elem.parent().parent().parent().find('.enabled_flag').find('.ace-icon').removeClass('fa-check-square-o').addClass('fa-check-square');
                }else{
                    elem.parent().parent().parent().find('.enabled_flag').find('.ace-icon').removeClass('fa-check-square').addClass('fa-check-square-o');
                }
                $.gSuccess(response.message);
            }
        });
        return false;
    });

    $(document).on('click','.editable-title',function(){
        var elem = $(this);
        var id = $(this).data('id');
        var field = $(this).data('field');
        var value = $(this).html();
        elem.removeClass('editable-title').addClass('submitable-title');
        var w = value.length  * 8;
        var html = '<input type="text" value="'+value+'" style="width:'+w+'px">';
        elem.html(html);
        return false;
    });

    $(document).on('keydown','.submitable-title input',function(ev){
        if(ev.which === 13) {
            var parentElem = $(this).parent();
            var id = parentElem.data('id');
            var field = parentElem.data('field');
            var value = $(this).val();
            parentElem.removeClass('submitable-title').addClass('editable-title');
            parentElem.html($(this).val());
            $.saveColumnValue( id, field, value);

        }
    });

    $.saveColumnValue = function( id, field, value ){
        $.ajax({
            url : "{{ url('project-scope/0/apg') }}/" +id+ "/update-header",
            method : "PUT",
            data : {
                field : field ,
                value : value
            },
            success : function(response){
                $.gSuccess(response.message);
            }
        });
    }

</script>
@yield('project_scope_apg_modal_scripts')
@endsection
