<!-- Create Projects Scopes Auditor Modal -->
<div class="modal fade " data-backdrop="static" id="create-project-scope-auditor-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Project Scope Auditor</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-project-scope-auditor-alert"></div>
                        <form class="form-horizontal" id="create-project-scope-auditor-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="auditor"> Auditor </label>
                                <div class="col-sm-8">
                                    <select class="form-control input-sm" name="auditor"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="role"> Role </label>
                                <div class="col-sm-8">
                                    <select class="form-control input-sm" name="role"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="allotted_misc_mandays"> Allotted Miscellaneous Mandays </label>
                                <div class="col-sm-8">
                                    <input type="text"  class="form-control input-sm" name="allotted_misc_mandays">
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Close
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Projects Scopes Auditor Modal -->

<!-- Edit Projects Scopes Auditor Modal -->
<div class="modal fade " data-backdrop="static" id="edit-project-scope-auditor-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Project Scope Auditor</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="edit-project-scope-auditor-alert"></div>
                        <form class="form-horizontal" id="edit-project-scope-auditor-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="auditor_full_name"> Auditor </label>
                                <div class="col-sm-8">
                                    <input type="text" name="auditor_full_name" class="form-control input-sm makelabel">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="auditor_type"> Role </label>
                                <div class="col-sm-8">
                                    <input type="text" name="auditor_type" class="form-control input-sm makelabel">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="allotted_misc_mandays"> Allotted Miscellaneous Mandays </label>
                                <div class="col-sm-8">
                                    <input type="text"  class="form-control input-sm" name="allotted_misc_mandays">
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Close
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Projects Scopes Auditor Modal -->

@section('project_scope_auditor_modal_scripts')
<script type="text/javascript">
    // project_scope_datatable came from tab_scope

    var cpsam_auditor_type_select = $("#create-project-scope-auditor-form [name=role]").makeSelectize({
        lookup : '{{ config('iapms.lookups.person_project_role') }}'
    })[0].selectize;

    var cpsam_auditor_select = $("#create-project-scope-auditor-form [name=auditor]").makeSelectize({
        url: "{{ url('auditor/show')}}",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    })[0].selectize;

    $("#create-project-scope-auditor-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#create-project-scope-auditor-form")[0].reset();
                cpsam_auditor_type_select.clear();
                cpsam_auditor_select.clear();
                project_scope_auditor_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#create-project-scope-auditor-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on('click','#show-create-project-scope-auditor-modal',function(){
        $(".alert").remove();
        $(".has-error").removeClass('has-error');
        $("#create-project-scope-auditor-form")[0].reset();
        cpsam_auditor_type_select.clear();
        cpsam_auditor_select.clear();
        cpsam_auditor_select.clearOptions();
        cpsam_auditor_select.load(function(callback) {
            $.ajax({
                url: '{{ url( 'project' )}}/'+selectedProject+'/auditor',
                success: function(response) {
                    callback(response.data);
                },
                error: function() {
                    callback();
                }
            })
        });
        $("#create-project-scope-auditor-modal").modal('show');
        return false;
    });

    $(document).on('click','#project-scope-auditor-table .edit',function(){
        var url = $(this).attr('href');
        $.ajax({
            url: url,
            method : "GET",
            success: function(response) {
                $("#edit-project-scope-auditor-form").attr('action',url);
                $("#edit-project-scope-auditor-form").supply(response.data);
                $("#edit-project-scope-auditor-form .makelabel").makeLabel();
                $("#edit-project-scope-auditor-modal").modal('show');
            }
        })
        return false;
    });

    $("#edit-project-scope-auditor-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : "PUT",
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#edit-project-scope-auditor-form")[0].reset();
                $("#edit-project-scope-auditor-modal").modal('hide');
                project_scope_auditor_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#edit-project-scope-auditor-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    }); 
</script>
@endsection