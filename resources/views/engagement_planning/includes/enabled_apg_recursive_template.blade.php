@if($group_by != "test_proc_table")
	@foreach($data as $item)
	<div class="apg-item">
	    <div class="content">
	        {{ $item->title }}
	    </div>
	    <div class="child on" >
	        @if($item->children)
	            @include("engagement_planning.includes.enabled_apg_recursive_template", [ 
	            	'data' => $item->children , 
	            	'group_by' => $item->next_field
	            ])
	        @endif
	    </div>
	</div>
	@endforeach
@else
<div class="apg-table">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Test Seq.</th>
                <th >Test Procedure Narrative</th>
                <th >Audit Objective</th>
                <th >Auditor</th>
                <th >Mandays</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $item)
            <tr>
                <td>{{ $item->control_test_seq }}</td>
                <td>{{ $item->test_proc_narrative }}</td>
                <td>{{ $item->audit_objective}}</td>
                <td>{{ $item->auditor_full_name}}</td>
                <td>{{ $item->allotted_mandays}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endif

