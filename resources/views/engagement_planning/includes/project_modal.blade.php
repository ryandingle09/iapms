<!-- Create Project Modal -->
<div class="modal fade " data-backdrop="static" id="create-project-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Project</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-project-alert"></div>
                        <form class="form-horizontal" id="create-project-form" action="{{ route('project.store') }}" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="project_name"> Project Name </label>
                                <div class="col-sm-10">
                                    <input type="text" name="project_name" class="form-control input-sm project_name" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_type"> Audit Type </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm" name="audit_type"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="project_status"> Status </label>
                                <div class="col-sm-10">
                                    <input type="text" name="project_status" class="form-control input-sm project_status" value="New" readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-10">
                                    <div class="input-daterange input-group">
                                        <input type="text" name="target_start_date" class="form-control input-sm form-control target_start_date" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" name="target_end_date" class="form-control input-sm form-control target_end_date" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="project_head"> Project Head </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm project_head auditor" name="project_head"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="project_remarks"> Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm" name="project_remarks" id="project_remarks" rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="audit_year"> Audit Year </label>
                                <div class="col-sm-10">
                                    <input type="text" name="audit_year" class="form-control input-sm audit_year" value="">
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Project Modal -->

<!-- Edit Project Modal -->
<div class="modal fade " data-backdrop="static" id="edit-project-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Project</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div id="edit-project-alert"></div>
                        <form class="form-horizontal" id="edit-project-form" action="" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="#" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="project_name"> Project Name </label>
                                <div class="col-sm-4">
                                    <input type="text" name="project_name" id="project_name" class="form-control input-sm" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_type"> Audit Type </label>
                                <div class="col-sm-4">
                                    <select class="form-control input-sm" name="audit_type" id=""></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="project_status"> Status </label>
                                <div class="col-sm-4">
                                    <input type="text" name="project_status" class="form-control input-sm project_status makelabel">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-4">
                                    <div class="input-daterange input-group">
                                        <input type="text" id="target_start_date" name="target_start_date" class="form-control input-sm form-control" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" id="target_end_date" name="target_end_date" class="form-control input-sm form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="project_head"> Project Head </label>
                                <div class="col-sm-4">
                                    <select class="form-control input-sm project_head" name="project_head"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="project_remarks"> Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm" name="project_remarks" id="project_remarks" rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="approver_fullname"> Approved By </label>
                                <div class="col-sm-4">
                                    <input type="text" name="approver_fullname" class="form-control input-sm makelabel" value="n/a">
                                </div>
                                <label class="col-sm-2 control-label align-left" for="approved_date"> Approved Date </label>
                                <div class="col-sm-4">
                                    <input type="text" name="approved_date" class="form-control input-sm makelabel" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="approver_remarks"> Approver Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm approver_remarks makelabel" name="approver_remarks" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="plan_budgeted_mandays"> Plan Budgeted Mandays </label>
                                <div class="col-sm-3">
                                    <input type="text" name="plan_budgeted_mandays" class="form-control input-sm makelabel">
                                </div>
             
                                <label class="col-sm-3 control-label align-left" for="allotted_miscellaneous_mandays"> Allotted Miscellaneous Mandays </label>
                                <div class="col-sm-3">
                                    <input type="text" name="allotted_miscellaneous_mandays" class="form-control input-sm makelabel">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="allotted_apg_mandays"> Allotted APG Mandays </label>
                                <div class="col-sm-3">
                                    <input type="text" name="allotted_apg_mandays" class="form-control input-sm makelabel">
                                </div>
               
                                <label class="col-sm-3 control-label align-left" for="allotted_total_mandays"> Allotted Total Mandays </label>
                                <div class="col-sm-3">
                                    <input type="text" name="allotted_total_mandays" class="form-control input-sm makelabel">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="annual_plan_plan_project_name"> Annual Audit Plan Project Name </label>
                                <div class="col-sm-3">
                                    <input type="text" name="annual_plan_plan_project_name" class="form-control input-sm makelabel" >
                                </div>
                                <label class="col-sm-3 control-label align-left" for="annual_plan_scopes_total_budgeted_mandays"> Annual Plan Budgeted Mandays </label>
                                <div class="col-sm-3">
                                    <input type="text" name="annual_plan_scopes_total_budgeted_mandays" class="form-control input-sm makelabel">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="annual_plan_target_start_date"> Annual Audit Plan Target Date Start </label>
                                <div class="col-sm-3">
                                    <input type="text" name="annual_plan_target_start_date" class="form-control input-sm makelabel">
                                </div>
                                <label class="col-sm-3 control-label align-left" for="annual_plan_target_end_date"> Annual Audit Plan Target Date End </label>
                                <div class="col-sm-3">
                                    <input type="text" name="annual_plan_target_end_date" class="form-control input-sm makelabel">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_year"> Audit Year </label>
                                <div class="col-sm-4">
                                    <input type="text" name="audit_year"  class="form-control input-sm">
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Project Modal -->

@section('project_modal_scripts')
<script type="text/javascript">
    //project_datatable came from engagement_planning.tab_projects

    var cpm_audit_type_select = $("#create-project-form [name='audit_type']").makeSelectize({
        lookup : '{{ config('iapms.lookups.audit_type') }}'
    })[0].selectize;

    var cpm_project_head_select = $("#create-project-form [name='project_head']").makeSelectize({
        url: "{{ url('auditor/show')}}",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    })[0].selectize;

    var epm_audit_type_select = $("#edit-project-form [name='audit_type']").makeSelectize({
        lookup : '{{ config('iapms.lookups.audit_type') }}'
    })[0].selectize;

    var epm_project_head_select = $("#edit-project-form [name='project_head']").makeSelectize({
        url: "{{ url('auditor/show')}}",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    })[0].selectize;


    $('#create-project-form .input-daterange').datepicker({
        format: "M-dd-yyyy",
        keyboardNavigation: false,
        autoclose : true,
    });


    $("#create-project-form").submit(function(){
        $(this).find('.has-error').removeClass('has-error');
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                cpm_audit_type_select.clear();
                cpm_audit_type_select.clear();
                $("#create-project-form")[0].reset();
                $("#create-project-modal").modal('hide');
                project_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#create-project-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on("click","#project-table .edit",function(){
        $(".alert").remove();
        $("#edit-project-form")[0].reset();
        epm_audit_type_select.clear();
        epm_project_head_select.clear();
        var url = $(this).attr('href');
        $.ajax({
            url : url,
            method : "GET",
            beforeSend : sLoading(),
            success : function(response){
                data = response.data;
                swal.close();

                epm_audit_type_select.setValue(data.audit_type);
                delete data['audit_type']; 
                epm_project_head_select.setValue(data.project_head);
                delete data['project_head'];

                $("#edit-project-form").supply(data).attr('action',url);
                $('#edit-project-form .input-daterange [name="target_start_date"]').datepicker('update', data.target_start_date);
                $('#edit-project-form .input-daterange [name="target_end_date"]').datepicker('update', data.target_end_date);
                $("#edit-project-form .makelabel").makeLabel();
                $("#edit-project-modal").modal('show');
            }
        });
        return false;
    });

    $('#edit-project-form .input-daterange').datepicker({
        format: "M-dd-yyyy",
        // startDate: 'today',
        keyboardNavigation: false,
        autoclose : true,
    }); 

    $("#edit-project-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#edit-project-modal").modal('hide');
                $.sSuccess(response.message);
                project_datatable.ajax.reload( null, false );
            },
            error : function(xhr){
                $("#edit-project-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

</script>
@endsection