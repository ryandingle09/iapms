<!-- Create Project Modal -->
<div class="modal fade " data-backdrop="static" id="cfaap-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Select Project to copy from Annual Audit Plan</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 mrgB15">
                        <div class="table-header"> Annual Audit Plan Projects </div>
                        <table id="cfaap-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th >Project Name</th>
                                    <th >Audit Type</th>
                                    <th >Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Project Modal -->
@section('show_cfaap_modal_scripts')
<script type="text/javascript">

    var cfaap_datatable = $('#cfaap-table').DataTable( {
        processing : true,
        order: [[ 1, 'asc' ]],
        columns: [
            { data: "plan_project_name" },
            { data: "audit_type" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" data-id="'+data.plan_project_id+'" class="copy"><i class="fa fa-copy" title="Copy" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });


    $("#show-cfaap-modal").click(function(){
        cfaap_datatable.ajax.url( "{{ url('plan/0/project/list') }}" ).load();
        $("#cfaap-modal").modal('show');
        return false;
    })

    $(document).on('click','#cfaap-table .copy',function(){
        var plan_project_id = $(this).data('id');
        $.sWarning("Are you sure you want to copy this project?",function(){
            $.ajax({
                url : "{{ url('project/cfaap') }}",
                method : "POST",
                dataType : "json",
                data : {
                    plan_project_id : plan_project_id
                },
                success : function(response){
                    project_datatable.ajax.reload( null, false );
                    $.sSuccess(response.message);
                }
            });
        });
        return false;
    })
</script>
@endsection