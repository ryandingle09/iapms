<!-- Annual Audit Plan Projects Modal -->
<div class="modal fade " data-backdrop="static" id="aapp-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Annual Audit Plan Project</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 mrgB15 ">
                        <div class="row">
                            <div class="col-sm-4 " for="description"> 
                                <label class="control-label">Project Name: </label>
                                <div class="input-group col-sm-12">
                                    <input type="text" class="form-control input-sm" placeholder="Search for...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-secondary" type="button" style="height: 30px;">Go!</button>
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-8 text-right" for="description"> 
                                <a href="{{route('additional_information')}}" class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </a>
                                <a href="{{route('additional_information')}}" class="btn btn-warning btn-sm">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 mrgB15">
                        <div class="table-header"> Project Engagement </div>
                    </div>
                    <div class="col-xs-12">
                        <a class="btn btn-primary btn-sm btn-create" data-toggle="modal" href='#additional-info-modal'>
                            <i class="ace-icon fa fa-plus bigger-120"></i>
                            Create
                        </a>
                    </div>
                    <div class="col-xs-12 mrgB15">
                        <table id="additional-info-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th >Project Name</th>
                                    <th >Audit Type</th>
                                    <th >Status</th>
                                    <th >Copy From</th>
                                    <th class="text-center" width="10%">Delete</th>
                                </tr>
                            </thead>

                            <tbody>
                                @for($i = 0; $i < 10;$i++)
                                <tr>
                                    <td> Sample Data</td>
                                    <td> Sample Data</td>
                                    <td> Sample Data</td>
                                    <td> <a href="" class="copy_overlay">Sample Data </a></td>
                                    <td class="text-center">
                                        <a href="#" class="btn-delete delete" title="delete" rel="tooltip">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>
                                </tr>
                                @endfor
                            </tbody>
                        </table>
                    </div>
                    <div class="col-xs-12 mrgB15 ">
                        <div class="row">
                            <div class="col-sm-12 text-right" for="description"> 
                                <a href="{{route('additional_information')}}" class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </a>
                                <a href="{{route('additional_information')}}" class="btn btn-warning btn-sm">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Annual Audit Plan Projects Modal -->