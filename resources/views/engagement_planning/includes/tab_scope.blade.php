<div class="col-xs-12 mrgB15">
    <div class="table-header"> Project Scopes </div>
</div>
<div class="col-xs-12">
    <a class="btn btn-primary btn-sm btn-create" data-toggle="modal" href='#create-project-scope-modal'>
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>
</div>
<div class="col-xs-12 mrgB15">
    <table id="project-scope-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Auditable Entity IDL</th>
                <th >Main Business Process Name</th>
                <th >Plan Budgeted Mandays</th>
                <th >Default Total Mandays</th>
                <th >Allotted Total Mandays</th>
                <th >Approval Status</th>
                <th >Freeze Status</th>
                <th class="text-center" width="80">Actions</th>
            </tr>
        </thead>

        <tbody></tbody>
    </table>
</div>
<div class="col-xs-12 mrgB15">
    <div class="table-header"> Project Scope - Auditors </div>
</div>
<div class="col-xs-12">
    <a class="btn btn-primary btn-sm" id='show-create-project-scope-auditor-modal' disabled="disabled">
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>
</div>
<div class="col-xs-12 mrgB15">
    <table id="project-scope-auditor-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Auditor Name</th>
                <th >Role</th>
                <th >Plan Budgeted Mandays</th>
                <th >Allotted Miscellaneous Mandays</th>
                <th >Allotted APG Mandays</th>
                <th >Allotted Total Mandays</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
</div>

@include('engagement_planning.includes.project_scope_modal')
@include('engagement_planning.includes.project_scope_auditor_modal')
@include('engagement_planning.includes.project_scope_iom_modal')
@include('includes.modals.custom_measures')
@include('includes.modals.freeze_modal')
@section('tab_scope_scripts')
<script type="text/javascript">
    // selectedProject initialized in index
    // selectedScope initialized in index 

    var project_scope_datatable = $("#project-scope-table").DataTable( {
        "bDestroy": true,
        "processing": true,
        order: [[ 1, 'asc' ]],
        createdRow: function( row, data, dataIndex ) {
            if(selectedScope == data.project_scope_id){
                $( row ).addClass('selected-row');
            }
            $( row ).attr('data-id', data.project_scope_id);
        },
        columns: [
            { data: "auditable_entity.auditable_entity_name" },
            { data: "main_bp.main_bp_name" },
            { data: "plan_budgeted_mandays" },
            { data: "default_total_mandays" },
            { data: "allotted_total_mandays" },
            { data: "project_scope_status" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return'<a href="#" data-id="'+data.project_scope_id+'" class="btn-freeze" title="Click to view Freeze History" rel="tooltip"> '+data.freeze_status+'</a>';
                }
            },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var iom_button = "";
                    // project scope id will be set if iom is empty
                    var iom_url = data.project_scope_id; 
                    if(selectedProjectStatus == 'Approved'){
                        if(data.iom != null){
                            // full link will be set if iom is not empty
                            iom_url = "{{ url('project-scope')}}/" + data.project_scope_id +"/iom/" + data.iom.project_scope_iom_id;
                        }
                        iom_button = '<a href="'+iom_url+'" data-id="'+data.project_scope_id+'" class="btn-generate-iom" ><i class="fa fa-envelope text-primary" title="Generate IOM" rel="tooltip"></i></a>';
                    }
                    return '<a href="{{ url('project')}}/'+selectedProject+'/scope/'+data.project_scope_id+'" class="edit" ><i class="fa fa-pencil text-primary" title="Edit" rel="tooltip"></i></a>  <a href="#" class="btn-custom-measure" data-ae_id="'+data.auditable_entity_id+'" data-id="'+data.main_bp.master_ae_mbp_id+'" ><i class="fa fa-dashboard text-primary" title="Custom Measures" rel="tooltip"></i></a>     '+iom_button+'    <a href="" class="btn-mbpa-details" data-aeid="'+data.auditable_entity_id+'" data-mbpaid="'+data.ae_mbp_id+'"><i class="fa fa-sitemap text-primary" title="" rel="tooltip" data-original-title="Details"></i></a>    <a href="{{ url('project-scope') }}/'+data.project_scope_id+'/apg/populate" class="btn-generate-apg"><i class="fa fa-database text-primary" title="Populate APG" rel="tooltip" data-original-title="Populate APG"></i></a>    <a href="'+scope_url+'/'+data.project_scope_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });

    var project_scope_auditor_datatable = $("#project-scope-auditor-table").DataTable( {
        "bDestroy": true,
        "processing": true,
        order: [[ 1, 'asc' ]],
        columns: [
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    return data.auditor.full_name;
                }
            },
            { data: "auditor_type" },
            { data: "plan_budgeted_mandays" },
            { data: "allotted_misc_mandays" },
            { data: "allotted_apg_mandays" },
            { data: "allotted_total_mandays" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="proficiency" data-id="'+data.auditor_id+'"><i class="fa fa-user text-primary" title="Proficiency" rel="tooltip"></i></a>    <a href="{{ url('project-scope')}}/' + selectedScope + '/auditor/'+data.id+'" class="edit"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a>    <a href="{{ url('project-scope')}}/' + selectedScope + '/auditor/'+data.id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });

    $(document).on('click',"#_engagement_tab a[href='#scope']",function(){
        if(selectedProject){
            scope_url = "{{ url('project')}}/"+selectedProject+"/scope";
            $("#create-project-scope-form").attr('action',scope_url);
            project_scope_datatable.ajax.url( scope_url + "/list" ).load();
        }else{
            $.sInfo("You have to select project to open this tab.");
        }
    });

    $(document).on("click","#project-scope-table .delete",function(){
        $(this).deleteItemFrom(project_scope_datatable);
        return false;
    });

    $(document).on('click','#project-scope-table tbody tr',function(){
        $("#_engagement_tab").find("a[href='#apg']").attr('data-toggle','tab');
        selectedScope = $(this).data('id');
        $(".selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        $("#show-create-project-scope-auditor-modal").removeAttr('disabled');
        var url = "{{ url('project-scope')}}/" + selectedScope + "/auditor";
        $("#create-project-scope-auditor-form").attr('action',url);
        project_scope_auditor_datatable.ajax.url( url + "/list" ).load();
    });


    $(document).on("click","#project-scope-auditor-table .delete",function(){
        $(this).deleteItemFrom(project_scope_auditor_datatable);
        return false;
    });

    $(document).on('click','.btn-generate-apg',function(){
        var url = $(this).attr('href');
        $.sWarning("Are you sure you want to repopulate APGs for this scope? Old apg will be overriden",function(){
            $.ajax({
                url : url,
                method : "POST",
                success : function(response){
                    $.sSuccess(response.message);
                }
            });
        });
        return false;
    });

</script>
@yield('project_scope_modal_scripts')
@yield('project_scope_auditor_modal_scripts')
@yield('project_scope_iom_modal_scripts')
@yield('ae_details_modal_scripts')
@yield('freeze_modal_scripts')
@yield('cm_scripts')
@endsection