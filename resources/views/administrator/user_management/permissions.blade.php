@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="#permission-modal" data-toggle="modal">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Permissions
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <div>
                        <table id="permissions-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Last Update</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<!-- Modal -->
<div class="modal fade" id="permission-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Permission Form</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => route('administrator.permissions.store'), 'class' => 'form-horizontal', 'id' => 'permissions-form']) !!}
                <input type="hidden" name="pid">
                <input type="hidden" name="_method" value="post">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="name"> Name </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm basic-info" name="name" id="name" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="description"> Description </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm basic-info" name="description" id="description" value="">
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="save()">Save changes</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_script')
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var form = $('#permissions-form'),
        modal = $('#permission-modal');

    $('#permissions-table').DataTable( {
        ajax: "{{route('administrator.permissions.list')}}",
        "processing": true,
        "serverSide": true,
        columns: [
            { data: "name" },
            { data: "readable_name" },
            { data: "updated_at" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var uri = encodeURIComponent(data.menu_name);
                    return '<a href="#" class="btn-update" data-id="'+data.id+'" data-name="'+data.name+'" data-readable_name="'+data.readable_name+'"><i class="fa fa-pencil edit" title="edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.id+'" data-readable_name="'+data.readable_name+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    // save the form through AJAX call
    function save() {
        $.ajax({
            url: form.attr('action'), // url based on the form action
            method: "POST",
            data: form.serialize(), // serializes all the form data
            beforeSend : function() {
                toggle_loading();
                modal.css('z-index', 999);
            },
            success : function(data) {
                if( data.success ) {
                    location.reload();
                }
            },
            error: function(xhr, textStatus, errorThrown){
                modal.css('z-index', 1040);

                if( xhr.status == 500 ) {
                    alert.removeClass('alert-warning').addClass('alert-danger');
                    alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                }
                else {
                    var texts = responsibilityTable.find('input[type="text"][name]').parent().parent().not('#add-row');
                    var select = responsibilityTable.find('select[name]').parent().parent().not('#add-row');

                    texts.find('td.has-error').removeClass('has-error');
                    select.find('td.has-error').removeClass('has-error');
                    $('.basic-info').parent().parent().removeClass('has-error');
                    // displays the validation error
                    var msg = '<ul class="list-unstyled">';
                    $.each(xhr.responseJSON, function(key, val){
                        msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[0]+'</li>';
                        $('#'+key).parent().parent().addClass('has-error');
                    });
                    msg += '</ul>';

                    alert.removeClass('alert-danger').addClass('alert-warning');
                    alert.html(msg).show();
                }

                toggle_loading();
            }
        });
    }

    // update permission
    $(document).on('click', '.btn-update', function() {
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        var readable_name = $(this).attr('data-readable_name');

        form.attr('action', '{{url('administrator/permissions/update')}}/'+id);
        $('input[name="_method"]').val('put');
        $('input[name="pid"]').val(id);
        $('#name').val(name);
        $('#description').val(readable_name);
        modal.modal('show');

        return false;
    });

    modal.on('hidden.bs.modal', function (e) {
        form.attr('action', '{{route('administrator.permissions.store')}}');
        $('input[name="_method"]').val('post');
        $('input[name="pid"]').val('');
        $('#name').val('');
        $('#description').val('');
    });

    // remove permission
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var readable_name = $(this).attr('data-readable_name');
        var ans = confirm('Are you sure you want to remove '+readable_name+'?');

        if( ans ) {
            $.ajax({
                url: '{{ url('administrator/permissions/delete') }}/'+remove_id,
                method: "POST",
                data: { _token : _token, _method : 'delete' }, // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        location.reload();
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    // displays the validation error
                    var msg = '<ul class="list-unstyled">';

                    $.each(xhr.responseJSON, function(key, val){
                        for(var i=0; i < val.length; i++) {
                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                        }
                    });
                    msg += '</ul>';

                    $('.alert-warning').html(msg).show();
                }
            });
        }

        return false;
    });
</script>
@endsection
