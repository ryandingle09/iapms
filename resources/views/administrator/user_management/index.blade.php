@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{route('administrator.users.create')}}">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        User Management
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <div>
                        <table id="lookup-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Employee ID</th>
                                    <th>User Name</th>
                                    <th>Roles</th>
                                    <th>Last Login</th>
                                    <th>Last Update</th>
                                    <th>Update By</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    $('#lookup-table').DataTable( {
        ajax: "{{route('administrator.users.list')}}",
        "processing": true,
        "serverSide": true,
        columns: [
            { data: "employee_id" },
            { data: "user_name" },
            {
                data: "roles[0].roles",
                defaultContent: "N/A",
            },
            { data: "last_login_date" },
            { data: "last_update_date" },
            { data: "updater.user_name" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var links = '';
                    links += '<a href="{{url('administrator/users/edit')}}/'+data.user_name+'"><i class="fa fa-pencil edit" title="edit" rel="tooltip"></i></a> &nbsp; ';
                    links += '<a href="#" class="btn-delete delete" data-id="'+data.user_id+'" data-user_name="'+data.user_name+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    return links;
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    // remove lookup
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var remove_user_name = $(this).attr('data-user_name');
        var ans = confirm('Are you sure you want to remove '+remove_user_name+'?');

        if( ans ) {
            $.ajax({
                url: '{{ url('administrator/users/delete') }}/'+remove_id,
                method: "POST",
                data: { _token : _token, _method : 'delete' }, // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        location.reload();
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    // displays the validation error
                    var msg = '<ul class="list-unstyled">';

                    $.each(xhr.responseJSON, function(key, val){
                        for(var i=0; i < val.length; i++) {
                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                        }
                    });
                    msg += '</ul>';

                    $('.alert-warning').html(msg).show();
                }
            });
        }

        return false;
    });
</script>
@endsection
