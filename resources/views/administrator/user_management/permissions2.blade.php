@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/plugins/treegrid/jquery.treegrid.css" />
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <style type="text/css">
        .selected {
            background-color: #afafaf;
            color: #fff;
        }
        .select2-search:after { display: none !important; }
        input[type=checkbox].ace.ace-switch.sub-parent-switch:checked + .lbl::before {
            background-color: #3cad00;
            border-color: #276b03;
        }
    </style>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="#permission-modal" data-toggle="modal">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Permissions
                    </div>
                    <table class="table table-striped table-bordered table-hover tree">
                        {!! permissionTableRow($permission_array) !!}
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<!-- Modal -->
<div class="modal fade" id="permission-modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Permission Form</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => route('administrator.permissions.store'), 'class' => 'form-horizontal', 'id' => 'permissions-form']) !!}
                <input type="hidden" name="pid">
                <input type="hidden" name="_method" value="post">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="category"> Category </label>
                    <div class="col-sm-8 category-read-only" style="display: none;"></div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control input-sm basic-info" name="category" id="category" value="">
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-default btn-add-subcategory" title="Add subcategory" rel="tooltip"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
                <div class="form-group subcategory-actions">
                    <label class="col-xs-2 control-label" for="description"> Action </label>
                    <div class="col-xs-6">
                        <select id="actions" name="actions[]" class="form-control" multiple="multiple">
                        @foreach(config('iapms.menu.actions') as $action)
                            <option value="{{ $action }}">{{ ucfirst($action) }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-xs-4">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="all_action">
                                Add all
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="description"> Description </label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control input-sm basic-info" name="description" id="description" value="">
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger btn-delete-permission">Delete</button>
                <button type="button" class="btn btn-primary" onclick="save()">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script type="text/template" id="readonly-template">
    <span class="label label-xlg label-primary arrowed-right"></span>
</script>
<script type="text/template" id="subcategory-template">
    <div class="form-group subcategory-group">
        <label class="col-sm-2 control-label" for="subcategory"> Subcategory </label>
        <div class="col-sm-8">
            <input type="text" class="form-control input-sm basic-info" name="subcategory[]" value="">
        </div>
        <div class="col-sm-2">
            <button type="button" class="btn btn-danger btn-delete-subcategory" title="Delete" rel="tooltip"><i class="fa fa-times"></i></button>
        </div>
    </div>
</script>
@endsection

@section('footer_script')
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/treegrid/jquery.treegrid.min.js"></script>
<script src="/js/select2.full.js"></script>
<script type="text/javascript">
    var form = $('#permissions-form'),
        modal = $('#permission-modal'),
        cat_readonly = $('.category-read-only'),
        sub_templ = $('#subcategory-template').html(),
        readonly_templ = $('#readonly-template').html();
    var $sub_cat_choices = [];

    $(document).ready(function() {
        $('.tree').treegrid({
            initialState : 'collapsed'
        });
        $("#actions").select2().next().css('width', '100%');
    });

    $('.sub-parent-switch').on('change', function(){
        toggleCheck($(this));
    });

    function toggleCheck( clicked ) {
        if( clicked.hasClass('sub-parent-switch') ) {
            var id = clicked.parent().parent().parent().prop('id');
            var element = $('.'+id);
            $.each(element, function(i, e){
                if( clicked.is(':checked') ) {
                    $(e).prop('checked', true);
                }
                else {
                    $(e).prop('checked', false);
                }
                toggleCheck($(e));
            });
        }
        else {
            return false;
        }
    }

    // save the form through AJAX call
    function save() {
        $.ajax({
            url: form.attr('action'), // url based on the form action
            method: "POST",
            data: form.serialize(), // serializes all the form data
            beforeSend : function() {
                toggle_loading();
                modal.css('z-index', 999);
            },
            success : function(data) {
                if( data.success ) {
                    location.reload();
                }
            },
            error: function(xhr, textStatus, errorThrown){
                modal.css('z-index', 1040);

                if( xhr.status == 500 ) {
                    alert.removeClass('alert-warning').addClass('alert-danger');
                    alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                }
                else {
                    var texts = responsibilityTable.find('input[type="text"][name]').parent().parent().not('#add-row');
                    var select = responsibilityTable.find('select[name]').parent().parent().not('#add-row');

                    texts.find('td.has-error').removeClass('has-error');
                    select.find('td.has-error').removeClass('has-error');
                    $('.basic-info').parent().parent().removeClass('has-error');
                    // displays the validation error
                    var msg = '<ul class="list-unstyled">';
                    $.each(xhr.responseJSON, function(key, val){
                        msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[0]+'</li>';
                        $('#'+key).parent().parent().addClass('has-error');
                    });
                    msg += '</ul>';

                    alert.removeClass('alert-danger').addClass('alert-warning');
                    alert.html(msg).show();
                }

                toggle_loading();
            }
        });
    }

    // update permission
    $(document).on('click', '.btn-update', function() {
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        var readable_name = $(this).attr('data-readable_name');

        form.attr('action', '{{url('administrator/permissions/update')}}/'+id);
        $('input[name="_method"]').val('put');
        $('input[name="pid"]').val(id);
        $('#name').val(name);
        $('#description').val(readable_name);
        modal.modal('show');

        return false;
    });

    modal.on('hidden.bs.modal', function (e) {
        $('input[name="_method"]').val('post');
        $('#category').val('');
        $('#description').val('');
        $('.subcategory-group').remove();
        $('#all_action').prop('checked', false);
        $("#all_action").trigger("change");
        $sub_cat_choices = [];
        cat_readonly.find('span').remove();
        cat_readonly.hide();
        cat_readonly.next().show();
        $('.subcategory-actions').show();
    });

    // remove permission
    $(document).on('click', '.btn-delete-permission', function() {
        var remove_id = $(this).attr('data-id');
        var readable_name = $(this).attr('data-readable_name');
        var remove_permission;

        swal({
            title: "Are you sure?",
            text: 'You are about to remove '+remove_permission+' permission.',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function(){
            // $.ajax({
            //     url: '{{url('controls_registry/details')}}/'+remove_details_id+'/test_procedures/delete/'+remove_sequence,
            //     method: "POST",
            //     data: { _method : 'delete', _token : '{{ csrf_token() }}' },
            //     success : function(data) {
            //         swal("Deleted!", 'Sequence #'+remove_sequence+" has been deleted.", "success");
            //         updateDatatable();
            //     }
            // });
        });

        // if( ans ) {
        //     $.ajax({
        //         url: '{{ url('administrator/permissions/delete') }}/'+remove_id,
        //         method: "POST",
        //         data: { _token : _token, _method : 'delete' }, // serializes all the form data
        //         beforeSend : function() {
        //             toggle_loading();
        //         },
        //         success : function(data) {
        //             if( data.success ) {
        //                 location.reload();
        //             }
        //         },
        //         error: function(xhr, textStatus, errorThrown){
        //             // displays the validation error
        //             var msg = '<ul class="list-unstyled">';

        //             $.each(xhr.responseJSON, function(key, val){
        //                 for(var i=0; i < val.length; i++) {
        //                     msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
        //                 }
        //             });
        //             msg += '</ul>';

        //             $('.alert-warning').html(msg).show();
        //         }
        //     });
        // }

        return false;
    });

    // Add subcategory
    $('.btn-add').on('click', function(){
        var category_name = $(this).attr('data-name');
        var category_key = $(this).attr('data-key');
        var branch = $(this).parent().parent();
        var branch_id = branch.treegrid('getNodeId');
        var parent_node = getRootParent(branch.treegrid('getParentNode'), branch_id);
        $('#category').val( parent_node );
        cat_readonly.show();
        cat_readonly.next().hide();
        cat_readonly.append(readonly_templ);
        cat_readonly.find('span').html(parent_node);

        // Show category hierchy
        // if( $sub_cat_choices.length > 1 ) {
            for (var i = $sub_cat_choices.length - 1; i >= 0; i--) {
                if($sub_cat_choices[i] != parent_node) {
                    $(readonly_templ).insertAfter(cat_readonly.find('span').last());
                    cat_readonly.find('span').last().html($sub_cat_choices[i]).addClass('arrowed-in');
                    // $('.btn-add-subcategory').trigger('click');
                    // var subcat_input = $('.subcategory-group').last();

                    // subcat_input.find('input[type="text"]').val($sub_cat_choices[i]);
                }
                else {
                    $('.subcategory-actions').hide();
                }
            }
        // }

        // Delete subcategory
        $(document).on('click', '.btn-delete-subcategory', function() {
            $(this).parent().parent().remove();
        });


        // set actions
        if( branch.treegrid('getChildNodes').length > 0 ) {
            branch.treegrid('getChildNodes').each(function(c, child){
                if( $(child).treegrid('getChildNodes').length == 0 ) {
                    $("#actions > option[value='"+$(child).treegrid('getNodeId')+"']").prop("selected","selected");
                    $("#actions").trigger("change");
                }
            });
        }
        $('#permission-modal').modal('show');

        return false;
    });

    // Get root levet parent id
    function getRootParent(node, branch_id) {
        if(!$sub_cat_choices.includes(branch_id)) $sub_cat_choices.push(branch_id);
        if( node === null ) return branch_id;
        if( $(node).treegrid('getParentNodeId') === null ) {
            return $(node).treegrid('getNodeId');
        }
        else {
            if(!$sub_cat_choices.includes($(node).treegrid('getNodeId'))) $sub_cat_choices.push($(node).treegrid('getNodeId'));
        }
        var parent_node_id = $(node).treegrid('getParentNodeId');

        if(!$sub_cat_choices.includes(parent_node_id)) $sub_cat_choices.push(parent_node_id);

        if(parent_node_id != branch_id) getRootParent($(node).treegrid('getParentNode'), branch_id);

        return $(node).treegrid('getParentNodeId');
    }

    // Insert new category
    $('.btn-add-subcategory').on('click', function(){
        var form_group = $(this).parent().parent();
        var to_insert;

        if( form_group.next().hasClass('subcategory-group') ) {
            // insert next to the last subcategory if subcateggory exists
            to_insert = $('.subcategory-group').last();
        }
        else {
            // insert next to the category if no subcateggory exists
            to_insert = form_group;
        }

        $(sub_templ).insertAfter(to_insert);
    });

    $('#all_action').on('change click', function(){
        if($("#all_action").is(':checked') ){
            $("#actions > option").prop("selected","selected");
            $("#actions").trigger("change");
        }else{
            $("#actions > option").removeAttr("selected");
             $("#actions").trigger("change");
         }
    });

    $('#actions').on('select2:unselect', function (evt) {
        $('#all_action').prop('checked', false);
    });
</script>
@endsection