@extends('layouts.app')

@section('styles')
    <style type="text/css">
        .deleted {
            text-decoration: line-through;
            color: red;
        }
        .selected {
            background-color: #afafaf;
            color: #fff;
        }
        .select2-search:after { display: none !important; }
    </style>
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/select2.min.css" />
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Users Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div style="display:none;" class="alert alert-warning"></div>
                                {!! Form::open(['url' => isset($details) ? route('administrator.users.update', ['username' => $details->user_name]) : route('administrator.users.store'), 'class' => 'form-horizontal', 'id' => 'users-form']) !!}
                                    @if( isset($details) )
                                    <input type="hidden" name="_method" value="put">
                                    <input type="hidden" name="uid" value="{{$details->user_id}}">
                                    <input type="hidden" name="to_update" value="">
                                    <input type="hidden" name="to_delete" value="">
                                    @endif
                                    <div class="form-group">
                                        <div class="pull-right btn-group">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button>
                                            <a href="{{route('administrator.users')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="emp_id"> Employee ID </label>
                                        <div class="col-sm-6">
                                            <input type="number" min="0" step="1" class="form-control input-sm basic-info" name="emp_id" id="emp_id" value="{{ isset($details) ? $details->employee_id : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="username"> Username </label>
                                        <div class="col-sm-6">
                                            <input type="text" class="form-control input-sm basic-info" name="username" id="username" value="{{ isset($details) ? $details->user_name : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="passw"> Password </label>
                                        <div class="col-sm-6">
                                            <input type="password" class="form-control input-sm basic-info" name="passw" id="passw" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="description"> Description </label>

                                        <div class="col-sm-6">
                                            <input type="text" class="form-control input-sm" name="description" id="description" value="{{ isset($details) ? $details->description : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="effectivity"> Effectivity Date </label>

                                        <div class="col-sm-3">
                                            <input type="text" class="form-control input-sm date-picker basic-info" id="start_date" name="start_date" placeholder="Start Date" data-date-format="dd-M-yyyy" value="{{ isset($details) ? $details->effective_start_date : date('d-M-Y') }}">
                                        </div>

                                        <div class="col-sm-3">
                                            <input type="text" class="form-control input-sm date-picker basic-info" id="end_date" name="end_date" placeholder="End Date" data-date-format="dd-M-yyyy" value="{{ isset($details) ? $details->effective_end_date : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="description"> Roles </label>

                                        <div class="col-sm-6">
                                            <select multiple="" id="roles" name="roles[]" class="form-control input-sm" data-placeholder="Click to Choose...">
                                            </select>
                                        </div>
                                    </div>

                                    <hr/>
                                    <div class="form-group">
                                        <div class="pull-right btn-group">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button>
                                            <a href="{{route('administrator.users')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')

    <script src="/js/select2.full.js"></script>
    <script src="/js/date-time/bootstrap-datepicker.js"></script>

    <script type="text/javascript">
        var form        = $('#users-form'),
            rolesTable  = $('#roles-table'),
            responsibilityTable = $('#responsibility-table'),
            responsibilitySelect = $('.resp_item'),
            deleted     = $('input[name="to_delete"]'),
            updated     = $('input[name="to_update"]'),
            alert       = $('.alert');

        var to_delete = [],
            to_update = [];

        @if (Session::has('message'))
            alert.html('{!! session('message') !!}').show();
        @endif

        function template(data, container) {
            return data.name;
        }

        function formatRole (role) {
            if (role.loading) return role.text;

            var markup = '<dl>';
            var permissions = [];
            var access_menu = [];

            $.each(role.permissions, function(i, permission){
                var name = permission.name.split('.');

                if( access_menu.indexOf(name[1]) === -1) access_menu.push(name[1]);
                permissions.push(permission.readable_name);
            })

            markup += '<dt>'+role.name+'</dt>';
            markup += '<dd><strong>Access:</strong> '+access_menu.join(' | ')+'</dd>';
            markup += '<dd><strong>Permissions:</strong> '+permissions.join(' | ')+'</dd>';
            markup += '</dl>';

            return markup;
        }

        var data = {!! $roles->toJson() !!};

        $(function(){
            $('.select2-container').css('width', '100%');
            // toggleAddButton();
            $('#roles').select2({
                allowClear:true,
                tags : true,
                tokenSeparators: [",", " "],
                data : data,
                escapeMarkup: function (markup) { return markup; },
                templateSelection: template,
                templateResult: formatRole
            });
            @if( isset($attached_roles) )
                var vals = {!! $attached_roles !!};

                $('#roles').val(vals).trigger("change");
            @endif
        });

        // hide the add button if the number of available responsibility is equal to the number of row
        function toggleAddButton() {
            if( responsibilitySelect.eq(0).find('option').not('option[value=""]').length == responsibilityTable.find('tbody > tr').not('#add-row').length) {
                $('#add-btn').hide();
            }
            else {
                $('#add-btn').show();
            }
        }

        // stops the page from reloading/closing if the form is edited
        $('input[type="text"]').change(function() {
            if( $(this).val() != "" ) {
                window.onbeforeunload = function() {
                    return 'Are you sure you want to navigate away from this page?';
                };
            }
        });

        // add new responsibility row
        function addResponsibility() {
            // cloning row template
            var new_row = responsibilityTable.find('tr#add-row').clone();

            // resetting input attributes
            addNameAttribute(new_row);
            new_row.removeAttr('id');
            new_row.find('input').removeAttr('disabled');
            new_row.insertBefore(responsibilityTable.find('tr#add-row'));
            new_row.show();

            toggleAddButton();
        }

        // save the form through AJAX call
        function save() {
            // roles.val( JSON.stringify(_roles) );
            // permissions.val( JSON.stringify(_permissions) );
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        window.onbeforeunload = null;
                        window.location = '{{ route('administrator.users') }}';
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    $('.basic-info').parent().parent().removeClass('has-error');
                    if( xhr.status == 500 ) {
                        alert.removeClass('alert-warning').addClass('alert-danger');
                        alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        var texts = responsibilityTable.find('input[type="text"][name]').parent().parent().not('#add-row');
                        var select = responsibilityTable.find('select[name]').parent().parent().not('#add-row');

                        texts.find('td.has-error').removeClass('has-error');
                        select.find('td.has-error').removeClass('has-error');
                        $('.basic-info').parent().parent().removeClass('has-error');
                        // displays the validation error
                        var msg = '<ul class="list-unstyled">';
                        $.each(xhr.responseJSON, function(key, val){
                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[0]+'</li>';
                            $('#'+key).parent().parent().addClass('has-error');
                        });
                        msg += '</ul>';

                        alert.removeClass('alert-danger').addClass('alert-warning');
                        alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        }

        // cancel the adding process
        $(document).on('click', '.btn-cancel', function() {
            var cancel_this = $(this).parent().parent(); // gets the TR parent
            if (cancel_this.find('input').filter(function() { return $(this).val(); }).length > 0) {
                var ans = confirm('Are you sure you want to remove this?');
                if( !ans ) {
                    return false;
                }
            }

            cancel_this.remove();

            $('#add-btn').show();

            return false;
        });

        // manage duplicate entry
        $(document).on('change', '.resp_item', function(e) {
            var selected = $(this).val();

            responsibilityTable.find('.resp_item').not(this).not(':last').each(function(){
                if( $('option:selected', this).val() == selected ) {
                    $(this).val('');
                }
            });

            return false;
        });

        // show update form
        $('.btn-update').on('click', function(){
            var update_this = $(this).parent().parent(); // gets the TR parent
            var update_id = $(this).attr('data-id');

            // resetting of row elements
            addNameAttribute(update_this);

            update_this.find('.resp-label').hide();
            update_this.find('.form-control').show();
            update_this.find('.row-control .default-ctrl').hide();
            update_this.find('.row-control .update-ctrl').show();
            update_this.find('.row-control .btn-undo').hide();

            to_update.push(update_id);
            updated.val(JSON.stringify(to_update));

            return false;
        });

        // cancel update
        $(document).on('click', '.btn-update-cancel', function() {
            var cancel_this = $(this).parent().parent(); // gets the TR parent
            var cancel_id = $(this).attr('data-id');

            to_update.splice( to_update.indexOf(cancel_id), 1 );
            updated.val(JSON.stringify(to_update));

            // revert changes from inputs
            $.each(cancel_this.find('.resp-label'), function(i, v) {
                var old_val = $(this).text();
                cancel_this.find('.form-control').eq(i).not('select').val(old_val);
            });

            removeNameAttribute(cancel_this);
            init_code_rows(cancel_this);
            return false;
        });

        function addNameAttribute(row) {
            $.each(row.find('.form-control'), function() {
                var class_name = $(this).parent().prop('class');
                var name = class_name.replace('resp-', '');

                if( name != 'item' ) {
                    $(this).attr('type', 'text');
                }
                $(this).attr('name', 'resp['+name+'][]');
            });
        }

        function removeNameAttribute(row) {
            $.each(row.find('.form-control'), function() {
                $(this).hide();
                $(this).removeAttr('name');
            });
        }

        // initialize row buttons
        function init_code_rows(row) {
            row.find('.resp-label').show();

            row.find('.row-control .default-ctrl').show();
            row.find('.row-control .update-ctrl').hide();
            row.find('.row-control .btn-undo').hide();
        }

        // remove existing responsibility
        $(document).on('click', '.btn-remove', function() {
            var remove_this = $(this).parent().parent(); // gets the TR parent
            var remove_id = $(this).attr('data-id');
            var remove_name = $(this).attr('data-name');
            var ans = confirm('Are you sure you want to remove '+remove_name+'?');

            if( ans ) {
                // update to_delete variable
                to_delete.push(remove_id);
                deleted.val(JSON.stringify(to_delete));

                // resetting of row elements
                remove_this.find('.resp-label').addClass('deleted');
                remove_this.find('.row-control .default-ctrl').hide();
                remove_this.find('.row-control .update-ctrl').hide();
                remove_this.find('.row-control .btn-undo').show();
            }

            return false;
        });

        // undo responsibility delete
        $(document).on('click', '.btn-undo', function() {
            var undo_this = $(this).parent().parent(); // gets the TR parent
            var undo_id = $(this).attr('data-id');

            // update to_delete variable
            to_delete.splice( to_delete.indexOf(undo_id), 1 );
            deleted.val(JSON.stringify(to_delete));

            // resetting of row elements
            undo_this.find('.resp-label').removeClass('deleted');
            init_code_rows(undo_this);

            return false;
        });
    </script>
@endsection