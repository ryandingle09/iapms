@extends('layouts.app')
@section('styles')
<link rel="stylesheet" href="/css/jquery-ui.css" />
<style type="text/css">
    .nestable-checkbox {}
    .nestable-checkbox > ul{list-style: none;margin: 0;}
    .nestable-checkbox > ul ul{ list-style: none;margin-left: 30px; }

</style>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Role Form
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                @include('includes.alerts')
                                {!! Form::open(['url' => isset($details) ? route('administrator.roles.update', ['id' => $details->id]) : route('administrator.roles.store'), 'class' => 'form-vertical', 'id' => 'roles-form']) !!}
                                    @if( isset($details) )
                                    <input type="hidden" name="_method" value="put">
                                    <input type="hidden" name="rid" value="{{$details->id}}">
                                    @endif
                                    <div class="form-group align-right">
                                        <button class="btn btn-primary btn-sm">
                                            <i class="ace-icon fa fa-save bigger-120"></i> Save
                                        </button>
                                        <a href="{{route('administrator.roles')}}" class="btn btn-warning btn-sm">
                                            <i class="ace-icon fa fa-times bigger-120"></i>
                                            Cancel
                                        </a>
                                    </div>
                                    <hr/>
                                    <div class="form-group">
                                        <label for="">Role Name</label>
                                        <input type="text" name="name" class="form-control" placeholder="Role Name" value="{{ old("name") ? old("name") : (isset($details) ? $details->name : '')  }}"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <div id="tabs">
                                            <ul>
                                                <li>
                                                    <a href="#tabs-1">Permissions</a>
                                                </li>

                                                <li>
                                                    <a href="#tabs-2">Menu Display</a>
                                                </li>
                                            </ul>

                                            <div id="tabs-1">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="middle col-sm-12">
                                                            <input class="ace" type="checkbox" id="check-all-permission" >
                                                            <span class="lbl text-primary"> Check All Permission</span>
                                                        </label> 
                                                    </div>
                                                    <div class="row">
                                                        <?php $old = old('permissions') ? old('permissions') : (isset($user_permissions) ? $user_permissions : []) ; ?>
                                                        @foreach($permissions as $key => $value)
                                                        <label class="middle col-sm-3">
                                                            @if(in_array($value->id, $old))
                                                            <input class="ace cb-permission" type="checkbox" name="permissions[]" value="{{ $value->id}} " checked="checked">
                                                            @else
                                                            <input class="ace cb-permission" type="checkbox" name="permissions[]" value="{{ $value->id}} " >
                                                            @endif
                                                            <span class="lbl"> {{ $value->readable_name}}</span>
                                                        </label> 
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tabs-2">
                                                <div class="row">
                                                    <label class="middle col-sm-12">
                                                        <input class="ace" type="checkbox" id="check-all-menu" >
                                                        <span class="lbl text-primary"> Check All Menu</span>
                                                    </label> 
                                                </div>
                                                <div class="row " >
                                                    <div class="col-sm-12 nestable-checkbox">
                                                    {!! menu()->getMenuNestableTemplate('administrator.user_management.forms.nested-checkbox',[],$user_menus) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <hr/>
                                    <div class="form-group align-right">
                                        <button class="btn btn-primary btn-sm">
                                            <i class="ace-icon fa fa-save bigger-120"></i> Save
                                        </button>
                                        <a href="{{route('administrator.roles')}}" class="btn btn-warning btn-sm">
                                            <i class="ace-icon fa fa-times bigger-120"></i>
                                            Cancel
                                        </a>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/js/jquery-ui.js"></script>
<script type="text/javascript">
    $( "#tabs" ).tabs();

    $(document).on("click",'#check-all-permission',function(){
        $('#roles-form').find('.cb-permission').prop('checked', this.checked);    
    });

    $(document).on("click",'#check-all-menu',function(){
        $('#roles-form').find('.cb-menu').prop('checked', this.checked);    
    });

    $(document).on("click",'.nestable-checkbox .cb-menu',function(){
        $(this).next().next().find('.cb-menu').prop('checked', this.checked);
    });
</script>
@endsection