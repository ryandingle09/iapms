<?php $old = old('menus') ? old('menus') : ( isset($params) ? $params : [] ); ?>
<ul>
    @foreach($menus as $item)
    <li>
        @if(in_array($item->id,$old))
        <input class="ace cb-menu" type="checkbox" name="menus[]" value="{{$item->id}}" checked="checked">
        @else
        <input class="ace cb-menu" type="checkbox" name="menus[]" value="{{$item->id}}">
        @endif
        <span class="lbl"> <i class="fa {{$item->icon}}"></i> {{$item->name}}</span>
        @if(!$item->children->isEmpty())
        {!! menu()->getMenuNestableTemplate('administrator.user_management.forms.nested-checkbox',$item->children,$old) !!}
        @endif
    </li>
    @endforeach
</ul>