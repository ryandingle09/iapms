<ol class="dd-list">
    @foreach($menus as $item)
    <?php $active_class = (Request::get('id') == $item->id ) ? 'btn-info' : ''?>
    <li class="dd-item dd2-item" data-id="{{ $item->id }}">
        <div class="dd-handle dd2-handle {{ $active_class }}">
            <i class="normal-icon ace-icon fa {{ $item->icon }} bigger-130"></i>
            <i class="drag-icon ace-icon fa fa-arrows-alt bigger-125"></i>
        </div>
        <div class="dd2-content {{ $active_class }}">
            <!--<i class="ace-icon fa {{ $item->icon }}"></i>-->
            {{ $item->name }}
            <div class="pull-right action-buttons">
                <a class="blue edit-menu" href="{{ url('administrator/menu?id='.$item->id)}}">
                    <i class="ace-icon fa fa-pencil bigger-130"></i>
                </a>

                <a class="red delete-menu" href="{{ route('administrator.menu.delete', ['id' => $item->id ])}}">
                    <i class="ace-icon fa fa-trash-o bigger-130"></i>
                </a>
            </div>
        </div>

        @if(!$item->children->isEmpty())
        {!! menu()->getMenuNestableTemplate('administrator.menu.nested-menu',$item->children) !!}
        @endif
    </li>
    @endforeach
</ol>

