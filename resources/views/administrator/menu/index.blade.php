@extends('layouts.app')
@section('styles')
    <style type="text/css">
        .dd {max-width: 100%;}
        .dd2-handle {cursor: move;}
    </style>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')
        <div class="page-content">
            @include('includes.content_header')
            @include('includes.alerts')
            <div class="row">
                <div class="col-xs-8">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Menu Editor
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="dd" id="menu-nestable">
                                    {!! menu()->getMenuNestableTemplate('administrator.menu.nested-menu') !!}
                                </div>
                                <div class="form-group clearfix">
                                    <a href="{{ route('administrator.menu.update_hierarchy')}}" class="btn btn-info pull-right" id="btn-save-menu">
                                        <i class="ace-icon fa fa-check"></i>
                                        Save
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                @if(isset($menu_item))
                                Edit <i>"{{$menu_item->name}}"</i>
                                @else
                                Add Menu Item Form
                                @endif
                            </h4>
                        </div>
                        <div class="widget-body">
                            <div class="widget-main">
                                {!! Form::open(['url' => isset($menu_item) ? route('administrator.menu.update', ['menu_id' => $menu_item->id]) : route('administrator.menu.store'), 'class' => 'form-vertical']) !!}
                                    <div class="form-group">
                                        <label for="">Menu Title</label>
                                        <input type="text" name="name" id="name" class="form-control" placeholder="e.g. Dashboard, Settings" value="{{ old("name") ? old("name") : (isset($menu_item) ? $menu_item->name : '')  }}">
                                    </div>
                                    <div class="form-group">
                                            <input class="ace" type="checkbox" id="parent-menu" name="is_parent" {{ old("is_parent") ? 'checked' : (isset($menu_item) ? ($menu_item->is_parent ? 'checked' : '') : '')  }} >
                                            <span class="lbl"> Parent Menu</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="">URL</label>
                                        <input type="text" name="url" id="url" class="form-control" placeholder="e.g. /dashboard/edit, /settings/list" value="{{ old("url") ? old("url") : (isset($menu_item) ? $menu_item->url : '')  }}" {{ old("is_parent") ? 'disabled' : (isset($menu_item) ? ($menu_item->is_parent ? 'disabled' : '') : '')  }}>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Icon
                                            (<a href="http://fontawesome.io/icons/" target="_blank">View on Fontawesome</a>)
                                        </label>
                                        <input type="text" name="icon" id="icon" class="form-control" placeholder="e.g. fa-user, fa-address-book" value="{{ old("icon") ? old("icon") : (isset($menu_item) ? $menu_item->icon : '')  }}">
                                    </div>
                                    <div class="form-group align-right">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        @if(isset($menu_item))
                                        <input type="hidden" name="_method" value="PUT">
                                        <a href="{{ route('administrator.menu') }}" class="btn btn-mute">
                                            <i class="ace-icon fa fa-remove"></i>
                                            Cancel
                                        </a>
                                        @endif
                                        <button class="btn btn-info">
                                            <i class="ace-icon fa fa-check"></i>
                                            Submit
                                        </button>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/js/jquery-ui-1.11.4.sortable/jquery-ui.min.js"></script>
<script src="/js/jquery.nestable.js"></script>
<script type="text/javascript">
    $('#menu-nestable').nestable({
        maxDepth : 5
    });

    $(".edit-menu").on('mousedown', function(e){
        location = $(this).attr('href');
        e.stopPropagation();
    });

    $(".delete-menu").on('mousedown', function(e){
        link = $(this).attr('href');
        sWarning("You are about to delete an item?",function(){
            $.ajax({
                url: link,
                method: "POST",
                data: { _token : _token, _method : 'delete' }, // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        location = data.url;
                    }
                }
            });
        });
        e.stopPropagation();
    });

    $("#parent-menu").click(function(){
         $("#url").prop("disabled", this.checked);
    });

    $("#btn-save-menu").click(function(e){
        menus = JSON.stringify($('#menu-nestable').nestable('serialize'));
        link = $(this).attr('href');
        sInfo("You are about to update menu hierarchy?",function(){
            $.ajax({
                url: link,
                method: "PUT",
                data: { _token : _token, _method : 'PUT' , menus : menus },
                success : function(data) {
                    if( data.success ) {
                        location = data.url;
                    }
                }
            });
        });
        return false;
    });
</script>
@endsection
