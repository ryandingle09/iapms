@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{route('administrator.lookup.create')}}">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Lookup Types
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <div>
                        <table id="lookup-table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Meaning</th>
                                    <th>Description</th>
                                    <th>Last Update</th>
                                    <th>Updated By</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var $datatable = $('#lookup-table').DataTable( {
        ajax: "{{route('administrator.lookup.list')}}",
        "lengthMenu": [ 25, 50, 75, 100 ],
        "processing": true,
        "serverSide": true,
        columns: [
            { data: "lookup_type" },
            { data: "meaning", searchable: false },
            { data: "description", searchable: false },
            { data: "last_update_date",searchable: false },
            { data: "updated_by.user_name", defaultContent : '', searchable: false },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('administrator/lookup/edit')}}/'+data.lookup_type+'"><i class="fa fa-pencil edit" title="edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.id+'" data-lookup_type="'+data.lookup_type+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    // remove lookup
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var remove_lookup_type = $(this).attr('data-lookup_type');
        var ans = confirm('Are you sure you want to remove '+remove_lookup_type+'?');

        if( ans ) {
            $.ajax({
                url: '{{ url('administrator/lookup/delete') }}/'+remove_id,
                method: "POST",
                data: { _token : _token, _method : 'delete' }, // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    toggle_loading();
                    if( data.success ) {
                        alert('Lookup type successfully deleted.');
                        $datatable.ajax.reload( null, false );
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    // displays the validation error
                    var msg = '<ul class="list-unstyled">';

                    $.each(xhr.responseJSON, function(key, val){
                        for(var i=0; i < val.length; i++) {
                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                        }
                    });
                    msg += '</ul>';

                    $('.alert-warning').html(msg).show();
                }
            });
        }

        return false;
    });
</script>
@endsection
