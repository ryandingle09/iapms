@extends('layouts.app')

@section('styles')
    <style type="text/css">
        .deleted {
            text-decoration: line-through;
            color: red;
        }
        .datepicker {
            z-index: 1500 !important;
        }
    </style>
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Lookup Type Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div style="display:none;" class="alert alert-warning alert-main"></div>
                                {!! Form::open(['url' => isset($details) ? route('administrator.lookup.update', ['type' => $details->lookup_type]) : route('administrator.lookup.store'), 'class' => 'form-horizontal', 'id' => 'lookup-form']) !!}
                                    @if( isset($details) )
                                    <input type="hidden" name="_method" value="put">
                                    <input type="hidden" name="to_delete" value="">
                                    <input type="hidden" name="to_update" value="">
                                    <input type="hidden" name="lookup_id" value="{{$details->id}}">
                                    @endif
                                    <div class="form-group">
                                        <div class="pull-right btn-group">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                {!! isset($details) ? '<i class="ace-icon fa fa-save bigger-120"></i> Save' : '<i class="ace-icon fa fa-forward bigger-120"></i> Continue to add code' !!}
                                            </button>
                                            <a href="{{route('administrator.lookup')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="value"> Type </label>

                                        <div class="col-sm-5">
                                            @if( isset($details) )
                                                <span class="label label-info" style="margin-top: 5px;">{{ $details->lookup_type }}</span>
                                                <input type="hidden" name="type" value="{{$details->lookup_type}}">
                                            @else
                                                <input type="text" class="form-control input-sm basic-info" name="type" id="type" value="{!! session('lookup.populate') !!}">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="meaning"> Meaning </label>

                                        <div class="col-sm-5">
                                            <input type="text" class="form-control input-sm basic-info" name="meaning" id="meaning" value="{{ isset($details) ? $details->meaning : session('lookup.populate') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="description"> Description </label>

                                        <div class="col-sm-5">
                                            <input type="text" class="form-control input-sm" name="description" id="description" value="{{ isset($details) ? $details->description : session('lookup.populate') }}">
                                        </div>
                                    </div>
                                    @if( isset($details) )
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#lookupvalue-modal">
                                                <i class="ace-icon fa fa-plus bigger-120"></i>
                                                Add
                                            </button>
                                            <div class="table-header">
                                                Lookup Values
                                            </div>
                                            <!-- div.dataTables_borderWrap -->
                                            <div>
                                                <table id="lookup-table" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Lookup Code</th>
                                                            <th>Meaning</th>
                                                            <th>Description</th>
                                                            <th>Start Date</th>
                                                            <th>End Date</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <hr/>
                                    <div class="form-group">
                                        <div class="pull-right btn-group">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                {!! isset($details) ? '<i class="ace-icon fa fa-save bigger-120"></i> Save' : '<i class="ace-icon fa fa-forward bigger-120"></i> Continue to add code' !!}
                                            </button>
                                            <a href="{{route('administrator.lookup')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<!-- Lookup value modal -->
<div class="modal fade" data-backdrop="static" id="lookupvalue-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Lookup Value Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning alert-modal"></div>
                {!! Form::open(['method' => 'post', 'url' => route('administrator.lookup.value_store'), 'class' => 'form-horizontal', 'id' => 'lookupvalue-form']) !!}
                    <input type="hidden" name="lookup_type_id" value="{{isset($details) ? $details->id : ''}}">
                    <input type="hidden" name="_method" value="post">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="value"> Lookup Code </label>
                        <div class="col-sm-5">
                            <span class="label label-info lookup_code" style="margin-top: 5px;display: none;"></span>
                            <input type="text" class="form-control input-sm" id="code" name="code" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="meaning"> Meaning </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm basic-info" id="code_meaning" name="code_meaning">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="description"> Description </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm" id="code_description" name="code_description">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="description"> Effectivity Date </label>

                        <div class="col-sm-9">
                            <div class="input-daterange input-group">
                                <input type="text" id="start_date" name="start_date" class="form-control input-sm form-control" placeholder="Start Date" value="{{ isset($details) ? $details->effective_start_date : '' }}"/>
                                <span class="input-group-addon">to</span>
                                <input type="text" id="end_date" name="end_date" class="form-control input-sm form-control" placeholder="End Date" value="{{ isset($details) ? $details->effective_end_date : '' }}"/>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-lookupvalue-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Lookup value modal -->
@endsection

@section('footer_script')

    <script src="/js/date-time/bootstrap-datepicker.js"></script>
    <script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
        var lookupTable = $('#lookup-table'),
            form        = $('#lookup-form'),
            deleted     = $('input[name="to_delete"]'),
            updated     = $('input[name="to_update"]'),
            $alert       = $('.alert-main'),
            $modal       = $('#lookupvalue-modal');
        var $datatable;
        var to_delete = [],
            to_update = [];

         @if (Session::has('message'))
            $alert.html('{!! session('message') !!}').show();
        @endif

        // stops the page from reloading/closing if the form is edited
        $('input').change(function() {
            if( $(this).val() != "" ) {
                window.onbeforeunload = function() {
                    return 'Are you sure you want to navigate away from this page?';
                };
            }
        });

        // save the form through AJAX call
        function save() {
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        window.onbeforeunload = null;
                        if( form.find('input[name="_method"]').val() !== undefined ) {
                            window.location = '{{ route('administrator.lookup') }}';
                        }
                        else {
                            window.location = '{{ url('administrator/lookup/edit') }}/'+$('#type').val();
                        }
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';
                        var editables = lookupTable.find('input[type="text"][name]').parent().parent().not('#add-row');

                        editables.find('td.has-error').removeClass('has-error');
                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                if( key.indexOf('.') !== -1 ) {
                                    var code_fields = key.split('.');
                                    var index = code_fields.splice(-1);

                                    editables.eq(index[0]).find('td.code-'+code_fields[1]).addClass('has-error')
                                }
                                else {
                                    $('#'+key).parent().parent().addClass('has-error');
                                }

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        }

        @if(isset($details))
        $(function(){
            $('.input-daterange').datepicker({
                autoclose : true,
                todayHighlight : true,
                format : 'dd-M-yyyy'
            });

            init_modal();
        });

        $datatable = $('#lookup-table').DataTable( {
            ajax: "{{route('administrator.lookup.value_list', ['type' => $details->lookup_type])}}",
            "lengthMenu": [ 25, 50, 75, 100 ],
            "processing": true,
            "serverSide": true,
            columns: [
                { data: "lookup_code" },
                { data: "meaning" },
                { data: "description" },
                { data: "effective_start_date",searchable: false },
                { data: "effective_end_date",searchable: false },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn-update" data-lookup_code="'+data.lookup_code+'" data-meaning="'+data.meaning+'" data-desc="'+data.description+'" data-start_date="'+data.effective_start_date+'" data-end_date="'+data.effective_end_date+'"><i class="fa fa-pencil edit" title="edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete btn-remove" data-lookup_code="'+data.lookup_code+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        } );

        // initialize modal fields
        function init_modal( code, meaning, desc, start_date, end_date) {
            if( code != undefined && code != "" ) {
                // update setting
                $modal.find('.lookup_code').html(code).show();
                $modal.find('input#code').hide();
                $('#lookupvalue-form').attr('action', '{{ url('administrator/lookup/value/update') }}/'+code)
                                      .find('input[name="_method"]').val('put');
            }
            else {
                // create setting
                $modal.find('input#code').show();
                $modal.find('.lookup_code').html(code).hide();
                $('#lookupvalue-form').attr('action', '{{route('administrator.lookup.value_store')}}')
                                      .find('input[name="_method"]').val('post');
            }
            $modal.find('input#code').val(code);
            $modal.find('input#code_meaning').val(meaning);
            $modal.find('input#code_description').val(desc);
            $modal.find('input#start_date').val(curr_date);
            $modal.find('input#end_date').val(end_date);
            $('#lookupvalue-form .form-group').removeClass('has-error').removeClass('has-success');
            $('.alert-modal').html('').hide();
        }

        $modal.on('hidden.bs.modal', function (e) {
            $modal.find('input#start_date').data('datepicker').setDate(null)
            $modal.find('input#end_date').data('datepicker').setDate(null)
            init_modal('', '', '', '', '');
        });

        function updateDatatable() {
            $datatable.ajax.reload( null, false );
            $modal.modal('hide');
        }

        $(document).on('click', '.btn-lookupvalue-save', function(){
            $('#lookupvalue-form').submit();
        });

        $('#lookupvalue-form').on('submit', function(){
            $.ajax({
                url: $(this).attr('action'), // url based on the form action
                method: "POST",
                data: $(this).serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    toggle_loading();
                    updateDatatable();
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        $('.alert-modal').removeClass('alert-warning').addClass('alert-danger');
                        $('.alert-modal').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';
                        var editables = lookupTable.find('input[type="text"][name]').parent().parent().not('#add-row');

                        editables.find('td.has-error').removeClass('has-error');
                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                if( key.indexOf('.') !== -1 ) {
                                    var code_fields = key.split('.');
                                    var index = code_fields.splice(-1);

                                    editables.eq(index[0]).find('td.code-'+code_fields[1]).addClass('has-error')
                                }
                                else {
                                    $('#'+key).parent().parent().addClass('has-error');
                                }

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $('.alert-modal').removeClass('alert-danger').addClass('alert-warning');
                        $('.alert-modal').html(msg).show();
                    }

                    toggle_loading();
                }
            });
            return false;
        });

        // show update form
        $(document).on('click', '.btn-update', function(){
            var code = $(this).attr('data-lookup_code'),
                meaning = $(this).attr('data-meaning'),
                desc = $(this).attr('data-desc'),
                start_date = $(this).attr('data-start_date'),
                end_date = $(this).attr('data-end_date');

            // set values
            init_modal(code, meaning, desc, start_date, end_date);

            $modal.modal('show');

            return false;
        });

        // remove existing code
        $(document).on('click', '.btn-remove', function() {
            var remove_code = $(this).attr('data-lookup_code');
            var ans = confirm('Are you sure you want to remove '+remove_code+'?');

            if( ans ) {
                $.ajax({
                    url: '{{url('administrator/lookup/'.$details->id.'/value/delete/')}}/'+remove_code,
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}' },
                    beforeSend : function() {
                        toggle_loading();
                    },
                    success : function(data) {
                        toggle_loading();
                        updateDatatable();
                    }
                });
            }

            return false;
        });
        @endif
    </script>
@endsection
