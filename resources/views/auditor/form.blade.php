@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Auditor Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div id="auditor-alert">
                                	@include('includes.alerts')
                            	</div>
                                {!! Form::open(['url' => isset($details) ? route('auditor.update', [ 'name' => $details->auditor_id ]) : route('auditor.store'), 'class' => 'form-horizontal', 'id' => 'auditor-form' ]) !!}
                                  	<div class="form-row align-right">
                                        <button class="btn btn-primary btn-sm">
                                            <i class="ace-icon fa fa-save bigger-120"></i>
                                            Save
                                        </button>
                                        <a href="{{route('auditor.list')}}" class="btn btn-warning btn-sm">
                                            <i class="ace-icon fa fa-times bigger-120"></i>
                                            Cancel
                                        </a>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="first_name"> Full name </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <input type="text" class="form-control input-sm basic-info" id="first_name" name="first_name" value="{{ isset($details) ? $details->first_name : '' }}" placeholder="First name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="middle_name"> &nbsp; </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <input type="text" class="form-control input-sm basic-info" id="middle_name" name="middle_name" value="{{ isset($details) ? $details->middle_name : '' }}" placeholder="Middle name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="last_name"> &nbsp; </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <input type="text" class="form-control input-sm basic-info" id="last_name" name="last_name" value="{{ isset($details) ? $details->last_name : '' }}" placeholder="Last name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="nickname"> Nickname </label>

                                        <div class="col-sm-3">
                                            <input type="text" class="form-control input-sm basic-info" id="nickname" name="nickname" value="{{ isset($details) ? $details->nickname : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="employee_number"> Employee Number </label>

                                        <div class="col-sm-3">
                                            <input type="number" class="form-control input-sm basic-info" id="employee_number" name="employee_number" value="{{ isset($details) ? $details->employee_number : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="gender"> Gender </label>

                                        <div class="col-sm-3">
                                            <select id="gender" name="gender" class="form-control input-sm basic-info">
                                                <option value="">--- Select gender ---</option>
                                                <option value="M" {{ isset($details) ? ($details->gender == 'M' ? 'selected="selected"' : '') : '' }}>Male</option>
                                                <option value="F" {{ isset($details) ? ($details->gender == 'F' ? 'selected="selected"' : '') : '' }}>Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="position"> Position </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <select id="position" name="position" class="form-control input-sm basic-info" placeholder="Select a position"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="email_address"> Email Address </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <input type="email" class="form-control input-sm basic-info" id="email_address" name="email_address" value="{{ isset($details) ? $details->email_address : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="supervisor"> Immediate Head </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <select id="supervisor" name="supervisor" class="form-control input-sm basic-info" placeholder="Select a head"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="mobile_number"> Mobile No. </label>

                                        <div class="col-sm-3">
                                            <input type="number" class="form-control input-sm basic-info" id="mobile_number" name="mobile_number" value="{{ isset($details) ? $details->mobile_number : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="present_address"> Present Address </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <textarea rows="5" id="present_address" name="present_address" class="form-control" style="resize: none;">{{ isset($details) ? $details->present_address : '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="provincial_address"> Provincial Address </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <textarea rows="5" id="provincial_address" name="provincial_address" class="form-control" style="resize: none;">{{ isset($details) ? $details->provincial_address : '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="effectivity"> Effectivity Date </label>

                                        <div class="col-sm-3 col-xs-10">
                                            <input type="text" class="form-control input-sm form-datepicker basic-info" id="start_date" name="start_date" placeholder="Start Date" value="{{ isset($details) ? $details->effective_start_date : date('d-M-Y') }}">
                                        </div>

                                        <div class="col-sm-3 col-xs-10">
                                            <input type="text" class="form-control input-sm form-datepicker basic-info" id="end_date" name="end_date" placeholder="End Date" value="{{ isset($details) ? $details->effective_end_date : '' }}">
                                        </div>
                                    </div>
                                <hr/>
                                <div class="form-row align-right">
                                    <button class="btn btn-primary btn-sm">
                                        <i class="ace-icon fa fa-save bigger-120"></i>
                                        Save
                                    </button>
                                    <a href="{{route('auditor.list')}}" class="btn btn-warning btn-sm">
                                        <i class="ace-icon fa fa-times bigger-120"></i>
                                        Cancel
                                    </a>
                                </div>
                                {!! Form::close() !!}
                                @if(isset($details))
                                    <div class="row">
                                        @include('auditor.lcm', ['auditor_id' => $details->auditor_id ])
                                        @include('auditor.training', ['auditor_id' => $details->auditor_id ])
                                        @include('auditor.proficiency', ['auditor_id' => $details->auditor_id ])
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection
@section('footer_script')
<script type="text/javascript">

    $(".form-datepicker").datepicker({
        format: "M-dd-yyyy",
        keyboardNavigation: false,
        autoclose : true,
        todayHighlight: true,
    });

    var position_select = $("#auditor-form [name='position']").makeSelectize({
        lookup : '{{ config('iapms.lookups.auditor.position') }}',
        onLoad : function(){
            @if(isset($details))
            this.setValue("{{ $details->position_code }}");
            @endif
        }
    })[0].selectize;

    var supervisor_select = $("#auditor-form [name='supervisor']").makeSelectize({
        url: "{{route('auditor.list.show')}}?except={{isset($details) ? $details->first_name.','.$details->middle_name.','.$details->last_name : ''}}",
        onLoad : function(){
            @if(isset($details))
            this.setValue({{ $details->supervisor_id }});
            @endif
        },
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    })[0].selectize;

	$("#auditor-form").submit(function(){
        var action = "{{ isset($details) ? "PUT" : "POST"  }}";
		$(".alert").remove();
		$.ajax({
			url : $(this).attr('action'),
			method : action,
            data : $(this).serialize(),
			beforeSend : sLoading(),
			success : function(response){
                if(action == "POST"){
                    $.sSuccess(response.message,function(){
                        location.href = "{{ url('auditor/edit') }}/"+ response.data.auditor_id;
                    });
                }else{
                    $.sSuccess(response.message);
                }
			}
		});
		return false;
	});
</script>
@yield('lcm_scripts')
@yield('proficiency_scripts')
@yield('training_scripts')
@endsection