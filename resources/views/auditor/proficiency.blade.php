<div class="col-xs-12 mrgB15">
    <button class="btn btn-default btn-sm"  id="show-proficiency-modal">
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Add
    </button>
    <div class="table-header">
        Proficiency
    </div>

    <!-- div.table-responsive -->

    <!-- div.dataTables_borderWrap -->
    <div>
        <table id="proficiency-table" class="table table-striped table-bordered table-hover" style="font-size: inherit;">
            <thead>
            <tr>
                <th width="20%">Proficiency Type</th>
                <th width="20%">Rate</th>
                <th width="30%">Remarks</th>
                <th width="10%"></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@include('auditor.includes.proficiency_modal', [ "auditor_id" => $auditor_id ] )
@section('proficiency_scripts')
<script type="text/javascript">
    var proficiency_url = "{{ url('auditor/'.$auditor_id.'/proficiency' ) }}";

    var proficiency_datatable = $("#proficiency-table").DataTable( {
        bDestroy: true,
        ajax: proficiency_url + "/list",
        processing: true,
        lengthMenu: [ 25, 50, 75, 100 ],
        processing: true,
        columns: [
            { data: "proficiency_type" },
            { data: "proficiency_rate" },
            { data: "remarks" },
            {
                data: null,
                className: 'row-control',
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="edit" rel="tooltip" data-original-title="Edit"><i class="fa fa-pencil"></i></a>    <a href="'+proficiency_url+'/'+data.auditor_proficiency_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    });

    $("#show-proficiency-modal").click(function(){
        $("#proficiency-form").attr('action',proficiency_url);
        $("#proficiency-form").attr('method', "POST" );
        $("#proficiency-modal").modal('show');
    });

    $(document).on("click","#proficiency-table .delete",function(){
        $(this).deleteItemFrom(proficiency_datatable);
        return false;
    });

</script>
@yield('proficiency_modal_scripts')
@endsection