@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')
            @include('includes.alerts')

            <div class="row">
                <div class="col-xs-12">
                    
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{route('auditor.setup.holiday.create')}}">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Holidays
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="holiday-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Holiday Date</th>
                                <th>Holiday Type</th>
                                <th>Description</th>
                                <th>Fixed Date</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var datatable= $('#holiday-table').DataTable( {
        ajax: "{{route('auditor.setup.holiday.lists')}}",
        "processing": true,
        columns: [
            { data: "holiday_date" },
            { data: "lv_holiday_type_meaning" },
            { data: "lv_holiday_type_desc" },
            {
                data: "fixed_date",
                render: function(data) {
                    return data == 'Y' ? 'Yes' : 'No';
                }
            },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('auditor/setup/holiday/edit')}}/'+data.id+'"><i class="fa fa-pencil edit" title="edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-description="'+data.lv_holiday_type_desc+'" data-holiday_date="'+data.holiday_date+'" data-id="'+data.id+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var remove_date = $(this).attr('data-holiday_date');
        var remove_description = $(this).attr('data-description');

        sWarning('Are you sure you want to remove '+remove_description+'?',function(){
            $.ajax({
                url: '{{ url('auditor/setup/holiday/delete') }}/'+remove_id,
                method: "POST",
                data: { _token : _token, _method : 'delete' },
                success : function(data) {
                    if( data.success ) {
                        location.reload();
                    }
                }
            });
        });
        return false;
    });
</script>
@endsection
