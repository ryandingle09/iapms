@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="/css/datepicker.css" />
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Holiday Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                @include('includes.alerts')
                                {!! Form::open(['url' => isset($details) ? route('auditor.setup.holiday.update', ['date' => $details->id]) : route('auditor.setup.holiday.store'), 'class' => 'form-horizontal', 'id' => 'holiday-form']) !!}
                                    @if( isset($details) )
                                    <input type="hidden" name="_method" value="put">
                                    @endif
                                    <div class="form-row align-right">
                                        <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                            <i class="ace-icon fa fa-save bigger-120"></i>
                                            Save
                                        </button>
                                        <a href="{{route('auditor.setup.holiday')}}" class="btn btn-warning btn-sm">
                                            <i class="ace-icon fa fa-times bigger-120"></i>
                                            Cancel
                                        </a>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="holiday_date"> Holiday Date </label>
                                        <div class="col-sm-6 col-xs-10">
                                            <input type="text" class="form-control  date-picker" id="holiday_date" name="holiday_date" placeholder="Holiday Date" data-date-format="dd-M-yyyy" value="{{ isset($details) ? $details->holiday_date : '' }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="holiday_type"> Holiday Type </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <select id="holiday_type" name="holiday_type" class="form-control  basic-info" data-placeholder="Click to Choose...">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="description"> Description </label>
                                        <div class="col-sm-6 col-xs-10">
                                            <span class="label label-info label-wrap" id="description">{{ isset($details) ? $details->description : '' }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="fixed_date"> Fixed Type </label>

                                        <div class="col-sm-6 col-xs-10">
                                            <label>
                                                <input id="fixed_date" name="fixed_date" class="ace ace-switch ace-switch-2" type="checkbox" {{ isset($details) ? ($details->fixed_date == 'Y' ? 'checked' : '') : '' }} value="Y" />
                                                <span class="lbl"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <hr/>
                                    <div class="form-row align-right">
                                        <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                            <i class="ace-icon fa fa-save bigger-120"></i>
                                            Save
                                        </button>
                                        <a href="{{route('auditor.setup.holiday')}}" class="btn btn-warning btn-sm mrgl15">
                                            <i class="ace-icon fa fa-times bigger-120"></i>
                                            Cancel
                                        </a>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/js/date-time/bootstrap-datepicker.js"></script>

<script type="text/javascript">
    var select_holiday;
    var holiday;
    holiday = $("#holiday_type").makeSelectize({
        lookup : "{{ config('iapms.lookups.holiday') }}" ,
        selectedTemplate : 'id_text_meaning',
        initialValue : '{{isset($details) ? $details->holiday_type : ''}}',
        searchField : ['description']
    }).change(function(){
        $('#description').html(select_holiday.options[$(this).val()].meaning || '');
    });
    select_holiday = holiday[0].selectize;

    // save the form through AJAX call
    function save() {
        form = $('#holiday-form')
        $.ajax({
            url: form.attr('action'), // url based on the form action
            method: "POST",
            data: form.serialize(), // serializes all the form data
            // beforeSend : sLoading(),
            success : function(data) {
                if( data.success ) {
                    window.onbeforeunload = null;
                    window.location = '{{ route('auditor.setup.holiday') }}';
                }
            }
        });
    }
</script>
@endsection