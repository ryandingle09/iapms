<div class="col-xs-12 mrgB15">
    <button class="btn btn-default btn-sm" type="button" id="show-lcm-modal">
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Add
    </button>
    <div class="table-header">
        License / Certification / Membership
    </div>

    <div>
        <table id="lcm-table" class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th width="10%">Type</th>
                <th width="30%">Meaning</th>
                <th width="20%">License / Certification Number</th>
                <th width="15%">Issued Date</th>
                <th width="15%">Expiry Date</th>
                <th width="10%"></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@include('auditor.includes.lcm_modal', [ "auditor_id" => $auditor_id ] )
@section('lcm_scripts')
<script type="text/javascript">
    var lcm_url = "{{ url('auditor/'.$auditor_id.'/lcm' ) }}";

    var lcm_datatables = $("#lcm-table").DataTable( {
        bDestroy: true,
        ajax: lcm_url + "/list",
        processing: true,
        lengthMenu: [ 25, 50, 75, 100 ],
        processing: true,
        columns: [
            { data: "lcm_type" },
            { data: "lv_lcm_type_meaning" },
            { data: "ref_no" },
            { data: "issued_date" },
            { data: "expiry_date" },
            {
                data: null,
                className: 'row-control',
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="edit" rel="tooltip" data-original-title="Edit"><i class="fa fa-pencil"></i></a>    <a href="'+lcm_url+'/'+data.auditor_lcm_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    });

    $("#show-lcm-modal").click(function(){
        $("#lcm-form").attr('action',lcm_url);
        $("#lcm-form").attr('method', "POST" );
        $("#lcm-modal").modal('show');
    });

    $(document).on("click","#lcm-table .delete",function(){
        $(this).deleteItemFrom(lcm_datatables);
        return false;
    });

</script>
@yield('lcm_modal_scripts')
@endsection