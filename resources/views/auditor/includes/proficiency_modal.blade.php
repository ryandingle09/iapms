<div class="modal fade" data-backdrop="static" id="proficiency-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Proficiency Form</h4>
            </div>
            <div class="modal-body">
                <div id="proficiency-alert" class="mrgB15"></div>
                <form class="form-horizontal" id="proficiency-form">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="proficiency_type"> Type </label>

                        <div class="col-sm-9">
                            <select class="form-control input-sm basic-info" name="proficiency_type"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="proficiency_rate"> Rate </label>

                        <div class="col-sm-3">
                            <input type="number" class="form-control input-sm basic-info" id="proficiency_rate" name="proficiency_rate">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="remarks"> Remarks </label>

                        <div class="col-sm-9">
                            <textarea class="form-control input-sm basic-info" id="remarks" name="remarks" rows="5" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="form-row align-right" >
                        <button class="btn btn-info btn-sm">
                            <i class="ace-icon fa fa-save bigger-120"></i>
                            Apply
                        </button>
                        <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@section('proficiency_modal_scripts')
<script type="text/javascript">
    var proficiency_type_select = $("#proficiency-form [name='proficiency_type']").makeSelectize({
        lookup : '{{ config('iapms.lookups.auditor.proficiency') }}',
    })[0].selectize;

    $("#proficiency-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#proficiency-form")[0].reset();
                $("#proficiency-modal").modal('hide');
                proficiency_type_select.clear();
                proficiency_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#proficiency-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on("click","#proficiency-table .edit",function(){
        var row = $(this).parent().parent();
        var data = proficiency_datatable.row( row ).data();
        $("#proficiency-form").attr('action',lcm_url +"/" + data.auditor_lcm_id);
        $("#proficiency-form").attr('method', "PUT" ).supply(data);
        proficiency_type_select.setValue(data.proficiency_type)
        $("#proficiency-modal").modal('show');
        return false;
    });

</script>
@endsection