<div class="modal fade" data-backdrop="static" id="training-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Training Form</h4>
            </div>
            <div class="modal-body">
                <div id="training-alert" class="mrgB15"></div>
                <form class="form-horizontal" id="training-form">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="training_type"> Type </label>
                        <div class="col-sm-6">
                            <select class="form-control input-sm basic-info" id="training_type" name="training_type">
                                <option value=""></option>
                                @foreach($trainings as $training)
                                    <option value="{{$training->lookup_code}}" data-desc="{{$training->description}}">{{$training->meaning}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="description"> Training Title/Description </label>

                        <div class="col-sm-6">
                            <textarea class="form-control input-sm basic-info" id="description" name="description" rows="5" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="venue"> Venue </label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control input-sm basic-info" id="venue" name="venue">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="completion_date"> Training Date Completed </label>

                        <div class="col-sm-6">
                            <input type="text" class="form-control input-sm basic-info date-picker" id="completion_date" name="completion_date" data-date-format="dd-M-yyyy">
                        </div>
                    </div>
                    <div class="form-row align-right" >
                        <button class="btn btn-info btn-sm">
                            <i class="ace-icon fa fa-save bigger-120"></i>
                            Apply
                        </button>
                        <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@section('training_modal_scripts')
<script type="text/javascript">
    var training_type_select = $("#training-form [name='training_type']").makeSelectize({
        lookup : '{{ config('iapms.lookups.auditor.training') }}',
    })[0].selectize;

    $("#training-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#training-form")[0].reset();
                $("#training-modal").modal('hide');
                training_type_select.clear();
                training_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#training-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on("click","#training-table .edit",function(){
        var row = $(this).parent().parent();
        var data = training_datatable.row( row ).data();
        $("#training-form").attr('action',training_url +"/" + data.auditor_training_id);
        $("#training-form").attr('method', "PUT" ).supply(data);
        training_type_select.setValue(data.training_type)
        $("#training-modal").modal('show');
        return false;
    });

</script>
@endsection
