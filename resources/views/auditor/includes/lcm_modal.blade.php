<div class="modal fade" data-backdrop="static" id="lcm-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">License / Certification / Membership Form</h4>
            </div>
            <div class="modal-body">
                <div id="lcm-alert"></div>
                <form id="lcm-form" class="form-horizontal padT15">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="lcm_type"> Type </label>
                        <div class="col-sm-9">
                            <select class="form-control input-sm basic-info" id="lcm_type" name="lcm_type"></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="ref_no"> License / Certification Number </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm basic-info" id="ref_no" name="ref_no">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="remarks"> Remarks </label>

                        <div class="col-sm-9">
                            <textarea class="form-control input-sm basic-info" id="remarks" name="remarks" rows="5" style="resize: none;"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="issued_date"> Issued Date </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm basic-info date-picker" id="issued_date" name="issued_date" data-date-format="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="expiry_date"> Expiry Date </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm basic-info date-picker" id="expiry_date" name="expiry_date" data-date-format="dd-mm-yyyy">
                        </div>
                    </div>
                    <div class="form-row align-right" >
                        <button class="btn btn-info btn-sm">
                            <i class="ace-icon fa fa-save bigger-120"></i>
                            Apply
                        </button>
                        <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@section('lcm_modal_scripts')
<script type="text/javascript">
    var lcm_type_select = $("#lcm-form [name='lcm_type']").makeSelectize({
        lookup : '{{ config('iapms.lookups.auditor.lcm') }}',
    })[0].selectize;

    $("#lcm-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#lcm-form")[0].reset();
                $("#lcm-modal").modal('hide');
                lcm_type_select.clear();
                lcm_datatables.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#lcm-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on("click","#lcm-table .edit",function(){
        var row = $(this).parent().parent();
        var data = lcm_datatables.row( row ).data();
        $("#lcm-form").attr('action',lcm_url +"/" + data.auditor_lcm_id);
        $("#lcm-form").attr('method', "PUT" ).supply(data);
        lcm_type_select.setValue(data.lcm_type)
        $("#lcm-modal").modal('show');
        return false;
    });


</script>
@endsection