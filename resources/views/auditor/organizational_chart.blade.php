@extends('layouts.app')
@section('styles')
<link rel="stylesheet" href="{{ asset('css/jquery.orgchart.min.css') }}">
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header',[ 'heading' => "Auditor Organizational Chart"])

            <div class="row">
                <div class="col-xs-12">
                    <div style="overflow-x: scroll;overflow-y: hidden;">
                        <div id="chart-container"></div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script type="text/javascript" src="{{ asset('js/jquery.orgchart.min.js') }}"></script>
<script type="text/javascript">
    var datascource = JSON.parse('{!! json_encode($data) !!}');
    var datascource = {
      'name': 'Organizational Chart',
      'title': 'Start Here',
      'children': datascource
    };
    console.log(datascource);
    $('#chart-container').orgchart({
      'data' : datascource,
      'nodeContent': 'title'
    });

</script>
@endsection
