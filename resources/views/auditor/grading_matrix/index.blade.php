@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary"  data-toggle="modal" href='#grading-matrix-modal'>
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Auditor Grading Matrix
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="grading-matrix-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Variance From</th>
                                <th>Variance To</th>
                                <th>Timeliness Max</th>
                                <th>Quality Max</th>
                                <th>Documentation Max</th>
                                <th>Remarks</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<!-- Modal form -->
<div class="modal fade" data-backdrop="static" id="grading-matrix-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Grading Matrix Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning alert-modal"></div>
                {!! Form::open(['class' => 'form-horizontal', 'id' => 'grading-matrix-form']) !!}
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="orig_from" value="">
                    <input type="hidden" name="orig_to" value="">
                    <input type="hidden" name="id" value="">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="variance_from"> Variance From </label>
                        <div class="col-sm-3">
                            <input type="number" name="variance_from" id="variance_from" class="form-control input-sm basic-info" value="" required="required">
                        </div>

                        <label class="col-sm-2 control-label" for="variance_to"> Variance To </label>
                        <div class="col-sm-3">
                            <input type="number" name="variance_to" id="variance_to" class="form-control input-sm basic-info" value="" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="rating"> Timeliness Max </label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control" id="rating" name="rating" step=".25">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="quality_max"> Quality Max </label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control" id="quality_max" name="quality_max" step=".5">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="documentation_max"> Documentation Max </label>
                        <div class="col-sm-3">
                            <input type="number" class="form-control" id="documentation_max" name="documentation_max" step=".5">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="remarks"> Remarks </label>
                        <div class="col-sm-9">
                            <textarea class="form-control" id="remarks" name="remarks" rows="3" style="resize: none;"></textarea>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Modal form -->
@endsection

@section('footer_script')
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var _token  = '{{ csrf_token() }}';
    var $alert  = $('.alert-modal'),
        form    = $('#grading-matrix-form'),
        $modal  = $('#grading-matrix-modal');
    var datatable= $('#grading-matrix-table').DataTable( {
        ajax: "{{route('auditor.grading_factors.list')}}",
        "processing": true,
        "serverSide": true,
        orderCellsTop: true,
        "searching": false,
        columns: [
            { data: "variance_from" },
            { data: "variance_to" },
            { data: "rating" },
            { data: "quality_max" },
            { data: "documentation_max" },
            { data: "remarks", defaultContent : 'N/A', orderable: false, searchable: false },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="btn-edit" data-id="'+data.id+'" data-variance_from="'+data.variance_from+'" data-variance_to="'+data.variance_to+'" data-rating="'+data.rating+'" data-quality_max="'+data.quality_max+'" data-documentation_max="'+data.documentation_max+'" data-remarks="'+data.remarks+'" title="Edit" rel="tooltip"><i class="fa fa-pencil"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.id+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    function init_form() {
        // fill modal form data
        $modal.find('form').prop('action', '');
        $modal.find('input[name="_method"]').val('post');
        $modal.find('input[name="id"]').val('');
        $modal.find('input[name="orig_from"]').val('');
        $modal.find('input[name="orig_to"]').val('');
        $modal.find('#variance_from').val('');
        $modal.find('#variance_to').val('');
        $modal.find('#rating').val('');
        $modal.find('#quality_max').val('');
        $modal.find('#documentation_max').val('');
        $modal.find('#remarks').val('');
        $modal.find('.basic-info').parent().parent().removeClass('has-error');
        $alert.hide();
    }

    $modal.on('hidden.bs.modal', function(){
        init_form();
    });

    $(document).on('click', '.btn-edit', function() {
        var from = $(this).attr('data-variance_from');
        var to = $(this).attr('data-variance_to');
        var rating = $(this).attr('data-rating');
        var quality_max = $(this).attr('data-quality_max');
        var documentation_max = $(this).attr('data-documentation_max');
        var remarks = $(this).attr('data-remarks');
        var id = $(this).attr('data-id');

        // fill modal form data
        $modal.find('form').prop('action', '{{route('auditor.grading_factors.update')}}');
        $modal.find('input[name="_method"]').val('put');
        $modal.find('input[name="id"]').val(id);
        $modal.find('input[name="orig_from"]').val(from);
        $modal.find('input[name="orig_to"]').val(to);
        $modal.find('#variance_from').val(from);
        $modal.find('#variance_to').val(to);
        $modal.find('#rating').val(rating);
        $modal.find('#quality_max').val(quality_max);
        $modal.find('#documentation_max').val(documentation_max);
        $modal.find('#remarks').val(remarks);
        $modal.modal('show');

        return false;
    });

    $(document).on('click', '.btn-save', function() {
        swal({
            title: "Continue?",
            text: "You are about to save changes for grading matrix.",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        location.reload();
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.has-error').removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }
                }
            });
        });
    });

    // remove auditor
    $(document).on('click', '.btn-delete', function() {
        var id = $(this).attr('data-id');
        swal({
            title: "Continue?",
            text: "You are about to remove a grading matrix.",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $.ajax({
                url: '{{ url('auditor/grading_factors/delete') }}/'+id,
                method: "POST",
                data: { _token : _token, _method : 'delete' },
                success : function(data) {
                    if( data.success ) {
                        location.reload();
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    // displays the validation error
                    var msg = '<ul class="list-unstyled">';

                    $.each(xhr.responseJSON, function(key, val){
                        for(var i=0; i < val.length; i++) {
                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                        }
                    });
                    msg += '</ul>';

                    $('.alert-warning').html(msg).show();
                }
            });
        });

        return false;
    });
</script>
@endsection
