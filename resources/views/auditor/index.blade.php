@extends('layouts.app')
@section('styles')

@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{route('auditor.create')}}">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Auditor
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="auditor-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Employee #</th>
                                <th>Gender</th>
                                <th>Position</th>
                                <th>Email Address</th>
                                <th>Effective Start</th>
                                <th>Effective End</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@endsection

@section('footer_script')
<script src="/js/jquery.cookie.js"></script>
<script type="text/javascript">
    auditorPageLength = $.cookie('auditorPageLength') ? $.cookie('auditorPageLength') : 10;

    var auditor_datatable = $('#auditor-table').DataTable( {
        ajax : "{{route('auditor.list.lists')}}",
        processing : true,
        pageLength : auditorPageLength,
        columns : [
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    return data.first_name+' '+data.middle_name+' '+data.last_name;
                }
            },
            { data: "employee_number" },
            { data: "gender" },
            { data: "position_code" },
            { data: "email_address" },
            { data: "effective_start_date" },
            { data: "effective_end_date" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('auditor/edit')}}/'+data.auditor_id+'"><i class="fa fa-pencil edit" title="Edit" rel="tooltip"></i></a> &nbsp; <a href="{{url('auditor')}}/'+data.auditor_id+'" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    $(document).on("click","#auditor-table .delete",function(){
        $(this).deleteItemFrom(auditor_datatable);
        return false;
    });

    $(document).on('change','select[name=auditor-table_length]',function(){
        num = $(this).val();
        $.cookie('auditorPageLength', num);
    });

</script>
@endsection
