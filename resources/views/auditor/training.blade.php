<div class="col-xs-12 mrgB15">
    <button class="btn btn-default btn-sm" type="button" id="show-training-modal">
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Add
    </button>
    <div class="table-header">
        Training
    </div>

    <div>
        <table id="training-table" class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th width="20%">Type</th>
                <th width="30%">Training Title/Description</th>
                <th width="20%">Venue</th>
                <th width="20%">Date Completed</th>
                <th width="10%"></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@include('auditor.includes.training_modal', [ "auditor_id" => $auditor_id ] )
@section('training_scripts')
<script type="text/javascript">
    var training_url = "{{ url('auditor/'.$auditor_id.'/training' ) }}";

    var training_datatable = $("#training-table").DataTable( {
        bDestroy: true,
        ajax: training_url + "/list",
        processing: true,
        lengthMenu: [ 25, 50, 75, 100 ],
        processing: true,
        columns: [
            { data: "lv_training_type_meaning" },
            { data: "description", sortable: false },
            { data: "venue" },
            { data: "completion_date" },
            {
                data: null,
                className: 'row-control',
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="edit" rel="tooltip" data-original-title="Edit"><i class="fa fa-pencil"></i></a>    <a href="'+training_url+'/'+data.auditor_training_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    });

    $("#show-training-modal").click(function(){
        $("#training-form").attr('action',training_url);
        $("#training-form").attr('method', "POST" );
        $("#training-modal").modal('show');
    });

    $(document).on("click","#training-table .delete",function(){
        $(this).deleteItemFrom(training_datatable);
        return false;
    });

</script>
@yield('training_modal_scripts')
@endsection