<div class="col-xs-12">
    {!! Form::open(['method' => 'get', 'class' => 'form-horizontal', 'id' => 'plan-search', 'style' => 'margin-top: 12px;']) !!}
        <div class="form-group">
            <label class="col-sm-3 control-label" for="search"> Audit Annual Year </label>
            <div class="col-sm-2">
                <input type="text" name="search" id="search" class="form-control input-sm year-picker" placeholder="Audit Year" required value="{{ !empty($details['data']) ? $details['data']['year'] : '' }}">
            </div>
            <div class="col-sm-2">
                <button class="btn btn-default btn-sm" type="submit">
                    <i class="ace-icon fa fa-search"></i>
                    Go
                </button>
            </div>
        </div>
    {!! Form::close() !!}
</div>
<div id="plan-details" class="col-xs-12" style="padding-top: 10px;">
    <div class="table-header">
        Annual Audit Plan
    </div>
    <div style="display:none;" class="alert alert-warning alert-planning"></div>
    <div style="padding: 10px 0px;">
        <button class="btn btn-success btn-sm btn-create-plan {{ !empty($details['data']) ? 'btn-hide' : '' }}" type="button">
            <i class="ace-icon fa fa-plus"></i>
            Create
        </button>
        <button class="btn btn-primary btn-sm btn-save-plan btn-hide" type="button">
            <i class="ace-icon fa fa-save"></i>
            Save
        </button>
        <button class="btn btn-warning btn-sm btn-cancel-create-plan btn-hide" type="button">
            <i class="ace-icon fa fa-times"></i>
            Cancel
        </button>
        <button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#modal-wizard">
            <i class="ace-icon fa fa-list-ol"></i>
            Guide
        </button>
    </div>

    {!! Form::open(['url' => '','class' => 'form-horizontal', 'id' => 'planning-form']) !!}
        <div class="form-group">
            <label class="col-sm-3 control-label" for="audit_year"> Audit Annual Year </label>
            <div class="col-sm-2">
                <input type="text" name="audit_year" id="audit_year" class="form-control input-sm basic-info" placeholder="Audit Year" value="{{ !empty($details['data']) ? $details['data']['year'] : '' }}" required disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="name"> Plan Name </label>
            <div class="col-sm-6">
                <input type="text" name="name" id="name" class="form-control input-sm basic-info" value="{{ !empty($details['data']) ? $details['data']['name'] : '' }}" required disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="status"> Status </label>
            <div class="col-sm-2">
                <?php $status = config('iapms.lookups.planning.status'); ?>
                <select id="status" name="status" class="form-control input-sm" disabled>
                    @if( count($status) )
                        @foreach($status as $key => $val)
                            <option value="{{ $val['text'] }}" {{ !empty($details['data']) ? ($details['data']['status'] == $val['text'] ? 'selected' : '') : '' }}>{{ $val['text'] }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="remarks"> Remarks </label>
            <div class="col-sm-6">
                <textarea name="remarks" id="remarks" class="form-control basic-info" rows="3" style="resize: none;" disabled>{{ !empty($details['data']) ? $details['data']['remarks'] : '' }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="reviewed_by"> Reviewed By </label>
            <div class="col-sm-3">
                <input type="text" name="reviewed_by" id="reviewed_by" class="form-control input-sm" value="" disabled>
            </div>
            <label class="col-sm-1 control-label" for="reviewed_date"> Date </label>
            <div class="col-sm-2">
                <input type="text" name="reviewed_date" id="reviewed_date" class="form-control input-sm" value="" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="approved_by"> Approved By </label>
            <div class="col-sm-3">
                <input type="text" name="approved_by" id="approved_by" class="form-control input-sm" value="" disabled>
            </div>
            <label class="col-sm-1 control-label" for="approved_date"> Date </label>
            <div class="col-sm-2">
                <input type="text" name="approved_date" id="approved_date" class="form-control input-sm" value="" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="au_annual_mandays"> Auditor's Annual Mandays </label>
            <div class="col-sm-2">
                <input type="text" name="au_annual_mandays" id="au_annual_mandays" class="form-control input-sm" value="" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="au_running_mandays"> Auditor's Running Mandays </label>
            <div class="col-sm-2">
                <input type="text" name="au_running_mandays" id="au_running_mandays" class="form-control input-sm" value="" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="au_available_mandays"> Auditor's Available Mandays </label>
            <div class="col-sm-2">
                <input type="text" name="au_available_mandays" id="au_available_mandays" class="form-control input-sm" value="" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="reviewer_remarks"> Reviewer Remarks </label>
            <div class="col-sm-6">
                <textarea name="reviewer_remarks" id="reviewer_remarks" class="form-control" rows="3" style="resize: none;" disabled></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="approver_remarks"> Approver Remarks </label>
            <div class="col-sm-6">
                <textarea name="approver_remarks" id="approver_remarks" class="form-control" rows="3" style="resize: none;" disabled></textarea>
            </div>
        </div>
        @if( !empty($details['data']) )
            @if($project_status_count->total_count > 0 && $project_status_count->total_count == $project_status_count->reviewed)
            <div class="form-group" id="approval" class="edit-components">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-info btn-sm"><i class="fa fa-thumbs-up"></i> Approval</button>
                </div>
            </div>
            @endif
        @endif
    {!! Form::close() !!}
</div>
<div class="col-xs-12" style="padding-top: 10px;">
    <div class="table-header">
        Annual Audit Plan - Auditors
    </div>
    <div style="padding-top: 10px;">
        <button class="btn btn-success btn-sm btn-add-auditor btn-hide" type="button" data-toggle="modal" data-target='#auditors-modal'>
            <i class="ace-icon fa fa-plus"></i>
            Add
        </button>
    </div>
    <table id="datatable-plan-auditor" class="table table-striped table-bordered datatable">
        <thead>
            <tr>
                <th>Auditor Name</th>
                <th>Auditor Type</th>
                <th>Annual Mandays</th>
                <th>Running Mandays</th>
                <th>Available Mandays</th>
                <th width="7%"></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<!-- Auditors Modal -->
<div class="modal fade" data-backdrop="static" id="auditors-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Auditor Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning alert-plan-auditor"></div>
                {!! Form::open(['url' => !empty($details['data']) ? route('planning.store.auditor', ['plan_id' => $details['data']['id']]) : '', 'class' => 'form-horizontal', 'id' => 'auditor-form']) !!}
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="auditor_name"> Auditor Name </label>
                        <div class="col-sm-8">
                            <select id="auditor_name" name="auditor_name" class="form-control input-sm auditor-name basic-info-modal" data-placeholder="Click to Choose...">
                                <option value="">---</option>
                                {{-- @foreach($auditors as $auditor)
                                <option value="{{$auditor->auditor_id}}">{{$auditor->full_name}}</option>
                                @endforeach --}}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="auditor_type"> Auditor Type </label>
                        <div class="col-sm-6">
                            <select id="auditor_type" name="auditor_type" class="form-control input-sm auditor-type basic-info-modal" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="annual_mandays"> Annual Mandays </label>
                        <div class="col-sm-2">
                            <input type="text" name="annual_mandays" id="annual_mandays" class="form-control input-sm" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="running_mandays"> Running Mandays </label>
                        <div class="col-sm-2">
                            <input type="text" name="running_mandays" id="running_mandays" class="form-control input-sm" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="available_mandays"> Available Mandays </label>
                        <div class="col-sm-2">
                            <input type="text" name="available_mandays" id="available_mandays" class="form-control input-sm" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="proficiency"> Proficiency </label>
                        <div class="col-sm-6" id="proficiency"></div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-plan-auditor">Save changes</button>
            </div>
        </div>
    </div>
</div>

<!-- /Auditors Modal -->
@section('plan_js')
<script type="text/javascript">
    var $btn_create_plan = $('.btn-create-plan'),
        $btn_save_plan = $('.btn-save-plan'),
        $btn_cancel_plan = $('.btn-cancel-create-plan'),
        audit_yr = $('#audit_year'),
        $btn_approval = $('#approval'),
        $btn_add_auditor = $('.btn-add-auditor'),
        $inputs = plan.find('.basic-info'),
        $alert = $('.alert-planning'),
        $form = $('#planning-form');

    var _proficiencies = [];
    var auditor_queue = [];
    var create = false;
    var current_status = '{{ !empty($details['data']) ? $details['data']['status'] : '' }}';
    var current_data = {!! !empty($details['data']) ? json_encode($details['data']) : '[]' !!};
    var plan_id = '{{ !empty($details['data']) ? $details['data']['id'] : '0' }}';

    $(function(){
        $(".auditor-name").select2();
        iapms.LookupCodeSelect2($('.auditor-type'), '{{ config('iapms.lookups.auditor_type') }}');

        auditorNameOnSelect($('#plan'));

        $('.select2-container').css('width', '100%');
        $('[data-toggle="popover"]').popover();

        generateMandays();
    });

    function auditorNameOnSelect(tab) {
        tab.find(".auditor-name").on('select2:select', function () {
            getAuditorMandays(plan_id, $(this).val(), tab);
        });
    }

    function generateMandays() {
        $('#au_annual_mandays').val(sumMandays($plan_auditor_datatable, 2));
        $('#au_running_mandays').val(sumMandays($plan_auditor_datatable, 3));
        $('#au_available_mandays').val(sumMandays($plan_auditor_datatable, 4));
    }

    function populateFields(data) {
        audit_yr.val( data === false ? '' : data.year );
        $form.find('#name').val( data === false ? '' : data.name );
        $form.find('#status').val( data === false ? '' : data.status );
        $form.find('#remarks').val( data === false ? '' : data.remarks );
        $form.find('#reviewed_by').val( data === false ? '' : data.reviewer );
        $form.find('#reviewed_date').val( data === false ? '' : data.review_date );
        $form.find('#approved_by').val( data === false ? '' : data.approver );
        $form.find('#approved_date').val( data === false ? '' : data.approve_date );
        $form.find('#approver_remarks').val( data === false ? '' : data.approver_remarks );
        $form.find('#reviewer_remarks').val( data === false ? '' : data.reviewer_remarks );
        if( data !== false ) generateMandays();
    }

    var $plan_auditor_datatable = $('#datatable-plan-auditor').DataTable( {
        data : {!! !empty($details['data']) ? json_encode($details['data']['auditors']['data']) : '[]' !!},
        "lengthMenu": [ 25, 50, 75, 100 ],
        "processing": true,
        rowId: 'id',
        columns: [
            { data: "name" },
            { data: "type" },
            { data: "mandays.annual", defaultContent : 'N/A' },
            { data: "mandays.running", defaultContent : 'N/A' },
            { data: "mandays.available", defaultContent : 'N/A' },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var prof = '';
                    if( typeof data.proficiencies === "object" && typeof data.proficiencies.length === "undefined" ) {
                        prof += '<ul class=\'list-unstyled spaced2\'>';
                        $.each(data.proficiencies, function(i, v){
                            prof += '<li><i class=\'fa fa-angle-right\'></i> '+i+'-'+v+'</li>';
                        });
                        prof += '</ul>';
                    }
                    else {
                        prof = '<span class=\'label label-white middle label-lg\'><i class=\'fa fa-info-circle\'></i> No proficiency available</span>'
                    }
                    return '<a href="javascript:void(0);" class="btn-auditor-prof" data-toggle="popover" data-content="'+prof+'" data-placement="left" data-html="true" data-trigger="hover" title="Proficiencies"><i class="fa fa-address-card"></i></a> &nbsp; <a href="#" class="btn-delete-auditor delete btn-hide" data-id="'+data.id+'" data-name="'+data.name+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    function getAuditorMandays(planning_id, auditor_id, tab) {
        $.ajax({
            url: '{{ url('planning') }}/'+planning_id+'/auditor/'+auditor_id+'/mandays',
            method: "GET",
            data: { _token : '{{csrf_token()}}' },
            beforeSend: function() {
                swal({
                    title: "Loading auditor's information",
                    text: "Please wait...",
                    type: "info",
                    showConfirmButton: false
                });
            },
            success : function(response) {
                swal.close();
                if( response.success ) {
                    var data = response.data;

                    tab.find('#annual_mandays').val(data.mandays.annual);
                    tab.find('#running_mandays').val(data.mandays.running);
                    tab.find('#available_mandays').val(data.mandays.available);

                    var proficiencies = '';
                    tab.find('#proficiency').html(proficiencies);
                    if( typeof data.proficiencies === "object" && typeof data.proficiencies.length === "undefined" ) {
                        _proficiencies = data.proficiencies;
                        $.each(data.proficiencies, function(i, v){
                            proficiencies += '<span class="label label-white middle label-lg">'+i+' - '+v+'</span> &nbsp;';
                        });
                    }
                    else {
                        proficiencies = '<span class="label label-white middle label-lg"><i class="fa fa-info-circle"></i> No proficiency available</span> &nbsp;';
                    }
                    tab.find('#proficiency').html(proficiencies);
                }
            }
        });
    }

    $btn_create_plan.on('click', function(){
        var date = new Date();
        var input = true;

        clear_form();
        init_plan();
        create = true;
        toggle_plan_input(input);

        audit_yr.datetimepicker({
            pickTime: false,
            viewMode: "years",
            minViewMode: "years",
            format: 'YYYY',
            autoclose: true,
            minDate: date.getFullYear()
        });
    });

    function toggle_plan_input(update) {
        if( update ) {
            if( !edit ) {
                $btn_create_plan.addClass('btn-hide'); // if edit mode is true do not show create button
                $btn_cancel_plan.removeClass('btn-hide');
            }
            $btn_save_plan.removeClass('btn-hide');
            if( !create ) $btn_add_auditor.removeClass('btn-hide');
            $('.btn-delete-auditor').removeClass('btn-hide');
            $inputs.attr('disabled', false);

            go_to('#plan-details');
            plan.find('#audit_year').focus();
        }
        else {
            clear_form();
            init_plan();

            if( !edit ) {
                $btn_create_plan.removeClass('btn-hide'); // if edit mode is true do not show create button
                $btn_cancel_plan.addClass('btn-hide');
            }
            $btn_save_plan.addClass('btn-hide');
            if( !create ) $btn_add_auditor.addClass('btn-hide');
            $('.btn-delete-auditor').addClass('btn-hide');

            if( current_status != '' ) {
                $plan_auditor_datatable.rows.add(current_data.auditors.data).draw();
                populateFields(current_data);
            }

            $inputs.attr('disabled', true);
            $btn_approval.hide();
        }
    }

    function init_plan() {
        create = false;

        if( current_status != '' )  $plan_auditor_datatable.clear().draw();
    }

    function clear_form() {
        $alert.hide();
        $inputs.parent().parent().removeClass('has-error');

        $form.find('.form-control').val('');
        $form.find('select#status').val('New');
    }

    function clear_auditor_form() {
        $('.alert-plan-auditor').hide();
        $('.alert-plan-auditor').find('.basic-info-modal').parent().parent().removeClass('has-error');

        $('#auditor-form').find('.form-control').val('');
        plan.find('#auditor_name').val('').trigger('change');
        plan.find('#proficiency').html('');
        plan.find('#auditor_type').val('').trigger('change');
    }

    $btn_cancel_plan.on('click', function() {
        toggle_plan_input(false);
    });

    // Save changes from planning
    $btn_save_plan.on('click', function(){
        swal({
            title: "Continue?",
            text: "You are about to add a new planning for the year "+audit_yr.val(),
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            var action = edit ? '{{ url('planning') }}/update/'+plan_id : '';
            var method = edit ? 'put' : 'post';
                $.ajax({
                url: action,
                method: "POST",
                data: $form.serialize()+'&auditors='+JSON.stringify(auditor_queue)+'&_method='+method+'&id='+plan_id, // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        if( typeof data.redirect != "undefined" && data.redirect != false )
                            window.location = data.redirect; // redirect if the planning year change on update
                        else
                            location.reload();
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();

                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }
                }
            });
        });
    });

    $('.btn-save-plan-auditor').on('click', function(){
        swal({
            title: "Continue?",
            text: "You are about to add an auditor to this plan",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $.ajax({
                url: $('#auditor-form').attr('action'),
                method: "POST",
                data: $('#auditor-form').serialize(), // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        $plan_auditor_datatable.clear().rows.add(data.data).draw();
                        if(edit) plan.find('.btn-delete-auditor').removeClass('btn-hide');
                        generateMandays();
                        swal('Success!', 'Auditor is successfully added to the plan.', 'success');
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();

                    if( xhr.status == 500 ) {
                        $('.alert-plan-auditor').removeClass('alert-warning').addClass('alert-danger');
                        $('.alert-plan-auditor').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info-modal').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $('.alert-plan-auditor').removeClass('alert-danger').addClass('alert-warning');
                        $('.alert-plan-auditor').html(msg).show();
                    }
                }
            });
            clear_auditor_form();
        });
    });

    $('body').on('click', '.btn-delete-auditor', function(){
        var remove_id = $(this).attr('data-id');
        var remove_name = $(this).attr('data-name');
        var row = $(this).parents('tr');

        swal({
            title: "Continue?",
            text: "You are about to delete "+remove_name+" to this plan.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            @if( !empty($details['data']) )
            if( !create ) {
                $.ajax({
                    url: '{{ url('planning') }}/{{$details['data']['id']}}/auditor/'+remove_id+'/delete',
                    method: "POST",
                    data: { _token : '{{csrf_token()}}', _method : 'delete' }, // serializes all the form data
                    success : function(data) {
                        delete_success(row);
                    }
                });
            }
            @endif

            if( create ) {
                setTimeout(function(){
                    delete_success(row);
                }, 300);
            }
        });

        return false;
    });

    function delete_success(row) {
        var id = $plan_auditor_datatable.row(row).id();
        var q_index = auditor_queue.indexOf(id);

        if(q_index != -1) auditor_queue.splice(q_index, 1);
        $plan_auditor_datatable.row(row).remove().draw();

        generateMandays();
        swal('Success!', 'Auditor is successfully deleted in the plan.', 'success');
    }

    $('.btn-cancel-plan-edit').on('click', function () {
        $('.plan-save-cancel').hide();
        $('.plan-edit').show();
        plan.find('#planning-form .basic-info').attr('readonly', true);

        return false;
    });

    $('.btn-edit-plan').on('click', function () {
        $('.plan-save-cancel').show();
        $('.plan-edit').hide();
        plan.find('#planning-form .basic-info').attr('readonly', false);

        return false;
    });

    plan.find('#auditors-modal').on('show.bs.modal', function (e) {
        getTabAuditors('plan', 0, plan.find('#auditor_name'));
    });

    plan.find('#auditors-modal').on('hidden.bs.modal', function (e) {
        clear_auditor_form();
    });
</script>
@endsection