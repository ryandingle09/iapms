<div class="col-xs-12">
    <div class="table-header">
        Annual Audit Plan - Projects
    </div>
    @if(!empty($details['data']))
    <button class="btn btn-success btn-sm btn-create-project edit-components" data-type="create" type="button">
        <i class="ace-icon fa fa-plus"></i>
        Create
    </button>
    @endif
    <table id="datatable-projects" class="table table-striped table-bordered datatable table-hover">
        <thead>
            <tr>
                <th>Project Name</th>
                <th>Audit Type</th>
                <th>Status</th>
                <th width="5%"></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-12" style="padding-top: 10px;">
    <div class="table-header" id="project-details">
        Projects Details
    </div>
    <div class="pull-right">
        <div class="project-approve-reject" style="display: none;">
            <button class="btn btn-success btn-sm btn-project-status-approve" type="button">
                <i class="ace-icon fa fa-thumbs-o-up"></i>
                <span></span>
            </button>
            <button class="btn btn-danger btn-sm btn-project-status-reject" type="button">
                <i class="ace-icon fa fa-thumbs-o-down"></i>
                Reject
            </button>
        </div>
        <div class="save-cancel" style="display: none;">
            <button class="btn btn-primary btn-sm btn-save-project" type="button">
                <i class="ace-icon fa fa-save"></i>
                Save
            </button>
            <a href="#" class="btn btn-warning btn-sm btn-cancel-project">
                <i class="ace-icon fa fa-times"></i>
                Cancel
            </a>
        </div>
    </div>
    <div style="display:none;" class="alert alert-warning alert-project"></div>
    {!! Form::open(['url' => (!empty($details['data']) ? route('planning.project.store', [ 'plan_id' => $details['data']['id'] ]) : ''), 'class' => 'form-horizontal', 'id' => 'project-form']) !!}
        <input type="hidden" name="project_id" id="project_id" value="" data-tag="id">
        <input type="hidden" name="_method" value="post">
        <div class="form-group">
            <label class="col-sm-3 control-label" for="project_name"> Project Name </label>
            <div class="col-sm-6">
                <input type="text" name="project_name" id="project_name" class="form-control input-sm basic-info" value="" data-tag="name" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="audit_type"> Audit Type </label>
            <div class="col-sm-6">
                <select id="audit_type" name="audit_type" class="form-control input-sm basic-info select2" data-placeholder="Click to Choose..."></select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="project_status"> Status </label>
            <div class="col-sm-2">
                <?php $status = config('iapms.lookups.planning.status'); ?>
                <select id="project_status" name="project_status" class="form-control input-sm" data-tag="status" required disabled>
                    @if( count($status) )
                        @foreach($status as $key => $val)
                            <option value="{{ $val['text'] }}">{{ $val['text'] }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"> Target Date </label>
            <div class="col-sm-3">
                <input type="text" name="start_date" id="start_date" placeholder="Start date" autocomplete="false" class="form-control input-sm basic-info date-picker" value="" data-date-format="dd-M-yyyy" required>
            </div>
            <div class="col-sm-3">
                <input type="text" name="end_date" id="end_date" placeholder="End date" autocomplete="false" class="form-control input-sm basic-info date-picker" value="" data-date-format="dd-M-yyyy" required>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="remarks"> Remarks </label>
            <div class="col-sm-6">
                <textarea name="remarks" id="remarks" class="form-control basic-info" rows="3" style="resize: none;" disabled></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="project_head"> Project Head </label>
            <div class="col-sm-6">
                <select id="project_head" name="project_head" class="form-control input-sm basic-info select2" data-placeholder="Click to Choose..."></select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="reviewed_by"> Reviewed By </label>
            <div class="col-sm-3">
                <input type="text" name="reviewed_by" id="reviewed_by" class="form-control input-sm" value="" disabled>
            </div>
            <label class="col-sm-1 control-label" for="reviewed_date"> Date </label>
            <div class="col-sm-2">
                <input type="text" name="reviewed_date" id="reviewed_date" class="form-control input-sm" value="" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="approved_by"> Approved By </label>
            <div class="col-sm-3">
                <input type="text" name="approved_by" id="approved_by" class="form-control input-sm" value="" disabled>
            </div>
            <label class="col-sm-1 control-label" for="approved_date"> Date </label>
            <div class="col-sm-2">
                <input type="text" name="approved_date" id="approved_date" class="form-control input-sm" value="" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="project_mandays"> Project Mandays </label>
            <div class="col-sm-2">
                <input type="text" name="project_mandays" id="pr_project_mandays" class="form-control input-sm" value="" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="running_mandays"> Running Allotted Mandays </label>
            <div class="col-sm-2">
                <input type="text" name="running_mandays" id="pr_running_mandays" class="form-control input-sm" value="" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="available_mandays"> Available Mandays </label>
            <div class="col-sm-2">
                <input type="text" name="available_mandays" id="pr_available_mandays" class="form-control input-sm" value="" disabled>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="reviewer_remarks"> Reviewer Remarks </label>
            <div class="col-sm-6">
                <textarea name="reviewer_remarks" id="reviewer_remarks" class="form-control" rows="3" style="resize: none;" disabled></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label" for="approver_remarks"> Approver Remarks </label>
            <div class="col-sm-6">
                <textarea name="approver_remarks" id="approver_remarks" class="form-control" rows="3" style="resize: none;" disabled></textarea>
            </div>
        </div>
        <div class="form-group" id="project-approval" style="display: none;"}>
            <div class="col-sm-12">
                <button type="button" class="btn btn-info btn-sm"><i class="fa fa-thumbs-up"></i> Approval</button>
            </div>
        </div>
    {!! Form::close() !!}
</div>
<div class="col-xs-12">
    <div class="pull-right">
        <div class="project-approve-reject" style="display: none;">
            <button class="btn btn-success btn-sm btn-project-status-approve" type="button">
                <i class="ace-icon fa fa-thumbs-o-up"></i>
                <span></span>
            </button>
            <button class="btn btn-danger btn-sm btn-project-status-reject" type="button">
                <i class="ace-icon fa fa-thumbs-o-down"></i>
                Reject
            </button>
        </div>
        <div class="save-cancel" style="display: none;">
            <button class="btn btn-primary btn-sm btn-save-project" type="button">
                <i class="ace-icon fa fa-save"></i>
                Save
            </button>
            <a href="#" class="btn btn-warning btn-sm btn-cancel-project">
                <i class="ace-icon fa fa-times"></i>
                Cancel
            </a>
        </div>
    </div>
</div>
<div class="col-xs-12" style="padding-top: 10px;">
    <div class="table-header">
        Project Auditors
    </div>
    <div style="padding-top: 10px;">
        <button class="btn btn-success btn-sm btn-add-proj-auditor" type="button" data-toggle="modal" data-target='#project-auditors-modal' {!! empty($details['data']['projects']['data']) ? 'style="display: none;"' : '' !!}>
            <i class="ace-icon fa fa-plus"></i>
            Add
        </button>
    </div>
    <table id="datatable-project-auditors" class="table table-striped table-bordered datatable">
        <thead>
            <tr>
                <th>Auditor Name</th>
                <th>Auditor Type</th>
                <th>Allotted Mandays</th>
                <th>Running Mandays</th>
                <th>Available Mandays</th>
                <th width="7%"></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<!-- Auditors Modal -->
<div class="modal fade" data-backdrop="static" id="project-auditors-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Auditor Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning alert-project-auditor"></div>
                {!! Form::open(['class' => 'form-horizontal', 'id' => 'project-auditor-form']) !!}
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="auditor_name"> Auditor Name </label>
                        <div class="col-sm-8">
                            <select id="auditor_name" name="auditor_name" class="form-control input-sm basic-info-modal auditor-name" data-placeholder="Click to Choose...">
                                <option value="">---</option>
                                {{-- @foreach($auditors as $auditor)
                                <option value="{{$auditor->auditor_id}}">{{$auditor->full_name}}</option>
                                @endforeach --}}
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="auditor_type"> Auditor Type </label>
                        <div class="col-sm-6">
                            <select id="auditor_type" name="auditor_type" class="form-control input-sm basic-info-modal auditor-type" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="allotted_mandays"> Allotted Mandays </label>
                        <div class="col-sm-2">
                            <input type="number" name="allotted_mandays" id="allotted_mandays" step="1" min="1" class="form-control input-sm basic-info-modal" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="running_mandays"> Running Mandays </label>
                        <div class="col-sm-2">
                            <input type="text" name="running_mandays" id="running_mandays" class="form-control input-sm" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="available_mandays"> Available Mandays </label>
                        <div class="col-sm-2">
                            <input type="text" name="available_mandays" id="available_mandays" class="form-control input-sm" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="proficiency"> Proficiency </label>
                        <div class="col-sm-6" id="proficiency"></div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-project-auditor">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Auditors Modal -->
@section('project_js')
<script type="text/javascript">
    var project_id = 0;
    var project_approval = projects.find('#project-approval');
    var statuses = ['New', 'For Review', 'Reviewed', 'For Approval', 'Approved', 'Closed' ];
    var $projects_datatable = $('#datatable-projects').DataTable( {
        data : {!! !empty($details['data']) ? json_encode($details['data']['projects']['data']) : '[]' !!},
        "lengthMenu": [ 25, 50, 75, 100 ],
        "processing": true,
        rowId: 'id',
        columns: [
            { data: "name" },
            { data: "audit_type" },
            { data: "status" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var prof = '';
                    if( data.status == 'New' ) return '<a href="#" class="btn-delete-project-auditor delete btn-hide" data-id="'+data.id+'" data-name="'+data.name+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    else return '<i class="fa fa-ban"></i>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    var $project_auditors_datatable = $('#datatable-project-auditors').DataTable( {
        data : [],
        "lengthMenu": [ 25, 50, 75, 100 ],
        "processing": true,
        rowId: 'id',
        drawCallback: function() {
            $('[data-toggle="popover"]').popover();
        },
        columns: [
            { data: "name" },
            { data: "type" },
            { data: "mandays.alloted" },
            { data: "mandays.running", defaultContent : 'N/A' },
            { data: "mandays.available", defaultContent : 'N/A' },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var prof = '';
                    if( typeof data.proficiencies === "object" && typeof data.proficiencies.length === "undefined" ) {
                        prof += '<ul class=\'list-unstyled spaced2\'>';
                        $.each(data.proficiencies, function(i, v){
                            prof += '<li><i class=\'fa fa-angle-right\'></i> '+i+'-'+v+'</li>';
                        });
                        prof += '</ul>';
                    }
                    else {
                        prof = '<span class=\'label label-white middle label-lg\'><i class=\'fa fa-info-circle\'></i> No proficiency available</span>'
                    }
                    return '<a href="javascript:void(0);" class="btn-auditor-prof" data-toggle="popover" data-content="'+prof+'" data-placement="left" data-html="true" data-trigger="hover" title="Proficiencies"><i class="fa fa-address-card"></i></a> &nbsp; <a href="#" class="btn-delete-project-auditor delete edit-components" data-id="'+data.id+'" data-name="'+data.name+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    $(function(){
        iapms.LookupCodeSelect2($('#audit_type'), '{{ config('iapms.lookups.audit_type') }}');
        $('.select2-container').css('width', '100%');
        @if(!empty( $details['data'] ))
        getProjectHeads({{$details['data']['id']}}); // set up project heads select2
        @endif
        clear_project_form();
        $('.select2-container').css('width', '100%');
    });
    @if( !empty($details['data']) )
    dtClickable(projects, $('#datatable-projects'), $projects_datatable, 'id', setupProjectDetails);

    function generateProjectMandays() {
        $('#pr_project_mandays').val(sumMandays($project_auditors_datatable, 2));
        $('#pr_running_mandays').val(sumMandays($project_auditors_datatable, 3));
        $('#pr_available_mandays').val(sumMandays($project_auditors_datatable, 4));
    }

    function setupProjectDetails(data) {
        project_id = data.id;
        $('#project-auditor-form').attr('action', '{{ url('planning/project') }}/' + project_id + '/auditor');
        projects.find('.basic-info').attr('disabled', true);

        if( data.status == 'New' ) {
            if( edit ) {
                projects.find('.save-cancel').show();
                projects.find('.btn-add-proj-auditor').show();
                projects.find('.basic-info').attr('disabled', false);
                project_approval.show();
                go_to('#project-details');
                projects.find('#project_name').focus();
                auditorNameOnSelect(projects);
                $project_auditors_datatable.column(-1).visible(true);
            }
        }
        else {
            if( edit ) {
                if (data.status == 'For Review') projects.find('#reviewer_remarks').attr('disabled', false);
                if (data.status == 'For Approval') projects.find('#approver_remarks').attr('disabled', false);
            }
            projects.find('.save-cancel').hide();
            projects.find('.btn-add-proj-auditor').hide();
            project_approval.hide();
            $project_auditors_datatable.column(-1).visible(false);
        }

        setAuditorsList(data.id, $project_auditors_datatable, generateProjectMandays);
        setScopeData(data.scopes);
        setFormAction('put');
    }

    $('body').on('click', '.btn-delete-project-auditor', function(){
        var remove_id = $(this).attr('data-id');
        var remove_name = $(this).attr('data-name');
        var row = $(this).parents('tr');

        swal({
            title: "Continue?",
            text: "You are about to delete "+remove_name+" to this project.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            @if( !empty($details['data']) )
            if( !create ) {
                $.ajax({
                    url: '{{ url('planning') }}/project/'+project_id+'/auditor/'+remove_id+'/delete',
                    method: "POST",
                    data: { _token : '{{csrf_token()}}', _method : 'delete' }, // serializes all the form data
                    success : function(data) {
                        delete_pa_success(row);
                    }
                });
            }
            @endif

            if( create ) {
                setTimeout(function(){
                    delete_success(row);
                }, 300);
            }
        });

        return false;
    });

    function delete_pa_success(row) {
        var id = $project_auditors_datatable.row(row).id();
//        var q_index = auditor_queue.indexOf(id);

//        if(q_index != -1) auditor_queue.splice(q_index, 1);
        $project_auditors_datatable.row(row).remove().draw();

        generateProjectMandays();
        swal('Success!', 'Auditor is successfully deleted in the project.', 'success');
    }
    @endif

    function setFormAction(type) {
        $('#project-form').find('input[name="_method"]').val(type);
    }

    $('.btn-create-project').on('click', function(){
        clear_project_form();

        projects.find('.save-cancel').show();
        projects.find('.basic-info').attr('disabled', false);

        go_to('#project-details');
        projects.find('#project_name').focus();
    });

    $('.btn-cancel-project').on('click', function(){
        clear_project_form();
        return false;
    });

    $('.btn-save-project').on('click', function(){
        swal({
            title: "Continue?",
            text: "You are about to add a new project",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $.ajax({
                url: $('#project-form').attr('action'),
                method: "POST",
                data: $('#project-form').serialize(), // serializes all the form data
                success : function(response) {
                    if( response.success ) {
                        clear_project_form();
                        $projects_datatable.clear().rows.add(response.data).draw();
                        swal('Success!', 'New project is successfully added to the plan.', 'success');
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();

                    if( xhr.status == 500 ) {
                        $('.alert-project').removeClass('alert-warning').addClass('alert-danger');
                        $('.alert-project').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        projects.find('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $('.alert-project').removeClass('alert-danger').addClass('alert-warning');
                        $('.alert-project').html(msg).show();
                    }
                    go_to($('.alert-project'));
                }
            });
        });
    });

    function clear_project_form() {
        $('#datatable-projects').find('tbody > tr').removeClass('selected-row');
        projects.find('#project-form .form-control').attr('disabled', true).not('select').val('');
        projects.find('.select2').val('').trigger('change');
        projects.find('.save-cancel').hide();
        projects.find('.btn-add-proj-auditor').hide();
        setFormAction('post');
        $project_auditors_datatable.clear().draw();
        projects.find('.basic-info').parent().parent().removeClass('has-error');
        $('.alert-project').html('').hide();
    }

    function projectHeadTemplate(data, container) {
        return data.text;
    }

    function projectHeadFormat(data) {
        if (data.loading) return data.text;
        var markup = '';

        markup = data.text+' | '+data.position;

        return markup;
    }

    function getProjectHeads(plan_id) {
        $url = '{{url('planning')}}/'+plan_id+'/project/'+project_id+'/project_heads';
        iapms.generateSelect2($('#project_head'), $url, 'full_name', projectHeadTemplate, projectHeadFormat);
    }

    function setAuditorsList(project_id, datatable, callback) {
        $.ajax({
            url: '{{url('planning')}}/project/'+project_id+'/auditors',
            dataType: "json",
            method: "GET",
            beforeSend: function() {
                swal({
                    title: "Loading auditor's information",
                    text: "Please wait...",
                    type: "info",
                    showConfirmButton: false
                });
            },
            data: { _token : '{{csrf_token()}}' },
            success: function (response) {
                swal.close();
                datatable.clear();
                datatable.rows.add(response.data).draw();
                if( edit ) $('.edit-components').addClass('edit-components-visible').removeClass('edit-components');

                callback();
            }
        });
    }

    function setScopeData(scope) {
        $scope_form.attr('action', '{{ url('planning/project') }}/'+project_id+'/scope/store');
        if( edit ) scopes.find('.btn-create-scope').removeClass('btn-hide');
        $scopes_datatable.clear();
        $scopes_datatable.rows.add(scope.data).draw();
        $scope_auditors_datatable.clear().draw();
    }

    $('.btn-save-project-auditor').on('click', function(){
        swal({
            title: "Continue?",
            text: "You are about to add an auditor to this project",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $.ajax({
                url: $('#project-auditor-form').attr('action'),
                method: "POST",
                data: $('#project-auditor-form').serialize(), // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        if( edit ) $('.btn-delete-project-auditor').show();

                        $project_auditors_datatable.clear().rows.add(data.data).draw();

                        recalculateMandays(projects.find('#auditor_name').val(), projects.find('#allotted_mandays').val());

                        swal('Success!', 'Auditor is successfully added in the project.', 'success');
                        $('.alert-project-auditor').hide();
                        $('.alert-project-auditor').find('.basic-info-modal').parent().parent().removeClass('has-error');

                        $('#project-auditor-form').find('.form-control').val('');
                        projects.find('#auditor_name').val('').trigger('change');
                        projects.find('#proficiency').html('');
                        projects.find('#auditor_type').val('').trigger('change');
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();

                    if( xhr.status == 500 ) {
                        $('.alert-project-auditor').removeClass('alert-warning').addClass('alert-danger');
                        $('.alert-project-auditor').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        projects.find('.basic-info-modal').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                projects.find('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        projects.find('.alert-project-auditor').removeClass('alert-danger').addClass('alert-warning');
                        projects.find('.alert-project-auditor').html(msg).show();
                    }
                }
            });
        });
    });

    function getNextStatus(status) {
        var current_status = statuses.indexOf(status);
        var next_status = status;

        if( current_status < (statuses.length - 1) ) {
            next_status = status == 'New' ? statuses[current_status+1] : status;
        }
        return next_status;
    }

    function updateProjectStatus(status) {
        var data = { _method : 'put', _token : '{{csrf_token()}}'};
        var current_status = statuses.indexOf(status);
        var next_status = status == 'New' ? statuses[current_status+1] : status;

        data['status'] = next_status;
        swal({
            title: "Continue?",
            text: "You are about to change the project status as "+next_status+".",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $.ajax({
                url: '{{url('planning/project')}}/'+project_id+'/status/update',
                method: "POST",
                data: data, // serializes all the form data
                success : function(data) {
                    if (data.success) {
                        location.reload();
                    }
                }
            });
        });
    }

    project_approval.find('button').on('click', function () {
        // Approval button is always in New status
        updateProjectStatus('New');
    });

    projects.find('#project_status').on('change', function () {
        updateProjectStatus($(this).val());
    });

    function toggle_project_input(update) {
        if( update ) {
            $('.btn-create-project').show();
            $('.btn-delete-project-auditor').removeClass('btn-hide');
        }
        else {
            $('.btn-create-project').hide();
            $('.btn-delete-project-auditor').addClass('btn-hide');
        }
    }

    projects.find('#project-auditors-modal').on('show.bs.modal', function (e) {
        getTabAuditors('project', {{ !empty($details['data']) ? $details['data']['id'] : 0 }}, projects.find('#auditor_name'));
    });

    projects.find('#project-auditors-modal').on('hidden.bs.modal', function (e) {
        $('#project-auditor-form').find('.form-control').val('');
        projects.find('#auditor_name').val('').trigger('change');
        projects.find('#proficiency').html('');
        projects.find('#auditor_type').val('').trigger('change');
        projects.find('.basic-info-modal').parent().parent().removeClass('has-error');
        $('.alert-project-auditor').html('').hide();
    });
</script>
@endsection