<div class="col-xs-12">
    <div class="table-header">
        Project Scope
    </div>
    <button class="btn btn-success btn-sm btn-create-scope btn-hide" type="button" data-toggle="modal" data-target='#project-scope-modal'>
        <i class="ace-icon fa fa-plus"></i>
        Create
    </button>
    <table id="datatable-scopes" class="table table-striped table-bordered datatable table-hover">
        <thead>
            <tr>
                <th>Auditable Entity</th>
                <th>Business Process</th>
                <th>Budgeted Mandays</th>
                <th>Adjusted Severity Value</th>
                <th width="5%"></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-12" style="padding-top: 10px;">
    <div class="table-header">
        Project Scope - Auditors
    </div>
    <div style="padding-top: 10px;">
        <button class="btn btn-success btn-sm btn-add-scope-auditor btn-hide" type="button" data-toggle="modal" data-target='#scope-auditors-modal'>
            <i class="ace-icon fa fa-plus"></i>
            Add
        </button>
    </div>
    <table id="datatable-scope-auditors" class="table table-striped table-bordered datatable">
        <thead>
            <tr>
                <th>Auditor Name</th>
                <th>Auditor Type</th>
                <th>Allotted Mandays</th>
                <th>Running Mandays</th>
                <th>Available Mandays</th>
                <th width="7%"></th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<!-- Scope Modal -->
<div class="modal fade" data-backdrop="static" id="project-scope-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Project Scope Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning alert-scope"></div>
                {!! Form::open(['class' => 'form-horizontal', 'id' => 'project-scope-form']) !!}
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="sequence"> Sequence </label>
                    <div class="col-sm-2">
                        <input type="number" name="sequence" id="sequence" step="1" min="1" class="form-control input-sm basic-info-modal" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="auditable_entity"> Auditable Entity </label>
                    <div class="col-sm-8">
                        <select id="auditable_entity" name="auditable_entity" class="form-control input-sm basic-info-modal" data-placeholder="Click to Choose...">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="business_process"> Business Process </label>
                    <div class="col-sm-8">
                        <select id="business_process" name="business_process" class="form-control input-sm basic-info-modal" data-placeholder="Click to Choose...">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="budgeted_mandays"> Budgeted Mandays </label>
                    <div class="col-sm-2">
                        <input type="number" name="budgeted_mandays" id="budgeted_mandays" step="1" min="1" class="form-control input-sm basic-info-modal" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="adjusted_sev_val"> Adjusted Severity Value </label>
                    <div class="col-sm-2">
                        <input type="text" id="adjusted_sev_val" class="form-control input-sm" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="location"> Audit Location </label>
                    <div class="col-sm-8">
                        <input type="text" id="location" name="location" class="form-control input-sm basic-info-modal">
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-scope">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Scope Modal -->

<!-- Auditors Modal -->
<div class="modal fade" data-backdrop="static" id="scope-auditors-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Auditor Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning alert-scope-auditor"></div>
                {!! Form::open(['class' => 'form-horizontal', 'id' => 'scope-auditor-form']) !!}
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="auditor_name"> Auditor Name </label>
                    <div class="col-sm-8">
                        <select id="auditor_name" name="auditor_name" class="form-control input-sm basic-info-modal auditor-name" data-placeholder="Click to Choose...">
                            <option value="">---</option>
                            {{-- @foreach($auditors as $auditor)
                                <option value="{{$auditor->auditor_id}}">{{$auditor->full_name}}</option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="auditor_type"> Auditor Type </label>
                    <div class="col-sm-6">
                        <select id="auditor_type" name="auditor_type" class="form-control input-sm basic-info-modal auditor-type" data-placeholder="Click to Choose..."></select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="allotted_mandays"> Allotted Mandays </label>
                    <div class="col-sm-2">
                        <input type="number" name="allotted_mandays" id="allotted_mandays" class="form-control input-sm" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="running_mandays"> Running Mandays </label>
                    <div class="col-sm-2">
                        <input type="text" name="running_mandays" id="running_mandays" class="form-control input-sm" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="available_mandays"> Available Mandays </label>
                    <div class="col-sm-2">
                        <input type="text" name="available_mandays" id="available_mandays" class="form-control input-sm" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="proficiency"> Proficiency </label>
                    <div class="col-sm-6" id="proficiency"></div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save-scope-auditor">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Auditors Modal -->

<!-- Custom measures Modal -->
@include('includes.modals.custom_measures')
<!-- /Custom measures Modal -->

@section('scope_js')
<script type="text/javascript">
    var $auditable_ent = scopes.find('#auditable_entity');
    var $business_proc = scopes.find('#business_process');
    var $scope_form = scopes.find('#project-scope-form');
    var $scope_alert = scopes.find('.alert-scope');
    var $scope_modal = scopes.find('#project-scope-modal');
    var bps = [];
    var scope_id;
    var $scopes_datatable = $('#datatable-scopes').DataTable( {
        "lengthMenu": [ 25, 50, 75, 100 ],
        "processing": true,
        data: [],
        rowId : 'id',
        columns: [
            { data: "auditable_entity.data.name" },
            { data: "business_process.data.name" },
            { data: "budgeted_mandays" },
            { data: null, defaultContent : 0 },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="btn-scope-details" data-id="'+data.id+'" title="Details" rel="tooltip"><i class="fa fa-eye"></i></a> &nbsp; <a href="#" class="btn-custom-measure" data-ae_id="'+data.auditable_entity.data.id+'" data-id="'+data.business_process.data.id+'" title="Custom Measures" rel="tooltip"><i class="fa fa-tachometer"></i></a> &nbsp; <a href="#" class="btn-delete-scope delete edit-components" data-id="'+data.id+'" data-name="'+data.name+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: 'row-control',
            }
        ]
    } );
    var $scope_auditors_datatable = $('#datatable-scope-auditors').DataTable( {
        data : [],
        "lengthMenu": [ 25, 50, 75, 100 ],
        "processing": true,
        rowId: 'id',
        drawCallback: function() {
            $('[data-toggle="popover"]').popover();
        },
        columns: [
            { data: "name" },
            { data: "type" },
            { data: "mandays.alloted" },
            { data: "mandays.running", defaultContent : 'N/A' },
            { data: "mandays.available", defaultContent : 'N/A' },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var prof = '';
                    if( typeof data.proficiencies === "object" && typeof data.proficiencies.length === "undefined" ) {
                        prof += '<ul class=\'list-unstyled spaced2\'>';
                        $.each(data.proficiencies, function(i, v){
                            prof += '<li><i class=\'fa fa-angle-right\'></i> '+i+'-'+v+'</li>';
                        });
                        prof += '</ul>';
                    }
                    else {
                        prof = '<span class=\'label label-white middle label-lg\'><i class=\'fa fa-info-circle\'></i> No proficiency available</span>'
                    }
                    return '<a href="javascript:void(0);" class="btn-auditor-prof" data-toggle="popover" data-content="'+prof+'" data-placement="left" data-html="true" data-trigger="hover" title="Proficiencies"><i class="fa fa-address-card"></i></a> &nbsp; <a href="#" class="btn-delete-scope-auditor delete btn-hide" data-id="'+data.id+'" data-name="'+data.name+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: 'row-control'
            }
        ]
    } );

    $('#project-scope-modal').on('show.bs.modal', function () {
        if( !edit ) {
            $(this).find('form-control').attr('disabled', true);
            $(this).find('.btn-save-scope').hide();
        }
        else {
            $(this).find('.form-control').attr('disabled', false);
            $(this).find('.btn-save-scope').show();
        }
    });

    function toggle_scope_input(update) {
        if( update ) {
            $('.btn-create-scope').removeClass('btn-hide');
            $('.btn-delete-scope-auditor').removeClass('btn-hide');
        }
        else {
            $('.btn-create-scope').addClass('btn-hide');
            $('.btn-delete-scope-auditor').addClass('btn-hide');
        }
    }

    function getAuditableEntities() {
        return $.get('{{ route('auditable_entities.list') }}?with=businessProcess&include=businessProcess&json=true&transform=true', { _token : '{{csrf_token()}}' }, function (data) {
            return data;
        });
    }

    $(function () {
        $business_proc.select2({
            data : bps
        });
        // $auditable_ent.select2({
        //     ajax: {
        //         url: '{{ route('auditable_entities.list') }}?with=businessProcess&include=businessProcess&json=true&transform=true&_token='+_token,
        //         dataType: 'json',
        //         allowClear: true,
        //         data: function (params) {
        //             return {
        //                     search: params.term, // search term
        //                     searchFields: 'auditable_entity_name:like', // search term
        //                     page: params.page
        //                 };
        //             },
        //             processResults: function (data, params) {
        //                 return {
        //                     results: data.data
        //                 };
        //             },
        //         cache: true
        //     },
        //     escapeMarkup: function (markup) { return markup; },
        //     templateSelection: auEntityTemplate,
        //     templateResult: auEntityFormat
        // });
        var auditable_entities = getAuditableEntities();
        auditable_entities.done(function (resp) {
            var entities = $.map(resp.data, function (obj) {
                obj.text = obj.text || obj.name; // replace name with the property used for the text

                return obj;
            });
            $auditable_ent.select2({
                data : entities,
                escapeMarkup: function (markup) { return markup; },
                templateSelection: auEntityTemplate,
                templateResult: auEntityFormat
            });
            scopes.find('.select2-container').css('width', '100%');
        });
    });

    @if( !empty($details['data']) )
    dtClickable(scopes, $('#datatable-scopes'), $scopes_datatable, 'id', setupScopeDetails);

    function setupScopeDetails(data) {
        scopes.find('#scope-auditor-form').attr('action', '{{ url('planning/project/scope') }}/'+data.id+'/auditor/store');
        scopes.find('.save-cancel').show();
        scopes.find('.basic-info').attr('disabled', false);

        scope_id = data.id;
        auditorNameOnSelect(scopes);
        setScopeAuditorsList(scope_id, $scope_auditors_datatable, generateScopeMandays);

        if( edit ) $('.btn-add-scope-auditor').removeClass('btn-hide');
        else $('.btn-add-scope-auditor').addClass('btn-hide');
    }

    function setScopeAuditorsList(id, datatable, callback) {
        $.ajax({
            url: '{{ url('planning') }}/project/scope/'+id+'/auditors',
            dataType: "json",
            method: "GET",
            beforeSend: function() {
                swal({
                    title: "Loading auditor's information",
                    text: "Please wait...",
                    type: "info",
                    showConfirmButton: false
                });
            },
            data: { _token : '{{ csrf_token() }}' },
            success: function (response) {
                swal.close();
                datatable.clear();
                datatable.rows.add(response.data).draw();
                callback;
                if( edit )  scopes.find('.btn-delete-scope-auditor').removeClass('btn-hide');
            }
        });
    }

    function generateScopeMandays() {
        scopes.find('#allotted_mandays').val(sumMandays($scope_auditors_datatable, 2));
        scopes.find('#running_mandays').val(sumMandays($scope_auditors_datatable, 3));
        scopes.find('#available_mandays').val(sumMandays($scope_auditors_datatable, 4));
    }
    @endif

    scopes.find("#auditable_entity").on('select2:select', function (evt) {
        scopes.find('#budgeted_mandays').val('');
        var data = $(this).select2("data");
        bps = $.map(data[0]['businessProcess']['data'], function (obj) {
            obj.text = obj.text || obj.name; // replace name with the property used for the text

            return obj;
        });
        $business_proc.html('').trigger('change');

        $business_proc.select2({
            data : bps
        });
    });

    function auEntityTemplate(data) {
        return data.name;
    }

    function auEntityFormat(data) {
        if (data.loading) return data.name;
        var markup = '';

        markup = data.name;

        return markup;
    }

    function clear_scope_form() {
        $scope_modal.find('#project-scope-form input').val('');
        $scope_modal.find('#auditable_entity').val('').trigger('change');
        $scope_modal.find('#business_process').val('').trigger('change');
        $scope_modal.find('.basic-info').parent().parent().removeClass('has-error');
        $scope_modal.find('.alert-scope').html('').hide();
    }

    $('.btn-save-scope').on('click', function () {
        $.ajax({
            url: $scope_form.attr('action'),
            method: "POST",
            data: $scope_form.serialize(), // serializes all the form data
            success : function(response) {
                if( response.success ) {
                    clear_scope_form();
                    $scopes_datatable.clear().rows.add(response.data).draw();
                    swal('Success!', 'New project is successfully added to the plan.', 'success');
                }
            },
            error: function(xhr, textStatus, errorThrown){
                swal.close();

                if( xhr.status == 500 ) {
                    $scope_alert.removeClass('alert-warning').addClass('alert-danger');
                    $scope_alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                }
                else {
                    // displays the validation error
                    var unique_message = [];
                    var msg = '<ul class="list-unstyled">';

                    scopes.find('.basic-info-modal').parent().parent().removeClass('has-error');

                    $.each(xhr.responseJSON, function(key, val){
                        for(var i=0; i < val.length; i++) {
                            // set error class to fields
                            $('#'+key).parent().parent().addClass('has-error');

                            // shows error message
                            if( unique_message.indexOf(val[i]) === -1 ) {
                                msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                unique_message.push(val[i]);
                            }
                        }
                    });
                    msg += '</ul>';

                    $scope_alert.removeClass('alert-danger').addClass('alert-warning');
                    $scope_alert.html(msg).show();
                }
            }
        });
    });

    $('.btn-save-scope-auditor').on('click', function(){
        swal({
            title: "Continue?",
            text: "You are about to add an auditor to this scope",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            if( !create ) {
                $.ajax({
                    url: $('#scope-auditor-form').attr('action'),
                    method: "POST",
                    data: $('#scope-auditor-form').serialize(), // serializes all the form data
                    success : function(data) {
                        if( data.success ) {
                            if( edit ) $('.btn-delete-scope-auditor').show();

                            $scope_auditors_datatable.clear().rows.add(data.data).draw();

                            recalculateMandays(scopes.find('#auditor_name').val(), scopes.find('#allotted_mandays').val());

                            swal('Success!', 'Auditor is successfully added in the scope.', 'success');
                            $('.alert-scope-auditor').hide();
                            $('.alert-scope-auditor').find('.basic-info-modal').parent().parent().removeClass('has-error');

                            $('#scope-auditor-form').find('.form-control').val('');
                            scopes.find('#auditor_name').val('').trigger('change');
                            scopes.find('#proficiency').html('');
                            scopes.find('#auditor_type').val('').trigger('change');
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal.close();

                        if( xhr.status == 500 ) {
                            $('.alert-scope-auditor').removeClass('alert-warning').addClass('alert-danger');
                            $('.alert-scope-auditor').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                        }
                        else {
                            // displays the validation error
                            var unique_message = [];
                            var msg = '<ul class="list-unstyled">';

                            scopes.find('.basic-info-modal').parent().parent().removeClass('has-error');

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    // set error class to fields
                                    scopes.find('#'+key).parent().parent().addClass('has-error');

                                    // shows error message
                                    if( unique_message.indexOf(val[i]) === -1 ) {
                                        msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                        unique_message.push(val[i]);
                                    }
                                }
                            });
                            msg += '</ul>';

                            $('.alert-scope-auditor').removeClass('alert-danger').addClass('alert-warning');
                            $('.alert-scope-auditor').html(msg).show();
                        }
                    }
                });
            }
            else {
                auditor_queue.push($('#auditor_name').val());
                var new_tr = $project_auditors_datatable.row.add({
                    id : $('#auditor_name').val(),
                    name : $('#auditor_name option:selected').text(),
                    type : $('#auditor_type').val(),
                    mandays : {
                        'annual' : $('#annual_mandays').val(),
                        'running' : $('#running_mandays').val(),
                        'available' : $('#available_mandays').val(),
                    },
                    proficiencies : _proficiencies
                }).draw();
                $('[data-toggle="popover"]').popover();
                setTimeout(function(){
                    swal('Success!', 'Auditor is added to the queue.', 'success');
                    clear_auditor_form();
                }, 300);
            }
        });
    });
    $('body').on('click', '.btn-delete-project-auditor', function(){
        var remove_id = $(this).attr('data-id');
        var remove_name = $(this).attr('data-name');
        var row = $(this).parents('tr');

        swal({
            title: "Continue?",
            text: "You are about to delete "+remove_name+" to this project.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            @if( !empty($details['data']) )
            if( !create ) {
                $.ajax({
                    url: '{{ url('planning') }}/project/'+project_id+'/auditor/'+remove_id+'/delete',
                    method: "POST",
                    data: { _token : '{{csrf_token()}}', _method : 'delete' }, // serializes all the form data
                    success : function(data) {
                        delete_pa_success(row);
                    }
                });
            }
            @endif

            if( create ) {
                setTimeout(function(){
                    delete_success(row);
                }, 300);
            }
        });

        return false;
    });

    $('body').on('click', '.btn-delete-scope-auditor', function(){
        var remove_id = $(this).attr('data-id');
        var remove_name = $(this).attr('data-name');
        var row = $(this).parents('tr');

        swal({
            title: "Continue?",
            text: "You are about to delete "+remove_name+" to this project.",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $.ajax({
                url: '{{ url('planning') }}/project/scope/'+scope_id+'/auditor/'+remove_id+'/delete',
                method: "POST",
                data: { _token : '{{csrf_token()}}', _method : 'delete' }, // serializes all the form data
                success : function(data) {
                    delete_sa_success(row);
                }
            });
        });

        return false;
    });

    function delete_sa_success(row) {
        $scope_auditors_datatable.row(row).remove().draw();

        generateScopeMandays();
        swal('Success!', 'Auditor is successfully deleted in the scope.', 'success');
    }

    $('body').on('click', '.btn-scope-details', function(){
        var id = $(this).attr('data-id');
        var row = $(this).parents('tr');
        var data = $scopes_datatable.row(row).data();

        $scope_modal.find('#sequence').val(data.sequence);
        $scope_modal.find('#auditable_entity').val(data.auditable_entity.data.id).trigger('change');
        $scope_modal.find('#business_process').append('<option value="'+data.business_process.data.id+'" selected>'+data.business_process.data.name+'</option>').val(data.business_process.data.id).trigger('change');
//        $scope_modal.find('#auditable_entity').val(data.auditable_entity.data.id).trigger('change');
//        $scope_modal.find('#business_process').val(data.business_process.data.id).trigger('change');
        $scope_modal.find('#budgeted_mandays').val(data.budgeted_mandays);
        $scope_modal.find('#location').val(data.location);

        if( !edit ) {
            $scope_modal.find('.basic-info-modal').attr('disabled', true);
        }
        else {
            $scope_modal.find('.basic-info-modal').attr('disabled', false);
        }

        $scope_modal.modal('show');

        return false;
    });

    $('#scope-auditors-modal').on('show.bs.modal', function (e) {
        getTabAuditors('scope', {{ !empty($details['data']) ? $details['data']['id'] : 0 }}, scopes.find('#auditor_name'));
    });

    scopes.find('#business_process').on('select2:select', function (evt) {
        var bp_id = $(this).val();
        $.ajax({
            url: '{{url('business_process')}}/'+bp_id+'/test_proc/mandays',
            dataType: "json",
            method: "GET",
            beforeSend: function() {
                swal({
                    title: "Getting default mandays",
                    text: "Please wait...",
                    type: "info",
                    showConfirmButton: false
                });
            },
            data: { _token : '{{csrf_token()}}' },
            success: function (response) {
                swal.close();
                if( response.success ) scopes.find('#budgeted_mandays').val(response.data.mandays);
            }
        });
    });

    scopes.find('#project-scope-modal').on('hidden.bs.modal', function (e) {
        $(this).find('.form-control').val('');
        $(this).find('#auditable_entity').val('').trigger('change');
        $(this).find('#business_process').val('').trigger('change');
        $(this).find('.basic-info-modal').parent().parent().removeClass('has-error');
        $(this).find('.alert-scope').html('').hide();
    });

    scopes.find('#scope-auditors-modal').on('hidden.bs.modal', function (e) {
        $(this).find('.form-control').val('');
        $(this).find('#auditor_name').val('').trigger('change');
        $(this).find('#auditor_type').val('').trigger('change');
        $(this).find('.basic-info-modal').parent().parent().removeClass('has-error');
        $(this).find('#proficiency').html('');
        $(this).find('.alert-scope-auditor').html('').hide();
    });
</script>

@yield('cm_scripts')
@endsection