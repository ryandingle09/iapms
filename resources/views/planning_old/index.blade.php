@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <link rel="stylesheet" href="/css/select2.min.css" />
    <style type="text/css">
        .edit-components {
            display: none;
        }
        .edit-components-visible {
            display: inline-block !important;
        }
        .label-default {
            width: 100%;
            text-align: right;
            background-color: #1f7d74;
        }
        .row-details {
            padding-bottom: 5px;
        }
        .selected-row {
            background-color: #ffe99e !important;
        }
        .table-hover tbody > tr:hover {
            cursor: pointer;
        }
        .selected {
            background-color: #afafaf;
            color: #fff;
        }
        .select2-search:after { display: none !important; }
        .btn-hide { display: none !important; }
    </style>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')
            <div class="row">
                @if (Session::has('message'))
                <div class="col-xs-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                </div>
                @endif

                @if( !empty($details['data']) )
                <div class="col-xs-12">
                    <div class="pull-right">
                        <button type="button" class="btn btn-primary btn-edit-mode"><i class="fa fa-pencil"></i> Edit Planning</button>
                        <button type="button" class="btn btn-warning btn-cancel-edit-mode btn-hide"><i class="fa fa-times"></i> Cancel Edit</button>
                        <a href="{{route('planning')}}" class="btn btn-danger btn-cancel-search"><i class="fa fa-window-close-o"></i> Cancel Search</a>
                    </div>
                </div>
                @endif

                <div class="col-xs-12">

                    <div {!! Request::get('search') == '' ? 'style="display:none;"' : ( !empty($details['data']) ? 'style="display:none;"' : '' ) !!} class="alert alert-warning">
                        {{ empty($details['data']) || Request::get('search') !='' ? 'No planning found for the year '.Request::get('search').'!' : '' }}
                    </div>
                    <div class="tabbable">
                        <?php $exclude_status = ['New', 'For Review']; ?>
                        <ul class="nav nav-tabs padding-12 tab-color-blue background-blue" id="inquiries">
                            <?php
                                $q = explode('|', Request::get('q'));
                                $back_q = count($q) > 0 ? array_splice($q, 0, -1) : ''; // Back url | (count($q) - 2) = last tab id or code
                                $back_q = implode('|', $back_q);
                            ?>
                            <li class="active">
                                <a href="#plan" data-toggle="tab" aria-expanded="false">Plan</a>
                            </li>

                            <li>
                                <a href="#projects" data-toggle="tab" aria-expanded="false">Projects</a>
                            </li>

                            <li>
                                <a href="#scope" data-toggle="tab" aria-expanded="true">Scope</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <!-- Plan -->
                            <div id="plan" class="tab-pane active">
                                <div class="row">
                                    @include('planning_old.includes.plan')
                                </div>
                            </div>
                            <!-- /Plan -->

                            <!-- Projects -->
                            <div id="projects" class="tab-pane">
                                <div class="row">
                                    @include('planning_old.includes.project')
                                </div>
                            </div>
                            <!-- /Projects -->

                            <!-- Scope -->
                            <div id="scope" class="tab-pane">
                                <div class="row">
                                    @include('planning_old.includes.scope')
                                </div>
                            </div>
                            <!-- /Scope -->
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<!-- guide Modal -->

<!-- /guide Modal -->
@endsection

@section('footer_script')
<script src="/js/iapms.js"></script>
<script src="/js/date-time/moment.js"></script>
<script src="/js/date-time/bootstrap-datepicker.js"></script>
<script src="/js/date-time/bootstrap-datetimepicker.js"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script src="/js/select2.full.js"></script>
<script src="/js/fuelux/fuelux.wizard.js"></script>
<script src="/js/ace/elements.wizard.js"></script>
<script type="text/javascript">
    var tab;
    var current_tab;
    var edit = false;
    var $edit_components = $('.edit-components');
    var $btn_edit_mode = $('.btn-edit-mode');
    var $btn_cancel_edit_mode = $('.btn-cancel-edit-mode');
    var inquiry = $('ul#inquiries');
    // tab containers
    var plan = $('div#plan');
    var projects = $('div#projects');
    var scopes = $('div#scope');

    $('body').on('focus', ".year-picker", function(){
        $(this).datetimepicker({
            pickTime: false,
            viewMode: "years",
            minViewMode: "years",
            format: 'YYYY',
            autoclose: true
        });
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        current_tab = $(e.target).parent();
    })

    $('#modal-wizard #modal-wizard-container').ace_wizard();
    $('#modal-wizard .wizard-actions .btn[data-dismiss=modal]').removeAttr('disabled');

    /**
     * Set  Auditable Entity and Business Process details on designated labels
     */
    function dtClickable( parent, table, datatable, id, callback ) {
        table.find('tbody').on('click', "tr:not('.selected-row')", function (e) {
            var target = $(e.target);
            if( target.hasClass('row-control') || target.hasClass('fa') ) return true;

            table.find('tbody > tr').removeClass('selected-row'); // reset tr highlights
            var data = datatable.row( this ).data();
            $(this).addClass('selected-row');

            $.each(data, function(i, v){
                var input = '';
                if( parent.find('[name="'+i+'"]').length ) {
                    input = parent.find('[name="'+i+'"]');
                    if(input.hasClass('select2')) {
                        input.append('<option value="'+v+'">'+v+'</option>');
                        input.trigger('change');
                    }
                    else
                        input.val(v);
                }
                if( parent.find('[data-tag="'+i+'"]').length ) {
                    input = parent.find('[data-tag="'+i+'"]');
                    input.val(v);
                }
            });
            if( typeof callback != "undefined" ) callback(data);
        } );
    }

    function go_to(element) {
        $('html,body').animate({
            scrollTop: ($(element).offset().top)
        });
    }

    function sumMandays(datatable, column) {
        var col_data = datatable.column(column).data();
        if( typeof col_data != "undefined" && col_data.length )
            return parseFloat(col_data.reduce(function(a,b){ return parseFloat(a) + parseFloat(b); })).toFixed(2);
        else
            return 0;
    }

    $btn_edit_mode.on('click', function () {
        edit = true;
        $('#datatable-projects').find('tbody > tr').removeClass('selected-row');
        $('#datatable-scopes').find('tbody > tr').removeClass('selected-row');
        $project_guide_datatable.rows().clear().draw();
        $scopes_datatable.rows().clear().draw();
        $scope_guide_datatable.rows().clear().draw();
        toggle_plan_input(true);
        toggle_project_input(true);

        // check if there's already project selected
        if( project_id > 0 ) {
            var row = $('#datatable-projects').find('tr#'+project_id);
            var data = $projects_datatable.row(row).data();
            row.addClass('selected-row');
            setupProjectDetails(data);
        }

        // check if there's already project selected
        if( scope_id > 0 ) {
            var scope_row = $('#datatable-scopes').find('tr#'+scope_id);
            var scope_data = $scopes_datatable.row(scope_row).data();
            scope_row.addClass('selected-row');
            setupScopeDetails(scope_data);
        }

        $btn_edit_mode.addClass('btn-hide');
        $btn_cancel_edit_mode.removeClass('btn-hide');
    });

    $btn_cancel_edit_mode.on('click', function(){
        swal({
            title: "Cancel edit?",
            text: "",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            location.reload();
        });
    });

    function getTabguide(tab, id, element) {
        $.ajax({
            url: '{{ url('planning/guide') }}/'+tab+'/'+id,
            method: "GET",
            data: { _token : _token },
            beforeSend: function() {
                swal({
                    title: "Getting available guide",
                    text: "Please wait...",
                    type: "info",
                    showConfirmButton: false
                });
            },
            success : function(response) {
                swal.close();
                if( response.success ) {
                    element.select2({ data : iapms.transformSelect2Data(response.data, 'auditor_id', 'full_name'), placeholder: 'Click to choose...' });
                    $('.select2-container').css('width', '100%');
                }
            }
        });
    }

    function recalculateMandays(id, val) {
        var plan_auditor = $('#datatable-plan-auditor').find('tr#'+id);
        var proj_auditor = $('#datatable-project-guide').find('tr#'+id);
        var scope_auditor = $('#datatable-scope-guide').find('tr#'+id);

        syncMandays($plan_auditor_datatable, plan_auditor, val);
    }

    function syncMandays(dt, row, val) {
        var running = dt.cell(row.find('td:eq(3)'));
        var remaining = dt.cell(row.find('td:eq(4)'));
        var new_running = parseInt(val) + parseFloat(running.data());
        var new_remaining = parseFloat(remaining.data()) - parseInt(val);

        running.data(new_running).draw();
        remaining.data(new_remaining).draw();

        generateMandays();
    }
</script>
@yield('plan_js')
@yield('project_js')
@yield('scope_js')
@endsection
