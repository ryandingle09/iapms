@extends('layouts.app')
@section('styles')

@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')
        <div class="page-content">
            @include('includes.content_header')
            <div class="row">
                @include('approval_setup.approval_head')
                @include('approval_setup.approval_detail')
                
            </div>
            <div class="row">
                @include('approval_setup.status_from')
                @include('approval_setup.status_to')
                
                
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

@endsection
@section('footer_script')
<script type="text/javascript">
    var selectedHead = 0;
    var selectedDetail = 0;
    var approval_setup_master_datatable = null;
    var approval_setup_detail_datatable = null;
    var approval_status_from_datatable = null;
    var approval_status_to_datatable = null;
    var ash_url = "{{ url('approval-setup') }}";
    var asd_url = "";
    var asdf_url = "";
    var asdt_url = "";
</script>
@yield('approval_head_scripts')
@yield('approval_detail_scripts')
@yield('status_from_scripts')
@yield('status_to_scripts')
@yield('additional_info_modal_scripts')
@endsection
