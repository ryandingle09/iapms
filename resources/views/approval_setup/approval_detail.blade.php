<div class="col-xs-6 mrgB15">
    <button class="btn btn-primary btn-sm"  id="show-approval-detail-modal">
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Add
    </button>
    <div class="table-header">
        Approval Detail
    </div>

    <!-- div.table-responsive -->

    <!-- div.dataTables_borderWrap -->
    <div>
        <table id="approval-setup-detail-table" class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th width="">Sequence</th>
                <th width="">Auditor Type</th>
                <th width="">Description</th>
                <th width="">Source Table</th>
                <th width="">Approver Flag</th>
                <th width="">Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@include('approval_setup.includes.approval_detail_modal')
@section('approval_detail_scripts')
<script type="text/javascript">
    approval_setup_detail_datatable = $('#approval-setup-detail-table').DataTable( {
        "bLengthChange": false,
        "processing": true,
        orderCellsTop: true,
        createdRow: function( row, data, dataIndex ) {
            if(selectedHead == data.approval_setup_detail_id){
                $( row ).addClass('selected-row');
            }
            $(row).attr('data-id', data.approval_setup_detail_id );
        },
        columns: [
            { data: "approval_sequence" },
            { data: "auditor_type" },
            { defaultContent: 'n/a'},
            { defaultContent: 'n/a' },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return data.approver_flag ? "Y" : "N";
                }
            },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+asd_url+'/'+data.approval_setup_detail_id+'"><i class="fa fa-pencil edit" title="Edit" rel="tooltip"></i></a> &nbsp; <a href="'+asd_url+'/'+data.approval_setup_detail_id+'" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );    
    
    $(document).on('click', "#approval-setup-detail-table .delete" ,function(){
        $(this).deleteItemFrom(approval_setup_detail_datatable);  
        return false;
    });

    $(document).on('click','#approval-setup-detail-table tr',function(){
        $("#approval-setup-detail-table .selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        selectedDetail = $(this).data('id');
        asdf_url = "{{ url('approval-setup-detail') }}/" +selectedDetail +"/status-from";
        asdt_url = "{{ url('approval-setup-detail') }}/" +selectedDetail +"/status-to";
        approval_status_from_datatable.ajax.url( asdf_url + "/list" ).load();
        approval_status_to_datatable.ajax.url( asdt_url + "/list" ).load();
    });
</script>
@yield('approval_detail_modal_scripts')
@endsection