<div class="col-xs-6 mrgB15">
    <button class="btn btn-primary btn-sm"  id="show-status-from-modal">
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Add
    </button>
    <div class="table-header">
        Approval Detail - Status From
    </div>

    <!-- div.table-responsive -->

    <!-- div.dataTables_borderWrap -->
    <div>
        <table id="approval-status-from-table" class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th width="">Status</th>
                <th width="">Description</th>
                <th width="">Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@include('approval_setup.includes.status_from_modal')
@section('status_from_scripts')
<script type="text/javascript">
    approval_status_from_datatable = $('#approval-status-from-table').DataTable( {
        "bLengthChange": false,
        "processing": true,
        orderCellsTop: true,
        columns: [
            { data: "status" },
            { data: "description" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+asdf_url+"/"+data.approval_from_status_id+'"><i class="fa fa-pencil edit" title="Edit" rel="tooltip"></i></a> &nbsp; <a href="'+asdf_url+"/"+data.approval_from_status_id+'" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );    
    
    $(document).on('click', "#approval-status-from-table .delete" ,function(){
        $(this).deleteItemFrom(approval_status_from_datatable);  
        return false;
    });
</script>
@yield('status_from_modal_scripts')
@endsection