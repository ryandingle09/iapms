<div class="modal fade" data-backdrop="static" id="approval-detail-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Approval Detail</h4>
            </div>
            <div class="modal-body">
                <div id="approval-detail-alert"></div>
                <form id="approval-detail-form" class="form-horizontal padT15">

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="approval_sequence"> Sequence </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm basic-info" id="approval_sequence" name="approval_sequence">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="auditor_type"> Auditor Type </label>
                        <div class="col-sm-9">
                            <select class="form-control input-sm auditor_type" name="auditor_type"></select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="approver_flag"> &nbsp; </label>

                        <div class="col-sm-9">
                            <input class="ace" type="checkbox" name="approver_flag" >
                            <span class="lbl"> Approver Flag </span>
                        </div>
                    </div>

                    <div class="form-row align-right" >
                        <button class="btn btn-info btn-sm">
                            <i class="ace-icon fa fa-save bigger-120"></i>
                            Apply
                        </button>
                        <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@section('approval_detail_modal_scripts')
<script type="text/javascript">
    var auditor_type_select = $("#approval-detail-form .auditor_type").makeSelectize({
        lookup : '{{ config('iapms.lookups.auditor.type') }}'
    })[0].selectize;

    $("#approval-detail-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#approval-detail-form")[0].reset();
                $("#approval-detail-modal").modal('hide');
                approval_setup_detail_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#approval-detail-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on('click','#show-approval-detail-modal',function(){
    	$("#approval-detail-form")[0].reset();
    	$("#approval-detail-form").attr('action',asd_url).attr('method',"POST");
         auditor_type_select.clear();
    	$("#approval-detail-modal").modal('show');
    	return false;
    });

    $(document).on("click","#approval-setup-detail-table .edit",function(){
    	$("#approval-detail-form")[0].reset();
        var row = $(this).parents('tr');
        auditor_type_select.clear();
        var data = approval_setup_detail_datatable.row( row ).data();
        $("#approval-detail-form").attr('action',asd_url + "/" + data.approval_setup_detail_id )
        						.attr('method',"PUT")
        						.supply(data);
        auditor_type_select.setValue(data.auditor_type);
    	$("#approval-detail-modal").modal('show');
        return false;
    });


</script>
@endsection