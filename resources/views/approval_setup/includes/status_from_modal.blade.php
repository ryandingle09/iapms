<div class="modal fade" data-backdrop="static" id="status-from-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Approval Detail Status From</h4>
            </div>
            <div class="modal-body">
                <div id="status-from-alert"></div>
                <form id="status-from-form" class="form-horizontal padT15">

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="status"> Status </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm basic-info" id="status" name="status">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="description"> Description</label>
                        <div class="col-sm-9">
                            <textarea class="form-control input-sm description" name="description"></textarea>
                        </div>
                    </div>

                    <div class="form-row align-right" >
                        <button class="btn btn-info btn-sm">
                            <i class="ace-icon fa fa-save bigger-120"></i>
                            Apply
                        </button>
                        <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@section('status_from_modal_scripts')
<script type="text/javascript">

    $("#status-from-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#status-from-form")[0].reset();
                $("#status-from-modal").modal('hide');
                approval_status_from_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#status-from-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on('click','#show-status-from-modal',function(){
    	$("#status-from-form")[0].reset();
    	$("#status-from-form").attr('action',asdf_url).attr('method',"POST");
    	$("#status-from-modal").modal('show');
    	return false;
    });

    $(document).on("click","#approval-status-from-table .edit",function(){
    	$("#status-from-form")[0].reset();
        var row = $(this).parents('tr');
        var data = approval_status_from_datatable.row( row ).data();
        $("#status-from-form").attr('action',asdf_url + "/" + data.approval_from_status_id )
        						.attr('method',"PUT")
        						.supply(data);
    	$("#status-from-modal").modal('show');
        return false;
    });


</script>
@endsection