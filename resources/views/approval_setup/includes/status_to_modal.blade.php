<div class="modal fade" data-backdrop="static" id="status-to-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Approval Detail Status to</h4>
            </div>
            <div class="modal-body">
                <div id="status-to-alert"></div>
                <form id="status-to-form" class="form-horizontal padT15">

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="status"> Status </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm basic-info" id="status" name="status">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="description"> Description</label>
                        <div class="col-sm-9">
                            <textarea class="form-control input-sm description" name="description"></textarea>
                        </div>
                    </div>

                    <div class="form-row align-right" >
                        <button class="btn btn-info btn-sm">
                            <i class="ace-icon fa fa-save bigger-120"></i>
                            Apply
                        </button>
                        <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@section('status_to_modal_scripts')
<script type="text/javascript">

    $("#status-to-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#status-to-form")[0].reset();
                $("#status-to-modal").modal('hide');
                approval_status_to_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#status-to-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on('click','#show-status-to-modal',function(){
    	$("#status-to-form")[0].reset();
    	$("#status-to-form").attr('action',asdt_url).attr('method',"POST");
    	$("#status-to-modal").modal('show');
    	return false;
    });

    $(document).on("click","#approval-status-to-table .edit",function(){
        alert();
    	$("#status-to-form")[0].reset();
        var row = $(this).parents('tr');
        var data = approval_status_to_datatable.row( row ).data();
        $("#status-to-form").attr('action',asdt_url + "/" + data.approval_to_status_id )
        						.attr('method',"PUT")
        						.supply(data);
    	$("#status-to-modal").modal('show');
        return false;
    });


</script>
@endsection