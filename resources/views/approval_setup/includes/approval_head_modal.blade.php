<div class="modal fade" data-backdrop="static" id="approval-head-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Approval Header</h4>
            </div>
            <div class="modal-body">
                <div id="approval-head-alert"></div>
                <form id="approval-head-form" class="form-horizontal padT15">

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="source_category"> Category </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm basic-info" id="source_category" name="source_category">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="source_subcategory"> Subcategory </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm basic-info" id="source_subcategory" name="source_subcategory">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="source_table"> Source Table </label>

                        <div class="col-sm-9">
                            <input type="text" class="form-control input-sm basic-info" id="source_table" name="source_table">
                        </div>
                    </div>

                    <div class="form-row align-right" >
                        <button class="btn btn-info btn-sm">
                            <i class="ace-icon fa fa-save bigger-120"></i>
                            Apply
                        </button>
                        <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                            <i class="ace-icon fa fa-times bigger-120"></i>
                            Cancel
                        </a>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@section('approval_head_modal_scripts')
<script type="text/javascript">

    $("#approval-head-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#approval-head-form")[0].reset();
                $("#approval-head-modal").modal('hide');
                approval_setup_master_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#approval-head-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on('click','#show-approval-head-modal',function(){
    	$("#approval-head-form")[0].reset();
    	$("#approval-head-form").attr('action',ash_url).attr('method',"POST");
    	$("#approval-head-modal").modal('show');
    	return false;
    });

    $(document).on("click","#approval-setup-master-table .edit",function(){
    	$("#approval-head-form")[0].reset();
        var row = $(this).parents('tr');
        var data = approval_setup_master_datatable.row( row ).data();
        $("#approval-head-form").attr('action',ash_url + "/" + data.approval_setup_master_id )
        						.attr('method',"PUT")
        						.supply(data);
    	$("#approval-head-modal").modal('show');
        return false;
    });


</script>
@endsection