<div class="col-xs-6 mrgB15">
    <button class="btn btn-primary btn-sm"  id="show-approval-head-modal">
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Add
    </button>
    <div class="table-header">
        Approval Header
    </div>

    <!-- div.table-responsive -->

    <!-- div.dataTables_borderWrap -->
    <div>
        <table id="approval-setup-master-table" class="table table-striped table-bordered table-hover">
            <thead>
            <tr>
                <th width="">Category</th>
                <th width="">Subcategory</th>
                <th width="">Role Source</th>
                <th width="">Start Status</th>
                <th width="">Finish Status</th>
                <th width="">Supersede Allowed</th>
                <th width="">Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
@include('approval_setup.includes.approval_head_modal')
@section('approval_head_scripts')
<script type="text/javascript">

    approval_setup_master_datatable = $('#approval-setup-master-table').DataTable( {
        ajax: ash_url + "/list",
        "bLengthChange": false,
        "processing": true,
        orderCellsTop: true,
        createdRow: function( row, data, dataIndex ) {
            if(selectedHead == data.approval_setup_master_id){
                $( row ).addClass('selected-row');
            }
            $(row).attr('data-id', data.approval_setup_master_id );
        },
        columns: [
            { data: "source_category" },
            { data: "source_subcategory", defaultContent: 'n/a'},
            { data: "role_source" },
            { data: "start_status"},
            { data: "finish_status"},
            { data: "supersede_allowed_flag" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+ash_url+'/'+data.approval_setup_master_id+'"><i class="fa fa-pencil edit" title="Edit" rel="tooltip"></i></a> &nbsp; <a href="'+ash_url+'/'+data.approval_setup_master_id+'" class="delete" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );    
    
    $(document).on('click', "#approval-setup-master-table .delete" ,function(){
        $(this).deleteItemFrom(approval_setup_master_datatable);  
        return false;
    });

    $(document).on('click','#approval-setup-master-table tr',function(){
        $("#approval-setup-master-table .selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        selectedHead = $(this).data('id');
        asd_url = "{{ url('approval-setup') }}/" +selectedHead +"/detail";
        approval_setup_detail_datatable.ajax.url( asd_url + "/list" ).load();
    });
</script>
@yield('approval_head_modal_scripts')
@endsection