<!-- Projects Scopes Auditor Modal -->
<div class="modal fade " data-backdrop="static" id="project-scope-auditor-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Project Scope Auditor</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="project-scope-auditor-alert"></div>
                        <form class="form-horizontal" id="project-scope-auditor-form" action="" method="">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="auditor"> Auditor </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm" name="auditor"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="auditor_type"> Role </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm" name="auditor_type"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="allotted_mandays"> Allotted Mandays </label>
                                <div class="col-sm-10">
                                    <input type="text"  class="form-control input-sm" name="allotted_mandays">
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Projects Scopes Auditor Modal -->

@section('project_scope_auditor_modal_scripts')
<script type="text/javascript">
    // project_scope_datatable came from tab_scope

    var psam_auditor_type_select = $("#project-scope-auditor-form [name=auditor_type]").makeSelectize({
        lookup : '{{ config('iapms.lookups.person_project_role') }}',
        onChange: function(value) {
            if (!value.length) return;
            $.ajax({
                url : "{{ url('plan-project') }}/"+selectedProject+"/scope/"+selectedScope+"/suggested-budget",
                method : "GET",
                data : { auditor_type : value},
                success : function(response){
                    var default_mandays = "";
                    switch(value){
                        case "Lead":
                            default_mandays = response.data.Lead;
                            break;

                        case "Reviewer":
                            default_mandays = response.data.Reviewer;
                            break;

                        default:
                            default_mandays = "0";
                            break;
                    }
                    $("#project-scope-auditor-form [name='allotted_mandays']").val(default_mandays);
                }
            })
        }
    })[0].selectize;

    var psam_auditor_select = $("#project-scope-auditor-form [name=auditor]").makeSelectize({
        url: '{{ url( 'plan-project' )}}/'+selectedProject+'/auditor',
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    })[0].selectize;

    $("#project-scope-auditor-form").submit(function(){
        $('.alert').remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#project-scope-auditor-form")[0].reset();
                psam_auditor_type_select.clear();
                psam_auditor_select.clear();
                project_scope_auditor_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
                $("#project-scope-auditor-modal").modal('hide');
            },
            error : function(xhr){
                $("#project-scope-auditor-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $(document).on('click','#show-project-scope-auditor-modal',function(){
        $("#project-scope-auditor-form").clearForm();
        $("#project-scope-auditor-form").attr("action", "{{ url('plan-project-scope')}}/" + selectedScope + "/auditor" ).attr('method','POST');
        psam_auditor_type_select.clear();
        $.reloadURL( psam_auditor_select, '{{ url( 'plan-project' )}}/'+selectedProject+'/auditor' );
        $.sDelay(function(){
            $("#project-scope-auditor-modal").modal('show');
        });
        return false;
    });


    $(document).on('click','#project-scope-auditor-table .edit',function(){
        var url = $(this).attr('href');
        $("#project-scope-auditor-form").clearForm();
        psam_auditor_select.clear();
        psam_auditor_select.clearOptions();
        $("#project-scope-auditor-form").attr("action", url ).attr('method','PUT');
        psam_auditor_select.load( function(callback) {
            $.ajax({
                url: '{{ url( 'plan-project' )}}/'+selectedProject+'/auditor',
                beforeSend : sLoading(),
                success: function(response) {
                    var auditors = response.data;
                    $.ajax({
                        url: url,
                        method : "GET",
                        success: function(response) {
                            callback(auditors);
                            swal.close();
                            var data = response.data;
                            psam_auditor_select.setValue(data.auditor_id);
                            psam_auditor_type_select.setValue(data.auditor_type)
                            $("#project-scope-auditor-form [name='allotted_mandays']").val(data.allotted_mandays);
                            $("#project-scope-auditor-modal").modal('show');
                        }
                    });
                },
                error: function() {
                    callback();
                }
            });
        } );
        
        return false;
    });

</script>
@endsection