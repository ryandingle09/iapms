<div class="col-xs-12 mrgB15">
    <div class="table-header"> Annual Audit Plan - Projects </div>
</div>

<div class="col-xs-12 align-left">
    <a class="btn btn-primary btn-sm " id="show-create-project-modal" >
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>
</div>

<div class="col-xs-12 mrgB15">
    <table id="project-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Project Name</th>
                <th >Audit Type</th>
                <th >Start Date</th>
                <th >End Date</th>
                <th >Status</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-12 mrgB15">
    <div class="table-header"> Project - Auditors </div>
</div>
<div class="col-xs-12">
    <a href="#" class="btn btn-primary btn-sm" id="show-create-project-auditor-modal" disabled>
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>
</div>
<div class="col-xs-12 mrgB15">
    <table id="project-auditor-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Auditor Name</th>
                <th >Allotted Mandays</th>
                <th >Annual Mandays</th>
                <th >Running Mandays</th>
                <th >Available Mandays</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@include('plan.includes.project_modal')
@include('plan.includes.project_auditor_modal')
@section('tab_projects_scripts')
<script type="text/javascript">
    //selectedProject came from plan.index
    var project_datatable = $('#project-table').DataTable( {
        bDestroy: true,
        processing: true,
        order: [[ 1, 'asc' ]],
        createdRow: function( row, data, dataIndex ) {
            if(selectedProject == data.plan_project_id){
                $( row ).addClass('selected-row');
            }
            $(row).attr('data-project-id', data.plan_project_id );
        },
        columns: [
            { data: "plan_project_name" },
            { data: "audit_type" },
            { data: "target_start_date" },
            { data: "target_end_date" },
            { data: "plan_project_status" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('plan-group')}}/'+data.plan_group_id+'/project/'+data.plan_project_id+'" class="edit"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a> <a href="{{url('plan-group')}}/'+data.plan_group_id+'/project/'+data.plan_project_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });;

    var project_auditor_datatable = $('#project-auditor-table').DataTable( {
        "bDestroy": true,
        "processing": true,
        order: [[ 1, 'asc' ]],
        columns: [
            { data: "auditor.full_name"}, 
            { data: "allotted_mandays" },
            { data: "annual_mandays" },
            { data: "running_mandays" },
            { data: "available_mandays" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="proficiency" data-id="'+data.auditor_id+'"><i class="fa fa-user text-primary" title="Proficiency" rel="tooltip"></i></a> <a href="{{ url('plan-project')}}/'+selectedProject+'/auditor/'+data.plan_project_auditor_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });

    $(document).on('click',"#main_tab a[href='#projects']",function(){
        if(selectedGroup){
            project_datatable.ajax.url( "{{ url('plan-group')}}/"+selectedGroup+"/project/list" ).load();
        }else{
            $.sInfo("You have to select annual plan group to open this tab.");
        }
    });

    $(document).on("click","#project-table .delete",function(){
        $(this).deleteItemFrom(project_datatable);
        return false;
    });

    $(document).on('click','#project-table tbody tr',function(){
        $(".selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        var data = project_datatable.row( $(this) ).data();
        selectedProject = data.plan_project_id;
        selectedProjectStatus = data.plan_project_status;
        selectedProjectAuditType = data.audit_type;
        $("#main_tab").find("a[href='#scope']").attr('data-toggle','tab');
        $("#show-create-project-auditor-modal").removeAttr('disabled');
        project_auditor_datatable.ajax.url( "{{url('plan-project')}}/"+selectedProject+"/auditor/list" ).load();
        return false;
    });

    $(document).on("click","#project-auditor-table .delete",function(){
        $(this).deleteItemFrom(project_auditor_datatable);
        return false;
    });

</script>
@yield('project_modal_scripts')
@yield('project_auditor_modal_scripts')
@endsection
