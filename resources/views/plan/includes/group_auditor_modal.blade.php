<!-- Create Plan Auditor Modal -->
<div class="modal fade " data-backdrop="static" id="create-group-auditor-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Plan Group Auditor</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-group-auditor-alert"></div>
                        <form class="form-horizontal" id="create-group-auditor-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="auditor"> Auditor </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm auditor" name="auditor"></select>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Close
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Plan Auditor Modal -->
@section('group_auditor_modal_scripts')
<script type="text/javascript">
    // group_auditor_datatable came form tab_projects

    var gam_auditor_select = $("#create-group-auditor-form .auditor").makeSelectize({
        url: "{{ url('auditor/show')}}",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    })[0].selectize;

    $("#create-group-auditor-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url : "{{ url('plan-group')}}/" + selectedGroup + '/auditor',
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                gam_auditor_select.clear();
                $("#create-group-auditor-form")[0].reset();
                // $("#create-group-auditor-modal").modal('hide');
                group_auditor_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
                $(".alert").remove();
            },
            error : function(xhr){
                $("#create-group-auditor-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $("#show-create-group-auditor-modal").click(function(){
        $("#create-group-auditor-form").clearForm();
        $.reloadURL( gam_auditor_select, '{{ url( 'plan' )}}/'+selectedPlan+'/auditor' );
        $("#create-group-auditor-form").attr('action', "{{url('plan-group')}}/"+selectedGroup+"/auditor" );
        $.sDelay(function(){
            $("#create-group-auditor-modal").modal('show');
        });
        
        return false;
    });

</script>
@endsection