<div class="col-xs-12 mrgB15">
    <div class="table-header"> Annual Audit Plan - Group </div>
</div>

<div class="col-xs-12 align-left">
    <a class="btn btn-primary btn-sm " id="show-create-group-modal" >
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>
</div>

<div class="col-xs-12 mrgB15">
    <table id="group-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Group Name</th>
                <th >Description</th>
                <th >Group Head</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
<div class="col-xs-12 mrgB15">
    <div class="table-header"> Group - Auditors </div>
</div>
<div class="col-xs-12">
    <a href="#" class="btn btn-primary btn-sm" id="show-create-group-auditor-modal" disabled>
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>
</div>
<div class="col-xs-12 mrgB15">
    <table id="group-auditor-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Auditor Name</th>
                <th >Allotted Mandays</th>
                <th >Annual Mandays</th>
                <th >Running Mandays</th>
                <th >Available Mandays</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@include('plan.includes.group_modal')
@include('plan.includes.group_auditor_modal')
@section('tab_group_scripts')
<script type="text/javascript">
    // selectedGroup came from plan.index
    var group_datatable = $('#group-table').DataTable( {
        bDestroy: true,
        processing: true,
        order: [[ 1, 'asc' ]],
        createdRow: function( row, data, dataIndex ) {
            if(selectedGroup == data.plan_group_id){
                $( row ).addClass('selected-row');
            }
            $(row).attr('data-group-id', data.plan_group_id );
        },
        columns: [
            { data: "plan_group_name" },
            { data: "description" },
            { data: "group_head_details.full_name" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="{{url('plan')}}/'+data.plan_id+'/group/'+data.plan_group_id+'" class="edit"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a> <a href="{{url('plan')}}/'+data.plan_id+'/group/'+data.plan_group_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });

    var group_auditor_datatable = $('#group-auditor-table').DataTable( {
        "bDestroy": true,
        "processing": true,
        order: [[ 1, 'asc' ]],
        columns: [
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    return data.auditor.full_name;
                }
            },
            { data: "allotted_mandays" },
            { data: "annual_mandays" },
            { data: "running_mandays" },
            { data: "available_mandays" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="proficiency" data-id="'+data.auditor_id+'"><i class="fa fa-user text-primary" title="Proficiency" rel="tooltip"></i></a> <a href="{{ url('plan-group')}}/'+selectedGroup+'/auditor/'+data.plan_group_auditor_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });

    $(document).on('click',"#main_tab a[href='#group']",function(){
        if(selectedPlan){
            group_datatable.ajax.url(  "{{ url('plan')}}/"+selectedPlan+"/group/list" ).load();
        }else{
            $.sInfo("You have to select annual plan to open this tab.");
        }
    });

    $(document).on("click","#group-table .delete",function(){
        $(this).deleteItemFrom(group_datatable);
        return false;
    });

    $(document).on('click','#group-table tbody tr',function(){
        $(".selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        var data = group_datatable.row( $(this) ).data();
        selectedGroup = data.plan_group_id;
        $("#main_tab").find("a[href='#projects']").attr('data-toggle','tab');
        $("#show-create-group-auditor-modal").removeAttr('disabled');
        group_auditor_datatable.ajax.url(  "{{url('plan-group')}}/"+selectedGroup+"/auditor/list" ).load();
        return false;
    });

    $(document).on("click","#group-auditor-table .delete",function(){
        $(this).deleteItemFrom(group_auditor_datatable);
        return false;
    });

</script>
@yield('group_modal_scripts')
@yield('group_auditor_modal_scripts')
@endsection
