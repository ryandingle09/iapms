<div class="col-xs-12 mrgB15">
    <div class="table-header"> Project Scopes </div>
</div>
<div class="col-xs-12">
    <a class="btn btn-primary btn-sm" id='show-create-project-scope-modal'>
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>
</div>
<div class="col-xs-12 mrgB15">
    <table id="project-scope-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Auditable Entity Name</th>
                <th >Main Business Process Name</th>
                <th >Budgeted Mandays</th>
                <th >Adjusted Severity Value</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>

        <tbody></tbody>
    </table>
</div>
<div class="col-xs-12 mrgB15">
    <div class="table-header"> Project Scope - Auditors </div>
</div>
<div class="col-xs-12">
    <a class="btn btn-primary btn-sm" id='show-project-scope-auditor-modal' disabled="disabled">
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>
</div>
<div class="col-xs-12 mrgB15">
    <table id="project-scope-auditor-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Auditor Name</th>
                <th >Role</th>
                <th >Allotted Mandays</th>
                <th >Annual Mandays</th>
                <th >Running Mandays</th>
                <th >Available Mandays</th>
                <th class="text-center" width="10%">Actions</th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
</div>

@include('plan.includes.project_scope_modal')
@include('plan.includes.project_scope_auditor_modal')
@include('plan.includes.project_scope_iom_modal')
@include('includes.modals.custom_measures')

@section('tab_scope_scripts')
<script type="text/javascript">
    // selectedProject initialized in index
    // selectedScope initialized in index 

    var scopeUrl = "{{ url('plan-project')}}/"+selectedProject+"/scope";
    var scopeAuditorUrl = "{{ url('plan-project-scope')}}/" + selectedScope + "/auditor";

    var project_scope_datatable = $("#project-scope-table").DataTable( {
        "bDestroy": true,
        "processing": true,
        order: [[ 1, 'asc' ]],
        createdRow: function( row, data, dataIndex ) {
            if(selectedScope == data.plan_project_scope_id){
                $( row ).addClass('selected-row');
            }
            $( row ).attr('data-id', data.plan_project_scope_id);
        },
        columns: [
            { data: "auditable_entity.auditable_entity_name" },
            { data: "main_bp.main_bp_name" },
            { data: "budgeted_mandays" },
            { data: "adjusted_severity_value" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    var iom_button = "";
                    // project scope id will be set if iom is empty
                    var iom_url = data.plan_project_scope_id; 
                    if(selectedProjectStatus == 'Approved'){
                        if(data.iom != null){
                            // full link will be set if iom is not empty
                            iom_url = "{{ url('project-scope')}}/" + data.plan_project_scope_id +"/iom/" + data.iom.project_scope_iom_id;
                        }
                        iom_button = '<a href="'+iom_url+'" data-id="'+data.plan_project_scope_id+'" class="btn-generate-iom" ><i class="fa fa-envelope text-primary" title="Generate IOM" rel="tooltip"></i></a>';
                    }
                    return '<a href="#" class="btn-custom-measure" data-ae_id="'+data.auditable_entity_id+'" data-id="'+data.main_bp.master_ae_mbp_id+'" ><i class="fa fa-dashboard text-primary" title="Custom Measures" rel="tooltip"></i></a>     '+iom_button+'    <a href="" class="btn-mbpa-details" data-aeid="'+data.auditable_entity_id+'" data-mbpaid="'+data.mbp_id+'" ><i class="fa fa-sitemap text-primary" title="Details" rel="tooltip"></i></a>    <a href="'+scopeUrl+'/'+data.plan_project_scope_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });

    var project_scope_auditor_datatable = $("#project-scope-auditor-table").DataTable( {
        "bDestroy": true,
        "processing": true,
        order: [[ 1, 'asc' ]],
        columns: [
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    return data.auditor.first_name+' '+data.auditor.middle_name+' '+data.auditor.last_name;
                }
            },
            { data: "auditor_type" },
            { data: "allotted_mandays" },
            { data: "annual_mandays" },
            { data: "running_mandays" },
            { data: "available_mandays" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="proficiency" data-id="'+data.auditor_id+'"><i class="fa fa-user text-primary" title="Proficiency" rel="tooltip"></i></a>    <a href="'+scopeAuditorUrl+'/'+data.plan_project_scope_auditor_id+'" class="edit"><i class="fa fa-pencil" title="Edit" rel="tooltip"></i></a>     <a href="'+scopeAuditorUrl+'/'+data.plan_project_scope_auditor_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });

    $(document).on('click',"#main_tab a[href='#scope']",function(){
        if(selectedProject){
            project_scope_datatable.ajax.url( "{{ url('plan-project')}}/"+selectedProject+"/scope/list" ).load();
        }else{
            $.sInfo("You have to select project to open this tab.");
        }
    });

    $(document).on("click","#project-scope-table .delete",function(){
        $(this).deleteItemFrom(project_scope_datatable);
        return false;
    });

    $(document).on('click','#project-scope-table tbody tr',function(){
        $("#_engagement_tab").find("a[href='#apg']").attr('data-toggle','tab');
        selectedScope = $(this).data('id');
        $(".selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        $("#show-project-scope-auditor-modal").removeAttr('disabled');
        scopeAuditorUrl = "{{ url('plan-project-scope')}}/" + selectedScope + "/auditor";
        $("#create-project-scope-auditor-form").attr('action',scopeAuditorUrl);
        project_scope_auditor_datatable.ajax.url( scopeAuditorUrl + "/list" ).load();
    });


    $(document).on("click","#project-scope-auditor-table .delete",function(){
        $(this).deleteItemFrom(project_scope_auditor_datatable);
        return false;
    });

</script>
@yield('project_scope_modal_scripts')
@yield('project_scope_auditor_modal_scripts')
@yield('project_scope_iom_modal_scripts')
@yield('ae_details_modal_scripts')
@yield('cm_scripts')
@endsection