<!-- Create Projects Scope Modal -->
<div class="modal fade " data-backdrop="static" id="create-project-scope-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Project Scope</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-project-scope-alert"></div>
                        <form class="form-horizontal" id="create-project-scope-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-4 control-label align-left " for="auditable_entity"> Auditable Entity </label>
                                <div class="col-sm-8">
                                    <select class="form-control input-sm auditable_entity" name="auditable_entity"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="main_business_process"> Main Business Process </label>
                                <div class="col-sm-8">
                                    <select class="form-control input-sm main_business_process" name="main_business_process"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="budgeted_mandays"> Budgeted Mandays </label>
                                <div class="col-sm-8">
                                    <input type="text" name="budgeted_mandays" class="form-control input-sm budgeted_mandays">
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label class="col-sm-4 control-label align-left" for="location"> Audit Location </label>
                                <div class="col-sm-8">
                                    <input type="text" id="location" name="location" class="form-control input-sm basic-info-modal">
                                </div>
                            </div>-->
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Projects Scope Modal -->
@section('project_scope_modal_scripts')
<script type="text/javascript">
    // project_scope_auditor_datatable from tab_scope
    // selectedProjectAuditType from index
    


    var cpsm_entity_select = $("#create-project-scope-form .auditable_entity").selectize({
        valueField: 'auditable_entity_id',
        labelField: 'auditable_entity_name',
        searchField: 'auditable_entity_name',
        options: [],
        create: false,
        render: {
            option: function (item, escape) {
                return " <div>" +escape(item.auditable_entity_name) + "</div>";
            }
        },
        load: function (query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '{{ url( 'auditable_entities/lists' )}}?search=' + encodeURIComponent(query) + '&searchFields=auditable_entity_name%3Alike&entity_type=actual',
                type: 'GET',
                success: function (res) {
                    callback(res);
                },
                error: function () {
                    callback();
                }
            });
        },
        onChange: function(value) {
            if (!value.length) return;
            cpsm_mbp_select.clear();
            cpsm_mbp_select.disable();
            cpsm_mbp_select.clearOptions();
            cpsm_mbp_select.load(function(callback) {
                $.ajax({
                    // TODO : get list of mbp actual from the rigth table
                    url: '{{ url( 'aea' )}}/'+value+'/mbpa?audit_type_filter=' + selectedProjectAuditType,
                    success: function(response) {
                        cpsm_mbp_select.enable();
                        callback(response.data);
                    },
                    error: function() {
                        callback();
                    }
                })
            });
        }
    })[0].selectize;

    var cpsm_mbp_select = $("#create-project-scope-form .main_business_process").selectize({
        valueField: 'ae_mbp_id',
        labelField: 'main_bp_name',
        searchField: 'main_bp_name',
        options: [],
        create: false,
        render: {
            option: function (item, escape) {
                return '<div>'+escape(item.main_bp_name) + '<div>';
            }
        },
        onChange: function(value) {
            if (!value.length) return;
            $.ajax({
                url : "{{ url('auditable_entities')}}/0/mbp/"+value+"/budgeted-mandays",
                method : "GET",
                data : { audit_type : selectedProjectAuditType },
                // beforeSend : sLoading(),
                success : function(response){
                    swal.close();
                    console.log(response.data)
                    $("#create-project-scope-form [name='budgeted_mandays']").val(response.data);
                }
            });
            
        }
    })[0].selectize;
    
    cpsm_mbp_select.disable();

    $('#create-project-scope-modal .input-daterange').datepicker({
        format: "dd-M-yyyy",
        startDate: 'today',
        keyboardNavigation: false,
        autoclose : true,
    });

    $("#create-project-scope-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#create-project-scope-form")[0].reset();
                $("#create-project-scope-modal").modal('hide');
                cpsm_entity_select.clear();
                cpsm_mbp_select.clear();
                project_scope_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                switch(parseInt(xhr.status)){
                    case 422:
                        swal.close();
                        alertWrapper = $("#create-project-scope-alert");
                        $("#create-project-scope-form").showError(xhr.responseJSON,alertWrapper);
                        $("#create-project-scope-modal").resetModalHeight();
                        break;
                    case 500:
                        $.sError("Error "+xhr.status+" : "+xhr.statusText);
                    break;
                }
            }
        }); 
        return false;
    });

    $("#show-create-project-scope-modal").click(function(){
        cpsm_entity_select.clear();
        cpsm_mbp_select.clear();
        $("#create-project-scope-form").clearForm();
        $("#create-project-scope-form").attr('action',"{{ url('plan-project')}}/"+selectedProject+"/scope").attr('method','POST');
        $("#create-project-scope-modal").modal('show');
        return false;
    });
</script>
@endsection