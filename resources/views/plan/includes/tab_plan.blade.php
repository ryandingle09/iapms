<div class="col-xs-12">
    <form method="GET" action="" class="form-horizontal" id="plan-search">
        <div class="form-group">
            <div class="col-sm-5">
                <div class="input-group">
                    <input type="number"  name="search_year" class="form-control" placeholder="Please type year here (ex. 2017 or 2018)" value="{{ $search_year }}">
                    <span class="input-group-btn">
                        <button class="btn btn-primary">
                            <i class="ace-icon fa fa-search bigger-110"></i>
                            Go!
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="col-xs-12 mrgB15" id="plan-details">
    <div class="table-header mrgB15">
        Annual Audit Plan {{ isset($plan) ? 'for '.$plan->plan_year : '' }}
    </div>
    @if(isset($plan))
    <div id="edit-plan-alert"></div>
    <form class="form-horizontal" id="edit-plan-form" action="{{ route('plan.update',[ 'planId' => $plan->plan_id]) }}" method="POST">
        <div class="form-row row mrgB15" >
            <div class="col-sm-6 align-left">
                <a href="{{ route('plan.index') }}?create_form=true" class="btn btn-primary btn-sm btn-create">
                    <i class="ace-icon fa fa-plus bigger-120"></i>
                    Create
                </a>
                <a href="{{ route('plan_guide.index') }}?dyear={{ $plan->plan_year }}" class="btn btn-success btn-sm">
                    <i class="ace-icon fa fa-shopping-cart bigger-120"></i>
                    Smart Guide
                </a>  
            </div>
            <div class="col-sm-6 align-right">
              <button class="btn btn-info btn-sm">
                    <i class="ace-icon fa fa-save bigger-120"></i>
                    Apply
                </button>
                <a href="{{ route('plan.index') }}" class="btn btn-warning btn-sm" data-dismiss="modal">
                    <i class="ace-icon fa fa-times bigger-120"></i>
                    Cancel
                </a>  
            </div>
            
        </div>
        <div class="form-group" >
            <label class="col-sm-2 control-label align-left " for="plan_year"> Annual Audit Year </label>
            <div class="col-sm-4">
                <input type="text" name="plan_year" class="form-control input-sm makelabel" value="{{ $plan->plan_year }}" readonly>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="plan_name"> Plan Name </label>
            <div class="col-sm-4">
                <input type="text" name="plan_name" class="form-control input-sm {{ ( $plan->plan_status == 'New') ? '' : 'makelabel'}}" value="{{ $plan->plan_name }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="plan_status"> Status </label>
            <div class="col-sm-4">
                <input type="text" name="plan_status" class="form-control input-sm plan_status" value="{{ $plan->plan_status }}" readonly="">

            </div>
            <!--<div class="col-sm-4">
                <a href="#" class="btn btn-success btn-sm btn-plan-status" style="display:none; padding: 6.5px 5px;">
                    <i class="ace-icon fa fa-check bigger-120"></i>
                    <span class="capt">n/a</span>
                </a>
            </div>-->
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="remarks"> Remarks </label>
            <div class="col-sm-10">
                <textarea name="remarks" class="form-control {{ ( $plan->plan_status == 'New') ? '' : 'makelabel'}}" rows="4">{{ $plan->remarks }}</textarea>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="approver_fullname"> Approved By </label>
            <div class="col-sm-4">
                <input type="text" name="approver_fullname" class="form-control input-sm makelabel" value="{{ isset($plan->end_approval) ? $plan->end_approval->approver->full_name  : 'n/a' }}">
            </div>
            <label class="col-sm-2 control-label align-left" for="approved_date"> Approved Date </label>
            <div class="col-sm-4">
                <input type="text" name="approved_date" class="form-control input-sm makelabel" value="{{ $plan->end_approval  ? $plan->end_approval->approval_date : 'n/a' }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="allotted_mandays"> Auditor's Allotted Mandays </label>
            <div class="col-sm-4">
                <input type="text" name="allotted_mandays" class="form-control input-sm makelabel" value="{{ $plan->allotted_mandays }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="annual_mandays"> Auditor's Annual Mandays </label>
            <div class="col-sm-4">
                <input type="text" name="annual_mandays" class="form-control input-sm makelabel" value="{{ $plan->annual_mandays ? : 'n/a' }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="available_mandays"> Auditor's Available Mandays </label>
            <div class="col-sm-4">
                <input type="text" name="available_mandays" class="form-control input-sm makelabel" value="{{ $plan->available_mandays ? : 'n/a' }}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label align-left" for="approver_remarks"> Approver Remarks </label>
            <div class="col-sm-10">
                <textarea name="approver_remarks" class="form-control makelabel" rows="4" readonly="">{{ $plan->end_approval  ? $plan->end_approval->remarks : 'n/a' }}</textarea>
            </div>
        </div>
        
        <div class="form-row align-right" >
            <button class="btn btn-info btn-sm">
                <i class="ace-icon fa fa-save bigger-120"></i>
                Apply
            </button>
            <a href="{{ route('plan.index') }}" class="btn btn-warning btn-sm" data-dismiss="modal">
                <i class="ace-icon fa fa-times bigger-120"></i>
                Cancel
            </a>
        </div>
    </form>
    @else
        @if( strlen($search_year) || isset($create_form) )
        @if(!isset($create_form))
        <div id="create-plan-alert">
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert">
                    <i class="ace-icon fa fa-times"></i>
                </button>
                <strong> <i class="ace-icon fa fa-warning"></i> </strong> Plan doesn't exist. Please search other year or  fill out the form below to create a plan for that year
                <br>
            </div>
        </div>
        @endif
        <form class="form-horizontal" id="create-plan-form" action="{{ route('plan.store') }}" method="POST">
            <div class="form-row align-right" >
                <button class="btn btn-info btn-sm">
                    <i class="ace-icon fa fa-save bigger-120"></i>
                    Apply
                </button>
                <a href="{{ route('plan.index') }}" class="btn btn-warning btn-sm" data-dismiss="modal">
                    <i class="ace-icon fa fa-times bigger-120"></i>
                    Cancel
                </a>
            </div>
            <div class="form-group" >
                <label class="col-sm-2 control-label align-left " for="plan_year"> Annual Audit Year </label>
                <div class="col-sm-4">
                    <input type="text" name="plan_year" class="form-control input-sm" value="{{ $search_year }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label align-left" for="plan_name"> Plan Name </label>
                <div class="col-sm-4">
                    <input type="text" name="plan_name" class="form-control input-sm">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label align-left" for="remarks"> Remarks </label>
                <div class="col-sm-10">
                    <textarea name="remarks" class="form-control" rows="4"></textarea>
                </div>
            </div>
            <div class="form-row align-right" >
                <input type="hidden" name="plan_status" value="New">
                <button class="btn btn-info btn-sm">
                    <i class="ace-icon fa fa-save bigger-120"></i>
                    Apply
                </button>
                <a href="{{ route('plan.index') }}" class="btn btn-warning btn-sm" data-dismiss="modal">
                    <i class="ace-icon fa fa-times bigger-120"></i>
                    Cancel
                </a>
            </div>
        </form>
        @else
        <div class="alert alert-info">
            <strong> <i class="ace-icon fa fa-info"></i> </strong> Please type the year above to search or create an annual plan.
            <br>
        </div>
        <a href="{{ route('plan.index') }}?create_form=true" class="btn btn-primary btn-sm btn-create">
            <i class="ace-icon fa fa-plus bigger-120"></i>
            Create
        </a>
        @endif
    @endif
</div>
@if($plan)
<div class="col-xs-12 mrgB15" id="plan-details">
    <div class="table-header">
        Annual Audit Plan - Auditors
    </div>
</div>
<div class="col-xs-12">
    <a class="btn btn-primary btn-sm" id="show-create-plan-auditor-modal">
        <i class="ace-icon fa fa-plus bigger-120"></i>
        Create
    </a>
    <a class="btn btn-primary btn-sm" id="caam">
        <i class="ace-icon fa fa-refresh bigger-120"></i>
        CAAM
    </a>
</div>
<div class="col-xs-12 mrgB15">
    <table id="plan-auditor-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Auditor Name</th>
                <th >Allotted Mandays</th>
                <th >Annual Mandays</th>
                <th >Running Mandays</th>
                <th >Available Mandays</th>
                <th >Action</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@include('plan.includes.plan_auditor_modal')
@include('includes.modals.auditor_mandays_breakdown_modal')
@endif
@section('tab_plan_scripts')
<script type="text/javascript">
    var planStatus = '{{ isset($plan->plan_status) ? $plan->plan_status : 0 }}';
    $("#create-plan-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                location.href = "?search_year=" + response.data.plan_year;
            }
        }); 
        return false;
    });

    plan_auditor_datatable = $('#plan-auditor-table').DataTable( {
        "bDestroy": true,
        ajax: "{{ url('plan')}}/" + selectedPlan +"/auditor/list",
        "processing": true,
        order: [[ 1, 'asc' ]],
        columns: [
            {
                data: null,
                render : function ( data, type, full, meta ) {
                    return data.auditor.first_name+' '+data.auditor.middle_name+' '+data.auditor.last_name;
                }
            },
            { data: "allotted_mandays" },
            { data: "annual_mandays" },
            { data: "running_mandays" },
            { data: "available_mandays" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="mandays_breakdown" data-id="'+data.auditor_id+'" data-plan_id="'+data.plan_id+'"><i class="fa fa-list text-primary" title="Mandays Breakdown" rel="tooltip"></i></a>    <a href="#" class="proficiency" data-id="'+data.auditor_id+'"><i class="fa fa-user text-primary" title="Proficiency" rel="tooltip"></i></a> <a href="{{ url('plan')}}/'+selectedPlan+'/auditor/'+data.plan_auditor_id+'" class="delete"><i class="fa fa-trash text-danger" title="Delete" rel="tooltip"></i></a>';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });

    $(document).on("click","#plan-auditor-table .delete",function(){
        $(this).deleteItemFrom(plan_auditor_datatable);
        return false;
    });

    $("#edit-plan-form .makelabel").makeLabel();

    $("#edit-plan-form").submit(function(){
        $(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : "PUT",
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $.sSuccess(response.message,function(){
                    location.reload();
                });
            }
        }); 
        return false;
    });

    $.reloadPlan = function(){
        $.ajax({
            url : "{{ url('plan') }}/" + selectedPlan,
            method : "GET",
            success : function(response){
                data = response.data;
                data['approver_fullname'] = data.end_approval ? data.end_approval.approver.full_name :  "n/a";
                data['approved_date'] = data.end_approval ? data.end_approval.approval_date :  "n/a";
                data['approver_remarks'] = data.end_approval ? data.end_approval.remarks :  "n/a";
                $("#edit-plan-form").supply(response.data);
                $("#edit-plan-form .makelabel").makeLabel();
            } 
        })
    }

    $("#caam").click(function(){
        $.ajax({
            url : "{{ url('plan')}}/" + selectedPlan + "/auditor/annual-mandays",
            method : "PUT",
            data : {
                year : "{{ isset($plan) ? $plan->plan_year : 0 }}"
            },
            beforeSend : sLoading(),
            success : function(response){
                plan_auditor_datatable.ajax.reload( null, false );
                $.reloadPlan();
                $.sSuccess(response.message);
            }
        }); 
        return false;
    });

    

</script>
@yield('plan_auditor_modal_scripts')
@yield('auditor_mandays_breakdown_modal_scripts')
@endsection