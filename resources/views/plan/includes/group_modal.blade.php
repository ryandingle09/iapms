<!-- Create Group Modal -->
<div class="modal fade " data-backdrop="static" id="create-group-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Plan Group</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-group-alert"></div>
                        <form class="form-horizontal" id="create-group-form" action="" method="POST">
                            <div class="form-group" >
                                <label class="col-sm-3 control-label align-left " for="plan_group_name"> Plan Group Name </label>
                                <div class="col-sm-9">
                                	<input type="text" name="plan_group_name" class="form-control input-sm">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label align-left" for="description"> Description </label>
                                <div class="col-sm-9">
                                	<textarea name="description" class="form-control input-sm" ></textarea>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-3 control-label align-left " for="group_head"> Group Head </label>
                                <div class="col-sm-9">
                                	<select type="text" name="group_head" class="form-control input-sm"></select>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Close
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Group Modal -->
@section('group_modal_scripts')
<script type="text/javascript">

	 var cgm_group_head_select = $("#create-group-form [name='group_head']").makeSelectize({
        url: "{{ url('plan')}}/" + selectedPlan + "/auditor",
        selectedTemplate : function(item, escape){
            return '<div> '+item.text+' | '+item.position+'</div>';
        }
    })[0].selectize;

	$("#show-create-group-modal").click(function(){
        cgm_group_head_select.clear();
        $(".alert").remove();
        $(".has-error").removeClass('has-error');
        var url = "{{ url('plan')}}/"+selectedPlan+"/group";
        $("#create-group-form").attr('action', url).attr('method','POST');
        $("#create-group-form")[0].reset();
        $("#create-group-modal").modal('show');
        return false;
    });

    $(document).on('click','#group-table .edit',function(){
        cgm_group_head_select.clear();
        $(".alert").remove();
        $(".has-error").removeClass('has-error');
        var url = $(this).attr('href');
        $("#create-group-form")[0].reset();
        $.ajax({
            url : url,
            method : "GET",
            success : function(response){
                var data = response.data;
                cgm_group_head_select.setValue(data.group_head);
                delete data['group_head'];
                $("#create-group-form").attr('action', url).attr('method','PUT').supply(data);
                $("#create-group-modal").modal('show');
            }
        });
        return false;
    });

    $("#create-group-form").submit(function(){
    	$(".alert").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){

                $("#create-group-modal").modal('hide');
                group_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#create-group-form").modalFormErrorHandler(xhr);
            }
        }); 
    	return false;
    });
</script>
@endsection