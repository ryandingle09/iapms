<!-- Create Project Modal -->
<div class="modal fade " data-backdrop="static" id="create-project-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Create Project</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    
                    <div class="col-xs-12 ">
                        <div id="create-project-alert"></div>
                        <form class="form-horizontal" id="create-project-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="plan_project_name"> Project Name </label>
                                <div class="col-sm-10">
                                    <input type="text" name="plan_project_name" class="form-control input-sm plan_project_name" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_type"> Audit Type </label>
                                <div class="col-sm-10">
                                    <select class="form-control input-sm" name="audit_type"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="plan_project_status"> Status </label>
                                <div class="col-sm-10">
                                    <input type="text" name="plan_project_status" class="form-control input-sm plan_project_status" value="New" readonly="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-10">
                                    <div class="input-daterange input-group">
                                        <input type="text" name="target_start_date" class="form-control input-sm form-control pm-datepicker" autocomplete="off" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" name="target_end_date" class="form-control input-sm form-control pm-datepicker" autocomplete="off" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="plan_project_remarks"> Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm" name="plan_project_remarks" id="plan_project_remarks" rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Create Project Modal -->

<!-- Edit Project Modal -->
<div class="modal fade " data-backdrop="static" id="edit-project-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Project</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div id="edit-project-alert"></div>
                        <form class="form-horizontal" id="edit-project-form" action="" method="POST">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="#" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label align-left " for="plan_project_name"> Project Name </label>
                                <div class="col-sm-4">
                                    <input type="text" name="plan_project_name" id="plan_project_name" class="form-control input-sm" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="audit_type"> Audit Type </label>
                                <div class="col-sm-4">
                                    <input type="text" name="audit_type" class="form-control input-sm makelabel">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="plan_project_status"> Status </label>
                                <div class="col-sm-4">
                                    <input type="text" name="plan_project_status" class="form-control input-sm plan_project_status" value="" readonly="">

                                </div>
                                <!--<div class="col-sm-4">
                                    <a href="#" class="btn btn-success btn-sm btn-project-status" style="display:none; padding: 6.5px 5px;">
                                        <i class="ace-icon fa fa-check bigger-120"></i>
                                        <span class="capt">n/a</span>
                                    </a>
                                </div>-->
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="name"> Target Date </label>
                                <div class="col-sm-4">
                                    <div class="input-daterange input-group">
                                        <input type="text" name="target_start_date" class="form-control input-sm form-control pm-datepicker" />
                                        <span class="input-group-addon">to</span>
                                        <input type="text" name="target_end_date" class="form-control input-sm form-control pm-datepicker" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="plan_project_remarks"> Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm" name="plan_project_remarks" id="plan_project_remarks" rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="approver_fullname"> Approved By </label>
                                <div class="col-sm-4">
                                    <input type="text" name="approver_fullname" class="form-control input-sm makelabel" value="n/a">
                                </div>
                                <label class="col-sm-2 control-label align-left" for="approved_date"> Approved Date </label>
                                <div class="col-sm-4">
                                    <input type="text" name="approved_date" class="form-control input-sm makelabel" value="n/a">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="running_allotted_mandays"> Running Allotted Mandays </label>
                                <div class="col-sm-4">
                                    <input type="text" name="running_allotted_mandays" id="running_allotted_mandays" class="form-control input-sm makelabel" value="" readonly="">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="approver_remarks"> Approver Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm approver_remarks makelabel" name="approver_remarks" rows="3" readonly="">n/a</textarea>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Cancel
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Edit Project Modal -->

@section('project_modal_scripts')
<script type="text/javascript">
    //project_datatable came from engagement_planning.tab_projects
    var pmyear = "{{ isset($plan) ? $plan->plan_year : date('Y') }}";
    var pmMinDate = new Date(pmyear,0,01);
    var pmMaxDate = new Date(pmyear,11,31);

    var cpm_audit_type_select = $("#create-project-form [name='audit_type']").makeSelectize({
        lookup : '{{ config('iapms.lookups.audit_type') }}'
    })[0].selectize;

    $('.pm-datepicker').datepicker({
        format: "M-dd-yyyy",
        startDate : pmMinDate,
        endDate : pmMaxDate,
        keyboardNavigation: false,
        autoclose : true
    });

    $("#create-project-form").submit(function(){
        $(this).find('.has-error').removeClass('has-error');
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                cpm_audit_type_select.clear();
                $("#create-project-form")[0].reset();
                $("#create-project-modal").modal('hide');
                project_datatable.ajax.reload( null, false );
                $.sSuccess(response.message);
            },
            error : function(xhr){
                $("#create-project-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

    $("#show-create-project-modal").click(function(){
        $(".alert").remove();
        $(".has-error").removeClass('has-error');
        cpm_audit_type_select.clear();
        $("#create-project-form").attr('action', "{{ url('plan-group')}}/"+selectedGroup+"/project");
        $("#create-project-form")[0].reset();
        $("#create-project-modal").modal('show');
        return false;
    });

    // $('#edit-project-form .input-daterange').datepicker({
    //     format: "M-dd-yyyy",
    //     // startDate: 'today',
    //     keyboardNavigation: false,
    //     autoclose : true,
    //     minDate : pmMinDate,
    //     maxDate : pmMaxDate
    // }); 

    

    $(document).on("click","#project-table .edit",function(){
        var project_id = $(this).parents('tr').data('project-id');
        var url = $(this).attr('href');
        $.ajax({
            url : url,
            method : "GET",
            success : function(response){
                data = response.data;
                swal.close();

                data['approver_fullname'] = data.end_approval == null ? 'n/a' : data.end_approval.approver.full_name;
                data['approved_date'] = data.end_approval == null ? 'n/a' : data.end_approval.approval_date;
                data['approver_remarks'] = data.end_approval == null ? 'n/a' : data.end_approval.remarks;

                $("#edit-project-form").supply(data);
                $("#edit-project-form").attr('action',url);
                $("#edit-project-form .makelabel").makeLabel();

                $('#edit-project-form .input-daterange [name="target_start_date"]').datepicker('update', data.target_start_date);
                $('#edit-project-form .input-daterange [name="target_end_date"]').datepicker('update', data.target_end_date);
                
                if(data.editable){
                    $("#edit-project-modal .btn-info").show();
                }else{
                    $("#edit-project-modal .btn-info").hide();
                }
                $("#edit-project-modal").modal('show');

                
            }
        });
        return false;
    });

    $("#edit-project-form").submit(function(){
        $(".alerts").remove();
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#edit-project-modal").modal('hide');
                $.sSuccess(response.message);
                project_datatable.ajax.reload( null, false );
            },
            error : function(xhr){
                $("#edit-project-form").modalFormErrorHandler(xhr);
            }
        }); 
        return false;
    });

</script>
@endsection