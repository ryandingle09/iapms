@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Business Process
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            {{-- <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button> --}}
                                            <a href="{{route('business_process.edit', ['bp_id' => $bp->bp_id])}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-chevron-left bigger-120"></i>
                                                Back
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="table-header"> Business Process Area </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div style="display:none;" class="alert alert-warning alert-main"></div>
                                        {!! Form::open(['url' => route('business_process.steps.update', ['bp_step_id' => $details->bp_step_id]), 'class' => 'form-horizontal', 'id' => 'bpsteprisk-form', 'style' => 'margin-top: 12px;']) !!}
                                            @if(isset($details))
                                                <input type="hidden" name="_method" value="PUT">
                                                <input type="hidden" name="id" value="{{ $details->bp_step_id }}">
                                            @endif
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="sequence"> Sequence </label>
                                                <div class="col-sm-1">
                                                    <input type="number" class="form-control input-sm" value="{{ $details->bp_steps_seq }}" disabled="disabled">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="activity_narrative"> Activity Narrative </label>
                                                <div class="col-sm-8">
                                                    <textarea name="activity_narrative" id="activity_narrative" class="form-control" rows="5" style="resize: none;" disabled="disabled">{{ $details->activity_narrative }}</textarea>
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#bpsteprisk-modal">
                                            <i class="ace-icon fa fa-plus bigger-120"></i>
                                            Add
                                        </button>
                                        <div class="table-header"> Business Process Area - Risk </div>
                                        <table id="bpsteps-table" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="10%">Sequence</th>
                                                    <th width="20%">Risk Name</th>
                                                    <th width="63%">Risk Description</th>
                                                    <th width="7%"></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            {{-- <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                Save
                                            </button> --}}
                                            <a href="{{route('business_process.edit', ['bp_id' => $bp->bp_id])}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-chevron-left bigger-120"></i>
                                                Back
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<!-- Step modal -->
<div class="modal fade" data-backdrop="static" id="bpsteprisk-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Business Process Area - Risk Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning modal-alert"></div>
                {!! Form::open(['method' => 'post', 'url' => route('business_process.steps.risks.store', ['bp_step_id' => $details->bp_steps_id]), 'class' => 'form-horizontal', 'id' => 'bpstepsrisk-modal-form']) !!}
                    <input type="hidden" name="id" value="{{$details->bp_steps_id}}">
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="sequence"> Sequence </label>
                        <div class="col-sm-2">
                            <input type="number" required class="form-control input-sm" id="sequence" name="sequence" value="" min="1" step="1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="risk"> Risk Name </label>
                        <div class="col-sm-9">
                            <select id="risk" name="risk" class="form-control input-sm basic-info" data-placeholder="Click to Choose...">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="risk_desc"> Risk Description </label>
                        <div class="col-xs-9">
                            <span class="label label-info label-xlg arrowed label-desc label-wrap">N/A</span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary modal-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Step modal -->

@endsection

@section('footer_script')
    <script src="/js/iapms.js"></script>
    <script src="/js/jquery.validate.js"></script>
    <script src="/js/date-time/bootstrap-datepicker.js"></script>
    <script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
        var form        = $('#bpsteprisk-form'),
            risk        = $('#risk'),
            $alert      = $('.alert-main'),
            $modal      = $('#bpsteprisk-modal');
        var $select_risk, select_risk;
        var $datatable;

         @if (Session::has('message'))
            $alert.html('{!! session('message') !!}').show();
        @endif

        $(function(){
            $select_risk = risk.selectize({
                valueField: 'risk_id',
                labelField: 'lv_risk_code_meaning',
                searchField: ['lv_risk_code_meaning'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.risk_code) + ' </span>' +
                            '<span class="by">' + escape(item.lv_risk_code_meaning) + ' | ' + escape(item.lv_risk_code_desc) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: "{{ url('risks/list')}}?response=json",
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    $(".label-desc").html(select_risk.options[value].lv_risk_code_desc);
                }
            });
            select_risk = $select_risk[0].selectize;

            $(document).on("mouseover",".selectize-dropdown-content div",function(){
                $(".label-desc").html($(this).find('.desc').html());
            })
        });

        // save the form through AJAX call
        function save() {
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        window.onbeforeunload = null;
                        if( data.redirect !== undefined ) window.location = data.redirect;
                        else window.location = '{{ route('business_process') }}';
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        }

        $datatable = $('#bpsteps-table').DataTable({
            "searching": false,
            "lengthMenu": [ 50, 75, 100 ],
            "processing": true,
            ajax: "{{route('business_process.steps.risks.list', ['bp_step_id' => $details->bp_steps_id])}}",
            "serverSide": true,
            columns: [
                { data: "pivot.bp_step_risk_seq" },
                { data: "lv_risk_meaning" },
                { data: "lv_risk_desc", orderable: false },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn-delete delete btn-remove" data-step="{{$details->bp_steps_id}}" data-risk="'+data.risk_id+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    orderable: false
                }
            ]
        });

        // initialize modal fields
        function init_modal( sequence, risk, id ) {
            $modal.find('input#sequence').val(sequence);
            $modal.find('#risk').val(risk).trigger('change');

            $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
            $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');
        }

        $modal.on('hidden.bs.modal', function (e) {
            init_modal('', '', '');
            $modal.find('.label-desc').html('N/A');
            $modal.find('.modal-alert').html('').hide();
        });

        $modal.on('show.bs.modal', function(e){
            var sequence = $datatable.rows().data().length + 1;
            $modal.find('input[name="sequence"]').val(sequence);
        });

        function updateDatatable() {
            $datatable.ajax.reload( null, false );
            $modal.modal('hide');
        }

        $(document).on('click', '.modal-save', function(){
            $('#bpstepsrisk-modal-form').submit();
        });

        $('#bpstepsrisk-modal-form').on('submit', function(){
            swal({
                title: "Continue saving?",
                text: "You are about to add a new business process area risk.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, save it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: $('#bpstepsrisk-modal-form').attr('action'), // url based on the form action
                    method: "POST",
                    data: $('#bpstepsrisk-modal-form').serialize(), // serializes all the form data
                    success : function(data) {
                        if( data.success ) {
                            swal("Saved!", "New step is successfully created.", "success");
                            updateDatatable();
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal.close();
                        $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');

                        if( xhr.status == 500 ) {
                            $('.modal-alert').removeClass('alert-warning').addClass('alert-danger');
                            $('.modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                        }
                        else {
                            // displays the validation error
                            var unique_message = [];
                            var msg = '<ul class="list-unstyled">';

                            $('.modal-input').parent().parent().removeClass('has-error');

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    // set error class to fields
                                    $('#'+key).parent().parent().addClass('has-error');

                                    // shows error message
                                    if( unique_message.indexOf(val[i]) === -1 ) {
                                        msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                        unique_message.push(val[i]);
                                    }
                                }
                            });
                            msg += '</ul>';

                            $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
                            $('.modal-alert').html(msg).show();
                        }
                    }
                });
            });

            return false;
        });

        // show update form
        $(document).on('click', '.btn-update', function(){
            var activity_narrative = $(this).attr('data-activity_narrative'),
                id = $(this).attr('data-id'),
                seq = $(this).attr('data-seq');

            // set values
            init_modal(seq, activity_narrative, id);

            $modal.modal('show');

            return false;
        });

        // remove existing control
        $(document).on('click', '.btn-remove', function() {
            var remove_step = $(this).attr('data-step');
            var remove_risk = $(this).attr('data-risk');
            swal({
                title: "Are you sure?",
                text: "You are about to remove a business process area risk.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: '{{url('business_process/steps')}}/'+remove_step+'/risks/'+remove_risk+'/delete',
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}' },
                    success : function(data) {
                        if( data.success ) {
                            swal("Deleted!", "Business process area reisk has been deleted.", "success");
                            updateDatatable();
                        }
                    }
                });
            });

            return false;
        });
    </script>
@endsection
