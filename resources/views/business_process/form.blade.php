@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/plugins/selectize/selectize.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.bootstrap3.css" />
    <link rel="stylesheet" href="/css/selectize-custom.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <style type="text/css">
        .row-details {
            padding-bottom: 5px;
        }
        .selected-row {
            background-color: #ffe99e !important;
        }
        .table-hover tbody > tr:hover {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Business Process Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                {{ (isset($details)) ? 'Save' : 'Save &amp; proceed to steps' }}
                                            </button>
                                            <a href="{{route('business_process')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="table-header"> Business Process </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div style="display:none;" class="alert alert-warning alert-main"></div>
                                        {!! Form::open(['url' => isset($details) ? route('business_process.update', ['bp_id' => $details->bp_id]) : route('business_process.store'), 'class' => 'form-horizontal', 'id' => 'bp-form', 'style' => 'margin-top: 12px;']) !!}
                                            @if(isset($details))
                                                <input type="hidden" name="_method" value="PUT">
                                                <input type="hidden" name="id" value="{{ $details->bp_id }}">
                                            @endif
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="bp_name"> IDL </label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="bp_name" id="bp_name" class="form-control input-sm" value="{{ isset($details) ? $details->bp_name : '' }}" {{ isset($details) ? 'disabled' : 'readonly' }}>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="bp_code"> Business Process </label>
                                                <div class="col-sm-6">
                                                    <select id="bp_code" name="bp_code" class="form-control input-sm basic-info lookup" placeholder="Select a process..." {{ isset($details) ? 'disabled' : 'required' }}>
                                                        @if(isset($details))
                                                        <option value="{{ $details->bp_code }}" selected>{{ $details->bp_code }}</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="bp_desc"> Business Process Description </label>
                                                <div class="col-xs-8" id="bp_code_desc">
                                                    <span class="label label-info label-bp_desc">{{ isset($details) ? $details->lv_bp_code_desc : 'N/A' }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="source_type"> Source Type </label>
                                                <div class="col-xs-3">
                                                    <select id="source_type" name="source_type" class="form-control input-sm basic-info lookup" placeholder="Select a source type">
                                                        @if(isset($details))
                                                        <option value="{{ $details->source_type }}" selected>{{ $details->source_type }}</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="source_type_desc"> Source Type Description </label>
                                                <div class="col-xs-8" id="source_type_desc">
                                                    <span class="label label-info label-source_type_desc">{{ isset($details) ? $details->lv_source_type_desc : 'N/A' }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="source_ref"> Source Reference </label>
                                                <div class="col-sm-6">
                                                    <textarea id="source_ref" name="source_ref" class="form-control" rows="2" style="resize: none;">{{ (isset($details)) ? $details->source_ref : '' }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="bp_remarks"> Remarks </label>
                                                <div class="col-sm-6">
                                                    <textarea id="risk_remarks" name="bp_remarks" class="form-control" rows="3" style="resize: none;">{{ (isset($details)) ? $details->bp_remarks : '' }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    @if( isset($details) )
                                                    <button type="button" class="btn btn-primary btn-sm btn-add-info" data-id="{{$details->bp_id}}"><i class="fa fa-list-ul"></i> Additional Info</button>
                                                    @endif
                                                    {{--<button type="button" class="btn btn-success btn-sm"><i class="fa fa-list-ol"></i> Apply To All</button>--}}
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                @if(isset($details))
                                <br/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <button class="btn btn-default btn-sm btn-add-objectives" type="button">
                                            <i class="ace-icon fa fa-plus bigger-120"></i>
                                            Add
                                        </button>
                                        <div class="table-header"> Objectives </div>
                                        <table id="bp-objectives-table" class="table table-striped table-bordered table-hover">
                                            <thead>
                                                <tr>
                                                    <th width="20%">Objective Name</th>
                                                    <th>Objective Narrative</th>
                                                    <th width="20%">Objective Category</th>
                                                    <th width="7%"></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-xs-12" style="padding-top: 10px;">
                                        <button class="btn btn-default btn-sm btn-bpstep-modal" type="button" data-toggle="modal" data-target="#bpstep-modal" style="display: none;">
                                            <i class="ace-icon fa fa-plus bigger-120"></i>
                                            Add
                                        </button>
                                        <div class="table-header"> Business Process Area </div>
                                        <table id="bpsteps-table" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th width="10%">Sequence</th>
                                                    <th width="80%">Activity Narrative</th>
                                                    <th width="10%"></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                @endif
                                <hr/>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <button class="btn btn-primary btn-sm" type="button" onclick="save()">
                                                <i class="ace-icon fa fa-save bigger-120"></i>
                                                {{ (isset($details)) ? 'Save' : 'Save &amp; proceed to steps' }}
                                            </button>
                                            <a href="{{route('business_process')}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-times bigger-120"></i>
                                                Cancel
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@if(isset($details))
<!-- Bp Step modal -->
<div class="modal fade" data-backdrop="static" id="bpstep-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Business Process Area Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning modal-alert"></div>
                {!! Form::open(['method' => 'post', 'class' => 'form-horizontal', 'id' => 'bpsteps-modal-form']) !!}
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="bp_steps_id" value="">
                    <input type="hidden" name="bp_obj_id" value="">
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="sequence"> Sequence </label>
                        <div class="col-sm-2">
                            <input type="number" required class="form-control input-sm" id="sequence" name="sequence" value="" min="1" step="1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="activity_narrative"> Activity Narrative </label>
                        <div class="col-xs-9">
                            <textarea name="activity_narrative" id="activity_narrative" class="form-control" rows="5" style="resize: none;" required="required"></textarea>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-steps-modal-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Bp step modal -->

<!-- Bp objectives modal -->
<div class="modal fade" data-backdrop="static" id="bp-objectives-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Business Process Objective Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning obj-modal-alert"></div>
                {!! Form::open(['method' => 'post', 'url' => route('business_process.objective.store', ['bp_id' => $details->bp_id]), 'class' => 'form-horizontal', 'id' => 'bp-obj-modal-form']) !!}
                    <input type="hidden" name="_method" value="post">
                    <input type="hidden" name="id" value="">
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="objective_name"> Objective Name </label>
                        <div class="col-xs-9">
                            <input type="text" name="objective_name" id="objective_name" class="form-control basic-input" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="objective_narrative"> Objective Narrative </label>
                        <div class="col-xs-9">
                            <textarea name="objective_narrative" id="objective_narrative" class="form-control" rows="5" style="resize: none;" required="required"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="objective_category"> Objective Category </label>
                        <div class="col-xs-9">
                            <input type="hidden" name="objective_category_check" value="">
                            <div class="checkbox">
                            @for($o = 0; $o < count($objective_categories); $o++)
                                <label>
                                    <input type="checkbox" name="objective_category[]" value="{{$objective_categories[$o]}}" class="ace">
                                    <span class="lbl"> {{$objective_categories[$o]}}</span>
                                </label>
                            @endfor
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-bp-objective-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Bp objectives modal -->
@include('includes.modals.additional_information', ['readonly' => true])
@endif
@endsection

@section('footer_script')
    <script src="/js/iapms.js"></script>
    <script src="/js/jquery.validate.js"></script>
    <script src="/plugins/selectize/selectize.js"></script>
    <script src="/js/date-time/bootstrap-datepicker.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
        var form        = $('#bp-form'),
            code        = $('#bp_code'),
            source_type = $('#source_type'),
            $alert      = $('.alert-main'),
            $modal      = $('#bpstep-modal');
        var ai_modal = $('#additional-information-modal');
        var $datatable, $objectives_datatable;
        var obj_url = '';
        var lookup_url = '{{url('/administrator/lookup/type')}}';

        var select_bp, $select_bp;
        var select_source_type, $select_source_type;

         @if (Session::has('message'))
            $alert.html('{!! session('message') !!}').show();
        @endif

        $(function(){
            // selectize
            $select_bp = code.selectize({
                    valueField: 'id',
                labelField: 'meaning',
                searchField: ['meaning', 'description'],
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.id) + '</span>' +
                            '<span class="by">' + escape(item.meaning) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.business_process') }}?search='+encodeURIComponent(query)+'&searchFields=description:like',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    $('#bp_code_desc span').html(select_bp.options[value].description);
                    $('#bp_name').val(select_bp.options[value].meaning);
                }
            });
            select_bp = $select_bp[0].selectize;

            $select_source_type = $('#source_type').selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: ['meaning', 'description'],
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.id) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.source_type') }}?search='+encodeURIComponent(query)+'&searchFields=description:like',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    $('#source_type_desc span').html(select_source_type.options[value].description);
                }
            });
            select_source_type = $select_source_type[0].selectize;
            // end selectize

            @if(isset($details))
            dtClickable($('#bp-objectives-table'), $objectives_datatable, bpStepsPopulate);
            @endif
        });

        $('.select2-drop').on('select2:select', function(evt){
            var data = evt.params.data;
            var target = evt.currentTarget.id;

            $('#'+target+'_desc').find('.label').html(data.meaning);
        });

        // save the form through AJAX call
        function save() {
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        window.onbeforeunload = null;
                        if( data.redirect !== undefined ) window.location = data.redirect;
                        else window.location = '{{ route('business_process') }}';
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        }

        @if(isset($details))
        $datatable = $('#bpsteps-table').DataTable({
            "searching": false,
            "lengthMenu": [ 50, 75, 100 ],
            "processing": true,
            data : [],
            rowId : 'bp_steps_id',
            columns: [
                { data: "bp_steps_seq" },
                { data: "activity_narrative", orderable: false },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn-add-info-steps" data-id="'+data.bp_steps_id+'" title="additional info" rel="tooltip"><i class="fa fa-list-ul edit"></i></a> &nbsp;<a href="#" class="btn-edit-bpstep" data-row="'+meta.row+'" data-id="'+data.bp_steps_id+'" title="Edit" rel="tooltip"><i class="fa fa-pencil edit"></i></a> &nbsp; <a href="{{url('business_process')}}/{{ $details->bp_id }}/steps/'+data.bp_steps_id+'" class="btn-step-risks" title="Risks" rel="tooltip"><i class="fa fa-exclamation"></i></a> &nbsp; <a href="#" class="btn-delete delete btn-remove-step" data-id="'+data.bp_steps_id+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    orderable: false
                }
            ]
        });

        $objectives_datatable = $('#bp-objectives-table').DataTable({
            "searching": false,
            "ordering": false,
            "lengthMenu": [ 50, 75, 100 ],
            "processing": true,
            data: {!! json_encode($details->bpObjectives) !!},
            columns: [
                { data: "objective_name" },
                { data: "objective_narrative" },
                { data: "objective_category" },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" data-id="'+data.bp_objective_id+'" data-row="'+meta.row+'" class="btn-edit-objective" title="Update" rel="tooltip"><i class="fa fa-pencil edit"></i></a> &nbsp; <a href="#" class="btn-delete delete btn-remove-obj" data-id="'+data.bp_objective_id+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    }
                }
            ]
        });

        // initialize modal fields
        function init_modal( sequence, activity_narrative, id ) {
            if( id != '' ) {
                // update setting
                $modal.find('input[name="id"]').val(id);
                $modal.find('form').attr('action', '{{ url('business_process/steps/update') }}/'+id)
                                      .find('input[name="_method"]').val('put');
            }
            else {
                // create setting
                $modal.find('input[name="id"]').val('');
                $modal.find('form').attr('action', '{{route('business_process.steps.store', ['bp_id' => $details->bp_id])}}')
                                      .find('input[name="_method"]').val('post');
            }
            $modal.find('input#sequence').val(sequence);
            $modal.find('#activity_narrative').val(activity_narrative);

            $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
            $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');
        }

        $modal.on('hidden.bs.modal', function (e) {
            $('#bpsteps-modal-form').attr('action', obj_url);
            $('#bpsteps-modal-form').find('[name="_method"]').val('post');
            $('#bpsteps-modal-form').find('[name="bp_steps_id"]').val('');
            $('#bpsteps-modal-form').find('[name="sequence"]').val('');
            $('#bpsteps-modal-form').find('[name="activity_narrative"]').val('');

            $modal.find('.modal-alert').html('').hide();
        });

        $modal.on('show.bs.modal', function (e) {
            $('#bpsteps-modal-form').find('[name="sequence"]').val($datatable.rows().data().length + 1);
        });

        $('#bp-objectives-modal').on('hidden.bs.modal', function (e) {
            $('#bp-obj-modal-form').attr('action', '');
            $('#bp-obj-modal-form').find('[name="_method"]').val('post');
            $('#bp-obj-modal-form').find('[name="objective_narrative"]').val('');
            $('#bp-obj-modal-form').find('[name="objective_name"]').val('');
            $('#bp-obj-modal-form').find('.label-obj_cat_desc').html('');
            $('#bp-obj-modal-form').find('[name="id"]').val('');
            $('#bp-obj-modal-form').find('[name="objective_category_check"]').val('');
            $('#bp-obj-modal-form').find('[name="objective_category[]"]').attr('checked', false);
        });

        $(document).on('click', '[name="objective_category[]"]', function() {
            var atLeastOneIsChecked = $('[name="objective_category[]"]:checked').length > 0;
            if( atLeastOneIsChecked ) {
                $('#bp-obj-modal-form').find('[name="objective_category_check"]').val('1');
            }
            else {
                $('#bp-obj-modal-form').find('[name="objective_category_check"]').val('');
            }
        });

        $(document).on('click', '.btn-add-objectives', function() {
            $('#bp-obj-modal-form').attr('action', '{{route('business_process.objective.store', ['bp_id' => $details->bp_id])}}');
            $('#bp-obj-modal-form').find('[name="_method"]').val('post');

            $('#bp-objectives-modal').modal('show');
        });

        $(document).on('click', '.btn-edit-bpstep', function(){
            var id = $(this).attr('data-id');
            var row = $(this).attr('data-row');
            var data = $datatable.row(row).data();
            var url = '{{ url('business_process/objective') }}/steps/update/'+id;

            $('#bpsteps-modal-form').attr('action', url);
            $('#bpsteps-modal-form').find('[name="_method"]').val('put');
            $('#bpsteps-modal-form').find('[name="bp_steps_id"]').val(id);
            $('#bpsteps-modal-form').find('[name="sequence"]').val(data.bp_steps_seq);
            $('#bpsteps-modal-form').find('[name="activity_narrative"]').val(data.activity_narrative);

            $('#bpstep-modal').modal('show');
            return false;
        });

        $(document).on('click', '.btn-edit-objective', function(){
            var id = $(this).attr('data-id');
            var row = $(this).attr('data-row');
            var data = $objectives_datatable.row(row).data();
            var url = '{{ url('business_process/objective') }}/update/'+id;

            $('#bp-obj-modal-form').attr('action', url);
            $('#bp-obj-modal-form').find('[name="_method"]').val('put');
            $('#bp-obj-modal-form').find('[name="id"]').val(data.bp_objective_id);
            $('#bp-obj-modal-form').find('[name="objective_name"]').val(data.objective_name);
            $('#bp-obj-modal-form').find('[name="objective_narrative"]').val(data.objective_narrative);
            var obj_categories = data.objective_category;
            obj_categories = obj_categories.split(',');
            for(var i = 0; i < obj_categories.length; i++) $('#bp-obj-modal-form').find('[value="'+obj_categories[i]+'"]').prop('checked', true);
            $('#bp-obj-modal-form').find('.label-obj_cat_desc').html(data.lv_objective_category_desc);

            $('#bp-objectives-modal').modal('show');
            return false;
        });

        $(document).on('click', '.btn-steps-modal-save', function(){
            swal({
                title: "Continue saving?",
                text: "You are about to add a new business process area.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, save it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: $('#bpsteps-modal-form').attr('action'), // url based on the form action
                    method: "POST",
                    data: $('#bpsteps-modal-form').serialize(), // serializes all the form data
                    success : function(data) {
                        if( data.success ) {
                            $modal.modal('hide');
                            swal("Saved!", "Step is successfully saved.", "success");
                            $('#bp-objectives-table  tr.selected-row').trigger('click');
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal.close();
                        $modal.find('form > .form-group').removeClass('has-error').removeClass('has-success');

                        if( xhr.status == 500 ) {
                            $('.modal-alert').removeClass('alert-warning').addClass('alert-danger');
                            $('.modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                        }
                        else {
                            // displays the validation error
                            var unique_message = [];
                            var msg = '<ul class="list-unstyled">';

                            $('.modal-input').parent().parent().removeClass('has-error');

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    // set error class to fields
                                    $('#'+key).parent().parent().addClass('has-error');

                                    // shows error message
                                    if( unique_message.indexOf(val[i]) === -1 ) {
                                        msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                        unique_message.push(val[i]);
                                    }
                                }
                            });
                            msg += '</ul>';

                            $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
                            $('.modal-alert').html(msg).show();
                        }
                    }
                });
            });
        });

        // show update form
        $(document).on('click', '.btn-update', function(){
            var activity_narrative = $(this).attr('data-activity_narrative'),
                id = $(this).attr('data-id'),
                seq = $(this).attr('data-seq');

            // set values
            init_modal(seq, activity_narrative, id);

            $modal.modal('show');

            return false;
        });

        // remove existing control
        $(document).on('click', '.btn-remove-step', function() {
            var remove_id = $(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "You are about to remove a business process area.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: '{{url('business_process/steps/delete')}}/'+remove_id,
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}' },
                    success : function(data) {
                        if( data.success ) {
                            swal("Deleted!", "Business process area has been deleted.", "success");
                            location.reload();
                        }
                    }
                });
            });

            return false;
        });

        // remove existing control
        $(document).on('click', '.btn-remove-obj', function() {
            var remove_id = $(this).attr('data-id');
            swal({
                title: "Are you sure?",
                text: "You are about to remove a business process objective.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: '{{url('business_process/objective/delete')}}/'+remove_id,
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}' },
                    success : function(data) {
                        if( data.success ) {
                            swal("Deleted!", "Business process objective has been deleted.", "success");
                            location.reload();
                        }
                    }
                });
            });

            return false;
        });

        $('.btn-bp-objective-save').on('click', function(){
            $.ajax({
                url: $('#bp-obj-modal-form').attr('action'), // url based on the form action
                method: "POST",
                data: $('#bp-obj-modal-form').serialize(), // serializes all the form data
                beforeSend: function() {
                    toggle_loading();
                },
                success : function(data) {
                    toggle_loading();
                    if( data.success ) {
                        swal("Saved!", "New objective is successfully created.", "success");
                        location.reload();
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    toggle_loading();
                    $('#bp-objectives-modal').find('form > .form-group').removeClass('has-error').removeClass('has-success');

                    if( xhr.status == 500 ) {
                        $('.obj-modal-alert').removeClass('alert-warning').addClass('alert-danger');
                        $('.obj-modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.obj-modal-input').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $('.obj-modal-alert').removeClass('alert-danger').addClass('alert-warning');
                        $('.obj-modal-alert').html(msg).show();
                    }
                }
            });
        });

        function dtClickable( table, datatable, callback ) {
            table.find('tbody').on('click', 'tr', function () {
                var data = datatable.row( this ).data();

                $('#bpsteps-modal-form').find('[name="bp_obj_id"]').val(data.bp_objective_id);
                table.find('tbody > tr').removeClass('selected-row'); // reset tr highlights
                $(this).addClass('selected-row');

                if( typeof callback == "function" ) callback(data);
            } );
        }

        function bpStepsPopulate(data) {
            obj_url = '{{ url('business_process/objective') }}/'+data.bp_objective_id+'/steps';
            $.ajax({
                url: obj_url,
                method: "GET",
                data: { _token : '{{ csrf_token() }}' },
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(response) {
                    $('.btn-bpstep-modal').show();
                    $('#bpsteps-modal-form').attr('action', obj_url);
                    $('#bpsteps-modal-form').find('[name="_method"]').val('post');
                    $('#bpsteps-modal-form').find('#sequence').val(response.length + 1);
                    $datatable.clear();
                    $datatable.rows.add(response).draw();

                    toggle_loading();
                }
            });
        }

        $(document).on('click', '.btn-add-info', function() {
            var id = $(this).attr('data-id');
            iapms.getAddInfoForm(id, 'business_processes').success(function(data) {
                if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                    ai_modal.find('.btn-save-ai').hide();
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                    ai_modal.find('.btn-save-ai').show();
                }
                ai_modal.modal('show');
                iapms.setAddInfoForm(ai_modal, '{{url('business_process')}}/'+id+'/additional_information/update', 'put');

                toggle_loading();
            });

            return false;
        });

        $(document).on('click', '.btn-add-info-steps', function() {
            var id = $(this).attr('data-id');
            iapms.getAddInfoForm(id, 'business_processes_steps').success(function(data) {
                if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                    ai_modal.find('.btn-save-ai').hide();
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                    ai_modal.find('.btn-save-ai').show();
                }
                ai_modal.modal('show');
                iapms.setAddInfoForm(ai_modal, '{{url('business_process')}}/step/'+id+'/additional_information/update', 'put');

                toggle_loading();
            });

            return false;
        });

        $(function(){
            ai_modal.find('form').validate();
        });

        ai_modal.find('form').on('submit', function(e){
            e.preventDefault();
            if($(this).valid()) {
                swal({
                    title: "Continue?",
                    text: "You are about to update additional information.",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, save it!",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true
                },
                function(){
                    $.ajax({
                        url: ai_modal.find('form').attr('action'),
                        method: "POST",
                        data: ai_modal.find('form').serialize(), // serializes all the form data
                        success : function(data) {
                            if(data.success) {
                                swal("Success!", "Additional information has been updated.", "success");
                                location.reload();
                            }
                        },
                        error: function(xhr, textStatus, errorThrown){
                            swal.close();
                            // displays the validation error
                            var msg = '<ul class="list-unstyled">';

                            $.each(xhr.responseJSON, function(key, val){
                                for(var i=0; i < val.length; i++) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                }
                            });
                            msg += '</ul>';

                            ai_modal.find('.alert-modal').html(msg).show();
                        }
                    });
                });
            }

            return false;
        });

        ai_modal.on('click', '.btn-save-ai', function() {
            ai_modal.find('form').submit();
        });
        @endif
    </script>
@endsection
