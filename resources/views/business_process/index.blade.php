@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href='{{ route('business_process.create') }}'>
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Business Processes
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="bp-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Business Process IDL</th>
                                <th>Business Process Code</th>
                                <th>Business Process Description</th>
                                <th>Source Type</th>
                                <th>Source Type Description</th>
                                <th width="7%"></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
@include('includes.modals.additional_information')
@endsection

@section('footer_script')
<script src="/js/iapms.js"></script>
<script src="/js/jquery.validate.js"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script src="/js/date-time/bootstrap-datepicker.js"></script>
<script type="text/javascript">
    var ai_modal = $('#additional-information-modal');
    var datatable= $('#bp-table').DataTable( {
        ajax: "{{route('business_process.list')}}",
        processing: true,
        orderCellsTop: true,
        columns: [
            { data: "bp_name"},
            { data: "bp_code" },
            { data: "lv_bp_code_desc"},
            { data: "source_type"},
            { data: "lv_source_type_desc" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="btn-add-info" data-id="'+data.bp_id+'" title="additional info" rel="tooltip"><i class="fa fa-list-ul edit"></i></a> &nbsp; <a href="{{url('business_process')}}/edit/'+data.bp_id+'"><i class="fa fa-pencil edit" title="Edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.bp_id+'" data-name="'+data.bp_name+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    // remove auditee
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var remove_name = $(this).attr('data-name');
        swal({
            title: "Are you sure?",
            text: "You are about to remove "+remove_name+" business process.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        },
        function(){
            $.ajax({
                url: '{{ url('business_process/delete') }}/'+remove_id,
                method: "POST",
                data: { _token : _token, _method : 'delete' }, // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        swal("Deleted!", "Business process has been deleted.", "success");
                        datatable.ajax.reload( null, false );
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    swal.close();
                    // displays the validation error
                    var msg = '<ul class="list-unstyled">';

                    $.each(xhr.responseJSON, function(key, val){
                        for(var i=0; i < val.length; i++) {
                            msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                        }
                    });
                    msg += '</ul>';

                    $('.alert-warning').html(msg).show();
                }
            });
        });

        return false;
    });

    $(document).on('click', '.btn-add-info', function() {
        var id = $(this).attr('data-id');
        iapms.getAddInfoForm(id, 'business_processes').success(function(data) {
            if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                }
            ai_modal.find('#additional-information-form input').attr('disabled', true);
            ai_modal.find('#additional-information-form select').attr('disabled', true);
            ai_modal.find('.btn-save-ai').hide();
            ai_modal.modal('show');
            iapms.setAddInfoForm(ai_modal, '{{url('business_process')}}/'+id+'/additional_information/update', 'put');

            toggle_loading();
        });

        return false;
    });
</script>
@endsection