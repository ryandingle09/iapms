@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/plugins/selectize/selectize.css" />
    <link rel="stylesheet" href="/plugins/selectize/selectize.bootstrap3.css" />
    <link rel="stylesheet" href="/css/selectize-custom.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <link rel="stylesheet" href="/plugins/fancytree/skin-xp/ui.fancytree.min.css" />
    <style type="text/css">
        .row-details {
            padding-bottom: 5px;
        }
        .selected-row {
            background-color: #ffe99e !important;
        }
        .pre-selected-row {
            background-color: #aedcff !important;
        }
        .table-hover tbody > tr:hover {
            cursor: pointer;
        }
        .checkbox label {
            padding-left: 10px !important;
        }
    </style>

@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Controls Registry Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <hr/>

                                        <div class="table-header">
                                            Control
                                        </div>
                                        <div style="display:none;" class="alert alert-warning alert-main"></div>
                                        {!! Form::open(['url' => isset($details) ? route('controls_registry.update', ['id' => $details->control_id]) : route('controls_registry.store'), 'class' => 'form-horizontal', 'id' => 'controls-form']) !!}
                                            @if(isset($details))
                                            <input type="hidden" name="id" value="{{$details->control_id}}">
                                            <input type="hidden" name="_method" value="put">
                                            @endif
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <div class="pull-right btn-group">
                                                        <a href="{{route('controls_registry')}}" class="btn btn-warning btn-sm">
                                                            <i class="ace-icon fa fa-times"></i>
                                                            Cancel
                                                        </a>
                                                        <button type="button" class="btn btn-primary btn-controls-save btn-sm"><i class="ace-icon fa fa-save"></i> Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label" for="control_code"> Control Name </label>
                                                <div class="col-xs-8">
                                                    <input type="hidden" name="control_name" value="{{ isset($details) ? $details->control_name : '' }}">
                                                    <select id="control_code" name="control_code" class="form-control input-sm basic-info lookup" placeholder="Select a control" {{isset($details) ? '' : 'required' }}></select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label" for="control_code"> Control Code Description </label>
                                                <div class="col-xs-8">
                                                    <span class="label label-primary label-wrap" id="control-code-desc"></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label" for="assertions"> Assertions </label>
                                                <div class="col-xs-10">
                                                    <div style="max-height: 200px;overflow-y: auto;">
                                                        <input type="hidden" name="_assertions" value="{{ isset($details) ? '1' : '0' }}">
                                                        @if(!is_null($assertions))
                                                            @if($assertions->lookupValue->count())
                                                                <?php
                                                                    $_assertions = isset($details) ? $details->assertions : [];
                                                                ?>
                                                                @foreach($assertions->lookupValue as $assertion)
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" name="assertions[]" class="ace assertions" value="{{$assertion->lookup_code}}" {{ in_array($assertion->lookup_code, $_assertions) ? 'checked' : '' }}>
                                                                        <span class="lbl">&nbsp;{{$assertion->lookup_code}}</span>
                                                                    </label>
                                                                </div>
                                                                @endforeach
                                                            @else
                                                            <span class="label label-warning label-sm">No control types available.</span>
                                                            @endif
                                                        @else
                                                        <span class="label label-warning label-sm">No control types available.</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label" for="control_types"> Control Types </label>
                                                <div class="col-xs-10">
                                                    <div style="max-height: 200px;overflow-y: auto;">
                                                        <input type="hidden" name="_types" value="{{ isset($details) ? '1' : '0' }}">
                                                        @if(!is_null($types))
                                                            @if($types->lookupValue->count())
                                                                @foreach($types->lookupValue as $type)
                                                                <?php
                                                                    $_types = isset($details) ? $details->control_types : [];
                                                                ?>
                                                                <div class="checkbox">
                                                                    <label>
                                                                        <input type="checkbox" name="control_types[]" class="ace types" value="{{$type->lookup_code}}" {{ in_array($type->lookup_code, $_types) ? 'checked' : '' }}>
                                                                        <span class="lbl">&nbsp;{{$type->lookup_code}}</span>
                                                                    </label>
                                                                </div>
                                                                @endforeach
                                                            @else
                                                            <span class="label label-warning label-sm">No control types available.</span>
                                                            @endif
                                                        @else
                                                        <span class="label label-warning label-sm">No control types available.</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label" for="key_control_flag"> Key Control Flag </label>
                                                <div class="col-xs-3">
                                                    <select id="key_control_flag" name="key_control_flag" class="form-control input-sm basic-info">
                                                        <option value="Y" {{ isset($details) && $details->key_control_flag == 'Y' ? 'selected'  : '' }}>Yes</option>
                                                        <option value="N" {{ isset($details) && $details->key_control_flag == 'N' ? 'selected'  : '' }}>No</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label" for="frequency"> Frequency </label>
                                                <div class="col-xs-6">
                                                    <select id="frequency" name="frequency" class="form-control input-sm basic-info" placeholder="Select a frequency"></select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label" for="frequency_desc"> Frequency Description </label>
                                                <div class="col-xs-8">
                                                    <span class="label label-primary label-frequency_desc label-wrap">{{ isset($details) ? $details->lv_frequency_desc : '' }}</span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label" for="mode"> Mode </label>
                                                <div class="col-xs-6">
                                                    <select id="mode" name="mode" class="form-control input-sm basic-info" placeholder="Select a mode..."></select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-2 control-label" for="control_source"> Control Source </label>
                                                <div class="col-xs-6">
                                                    <textarea name="control_source" id="control_source" class="form-control basic-info" rows="5" required="required" style="resize: none;">{{ isset($details) ? $details->control_source : '' }}</textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-xs-12">
                                                    <div class="pull-right btn-group">
                                                        <a href="{{route('controls_registry')}}" class="btn btn-warning btn-sm">
                                                            <i class="ace-icon fa fa-times"></i>
                                                            Cancel
                                                        </a>
                                                        <button type="button" class="btn btn-primary btn-controls-save btn-sm"><i class="ace-icon fa fa-save"></i> Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        {!! Form::close() !!}
                                    </div>
                                @if(isset($details))
                                    <!-- coso components -->
                                    <div class="col-xs-12">
                                        <div class="table-header">
                                            COSO Components
                                        </div>
                                        <!-- div.dataTables_borderWrap -->
                                        <div>
                                            @include('controls.coso_tree')
                                            <div class="pull-right">
                                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#coso-modal">Update COSO</button>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /coso components -->
                                    <div class="col-xs-12">
                                        <button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#test-proc-modal">
                                            <i class="ace-icon fa fa-plus"></i>
                                            Create
                                        </button>
                                        <div class="table-header">
                                            Controls Test Procedures
                                        </div>
                                        <!-- div.dataTables_borderWrap -->
                                        <div>
                                            <table id="control-table" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th width="5%">Sequence</th>
                                                        <th width="15%">Test Procedure Name</th>
                                                        <th width="35%">Test Procedure Narrative</th>
                                                        <th width="35%">Audit Objective</th>
                                                        <th width="5%">Budgeted Mandays</th>
                                                        <th width="5%"></th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-xs-12" style="padding-top: 10px;">
                                        <button class="btn btn-default btn-sm btn-audit-create" type="button" data-toggle="modal" data-target="#testproc-audit-modal" style="display: none;">
                                            <i class="ace-icon fa fa-plus"></i>
                                            Create
                                        </button>
                                        <div class="table-header">
                                            Controls Test Procedures - Audit Type
                                        </div>
                                        <!-- div.dataTables_borderWrap -->
                                        <div>
                                            <table id="control-audit-type-table" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Audit Type</th>
                                                        <th>Description</th>
                                                        <th width="10%"></th>
                                                    </tr>
                                                </thead>

                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->

<!-- Test process modal -->
<div class="modal fade" data-backdrop="static" id="test-proc-modal">
    <div class="modal-dialog">
        <div class="modal-content" style="width:800px;margin-left: -50px !important;" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Test Procedure Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning modal-test-alert"></div>
                {!! Form::open(['method' => 'post', 'url' => route('controls_registry.test_procedures.store'), 'class' => 'form-horizontal', 'id' => 'testprocedure-form']) !!}
                    <input type="hidden" name="origsq" value="">
                    <input type="hidden" name="control_id" value="{{ isset($details) ? $details->control_id : ''}}">
                    <input type="hidden" name="control_test_id" value="">
                    <input type="hidden" name="_method" value="post">
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="sequence"> Sequence </label>
                        <div class="col-sm-2">
                            <input type="number" required class="form-control input-sm" id="sequence" name="sequence" value="" min="1" step="1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="test_proc"> Test Procedure </label>

                        <div class="col-xs-9">
                            <select id="test_proc" name="test_proc" class="form-control input-sm basic-info lookup" placeholder="Select a test procedure" required></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="test_proc_narrative" style="padding-top: 0;"> Test Procedure Narrative </label>
                        <div class="col-sm-9 label-overflow" id="test_proc_narrative">
                            <span class="label-wrap"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="audit_objective" style="padding-top: 0;"> Audit Objective </label>
                        <div class="col-sm-9" id="audit_objective">
                            <span class="label label-info label-wrap"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="budgeted_mandays" style="padding-top: 0;"> Budgeted Mandays </label>
                        <div class="col-sm-2" id="budgeted_mandays">
                            <span class="label label-info"></span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-controlsmodal-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Test process modal -->

<!-- test audit type modal -->
<div class="modal fade" data-backdrop="static" id="testproc-audit-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Test Process Audit Type</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning modal-alert"></div>
                {!! Form::open(['method' => 'post', 'class' => 'form-horizontal', 'id' => 'testproc-audit-form']) !!}
                    <div class="form-group">
                        <label class="col-sm-2 control-label modal-input" for="audit_type"> Audit type </label>
                        <div class="col-sm-8">
                            <select id="audit_type" name="audit_type" class="form-control input-sm basic-info" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-tp-au-modal-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /test audit type modal -->

<!-- coso modal -->
<div class="modal fade" data-backdrop="static" id="coso-modal">
    <div class="modal-dialog" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">COSO Components</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning coso-modal-alert"></div>
                {!! Form::open(['method' => 'post', 'url' => route('controls_registry.coso.update'), 'class' => 'form-horizontal', 'id' => 'coso-form']) !!}
                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" name="control_id" value="{{ isset($details) ? $details->control_id : '' }}">
                    <div class="form-group">
                        <div class="col-xs-4">
                            <table id="component-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Code</th>
                                    <th>Component</th>
                                </tr>
                                </thead>

                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-4">
                            <table id="principle-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Code</th>
                                    <th>Principle</th>
                                </tr>
                                </thead>

                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-4">
                            <table id="focus-table" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Code</th>
                                    <th>Focus</th>
                                </tr>
                                </thead>

                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-coso-modal-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /coso modal -->
@include('includes.modals.additional_information')

@endsection

@section('footer_script')
    <script src="/js/iapms.js"></script>
    <script src="/js/jquery.validate.js"></script>
    <script src="/plugins/selectize/selectize.js"></script>
    <script src="/js/date-time/bootstrap-datepicker.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
        var form         = $('#controls-form'),
            control_code = $('#control_code'),
            $alert       = $('.alert-main'),
            $modal       = $('#test-proc-modal'),
            mode         = $('#mode'),
            coso         = $('#coso-table');
        var ai_modal = $('#additional-information-modal');
        var $datatable,
            $component_datatable,
            $principle_datatable,
            $focus_datatable,
            $audit_type_datatable;
        var select_control, $select_control;
        var select_frequency, $select_frequency;
        var select_mode, $select_mode;
        var select_test_proc, $select_test_proc;
        var select_audit_type, $select_audit_type;
        var lookup_url = '{{url('/administrator/lookup/type')}}';
        var components = {!! isset($details) ? ($details->component_code != '' ? $details->component_code : '[]') : '[]' !!},
            sel_pr = {!! isset($details) ? ($details->getOriginal('principle_code') != '' ? $details->getOriginal('principle_code') : '{}') : '{}' !!},
            sel_fo = {!! isset($details) ? ($details->getOriginal('focus_code') != '' ? $details->getOriginal('focus_code') : '{}') : '{}' !!};
        var last_cp_selected = '',
            last_pr_selected = '';
        var coso_data = {!! isset($coso_components) && !is_null($coso_components['data']) ? json_encode($coso_components['data']) : json_encode([]) !!};

        function coso_components($table, dt, next_dt, callback, other_dt) {
            $table.find('tbody').on('click', 'tr:not(".pre-selected-row")', function () {
                var data = dt.row( this ).data();

                $table.find('tbody > tr').removeClass('pre-selected-row'); // reset tr highlights
                $(this).addClass('pre-selected-row');

                if( typeof next_dt !== "undefined") {
//                    get_coso_data(data.lookup_code, next_dt, other_dt);
                    next_dt.clear();
                    next_dt.rows.add(data.child.data).draw();
                    if( typeof other_dt != "undefined" ) other_dt.clear().draw();

                    var to_restore = typeof sel_pr[data.id] !== "undefined" ? sel_pr[data.id] : sel_fo[data.id];
                    if( typeof to_restore === "object" ) restoreCheck(to_restore);
                }

                if( typeof callback === "function" ) callback(data.id);
            } );
        }

        function get_coso_data(code, next_dt, other_dt) {
            $.ajax({
                url: "{{url('administrator/lookup')}}/"+code+"/value/list",
                type: 'GET',
                beforeSend: function() {
                    swal({
                        title: "Loading",
                        text: "Please wait...",
                        type: "info",
                        showConfirmButton: false
                    });
                },
                success: function(res) {
                    swal.close();
                    next_dt.clear();
                    next_dt.rows.add(res.data).draw();
                    if( typeof other_dt != "undefined" ) other_dt.clear().draw();

                    var to_restore = typeof sel_pr[code] !== "undefined" ? sel_pr[code] : sel_fo[code];
                    if( typeof to_restore === "object" ) restoreCheck(to_restore);
                }
            });
        }

        function restoreCheck(data) {
            console.log(data);
            if( data.length ) {
                for (var i = 0; i < data.length; i++) {
                    $(":checkbox[value='"+data[i]+"']")
                    .prop("checked","true")
                    .parent()
                    .parent()
                    .addClass('selected-row');
                }
            }
        }

        function set_last_component(cp) {
            last_cp_selected = cp;
        }

        function set_last_principle(pr) {
            last_pr_selected = pr;
        }

        $(function() {
            // selectize
            $select_control = control_code.selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: 'description',
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.text) + ' </span>' +
                            '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.control.controls') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    $('#control-code-desc').html(select_control.options[value].description);
                    $('input[name="control_name"]').val(select_control.options[value].meaning);
                }
                @if(isset($details))
                ,onLoad: function() {
                    select_control.setValue('{{ $details->control_code }}');
                    select_control.disable();
                }
                @endif
            });
            select_control = $select_control[0].selectize;

            $select_frequency = $('#frequency').selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: 'description',
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.text) + ' - </span>' +
                            '<span class="by">' + escape(item.description) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.control.frequency') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    $('.label-frequency_desc').html(select_frequency.options[value].description);
                }
                @if(isset($details))
                ,onLoad: function(data) {
                    select_frequency.setValue('{{$details->frequency}}');
                }
                @endif
            });
            select_frequency = $select_frequency[0].selectize;

            $select_mode = mode.selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: 'description',
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.text) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.control.application') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                }
                @if(isset($details))
                ,onLoad: function(data) {
                    select_mode.setValue('{{$details->mode}}');
                }
                @endif
            });
            select_mode = $select_mode[0].selectize;

            $select_test_proc = $('#test_proc').selectize({
                valueField: 'test_proc_id',
                labelField: 'test_proc_name',
                searchField: ['test_proc_name'],
                options: [],
                preload: true,
                create: false,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.test_proc_name) + ' </span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: '{{ route('test_procedures.lists') }}?format=json',
                        type: 'GET',
                        success: function(res) {
                            callback(res);
                        },
                        error: function() {
                            callback();
                        }
                    });
                },
                onChange: function(value) {
                    if (!value.length) return;
                    $('#test_proc_narrative span').html(select_test_proc.options[value].test_proc_narrative);
                    $('#audit_objective span').html(select_test_proc.options[value].audit_objective);
                    $('#budgeted_mandays span').html(select_test_proc.options[value].budgeted_mandays);
                }
            });
            select_test_proc = $select_test_proc[0].selectize;

            $select_audit_type = $('#audit_type').selectize({
                valueField: 'id',
                labelField: 'meaning',
                searchField: 'description',
                options: [],
                create: false,
                preload: true,
                render: {
                    option: function(item, escape) {
                        return '<div>' +
                            '<span class="title">' +
                            '<span class="name">' + escape(item.text) + ' </span>' +
                            '<span class="by">' + escape(item.meaning) + ' | ' + escape(item.description) + '</span>' +
                            '</span>' +
                            '</div>';
                    }
                },
                load: function(query, callback) {
                    $.ajax({
                        url: lookup_url+'/{{ config('iapms.lookups.audit_type') }}',
                        type: 'GET',
                        success: function(res) {
                            callback(res.data);
                        },
                        error: function() {
                            callback();
                        }
                    });
                }
            });
            select_audit_type = $select_audit_type[0].selectize;
            // end selectize

            ai_modal.find('form').validate();
            $('.dataTables_paginate').parent().removeClass('col-sm-7').addClass('col-sm-12');
            $('#component-table_wrapper, #principle-table_wrapper, #focus-table_wrapper').find('.row:eq(0)').remove();

            coso_components($('#component-table'), $component_datatable, $principle_datatable, set_last_component, $focus_datatable);
            coso_components($('#principle-table'), $principle_datatable, $focus_datatable, set_last_principle);

            dtClickable($('#control-table'), $datatable);
            @if(isset($details))
            generateCosoTree(components, sel_pr, sel_fo);
            @endif
        });

        $('.btn-controls-save').on('click', function() {
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        @if(!isset($details))
                            window.location = '{{url('controls_registry/edit')}}/'+data.id;
                        @else
                            window.location = '{{route('controls_registry')}}';
                        @endif
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                if(key != 'control_name') $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        });

        $('.assertions').on('change', function(){
            if( $('.assertions:checked').length ) {
                $('input[name="_assertions"]').val(1);
            }
            else {
                $('input[name="_assertions"]').val(0);
            }
        });

        $('.types').on('change', function(){
            if( $('.types:checked').length ) {
                $('input[name="_types"]').val(1);
            }
            else {
                $('input[name="_types"]').val(0);
            }
        });

        function dtClickable( table, datatable, id, next, data_id ) {
            table.find('tbody').on('click', 'tr', function () {
                var data = datatable.row( this ).data();

                table.find('tbody > tr').removeClass('selected-row'); // reset tr highlights
                $(this).addClass('selected-row');
                $audit_type_datatable.clear();
                $audit_type_datatable.rows.add(data.audit_types).draw();

                $('#testproc-audit-form').prop('action', '{{url('controls_registry/test_procedures')}}/'+data.control_test_id+'/audit_types');
                $('.btn-audit-create').show();
            } );
        }

        function clearDetails() {
            $('.table-details td').html('');
        }
        @if(isset($details))
        $datatable = $('#control-table').DataTable( {
            ajax: "{{route('controls_registry.test_procedures', ['control_id' => $details->control_id])}}",
            "lengthMenu": [ 10, 25, 50, 100 ],
            "processing": true,
            "serverSide": true,
            columns: [
                { data: "control_test_seq", searchable : false },
                { data: "test_proc_name" },
                { data: "test_proc_narrative", sortable: false, searchable : false },
                { data: "audit_objective", sortable: false, searchable : false },
                { data: "budgeted_mandays", searchable : false },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn-delete delete btn-remove-test" data-id="'+data.control_test_id+'" data-seq="'+data.control_test_seq+'" title="Delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        } );
        @endif

        $audit_type_datatable = $('#control-audit-type-table').DataTable( {
            data : [],
            "lengthMenu": [ 25, 50, 75, 100 ],
            "processing": true,
            columns: [
                { data: "audit_type" },
                { data: "description", defaultContent : 'N/A', searchable : false },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn-audittype-delete delete" data-test_id="'+data.control_test_id+'" data-audit_type="'+data.audit_type+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        } );

        $(document).on('click', '.btn-controlsmodal-save', function(){
            $.ajax({
                url: $('#testprocedure-form').attr('action'), // url based on the form action
                method: "POST",
                data: $('#testprocedure-form').serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        toggle_loading();
                        init_modal('', '', '', '');
                        swal("Success!", "Your changes has been saved!", "success");
                        updateDatatable($datatable);
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        $('.modal-test-alert').removeClass('alert-warning').addClass('alert-danger');
                        $('.modal-test-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $('.modal-test-alert').removeClass('alert-danger').addClass('alert-warning');
                        $('.modal-test-alert').html(msg).show();
                    }

                    toggle_loading();
                }
            });
        });

        $(document).on('click', '.btn-tp-au-modal-save', function(){
            $.ajax({
                url: $('#testproc-audit-form').attr('action'), // url based on the form action
                method: "POST",
                data: $('#testproc-audit-form').serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    toggle_loading();
                    init_modal('', '', '');
                    swal("Success!", "Your changes has been saved!", "success");
                    location.reload();
                },
                error: function(xhr, textStatus, errorThrown){
                    $('#testprocedure-form .form-group').removeClass('has-error');
                    if( xhr.status == 500 ) {
                        $('.modal-alert').removeClass('alert-warning').addClass('alert-danger');
                        $('.modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.modal-input').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
                        $('.modal-alert').html(msg).show();
                    }

                    toggle_loading();
                }
            });

            return false;
        });

        function updateDatatable(datatable) {
            datatable.ajax.reload( null, false );
            $modal.modal('hide');
        }

        $modal.on('hidden.bs.modal', function (e) {
            init_modal('', '', '');
            $modal.find('#test_proc_narrative span').html('');
            $modal.find('#audit_objective span').html('');
            $modal.find('#budgeted_mandays span').html('');
        });

        $modal.on('show.bs.modal', function (e) {
            if( $modal.find('#sequence').val() === '' ) $modal.find('#sequence').val($datatable.rows().data().length + 1);
        });

        $('#testproc-audit-modal').on('hidden.bs.modal', function (e) {
            $(this).find('#audit_type').val('').trigger('change');
            $('.modal-alert').hide();
            $('#testproc-audit-form .form-group').removeClass('has-error').removeClass('has-success');
        });

        @if( isset($details) )
        $('#coso-modal').on('show.bs.modal', function (e) {
            restoreCheck(components);
        });
        @endif

        // initialize modal fields
        function init_modal( id, sequence, test_proc ) {
            $('#testprocedure-form').attr('action', '{{route('controls_registry.test_procedures.store')}}')
                                  .find('input[name="_method"]').val('post');
            $modal.find('#sequence').val(sequence);
            $modal.find('#test_proc').val(test_proc);
            $modal.find('input[name="origsq"]').val(sequence);
            $modal.find('input[name="control_test_id"]').val('');

            $('.modal-test-alert').hide();
            $('#testprocedure-form .form-group').removeClass('has-error').removeClass('has-success');
        }

        // remove existing test
        $(document).on('click', '.btn-remove-test', function() {
            var remove_id = $(this).attr('data-id');
            var remove_sequence = $(this).attr('data-seq');

            swal({
                title: "Are you sure?",
                text: 'Remove sequence #'+remove_sequence+'?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function(){
                $.ajax({
                    url: '{{url('controls_registry/test_procedures/delete')}}/'+remove_id,
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}' },
                    beforeSend : function() {
                        toggle_loading();
                    },
                    success : function(data) {
                        swal("Deleted!", 'Sequence #'+remove_sequence+" has been deleted.", "success");
                        updateDatatable($datatable);
                    }
                });
                toggle_loading();
            });

            return false;
        });

        // remove existing audit type
        $(document).on('click', '.btn-audittype-delete', function() {
            var remove_id = $(this).attr('data-test_id');
            var remove_audit_type = $(this).attr('data-audit_type');

            swal({
                title: "Are you sure?",
                text: 'Remove audit type '+remove_audit_type+'?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function(){
                $.ajax({
                    url: '{{url('controls_registry/test_procedures')}}/'+remove_id+'/delete/audit_type',
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}', type : remove_audit_type },
                    beforeSend : function() {
                        toggle_loading();
                    },
                    success : function(data) {
                        swal("Deleted!", 'Audit type '+remove_audit_type+" has been deleted.", "success");
                        location.reload();
                    }
                });
                toggle_loading();
            });

            return false;
        });

        // old
        // save the form through AJAX call
        function save() {
            $.ajax({
                url: form.attr('action'), // url based on the form action
                method: "POST",
                data: form.serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    if( data.success ) {
                        window.onbeforeunload = null;
                        window.location = '{{ route('auditee.index') }}';
                    }
                },
                error: function(xhr, textStatus, errorThrown){
                    if( xhr.status == 500 ) {
                        $alert.removeClass('alert-warning').addClass('alert-danger');
                        $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.basic-info').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $alert.removeClass('alert-danger').addClass('alert-warning');
                        $alert.html(msg).show();
                    }

                    toggle_loading();
                }
            });
        }

        $(document).on('click', '.btn-add-info', function() {
        var id = $(this).attr('data-id');
        iapms.getAddInfoForm(id, 'controls_master').success(function(data) {
            if( !data.success ) {
                    ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                    ai_modal.find('.btn-save-ai').hide();
                }
                else {
                    ai_modal.find('#ai_wrapper').html(data.data);
                    ai_modal.find('.btn-save-ai').show();
                }
            ai_modal.modal('show');
            iapms.setAddInfoForm(ai_modal, '{{url('controls_registry')}}/'+id+'/additional_information/update', 'put');

            toggle_loading();
        });

        return false;
    });

    ai_modal.find('form').on('submit', function(e){
        e.preventDefault();
        if($(this).valid()) {
            swal({
                title: "Continue?",
                text: "You are about to update additional information.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, save it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: ai_modal.find('form').attr('action'),
                    method: "POST",
                    data: ai_modal.find('form').serialize(), // serializes all the form data
                    success : function(data) {
                        if(data.success) {
                            swal("Success!", "Additional information has been updated.", "success");
                            location.reload();
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal.close();
                        // displays the validation error
                        var msg = '<ul class="list-unstyled">';

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                            }
                        });
                        msg += '</ul>';

                        ai_modal.find('.alert-modal').html(msg).show();
                    }
                });
            });
        }

        return false;
    });

    ai_modal.on('click', '.btn-save-ai', function() {
        ai_modal.find('form').submit();
    });

    $component_datatable = $('#component-table').DataTable( {
        data: coso_data,
        "lengthChange": false,
        "paginate": false,
        info : false,
        searching: false,
        rowId: 'lookup_code',
        columns: [
            {
                data: null,
                className: 'dt-center',
                "render": function ( data, type, full, meta ) {
                    return '<input type="checkbox" name="components[]" value="'+data.id+'" data-meaning="'+data.meaning+'" class="coso-components-chk">';
                },
                sortable: false, searchable : false
            },
            { data: "id", sortable: false, searchable : false },
            { data: "meaning" }
        ]
    } );

    $principle_datatable = $('#principle-table').DataTable( {
        data: [],
        "paginate": false,
        "lengthChange": false,
        info : false,
        searching: false,
        columns: [
            {
                data: null,
                className: 'dt-center',
                "render": function ( data, type, full, meta ) {
                    return '<input type="checkbox" name="principles[]" value="'+data.id+'" class="coso-components-chk">';
                },
                sortable: false, searchable : false
            },
            { data: "id", sortable: false, searchable : false },
            { data: "meaning" }
        ]
    } );

    $focus_datatable = $('#focus-table').DataTable( {
        data: [],
        "paginate": false,
        "lengthChange": false,
        info : false,
        searching: false,
        columns: [
            {
                data: null,
                className: 'dt-center',
                "render": function ( data, type, full, meta ) {
                    return '<input type="checkbox" name="focus[]" value="'+data.id+'" class="coso-components-chk">';
                },
                sortable: false, searchable : false
            },
            { data: "id", sortable: false, searchable : false },
            { data: "meaning" }
        ]
    } );

    $(document).on('click', '.coso-components-chk', function () {
        var value = $(this).val();
        var name = $(this).attr('name');
        name = name.replace('[]', '');

        if( !$(this).is(':checked') ) {
            var pr_cbox = $(":checkbox[name='principles[]']:checked");
            var fo_cbox = $(":checkbox[name='focus[]']:checked");
            if( name == 'components' ) {
                components.splice( components.indexOf(value), 1 );
                pr_cbox.prop("checked", false).parent().parent().removeClass('selected-row');
                fo_cbox.prop("checked", false).parent().parent().removeClass('selected-row');
                for (var d=0; d < sel_pr[value].length; d++) delete sel_fo[sel_pr[value][d]]; // remove all principles child
                delete sel_pr[value]; // remove all principles
            }
            else if( name == 'principles' ) {
                sel_pr[last_cp_selected].splice( sel_pr[last_cp_selected].indexOf(value), 1 );
                delete sel_fo[last_pr_selected]; // remove all principles child
                fo_cbox.prop("checked", false).parent().parent().removeClass('selected-row');
                focus = [];
            }
            else {
                sel_fo[last_pr_selected].splice( sel_fo[last_pr_selected].indexOf(value), 1 );
            }
            $(this).parent().parent().removeClass('selected-row');
        }
        else {
            // store and check component if not checked
            if( name !== 'components' && components.indexOf(last_cp_selected) === -1 ) {
                components.push(last_cp_selected);
                $(":checkbox[value='"+last_cp_selected+"']").prop("checked","true").parent().parent().addClass('selected-row');
            }

            if( !sel_pr.hasOwnProperty(last_cp_selected) ) sel_pr[last_cp_selected] = []; //initiate component principle object
            if( name == 'principles' ) sel_pr[last_cp_selected].push(value); // store principles

            // check the parent principle if not check
            if( name == 'focus' ) {
                if( sel_pr[last_cp_selected].indexOf(last_pr_selected) === -1 ) sel_pr[last_cp_selected].push(last_pr_selected); // store principle if not checked

                if( !sel_fo.hasOwnProperty(last_pr_selected) ) sel_fo[last_pr_selected] = []; //initiate principle focus object
                if( sel_fo[last_pr_selected].indexOf(last_pr_selected) === -1 ) {
                    sel_fo[last_pr_selected].push(value);
                }
                else {
                    sel_fo[last_pr_selected].push(value); // store focus
                }
                $(":checkbox[value='"+last_pr_selected+"']").prop("checked","true").parent().parent().addClass('selected-row');
            }
            $(this).parent().parent().addClass('selected-row');
        }
    });

    $(document).on('click', '.btn-coso-modal-save', function () {
        swal({
            title: "Continue?",
            text: 'You are about to update COSO components',
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, save it!",
            closeOnConfirm: false
        }, function(){
            var pr_data = encodeURIComponent(JSON.stringify(sel_pr));
            var fo_data = encodeURIComponent(JSON.stringify(sel_fo));
            $.ajax({
                url: $('#coso-form').attr('action'),
                method: "POST",
                data: $('#coso-form').serialize()+'&sel_pr='+pr_data+'&sel_fo='+fo_data,
                dataType: 'JSON',
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    swal("Saved!", "COSO components is successfully update.", "success");
                    location.reload();
                }
            });
            toggle_loading();
        });
    });
    </script>
    @stack('coso_script')
@endsection