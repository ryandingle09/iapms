@extends('layouts.app')

@section('styles')
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <style type="text/css">
        .row-details {
            padding-bottom: 5px;
        }
        .selected-row {
            background-color: #ffe99e !important;
        }
        .table-hover tbody > tr:hover {
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="widget-box">
                        <div class="widget-header">
                            <h4 class="smaller">
                                Controls Registry Form
                            </h4>
                        </div>

                        <div class="widget-body">
                            <div class="widget-main">
                                <div style="display:none;" class="alert alert-warning alert-main"></div>
                                {!! Form::open(['url' => isset($details) ? route('controls_registry.update', ['id' => $details->control_id]) : route('controls_registry.store'), 'class' => 'form-horizontal', 'id' => 'controls-form']) !!}
                                    <div class="form-group">
                                        <div class="pull-right btn-group">
                                            <a href="{{route('controls_registry.edit', ['id' => $details->control_id])}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-chevron-left"></i>
                                                Back
                                            </a>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <div class="table-header">
                                                Controls
                                            </div>
                                            <table class="table table-striped table-bordered table-details">
                                                <tbody>
                                                    <tr>
                                                        <th width="15%">Sequence</th>
                                                        <td>#{{ $details->control_seq }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Control Code</th>
                                                        <td>{{ $details->control_code }}</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Description</th>
                                                        <td>{{ $details->lv_control_code_desc }}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <button class="btn btn-default btn-sm" type="button" data-toggle="modal" data-target="#controls-modal">
                                                <i class="ace-icon fa fa-plus"></i>
                                                Create
                                            </button>
                                            <div class="table-header">
                                                Controls Test Procedures
                                            </div>
                                            <!-- div.dataTables_borderWrap -->
                                            <div>
                                                <table id="control-table" class="table table-striped table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>Sequence</th>
                                                            <th>Test Procedure Narrative</th>
                                                            <th>Budgeted Mandays</th>
                                                            <th width="10%"></th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-xs-12">
                                            <button class="btn btn-default btn-sm btn-audit-create" type="button" data-toggle="modal" data-target="#testproc-audit-modal" style="display: none;">
                                                <i class="ace-icon fa fa-plus"></i>
                                                Create
                                            </button>
                                            <div class="table-header">
                                                Controls Test Procedures - Audit Type
                                            </div>
                                            <!-- div.dataTables_borderWrap -->
                                            <div>
                                                <table id="control-audit-type-table" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Audit Type</th>
                                                            <th>Description</th>
                                                            <th width="10%"></th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <hr/>
                                    <div class="form-group">
                                        <div class="pull-right btn-group">
                                            <a href="{{route('controls_registry.edit', ['id' => $details->control_id])}}" class="btn btn-warning btn-sm">
                                                <i class="ace-icon fa fa-chevron-left"></i>
                                                Back
                                            </a>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<!-- Test process modal -->
<div class="modal fade" data-backdrop="static" id="controls-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Controls Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning modal-alert"></div>
                {!! Form::open(['method' => 'post', 'url' => route('controls_registry.details.test_procedures.store'), 'class' => 'form-horizontal', 'id' => 'testprocedure-form']) !!}
                    <input type="hidden" name="control_det_id" value="{{ $details->control_det_id }}">
                    <input type="hidden" name="origsq" value="">
                    <input type="hidden" name="_method" value="post">
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="sequence"> Sequence </label>
                        <div class="col-sm-3">
                            <input type="number" required class="form-control input-sm" id="sequence" name="sequence" value="" min="1" step="1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="test_proc"> Test Procedure Narrative </label>

                        <div class="col-xs-9">
                            <textarea id="test_proc" name="test_proc" class="form-control input-sm" required style="resize: none;" rows="4"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label modal-input" for="budgeted_mandays"> Budgeted Mandays </label>
                        <div class="col-sm-3">
                            <input type="number" required class="form-control input-sm" id="budgeted_mandays" name="budgeted_mandays" value="" min="1" step="1">
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-controlsmodal-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Test process modal -->

<!-- test audit type modal -->
<div class="modal fade" data-backdrop="static" id="testproc-audit-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Test Procedure Audit Type</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning modal-alert"></div>
                {!! Form::open(['method' => 'post', 'class' => 'form-horizontal', 'id' => 'testproc-audit-form']) !!}
                    <div class="form-group">
                        <label class="col-sm-2 control-label modal-input" for="audit_type"> Audit type </label>
                        <div class="col-sm-8">
                            <select id="audit_type" name="audit_type" class="form-control input-sm basic-info" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-tp-au-modal-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /test audit type modal -->
@endsection

@section('footer_script')
    <script src="/js/iapms.js"></script>
    <script src="/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="/js/select2.full.js"></script>
    <script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
    <script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>

    <script type="text/javascript">
        var $alert       = $('.alert-main'),
            $modal      = $('#controls-modal');

        var $datatable,
            $audit_type_datatable;

        /**
         * Set  Auditable Entity and Business Process details on designated labels
         */
        function dtClickable( table, datatable, id, next, data_id ) {
            table.find('tbody').on('click', 'tr', function () {
                var data = datatable.row( this ).data();

                table.find('tbody > tr').removeClass('selected-row'); // reset tr highlights
                $(this).addClass('selected-row');
                $audit_type_datatable.clear();
                $audit_type_datatable.rows.add(data.audit_types).draw();

                $('#testproc-audit-form').prop('action', '{{url('controls_registry/details/test_procedures')}}/'+data.control_test_id+'/audit_types');
                $('.btn-audit-create').show();
            } );
        }

        function clearDetails() {
            $('.table-details td').html('');
        }

        // stops the page from reloading/closing if the form is edited
        $('input').change(function() {
            if( $(this).val() != "" ) {
                window.onbeforeunload = function() {
                    return 'Are you sure you want to navigate away from this page?';
                };
            }
        });

        $(document).ready(function(){
            dtClickable($('#control-table'), $datatable);

            iapms.LookupCodeSelect2($('#audit_type'), '{{ config('iapms.lookups.audit_type') }}');
            $('.select2-container').css('width', '100%');
        });

        $datatable = $('#control-table').DataTable( {
            ajax: "{{route('controls_registry.details.test_procedures', ['details_id' => $details->control_det_id])}}",
            "lengthMenu": [ 25, 50, 75, 100 ],
            "processing": true,
            "serverSide": true,
            columns: [
                { data: "control_test_seq" },
                { data: "test_proc_narrative", sortable: false, searchable : false },
                { data: "budgeted_mandays", searchable : false },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn-delete delete btn-remove" data-id="'+data.control_test_id+'" data-seq="'+data.control_test_seq+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        } );

        $audit_type_datatable = $('#control-audit-type-table').DataTable( {
            data : [],
            "lengthMenu": [ 25, 50, 75, 100 ],
            "processing": true,
            columns: [
                { data: "audit_type" },
                { data: "description", defaultContent : 'N/A', searchable : false },
                {
                    data: null,
                    "render": function ( data, type, full, meta ) {
                        return '<a href="#" class="btn-audittype-delete" data-test_id="'+data.control_test_id+'" data-audit_type="'+data.audit_type+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                    },
                    orderable: false,
                    searchable: false
                }
            ]
        } );

        // initialize modal fields
        function init_modal( id, sequence, test_proc, mandays ) {
            if( sequence != '' ) {
                // update setting
                $('#testprocedure-form').attr('action', '{{url('controls_registry/details')}}/test_procedures/delete/'+id)
                                      .find('input[name="_method"]').val('put');
            }
            else {
                // create setting
                $('#testprocedure-form').attr('action', '{{route('controls_registry.details.test_procedures.store')}}')
                                      .find('input[name="_method"]').val('post');
            }
            $modal.find('input#sequence').val(sequence);
            $modal.find('#test_proc').val(test_proc);
            $modal.find('#budgeted_mandays').val(mandays);
            $modal.find('input[name="origsq"]').val(sequence);

            $('.modal-alert').hide();
            $('#testprocedure-form .form-group').removeClass('has-error').removeClass('has-success');
        }

        $modal.on('hidden.bs.modal', function (e) {
            init_modal('', '', '');
        });

        $('#testproc-audit-modal').on('hidden.bs.modal', function (e) {
            $(this).find('#audit_type').val('').trigger('change');
            $('.modal-alert').hide();
            $('#testproc-audit-form .form-group').removeClass('has-error').removeClass('has-success');
        });

        function updateDatatable() {
            $datatable.ajax.reload( null, false );
            $modal.modal('hide');
        }

        $(document).on('click', '.btn-controlsmodal-save', function(){
            $('#testprocedure-form').submit();
        });

        $(document).on('click', '.btn-tp-au-modal-save', function(){
            $('#testproc-audit-form').submit();
        });

        $('#testprocedure-form').on('submit', function(){
            $.ajax({
                url: $(this).attr('action'), // url based on the form action
                method: "POST",
                data: $(this).serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    toggle_loading();
                    init_modal('', '', '');
                    swal("Success!", "Your changes has been saved!", "success");
                    updateDatatable();
                },
                error: function(xhr, textStatus, errorThrown){
                    $('#testprocedure-form .form-group').removeClass('has-error');
                    if( xhr.status == 500 ) {
                        $('.modal-alert').removeClass('alert-warning').addClass('alert-danger');
                        $('.modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.modal-input').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
                        $('.modal-alert').html(msg).show();
                    }

                    toggle_loading();
                }
            });

            return false;
        });

        $('#testproc-audit-form').on('submit', function(){
            $.ajax({
                url: $('#testproc-audit-form').attr('action'), // url based on the form action
                method: "POST",
                data: $('#testproc-audit-form').serialize(), // serializes all the form data
                beforeSend : function() {
                    toggle_loading();
                },
                success : function(data) {
                    toggle_loading();
                    init_modal('', '', '');
                    swal("Success!", "Your changes has been saved!", "success");
                    updateDatatable();
                },
                error: function(xhr, textStatus, errorThrown){
                    $('#testprocedure-form .form-group').removeClass('has-error');
                    if( xhr.status == 500 ) {
                        $('.modal-alert').removeClass('alert-warning').addClass('alert-danger');
                        $('.modal-alert').html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                    }
                    else {
                        // displays the validation error
                        var unique_message = [];
                        var msg = '<ul class="list-unstyled">';

                        $('.modal-input').parent().parent().removeClass('has-error');

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                // set error class to fields
                                $('#'+key).parent().parent().addClass('has-error');

                                // shows error message
                                if( unique_message.indexOf(val[i]) === -1 ) {
                                    msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                    unique_message.push(val[i]);
                                }
                            }
                        });
                        msg += '</ul>';

                        $('.modal-alert').removeClass('alert-danger').addClass('alert-warning');
                        $('.modal-alert').html(msg).show();
                    }

                    toggle_loading();
                }
            });

            return false;
        });

        // show update form
        $(document).on('click', '.btn-update', function(){
            var seq = $(this).attr('data-seq'),
                id = $(this).attr('data-id'),
                proc = $(this).attr('data-proc'),
                mandays = $(this).attr('data-mandays');

            // set values
            init_modal(id, seq, proc, mandays);

            $modal.modal('show');

            return false;
        });

        // remove existing code
        $(document).on('click', '.btn-remove', function() {
            var remove_id = $(this).attr('data-id');
            var remove_sequence = $(this).attr('data-seq');

            swal({
                title: "Are you sure?",
                text: 'Remove sequence #'+remove_sequence+'?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function(){
                $.ajax({
                    url: '{{url('controls_registry/details/test_procedures/delete')}}/'+remove_id,
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}' },
                    beforeSend : function() {
                        toggle_loading();
                    },
                    success : function(data) {
                        swal("Deleted!", 'Sequence #'+remove_sequence+" has been deleted.", "success");
                        updateDatatable();
                    }
                });
                toggle_loading();
            });

            return false;
        });

        // remove existing audit type
        $(document).on('click', '.btn-audittype-delete', function() {
            var remove_id = $(this).attr('data-test_id');
            var remove_audit_type = $(this).attr('data-audit_type');

            swal({
                title: "Are you sure?",
                text: 'Remove audit type '+remove_audit_type+'?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function(){
                $.ajax({
                    url: '{{url('controls_registry/details/test_procedures')}}/'+remove_id+'/delete/audit_type/'+remove_audit_type,
                    method: "POST",
                    data: { _method : 'delete', _token : '{{ csrf_token() }}' },
                    beforeSend : function() {
                        toggle_loading();
                    },
                    success : function(data) {
                        swal("Deleted!", 'Audit type '+remove_audit_type+" has been deleted.", "success");
                        location.reload();
                    }
                });
                toggle_loading();
            });

            return false;
        });
    </script>
@endsection
