@extends('layouts.app')
@section('styles')
    <link rel="stylesheet" href="/css/select2.min.css" />
    <link rel="stylesheet" href="/plugins/sweetalert/sweetalert.css" />
    <link rel="stylesheet" href="/css/datepicker.css" />
    <link rel="stylesheet" href="/css/datatables.bootstrap.css" />
    <style type="text/css">
        .select2-search:after { display: none !important; }
    </style>
@endsection
@section('content')
<div class="main-content">
    <div class="main-content-inner">
        @include('includes.breadcrumb')

        <div class="page-content">
            @include('includes.content_header')

            <div class="row">
                <div class="col-xs-12">
                    @if (Session::has('message'))
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">
                            <i class="ace-icon fa fa-times"></i>
                        </button>

                        <strong>
                            <i class="ace-icon fa fa-check"></i>

                        </strong>

                        {!! session('message') !!}
                        <br>
                    </div>
                    @endif
                    <div class="clearfix">
                        <div class="pull-right">
                            <a class="btn btn-primary" href="{{ route('controls_registry.create') }}">
                                <i class="ace-icon fa fa-plus"></i>
                                Create
                            </a>
                        </div>
                    </div>
                    <div class="table-header">
                        Controls List
                    </div>

                    <!-- div.table-responsive -->

                    <!-- div.dataTables_borderWrap -->
                    <table id="controls-table" class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th width="15%">Control Name</th>
                                <th>Control Description</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>

                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- /.page-content -->
    </div>
</div><!-- /.main-content -->
<!-- Control modal -->
<div class="modal fade" data-backdrop="static" id="controls-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Controls Form</h4>
            </div>
            <div class="modal-body">
                <div style="display:none;" class="alert alert-warning modal-alert"></div>
                {!! Form::open(['url' => route('controls_registry.store'), 'class' => 'form-horizontal', 'id' => 'controls-form']) !!}
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="control_name"> Control Name </label>
                        <div class="col-xs-8">
                            <input type="text" id="control_name" name="control_name" class="form-control input-sm" readonly="true">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="control_code"> Control Code </label>
                        <div class="col-xs-8">
                            <select id="control_code" name="control_code" class="form-control input-sm basic-info" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="control_code"> Control Code Description </label>
                        <div class="col-xs-8">
                            <span class="label label-primary label-wrap"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="component"> Component </label>
                        <div class="col-xs-8">
                            <select id="component" name="component" class="form-control input-sm basic-info" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="control_code"> Component Description </label>
                        <div class="col-xs-8">
                            <span class="label label-primary label-wrap"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="principle"> Principle </label>
                        <div class="col-xs-8">
                            <select id="principle" name="principle" class="form-control input-sm basic-info" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="control_code"> Principle Description </label>
                        <div class="col-xs-8">
                            <span class="label label-primary label-wrap"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="focus"> Focus </label>
                        <div class="col-xs-8">
                            <select id="focus" name="focus" class="form-control input-sm basic-info" data-placeholder="Click to Choose..."></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="control_code"> Focus Description </label>
                        <div class="col-xs-8">
                            <span class="label label-primary label-wrap"></span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-controlsmodal-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
<!-- /Control modal -->
@include('includes.modals.additional_information')
@endsection

@section('footer_script')
<script src="/js/iapms.js"></script>
<script src="/js/jquery.validate.js"></script>
<script src="/js/date-time/bootstrap-datepicker.js"></script>
<script src="/js/select2.full.js"></script>
<script src="/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/js/dataTables/1.10.12/jquery.dataTables.min.js"></script>
<script src="/js/dataTables/1.10.12/dataTables.bootstrap.min.js"></script>
<script type="text/javascript">
    var _token      = '{{ csrf_token() }}',
        control     = $('#control_code'),
        component   = $('#component'),
        principle   = $('#principle'),
        focus       = $('#focus'),
        $alert      = $('.modal-alert'),
        form        = $('#controls-form'),
        $modal      = $('#controls-modal');
    var ai_modal = $('#additional-information-modal');
    var datatable= $('#controls-table').DataTable( {
        ajax: "{{route('controls_registry.list')}}",
        processing: true,
        orderCellsTop: true,
        columns: [
            { data: "control_name"  },
            { data: "lv_control_code_desc", orderable: false },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class="btn-add-info" data-id="'+data.control_id+'" title="additional info" rel="tooltip"><i class="fa fa-list-ul edit"></i></a> &nbsp; <a href="{{url('controls_registry/edit')}}/'+data.control_id+'"><i class="fa fa-pencil edit" title="edit" rel="tooltip"></i></a> &nbsp; <a href="#" class="btn-delete delete" data-id="'+data.control_id+'" data-control_name="'+data.control_name+'" title="delete" rel="tooltip"><i class="fa fa-trash"></i></a>';
                },
                orderable: false,
                searchable: false
            }
        ]
    } );

    $(function(){
        iapms.LookupCodeSelect2(control, '{{ config('iapms.lookups.control.controls') }}');
        iapms.LookupCodeSelect2(component, '{{ config('iapms.lookups.control.component') }}');
        iapms.LookupCodeSelect2(principle, '{{ config('iapms.lookups.control.principle') }}');
        iapms.LookupCodeSelect2(focus, '{{ config('iapms.lookups.control.focus') }}');
        $('.select2-container').css('width', '100%');
    });

    function init_modal(co, pr, fo, id) {
        component.val(co).trigger('change');
        principle.val(pr).trigger('change');
        focus.val(fo).trigger('change');
        $modal.find('input[name="id"]').val(id);
        if( id != '' )
            $modal.find('input[name="_method"]').val('put');
        else
            $modal.find('input[name="_method"]').val('post');

        $modal.find('.modal-alert').hide();
        $modal.find('.form-group').removeClass('has-error').removeClass('has-success');
    }

    $modal.on('hidden.bs.modal', function (e) {
        init_modal('', '', '', '');
    });

    $('.basic-info').on('change blur', function(){
        var control_name = control.val()+'.'+(component.val() || '')+'-'+(principle.val() || '')+'-'+(focus.val() || '');
        $('#control_name').val(control_name);
    });

    // save the form through AJAX call
    $('.btn-controlsmodal-save').on('click', function() {
        $.ajax({
            url: form.attr('action'), // url based on the form action
            method: "POST",
            data: form.serialize(), // serializes all the form data
            beforeSend : function() {
                toggle_loading();
            },
            success : function(data) {
                if( data.success ) {
                    location.reload();
                }
            },
            error: function(xhr, textStatus, errorThrown){
                if( xhr.status == 500 ) {
                    $alert.removeClass('alert-warning').addClass('alert-danger');
                    $alert.html('Oops! There\'s something wrong with the system. Please report immediately!').show();
                }
                else {
                    // displays the validation error
                    var unique_message = [];
                    var msg = '<ul class="list-unstyled">';

                    $('.basic-info').parent().parent().removeClass('has-error');

                    $.each(xhr.responseJSON, function(key, val){
                        for(var i=0; i < val.length; i++) {
                            // set error class to fields
                            $('#'+key).parent().parent().addClass('has-error');

                            // shows error message
                            if( unique_message.indexOf(val[i]) === -1 ) {
                                msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                                unique_message.push(val[i]);
                            }
                        }
                    });
                    msg += '</ul>';

                    $alert.removeClass('alert-danger').addClass('alert-warning');
                    $alert.html(msg).show();
                }

                toggle_loading();
            }
        });
    });

    $( '#component_code, #principle_code, #focus_code' ).on( 'keyup change', function () {
        var index = $(this).attr('data-index');
        if ( datatable.column(index).search() !== $(this).val() ) {
            datatable
            .column( index )
            .search( $(this).val() )
            .draw();
        }
    } );

    // remove auditee
    $(document).on('click', '.btn-delete', function() {
        var remove_id = $(this).attr('data-id');
        var remove_control = $(this).attr('data-control_name');

        swal({
            title: "Continue?",
            text: "You are about to delete component "+remove_control+"?",
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true
        }, function(){
            $.ajax({
                url: '{{ url('controls_registry/delete') }}/'+remove_id,
                method: "POST",
                data: { _token : '{{csrf_token()}}', _method : 'delete' }, // serializes all the form data
                success : function(data) {
                    if( data.success ) {
                        swal({
                            title: "Success!",
                            text: "Redirecting, please wait...",
                            type: "success",
                            showConfirmButton: false
                        });
                        location.reload();
                    }
                }
            });
        });

        return false;
    });

    $(document).on('click', '.btn-add-info', function() {
        var id = $(this).attr('data-id');
        iapms.getAddInfoForm(id, 'controls_master').success(function(data) {
            if( !data.success ) {
                ai_modal.find('#ai_wrapper').html('<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i> '+data.message+'</strong></div>');
                ai_modal.find('.btn-save-ai').hide();
            }
            else {
                ai_modal.find('#ai_wrapper').html(data.data);
                ai_modal.find('.btn-save-ai').show();
            }
            ai_modal.modal('show');
            iapms.setAddInfoForm(ai_modal, '{{url('controls_registry')}}/'+id+'/additional_information/update', 'put');

            toggle_loading();
        });

        return false;
    });

    $(function(){
        ai_modal.find('form').validate();
    });

    ai_modal.find('form').on('submit', function(e){
        e.preventDefault();
        if($(this).valid()) {
            swal({
                title: "Continue?",
                text: "You are about to update additional information.",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, save it!",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            },
            function(){
                $.ajax({
                    url: ai_modal.find('form').attr('action'),
                    method: "POST",
                    data: ai_modal.find('form').serialize(), // serializes all the form data
                    success : function(data) {
                        if(data.success) {
                            swal("Success!", "Additional information has been updated.", "success");
                            location.reload();
                        }
                    },
                    error: function(xhr, textStatus, errorThrown){
                        swal.close();
                        // displays the validation error
                        var msg = '<ul class="list-unstyled">';

                        $.each(xhr.responseJSON, function(key, val){
                            for(var i=0; i < val.length; i++) {
                                msg += '<li><i class="ace-icon fa fa-times bigger-110 red"></i> '+ val[i]+'</li>';
                            }
                        });
                        msg += '</ul>';

                        ai_modal.find('.alert-modal').html(msg).show();
                    }
                });
            });
        }

        return false;
    });

    ai_modal.on('click', '.btn-save-ai', function() {
        ai_modal.find('form').submit();
    });
</script>
@endsection
