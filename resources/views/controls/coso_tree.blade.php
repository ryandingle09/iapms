@section('styles')
    <style type="text/css">
        span.ws-wrap span.fancytree-title {
            white-space: normal;
        }
        span.ws-pre span.fancytree-title { white-space: pre; }
    </style>
@endsection
<div id="coso_tree"></div>
@push('coso_script')
    <script src="/js/jquery-ui.js"></script>
    <script src="/plugins/fancytree/jquery.fancytree-all.min.js"></script>
    <script type="text/javascript">
        function generateCosoTree(components, principle, focus) {
            var tree = [];
            for(var c=0; c < components.length; c++) {
                var pr_child = [];
                var component = coso_data.filter(function (coso_data) { return coso_data.id == components[c] });

                if( typeof principle[components[c]] != "undefined" ) {
                    for(var p=0; p < principle[components[c]].length; p++) {
                        var fo_child = [];
                        var pr = component[0].child.data.filter(function (coso_data) { return coso_data.id == principle[components[c]][p] });
                        if( typeof focus[principle[components[c]][p]] != "undefined" ) {
                            for(var f=0; f < focus[principle[components[c]][p]].length; f++) {
                                if( typeof pr[0] != "undefined" ) {
                                    var fo = pr[0].child.data.filter(function (coso_data) { return coso_data.id == focus[principle[components[c]][p]][f] });
                                    if( fo.length && typeof fo[0].id != "undefined" ) fo_child.push({title: fo[0].id+' - '+fo[0].meaning, key: focus[principle[components[c]][p]][f], icon : 'fa fa-bullseye'});
                                }
                            }
                        }
                        console.log(pr.length);
                        if(pr.length){
                            pr_child.push({title: pr[0].id+' - '+pr[0].meaning, key: principle[components[c]][p], icon : 'fa    fa-certificate', children : fo_child});
                        }
                    }
                    tree.push({title: component[0].id+' - '+component[0].meaning, key: components[c], icon : 'fa fa-cubes', children : pr_child});
                }
            }

            if( !tree.length ) {
                tree = [{title:'No data to display.', icon: 'fa fa-exclamation-circle'}];
            }

            $('#coso_tree').fancytree({
                source : tree,
                expand: function(event, data){
                    var node = data.node;
                    node.visit(function(node){
                        node.setExpanded(true);
                    });
                }
            });
            $.ui.fancytree.getTree("#coso_tree").reload(tree);
        }
    </script>
@endpush