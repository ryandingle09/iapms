<div class="modal fade" data-backdrop="static" id="coso-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">COSO Components</h4>
            </div>
            <div class="modal-body">
                @include('controls.coso_tree')
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@push('coso_script')
    <script type="text/javascript">
        $(document).on('click', '.btn-view-coso', function(){
            var row, data;
            if( typeof $datatable != "undefined" ) {
                row = $(this).parent().parent();
                data = $datatable.row(row).data();
            }
            else {
                row = $(this).attr('data-id');
                data = coso_components[row];
            }
            var _component = {}, _principle = {}, _focus = {};
            if( typeof data != "undefined" ) {
                _component = typeof data.component_code != "object" ? JSON.parse(data.component_code || '{}') : data.component_code;
                _principle = typeof data.principle_code != "object" ? JSON.parse(data.principle_code || '{}') : data.principle_code;
                _focus = typeof data.focus_code != "object" ? JSON.parse(data.focus_code || '{}') : data.focus_code;
            }

            generateCosoTree(_component, _principle, _focus);
            $('#coso-modal').modal('show');

            return false;
        });
    </script>
@endpush

