<div class="col-xs-12">
    <div class="table-header"> Project - Scopes </div>
</div>
<div class="col-xs-12 mrgB15">
    <table id="scope-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Project Name</th>
                <th >Audit Type</th>
                <th >Auditable Entity Name</th>
                <th >Main Business Process Name</th>
                <th >IOM No.</th>
                <th >Target Start Date</th>
                <th >Target End Date</th>
                <th >Budgeted Mandays</th>
                <th >Status</th>
                <th >Action</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
<div class="approval_form hidden" >
    <div class="col-xs-12 mrgB15 ">
        <div class="table-header"> Project Scope Approval </div>
    </div>
    <div class="col-xs-12 ">
        <div id="project-scope-approval-alert"></div>
        <form class="form-horizontal" id="project-scope-approval-form" action="" method="POST">
            {{ csrf_field() }}
            <div class="form-row align-right mrgB15" >
                <button class="btn btn-info btn-sm">
                    <i class="ace-icon fa fa-save bigger-120"></i>
                    Apply
                </button>
                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                    <i class="ace-icon fa fa-times bigger-120"></i>
                    Cancel
                </a>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label align-left" for="remarks"> CAE Approver Remarks </label>
                <div class="col-sm-10">
                    <textarea class="form-control input-sm" name="remarks" id="remarks" rows="3" ></textarea>
                </div>
            </div>
            <div class="form-row align-right" >
                <button class="btn btn-info btn-sm">
                    <i class="ace-icon fa fa-save bigger-120"></i>
                    Apply
                </button>
                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                    <i class="ace-icon fa fa-times bigger-120"></i>
                    Cancel
                </a>
            </div>
        </form>
    </div>
</div>
<div class="approval_view hidden" >
    <div class="col-xs-12 mrgB15 ">
        <div class="table-header"> Project Scope Approval </div>
    </div>
    <div class="col-xs-12 ">
        <div id="project-scope-approval-alert"></div>
        <form class="form-horizontal" id="project-scope-approval-view" action="" method="POST">
            <div class="form-group">
                <label class="col-sm-2 control-label align-left" for="approver"> CAE Approver Name </label>
                <div class="col-sm-6">
                    <input type="text" name="approver" class="form-control input-sm makelabel" value="n/a">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label align-left" for="status_from"> Status From </label>
                <div class="col-sm-2">
                    <input type="text" name="status_from" class="form-control input-sm status_from makelabel" value="" readonly>
                </div>
                <label class="col-sm-2 control-label align-left" for="status_to"> Status To</label>
                <div class="col-sm-2">
                    <input type="text" name="status_to" class="form-control input-sm  status_to makelabel" value="" readonly>
                </div>
                <label class="col-sm-2 control-label align-left" for="status_date"> Status Date</label>
                <div class="col-sm-2">
                    <input type="text" name="status_date" class="form-control input-sm status_date makelabel" value="" readonly>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label align-left" for="remarks"> CAE Approver Remarks </label>
                <div class="col-sm-10">
                    <input type="text" name="remarks" class="form-control input-sm makelabel" value="n/a">
                </div>
            </div>
        </form>
    </div>
</div>
@section('tab_scope_scripts')
<script type="text/javascript">
    // selectedScope coming from fieldwork.index;
    var scope_datatable = $("#scope-table").DataTable( {
        "bDestroy": true,
        ajax: "{{ url('project/0/scope/list')}}",
        "processing": true,
        order: [[ 1, 'asc' ]],
        createdRow: function( row, data, dataIndex ) {
            if(selectedScope == data.project_scope_id){
                $( row ).addClass('selected-row');
            }
            $( row ).attr('data-id', data.project_scope_id);
        },
        columns: [
            { data: "project.project_name" },
            { data: "project.audit_type" },
            { data: "auditable_entity.auditable_entity_name" },
            { data: "main_bp.main_bp_name" },
            { data: "iom.ref_no" },
            { data: "target_start_date" },
            { data: "target_end_date" },
            { data: "budgeted_mandays" },
            { data: "project_scope_status" },
            {
                data: null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="#" class=""><i class="fa fa-sitemap text-primary" title="Details" rel="tooltip"></i></a> ';
                },
                orderable: false,
                searchable: false,
                className: "text-center"
            }
        ]
    });

    $(document).on('click','#scope-table tbody tr',function(){
        $("#main_tab").find("a[href='#apg']").attr('data-toggle','tab');
        $("#main_tab").find("a[href='#highlights']").attr('data-toggle','tab');
        selectedScope = $(this).data('id');
        $(".selected-row").removeClass('selected-row');
        $(this).addClass('selected-row');
        
        $.ajax({
            url : "{{ url('project-scope') }}/" + selectedScope + "/approvals",
            method : "GET",
            beforeSend : sLoading(),
            success : function(response){
                swal.close();
                if(response.data != null){
                    $("#project-scope-approval-view [name='approver']").val(response.data.auditor.description)
                    $("#project-scope-approval-view").supply(response.data);
                    $("#project-scope-approval-view .makelabel").makeLabel();
                    $(".approval_form").addClass('hidden');
                    $(".approval_view").removeClass('hidden').scroll();
                }else{
                    $(".approval_view").addClass('hidden');
                    $(".approval_form").removeClass('hidden').scroll();
                }
            }
        }); 
    });

    $(document).on("submit",'#project-scope-approval-form',function(){
        $.ajax({
            url : "{{ url('project-scope') }}/" + selectedScope + "/approvals",
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#project-scope-approval-form")[0].reset();
                swal.close();
                scope_datatable.ajax.reload( null, false );
                $("body").scroll();
                $("#alert-wrapper").aSuccess(response.message);
                $(".approval_form").addClass('hidden');
            }
        }); 
        return false;
    })

</script>
@endsection