<!-- Change APG status Modal -->
<div class="modal fade " data-backdrop="static" id="change-apg-status-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Fieldwork Audit Program Guide Step Approval</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div id="change-apg-status-alert"></div>
                        <form class="form-horizontal" id="change-apg-status-form" action="" method="POST">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="status_from"> Status From </label>
                                <div class="col-sm-4">
                                    <input type="text" name="status_from" class="form-control input-sm status_from makelabel" value="n/a">
                                </div>
                                <label class="col-sm-2 control-label align-left" for="status_to"> Status To</label>
                                <div class="col-sm-4">
                                    <select class="cold-sm-2 form-control input-sm" name="status_to"></select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="remarks"> Auditor Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm" name="remarks" id="remarks" rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <button class="btn btn-info btn-sm">
                                    <i class="ace-icon fa fa-save bigger-120"></i>
                                    Apply
                                </button>
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Close
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Change APG status Modal -->
@section('change_apg_status_scripts')
<script type="text/javascript">
    // apg_approval_datatable came from fieldwork.tab_apg_approval
    
    $(document).on('submit','#change-apg-status-form',function(){
        $.ajax({
            url : $(this).attr('action'),
            method : $(this).attr('method'),
            data : $(this).serialize(),
            beforeSend : sLoading(),
            success : function(response){
                $("#change-apg-status-modal").modal('hide');
                $("#change-apg-status-form")[0].reset();
                apg_approval_datatable.ajax.reload( null, false );
                sSuccess(response.message);
            },
            error : function(xhr){
                switch(parseInt(xhr.status)){
                    case 422:
                        swal.close();
                        alertWrapper = $("#change-apg-status-alert");
                        $("#change-apg-status-form").showError(xhr.responseJSON,alertWrapper);
                        $("#change-apg-status-modal").resetModalHeight();
                        break;
                    case 500:
                        sError("Error "+xhr.status+" : "+xhr.statusText);
                    break;
                }
            }
        });
        return false;
    });
</script>
@endsection