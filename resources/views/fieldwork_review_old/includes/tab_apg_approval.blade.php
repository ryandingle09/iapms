<div class="col-xs-12">
    <a class="btn btn-success btn-sm btn-change-apg-status" href='#change-apg-status-modal'>
        <i class="ace-icon fa fa-check bigger-120"></i>
        Change Status
    </a>
</div>
<div class="col-xs-12 ">
    <div class="table-header"> Fieldwork Audit Program Guide Step Approval Workflow </div>
</div>

<div class="col-xs-12 mrgB15">
    <table id="apg-approval-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Sequence</th>
                <th >Name</th>
                <th >Role</th>
                <th >Status From</th>
                <th >Status To</th>
                <th >Remarks</th>
                <th >Date</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

</div>
@include('fieldwork_review.includes.change_apg_status_modal')
@section('tab_apg_approval_scripts')
<script type="text/javascript">
    // selectedScope variables can coming tab_scope.blade.php
    // selectedApg variables can coming tab_scope.blade.php
    var apg_approval_datatable = null;
    

    $(document).on('click',"#main_tab a[href='#apg_approval']",function(){
        if(selectedApg){
            var url =  "{{ url('project-scope-apg')}}/" + selectedApg + "/approvals";
            $("#change-apg-status-form").attr('action',url);  

            apg_approval_datatable = $("#apg-approval-table").DataTable( {
                "bDestroy": true,
                ajax: url + "/list",
                "processing": true,
                order: [[ 0, 'desc' ]],
                createdRow: function( row, data, dataIndex ) {
                    $( row ).attr('data-id', data.project_scope_apg_id);
                },
                columns: [
                    { data: "approval_seq" },
                    { data: "remarks" },
                    { data: "remarks" },
                    { data: "status_from" },
                    { data: "status_to" },
                    { data: "remarks" },
                    { data: "status_date" },
                ]
            });
             
        }else{
            sAlert("Opps!","You have to select project scope apg to open this tab.");
        }
    });

    $(document).on('click','.btn-change-apg-status',function(){
        $.ajax({
            url : "{{ url('project-scope') }}/" + selectedScope + "/apg/"+selectedApg,
            method : "GET",
            beforeSend : sLoading(),
            success : function(response){
                swal.close();
                setNextStatusValues(response.data.status);
                $('#change-apg-status-form [name="status_from"]').val(response.data.status);
                $("#change-apg-status-form .makelabel").makeLabel();
                $("#change-apg-status-modal").modal('show');
            }
        });
        return false;
    })

    function setNextStatusValues(status){
        selectValues = [];
        switch(status){
            case "Reviewer Reject":
            case "Reviewed":
                selectValues =  ["Approved" , "Approver Reject"];
            break;
            case "For Review":
                selectValues =  ["Reviewed" , "Reviewer Reject"];
            break;
            default:
                selectValues =  ["For Review"];
            break;
        }
        $('#change-apg-status-form [name="status_to"]').html("");
        $.each(selectValues, function(key, value) {   
            $('#change-apg-status-form [name="status_to"]')
                .append($("<option></option>")
                .attr("value",value)
                .text(value)); 
        });
    }
</script>
@yield('change_apg_status_scripts')
@endsection

