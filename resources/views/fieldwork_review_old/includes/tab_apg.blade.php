<div class="col-xs-12">
    <div class="table-header"> Fieldwork Audit Program Guide </div>
</div>
<div class="col-xs-12 mrgB15">
    <table id="apg-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Control Sequence</th>
                <th >Control Name</th>
                <th >Test Procedure Narrative</th>
                <th >Audit Objectives</th>
                <th >Auditor Name</th>
                <th >Budgeted Mandays</th>
                <th >Status</th>
                <th >Action</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@include('fieldwork_review.includes.view_apg_findings_modal')
@section('tab_apg_scripts')
<script type="text/javascript">
    // selectedScope variables can coming tab_scope.blade.php
    // selectedApg variables can coming tab_scope.blade.php
    var apg_datatable = null;
    var apg_status = "New";
    $(document).on('click',"#main_tab a[href='#apg']",function(){
        if(selectedScope){
            apg_datatable = $("#apg-table").DataTable( {
                "bDestroy": true,
                ajax: "{{ url('project-scope') }}/" + selectedScope + "/apg/list",
                "processing": true,
                order: [[ 1, 'asc' ]],
                createdRow: function( row, data, dataIndex ) {
                    if(selectedApg == data.project_scope_apg_id){
                        $( row ).addClass('selected-row');
                    }
                    $( row ).attr('data-id', data.project_scope_apg_id)
                            .attr('data-status', data.status);
                },
                columns: [
                    {
                        data: null,
                        render : function ( data, type, full, meta ) {

                            return data.control_seq ? data.control_seq : 'n/a';
                        }
                    },
                    { 
                        data: null,
                        render : function(data, type, full, meta){
                            return data.control_master ? data.control_master.control_name : "n/a";
                        }

                    },
                    { 
                        data: null,
                        render : function(data, type, full, meta){
                            return data.test_proc ? data.test_proc_narrative : "n/a";
                        }

                    },
                    { 
                        data: null,
                        render : function(data, type, full, meta){
                            return data.test_proc ? data.audit_objective : "n/a";
                        }

                    },
                    {
                        data: null,
                        render : function ( data, type, full, meta ) {

                            return data.auditor ? data.auditor.first_name+' '+data.auditor.middle_name+' '+data.auditor.last_name : 'n/a';
                        }
                    },
                    { data: "budgeted_mandays" },
                    { data: "status" },
                    {
                        data: null,
                        render : function ( data, type, full, meta ) {

                            return '<a href="#" class="auditor_input" data-id="'+data.project_scope_apg_id+'"><i class="fa fa-file text-danger" title="Auditor Input" rel="tooltip"></i></a>';
                        },
                        className: "text-center"
                    },
                ]
            });     
        }else{
            sAlert("Opps!","You have to select project scope to open this tab.");
        }
    });

    $(document).on('click',"#apg-table .auditor_input",function(){
        var apg = $(this).data('id');
        $.ajax({
            url : "{{ url('project-scope-apg')}}/"+apg+"/findings",
            method : "GET",
            beforeSend : sLoading(),
            success : function(response){
                if(response.data){
                    swal.close();
                    $("#view-apg-findings-form").supply(response.data);
                    $("#view-apg-findings-form .makelabel").makeLabel();
                    $("#view-apg-findings-modal").modal('show');
                }else{
                    $.sInfo("This APG doesn't have auditor input yet.");
                }
            }
        }); 
        return false
    });

    $(document).on('click',"#apg-table tbody tr",function(){
        selectedApg = $(this).data('id');
        apg_status = $(this).data('status');
        $(".selected-row").removeClass('selected-row');
        $("#main_tab").find("a[href='#apg_approval']").attr('data-toggle','tab');
        $(this).addClass('selected-row');
        return false;
    });

</script>
@endsection