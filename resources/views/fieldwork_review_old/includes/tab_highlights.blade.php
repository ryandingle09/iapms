<div class="col-xs-12">
    <div class="table-header"> Fieldwork Audit Highlights </div>
</div>
<div class="col-xs-12 mrgB15">
    <table id="highlights-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th >Sequence</th>
                <th >Findings</th>
                <th >Action Taken</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>

@section('tab_highlights_scripts')
<script type="text/javascript">
    // selectedScope coming from fieldwork.index;
    var highlights_datatable = null;
    $(document).on('click',"#main_tab a[href='#highlights']",function(){
        if(selectedScope){
            var highlights_datatable = $("#highlights-table").DataTable( {
                "bDestroy": true,
                ajax: "{{ url('project-scope')}}/"+selectedScope + "/highlight/list" ,
                "processing": true,
                order: [[ 0, 'asc' ]],
                columns: [
                    {data: "highlights_seq"},
                    {data: "findings"},
                    {data: "auditee_actions_taken"}
                ]
            });   
        }else{
            sAlert("Opps!","You have to select project scope to open this tab.");
        }
    });

</script>
@endsection