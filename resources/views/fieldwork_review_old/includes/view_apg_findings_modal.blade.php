<!-- View APG Findings Modal -->
<div class="modal fade " data-backdrop="static" id="view-apg-findings-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Fieldwork Auditor Input</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12 ">
                        <div id="view-apg-findings-alert"></div>
                        <form class="form-horizontal" id="view-apg-findings-form" action="" method="POST">
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="status"> Status </label>
                                <div class="col-sm-2">
                                    <input type="text" name="status" class="form-control input-sm status makelabel" value="n/a">
                                </div>
                                <div class="col-sm-2" style="padding-top: 5px;">
                                    <input class="ace" type="checkbox" name="non_issue" value="Y" readonly>
                                    <span class="lbl"> Non-Issue?</span>
                                </div>
                                <div class="col-sm-2" style="padding-top: 5px;">
                                    <input class="ace" type="checkbox" name="highlight" value="Y" readonly>
                                    <span class="lbl"> Highlight?</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="issues"> Issues/ Findings </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm makelabel" name="issues" id="issues" rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="conclusions"> Conclusion/ Opinion </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm makelabel" name="conclusions" id="conclusions" rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="recommendations"> Recommendations </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm makelabel" name="recommendations" id="recommendations" rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="auditee_actions_needed"> Auditee Actions Needed </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm makelabel" name="auditee_actions_needed" id="auditee_actions_needed " rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="auditee_action_due_date"> Auditee Action Due Date </label>
                                <div class="col-sm-4">
                                    <input type="text" name="auditee_action_due_date" class="form-control input-sm auditee_action_due_date makelabel" value="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="auditee_actions_taken"> Auditee Actions Taken </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm makelabel" name="auditee_actions_taken" id="auditee_actions_taken" rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label align-left" for="auditor_remarks"> Auditor Remarks </label>
                                <div class="col-sm-10">
                                    <textarea class="form-control input-sm makelabel" name="auditor_remarks" id="auditor_remarks" rows="3" ></textarea>
                                </div>
                            </div>
                            <div class="form-row align-right" >
                                <a href="" class="btn btn-warning btn-sm" data-dismiss="modal">
                                    <i class="ace-icon fa fa-times bigger-120"></i>
                                    Close
                                </a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /View APG Findings Modal -->
@section('view_apg_findings_modal_scripts')
<script type="text/javascript">

</script>
@endsection