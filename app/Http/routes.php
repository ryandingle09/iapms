<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web'] ], function () {

    // Authentication Start
    Route::get('login', ['as' => 'auth.index',  'uses' => 'AuthController@index']);
    Route::post('login', ['as' => 'auth.login',  'uses' => 'AuthController@login']);
    Route::get('logout', ['as' => 'auth.logout',  'uses' => 'AuthController@logout']);
    // Authentication End

    // Session Manager Start
    Route::post('session/check-idle', ['uses' => 'SessionController@checkIdle', 'as' => 'session.check_idle']);
    // Session Manager End

    // Authenticated Routes
    Route::group(['middleware' => [ 'auth' , 'reset_last_activity' ] ], function(){
        // Administrators Start
        Route::get('/', ['as' => 'home',  'uses' => 'AdministratorController@index']);
        // Administrators End

        Route::group(['prefix' => 'administrator'], function(){
            # Lookup
            Route::group(['prefix' => 'lookup'], function(){
                Route::get('/', ['as' => 'administrator.lookup',  'uses' => 'LookupController@index']);
                Route::get('/type/{type}', ['as' => 'administrator.lookup.type',  'uses' => 'LookupController@ajaxLookupValueByType']);
                Route::post('/', ['as' => 'administrator.lookup.store',  'uses' => 'LookupController@store']);
                Route::get('/create', ['as' => 'administrator.lookup.create',  'uses' => 'LookupController@create']);
                Route::get('/list', ['as' => 'administrator.lookup.list',  'uses' => 'LookupController@getLookupList']);
                Route::get('/edit/{type}', ['as' => 'administrator.lookup.edit',  'uses' => 'LookupController@edit']);
                Route::put('/update/{type}', ['as' => 'administrator.lookup.update',  'uses' => 'LookupController@update']);
                Route::delete('/delete/{id}', ['as' => 'administrator.lookup.delete',  'uses' => 'LookupController@destroy']);
                Route::get('{type}/value/list', ['as' => 'administrator.lookup.value_list',  'uses' => 'LookupController@getLookupValueList']);
                Route::post('/value', ['as' => 'administrator.lookup.value_store',  'uses' => 'LookupController@storeLookupValue']);
                Route::put('/value/update/{code}', ['as' => 'administrator.lookup.value_update',  'uses' => 'LookupController@putUpdateLookupValue']);
                Route::delete('{type}/value/delete/{code}', ['as' => 'administrator.lookup.value_destroy',  'uses' => 'LookupController@destroyLookupValue']);
            });

            # Users : don not use this group anymore
            Route::group(['prefix' => 'users'], function(){
                Route::get('/', ['as' => 'administrator.users',  'uses' => 'UserManagementController@index']);
                Route::post('/', ['as' => 'administrator.users.store',  'uses' => 'UserManagementController@store']);
                Route::get('/create', ['as' => 'administrator.users.create',  'uses' => 'UserManagementController@create']);
                Route::get('/edit/{username}', ['as' => 'administrator.users.edit',  'uses' => 'UserManagementController@edit']);
                Route::put('/update/{username}', ['as' => 'administrator.users.update',  'uses' => 'UserManagementController@update']);
                Route::get('/list', ['as' => 'administrator.users.list',  'uses' => 'UserManagementController@getUserList']);
                Route::delete('/delete/{id}', ['as' => 'administrator.users.delete',  'uses' => 'UserManagementController@destroy']);
            });

            # roles
            Route::group(['prefix' => 'roles'], function(){
                Route::get('/', ['as' => 'administrator.roles',  'uses' => 'RolesController@index']);
                Route::post('/', ['as' => 'administrator.roles.store',  'uses' => 'RolesController@store']);
                Route::get('/create', ['as' => 'administrator.roles.create',  'uses' => 'RolesController@create']);
                Route::get('/list', ['as' => 'administrator.roles.list',  'uses' => 'RolesController@getRoleList']);
                Route::get('/edit/{name}', ['as' => 'administrator.roles.edit',  'uses' => 'RolesController@edit']);
                Route::put('/update/{name}', ['as' => 'administrator.roles.update',  'uses' => 'RolesController@update']);
                Route::delete('/delete/{id}', ['as' => 'administrator.roles.delete',  'uses' => 'RolesController@destroy']);
            });

            # permissions
            Route::group(['prefix' => 'permissions'], function(){
                Route::get('/', ['as' => 'administrator.permissions',  'uses' => 'PermissionsController@index']);
                Route::post('/', ['as' => 'administrator.permissions.store',  'uses' => 'PermissionsController@store']);
                // Route::get('/create', ['as' => 'administrator.users.create',  'uses' => 'UserManagementController@create']);
                Route::get('/list/{role?}', ['as' => 'administrator.permissions.list',  'uses' => 'PermissionsController@getPermissionList']);
                // Route::get('/edit/{username}', ['as' => 'administrator.users.edit',  'uses' => 'UserManagementController@edit']);
                Route::put('/update/{id}', ['as' => 'administrator.permissions.update',  'uses' => 'PermissionsController@update']);
                Route::delete('/delete/{id}', ['as' => 'administrator.permissions.delete',  'uses' => 'PermissionsController@destroy']);
            });

            # Responsibilities
            Route::group(['prefix' => 'responsibility'], function(){
                Route::get('/', ['as' => 'administrator.responsibility',  'uses' => 'ResponsibilityController@index']);
                Route::post('/', ['as' => 'administrator.responsibility.store',  'uses' => 'ResponsibilityController@store']);
                Route::get('/create', ['as' => 'administrator.responsibility.create', 'uses' => 'ResponsibilityController@create']);
                Route::get('/list', ['as' => 'administrator.responsibility.list',  'uses' => 'ResponsibilityController@getresponsibilityList']);
                Route::get('/edit/{name}', ['as' => 'administrator.responsibility.edit',  'uses' => 'ResponsibilityController@edit']);
                Route::put('/update/{name}', ['as' => 'administrator.responsibility.update',  'uses' => 'ResponsibilityController@update']);
                Route::delete('/delete/{id}', ['as' => 'administrator.responsibility.delete',  'uses' => 'ResponsibilityController@destroy']);
            });

            # Menus
            Route::group(['prefix' => 'menu'], function(){
                Route::get('/', ['as' => 'administrator.menu',  'uses' => 'MenuController@index']);
                Route::post('/', ['as' => 'administrator.menu.store',  'uses' => 'MenuController@store']);
                Route::get('/create', ['as' => 'administrator.menu.create',  'uses' => 'MenuController@create']);
                Route::get('/edit/{id}', ['as' => 'administrator.menu.edit',  'uses' => 'MenuController@edit']);
                Route::put('/update/{id}', ['as' => 'administrator.menu.update',  'uses' => 'MenuController@update']);
                Route::delete('/delete/{id}', ['as' => 'administrator.menu.delete',  'uses' => 'MenuController@destroy']);
                Route::put('/update-hierarchy', ['as' => 'administrator.menu.update_hierarchy',  'uses' => 'MenuController@updateHierarchy']);
                # Functions
                Route::group(['prefix' => 'function'], function(){
                    Route::get('/', ['as' => 'administrator.menu.function',  'uses' => 'FunctionController@index']);
                    Route::post('/', ['as' => 'administrator.menu.function.store',  'uses' => 'FunctionController@store']);
                    Route::get('/create', ['as' => 'administrator.menu.function.create',  'uses' => 'FunctionController@create']);
                    Route::get('/edit/{name}', ['as' => 'administrator.menu.function.edit',  'uses' => 'FunctionController@edit']);
                    Route::put('/update/{name}', ['as' => 'administrator.menu.function.update',  'uses' => 'FunctionController@update']);
                    Route::get('/list', ['as' => 'administrator.menu.function.list',  'uses' => 'FunctionController@getFunctionList']);
                });

                # Global config
                
            });
        });
        // End Administrator

        // User Start
        Route::group(['prefix' => 'user'], function(){
            Route::get('list', ['as' => 'user.list',  'uses' => 'UserController@list']);
            Route::get('create', ['as' => 'user.create',  'uses' => 'UserController@create']);
            Route::post('/', ['as' => 'user.store',  'uses' => 'UserController@store']);
            Route::get('/', ['as' => 'user.index',  'uses' => 'UserController@index']);
            Route::get('edit/{id}', ['as' => 'user.edit',  'uses' => 'UserController@edit']);
            Route::put('{id}', ['as' => 'user.update',  'uses' => 'UserController@update']);
            Route::delete('{id}', ['as' => 'user.destroy',  'uses' => 'UserController@destroy']);
        });
        // User End

        // Auditors
        // TODO: Add 'middleware' => 'needsRole:auditor'
        Route::group(['prefix' => 'auditor', ], function(){
            //TODO: Refactor AuditorController old routes
            //NOTES : Do not use 'OLD'  route. Please ask romnick
            Route::get('organizational-chart', ['as' => 'auditor.organizational_chart',  'uses' => 'AuditorController@organizationalChart']);
            Route::get('/', ['as' => 'auditor.list',  'uses' => 'AuditorController@index']); //OLD
            Route::get('/create', ['as' => 'auditor.create',  'uses' => 'AuditorController@create']);
            Route::post('/store', ['as' => 'auditor.store',  'uses' => 'AuditorController@store']);
            Route::get('/show', ['as' => 'auditor.list.show',  'uses' => 'AuditorController@show']);//OLD
            Route::get('/edit/{id}', ['as' => 'auditor.edit',  'uses' => 'AuditorController@edit']);
            Route::put('/{id}', ['as' => 'auditor.update',  'uses' => 'AuditorController@update']);
            Route::get('/lists', ['as' => 'auditor.list.lists',  'uses' => 'AuditorController@getAuditorList']); //OLD
            Route::delete('{id}', ['as' => 'auditor.destroy',  'uses' => 'AuditorController@destroy']);

            // LCM
            Route::post('{auditor_id}/lcm', ['as' => 'auditor_lcm.store', 'uses' => 'AuditorLcmController@store']);
            Route::put('{auditor_id}/lcm/{id}', ['as' => 'auditor_lcm.update', 'uses' => 'AuditorLcmController@update']);
            Route::delete('{auditor_id}/lcm/{id}', ['as' => 'auditor_lcm.destroy', 'uses' => 'AuditorLcmController@destroy']);
            Route::get('{auditor_id}/lcm/list', ['as' => 'auditor_lcm.list', 'uses' => 'AuditorLcmController@list']);

            // Training
            Route::post('{auditor_id}/training', ['as' => 'auditor_training.store', 'uses' => 'AuditorTrainingController@store']);
            Route::put('{auditor_id}/training/{id}', ['as' => 'auditor_training.update', 'uses' => 'AuditorTrainingController@update']);
            Route::delete('{auditor_id}/training/{id}', ['as' => 'auditor_training.destroy', 'uses' => 'AuditorTrainingController@destroy']);
            Route::get('{auditor_id}/training/list', ['as' => 'auditor_training.list', 'uses' => 'AuditorTrainingController@list']);

            // Procificiency
            Route::post('{auditor_id}/proficiency', ['as' => 'auditor_proficiency.store', 'uses' => 'AuditorProficiencyController@store']);
            Route::put('{auditor_id}/proficiency/{id}', ['as' => 'auditor_proficiency.update', 'uses' => 'AuditorProficiencyController@update']);
            Route::delete('{auditor_id}/proficiency/{id}', ['as' => 'auditor_proficiency.destroy', 'uses' => 'AuditorProficiencyController@destroy']);
            Route::get('{auditor_id}/proficiency/list', ['as' => 'auditor_proficiency.list', 'uses' => 'AuditorProficiencyController@list']);
            
            // Setup
            // TODO : refactor before using again
            Route::group(['prefix' => 'setup', ], function(){
                Route::group(['prefix' => 'holiday', ], function(){
                    Route::get('/', ['as' => 'auditor.setup.holiday',  'uses' => 'HolidayController@index']);
                    Route::get('/create', ['as' => 'auditor.setup.holiday.create',  'uses' => 'HolidayController@create']);
                    Route::post('/store', ['as' => 'auditor.setup.holiday.store',  'uses' => 'HolidayController@store']);
                    Route::get('/edit/{date}', ['as' => 'auditor.setup.holiday.edit',  'uses' => 'HolidayController@edit']);
                    Route::put('/update/{id}', ['as' => 'auditor.setup.holiday.update',  'uses' => 'HolidayController@update']);
                    Route::delete('/delete/{id}', ['as' => 'auditor.setup.holiday.delete',  'uses' => 'HolidayController@destroy']);
                    Route::get('/lists', ['as' => 'auditor.setup.holiday.lists',  'uses' => 'HolidayController@getHolidayList']);
                });

            });
            // End Setup
        });
        // End Auditors

        // Grading Matrix
        Route::group(['prefix' => 'auditor-grading-matrix', ], function(){
            Route::get('/', ['as' => 'auditor_grading_matrix.index',  'uses' => 'AuditorGradingMatrixController@index']);
            Route::post('/', ['as' => 'auditor_grading_matrix.store',  'uses' => 'AuditorGradingMatrixController@store']);
            Route::put('{id}', ['as' => 'auditor_grading_matrix.update',  'uses' => 'AuditorGradingMatrixController@update']);
            Route::get('list', ['as' => 'auditor_grading_matrix.list',  'uses' => 'AuditorGradingMatrixController@list']);
            Route::delete('{id}', ['as' => 'auditor_grading_matrix.destroy',  'uses' => 'AuditorGradingMatrixController@destroy']);
        });
        // Grading Matrix

        //Leave
        Route::group(['prefix' => 'leave', ], function(){
            Route::get('/', ['as' => 'leave.index',  'uses' => 'LeaveController@index']);
            Route::get('create', ['as' => 'leave.create',  'uses' => 'LeaveController@create']);
            Route::post('/', ['as' => 'leave.store',  'uses' => 'LeaveController@store']);
            Route::get('edit/{id}', ['as' => 'leave.edit',  'uses' => 'LeaveController@edit']);
            Route::put('{id}', ['as' => 'leave.update',  'uses' => 'LeaveController@update']);
            Route::delete('{id}', ['as' => 'leave.destroy',  'uses' => 'LeaveController@destroy']);
            Route::get('list', ['as' => 'leave.list',  'uses' => 'LeaveController@list']);
        });
        //Leave

        // Auditee
        // TODO: Add 'middleware' => 'needsRole:auditee'
        Route::group(['prefix' => 'auditee', ], function(){
            Route::get('/', ['as' => 'auditee',  'uses' => 'AuditeeController@index']);
            Route::get('/', ['as' => 'auditee.index',  'uses' => 'AuditeeController@index']);
            Route::get('/show', ['as' => 'auditee.show',  'uses' => 'AuditeeController@show']);
            Route::get('/create', ['as' => 'auditee.create',  'uses' => 'AuditeeController@create']);
            Route::post('/store', ['as' => 'auditee.store',  'uses' => 'AuditeeController@store']);
            Route::get('/edit/{id}', ['as' => 'auditee.edit',  'uses' => 'AuditeeController@edit']);
            Route::put('/update/{id}', ['as' => 'auditee.update',  'uses' => 'AuditeeController@update']);
            Route::get('/list', ['as' => 'auditee.list',  'uses' => 'AuditeeController@getAuditeeList']);
            Route::delete('/delete/{id}', ['as' => 'auditee.delete',  'uses' => 'AuditeeController@destroy']);
        });
        // End Auditee

        // Controls Registry
        Route::group(['prefix' => 'controls_registry', ], function(){
            Route::get('/', ['as' => 'controls_registry',  'uses' => 'ControlsRegistryController@index']);
            Route::get('/show/{id}', ['as' => 'controls_registry.show',  'uses' => 'ControlsRegistryController@show']);
            Route::get('/create', ['as' => 'controls_registry.create',  'uses' => 'ControlsRegistryController@create']);
            Route::post('/store', ['as' => 'controls_registry.store',  'uses' => 'ControlsRegistryController@store']);
            Route::get('/edit/{id}', ['as' => 'controls_registry.edit',  'uses' => 'ControlsRegistryController@edit']);
            Route::put('/update/{id}', ['as' => 'controls_registry.update',  'uses' => 'ControlsRegistryController@update']);
            Route::get('/list', ['as' => 'controls_registry.list',  'uses' => 'ControlsRegistryController@getControlsList']);
            Route::delete('/delete/{id}', ['as' => 'controls_registry.delete',  'uses' => 'ControlsRegistryController@destroy']);
            Route::put('{id}/additional_information/update', ['as' => 'controls_registry.additional_information.update',  'uses' => 'ControlsRegistryController@putUpdateAdditionalInformation']);

            // test procedures
            Route::get('/test_procedures/{control_id}', ['as' => 'controls_registry.test_procedures',  'uses' => 'ControlsRegistryController@getControlTestProcedure']);
            Route::post('/test_procedures/store', ['as' => 'controls_registry.test_procedures.store',  'uses' => 'ControlsRegistryController@postStoreControlTestProcedure']);
            Route::put('/test_procedures/update', ['as' => 'controls_registry.test_procedures.update',  'uses' => 'ControlsRegistryController@putUpdateControlTestProcedure']);
            Route::delete('/test_procedures/delete/{control_test_id}', ['as' => 'controls_registry.test_procedures.delete',  'uses' => 'ControlsRegistryController@destroyControlTestProcedure']);
            // test procedure audit types
            Route::get('/test_procedure/{control_test_id}/audit_types', ['as' => 'controls_registry.test_procedure.audit_type',  'uses' => 'ControlsRegistryController@getControlTestAuditTypes']);
            Route::post('/test_procedures/{control_test_id}/audit_types', ['as' => 'controls_registry.test_procedure.audit_type.store',  'uses' => 'ControlsRegistryController@postStoreControlTestProcedureAuditType']);
            Route::delete('/test_procedures/{control_test_id}/delete/audit_type', ['as' => 'controls_registry.test_procedure.audit_type.delete',  'uses' => 'ControlsRegistryController@destroyControlTestProcedureAuditType']);

            // COSO components
            Route::put('/coso/update', ['as' => 'controls_registry.coso.update',  'uses' => 'ControlsRegistryController@putUpdateCosoComponents']);
        });
        // End Controls Registry

        // Test procedures
        Route::group(['prefix' => 'test_procedures', ], function() {
            Route::get('/', ['as' => 'test_procedures', 'uses' => 'TestProcedureController@index']);
            Route::post('store', ['as' => 'test_procedures.store', 'uses' => 'TestProcedureController@store']);
            Route::put('update', ['as' => 'test_procedures.update', 'uses' => 'TestProcedureController@update']);
            Route::get('list', ['as' => 'test_procedures.lists', 'uses' => 'TestProcedureController@lists']);
            Route::delete('delete', ['as' => 'test_procedures.delete', 'uses' => 'TestProcedureController@destroy']);
        });
        // End Test procedures

        // Risks
        Route::group(['prefix' => 'risks', ], function(){
            Route::get('/', ['as' => 'risks',  'uses' => 'RiskController@index']);
            Route::get('/create', ['as' => 'risks.create',  'uses' => 'RiskController@create']);
            Route::post('/store', ['as' => 'risks.store',  'uses' => 'RiskController@store']);
            Route::get('/edit/{risk_id}', ['as' => 'risks.edit',  'uses' => 'RiskController@edit']);
            Route::put('/update/{risk_id}', ['as' => 'risks.update',  'uses' => 'RiskController@update']);
            Route::get('/list', ['as' => 'risks.list',  'uses' => 'RiskController@getRisksList']);
            Route::delete('/delete/{id}', ['as' => 'risks.delete',  'uses' => 'RiskController@destroy']);
            Route::put('{id}/additional_information/update', ['as' => 'risks.additional_information.update',  'uses' => 'RiskController@putUpdateAdditionalInformation']);

            // risks control
            Route::get('/{risk_id}/control', ['as' => 'risks.control',  'uses' => 'RiskController@getRiskControl']);
            Route::post('/{risk_id}/control', ['as' => 'risks.control.store',  'uses' => 'RiskController@postStoreRiskControl']);
            Route::put('/control/update/{id}', ['as' => 'risks.control.update',  'uses' => 'RiskController@putUpdateRiskControl']);
            Route::delete('/control/delete/{id}', ['as' => 'risks.control.delete',  'uses' => 'RiskController@deleteRiskControl']);
        });
        // End Risks

        // Business Processes
        Route::group(['prefix' => 'business_process', ], function(){
            Route::get('/', ['as' => 'business_process',  'uses' => 'BusinessProcessController@index']);
            Route::get('/create', ['as' => 'business_process.create',  'uses' => 'BusinessProcessController@create']);
            Route::post('/store', ['as' => 'business_process.store',  'uses' => 'BusinessProcessController@store']);
            Route::get('/edit/{bp_id}', ['as' => 'business_process.edit',  'uses' => 'BusinessProcessController@edit']);
            Route::put('/update/{bp_id}', ['as' => 'business_process.update',  'uses' => 'BusinessProcessController@update']);
            Route::get('/list', ['as' => 'business_process.list',  'uses' => 'BusinessProcessController@lists']);
            Route::get('/processes', ['as' => 'business_process.processes',  'uses' => 'BusinessProcessController@getProcesses']);
            Route::delete('/delete/{bp_id}', ['as' => 'business_process.delete',  'uses' => 'BusinessProcessController@destroy']);
            Route::put('{id}/additional_information/update', ['as' => 'business_process.additional_information.update',  'uses' => 'BusinessProcessController@putUpdateAdditionalInformation']);

            // objectives
            Route::get('objectives/{bp_id?}', ['as' => 'business_process.objectives',  'uses' => 'BusinessProcessController@getObjectives']);
            Route::get('objectives/{objective_id}/risks', ['as' => 'business_process.objective.risks',  'uses' => 'BusinessProcessController@getObjectiveRisks']);
            Route::post('{bp_id}/objective/store', ['as' => 'business_process.objective.store',  'uses' => 'BusinessProcessController@postStoreObjective']);
            Route::put('objective/update/{bp_obj_id}', ['as' => 'business_process.objective.update',  'uses' => 'BusinessProcessController@putUpdateObjective']);
            Route::delete('objective/delete/{bp_obj_id}', ['as' => 'business_process.objective.delete',  'uses' => 'BusinessProcessController@destroyObjective']);

            // steps
            Route::get('objective/{objective_id}/steps', ['as' => 'business_process.steps',  'uses' => 'BusinessProcessController@getSteps']);
            Route::post('objective/{objective_id}/steps', ['as' => 'business_process.steps.store',  'uses' => 'BusinessProcessController@postStoreStep']);
            Route::put('objective/steps/update/{bp_step_id}', ['as' => 'business_process.steps.update',  'uses' => 'BusinessProcessController@putUpdateStep']);
            Route::delete('steps/delete/{bp_step_id}', ['as' => 'business_process.steps.delete',  'uses' => 'BusinessProcessController@destroyStep']);
            Route::put('step/{id}/additional_information/update', ['as' => 'business_process.steps.additional_information.update',  'uses' => 'BusinessProcessController@putUpdateStepAdditionalInformation']);

            // steps risk
            Route::get('{bp_id}/steps/{bp_step_id}', ['as' => 'business_process.steps.risks',  'uses' => 'BusinessProcessController@getStepRisks']);
            Route::get('/steps/{bp_step_id}/list', ['as' => 'business_process.steps.risks.list',  'uses' => 'BusinessProcessController@getStepRiskList']);
            Route::post('/steps/{bp_step_id}/store', ['as' => 'business_process.steps.risks.store',  'uses' => 'BusinessProcessController@postStoreStepRisk']);
            Route::delete('/steps/{bp_step_id}/risks/{risk_id}/delete', ['as' => 'business_process.steps.risks.delete',  'uses' => 'BusinessProcessController@destroyStepRisk']);

            // test procedures
            Route::get('{bp_id}/test_proc/mandays', ['as' => 'business_process.controls.mandays',  'uses' => 'BusinessProcessController@getBusinessProcessTestProcMandays']);
        });
        // End Business Processes

        // Auditable Entity
        Route::group(['prefix' => 'auditable_entities', ], function(){
            // Master entity
            Route::group(['prefix' => 'master', ], function(){
                Route::get('/', ['as' => 'auditable_entities.master',  'uses' => 'AuditableEntityMasterController@index']);
                Route::post('/', ['as' => 'auditable_entities.master.store',  'uses' => 'AuditableEntityMasterController@store']);
                Route::put('update', ['as' => 'auditable_entities.master.update',  'uses' => 'AuditableEntityMasterController@update']);
                Route::get('lists', ['as' => 'auditable_entities.master.lists',  'uses' => 'AuditableEntityMasterController@lists']);
                Route::get('edit/{id}', ['as' => 'auditable_entities.master.edit',  'uses' => 'AuditableEntityMasterController@edit']);
                Route::put('business_processes/{main_bp_id}/main/update', ['as' => 'auditable_entities.master.bp.main.update',  'uses' => 'AuditableEntityMasterController@putUpdateMainBp']);
                Route::get('{entity_id}/business_processes/main', ['as' => 'auditable_entities.master.bp.main',  'uses' => 'AuditableEntityController@getEntityMainBp']);
                Route::post('{entity_id}/business_processes/main/store', ['as' => 'auditable_entities.bp.main.store',  'uses' => 'AuditableEntityMasterController@postStoreEntityMainBp']);
                Route::delete('business_process/main/delete', ['as' => 'auditable_entities.bp.main.delete',  'uses' => 'AuditableEntityMasterController@deleteMainBusinessProcess']);
                Route::post('business_processes/{main_bp_id}/sub/store', ['as' => 'auditable_entities.bp.sub.store',  'uses' => 'AuditableEntityMasterController@postStoreSubProcess']);
                Route::delete('business_process/sub/delete', ['as' => 'auditable_entities.bp.sub.delete',  'uses' => 'AuditableEntityMasterController@deleteSubBusinessProcess']);
                Route::get('custom_measures/{main_bp}/{entity_id}', ['as' => 'auditable_entities.master.custom_measure',  'uses' => 'AuditableEntityMasterController@getCustomMeasure']);
                Route::get('custom_measures/{main_bp}/{entity_id}/percentile', ['as' => 'auditable_entities.master.custom_measure.percentile',  'uses' => 'AuditableEntityMasterController@getCustomMeasurePercentile']);
                Route::delete('delete/{id}', ['as' => 'auditable_entities.master.delete',  'uses' => 'AuditableEntityMasterController@destroy']);
                Route::get('create', ['as' => 'auditable_entities.master.create',  'uses' => 'AuditableEntityController@create']);
            });

            // Actual
            Route::get('/', ['as' => 'auditable_entities',  'uses' => 'AuditableEntityController@index']);
            Route::get('/create', ['as' => 'auditable_entities.create',  'uses' => 'AuditableEntityController@create']);
            Route::post('/store', ['as' => 'auditable_entities.store',  'uses' => 'AuditableEntityController@store']);
            Route::get('/edit/{id}', ['as' => 'auditable_entities.edit',  'uses' => 'AuditableEntityController@edit']);
            Route::put('/update/{id}', ['as' => 'auditable_entities.update',  'uses' => 'AuditableEntityController@update']);
            Route::delete('/delete/{id}', ['as' => 'auditable_entities.delete',  'uses' => 'AuditableEntityController@destroy']);
            Route::get('/list', ['as' => 'auditable_entities.list',  'uses' => 'AuditableEntityController@getAuditableEntityList']);
            Route::get('/lists', ['as' => 'auditable_entities.lists',  'uses' => 'AuditableEntityController@lists']);
            Route::get('/parents', ['as' => 'auditable_entities.parents',  'uses' => 'AuditableEntityController@getAuditableEntityParents']);
            Route::get('/parents/search', ['as' => 'auditable_entities.parents.search',  'uses' => 'AuditableEntityController@getParentEntities']);
            Route::put('{id}/additional_information/update', ['as' => 'auditable_entities.additional_information.update',  'uses' => 'AuditableEntityController@putUpdateAdditionalInformation']);
            Route::get('{id}/mbp/{mbp_id}/budgeted-mandays',  ['as' => 'auditable_entities.budgeted_mandays',  'uses' => 'AuditableEntityController@budgetedMandays']);

            // Business Processes
            Route::get('{auditable_ent_id}/business_process', ['as' => 'auditable_entities.bp',  'uses' => 'AuditableEntityController@getAuditableEntityBp']);
            Route::post('{auditable_ent_id}/business_process/store', ['as' => 'auditable_entities.business_process.store',  'uses' => 'AuditableEntityController@storeAuditableEntityBp']);
            Route::delete('{auditable_ent_id}/business_process/delete', ['as' => 'auditable_entities.business_process.delete',  'uses' => 'AuditableEntityController@deleteAuditableEntityBp']);
            Route::put('business_process/main/{main_bp_id}/mandays/update', ['as' => 'auditable_entities.bp.main.mandays.update',  'uses' => 'MasterEntityController@putUpdateMainBpMandays']);
            #TODO: Remove custom measures routes - not in use
            // Custom measures
            Route::get('{auditable_ent_id}/business_process/{bp_id}/custom_measures/percentile', ['as' => 'auditable_entities.business_process.cm.percentile',  'uses' => 'AuditableEntityController@getBusinessProcessPercentile']);
            Route::put('{auditable_ent_id}/business_process/custom_measures/update', ['as' => 'auditable_entities.business_process.cm.update',  'uses' => 'AuditableEntityController@putUpdateAeBpCustomMeasures']);
            Route::get('{entity_id}/business_process/{bp_id}/custom_measures', ['as' => 'auditable_entities.business_process.cm',  'uses' => 'AuditableEntityController@getBusinessProcessCustomMeasure']);

            // Inquiry
            Route::get('/inquiry', ['as' => 'auditable_entities.inquiry',  'uses' => 'AuditableEntityController@getInquiry']);
            Route::get('/inquiry/tree', ['as' => 'auditable_entities.inquiry.tree',  'uses' => 'AuditableEntityController@getInquiryTree']);
            Route::post('/inquiry/{tab}', ['as' => 'auditable_entities.inquiry.ajax',  'uses' => 'AuditableEntityController@postAjaxInquiry']);
            Route::get('/inquiry/filter', ['as' => 'auditable_entities.inquiry.filter',  'uses' => 'AuditableEntityController@getInquiryFilter']);

            // templates
            Route::post('template/apply', ['as' => 'auditable_entities.template.apply',  'uses' => 'AuditableEntityController@postApplyTemplate']);

        });
        // End Auditable Entity
        
        // Auditable Entity Master Start
        Route::group(['prefix' => 'ae' ], function(){
            Route::get('list', ['as' => 'ae.list',  'uses' => 'AuditableEntityMasterController@list']);
            Route::get('/', ['as' => 'ae.index',  'uses' => 'AuditableEntityMasterController@index']);
            Route::get('{id}', ['as' => 'ae.show',  'uses' => 'AuditableEntityMasterController@show']);
        });
        // Auditable Entity Master ENd

        // Main Business Proccess Master Start
        Route::group(['prefix' => 'ae/{ae_id}/mbp' ], function(){
            Route::get('/', ['as' => 'ae_mbp.index',  'uses' => 'MainBusinessProccessMasterController@index']);
            Route::get('{id}', ['as' => 'ae_mbp.show',  'uses' => 'MainBusinessProccessMasterController@show']);
        });
        // Main Business Proccess Master End

        // Sub Business Proccess Master Start
        Route::group(['prefix' => 'ae-mbp/{mbpa_id}/sbp' ], function(){
            Route::get('/', ['as' => 'ae_mbp_sbp.index',  'uses' => 'SubBusinessProccessMasterController@index']);
        });
        // Sub Business Proccess Master End

        // Auditable Entity Actual Start
        Route::group(['prefix' => 'aea' ], function(){
            Route::get('list', ['as' => 'aea.list',  'uses' => 'AuditableEntityActualController@list']);
            Route::get('/', ['as' => 'aea.index',  'uses' => 'AuditableEntityActualController@index']);
            Route::get('{id}', ['as' => 'aea.show',  'uses' => 'AuditableEntityActualController@show']);
        });
        // Auditable Entity Actual ENd

        // Main Business Proccess Actual Start
        Route::group(['prefix' => 'aea/{ae_id}/mbpa' ], function(){
            Route::get('/', ['as' => 'aea_mbpa.index',  'uses' => 'MainBusinessProccessActualController@index']);
            Route::get('{id}', ['as' => 'aea_mbpa.show',  'uses' => 'MainBusinessProccessActualController@show']);
        });
        // Main Business Proccess Actual End

        // Sub Business Proccess Actual Start
        Route::group(['prefix' => 'aea-mbpa/{mbpa_id}/sbp' ], function(){
            Route::get('/', ['as' => 'aea_mbpa_sbp.index',  'uses' => 'SubBusinessProccessActualController@index']);
        });
        // Sub Business Proccess Actual End

        // Risk Assessment Scale
        Route::group(['prefix' => 'risk_assessment', ], function(){
            Route::get('/', ['as' => 'risk_assessment',  'uses' => 'RiskAssessmentScaleController@index']);
            Route::get('/create', ['as' => 'risk_assessment.create',  'uses' => 'RiskAssessmentScaleController@create']);
            Route::post('/', ['as' => 'risk_assessment.store',  'uses' => 'RiskAssessmentScaleController@store']);
            Route::get('/edit/{ae_id}/{bp_id}', ['as' => 'risk_assessment.edit',  'uses' => 'RiskAssessmentScaleController@edit']);
            Route::put('{id}', ['as' => 'risk_assessment.update',  'uses' => 'RiskAssessmentScaleController@update']);
            Route::delete('/delete/{ae_id}/{bp_id}', ['as' => 'risk_assessment.delete',  'uses' => 'RiskAssessmentScaleController@destroy']);
            Route::delete('/delete/{ae_id}/{bp_id}/risk/{risk_id}', ['as' => 'risk_assessment.delete',  'uses' => 'RiskAssessmentScaleController@deleteRisk']);
            Route::get('/list', ['as' => 'risk_assessment.list',  'uses' => 'RiskAssessmentScaleController@list']);
            Route::get('/risks/list/{ae_id}/{bp_id}', ['as' => 'risk_assessment.risk.list',  'uses' => 'RiskAssessmentScaleController@getRiskList']);
            Route::put('/risks/update/{ae_id}/{bp_id}/{risk_id}', ['as' => 'risk_assessment.risk.update',  'uses' => 'RiskAssessmentScaleController@putUpdateRisk']);
        });
        // End Risk Assessment Scale

        // Matrix Definition
        Route::group(['prefix' => 'matrix_definition', ], function(){
            Route::get('/', ['as' => 'matrix_definition',  'uses' => 'MatrixDefinitionController@index']);
            Route::get('/create', ['as' => 'matrix_definition.create',  'uses' => 'MatrixDefinitionController@create']);
            Route::post('/', ['as' => 'matrix_definition.store',  'uses' => 'MatrixDefinitionController@store']);
            Route::get('edit/{id}', ['as' => 'matrix_definition.edit',  'uses' => 'MatrixDefinitionController@edit']);
            Route::put('/update/{id}', ['as' => 'matrix_definition.update',  'uses' => 'MatrixDefinitionController@update']);
            Route::put('/update/{id}/details', ['as' => 'matrix_definition.details.update',  'uses' => 'MatrixDefinitionController@putUpdateDetails']);
            Route::get('/list', ['as' => 'matrix_definition.list',  'uses' => 'MatrixDefinitionController@list']);
            Route::delete('/delete/{id}', ['as' => 'matrix_definition.delete',  'uses' => 'MatrixDefinitionController@destroy']);
        });
        // End Matrix Definition

        // Company
        Route::group(['prefix' => 'company_profile', ], function(){
            Route::get('/lists', ['as' => 'company.list',  'uses' => 'CompanyController@lists']);
            Route::get('/{company_code?}', ['as' => 'company',  'uses' => 'CompanyController@index']);
            Route::post('/', ['as' => 'company.store',  'uses' => 'CompanyController@store']);
            Route::get('/create', ['as' => 'company.create',  'uses' => 'CompanyController@create']);
            Route::get('/details/{code}', ['as' => 'company.details',  'uses' => 'CompanyController@details']);
            Route::put('/details/{code}', ['as' => 'company.details.update',  'uses' => 'CompanyController@putUpdateDetails']);
            // Route::put('/update/{id}', ['as' => 'company.update',  'uses' => 'CompanyController@update']);
            Route::delete('/delete/{id}', ['as' => 'company.delete',  'uses' => 'CompanyController@destroy']);
            Route::get('{code}/branches', ['as' => 'company.branches',  'uses' => 'CompanyController@getBranches']);
            Route::post('{id}/branch/store', ['as' => 'company.branch.store',  'uses' => 'CompanyController@postStoreBranch']);
            Route::put('branch/update', ['as' => 'company.branch.update',  'uses' => 'CompanyController@putUpdateBranch']);
            Route::delete('{id}/branch/delete', ['as' => 'company.branch.delete',  'uses' => 'CompanyController@deleteBranch']);
        });
        // End Company

        // Questionnaire
        Route::group(['prefix' => 'questionnaire', ], function(){
            Route::get('/', ['as' => 'questionnaire.index',  'uses' => 'QuestionnaireController@index']);
            Route::post('/', ['as' => 'questionnaire.store',  'uses' => 'QuestionnaireController@store']);
            Route::get('/create', ['as' => 'questionnaire.create',  'uses' => 'QuestionnaireController@create']);
            Route::get('edit/{name}', ['as' => 'questionnaire.edit',  'uses' => 'QuestionnaireController@edit']);
            Route::put('/update/{id}', ['as' => 'questionnaire.update',  'uses' => 'QuestionnaireController@update']);
            Route::delete('/delete/{id}', ['as' => 'questionnaire.delete',  'uses' => 'QuestionnaireController@destroy']);
            Route::delete('{questionnaire_id}/delete/question/{seq}', ['as' => 'questionnaire.question.delete',  'uses' => 'QuestionnaireController@deleteQuestion']);
            Route::get('/list', ['as' => 'questionnaire.list',  'uses' => 'QuestionnaireController@list']);
        });
        // End Questionnaire

        // Enterprise Risk
        Route::group(['prefix' => 'enterprise_risk', ], function(){
            Route::get('/', ['as' => 'enterprise_risk',  'uses' => 'EnterpriseRiskController@index']);
            Route::post('/', ['as' => 'enterprise_risk.store',  'uses' => 'EnterpriseRiskController@store']);
            Route::get('edit/{id}', ['as' => 'enterprise_risk.edit',  'uses' => 'EnterpriseRiskController@edit']);
            Route::get('/list', ['as' => 'enterprise_risk.list',  'uses' => 'EnterpriseRiskController@lists']);
            Route::get('/available/entities', ['as' => 'enterprise_risk.available.entities',  'uses' => 'EnterpriseRiskController@getAvailableEntity']);
            Route::delete('delete/{id}', ['as' => 'enterprise_risk.delete',  'uses' => 'EnterpriseRiskController@destroy']);

            // details
            Route::get('{id}/detail/list', ['as' => 'enterprise_risk.details.list',  'uses' => 'EnterpriseRiskController@getRisklist']);
            Route::post('{id}/details/store', ['as' => 'enterprise_risk.details.store',  'uses' => 'EnterpriseRiskController@postStoreDetails']);
            Route::put('details/{id}/update', ['as' => 'enterprise_risk.details.update',  'uses' => 'EnterpriseRiskController@putUpdateDetails']);
            Route::delete('details/delete/{id}', ['as' => 'enterprise_risk.details.delete',  'uses' => 'EnterpriseRiskController@deleteDetails']);
        });
        // End Enterprise Risk

        // Value Set
        Route::group(['prefix' => 'value_set', ], function(){
            Route::get('/', ['as' => 'value_set',  'uses' => 'ValueSetController@index']);
            Route::post('/', ['as' => 'value_set.store',  'uses' => 'ValueSetController@store']);
            Route::get('create', ['as' => 'value_set.create',  'uses' => 'ValueSetController@create']);
            Route::get('edit/{name}', ['as' => 'value_set.edit',  'uses' => 'ValueSetController@edit']);
            Route::put('update/{id}', ['as' => 'value_set.update',  'uses' => 'ValueSetController@update']);
            Route::post('test_query', ['as' => 'value_set.test_query',  'uses' => 'ValueSetController@postTestQuery']);
            Route::get('run_query', ['as' => 'value_set.run_query',  'uses' => 'ValueSetController@getRunQuery']);
            Route::get('/list', ['as' => 'value_set.list',  'uses' => 'ValueSetController@list']);
            Route::delete('delete/{id}', ['as' => 'value_set.delete',  'uses' => 'ValueSetController@destroy']);
        });
        // End Value Set

        // Additional Information
        Route::group(['prefix' => 'additional_information', ], function(){
            Route::get('/', ['as' => 'additional_information.index',  'uses' => 'AdditionalInformationController@index']);
            Route::get('create', ['as' => 'additional_information.create',  'uses' => 'AdditionalInformationController@create']);
            Route::post('/', ['as' => 'additional_information.store',  'uses' => 'AdditionalInformationController@store']);
            Route::get('show/{id}', ['as' => 'additional_information.show',  'uses' => 'AdditionalInformationController@show']);
            Route::get('edit/{id}', ['as' => 'additional_information.edit',  'uses' => 'AdditionalInformationController@edit']);
            Route::put('{id}', ['as' => 'additional_information.update',  'uses' => 'AdditionalInformationController@update']);
            Route::delete('{id}', ['as' => 'additional_information.destroy',  'uses' => 'AdditionalInformationController@destroy']);
            Route::get('list', ['as' => 'additional_information.list',  'uses' => 'AdditionalInformationController@list']);
            Route::get('table/{id}/{table}', ['as' => 'additional_information.table',  'uses' => 'AdditionalInformationController@getTableDetails']);
            
            // Additional Information Details
            Route::post('{aiId}/details', ['as' => 'additional_information_details.store', 'uses' => 'AdditionalInformationDetailController@store']);
            Route::get('{aiId}/details/list', ['as' => 'additional_information_details.list', 'uses' => 'AdditionalInformationDetailController@list']);
            Route::put('{aiId}/details/{id}', ['as' => 'additional_information_details.update', 'uses' => 'AdditionalInformationDetailController@update']);
            Route::delete('{aiId}/details/{id}', ['as' => 'additional_information_details.destroy',  'uses' => 'AdditionalInformationDetailController@destroy']);
            
        });
        // End Additional Information

        // Approval Set up Head
        Route::group(['prefix' => 'approval-setup', ], function(){
            Route::get('list', ['as' => 'approval_setup.list', 'uses' => 'ApprovalSetupMastersController@list']);
            Route::get('/', ['as' => 'approval_setup.index', 'uses' => 'ApprovalSetupMastersController@index']);
            Route::post('/', ['as' => 'approval_setup.store', 'uses' => 'ApprovalSetupMastersController@store']);
            Route::get('{id}', ['as' => 'approval_setup.show', 'uses' => 'ApprovalSetupMastersController@show']);
            Route::put('{id}', ['as' => 'approval_setup.update', 'uses' => 'ApprovalSetupMastersController@update']);
            Route::delete('{id}', ['as' => 'approval_setup.destroy', 'uses' => 'ApprovalSetupMastersController@destroy']);
        });
        // Approval Set up Head

        // Approval Set up Details
        Route::group(['prefix' => 'approval-setup/{head_id}/detail', ], function(){
            Route::get('list', ['as' => 'approval_setup_detail.list', 'uses' => 'ApprovalSetupDetailsController@list']);
            Route::get('/', ['as' => 'approval_setup_detail.index', 'uses' => 'ApprovalSetupDetailsController@index']);
            Route::post('/', ['as' => 'approval_setup_detail.store', 'uses' => 'ApprovalSetupDetailsController@store']);
            Route::get('{id}', ['as' => 'approval_setup_detail.show', 'uses' => 'ApprovalSetupDetailsController@show']);
            Route::put('{id}', ['as' => 'approval_setup_detail.update', 'uses' => 'ApprovalSetupDetailsController@update']);
            Route::delete('{id}', ['as' => 'approval_setup_detail.destroy', 'uses' => 'ApprovalSetupDetailsController@destroy']);
        });
        // Approval Set up Details

        // Approval From Statuses
        Route::group(['prefix' => 'approval-setup-detail/{detail_id}/status-from', ], function(){
            Route::get('list', ['as' => 'approval_setup_detail_status_form.list', 'uses' => 'ApprovalFromStatusesController@list']);
            Route::get('/', ['as' => 'approval_setup_detail_status_form.index', 'uses' => 'ApprovalFromStatusesController@index']);
            Route::post('/', ['as' => 'approval_setup_detail_status_form.store', 'uses' => 'ApprovalFromStatusesController@store']);
            Route::get('{id}', ['as' => 'approval_setup_detail_status_form.show', 'uses' => 'ApprovalFromStatusesController@show']);
            Route::put('{id}', ['as' => 'approval_setup_detail_status_form.update', 'uses' => 'ApprovalFromStatusesController@update']);
            Route::delete('{id}', ['as' => 'approval_setup_detail_status_form.destroy', 'uses' => 'ApprovalFromStatusesController@destroy']);
        });
        // Approval From Statuses

        // Approval To Statuses
        Route::group(['prefix' => 'approval-setup-detail/{detail_id}/status-to', ], function(){
            Route::get('list', ['as' => 'approval_setup_detail_status_to.list', 'uses' => 'ApprovalToStatusesController@list']);
            Route::get('/', ['as' => 'approval_setup_detail_status_to.index', 'uses' => 'ApprovalToStatusesController@index']);
            Route::post('/', ['as' => 'approval_setup_detail_status_to.store', 'uses' => 'ApprovalToStatusesController@store']);
            Route::get('{id}', ['as' => 'approval_setup_detail_status_to.show', 'uses' => 'ApprovalToStatusesController@show']);
            Route::put('{id}', ['as' => 'approval_setup_detail_status_to.update', 'uses' => 'ApprovalToStatusesController@update']);
            Route::delete('{id}', ['as' => 'approval_setup_detail_status_to.destroy', 'uses' => 'ApprovalToStatusesController@destroy']);
        });
        // Approval To Statuses


        // Approval
        Route::group(['prefix' => 'approval', ], function(){
            Route::get('/', ['as' => 'approval.index', 'uses' => 'ApprovalController@index']);
            Route::post('/', ['as' => 'approval.store', 'uses' => 'ApprovalController@store']);
            Route::put('{id}', ['as' => 'approval.update', 'uses' => 'ApprovalController@update']);
            Route::get('approval_info', ['as' => 'approval.approval_info', 'uses' => 'ApprovalController@approval_info']);
        });
        // Approval

        Route::get('engagement-planning', ['as' => 'engagement_planning',  'uses' => 'EngamentPlanningController@index']);
        Route::get('engagement-planning-inquiry', ['as' => 'engagement_planning.inquiry',  'uses' => 'EngamentPlanningController@inquiry']);
        // Route::get('engagement-planning-inquiry', ['as' => 'engagement_planning.inquiry',  'uses' => 'ProjectEngagementUpdateController@index']);
        Route::get('planning', ['as' => 'project_planning',  'uses' => 'EngamentPlanningController@projectPlanning']);
        Route::get('fieldwork', ['as' => 'fieldwork_review',  'uses' => 'FieldWorkController@index']);
        Route::get('fieldwork-review', ['as' => 'fieldwork_review',  'uses' => 'FieldWorkReviewController@index']);

        // Plan Start
        Route::group(['prefix' => 'plan', ], function(){
            Route::get('/', ['as' => 'plan.index',  'uses' => 'PlanController@index']);
            Route::get('create', ['as' => 'plan.create',  'uses' => 'PlanController@create']);
            Route::get('process', ['as' => 'plan.process',  'uses' => 'PlanController@process']);
            Route::get('inquiry', ['as' => 'plan.inquiry',  'uses' => 'PlanController@inquiry']);
            Route::post('/', ['as' => 'plan.store',  'uses' => 'PlanController@store']);
            Route::put('{planId}', ['as' => 'plan.update',  'uses' => 'PlanController@update']);
            Route::get('{planId}', ['as' => 'plan.show',  'uses' => 'PlanController@show']);

        });
        // Plan end

        // Plan Guide Start
        Route::group(['prefix' => 'plan-guide', ], function(){
            Route::get('/', ['as' => 'plan_guide.index',  'uses' => 'PlanGuideController@index']);
            Route::get('search-data', ['as' => 'plan_guide.search_data',  'uses' => 'PlanGuideController@searchData']);
            Route::get('project-list', ['as' => 'plan_guide.project_list',  'uses' => 'PlanGuideController@projectList']);
            Route::put('update-cart', ['as' => 'plan_guide.update_cart',  'uses' => 'PlanGuideController@updateCart']);
            Route::put('check-plan', ['as' => 'plan_guide.check_plan',  'uses' => 'PlanGuideController@checkPlan']);
            Route::post('add-to-project', ['as' => 'plan_guide.add_to_project',  'uses' => 'PlanGuideController@addToProject']);
            Route::get('pasv', ['as' => 'plan_guide.pasv',  'uses' => 'PlanGuideController@pasv']);
            Route::get('cart-data', ['as' => 'plan_guide.cart_data',  'uses' => 'PlanGuideController@cartData']);
            Route::get('test', ['as' => 'plan_guide.test',  'uses' => 'PlanGuideController@test']);
        });
        // Plan Guide Start

        // Plan Auditor Start
        Route::group(['prefix' => 'plan/{planId}/auditor', ], function(){
            Route::put('annual-mandays', ['as' => 'plan_auditor.annual_mandays',  'uses' => 'PlanAuditorController@annualMandays']);
            Route::get('list', ['as' => 'plan_auditor.list',  'uses' => 'PlanAuditorController@list']);
            Route::get('/', ['as' => 'plan_auditor',  'uses' => 'PlanAuditorController@index']);
            Route::post('/', ['as' => 'plan_auditor.store',  'uses' => 'PlanAuditorController@store']);
            Route::get('{id}', ['as' => 'plan_auditor.show',  'uses' => 'PlanAuditorController@show']);
            Route::get('{id}/mandays-breakdown', ['as' => 'plan_auditor.mandays_breakdown',  'uses' => 'PlanAuditorController@mandaysBreakdown']);
            Route::put('{id}', ['as' => 'plan_auditor.update',  'uses' => 'PlanAuditorController@update']);
            Route::delete('{id}', ['as' => 'plan_auditor.destroy',  'uses' => 'PlanAuditorController@destroy']);
        });
        // Plan Auditor End

        // Plan Group Start
        Route::group(['prefix' => 'plan/{plan_id}/group', ], function(){
            Route::get('list', ['as' => 'plan_group.list',  'uses' => 'PlanGroupsController@list']);
            Route::get('/', ['as' => 'plan_group',  'uses' => 'PlanGroupsController@index']);
            Route::post('/', ['as' => 'plan_group.store',  'uses' => 'PlanGroupsController@store']);
            Route::get('{id}', ['as' => 'plan_group.show',  'uses' => 'PlanGroupsController@show']);
            Route::put('{id}', ['as' => 'plan_group.update',  'uses' => 'PlanGroupsController@update']);
            Route::delete('{id}', ['as' => 'plan_group.destroy',  'uses' => 'PlanGroupsController@destroy']);
        });
        // Plan Group End

        // Plan Group Auditor Start
        Route::group(['prefix' => 'plan-group/{group_id}/auditor', ], function(){
            Route::get('list', ['as' => 'plan_group_auditor.list',  'uses' => 'PlanGroupAuditorsController@list']);
            Route::get('/', ['as' => 'plan_group_auditor',  'uses' => 'PlanGroupAuditorsController@index']);
            Route::post('/', ['as' => 'plan_group_auditor.store',  'uses' => 'PlanGroupAuditorsController@store']);
            Route::get('{id}', ['as' => 'plan_group_auditor.show',  'uses' => 'PlanGroupAuditorsController@show']);
            Route::put('{id}', ['as' => 'plan_group_auditor.update',  'uses' => 'PlanGroupAuditorsController@update']);
            Route::delete('{id}', ['as' => 'plan_group_auditor.destroy',  'uses' => 'PlanGroupAuditorsController@destroy']);
        });
        // Plan Group Auditor End

        // Plan Projects Start
        Route::group(['prefix' => 'plan-group/{plan_id}/project', ], function(){
            Route::get('/', ['as' => 'plan_group_project',  'uses' => 'PlanProjectController@index']);
            Route::get('list', ['as' => 'plan_group_project.list',  'uses' => 'PlanProjectController@list']);
            Route::post('/', ['as' => 'plan_group_project.store',  'uses' => 'PlanProjectController@store']);
            Route::get('{id}', ['as' => 'plan_group_project.show',  'uses' => 'PlanProjectController@show']);
            Route::put('{id}', ['as' => 'plan_group_project.update',  'uses' => 'PlanProjectController@update']);
            Route::delete('{id}', ['as' => 'plan_group_project.destroy',  'uses' => 'PlanProjectController@destroy']);
        });
        // Plan Projects End

        // Plan Project Auditor Start
        Route::group(['prefix' => 'plan-project/{projectId}/auditor', ], function(){
            Route::get('/', ['as' => 'plan_project_auditor',  'uses' => 'PlanProjectAuditorController@index']);
            Route::get('list', ['as' => 'plan_project_auditor.list',  'uses' => 'PlanProjectAuditorController@list']);
            Route::post('/', ['as' => 'plan_project_auditor.store',  'uses' => 'PlanProjectAuditorController@store']);
            Route::get('{id}', ['as' => 'plan_project_auditor.show',  'uses' => 'PlanProjectAuditorController@show']);
            Route::put('{id}', ['as' => 'plan_project_auditor.update',  'uses' => 'PlanProjectAuditorController@update']);
            Route::delete('{id}', ['as' => 'plan_project_auditor.destroy',  'uses' => 'PlanProjectAuditorController@destroy']);
        });
        // Plan Project Auditor End

        // Plan Project Scope Start
        Route::group(['prefix' => 'plan-project/{projectId}/scope', ], function(){
            Route::get('/', ['as' => 'plan_project_scope',  'uses' => 'PlanProjectScopeController@index']);
            Route::get('list', ['as' => 'plan_project_scope.list',  'uses' => 'PlanProjectScopeController@list']);
            Route::post('/', ['as' => 'plan_project_scope.store',  'uses' => 'PlanProjectScopeController@store']);
            Route::get('{id}', ['as' => 'plan_project_scope.show',  'uses' => 'PlanProjectScopeController@show']);
            Route::put('{id}', ['as' => 'plan_project_scope.update',  'uses' => 'PlanProjectScopeController@update']);
            Route::delete('{id}', ['as' => 'plan_project_scope.destroy',  'uses' => 'PlanProjectScopeController@destroy']);
            Route::get('{id}/suggested-budget', ['as' => 'project_scope.suggested_budget', 'uses' => 'PlanProjectScopeController@suggestedBudget']);
        });
        // Plan Project Scope End

        // Projects Start
        Route::group(['prefix' => 'project', ], function(){
            Route::post('cfaap', ['as' => 'project.cfaap',  'uses' => 'ProjectController@cfaap']);
            Route::get('list', ['as' => 'project.list',  'uses' => 'ProjectController@list']);
            Route::post('/', ['as' => 'project.store',  'uses' => 'ProjectController@store']);
            Route::get('{id}', ['as' => 'project.show',  'uses' => 'ProjectController@show']);
            Route::put('{id}', ['as' => 'project.update',  'uses' => 'ProjectController@update']);
            Route::delete('{id}', ['as' => 'project.destroy',  'uses' => 'ProjectController@destroy']);
        });
        // Projects end

        // Plan Project Scope Auditor Start
        Route::group(['prefix' => 'plan-project-scope/{scopeId}/auditor', ], function(){
            Route::get('/', ['as' => 'plan_project_scope_auditor.index',  'uses' => 'PlanProjectScopeAuditorController@index']);
            Route::get('list', ['as' => 'plan_project_scope_auditor.list',  'uses' => 'PlanProjectScopeAuditorController@list']);
            Route::post('/', ['as' => 'plan_project_scope_auditor.store',  'uses' => 'PlanProjectScopeAuditorController@store']);
            Route::get('{id}', ['as' => 'plan_project_scope_auditor.show',  'uses' => 'PlanProjectScopeAuditorController@show']);
            Route::put('{id}', ['as' => 'plan_project_scope_auditor.update',  'uses' => 'PlanProjectScopeAuditorController@update']);
            Route::delete('{id}', ['as' => 'plan_project_scope_auditor.destroy',  'uses' => 'PlanProjectScopeAuditorController@destroy']);
        });
        // Plan Project Scope Auditor End

        // Project Auditor Start
        Route::group(['prefix' => 'project/{projectId}/auditor', ], function(){
            Route::get('/', ['as' => 'project_auditor',  'uses' => 'ProjectAuditorController@index']);
            Route::get('list', ['as' => 'project_auditor.list',  'uses' => 'ProjectAuditorController@list']);
            Route::post('/', ['as' => 'project_auditor.store',  'uses' => 'ProjectAuditorController@store']);
            Route::get('{id}', ['as' => 'project_auditor.show',  'uses' => 'ProjectAuditorController@show']);
            Route::put('{id}', ['as' => 'project_auditor.update',  'uses' => 'ProjectAuditorController@update']);
            Route::delete('{id}', ['as' => 'project_auditor.destroy',  'uses' => 'ProjectAuditorController@destroy']);
        });
        // Project Auditor End

        // Project Scope Start
        Route::group(['prefix' => 'project/{projectId}/scope', ], function(){
            Route::get('/', ['as' => 'project_scope',  'uses' => 'ProjectScopeController@index']);
            Route::get('list', ['as' => 'project_scope.list',  'uses' => 'ProjectScopeController@list']);
            Route::post('/', ['as' => 'project_scope.store',  'uses' => 'ProjectScopeController@store']);
            Route::get('{id}', ['as' => 'project_scope.show',  'uses' => 'ProjectScopeController@show']);
            Route::put('{id}', ['as' => 'project_scope.update',  'uses' => 'ProjectScopeController@update']);
            Route::delete('{id}', ['as' => 'project_scope.destroy',  'uses' => 'ProjectScopeController@destroy']);
        });
        // Project Scope End

        // Project Scope Auditor Start
        Route::group(['prefix' => 'project-scope/{scopeId}/auditor', ], function(){
            Route::get('/', ['as' => 'project_scope_auditor',  'uses' => 'ProjectScopeAuditorController@index']);
            Route::get('list', ['as' => 'project_scope_auditor.list',  'uses' => 'ProjectScopeAuditorController@list']);
            Route::post('/', ['as' => 'project_scope_auditor.store',  'uses' => 'ProjectScopeAuditorController@store']);
            Route::get('{id}', ['as' => 'project_scope_auditor.show',  'uses' => 'ProjectScopeAuditorController@show']);
            Route::put('{id}', ['as' => 'project_scope_auditor.update',  'uses' => 'ProjectScopeAuditorController@update']);
            Route::delete('{id}', ['as' => 'project_scope_auditor.destroy',  'uses' => 'ProjectScopeAuditorController@destroy']);
        });
        // Project Scope Auditor End

        // Project Scope Freeze Start
        Route::group(['prefix' => 'project-scope/{scope_id}/freeze', ], function(){
            Route::get('/', ['as' => 'project_scope_freeze',  'uses' => 'ProjectScopeFreezesController@index']);
            Route::get('list', ['as' => 'project_scope_freeze.list',  'uses' => 'ProjectScopeFreezesController@list']);
            Route::post('/', ['as' => 'project_scope_freeze.store',  'uses' => 'ProjectScopeFreezesController@store']);
            Route::get('{id}', ['as' => 'project_scope_freeze.show',  'uses' => 'ProjectScopeFreezesController@show']);
            Route::put('{id}', ['as' => 'project_scope_freeze.update',  'uses' => 'ProjectScopeFreezesController@update']);
            Route::delete('{id}', ['as' => 'project_scope_freeze.destroy',  'uses' => 'ProjectScopeFreezesController@destroy']);
        });
        // Project Scope Freeze End

        // Project Scope Approvals Start
        Route::group(['prefix' => 'project-scope/{scopeId}/approvals', ], function(){
            Route::get('/', ['as' => 'project_scope_approvals',  'uses' => 'ProjectScopeApprovalsController@index']);
            Route::post('/', ['as' => 'project_scope_approvals.store',  'uses' => 'ProjectScopeApprovalsController@store']);
        });
        // Project Scope Approvals End

        // Project Scope Highligths Start
        Route::group(['prefix' => 'project-scope/{scopeId}/highlight', ], function(){
            Route::get('list', ['as' => 'project_scope_highlight.list',  'uses' => 'ProjectScopeDarHighlightController@list']);
            Route::post('/', ['as' => 'project_scope_highlight.store',  'uses' => 'ProjectScopeDarHighlightController@store']);
            Route::delete('{id}', ['as' => 'project_scope_highlight.destroy',  'uses' => 'ProjectScopeDarHighlightController@destroy']);
        });
        // Project Scope Highligths End

        // Project Scope Findings Start
        Route::group(['prefix' => 'project-scope/{scopeId}/finding', ], function(){
            Route::get('list', ['as' => 'project_scope_finding.list',  'uses' => 'ProjectScopeDarFindingsController@list']);
            Route::post('/', ['as' => 'project_scope_finding.store',  'uses' => 'ProjectScopeDarFindingsController@store']);
            Route::delete('{id}', ['as' => 'project_scope_finding.destroy',  'uses' => 'ProjectScopeDarFindingsController@destroy']);
        });
        // Project Scope Findings End

        // Project Scope Iom Start
        Route::group(['prefix' => 'project-scope/{scopeId}/iom', ], function(){
            Route::post('layout', ['as' => 'project_scope_iom.layout',  'uses' => 'ProjectScopeIomController@layout']);
            Route::post('send', ['as' => 'project_scope_iom.send',  'uses' => 'ProjectScopeIomController@send']);
            Route::get('{id}', ['as' => 'project_scope_iom.show',  'uses' => 'ProjectScopeIomController@show']);
            Route::post('save', ['as' => 'project_scope_iom.save',  'uses' => 'ProjectScopeIomController@save']);
        });
        // Project Scope Iom End

        // Project Scope Dist Start
        Route::group(['prefix' => 'project-scope/{scopeId}/dist', ], function(){
            Route::get('list', ['as' => 'project_scope_dist.list',  'uses' => 'ProjectScopeDistController@list']);
            Route::post('/', ['as' => 'project_scope_dist.store',  'uses' => 'ProjectScopeDistController@store']);
            Route::delete('{id}', ['as' => 'project_scope_dist.destroy',  'uses' => 'ProjectScopeDistController@destroy']);
        });
        // Project Scope Dist End
        
        // Project Scope DAR Start
        Route::group(['prefix' => 'project-scope/{scopeId}/dar', ], function(){
            Route::get('/', ['as' => 'project_scope_dar',  'uses' => 'ProjectScopeDarController@index']);
            Route::post('/', ['as' => 'project_scope_dar.store',  'uses' => 'ProjectScopeDarController@store']);
            Route::get('layout', ['as' => 'project_scope_dar.layout',  'uses' => 'ProjectScopeDarController@layout']);
            Route::post('send', ['as' => 'project_scope_dar.send',  'uses' => 'ProjectScopeDarController@send']);
        });
        // Project Scope DAR End

        // Project Scope APG Start
        Route::group(['prefix' => 'project-scope/{scopeId}/apg', ], function(){
            Route::post('populate' , ['as' => 'project_scope_apg.populate',  'uses' => 'ProjectScopeApgController@populate'] );
            Route::get('partial-view' , ['as' => 'project_scope_apg.partial_view',  'uses' => 'ProjectScopeApgController@partialView'] );
            Route::get('full-view' , ['as' => 'project_scope_apg.full_view',  'uses' => 'ProjectScopeApgController@fullView'] );
            Route::delete('recursive-destroy' , ['as' => 'project_scope_apg.recursive_destroy',  'uses' => 'ProjectScopeApgController@recursiveDestroy'] );
            Route::put('{id}/enabled-flag' , ['as' => 'project_scope_apg.enabled_flag',  'uses' => 'ProjectScopeApgController@enabledFlag'] );
            Route::put('{id}/update-header' , ['as' => 'project_scope_apg.update_header',  'uses' => 'ProjectScopeApgController@updateHeader'] );

            Route::get('/', ['as' => 'project_scope_apg.index',  'uses' => 'ProjectScopeApgController@index']);
            Route::get('list', ['as' => 'project_scope_apg.list',  'uses' => 'ProjectScopeApgController@list']);
            Route::post('/', ['as' => 'project_scope_apg.store',  'uses' => 'ProjectScopeApgController@store']);
            Route::get('{id}', ['as' => 'project_scope_apg.show',  'uses' => 'ProjectScopeApgController@show']);
            Route::put('{id}', ['as' => 'project_scope_apg.update',  'uses' => 'ProjectScopeApgController@update']);
            Route::delete('{id}', ['as' => 'project_scope_apg.destroy',  'uses' => 'ProjectScopeApgController@destroy']);
        });
        // Project Scope APG End

        // Project Scope APG Approvals Start
        Route::group(['prefix' => 'project-scope-apg/{apgId}/approvals', ], function(){
            Route::get('list', ['as' => 'project_scope_apg_approvals.list',  'uses' => 'ProjectScopeApgApprovalsController@list']);
            Route::post('/', ['as' => 'project_scope_apg_approvals.store',  'uses' => 'ProjectScopeApgApprovalsController@store']);
        });
        // Project Scope APG Approvals End

        // Project Scope APG Findings Start
        Route::group(['prefix' => 'project-scope-apg/{apgId}/findings', ], function(){
            Route::get('/', ['as' => 'project_scope_apg_findings.show',  'uses' => 'ProjectScopeApgFindingsController@show']);
            Route::post('/', ['as' => 'project_scope_apg_findings.save',  'uses' => 'ProjectScopeApgFindingsController@save']);
        });
        // Project Scope APG Findings End

        // WBS Auditor Management
        Route::group(['prefix' => 'auditor-management'], function() {
            // WBS Header Auditor's Management Input
            Route::group(['prefix' => 'management-input'], function() {
                Route::get('/', ['as' => 'auditor_management_input', 'uses' => 'AuditorManagementInputController@index']);
                Route::get('list', ['as' => 'auditor_management_input.list',  'uses' => 'AuditorManagementInputController@list']);
                Route::post('/', ['as' => 'auditor_management_input.store',  'uses' => 'AuditorManagementInputController@store']);
                Route::delete('{id}', ['as' => 'auditor_management_input.destroy',  'uses' => 'AuditorManagementInputController@destroy']);

                // WBS Project management Input
                Route::get('{id}/project/list', ['as' => 'auditor_management_input_project.list',  'uses' => 'AuditorManagementInputProjectController@index']);

                Route::get('{id}/project', ['as' => 'auditor_management_input_project.show',  'uses' => 'AuditorManagementInputProjectController@show']);
                Route::post('{id}/project', ['as' => 'auditor_management_input_project.store',  'uses' => 'AuditorManagementInputProjectController@store']);
                Route::put('{id}/project', ['as' => 'auditor_management_input_project.update',  'uses' => 'AuditorManagementInputProjectController@update']);
                Route::delete('{id}/project', ['as' => 'auditor_management_input_project.destroy',  'uses' => 'AuditorManagementInputProjectController@destroy']);
                
                //WBS Project Scope management Input
                Route::get('{id}/scope', ['as' => 'auditor_management_input_scope.show',  'uses' => 'AuditorManagementInputProjectScopeController@show']);
                Route::post('{id}/scope', ['as' => 'auditor_management_input_scope.store',  'uses' => 'AuditorManagementInputProjectScopeController@store']);
                Route::put('{id}/scope', ['as' => 'auditor_management_input_scope.update',  'uses' => 'AuditorManagementInputProjectScopeController@update']);
                Route::delete('{id}/scope', ['as' => 'auditor_management_input_scope.destroy',  'uses' => 'AuditorManagementInputProjectScopeController@destroy']);
            });

            // Auditor’s Tasks Schedule Inquiry
            Route::group(['prefix' => 'tasks-schedule-inquiry'], function() {
                Route::get('/', ['as' => 'auditor_tasks_schedule_inquiry', 'uses' => 'AuditorTasksScheduleInquiryController@index']);
                Route::get('list', ['as' => 'auditor_tasks_schedule_inquiry.list',  'uses' => 'AuditorTasksScheduleInquiryController@list']);
                Route::post('/', ['as' => 'auditor_tasks_schedule_inquiry.store',  'uses' => 'AuditorTasksScheduleInquiryController@store']);
                Route::get('{id}', ['as' => 'auditor_tasks_schedule_inquiry.show',  'uses' => 'AuditorTasksScheduleInquiryController@show']);
                Route::put('{id}', ['as' => 'auditor_tasks_schedule_inquiry.update',  'uses' => 'AuditorTasksScheduleInquiryController@update']);
                Route::delete('{id}', ['as' => 'auditor_tasks_schedule_inquiry.destroy',  'uses' => 'AuditorTasksScheduleInquiryController@destroy']);
            });
        });
        // Auditor Management End
        
        // Global Config Start
        Route::group(['prefix' => 'global_config', ], function(){
            Route::get('/', ['as' => 'global_config',  'uses' => 'GlobalConfigController@index']);
            Route::get('/{id}', ['as' => 'global_config.show',  'uses' => 'GlobalConfigController@show']);
            Route::put('/save', ['as' => 'global_config.save',  'uses' => 'GlobalConfigController@save']);
        });
        // Global Config End

    });
    // Authenticated Routes
});
