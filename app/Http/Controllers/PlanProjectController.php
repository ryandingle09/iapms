<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PlanProjectRepository;
use App\Http\Requests\PlanProjectRequest;
use App\Criteria\CurrentUserPlanProjectsCriteria;
use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Auth;

class PlanProjectController extends Controller
{
    protected $repository;

    public function __construct(
        PlanProjectRepository $repository
    ){
        $this->repository = $repository;
    }

    public function index($group_id){
        $projects = $this->repository->findWhere([ 'plan_id' => $group_id ]);
    	return response()->json([
            'success' => true,
            'data' => $projects
        ]);
    }

    public function store($group_id, PlanProjectRequest $request){
    	$count = $this->repository->findWhere(['plan_group_id' => $group_id ])->count();
        $attributes = [
        	'plan_group_id' =>  $request->group ? $request->group : $group_id ,
	        'project_sequence' => $count + 1,
	        'plan_project_name' => $request->plan_project_name,
	        'audit_type' => $request->audit_type,
	        'plan_project_status' => 'New',
	        'target_start_date' => ymd($request->target_start_date),
            'target_end_date' => ymd($request->target_end_date),
	        'plan_project_remarks' => $request->plan_project_remarks
        ];
        $project = $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Project has been created.'
        ]);
    }

    public function show($group_id, $id){
        $details = $this->repository->find($id);
        return response()->json([
            'success' => true,
            'data' => $details
        ]);
    }

    public function update($group_id, $id, PlanProjectRequest $request)
    {
        $attributes = [
            'plan_project_name' => $request->plan_project_name,
            'target_start_date' => $request->target_start_date,
            'target_end_date' => $request->target_end_date,
            'project_remarks' => $request->project_remarks
        ];

        $this->repository->update($attributes , $id);
        return response()->json([
            'success' => true,
            'message' => 'Project has been updated.'
        ]);
    }

    public function destroy($group_id , $id, Request $request)
    {
    	$this->repository->delete($id);

        $left = $this->repository->findWhere([ 'plan_group_id' => $group_id ]);
        foreach ($left as $key => $value) {
           $value->project_sequence =  $key + 1;
           $value->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'Project has been removed.'
        ]);
    }
    
    public function list($group_id){

        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
    	$projects = $this->repository->findWhere([
            'plan_group_id' => $group_id
        ]);

        return Datatables::of($projects)->make(true);
    }
}
