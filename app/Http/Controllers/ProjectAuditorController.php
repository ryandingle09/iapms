<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProjectAuditorRepository;
use App\Http\Requests\ProjectAuditorCreateRequest;
use Yajra\Datatables\Datatables;
use App\Services\ObjectMapperService;
use App\Http\Requests;

class ProjectAuditorController extends Controller
{
    protected $repository;

    public function __construct(
        ProjectAuditorRepository $repository
    ){
        $this->repository = $repository;
    }

    public function index($projectId, ObjectMapperService $mapper)
    {
        $auditors = $this->repository->with('auditor')->findWhere([
            'project_id' => $projectId
            ]);

        $auditors = $mapper->setTemplate('AuditorFromWith')->map( $auditors);
       
        return response()->json([
            'success' => true,
            'message' => 'Project Auditor list.',
            'data' => $auditors
        ]);
    }


    public function store($projectId, ProjectAuditorCreateRequest $request)
    {
    	$attributes = [
            'project_id' => $projectId,
            'auditor_id' => $request->auditor,
            'auditor_type' => $request->role
        ];
        $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Project Auditor has been added.',
            'data' => $attributes
        ]);
    }

    public function list($projectId)
    {
        $auditors = $this->repository->with('auditor')->findWhere([
            'project_id' => $projectId
            ]);
        return Datatables::of($auditors)->make(true);
    }

    public function destroy($id){
        $projects = $this->repository->delete($id);
        return response()->json([
            'success' => true,
            'message' => 'Project Auditor has been removed.'
        ]);
    }
}
