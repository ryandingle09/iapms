<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PlanRepository;
use App\Http\Requests;
use App\Http\Requests\PlanRequest;
use Auth;

class PlanController extends Controller
{
	const VIEW_PATH = 'plan.';
	protected $repository;

	public function __construct(PlanRepository $repository){
		$this->repository = $repository;
	}

    public function index(Request $request){
    	$search_year = $request->search_year ? $request->search_year : "";
        $plan = null;
        $create_form = $request->create_form ? $request->create_form : null;
    	if($search_year){
    		$plan = $this->repository->findWhere([ 'plan_year' => $search_year ] )->first();
    	}
        return view( self::VIEW_PATH . 'index', compact("search_year", "plan" , "create_form") ); 
    }

    public function store(PlanRequest $request){

    	$attributes = $request->all();
        $attributes['plan_status'] = 'New';
        $plan = $this->repository->create($attributes);

        // $auditors = \App\Models\Auditors::get();

        // $plan->auditor->saveMany($auditors)

        return response()->json([
            'success' => true,
            'message' => 'Annual Plan has been created.',
            'data' => $plan
        ]);
    }

    public function show($planId){
        $plan = $this->repository->find($planId);

        return response()->json([
            'success' => true,
            'message' => 'Annual Plan has been found.',
            'data' => $plan
        ]);
    }

    public function update($planId, PlanRequest $request){
        $plan = $this->repository->find($planId);
    	$attributes = [
    		'plan_name' =>  $request->plan_name,
    		'remarks' =>  $request->remarks,
            // 'plan_status' => $request->plan_status
    	];

        // if($request->plan_status == "Reviewed"){
        //     $status = [ 'For Review' , 'New' ];
        //     $find = $plan->projects->whereIn('plan_project_status',$status)->count();
        //     if($find){
        //         return response()->json([ "plan_status" => ["All project must be set as `Reviewed` before changing the status of this plan."] ], 422);
        //     }
        //     $attributes['reviewer_remarks'] = $request->reviewer_remarks;
        //     $attributes['reviewed_by'] = Auth::user()->user_id;
        //     $attributes['reviewed_date'] = date('Y-m-d');
        // }
        // if($request->plan_status == "Approved"){
        //     $status = [ 'For Review' , 'New', 'Reviewed' , 'For Approval'];
        //     $find = $plan->projects->whereIn('plan_project_status',$status)->count();
        //     if($find){
        //         return response()->json([ "plan_status" => ["All project must be set as `Approved` before changing the status of this plan."] ], 422);
        //     }
        //     $attributes['approver_remarks'] = $request->approver_remarks;
        //     $attributes['approved_by'] = Auth::user()->user_id;
        //     $attributes['approved_date'] = date('Y-m-d');
        // }
        
        $project = $this->repository->update($attributes,$planId);

        return response()->json([
            'success' => true,
            'message' => 'Annual Plan has been updated.',
            'data' => $project
        ]);
    }

    public function process(Request $request){
        $search_year = $request->search_year ? $request->search_year : "";
        $plan = null;
        if($search_year){
            $plan = $this->repository->findWhere([ 'plan_year' => $search_year ] )->first();
        }
        return view( 'plan_proccess.index', compact("search_year","plan") ); 
    }

    public function inquiry(Request $request){
        $search_year = $request->search_year ? $request->search_year : "";
        $plan = null;
        if($search_year){
            $plan = $this->repository->findWhere([ 'plan_year' => $search_year ] )->first();
        }
        return view( 'plan_inquiry.index', compact("search_year","plan") ); 
    }
}
