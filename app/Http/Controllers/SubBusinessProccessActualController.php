<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AeMbpSbpRepositoryEloquent;
use App\Services\ObjectMapperService;
use App\Http\Requests;

class SubBusinessProccessActualController extends Controller
{
	protected $repository;

	public function __construct(
		AeMbpSbpRepositoryEloquent $repository
	){
		$this->repository = $repository;
	}

    public function index($mbpa_id , ObjectMapperService $mapper)
	{
		$mbpa = \App\Models\AeMbpActual::find($mbpa_id);		
		$mbp_id = $mbpa->master_ae_mbp_id;

		$sbps = $this->repository->with('businessProcess')->findWhere([
					'ae_mbp_id' => $mbp_id 
				]);

		$data = $mapper->setTemplate('BPFromWith')->map( $sbps );

    	return response()->json([
            'success' => true,
            'data' => $data
        ]);
	}
}
