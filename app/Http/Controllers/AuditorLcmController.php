<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Criteria\JoinLookupValuesCriteria;
use App\Repositories\AuditorLcmRepository;
use App\Http\Requests\AuditorLcmRequest;
use Yajra\Datatables\Datatables;

class AuditorLcmController extends Controller
{
    public function __construct(AuditorLcmRepository $auditor)
    {
        $this->repository = $auditor;
    }

    public function store($auditor_id, AuditorLcmRequest $request)
    {

        $attributes = $request->only(['lcm_type', 'ref_no', 'remarks', 'issued_date', 'expiry_date']);
        $attributes['auditor_id'] = $auditor_id;
        $lcm = $this->repository->create($attributes);

        return response()->json([ 
            'success' => true , 
            'message' => 'License / Certification / Membership has been successfully save!' ,
            'data' => $lcm
        ]);

    }

    public function update($auditor_id, $id, AuditorLcmRequest $request)
    {
        $attributes = $request->only(['lcm_type', 'ref_no', 'remarks', 'issued_date', 'expiry_date']);
        $attributes['auditor_id'] = $auditor_id;
        $this->repository->update($attributes, $id);

        return response()->json([ 
            'success' => true , 
            'message' => 'License / Certification / Membership has been updated successfully!',
        ]);
    }

    public function destroy($auditor_id, $id)
    {
        $this->repository->delete($id);
        return response()->json([ 
            'success' => true , 
            'message' => 'License / Certification / Membership has been removed successfully!',
        ]);
    }

    public function list($auditor_id){
    	$this->repository->pushCriteria(
    		new JoinLookupValuesCriteria([
    			'auditor_lcm.lcm_type' => config('iapms.lookups.auditor.lcm')
    		])
    	);
    	$lcms = $this->repository->findWhere([
    		'auditor_id' => $auditor_id,
    	]);
        return Datatables::of($lcms)->make(true);
    }
}
