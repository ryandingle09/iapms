<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\IomDistListRepository;
use App\Http\Requests\IomDistCreateRequest;
use Yajra\Datatables\Datatables;
use App\Http\Requests;

class ProjectScopeDistController extends Controller
{
	protected $repository;

    public function __construct(IomDistListRepository $repository)
    {   
        $this->repository = $repository;
    }

    public function store($scopeId, IomDistCreateRequest $request)
    {
        $attribute = [
            'project_scope_id' => $scopeId ,
            'auditee_id' => $request->dist_auditee,
            'distribution_type' => $request->dist_type,
        ];
        $this->repository->create($attribute);

        return response()->json([
            'success' => true , 
            'message' => 'Distribution has been created.' 
        ]);
    }
    
    public function destroy($scopeId, $id)
    {
        $deleted = $this->repository->delete($id);
        return response()->json([
            'success' => true , 
            "message" => "Distribution has been removed."  
        ]);
    }

    public function list($scopeId)
    {
        $dist = $this->repository
            ->with([
                'auditee' => function($q) {
                    $q->select('auditees.*')
                        ->lookupDetails(config('iapms.lookups.auditee.position'), 'lv_position_code', 'position_code');
                }
            ])->findWhere([
                'project_scope_id' => $scopeId
            ]);

        return Datatables::of($dist)->make(true);
    }
}
