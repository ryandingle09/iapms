<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AuditableEntitiesActualRepository;
use App\Http\Requests;

class AuditableEntityActualController extends Controller
{
	protected $repository;

	public function __construct(
		AuditableEntitiesActualRepository $repository
	){
		$this->repository = $repository;
	}

	public function index()
	{
		$data = $this->repository->all();
    	return response()->json([
            'success' => true,
            'data' => $data
        ]);
	}

    public function list()
    {

    }
}
