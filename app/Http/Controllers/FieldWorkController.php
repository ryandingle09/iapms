<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ProjectScopeApgFindingsRequest;
use App\Repositories\ProjectScopeApgFindingsRepository;
use App\Models\ProjectScopeApgFindings;

class FieldWorkController extends Controller
{
    const VIEW_PATH = 'fieldwork';
    protected $apg_findings_repo;

    public function __construct(
    	ProjectScopeApgFindingsRepository $apg_findings_repo)
    {
        $this->apg_findings_repo = $apg_findings_repo;
    }

    public function index()
    {
        return view(self::VIEW_PATH.'.index');
    }

    
}
