<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ValueSetRequest;
use App\Repositories\ValueSetRepository;
use App\Services\AdditionalInfoService;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ValueSetController extends Controller
{
    const VIEW_PATH = 'value_set';
    private $repository;
    private $service;

    public function __construct(ValueSetRepository $value_set, AdditionalInfoService $service)
    {
        $this->repository = $value_set;
        $this->service = $service;
    }

    public function index()
    {
        return view(self::VIEW_PATH.'.index');
    }

    public function create()
    {
        return view(self::VIEW_PATH.'.form');
    }

    public function store(ValueSetRequest $request)
    {
        $attributes = [
            $request->get('code'),
            $request->get('meaning'),
        ];
        $test_query = $this->service
                           ->setFrom($request->get('from_clause'))
                           ->setWhere($request->get('where_clause'))
                           ->setAttributes($attributes)
                           ->testQuery();
    	if( !$test_query['success'] ) return response()->json($test_query); // return if query test failed

    	$attributes = [
	    	'value_set_name' => $request->get('name'),
	    	'description' => $request->get('description'),
	    	'code' => $request->get('code'),
	    	'meaning' => $request->get('meaning'),
	    	'from_clause' => $request->get('from_clause'),
	    	'where_clause' => $request->get('where_clause'),
    	];

    	$this->repository->create($attributes);

    	$request->session()->flash('message', 'Value set is successfully created.');

    	return response()->json(['success' => true]);
    }

    public function edit($id, Request $request)
    {
    	$details = $this->repository->find($id);

        return view(self::VIEW_PATH.'.form', compact('details'));
    }

    public function update($id, ValueSetRequest $request)
    {
    	$details = $this->repository->find($id);

        $attributes = [
            $request->get('code'),
            $request->get('meaning'),
        ];
    	$test_query = $this->service
                           ->setFrom($request->get('from_clause'))
                           ->setWhere($request->get('where_clause'))
                           ->setAttributes($attributes)
                           ->testQuery();
    	if( !$test_query['success'] ) return response()->json($test_query); // return if query test failed

    	$attributes = [
	    	'value_set_name' => $request->get('name'),
	    	'description' => $request->get('description'),
	    	'code' => $request->get('code'),
	    	'meaning' => $request->get('meaning'),
	    	'from_clause' => $request->get('from_clause'),
	    	'where_clause' => $request->get('where_clause'),
    	];
    	$details->update($attributes);

		$request->session()->flash('message', 'Value set is successfully updated.');

    	return response()->json(['success' => true]);
    }

    public function destroy($id, Request $request)
    {
    	$value_set = $this->repository->find($id);
    	$value_set->delete();

		$request->session()->flash('message', 'Value set is successfully deleted.');

    	return response()->json(['success' => true]);
    }

    public function list()
    {
        if( \Request::get('format') == 'json' ) $this->repository->setPresenter("App\Presenters\ValueSetPresenter");

        $value_sets = $this->repository->all();

        if( \Request::get('format') == 'json' )
            return response()->json($value_sets);
        else
            return Datatables::of($value_sets)->make(true);
    }

    public function postTestQuery(Request $request)
    {
        $this->validate($request, [
            'from_clause' => 'required',
            'where_clause' => 'required',
            'code' => 'required',
            'meaning' => 'required',
        ]);
        $attributes = [
            $request->get('code'),
            $request->get('meaning'),
        ];
        $test_query = $this->service
                           ->setFrom($request->get('from_clause'))
                           ->setWhere($request->get('where_clause'))
                           ->setAttributes($attributes)
                           ->testQuery();
        return response()->json($test_query);
    }

    public function getRunQuery(Request $request)
    {
    	$this->validate($request, [
	        'from_clause' => 'required',
	        'where_clause' => 'required',
            'code' => 'required',
            'meaning' => 'required',
	    ]);
        $attributes = [
            $request->get('code'),
            $request->get('meaning'),
        ];
        $query = $this->service
                      ->setFrom($request->get('from_clause'))
                      ->setWhere($request->get('where_clause'))
                      ->setAttributes($attributes)
                      ->runQuery();
	    return response()->json($query);
    }
}
