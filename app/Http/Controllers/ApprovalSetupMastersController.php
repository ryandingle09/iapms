<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ApprovalSetupMasterRequest;
use App\Repositories\ApprovalSetupMasterRepository;
use Yajra\Datatables\Datatables;

class ApprovalSetupMastersController extends Controller
{

    /**
     * @var ApprovalSetupMasterRepository
     */
    protected $repository;

    public function __construct(ApprovalSetupMasterRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $approval_setup = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $approval_setup,
            ]);
        }

        return view('approval_setup.index', compact('approval_setup'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ApprovalSetupMasterRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ApprovalSetupMasterRequest $request)
    {

        $approval_head = $this->repository->create($request->all());

        return response()->json([
            'success' => true,
            'message' => 'Approval setup head created.',
            'data' => $approval_head
        ]);

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $approvalSetupMaster = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $approvalSetupMaster,
            ]);
        }

        return view('approval_setup.show', compact('approvalSetupMaster'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $approvalSetupMaster = $this->repository->find($id);

        return view('approval_setup.edit', compact('approvalSetupMaster'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  ApprovalSetupMasterRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update($id, ApprovalSetupMasterRequest $request)
    {

        $approval_head = $this->repository->update($request->all() , $id);

        return response()->json([
            'success' => true,
            'message' => 'Approval setup head updated.',
            'data' => $approval_head
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        return response()->json([
            'success' => true,
            'message' => 'Approval setup head removed.',
            'data' => $deleted
        ]);
    }

    public function list(){
        $approval_head = $this->repository->all();
        return Datatables::of($approval_head)->make(true);
    }
}
