<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ApprovalToStatusRequest;
use App\Repositories\ApprovalToStatusRepository;
use Yajra\Datatables\Datatables;

class ApprovalToStatusesController extends Controller
{

    /**
     * @var ApprovalToStatusRepository
     */
    protected $repository;

    public function __construct(ApprovalToStatusRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $approvalToStatuses = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $approvalToStatuses,
            ]);
        }

        return view('approvalToStatuses.index', compact('approvalToStatuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ApprovalToStatusRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store($detail_id , ApprovalToStatusRequest $request)
    {

        $attributes = $request->all();
        $attributes['approval_setup_detail_id'] = $detail_id;
        $status = $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Approval setup detail To status created.',
            'data' => $status
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $approvalToStatus = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $approvalToStatus,
            ]);
        }

        return view('approvalToStatuses.show', compact('approvalToStatus'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $approvalToStatus = $this->repository->find($id);

        return view('approvalToStatuses.edit', compact('approvalToStatus'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  ApprovalToStatusRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update($detail_id, ApprovalToStatusRequest $request, $id)
    {

        $attributes = $request->all();
        $status = $this->repository->update($attributes , $id);

        return response()->json([
            'success' => true,
            'message' => 'Approval setup detail To status updated.',
            'data' => $status
        ]);
    }


    /**
     * Remove the specified resource To storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($detail_id, $id)
    {
        $deleted = $this->repository->delete($id);

        return response()->json([
            'message' => 'Status deleted.',
            'deleted' => $deleted
        ]);
    }

    public function list($detail_id){
        $details = $this->repository->findWhere([
                'approval_setup_detail_id' => $detail_id
            ]);
        return Datatables::of($details)->make(true);
    }
}
