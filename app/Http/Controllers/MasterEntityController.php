<?php

namespace App\Http\Controllers;

use App\Events\MainBpMandayWasUpdated;
use App\Repositories\AeMbpRepository;
use App\Repositories\AeMbpSbpRepository;
use App\Repositories\AuditableEntitiesBpRepository;
use App\Repositories\AuditableEntityRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Repository\Criteria\RequestCriteria;

class MasterEntityController extends Controller
{
    const VIEW_PATH = 'auditable_entities';

    private $repository;

    function __construct(AuditableEntityRepository $auditable_entities)
    {
        $this->repository = $auditable_entities;
    }

    /**
     * Update Main Business Process Mandays (default)
     * @param $main_bp_id
     * @param Requests\BpMandaysRequest $request
     * @param AeMbpRepository $main_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdateMainBpMandays($main_bp_id, Requests\BpMandaysRequest $request, AeMbpRepository $main_bp)
    {
        $mainbp = $main_bp->find($main_bp_id);
        $attributes = $request->except(['_method', '_token']);
        $mainbp->update($attributes);

        event(new MainBpMandayWasUpdated($mainbp, $attributes));

        return response()->json(['success' => true]);
    }

    /**
     * Get main business process with sub processes
     * @param $entity_id
     * @param AuditableEntitiesBpRepository $entity_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEntityMainBp($entity_id, AuditableEntitiesBpRepository $entity_bp)
    {
        if( \Request::get('json') != true ) $this->repository->popCriteria(app(RequestCriteria::class));

        $entity = $this->repository->find($entity_id);
        $main_bp = $entity_bp->getMainBusinessProcess($entity);

        if( \Request::get('json') == true ) {
            return response($main_bp->get())->json();
        }
        return Datatables::of($main_bp)->make(true);
    }

    /**
     * Save main business process and sub processes
     * @param $entity_id
     * @param Requests\MainBpRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStoreEntityMainBp($entity_id, Requests\MainBpRequest $request)
    {
        $entity = $this->repository->find($entity_id);
        $attributes = ['main_bp_name' => $request->get('main_bp')];
        $custom_measures = $request->get('custom_measure');
        for ($i=0; $i < count($custom_measures); $i++) {
            if( $custom_measures[$i]['name'] != '' && $custom_measures[$i]['value'] != '' ) {
                $number = $i+1;
                $attributes['cm_'.$number.'_name'] = $custom_measures[$i]['name'];
                $attributes['cm_'.$number.'_high_score_type'] = $custom_measures[$i]['value'];
            }
        }
        $entity->masterBp()->create($attributes);

        return response()->json(['success' => true]);
    }

    /**
     * @param $main_bp_id
     * @param Requests\SubBusinessProcessRequest $request
     * @param AuditableEntitiesBpRepository $entity_bp
     * @param AeMbpRepository $main_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStoreSubProcess($main_bp_id, Requests\SubBusinessProcessRequest $request, AuditableEntitiesBpRepository $entity_bp, AeMbpRepository $main_bp)
    {
        $mainbp = $main_bp->find($main_bp_id);
        $attributes = [
            'mbp_bp_seq' => $request->get('sp_seq'),
            'bp_id' => $request->get('sub_bp'),
        ];
        $entity_bp->createMainBusinessProcess($mainbp, $attributes);

        return response()->json(['success' => true]);
    }

    /**
     * @param $main_bp_id
     * @param Request $request
     * @param AeMbpRepository $main_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdateMainBp($main_bp_id, Request $request, AeMbpRepository $main_bp)
    {
        $mainbp = $main_bp->find($main_bp_id);
        $attributes = [];
        $custom_measures = $request->get('custom_measure');
        for ($i=0; $i < count($custom_measures); $i++) {
            if( $custom_measures[$i]['name'] != '' && $custom_measures[$i]['value'] != '' ) {
                $number = $i+1;
                $attributes['cm_'.$number.'_name'] = $custom_measures[$i]['name'];
                $attributes['cm_'.$number.'_high_score_type'] = $custom_measures[$i]['value'];
            }
        }
        $mainbp->update($attributes);

        return response()->json(['success' => true]);
    }

    /**
     * Delete main business process
     * @param Request $request
     * @param AeMbpRepository $main_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMainBusinessProcess(Request $request, AeMbpRepository $main_bp)
    {
        $id = $request->get('id');
        $main_bp->delete($id);

        return response()->json(['success' => true]);
    }

    /**
     * Delete sub process
     * @param Request $request
     * @param AeMbpSbpRepository $sub_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSubBusinessProcess(Request $request, AeMbpSbpRepository $sub_bp)
    {
        $id = $request->get('id');
        $sub_bp->delete($id);

        return response()->json(['success' => true]);
    }
}
