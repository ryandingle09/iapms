<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Requests\AuditorGradingMatrixRequest;
use App\Repositories\AuditorGradingMatrixRepository;
use Yajra\Datatables\Datatables;

class AuditorGradingMatrixController extends Controller
{
	const VIEW_PATH = 'auditor_grading_matrix';
    private $repository;

    public function __construct(AuditorGradingMatrixRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        return view( self::VIEW_PATH.'.index' );
    }

    public function store(AuditorGradingMatrixRequest $request)
    {
        $attributes = [
            'variance_from' => $request->get('variance_from'),
            'variance_to' => $request->get('variance_to'),
            'rating' => $request->get('rating'),
            'quality_max' => $request->get('quality_max'),
            'documentation_max' => $request->get('documentation_max'),
            'remarks' => $request->get('remarks'),
        ];
        $grading = $this->repository->create($attributes);

       	return response()->json([ 
            'success' => true , 
            'message' => 'Grading matrix has been created.' ,
            'data' => $grading
        ]);
    }

    public function update($id, AuditorGradingMatrixRequest $request)
    {
        $attributes = [
            'variance_from' => $request->get('variance_from'),
            'variance_to' => $request->get('variance_to'),
            'rating' => $request->get('rating'),
            'quality_max' => $request->get('quality_max'),
            'documentation_max' => $request->get('documentation_max'),
            'remarks' => $request->get('remarks'),
        ];
        $this->repository->update($attributes, $id);

        return response()->json([ 
            'success' => true , 
            'message' => 'Grading matrix has been updated.' 
        ]);
    }

    public function destroy($id, Request $request)
    {
        $this->repository->delete($id);

        return response()->json([ 
            'success' => true , 
            'message' => 'Grading matrix has been removed.' 
        ]);
    }

    public function list(){
    	$data = $this->repository->all();
        return Datatables::of($data)->make(true);
    }
}
