<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PlanProjectScopeAuditorRepository;
use App\Http\Requests\PlanProjectScopeAuditorRequest;
use Yajra\Datatables\Datatables;
use App\Http\Requests;

class PlanProjectScopeAuditorController extends Controller
{
    protected $repository;

    public function __construct(
        PlanProjectScopeAuditorRepository $repository
    ){
        $this->repository = $repository;
    }

    public function index($scopeId)
    {

    }


    public function store($scopeId, PlanProjectScopeAuditorRequest $request)
    {
    	$attributes = [
            'plan_project_scope_id' => $scopeId,
            'auditor_id' => $request->auditor,
            'auditor_type' => $request->auditor_type,
            'allotted_mandays' => $request->allotted_mandays,
        ];
        $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Project Scope Auditor has been added.',
            'data' => $attributes
        ]);
    }

    public function show($scopeId, $id){
        $data = $this->repository->with('auditor')->find($id);
        return response()->json([
            'success' => true,
            'message' => 'Project Scope Auditor has been read.',
            'data' => $data
        ]);
    }

    public function update($scopeId, $id,  PlanProjectScopeAuditorRequest $request){
        $attributes = [
            'auditor_id' => $request->auditor,
            'auditor_type' => $request->auditor_type,
            'allotted_mandays' => $request->allotted_mandays,

        ];
        $this->repository->update($attributes, $id);

        return response()->json([
            'success' => true,
            'message' => 'Project Scope Auditor has been updated.',
            'data' => $attributes
        ]);
    }

    public function destroy($scopeId, $id){
        $this->repository->delete($id);
        return response()->json([
            'success' => true,
            'message' => 'Project Scope Auditor has been removed.'
        ]);
    }

    public function list($scopeId)
    {

        $auditors = $this->repository->with(['auditor'])->findWhere([
            'plan_project_scope_id' => $scopeId
        ]);

        return Datatables::of($auditors)->make(true);
    }
}
