<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PlanProjectAuditorRepository;
use App\Http\Requests\PlanProjectAuditorRequest;
use Yajra\Datatables\Datatables;
use App\Services\ObjectMapperService;
use App\Http\Requests;

class PlanProjectAuditorController extends Controller
{
    protected $repository;

    public function __construct(
        PlanProjectAuditorRepository $repository
    ){
        $this->repository = $repository;
    }

    public function index($project_id, ObjectMapperService $mapper)
    {

        $auditors = $this->repository->with('auditor')->findWhere([
            'plan_project_id' => $project_id
        ]);

        $auditors = $mapper->setTemplate('AuditorFromWith')->map( $auditors);
       
        return response()->json([
            'success' => true,
            'message' => 'Plan Project Auditor list.',
            'data' => $auditors
        ]);
    }


    public function store($projectId,PlanProjectAuditorRequest $request)
    {
    	$attributes = [
            'plan_project_id' => $projectId,
            'auditor_id' => $request->auditor,
            'auditor_type' => $request->auditor_type,
            // 'allotted_mandays' => $request->allotted_mandays,
        ];
        $data = $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Project Auditor has been added.',
            'data' => $data
        ]);
    }

    public function list($projectId){
        $auditors = $this->repository->with('auditor')->findWhere([
            'plan_project_id' => $projectId
            ]);
        return Datatables::of($auditors)->make(true);
    }

    public function destroy($projectId,$id){
        $projects = $this->repository->delete($id);
        return response()->json([
            'success' => true,
            'message' => 'Project Auditor has been removed.'
        ]);
    }
}
