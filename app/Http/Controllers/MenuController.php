<?php

namespace App\Http\Controllers;

use App\Http\Requests\MenuItemsRequest;
use App\Models\Functions;
use App\Models\LookupType;
use App\Models\Menu;
use App\Services\MenuService;
use App\Repositories\MenuRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class MenuController extends Controller
{
    const VIEW_PATH = 'administrator.menu';

    private $menu;
    private $menu_service;

    function __construct(MenuRepository $menu,MenuService $menu_service)
    {
        $this->menu = $menu;
        $this->menu_service = $menu_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $menu_item = null;
        if($request->id){
            $menu_item = $this->menu->find($request->id);
        }
        $menus = Menu::tree();
        return view(self::VIEW_PATH.'.index',compact('menus','menu_item'));
    }

    /**
     * Store a newly created menu item in storage.
     *
     * @param  App\Http\Requests\MenuItemsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuItemsRequest $request)
    {
        $attributes = [
            'name' => $request->name,
            'url' => $request->is_parent ? null : $request->url ,
            'is_parent' => $request->is_parent ? 1 : 0,
            'icon' => $request->icon ? $request->icon : 'fa-cog',
        ];
        $this->menu->create($attributes);
        menu()->clearCache();
        $request->session()->flash('success_message', 'Menu has been added.');
        return redirect()->route('administrator.menu');
    }

    /**
     * Update existing menu item in storage.
     *
     * @param  App\Http\Requests\MenuItemsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, MenuItemsRequest $request)
    {
        $attributes = [
            'name' => $request->name,
            'url' => $request->is_parent ? null : $request->url ,
            'is_parent' => $request->is_parent ? 1 : 0,
            'icon' => $request->icon ? $request->icon : 'fa-cog',
        ];
        $this->menu->update($attributes, $id);
        menu()->clearCache();
        $request->session()->flash('success_message', 'Menu has been Updated.');
        return redirect()->route('administrator.menu');
    }


    /**
     * Get all the menus
     * @return Datatables
     */
    public function getMenuList()
    {
        $menus = $this->menu
                      ->with('updatedBy')
                      ->all();
        return Datatables::of($menus)->make(true);
    }

    /**
     * Delete selected menu in storage.
     *
     * @param  App\Http\Requests  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $menu = $this->menu->find($id);
        $menu->delete();
        menu()->clearCache();
        $request->session()->flash('success_message', 'Menu has been removed.');
        return response()->json(['success' => true , 'url' => route('administrator.menu') ]);
    }

    /**
     * Update Hierarchy of menu sequence
     *
     * @param  App\Http\Requests  $request
     * @return \Illuminate\Http\Response
     */
    public function updateHierarchy(Request $request)
    {
        $menus = json_decode($request->get('menus'));
        if( count( $menus ) ) {
            $this->updateChildren($menus);
        }
        menu()->clearCache();
        $request->session()->flash('success_message', 'Menu hierarchy has been update.');
        return response()->json(['success' => true , 'url' => route('administrator.menu')]);
    }

    public function updateChildren($menus,$parent_id = 0){
        foreach ($menus as $key => $value) {
            $attr = [
                'menu_sequence' => $key,
                'parent_id' => $parent_id,
            ];
            $this->menu->update($attr,$value->id);
            if( isset($value->children) ) {
                $this->updateChildren($value->children,$value->id);
            }
        }
    }

}
