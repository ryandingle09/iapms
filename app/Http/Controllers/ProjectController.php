<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProjectRepository;
use App\Http\Requests\ProjectRequest;
use App\Services\ProjectEngagementService;
use App\Jobs\CopyFromAnnualAuditPlan;
use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Auth;

class ProjectController extends Controller
{
    protected $repository;
    protected $engagement_service;

    public function __construct(
        ProjectRepository $repository,
        ProjectEngagementService $engagement_service
    ){
        $this->repository = $repository;
        $this->engagement_service = $engagement_service;
    }

    public function store(ProjectRequest $request){
        $attributes = [
            'project_name' => $request->project_name,
            'audit_type' => $request->audit_type,
            'project_status' => 'New',
            'target_start_date' => $request->target_start_date,
            'target_end_date' => $request->target_end_date,
            'project_remarks' => $request->project_remarks,
            'project_head' => $request->project_head,
            'audit_year' => $request->audit_year,
        ];
        $project = $this->repository->create($attributes);
        return response()->json([
            'success' => true,
            'message' => 'Project has been created.'
        ]);
    }

    public function show($id){
        $details = $this->repository->with(['reviewer','approver','annualPlanProject'])->find($id);
        return response()->json([
            'success' => true,
            'data' => $details
        ]);
    }

    public function update($id, ProjectRequest $request)
    {
        $attributes = [
            'project_name' => $request->project_name,
            'audit_type' => $request->audit_type,
            'target_start_date' => $request->target_start_date,
            'target_end_date' => $request->target_end_date,
            'project_remarks' => $request->project_remarks,
            'project_head' => $request->project_head,
            'audit_year' => $request->audit_year
        ];
        $project = $this->repository->update($attributes , $id);
        return response()->json([
            'success' => true,
            'message' => 'Project has been updated.',
            'data' => $project
        ]);
    }

    public function destroy($id,Request $request)
    {
        $this->repository->delete($id);
        return response()->json([
            'success' => true,
            'message' => 'Project has been removed.'
        ]);
    }
    
    public function list()
    {
    	$projects = $this->repository->all();
        return Datatables::of($projects)->make(true);
    }

    public function cfaap(Request $request)
    {
        dispatch(new CopyFromAnnualAuditPlan(
            $request->plan_project_id
        ));

        return response()->json([
            'success' => true,
            'message' => 'Project has been copied.'
        ]);
    }



}
