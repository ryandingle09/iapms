<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\AuditorManagementInputProjectRequest;
use App\Repositories\AuditorManagementInputProjectRepository;

class AuditorManagementInputProjectController extends Controller
{
    const VIEW = 'auditor_management.management_input.';

    private $repository;

    public function __construct(AuditorManagementInputProjectRepository $auditorManagementInputProject)
    {
        $this->repository = $auditorManagementInputProject;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($wbs_header_id)
    {
        $projects = $this->repository
                    ->with(['scope', 'auditor'])
                    ->findWhere([
                        'wbs_header_id' => $wbs_header_id
                    ]);

        $data = [
            'projects' => $projects
        ];

        return view(self::VIEW.'includes.project_and_scope', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuditorManagementInputProjectRequest $request, $wbs_header_id)
    {
        $attributes = [
            'wbs_header_id'     => $wbs_header_id,
            'project_name'      => $request->project_name,
            'project_head_id'   => $request->project_head,
            'audit_type'        => $request->audit_type,
            'status'            => $request->status,
            'start_date'        => ymd($request->start_date),
            'end_date'          => ymd($request->end_date),
        ];

        $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Project successfull created.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = $this->repository
                    ->with(['auditor'])
                    ->findWhere([
                        'wbs_project_id' => $id
                    ]);

        return response()->json($project);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = $this->repository
                    ->with(['auditor'])
                    ->findWhere([
                        'wbs_project_id' => $id
                    ]);

        return response()->json($project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AuditorManagementInputProjectRequest $request, $id)
    {
        $attributes = [
            'project_name'      => $request->project_name,
            'project_head_id'   => $request->project_head,
            'audit_type'        => $request->audit_type,
            'status'            => $request->status,
            'start_date'        => ymd($request->start_date),
            'end_date'          => ymd($request->end_date),
        ];

        $this->repository->update($attributes, $id);

        return response()->json([
            'success' => true,
            'message' => 'Project successfull Updated.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$data = $this->repository->delete($id);
        return response()->json([
            'success' => true,
            'message' => 'Project has been removed.'
        ]);
    }
}
