<?php

namespace App\Http\Controllers;

use App\Criteria\AuditeeEntitiesCriteria;
use App\Criteria\CompanyProfileCriteria;
use App\Criteria\EnterpriseRiskCriteria;
use App\Criteria\EntitiesByBranchCodeCriteria;
use App\Criteria\EntitiesByCompanyCodeCriteria;
use App\Criteria\ExcludeAssignEnterpriseRisksCriteria;
use App\Criteria\JoinCompanyBranchCriteria;
use App\Criteria\JoinLookupValuesCriteria;
use App\Http\Requests;
use App\Http\Requests\EnterpriseRiskDetailsRequest;
use App\Http\Requests\EnterpriseRiskRequest;
use App\Repositories\AuditableEntitiesActualRepository;
use App\Repositories\AuditableEntityRepository;
use App\Repositories\EnterpriseRiskDetailRepository;
use App\Repositories\EnterpriseRiskRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;

class EnterpriseRiskController extends Controller
{
    const VIEW_PATH = 'enterprise_risk';
    const UPLOAD_PATH = 'uploads/enterprise_risk/';
    private $repository;
    private $table;

    public function __construct(EnterpriseRiskRepository $ent_risk)
    {
        $this->repository = $ent_risk;
        $this->table = 'enterprise_risks';
    }

    /**
     * Display a listing of the resource.
     *
     * @param AuditableEntityRepository $auditable_entity
     * @return \Illuminate\Http\Response
     */
    public function index(AuditableEntityRepository $auditable_entity)
    {
        $auditable_entities = $auditable_entity->with('businessProcess')->all();

        return view(self::VIEW_PATH.'.index', compact('auditable_entities'));
    }

    public function store(EnterpriseRiskRequest $request)
    {
        $attributes = [
            'auditable_entity_id' => $request->get('auditable_entity'),
            'bp_id' => $request->get('business_process'),
            'ae_mbp_id' => $request->get('main_business_process'),
        ];
        $this->repository->create($attributes);

        $request->session()->flash('message', 'New enterprise risk has been created.');
        return response()->json(['success' => true]);
    }

    public function postStoreDetails($id, EnterpriseRiskDetailsRequest $request)
    {
        $ent_risk = $this->repository->find($id);
        // TODO: duplicate process - move to other method to reuse
        // process supporting docs - upload file
        $docs_name = null;
        $docs_filepath = null;
        if( $request->hasFile('supporting_docs') ) {
            $extension = $request->file('supporting_docs')->getClientOriginalExtension();
            $docs_name = $request->file('supporting_docs')->getClientOriginalName(); // for readable name purposes we save the original filename
            $file_name = $ent_risk->enterprise_risk_id.'_'.Carbon::now()->timestamp.'.'.$extension; // renaming the file to [ID]_[current] timestamp
            $docs_filepath = self::UPLOAD_PATH.$file_name;
            $request->file('supporting_docs')->move(self::UPLOAD_PATH, $file_name);
        }

        $attributes = [
            'risk_name' => $request->get('risk'),
            'risk_desc' => $request->get('description'),
            'risk_type' => $request->get('risk_type'),
            'risk_remarks' => $request->get('risk_remarks'),
            'risk_class' => $request->get('risk_class'),
            'risk_area' => $request->get('risk_area'),
            'response_status' => $request->get('response_status'),
            'response_status_comment' => $request->get('response_status_comment'),
            'response_type' => $request->get('response_type'),
            'inherent_impact_rate' => $request->get('inherent_impact_rate'),
            'inherent_likelihood_rate' => $request->get('inherent_likelihood_rate'),
            'residual_impact_rate' => $request->get('residual_impact_rate'),
            'residual_likelihood_rate' => $request->get('residual_likelihood_rate'),
            'rating_comment' => $request->get('rating_comment'),
            'supporting_docs_name' => $docs_name,
            'supporting_docs_file' => $docs_filepath,
            'rated_date' => $request->get('rated_date'),
        ];
        $ent_risk->details()->create($attributes);

        $request->session()->flash('message', 'New enterprise risk details has been created.');
        return response()->json(['success' => true]);
    }

    public function putUpdateDetails($id, EnterpriseRiskDetailsRequest $request, EnterpriseRiskDetailRepository $details)
    {
        $ent_risk_details = $details->find($id);
        // TODO: duplicate process - move to other method to reuse
        // process supporting docs - upload file
        $docs_name = $ent_risk_details->supporting_docs_name;
        $docs_filepath = $ent_risk_details->supporting_docs_file;
        if( $request->get('supporting_docs_delete') != '' )  {
            Storage::delete($docs_filepath); // if docs is deleted
            $docs_name = null;
            $docs_filepath = null;
        }

        if( $request->hasFile('supporting_docs') ) {
            Storage::delete($docs_filepath); // delete the old docs

            $extension = $request->file('supporting_docs')->getClientOriginalExtension();
            $docs_name = $request->file('supporting_docs')->getClientOriginalName(); // for readable name purposes we save the original filename
            $file_name = $ent_risk_details->enterprise_risk_id.'_'.Carbon::now()->timestamp.'.'.$extension; // renaming the file to [ID]_[current] timestamp
            $docs_filepath = self::UPLOAD_PATH.$file_name;
            $request->file('supporting_docs')->move(self::UPLOAD_PATH, $file_name);
        }
        $attributes = [
            'risk_name' => $request->get('risk'),
            'risk_desc' => $request->get('description'),
            'risk_type' => $request->get('risk_type'),
            'risk_remarks' => $request->get('risk_remarks'),
            'risk_class' => $request->get('risk_class'),
            'risk_area' => $request->get('risk_area'),
            'response_status' => $request->get('response_status'),
            'response_status_comment' => $request->get('response_status_comment'),
            'response_type' => $request->get('response_type'),
            'inherent_impact_rate' => $request->get('inherent_impact_rate'),
            'inherent_likelihood_rate' => $request->get('inherent_likelihood_rate'),
            'residual_impact_rate' => $request->get('residual_impact_rate'),
            'residual_likelihood_rate' => $request->get('residual_likelihood_rate'),
            'rating_comment' => $request->get('rating_comment'),
            'supporting_docs_name' => $docs_name,
            'supporting_docs_file' => $docs_filepath,
            'rated_date' => $request->get('rated_date'),
        ];
        $ent_risk_details->update($attributes);

        $request->session()->flash('message', 'Enterprise risk details is successfully udpated.');
        return response()->json(['success' => true]);
    }

    public function deleteDetails($id, Request $request, EnterpriseRiskDetailRepository $details)
    {
        $ent_risk_details = $details->find($id);
        $ent_risk_details->delete();

        return response()->json(['success' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $details = $this->repository->with([ 'auditableEntity', 'businessProcess' ])->find($id);

        return view( self::VIEW_PATH.'.form', compact('details'));
    }

    public function destroy($id, Request $request)
    {
        $enterprise_risk = $this->repository->find($id);
        $enterprise_risk->delete();

        return response()->json(['success' => true]);
    }

    public function lists()
    {
        $this->repository->popCriteria(app('Prettus\Repository\Criteria\RequestCriteria')); // disable searching through URI
        $this->repository->pushCriteria(new EnterpriseRiskCriteria());
        $this->repository->pushCriteria(new JoinLookupValuesCriteria( [
            'mbp.main_bp_name' => config('iapms.lookups.main_bp'),
            'ae.company_code' => config('iapms.lookups.company'),
        ] ));
        $this->repository->pushCriteria(new EntitiesByCompanyCodeCriteria());
        $this->repository->pushCriteria(new EntitiesByBranchCodeCriteria());
        $this->repository->pushCriteria(new CompanyProfileCriteria('', 'ae'));
        $this->repository->pushCriteria(new JoinCompanyBranchCriteria(false, '', 'ae'));
        $this->repository->pushCriteria(new AuditeeEntitiesCriteria('ae'));
        $ent_risk = $this->repository->all();

        return Datatables::of($ent_risk)->make(true);
    }

    public function getRisklist($id, EnterpriseRiskDetailRepository $details)
    {
        $details->pushCriteria(new JoinLookupValuesCriteria( [
            'enterprise_risk_details.response_type' =>  config('iapms.lookups.risk.response'),
            'enterprise_risk_details.risk_type' =>  config('iapms.lookups.risk.ent_type'),
            'enterprise_risk_details.risk_class' =>  config('iapms.lookups.risk.ent_class'),
            'enterprise_risk_details.risk_area' =>  config('iapms.lookups.risk.ent_area'),
            'enterprise_risk_details.response_status' =>  config('iapms.lookups.risk.response_status'),
        ] ));

        if( \Request::get('exclude_assigned') == true ) $details->pushCriteria(new ExcludeAssignEnterpriseRisksCriteria());

        if( $id == 'all' )
            $ent_risk_detail = $details->getAll();
        else
            $ent_risk_detail = $details->findByField('enterprise_risk_id', $id);
        if( \Request::get('format') == 'json' )
            return $ent_risk_detail;
        else
            return Datatables::of($ent_risk_detail)->make(true);
    }

    /**
     *
     * @param AuditableEntitiesActualRepository $entity
     * @return mixed
     */
    public function getAvailableEntity(AuditableEntitiesActualRepository $entity)
    {
        $entity->pushCriteria(new AuditeeEntitiesCriteria());
        $auditee_entity = $entity->all();

        return $auditee_entity;
    }
}
