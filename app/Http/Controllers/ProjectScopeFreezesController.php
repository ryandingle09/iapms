<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ProjectScopeFreezeCreateRequest;
use App\Http\Requests\ProjectScopeFreezeUpdateRequest;
use App\Repositories\ProjectScopeFreezeRepository;


class ProjectScopeFreezesController extends Controller
{

    /**
     * @var ProjectScopeFreezeRepository
     */
    protected $repository;

    public function __construct(ProjectScopeFreezeRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($scope_id)
    {
        $data = $this->repository->findWhere([
            'project_scope_id' => $scope_id
        ]);
        return response()->json([
            'success' => true,
            'message' => 'Project Scope Freeze has been retrieved.',
            'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProjectScopeFreezeCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store($scope_id, ProjectScopeFreezeCreateRequest $request)
    {

        $attributes = [
            "project_scope_id" => $scope_id,
            "freeze_date" => date('Y-m-d'),
            "effective_date_from" => $request->effective_date_from,
            "effective_date_to" => $request->effective_date_to,
            "remarks" => $request->remarks,
        ];
        $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Project Scope Freeze has been created.',
            'data' => $attributes
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($scope_id, $id)
    {
        $data = $this->repository->find($id);

        return response()->json([
            'success' => true,
            'message' => 'Project Scope Freeze has been created.',
            'data' => $data
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $projectScopeFreeze = $this->repository->find($id);

        return view('projectScopeFreezes.edit', compact('projectScopeFreeze'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  ProjectScopeFreezeCreateRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update($scope_id, $id, ProjectScopeFreezeCreateRequest $request)
    {

        $attributes = [
            "effective_date_from" => $request->effective_date_from,
            "effective_date_to" => $request->effective_date_to,
            "remarks" => $request->remarks,
        ];
        $data = $this->repository->update($attributes, $id);

        return response()->json([
            'success' => true,
            'message' => 'Project Scope Freeze has been updated.',
            'data' => $data
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'ProjectScopeFreeze deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'ProjectScopeFreeze deleted.');
    }
}
