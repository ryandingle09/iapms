<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Criteria\JoinLookupValuesCriteria;
use App\Repositories\AuditorTrainingRepository;
use App\Http\Requests\AuditorTrainingRequest;
use Yajra\Datatables\Datatables;

class AuditorTrainingController  extends Controller
{
    public function __construct(AuditorTrainingRepository $auditor)
    {
        $this->repository = $auditor;
    }

    public function store($auditor_id, AuditorTrainingRequest $request)
    {

        $attributes = $request->only(['training_type', 'description', 'venue', 'completion_date']);
        $attributes['auditor_id'] = $auditor_id;
        $lcm = $this->repository->create($attributes);

        return response()->json([ 
            'success' => true , 
            'message' => 'Training has been created.' ,
            'data' => $lcm
        ]);

    }

    public function update($auditor_id, $id, AuditorTrainingRequest $request)
    {
        $attributes = $request->only(['training_type', 'description', 'venue', 'completion_date']);
        $attributes['auditor_id'] = $auditor_id;
        $this->repository->update($attributes, $id);

        return response()->json([ 
            'success' => true , 
            'message' => 'Training has been updated.',
        ]);
    }

    public function destroy($auditor_id, $id)
    {
        $this->repository->delete($id);
        
        return response()->json([ 
            'success' => true , 
            'message' => 'Training has been removed.',
        ]);
    }

    public function list($auditor_id){
    	$this->repository->pushCriteria(
    		new JoinLookupValuesCriteria([
    			'auditor_training.training_type' => config('iapms.lookups.auditor.training')
    		])
    	);
    	$lcms = $this->repository->findWhere([
    		'auditor_id' => $auditor_id,
    	]);
        return Datatables::of($lcms)->make(true);
    }
}
