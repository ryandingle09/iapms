<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ApprovalSetupDetailRequest;
use App\Repositories\ApprovalSetupDetailRepository;
use Yajra\Datatables\Datatables;


class ApprovalSetupDetailsController extends Controller
{

    /**
     * @var ApprovalSetupDetailRepository
     */
    protected $repository;

    public function __construct(ApprovalSetupDetailRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $details = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $details,
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ApprovalSetupDetailRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store($head_id, ApprovalSetupDetailRequest $request)
    {

        $attributes = $request->all();
        $attributes['approval_setup_master_id'] = $head_id;
        $attributes['approver_flag'] = $request->approver_flag ? 1 : 0 ;
        $detail = $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Approval setup detail created.',
            'data' => $detail
        ]);

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $approvalSetupDetail = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $approvalSetupDetail,
            ]);
        }

        return view('approvalSetupDetails.show', compact('approvalSetupDetail'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $approvalSetupDetail = $this->repository->find($id);

        return view('approvalSetupDetails.edit', compact('approvalSetupDetail'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  ApprovalSetupDetailRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update($head_id, ApprovalSetupDetailRequest $request, $id)
    {

        $attributes = $request->all();
        $attributes['approver_flag'] = $request->approver_flag ? 1 : 0 ;
        $detail = $this->repository->update($attributes , $id);

        return response()->json([
            'success' => true,
            'message' => 'Approval setup detail updated.',
            'data' => $detail
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($head_id, $id)
    {
        $deleted = $this->repository->delete($id);

        return response()->json([
            'message' => 'Approval Detail deleted.',
            'deleted' => $deleted
        ]);

    }


    public function list($head_id){
        $details = $this->repository->findWhere([
                'approval_setup_master_id' => $head_id
            ]);
        return Datatables::of($details)->make(true);
    }
}
