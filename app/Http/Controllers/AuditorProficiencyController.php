<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Criteria\JoinLookupValuesCriteria;
use App\Repositories\AuditorProficiencyRepository;
use App\Http\Requests\AuditorProficiencyRequest;
use Yajra\Datatables\Datatables;

class AuditorProficiencyController  extends Controller
{
    public function __construct(AuditorProficiencyRepository $auditor)
    {
        $this->repository = $auditor;
    }

    public function store($auditor_id, AuditorProficiencyRequest $request)
    {

        $attributes = $request->only(['proficiency_type', 'remarks', 'proficiency_rate']);
        $attributes['auditor_id'] = $auditor_id;
        $lcm = $this->repository->create($attributes);

        return response()->json([ 
            'success' => true , 
            'message' => 'Proficiency has been created.' ,
            'data' => $lcm
        ]);

    }

    public function update($auditor_id, $id, AuditorProficiencyRequest $request)
    {
        $attributes = $request->only(['proficiency_type', 'remarks', 'proficiency_rate']);
        $attributes['auditor_id'] = $auditor_id;
        $this->repository->update($attributes, $id);

        return response()->json([ 
            'success' => true , 
            'message' => 'Proficiency has been updated.',
        ]);
    }

    public function destroy($auditor_id, $id)
    {
        $this->repository->delete($id);
        return response()->json([ 
            'success' => true , 
            'message' => 'Proficiency has been removed.',
        ]);
    }

    public function list($auditor_id){
    	$this->repository->pushCriteria(
    		new JoinLookupValuesCriteria([
    			'auditor_proficiency.proficiency_type' => config('iapms.lookups.auditor.proficiency')
    		])
    	);
    	$lcms = $this->repository->findWhere([
    		'auditor_id' => $auditor_id,
    	]);
        return Datatables::of($lcms)->make(true);
    }
}
