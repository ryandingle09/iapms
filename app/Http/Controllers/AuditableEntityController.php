<?php

namespace App\Http\Controllers;

use App\Criteria\ActiveCompanyBranchCriteria;
use App\Criteria\ActualEntityGroupByCompanyCodeCriteria;
use App\Criteria\AuditableEntityExcludeCompanyBranchCriteria;
use App\Criteria\AuditableEntityInquirySearchCriteria;
use App\Criteria\AuditableEntityParentCriteria;
use App\Criteria\CompanyProfileCriteria;
use App\Criteria\DistinctBranchByCompanyCriteria;
use App\Criteria\DistinctCompanyCriteria;
use App\Criteria\DistinctDepartmentByBranchCriteria;
use App\Criteria\JoinCompanyBranchCriteria;
use App\Criteria\JoinLookupValuesCriteria;
use App\Criteria\ParentEntityByGroupBusinessClassCriteria;
use App\Criteria\ParentEntityCriteria;
use App\Events\EntityMainBpWasCreated;
use App\Events\EntitySubBpWasDeleted;
use App\Http\Requests;
use App\Http\Requests\AuditableEntityRequest;
use App\Models\AeMbp;
use App\Models\AuditableEntitiesBp;
use App\Repositories\AeMbpRepository;
use App\Repositories\AeMbpRepositoryEloquent;
use App\Repositories\AeMbpSbpRepository;
use App\Repositories\AuditableEntitiesActualRepository;
use App\Repositories\AuditableEntitiesBpRepository;
use App\Repositories\AuditableEntityRepository;
use App\Repositories\CompanyBranchRepository;
use App\Services\AdditionalInfoService;
use App\Services\AuditableEntityService;
use App\Services\BusinessProcessService;
use App\Services\CosoService;
use App\Services\LookupService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Yajra\Datatables\Datatables;
use App\Repositories\AeMbpActualRepository;

class AuditableEntityController extends Controller
{
    const VIEW_PATH = 'auditable_entities';
    const ENTITY_TYPE = 'Actual';
    private $_repository;
    private $repository;
    private $actual_entity;
    private $lookup_codes;
    private $lookup;
    private $table;

    function __construct(
        AuditableEntityRepository $auditable_entities,
        LookupService $lookup,
        AuditableEntitiesActualRepository $actual_entity
    )
    {
        $this->_repository = $this->repository = $auditable_entities;
        $this->actual_entity = $actual_entity;
        $this->lookup = $lookup;
        $this->lookup_codes = [
            'auditable_entities.branch_code' => config('iapms.lookups.branch.code'),
            'auditable_entities.department_code' => config('iapms.lookups.dept_code'),
        ];
    }

    public function index()
    {
        return view(self::VIEW_PATH.'.index')->with(['type' => self::ENTITY_TYPE]);
    }

    public function create(AuditableEntityService $service)
    {
        // get master companies
        $master_companies = $service->getMasterCompanyFromLookup();

        return view(self::VIEW_PATH.'.form', compact('master_companies'));
    }

    public function store(AuditableEntityRequest $request, CompanyBranchRepository $branch)
    {
        $attributes = [
            'auditable_entity_name' => $request->get('name'),
            'company_code' => $request->get('company'),
            'branch_code' => $request->get('branch'),
            'department_code' => $request->get('department_code'),
            'contact_name' => $request->get('contact_name'),
            'entity_head_name' => $request->get('head_name'),
        ];
        $auditable_entity = $this->actual_entity->createEntity($attributes);
        // update branch status
        $branch->updateStatus($request->get('company_id'), $request->get('branch'), $request->get('active'));

        return response()->json([
            'success' => true,
            'redirect' => route('auditable_entities.edit', ['id' => $auditable_entity->auditable_entity_id]),
        ]);
    }

    public function edit($id, AuditableEntityService $service)
    {
        $lookup = ['auditable_entities_actual.company_code' => config('iapms.lookups.company')] ;
        $this->actual_entity->pushCriteria(new CompanyProfileCriteria());
        $this->actual_entity->pushCriteria(new JoinCompanyBranchCriteria(true));
        $this->actual_entity->pushCriteria(new JoinLookupValuesCriteria( $lookup, '', true ));
        $this->actual_entity->pushCriteria(new JoinLookupValuesCriteria( [
            'auditable_entities_actual.branch_code' => config('iapms.lookups.branch.code'),
            'auditable_entities_actual.department_code' => config('iapms.lookups.dept_code'),
            'company_branches.branch_zone' => config('iapms.lookups.branch.zone'),
        ] ));
        $details = $this->actual_entity->find($id);

        // business processes base on master company
        $bp = $details->master->bps()->PrcessDetails()->get();

        // get master companies
        $master_companies = $service->setMasterCompanies()->master_companies;

        return view(self::VIEW_PATH.'.form', compact('details', 'master_companies', 'bp'))->with(['type' => self::ENTITY_TYPE]);
    }

    public function update($id, Request $request, CompanyBranchRepository $branch)
    {
        $attributes = [
            'contact_name' => $request->get('contact_name'),
            'entity_head_name' => $request->get('head_name'),
        ];
        $entity = $this->actual_entity->find($id);
        $entity->update($attributes);

        // update branch status
        $branch->updateStatus($request->get('company_id'), $request->get('branch'), $request->get('active'));

        $request->session()->flash('message', 'Auditable entity is successfully updated.');

        return response()->json(['success' => true]);
    }

    public function destroy($id)
    {
        $this->actual_entity->delete($id);

        return response()->json(['success' => true]);
    }

    #Note: new method to get entities, must implement. @getAuditableEntityList
    public function lists( Request $request )
    {
        $entities = [];
        $search_query = array('search', 'entity_search');
        if( count(array_intersect($search_query, array_keys($request->all()))) ) {
            if( $request->get('is_parent') == true ) {
                $this->actual_entity->pushCriteria(new AuditableEntityParentCriteria());
            }
            if( $request->get('ra') == true ) $this->actual_entity->pushCriteria(new AuditableEntityExcludeCompanyBranchCriteria());

            $this->actual_entity->pushCriteria(new CompanyProfileCriteria());
            $this->actual_entity->pushCriteria(new JoinCompanyBranchCriteria(true));
            $this->actual_entity->pushCriteria(new JoinLookupValuesCriteria(
                ['auditable_entities_actual.branch_code' => config('iapms.lookups.branch.code')]
            ));
            $this->actual_entity->pushCriteria(new ActualEntityGroupByCompanyCodeCriteria());
            $entities = $this->actual_entity->all();
        }

        return response()->json($entities);
    }

    /**
     * Switch repository based on \Request::get('type')
     * '' || master && actual
     */
    private function getRepository()
    {
        $entity_table = 'auditable_entities';
        if(\Request::get('type') == 'actual') {
            $this->_repository = $this->actual_entity;
            $this->table = $entity_table.'_actual';
        }
        else {
            $this->_repository = $this->repository;
            $this->table = $entity_table;
        }
    }

    # Old method. Use @lists method instead.
    public function getAuditableEntityList(Request $request)
    {
        if( $request->get('search')['value'] != '' ) {
            $this->actual_entity->popCriteria(app(RequestCriteria::class));
            $this->actual_entity->pushCriteria(new CompanyProfileCriteria());
            $this->actual_entity->pushCriteria(new JoinCompanyBranchCriteria(true));
            $this->actual_entity->pushCriteria(new JoinLookupValuesCriteria(
            ['auditable_entities_actual.company_code' => config('iapms.lookups.company'), 'auditable_entities_actual.branch_code' => config('iapms.lookups.branch.code'), 'auditable_entities_actual.department_code' => config('iapms.lookups.dept_code')],
            '',
            true,
            true
            ));
            $entities = $this->actual_entity->all();

            return Datatables::of($entities)->make(true);
        }
        else
            return response()->json(['data' => [], 'recordsFiltered' => 0]);
    }

    public function getAuditableEntityBp($auditable_ent_id, BusinessProcessService $bp_service)
    {
        #NOTE: instead of using this method, I call the process underneath
        # to get the same output needed
        $bp = null;
        $auditable_entities = $this->repository->find($auditable_ent_id);
        if( !is_null($auditable_entities) ) {
            $bp = $bp_service->getAuditableEntityBp($auditable_entities);
        }
        if( \Request::get('format') == 'json' )
            return response()->json(['success' => true, 'data' => $bp]);
        else
            return Datatables::of($bp)->make(true);
    }

    public function getAuditableEntityParents()
    {
        $this->repository->pushCriteria(new AuditableEntityParentCriteria());
        $parents = $this->repository->all();

        return response()->json($parents);
    }

    public function storeAuditableEntityBp($auditable_ent_id, Request $request)
    {
        $auditable_ent = $this->repository->find($auditable_ent_id);

        $bps = array_unique($request->get('bps'));

        $auditable_ent->businessProcess()->syncWithoutDetaching($bps);

        return response()->json(['success' => true]);
    }

    public function deleteAuditableEntityBp($auditable_ent_id, Request $request)
    {
        $auditable_ent = $this->repository->find($auditable_ent_id);
        $bp_id = $request->get('business_process');
        $auditable_ent->businessProcess()->detach($bp_id);

        event(new EntitySubBpWasDeleted($auditable_ent, $bp_id));

        return response()->json(['success' => true]);
    }

    public function getInquiry(Request $request)
    {
        $tree = null;
        $filters = $request->get('filter');
        if( $filters || $request->get('id') )
            $tree = $this->repository->processInquiry()['data'];

        if( $request->get('json') == true )
            return response()->json( $tree );
        else {
            $coso = app(CosoService::class);
            $coso_components = $coso->getComponents();
            return view( self::VIEW_PATH.'.inquiry.new', compact('tree', 'coso_components'));
        }
    }

    /**
     * Get inquiry tree by request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInquiryTree()
    {
        $response = $this->repository->processInquiry();

        return response()->json( $response['data'] );
    }

    public function putUpdateAeBpCustomMeasures($auditable_ent_id, Request $request)
    {
        $entity = $this->actual_entity->find($auditable_ent_id);
        $attributes = [];
        $cm_values = $request->get('cm_value');
        for($i=0; $i < count($cm_values); $i++) {
            $number = $i+1;
            $attributes['cm_'.$number.'_value'] = $cm_values[$i];
        }
        $entity->masterBp()->where('master_ae_mbp_id', $request->get('bp_id'))->update($attributes);
        $request->session()->flash('message', 'Custom measure is successfully updated.');

        return response()->json(['success' => true]);
    }

    public function postApplyTemplate(Request $request)
    {
        $ae_id = $request->get('ae_id');
        $branches = $request->get('branch_code');

        $entity = $this->actual_entity->find($ae_id);
        $response = $this->actual_entity->makeBranchFromCompany($entity, $branches);

        $request->session()->flash('message', $response);

        return response()->json(['success' => true, 'redirect' => route('auditable_entities')]);
    }

    #TODO: Remove this method - not in use
    public function getBusinessProcessPercentile($ae_id, $bp_id, AuditableEntitiesBpRepository $ae_bp, Request $request, AuditableEntityService $service)
    {
        $index = $request->get('index');
        $value = $request->get('value');
        $cm_values = $request->get('cm_value');

        if( !count($cm_values) ) return response()->json(['success' => false]); // if no custom measure return
        $entity = $this->actual_entity->find($ae_id);
        $return = $this->actual_entity->getCustomMeasurePercentile($entity, $cm_values);

        return response()->json(['success' => true, 'data' => $return]);
    }

    public function putUpdateAdditionalInformation($id, AdditionalInfoService $additional_info)
    {
        $this->getRepository();
        $ae = $this->_repository->find($id);

        $additional_info->saveAdditionalInfo($ae);

        return response()->json(['success' => true]);
    }

    #TODO: Remove this method - not in use
    public function getParentEntities(Request $request)
    {
        $group = $request->get('group');
        $business = $request->get('business');
        $entity = $request->get('entity');
        $id = $request->get('id');

        $this->repository->pushCriteria(new AuditableEntityParentCriteria($id));
        $this->repository->pushCriteria(new ParentEntityByGroupBusinessClassCriteria($group, $business, $entity));
        $parents = $this->repository->all();
        return response()->json(['success' => true, 'data' => $parents]);
    }

    #TODO: Remove this method - not in use
    public function getBusinessProcessCustomMeasure($entity_id, $bp_id, AuditableEntityService $service)
    {
        $entity = $this->actual_entity->find($entity_id);
        dd($entity);
//        $cm = $service->getEntityBusinessProcessCustomMeasures($entity, $bp_id);

        return response()->json( ['success' => true, 'data' => $cm ] );
    }

    public function getInquiryFilter(Request $request)
    {
        $company_code = $request->get('company_code');
        $branch_code = $request->get('branch_code');
        if($request->get('by_branch') == true){
            $this->actual_entity->pushCriteria(new ParentEntityCriteria('Y'));
            $data = $this->actual_entity->getBranchCompanies($branch_code);

            return response()->json($data);
        }

        if( $company_code != '') {
            $this->actual_entity->pushCriteria(new JoinLookupValuesCriteria([ 'auditable_entities_actual.branch_code' => config('iapms.lookups.branch.code') ]));
            $this->actual_entity->pushCriteria(new DistinctBranchByCompanyCriteria($company_code));
        }
        elseif( $branch_code != '' ){
            $this->actual_entity->pushCriteria(new DistinctDepartmentByBranchCriteria($branch_code));
        }
        else {
            $this->actual_entity->pushCriteria(new ParentEntityCriteria());
            $this->actual_entity->pushCriteria(new DistinctCompanyCriteria());
        }

        $data = $this->actual_entity->all();

        return response()->json($data);
    }

    /**
     * Get main business process with sub processes
     * @param $entity_id
     * @param AuditableEntitiesBpRepository $entity_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEntityMainBp($entity_id, AuditableEntitiesBpRepository $entity_bp)
    {
        $this->getRepository();
        if( \Request::get('json') != true ) $this->_repository->popCriteria(app(RequestCriteria::class));

        $entity = $this->_repository->find($entity_id);

        if( \Request::get('type') == 'master' )
            $main_bp = $entity_bp->getMainBusinessProcess($entity);
        else {
            $main_bp = $entity_bp->getActualBusinessProcesses($entity->auditable_entity_id);
        }

        if( \Request::get('json') == true ) {
            if( !($main_bp instanceof \Illuminate\Support\Collection) ) {
                $resp = $main_bp->count() ? $main_bp->get() : [];
            }
            else {
                $resp = !is_null($main_bp) ? $main_bp : [];
            }
            return response()->json($resp);
        }
        return Datatables::of(!is_null($main_bp) ? $main_bp : collect([]))->make(true);
    }

    /**
     * Save main business process and sub processes
     * @param $entity_id
     * @param MainBpRequest $request
     * @param AuditableEntitiesBpRepository $entity_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStoreEntityMainBp($entity_id, Requests\MainBpRequest $request)
    {
        $entity = $this->repository->find($entity_id);
        $attributes = ['main_bp_name' => $request->get('main_bp')];
        $custom_measures = $request->get('custom_measure');
        for ($i=0; $i < count($custom_measures); $i++) {
            if( $custom_measures[$i]['name'] != '' && $custom_measures[$i]['value'] != '' ) {
                $number = $i+1;
                $attributes['cm_'.$number.'_name'] = $custom_measures[$i]['name'];
                $attributes['cm_'.$number.'_high_score_type'] = $custom_measures[$i]['value'];
            }
        }
        $master_bp = $entity->masterBp()->create($attributes);
//        trigger event to cascade main bp to actual entities.
        event(new EntityMainBpWasCreated($master_bp));
        return response()->json(['success' => true]);
    }

    /**
     * @param $main_bp_id
     * @param Requests\SubBusinessProcessRequest $request
     * @param AuditableEntitiesBpRepository $entity_bp
     * @param AeMbpRepository $main_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStoreSubProcess($main_bp_id, Requests\SubBusinessProcessRequest $request, AuditableEntitiesBpRepository $entity_bp, AeMbpRepository $main_bp)
    {
        $mainbp = $main_bp->find($main_bp_id);
        $attributes = [
            'mbp_bp_seq' => $request->get('sp_seq'),
            'bp_id' => $request->get('sub_bp'),
        ];
        $entity_bp->createMainBusinessProcess($mainbp, $attributes);

        return response()->json(['success' => true]);
    }

    /**
     * Delete main business process
     * @param Request $request
     * @param AeMbpRepository $main_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMainBusinessProcess(Request $request, AeMbpRepository $main_bp)
    {
        $id = $request->get('id');
        $main_bp->delete($id);

        return response()->json(['success' => true]);
    }

    /**
     * Delete sub process
     * @param Request $request
     * @param AeMbpSbpRepository $sub_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSubBusinessProcess(Request $request, AeMbpSbpRepository $sub_bp)
    {
        $id = $request->get('id');
        $sub_bp->delete($id);

        return response()->json(['success' => true]);
    }

    public function budgetedMandays($ae_id, $mbpa_id, AeMbpActualRepository $mbp_actual_repo, Request $request){
        $audit_type = $request->audit_type;
        $data = $mbp_actual_repo->findBudgetedMandays($mbpa_id, $audit_type);
        return response()->json([
            'success' => true,
            'messsage' => 'Budgeted Mandays for mbp = ' .$mbpa_id,
            'data' => $data
        ]); 
    }

}
