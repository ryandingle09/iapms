<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProjectScopeDarHighlightRepository;
use App\Http\Requests\ProjectScopeDarHighlightsRequest;
use App\Http\Requests;
use Yajra\Datatables\Datatables;

class ProjectScopeDarHighlightController extends Controller
{
	const VIEW_PATH = 'dar';
    private $repository;

    public function __construct(
        ProjectScopeDarHighlightRepository $repository
    	)
    {
        $this->repository = $repository;
    }

    public function store($scopeId, ProjectScopeDarHighlightsRequest $request){

        $count = $this->repository->findWhere([ 'project_scope_id' => $scopeId ])->count();

        $attributes = $request->except(['_token']);
        $attributes['highlights_seq'] = $count + 1;
        $attributes['project_scope_id'] = $scopeId;

        $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Highlight has been saved.'
        ]);
    }

    public function destroy($scopeId, $id){
        $this->repository->delete($id);

        $left = $this->repository->findWhere([ 'project_scope_id' => $scopeId ]);
        foreach ($left as $key => $value) {
           $value->highlights_seq =  $key + 1;
           $value->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'Highlight has been removed.'
        ]);
    }

    public function list($scopeId){
        $highlights = $this->repository->findWhere([ 'project_scope_id' => $scopeId ]);
        return Datatables::of($highlights)->make(true); 
    }
}
