<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\QuestionnaireRequest;
use App\Repositories\QuestionnaireDetailRepository;
use App\Repositories\QuestionnaireRepository;
use App\Services\LookupService;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class QuestionnaireController extends Controller
{
    const VIEW_PATH = 'questionnaire';
    private $repository;
    private $lookup;

    /**
     * Dependency injection
     * @param RiskAssessmentScaleRepository $risk_assessment
     */
    function __construct(QuestionnaireRepository $questionnaire, LookupService $lookup)
    {
        $this->repository = $questionnaire;
        $this->lookup = $lookup;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( self::VIEW_PATH.'.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $config = $this->questionLookup();

        return view( self::VIEW_PATH.'.form', compact('config') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\QuestionnaireRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionnaireRequest $request, QuestionnaireDetailRepository $q_details)
    {
        $attributes = [
            'questionnaire_name' => $request->get('name'),
            'description' => $request->get('description'),
            'questionnaire_type' => $request->get('question_type'),
        ];
        $questionnaire = $this->repository->create($attributes);
        $details = $request->get('questionnaire');
        for($i=0; $i < count($details); $i++ ) {
            $q_details->setAttributes($i, $details[$i])->attachToQuestionnaire($questionnaire);
        }
        return response()->json([ 
            'success' => true , 
            'message' => 'Questionnaire has been created.' 
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $name
     * @return \Illuminate\Http\Response
     */
    public function edit($name)
    {
        $questionnaire = $this->repository
                              ->with('details')
                              ->findByField('questionnaire_name', $name)
                              ->first();
        if( is_null($questionnaire) ) return abort(404);

        $config = $this->questionLookup();

        return view( self::VIEW_PATH.'.form', compact('config', 'questionnaire') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\QuestionnaireRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, QuestionnaireRequest $request, QuestionnaireDetailRepository $q_details)
    {
        $questionnaire = $this->repository->find($id);

        $attributes = [
            'questionnaire_name' => $request->get('name'),
            'description' => $request->get('description'),
            'questionnaire_type' => $request->get('question_type'),
        ];
        // update quesitonnaire details
        $questionnaire->update($attributes);

        // remove all questions
        $questionnaire->details()->delete();

        // re-insert questions so it will get the correct sequence and its details
        $details = $request->get('questionnaire');
        foreach ($details as $seq => $question) {
            $q_details->setAttributes($seq, $question)
                      ->attachToQuestionnaire($questionnaire);
        }

        return response()->json([ 
            'success' => true , 
            'message' => 'Questionnaire has been updated.' 
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $q = $this->repository->find($id);
        $q->delete();

        $request->session()->flash('message', 'Questionnaire successfully deleted.');

        return response()->json(['success' => true]);
    }

    public function deleteQuestion($questionnaire_id, $seq, QuestionnaireDetailRepository $q_details)
    {
        $questionnaire = $this->repository->find($questionnaire_id);
        $q_details->detachToQuestionnaire($questionnaire, $seq);

        return response()->json(['success' => true]);
    }

    public function list()
    {
        $questionnaires = $this->repository->all();

        return Datatables::of($questionnaires)->make(true);
    }

    private function questionLookup()
    {
        return [
            'question_type' => $this->lookup->getLookup( config('iapms.lookups.questionnaire.type') ),
            'response_type' => $this->lookup->getLookup( config('iapms.lookups.questionnaire.response') ),
            'choices' => $this->lookup->getLookup( config('iapms.lookups.questionnaire.choices') ),
            'datatype' => $this->lookup->getLookup( config('iapms.lookups.questionnaire.datatype') ),
        ];
    }
}
