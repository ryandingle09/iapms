<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AeMbpActualRepository;
use App\Criteria\SearchMbpaThroughAuditTypeCriteria;
use App\Http\Requests;

class MainBusinessProccessActualController extends Controller
{	
	protected $repository;

	public function __construct(
		AeMbpActualRepository $repository
	){
		$this->repository = $repository;
	}

	public function index($aea_id)
	{
		$this->repository->pushCriteria( new SearchMbpaThroughAuditTypeCriteria() );
		$data = $this->repository->findWhere([ 'auditable_entity_id' => $aea_id ]);
		
		return response()->json([
            'success' => true,
            'message' => 'MBP list.',
            'data' => $data
        ]);
	}

	public function show($aea_id, $id){
		$data = $this->repository->find( $id );
		return response()->json([
            'success' => true,
            'message' => 'MBP list.',
            'data' => $data
        ]);
	}

    public function list($aea_id, Request $request)
    {
    	
    }
}
