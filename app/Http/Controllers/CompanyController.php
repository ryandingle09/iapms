<?php

namespace App\Http\Controllers;

use App\Criteria\ActiveBranchesCriteria;
use App\Criteria\JoinCompanyBranchCriteria;
use App\Criteria\JoinLookupValuesCriteria;
use App\Events\CompanyWasCreated;
use App\Repositories\AuditableEntityRepository;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CompanyRequest;
use App\Http\Requests\CompanyBranchRequest;
use App\Repositories\CompanyProfileRepository;
use Yajra\Datatables\Datatables;

class CompanyController extends Controller
{
    const VIEW_PATH = 'company';
    private $repository;

    public function __construct(CompanyProfileRepository $company)
    {
        $this->repository = $company;
    }

    /**
     * Display a listing of the resource.
     * @param $company_code string
     * @return \Illuminate\Http\Response
     */
    public function index($company_code = null)
    {
        if( !is_null($company_code) ) {
            $company = $this->repository->findByField('company_code', $company_code)->first();
            return $company;
        }

        return view(self::VIEW_PATH.'.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CompanyRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        $attributes = [
            'company_name' => $request->get('company_name'),
            'company_code' => $request->get('company_code'),
            'description' => $request->get('description'),
        ];
        $company = $this->repository->create($attributes);

        return response()->json(['success' => true, 'redirect' => route('company.details', ['code' => $request->get('company_code')])]);
    }

    /**
     * Get company details
     * @param $code
     * @param AuditableEntityRepository $entity
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function details($code, AuditableEntityRepository $entity, Request $request)
    {
        if( $request->get('tab') == '' ||  $request->get('tab') == 'basic' ) {
            $this->repository->pushCriteria(new JoinLookupValuesCriteria([
                'company_profile.group_type' => config('iapms.lookups.group_type'),
                'company_profile.business_type' => config('iapms.lookups.business_type'),
            ], '', true, true));
        }
        $details = $this->repository->findByField('company_code', $code)->first();
        if(is_null($details)) return abort(404);

        $risks = [];
        $branches = [];
        if( $request->get('tab') == 'audit_info' ) $risks = $entity->getEntityRisksWithAssessment($code);
        if( $request->get('tab') == 'branches' ) {
            $branches = $details->branches()
                                ->select('company_branches.*')
                                ->lookupDetails(config('iapms.lookups.branch.code'),'lv_branch_code', 'company_branches.branch_code')
                                ->lookupDetails(config('iapms.lookups.branch.zone'),'lv_branch_zone', 'company_branches.branch_zone')
                                ->get();
        }

        return view( self::VIEW_PATH.'.form', compact('details', 'risks', 'branches') );
    }

    /**
     * Update the specified details.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $code
     * @return \Illuminate\Http\Response
     */
    public function putUpdateDetails(Request $request, $code)
    {
        $attributes = $request->except(['_token', '_method']);
        $company = $this->repository->findByField('company_code', $code)->first();
        $company->update($attributes);

        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repository->delete($id);
        return response()->json(['success' => true]);
    }

    /**
     * Get company lists
     * @return mixed
     */
    public function lists()
    {
        $companies = $this->repository->all();
        return Datatables::of($companies)->make(true);
    }

    /**
     * Save company branches
     * @param String $id
     * @param CompanyBranchRequest $request
     * @return json
     */
    public function postStoreBranch($id, CompanyBranchRequest $request)
    {
        $company = $this->repository->find($id);
        $attributes = [
            'branch_code' => $request->get('branch_code'),
            'branch_address' => $request->get('branch_address'),
            'branch_store_size' => $request->get('branch_store_size'),
            'branch_zone' => $request->get('branch_zone'),
            'branch_store_class' => $request->get('branch_store_class'),
            'branch_class' => $request->get('branch_class'),
            'entity_class' => $request->get('entity_class'),
            'branch_opening_date' => $request->get('branch_opening_date') != '' ? date('Y-m-d', strtotime($request->get('branch_opening_date'))) : null,
            'active' => $request->get('active'),
        ];

        $company->branches()->create($attributes);

        return response()->json(['success' => true]);
    }

    /**
     * Update company branch details
     * @param CompanyBranchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdateBranch(CompanyBranchRequest $request)
    {
        $company = $this->repository->find($request->get('company_id'));
        $attributes = [
            'branch_address' => $request->get('branch_address'),
            'branch_store_size' => $request->get('branch_store_size'),
            'branch_zone' => $request->get('branch_zone'),
            'branch_store_class' => $request->get('branch_store_class'),
            'branch_class' => $request->get('branch_class'),
            'entity_class' => $request->get('entity_class'),
            'branch_opening_date' => $request->get('branch_opening_date') != '' ? date('Y-m-d', strtotime($request->get('branch_opening_date'))) : null,
            'active' => $request->get('active'),
        ];
        $company->branches()->where('branch_code', $request->get('branch_code'))->update($attributes);

        return response()->json(['success' => true]);
    }

    /**
     * Delete branch by branch code
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteBranch($id, Request $request)
    {
        $company = $this->repository->find($id);
        $company->branches()->where('branch_code', $request->get('branch'))->delete();

        return response()->json(['success' => true]);
    }

    /**
     * Get branches by company code
     * @param $code company code
     * @return mixed
     */
    public function getBranches($code)
    {
        if( \Request::get('all') != true )
            $this->repository->pushCriteria(new ActiveBranchesCriteria());

        $this->repository->pushCriteria(new JoinCompanyBranchCriteria());
        $this->repository->pushCriteria(new JoinLookupValuesCriteria([ 'company_branches.branch_code' => config('iapms.lookups.branch.code') ], true, true));
        $company = $this->repository->scopeQuery(function ($query){
            return $query->select('company_branches.*')->addSelect( \DB::raw('lv_branch_code.lookup_code,lv_branch_code.meaning,lv_branch_code.description') );
        })->findByField('company_code', $code);

        return !is_null($company) ? $company : [];
    }
}
