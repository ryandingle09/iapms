<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectScopeApgFindingsRequest;
use App\Repositories\ProjectScopeApgFindingsRepository;
use App\Http\Requests;

class ProjectScopeApgFindingsController extends Controller
{
	protected $repository;

    public function __construct(
        ProjectScopeApgFindingsRepository $repository
        )
    {
        $this->repository = $repository;
    }

    public function save($apg_id, ProjectScopeApgFindingsRequest $request)
    {

    	$attributes = $request->except(['_token','auditee_action_due_date']);
        $attributes['non_issue'] = $request->non_issue ? 'Y' : 'N';
        $attributes['highlight'] = $request->highlight ? 'Y' : 'N';
        $attributes['project_scope_apg_id'] = $apg_id;
    	$attributes['auditee_action_due_date'] = ymd($request->auditee_action_due_date);

    	//Store or Update
        $find = $this->repository->findWhere([ 'project_scope_apg_id' => $apg_id]);
        if($find->count()){
        	$find = $find->first();
        	$this->repository->update( $attributes, $find->project_scope_apg_finding_id);
        }else{
        	$this->repository->create($attributes);
        }
    	
    	return response()->json([
            'success' => true,
            'message' => 'APG Findings has been saved.'
        ]);
    }

    public function show($apg_id)
    {
        $data = $this->repository->findWhere([
            'project_scope_apg_id' => $apg_id
        ]);
        return response()->json([
            'success' => true,
            'data' => $data->first()
        ]);
    }
}
