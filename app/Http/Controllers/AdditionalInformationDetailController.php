<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AdditionalInformationDetailsRequest;
use App\Repositories\AdditionalInfoDetailRepository;
use App\Services\AdditionalInfoService;
use App\Http\Requests;
use Yajra\Datatables\Datatables;

class AdditionalInformationDetailController extends Controller
{
    protected $repository;

    public function __construct(AdditionalInfoDetailRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store($aiId, AdditionalInformationDetailsRequest $request)
    {
    	$list = $this->repository->findWhere([ 'additional_info_id' => $aiId ])->count();

        $attributes = [
        	'additional_info_id' => $aiId,
            'additional_info_seq' => $list + 1,
            'prompt' => $request->get('prompt'),
            'data_type' => $request->get('data_type'),
            'attribute_assignment' => $request->get('attribute'),
            'value_set_id' => $request->get('value_set'),
            'required' => $request->get('required'),
            'remarks' => $request->get('remarks'),
        ];

        $data = $this->repository->create($attributes);

        return response()->json([ 
            'success' => true , 
            'message' => 'Additional information has been created.' ,
            'data' => $data
        ]);
    }

    public function update($aiId, $id, AdditionalInformationDetailsRequest $request)
    {
        $attributes = [
            'prompt' => $request->get('prompt'),
            'data_type' => $request->get('data_type'),
            'attribute_assignment' => $request->get('attribute'),
            'value_set_id' => $request->get('value_set'),
            'required' => $request->get('required'),
            'remarks' => $request->get('remarks'),
        ];

        $this->repository->update($attributes ,$id );

        return response()->json([ 
            'success' => true , 
            'message' => 'Additional information has been updated.' ,
        ]);
    }

    public function destroy($aiId, $id)
    {
        $this->repository->delete($id);

        return response()->json([ 
            'success' => true , 
            'message' => 'Additional information has been removed.' 
        ]);
    }

    

    public function list($add_info_id, AdditionalInfoDetailRepository $add_info_details)
    {
        $additional_info = $add_info_details->with('valueSet')->findByField('additional_info_id', $add_info_id);
        return Datatables::of($additional_info)->make(true);
    }

    /**
     * Get the additional information by table name
     * @param  string                $table
     * @param  AdditionalInfoService $service
     * @return string | html
     */
    public function getTableDetails($id, $table, AdditionalInfoService $service)
    {
        // get the correct entity table name
        $is_entity = false;
        $orig_table = $e_table = 'auditable_entities';
        $entity = [$e_table, $e_table.'_actual'];
        if( in_array($table, $entity) ) {
            // set master entity table as additional info proxy table
            $orig_table = $table;
            $table = $e_table;
            $is_entity = true;
        }
        $additional_info = $this->repository->findByField('table_name', $table)->first();
        if( !is_null($additional_info) )
            return response()->json(['success' => true, 'data' => $service->generateAdditionalInfo($additional_info, ($is_entity ? $orig_table : $table), $id)]);
        else
            return response()->json(['success' => false, 'message' => 'Additional information is not yet set up. Click <a href="'.route('administrator.lookup').'">here</a> to set up the additional information.' ]);
    }
}
