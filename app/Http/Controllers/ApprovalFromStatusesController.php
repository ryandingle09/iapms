<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ApprovalFromStatusRequest;
use App\Repositories\ApprovalFromStatusRepository;
use Yajra\Datatables\Datatables;

class ApprovalFromStatusesController extends Controller
{

    /**
     * @var ApprovalFromStatusRepository
     */
    protected $repository;

    public function __construct(ApprovalFromStatusRepository $repository)
    {
        $this->repository = $repository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $approvalFromStatuses = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $approvalFromStatuses,
            ]);
        }

        return view('approvalFromStatuses.index', compact('approvalFromStatuses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ApprovalFromStatusRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store($detail_id , ApprovalFromStatusRequest $request)
    {

        $attributes = $request->all();
        $attributes['approval_setup_detail_id'] = $detail_id;
        $status = $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Approval setup detail from status created.',
            'data' => $status
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $approvalFromStatus = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $approvalFromStatus,
            ]);
        }

        return view('approvalFromStatuses.show', compact('approvalFromStatus'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $approvalFromStatus = $this->repository->find($id);

        return view('approvalFromStatuses.edit', compact('approvalFromStatus'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  ApprovalFromStatusRequest $request
     * @param  string            $id
     *
     * @return Response
     */
    public function update($detail_id, ApprovalFromStatusRequest $request, $id)
    {

        $attributes = $request->all();
        $status = $this->repository->update($attributes , $id);

        return response()->json([
            'success' => true,
            'message' => 'Approval setup detail from status updated.',
            'data' => $status
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($detail_id, $id)
    {
        $deleted = $this->repository->delete($id);

        return response()->json([
            'message' => 'Status deleted.',
            'deleted' => $deleted
        ]);
    }

    public function list($detail_id){
        $details = $this->repository->findWhere([
                'approval_setup_detail_id' => $detail_id
            ]);
        return Datatables::of($details)->make(true);
    }
}
