<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\FunctionsRequest;
use App\Models\Functions;
use App\Repositories\FunctionsRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class FunctionController extends Controller
{
	const VIEW_PATH = 'administrator.functions';

	private $function;

	function __construct(FunctionsRepository $function)
	{
		$this->function = $function;
	}

    public function index()
    {
    	return view(self::VIEW_PATH.'.index');
    }

    public function create()
    {
    	return view(self::VIEW_PATH.'.form');
    }

    public function store(FunctionsRequest $request)
    {
    	$attributes = [
    		'function_name' => $request->get('name'),
	    	'description' => $request->get('description'),
	    	'executable_name' => $request->get('exec_name'),
	    	'executable_type' => $request->get('exec_type'),
	    	'effective_start_date' => $request->get('start_date'),
	    	'effective_end_date' => $request->get('end_date'),
	    	'created_by' => \Auth::user()->user_id,
	    	'last_update_by' => \Auth::user()->user_id,
    	];
    	$this->function->create($attributes);

    	$request->session()->flash('message', 'New function has been created!');

    	return response()->json(['success' => true]);
    }

    public function getFunctionList()
    {
    	$functions = Functions::with('updatedBy')->get();

    	return Datatables::of($functions)->make(true);
    }
}
