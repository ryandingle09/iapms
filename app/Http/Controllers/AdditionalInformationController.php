<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\AdditionalInformationRequest;
use App\Models\AdditionalInfoDetail;
use App\Repositories\AdditionalInfoMasterRepository;
use App\Services\AdditionalInfoService;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AdditionalInformationController extends Controller
{
    const VIEW_PATH = 'additional_info';
    private $repository;

    public function __construct(AdditionalInfoMasterRepository $additional_info)
    {
        $this->repository = $additional_info;
    }

    public function index()
    {
        return view(self::VIEW_PATH.'.index');
    }

    public function show($id, AdditionalInfoService $service)
    {
        $additional_info = $this->repository->find($id);
        return $service->generateAdditionalInfo($additional_info);
    }

    public function create()
    {
        return view(self::VIEW_PATH.'.form');
    }

    public function store(AdditionalInformationRequest $request)
    {
        $attributes = [
            'additional_info_name' => $request->get('name'),
            'description' => $request->get('description'),
            'table_name' => $request->get('table'),
        ];

        $info = $this->repository->create($attributes);

        return response()->json([ 
            'success' => true , 
            'message' => 'Additional information has been created.' ,
            'data' => $info
        ]);
    }

    public function edit($id, Request $request)
    {
        $details = $this->repository
                        ->with('details')
                        ->findByField('additional_info_id', $id)
                        ->first();
        if( is_null($details) ) return abort(404);

        return view(self::VIEW_PATH.'.form', compact('details'));
    }

    public function update($id, AdditionalInformationRequest $request)
    {
        $details = $this->repository->find($id);

        $attributes = [
            'additional_info_name' => $request->get('name'),
            'description' => $request->get('description'),
            'table_name' => $request->get('table'),
        ];
        $details->update($attributes);

        return response()->json([ 
            'success' => true , 
            'message' => 'Additional information has been updated.' ,
        ]);
    }

    public function destroy($id, Request $request)
    {
        $this->repository->delete($id);

        return response()->json([ 
            'success' => true , 
            'message' => 'Additional information has been removed.' ,
            'data' => $info
        ]);
    }

    public function list()
    {
        $additional_info = $this->repository->all();
        return Datatables::of($additional_info)->make(true);
    }

    /**
     * Get the additional information by table name
     * @param  string                $table
     * @param  AdditionalInfoService $service
     * @return string | html
     */
    public function getTableDetails($id, $table, AdditionalInfoService $service)
    {
        // get the correct entity table name
        $is_entity = false;
        $orig_table = $e_table = 'auditable_entities';
        $entity = [$e_table, $e_table.'_actual'];
        if( in_array($table, $entity) ) {
            // set master entity table as additional info proxy table
            $orig_table = $table;
            $table = $e_table;
            $is_entity = true;
        }
        $additional_info = $this->repository->findByField('table_name', $table)->first();
        if( !is_null($additional_info) )
            return response()->json(['success' => true, 'data' => $service->generateAdditionalInfo($additional_info, ($is_entity ? $orig_table : $table), $id)]);
        else
            return response()->json(['success' => false, 'message' => 'Additional information is not yet set up. Click <a href="'.route('administrator.lookup').'">here</a> to set up the additional information.' ]);
    }
}
