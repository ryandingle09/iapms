<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ApgCreateRequest;
use App\Http\Requests\ApgUpdateRequest;
use App\Http\Requests\ProjectRequest;
use App\Http\Requests\ProjectScopeRequest ;
use App\Http\Requests\ProjectAuditorCreateRequest;
use App\Http\Requests\ScopeAuditorCreateRequest;
use App\Http\Requests\ChangeProjectStatusRequest;
use App\Repositories\ProjectRepository;
use App\Repositories\ProjectAuditorRepository;
use App\Repositories\ProjectScopeRepository;
use App\Repositories\ProjectScopeAuditorRepository;
use App\Repositories\ProjectScopeApgRepository;
use App\Repositories\AuditableEntitiesActualRepository;
use App\Criteria\SelectTestProcudureCriteria;
use App\Jobs\PopulateProjectScopeApg;
use Yajra\Datatables\Datatables;
use App\Repositories\TestProcRepository;
use Auth,Mail;
class EngamentPlanningController extends Controller
{
    private $project_repo;
    private $project_auditor_repo;
    private $scope_repo;
    private $scope_auditor_repo;
    private $apg_repo;
    private $aea_repo;
    private $test_proc_repo;

    public function __construct(
        ProjectRepository $project_repo,
        ProjectAuditorRepository $project_auditor_repo,
        ProjectScopeRepository $scope_repo,
        ProjectScopeAuditorRepository $scope_auditor_repo,
        ProjectScopeApgRepository $apg_repo,
        AuditableEntitiesActualRepository $aea_repo,
        TestProcRepository $test_proc_repo
    ){
        $this->project_repo = $project_repo;
        $this->project_auditor_repo = $project_auditor_repo;
        $this->scope_repo = $scope_repo;
        $this->scope_auditor_repo = $scope_auditor_repo;
        $this->apg_repo = $apg_repo;
        $this->aea_repo = $aea_repo;
        $this->test_proc_repo = $test_proc_repo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('engagement_planning.index');
    }

    public function projectPlanning(Request $request){
        $search_year = $request->search_year ? $request->search_year : "";
        return view('planning.index', compact("search_year") );   
    }
 
    /**  
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteProject($id,Request $request)
    {
        $this->project_repo->delete($id);

        $request->session()->flash('success_message', 'Project has been removed.');

        return response()->json(['success' => true]);
    }

    public function getProjectAuditor($id){
        $attributes = [
            'project_id' => $id,
            'auditor_id' => $request->auditor,
            'auditor_type' => $request->auditor_type,
            'allotted_mandays' => 0,
        ];
        $auditors = $this->project_auditor_repo->create($attributes);
        
        return response()->json([
            'success' => true,
            'data' => $auditors
        ]);
    }

    public function storeProjectAuditor($id,ProjectAuditorCreateRequest $request){
        $attributes = [
            'project_id' => $id,
            'auditor_id' => $request->auditor,
            'auditor_type' => $request->auditor_type,
            'allotted_mandays' => $request->allotted_mandays,
        ];
        $this->project_auditor_repo->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Project Auditor has been added.',
            'data' => $attributes
        ]);
    }

    public function deleteProjectAuditor($id){
        $projects = $this->project_auditor_repo->find($id);
        $projects->delete();
        return response()->json([
            'success' => true,
            'message' => 'Project Auditor has been removed.'
        ]);
    }
    
    public function getProjectAuditorList($id)
    {
        $projects = $this->project_auditor_repo->with('auditor')->findWhere([
            'project_id' => $id
            ]);
        return Datatables::of($projects)->make(true);
    }

    public function getProjectScopesList($project_id){
        $scopes = $this->scope_repo->with(['auditableEntity','mainBp'])->findWhere(['project_id' => $project_id ]);
        return Datatables::of($scopes)->make(true);
    }

    public function getAllProjectScopesList(){
        $scopes = $this->scope_repo->with(['auditableEntity','mainBp','project','iom'])->all();
        return Datatables::of($scopes)->make(true);   
    }

    public function storeProjectScope($project_id, ProjectScopeRequest $request)
    {

        $project = $this->project_repo->find($project_id);
        $attributes = [
            'scope_sequence' => $request->get('sequence'),
            'auditable_entity_id' => $request->get('auditable_entity'),
            'mbp_id' => $request->get('main_business_process'),
            'audit_location' => $request->get('location'),
            'budgeted_mandays' => $request->get('budgeted_mandays'),
            'target_start_date' => date('Y-m-d',strtotime($request->get('target_start_date') )),
            'target_end_date' => date('Y-m-d',strtotime( $request->get('target_end_date')))
        ];
        $scope = $project->scopes()->create($attributes);

        dispatch(new PopulateProjectScopeApg(
            $request->get('auditable_entity'), 
            $request->get('main_business_process'), 
            $scope->project_scope_id, 
            Auth::user()->user_id 
        ));

        return response()->json( ['success' => true , 'message' => "Scope has been added."]);
    }

    public function deleteProjectScope($scope_id, Request $request)
    {
        $scope = $this->scope_repo->find($scope_id);
        $scope->delete();
        return response()->json(['success' => true , 'message' => 'Project scope has been removed.']);
    }

    public function getScopeAuditorList($scope_id)
    {
        $auditors = $this->scope_auditor_repo->with('auditor')->findWhere(['project_scope_id' => $scope_id ]);
        return Datatables::of($auditors)->make(true);
    }

    public function storeScopeAuditor($scope_id, ScopeAuditorCreateRequest $request)
    {
        $attributes = [
            'project_scope_id' => $scope_id,
            'auditor_id' => $request->auditor,
            'auditor_type' => $request->auditor_type,
            'allotted_mandays' => $request->allotted_mandays,
        ];
        $this->scope_auditor_repo->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Project Scope Auditor has been added.',
            'data' => $attributes
        ]);
    }

    public function deleteScopeAuditor($id){
        $auditor = $this->scope_auditor_repo->find($id);
        $auditor->delete();
        return response()->json([
            'success' => true,
            'message' => 'Project Scope Auditor has been removed.'
        ]);
    }

    public function getApgList($scope_id){
        $apgs = $this->apg_repo->with(['auditor','controlMaster','testProc'])->findWhere(['project_scope_id' => $scope_id ]);
        return Datatables::of($apgs)->make(true);
    }

    public function getApg($project_scope_apg_id){
        $apg = $this->apg_repo->find($project_scope_apg_id);
        return response()->json([
            'success' => true,
            'data' => $apg
        ]);
    }

    public function storeApg($scope_id,ApgCreateRequest $request){
        
        $attributes = $request->only([ 'test_proc_name', 'test_proc_narrative', 'audit_objective', 'budgeted_mandays' ]);
        $test_proc = $this->test_proc_repo->create($attributes);
        $attributes = [
            'project_scope_id' => $scope_id,
            'test_proc_id' => $test_proc->test_proc_id,
            'auditor_id' => $request->auditor,
            'test_proc_narrative' => $test_proc->test_proc_narrative,
            'audit_objective' => $test_proc->audit_objective,
            'budgeted_mandays' => $test_proc->budgeted_mandays,
            'status' => "New",
        ];
        $this->apg_repo->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'APG has been added.',
            'data' => $attributes
        ]);
    }   

    public function updateApg($apg_id,ApgUpdateRequest $request){
        $attributes = [
            'auditor_id' => $request->auditor,
            'budgeted_mandays' => $request->budgeted_mandays,
        ];
        if($request->status == "New"){
            $attributes['test_proc_narrative'] = $request->test_proc_narrative;
            $attributes['audit_objective'] = $request->audit_objective;
        }
        $this->apg_repo->update($attributes,$apg_id);
        return response()->json([
            'success' => true,
            'message' => 'APG has been updated.',
            'data' => $attributes
        ]);
    }

    public function deleteApg($id){
        $apg = $this->apg_repo->find($id);
        $apg->delete();
        return response()->json([
            'success' => true,
            'message' => 'APG has been removed.'
        ]);
    }

    public function getTest(){

        dispatch(new PopulateProjectScopeApg(
            10092, 
            26, 
            15, 
            Auth::user()->user_id 
        ));
        
    }

    public function inquiry(){
        return view('engagement_planning_inquiry.index');
    }
}
