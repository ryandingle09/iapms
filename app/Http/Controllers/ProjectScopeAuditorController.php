<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProjectScopeAuditorRepository;
use App\Http\Requests\ProjectScopeAuditorRequest;
use App\Services\ObjectMapperService;
use Yajra\Datatables\Datatables;
use App\Http\Requests;

class ProjectScopeAuditorController extends Controller
{
    protected $repository;

    public function __construct(
        ProjectScopeAuditorRepository $repository
    ){
        $this->repository = $repository;
    }

    public function index($scope_id, ObjectMapperService $mapper)
    {
        $auditors = $this->repository->with('auditor')->findWhere([
            'project_scope_id' => $scope_id
            ]);

        $auditors = $mapper->setTemplate('AuditorFromWith')->map( $auditors);
       
        return response()->json([
            'success' => true,
            'message' => 'Project Scope Auditor list.',
            'data' => $auditors
        ]);
    }


    public function store($scopeId,ProjectScopeAuditorRequest $request)
    {
    	$attributes = [
            'project_scope_id' => $scopeId,
            'auditor_id' => $request->auditor,
            'auditor_type' => $request->role,
            'allotted_misc_mandays' => $request->allotted_misc_mandays,
        ];
        $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Project Scope Auditor has been added.',
            'data' => $attributes
        ]);
    }

    public function show($scopeId, $id){
        $data = $this->repository->with('auditor')->find($id);
        return response()->json([
            'success' => true,
            'message' => 'Project Scope Auditor has been read.',
            'data' => $data
        ]);
    }

    public function update($scopeId, $id,  ProjectScopeAuditorRequest $request){
        $attributes = [
            'allotted_misc_mandays' => $request->allotted_misc_mandays,
        ];
        $this->repository->update($attributes, $id);

        return response()->json([
            'success' => true,
            'message' => 'Project Scope Auditor has been updated.',
            'data' => $attributes
        ]);
    }

    public function destroy($scopeId,$id){
        $this->repository->delete($id);
        return response()->json([
            'success' => true,
            'message' => 'Project Scope Auditor has been removed.'
        ]);
    }

    public function list($scopeId)
    {

        $auditors = $this->repository->with('auditor')->findWhere([
            'project_scope_id' => $scopeId
            ]);
        return Datatables::of($auditors)->make(true);
    }
}
