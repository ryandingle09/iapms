<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectScopeApprovalRequest;
use App\Http\Requests\ProjectScopeApgApprovalRequest ;
use App\Repositories\ProjectScopeApgApprovalRepository;
use App\Repositories\ProjectScopeApprovalRepository;
use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Auth;

class FieldWorkReviewController extends Controller
{
    const VIEW_PATH = 'fieldwork_review';
    protected $scope_approval_repo;
    protected $apg_approval_repo;

    public function __construct(
        ProjectScopeApprovalRepository $scope_approval_repo,
        ProjectScopeApgApprovalRepository $apg_approval_repo

        )
    {
        $this->scope_approval_repo = $scope_approval_repo;
        $this->apg_approval_repo = $apg_approval_repo;
    }

    public function index()
    {
        return view(self::VIEW_PATH.'.index');
    }

}
