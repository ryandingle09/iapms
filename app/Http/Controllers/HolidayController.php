<?php

namespace App\Http\Controllers;

use App\Criteria\JoinLookupValuesCriteria;
use App\Http\Requests;
use App\Http\Requests\HolidayRequest;
use App\Models\Holiday;
use App\Repositories\HolidayRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class HolidayController extends Controller
{
    const VIEW_PATH = 'auditor.setup.holiday';
    private $repository;

    public function __construct(HolidayRepository $holiday)
    {
        $this->repository = $holiday;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( self::VIEW_PATH.'.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( self::VIEW_PATH.'.form' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  HolidayRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HolidayRequest $request)
    {
        $attributes = $request->except(['token']);
        $attributes['fixed_date'] = $request->fixed_date ? 'Y' : 'N';
        $this->repository->create($attributes);
        $request->session()->flash('success_message', 'New holiday has been created!');
        return response()->json(['success' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $details = $this->repository->find($id);
        return view(self::VIEW_PATH.'.form', compact('details'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  HolidayRequest  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update( $id, HolidayRequest $request )
    {
        $details = Holiday::find($id);

        $attributes = $request->except(['_token', '_method']);
        $attributes['fixed_date'] = $request->get('fixed_date') ? $request->get('fixed_date') : 'N';

        $details->update($attributes);

        $request->session()->flash('success_message', 'Holiday has been updated!');

        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $holiday = Holiday::find($id);
        $holiday->delete();

        $request->session()->flash('success_message', 'Holiday has been deleted!');

        return response()->json(['success' => true]);
    }

    public function getHolidayList()
    {
        $this->repository->pushCriteria(new JoinLookupValuesCriteria([ 'holidays.holiday_type' => config('iapms.lookups.holiday') ]));
        $holidays = $this->repository->all();
        return Datatables::of($holidays)->make(true);
    }
}
