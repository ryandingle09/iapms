<?php

namespace App\Http\Controllers;

use App\Criteria\JoinLookupValuesCriteria;
use App\Criteria\JoinTestControlDetailsCriteria;
use App\Http\Requests;
use App\Http\Requests\ControlDetailsRequest;
use App\Http\Requests\ControlRegistryRequest;
use App\Http\Requests\ControlTestProcedureRequest;
use App\Repositories\ControlsDetailRepository;
use App\Repositories\ControlsMasterRepository;
use App\Repositories\ControlsTestAuditTypeRepository;
use App\Repositories\ControlsTestProcRepository;
use App\Services\AdditionalInfoService;
use App\Services\CosoService;
use App\Services\LookupService;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ControlsRegistryController extends Controller
{
    const VIEW_PATH = 'controls';

    private $repository;
    private $details;
    private $control_test;
    private $test_audit_type;
    private $lookup_codes;
    private $lookup;
    private $service;

    public function __construct(
        ControlsMasterRepository $control_master,
        ControlsDetailRepository $control_detail,
        ControlsTestProcRepository $control_test,
        ControlsTestAuditTypeRepository $test_audit_type,
        LookupService $lookup,
        CosoService $service
    )
    {
        $this->repository = $control_master;
        $this->service = $service;
        $this->details = $control_detail;
        $this->control_test = $control_test;
        $this->test_audit_type = $test_audit_type;
        $this->lookup_codes = [
            'controls_master.control_code' => config('iapms.lookups.control.controls'),
            'controls_master.component_code' => config('iapms.lookups.control.component'),
            'controls_master.principle_code' => config('iapms.lookups.control.principle'),
            'controls_master.focus_code' => config('iapms.lookups.control.focus'),
        ];
        $this->lookup = $lookup;
    }

    public function index()
    {
        return view(self::VIEW_PATH.'.index');
    }

    public function create()
    {
        $assertions = $this->lookup->getLookup(config('iapms.lookups.control.assertions'));
        $types = $this->lookup->getLookup(config('iapms.lookups.control.types'));

        return view(self::VIEW_PATH.'.form', compact('assertions', 'types'));
    }

    public function store(ControlRegistryRequest $request)
    {
        $assertions = serialize($request->get('assertions'));
        $control_types = serialize($request->get('control_types'));
        $attributes = [
            'control_name' => $request->get('control_name'),
            'control_code' => $request->get('control_code'),
            'assertions' => $assertions,
            'control_types' => $control_types,
            'frequency' => $request->get('frequency'),
            'mode' => $request->get('mode'),
            'key_control_flag' => $request->get('key_control_flag'),
            'control_source' => $request->get('control_source'),
        ];
        $control = $this->repository->create($attributes);

        $request->session()->flash('message', 'New control has been created!');

        return response()->json(['success' => true, 'id' => $control->control_id]);
    }

    public function edit($id)
    {
        $coso_components = $this->service->getComponents();

        $assertions = $this->lookup->getLookup(config('iapms.lookups.control.assertions'));
        $types = $this->lookup->getLookup(config('iapms.lookups.control.types'));
        $this->repository->pushCriteria(new JoinLookupValuesCriteria($this->lookup_codes));
        $details = $this->repository->find($id);

        return view(self::VIEW_PATH.'.form', compact('details', 'assertions', 'types', 'coso_components'));
    }

    public function update($id, ControlRegistryRequest $request)
    {
        $control = $this->repository->find($id);

        $assertions = serialize($request->get('assertions'));
        $control_types = serialize($request->get('control_types'));
        $attributes = [
            'assertions' => $assertions,
            'control_types' => $control_types,
            'frequency' => $request->get('frequency'),
            'mode' => $request->get('mode'),
            'key_control_flag' => $request->get('key_control_flag'),
            'control_source' => $request->get('control_source'),
        ];
        $control->update($attributes);

        $request->session()->flash('message', 'Control is successfully updated!');

        return response()->json(['success' => true]);
    }

    public function show($id)
    {
        $lookup_codes = ['controls_detail.control_code' => config('iapms.lookups.control.controls')];
        $this->details->pushCriteria(new JoinLookupValuesCriteria($lookup_codes));
        $details = $this->details->findByField('control_id', $id);

        return Datatables::of($details)->make(true);
    }

    public function destroy($id)
    {
        $this->repository->delete($id);

        return response()->json(['success' => true]);
    }

    public function getControlsList()
    {
        if( \Request::get('response') != 'json' ) $this->repository->popCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $this->repository->pushCriteria(new JoinLookupValuesCriteria($this->lookup_codes));
        $controls = $this->repository->all();

        if( \Request::get('response') != 'json' )
            return Datatables::of($controls)->make(true);
        else
            return response()->json($controls);
    }

    public function getControlDetails($control_id, $control_code)
    {
        $lookup_codes = ['controls_detail.control_code' => config('iapms.lookups.control.controls')];
        $this->details->pushCriteria(new JoinLookupValuesCriteria($lookup_codes));
        $details = $this->details->findWhere([
            'control_id' => $control_id,
            'control_code' => $control_code,
        ])->first();

        if( is_null($details) ) abort(404);

        return view( self::VIEW_PATH.'.details', compact('details') );
    }

    public function postStoreControlDetails(ControlDetailsRequest $request)
    {
        // TODO: Add checking
        $control_id = $request->get('control_id');
        $control = $this->repository->find($control_id);
        $control->details()->create([
            'control_seq' => $request->get('sequence'),
            'control_code' => $request->get('control_code'),
        ]);

        return response()->json(['success' => true]);
    }

    public function putUpdateControlDetails($code, ControlDetailsRequest $request)
    {
        $details = $this->details->findWhere(['control_id' => $request->get('control_id'), 'control_code' => $code])->first();

        $details->control_code = $request->get('control_code');
        $details->control_seq = $request->get('sequence');
        $details->save();

        return response()->json(['success' => true]);
    }

    public function destroyControlDetails($id)
    {
        $this->details->delete($id);

        return response()->json(['success' => true]);
    }

    public function getControlTestProcedure($control_id)
    {
        $this->control_test->pushCriteria(new JoinTestControlDetailsCriteria());
        $procedures = $this->control_test->with(['auditTypes' => function($j){
            $j->leftJoin('lookup_values', 'lookup_code', '=', 'audit_type');
        }])->findByField('control_id', $control_id);

        return Datatables::of($procedures)->make(true);
    }

    public function postStoreControlTestProcedure(ControlTestProcedureRequest $request)
    {
        $attributes = [
            'control_test_seq' => $request->get('sequence'),
            'test_proc_id' => $request->get('test_proc'),
        ];
        $control = $this->repository->find($request->get('control_id'));
        $control->testProcedures()->create($attributes);

        return response()->json(['success' => true]);
    }

    public function putUpdateControlTestProcedure(ControlTestProcedureRequest $request)
    {
        $test_proc = $this->control_test->find($request->get('control_test_id'));

        $attributes = [
            'control_test_seq' => $request->get('sequence'),
            'test_proc_id' => $request->get('test_proc'),
        ];
        $test_proc->update($attributes);

        return response()->json(['success' => true]);
    }

    public function destroyControlTestProcedure($control_test_id)
    {
        $this->control_test->delete($control_test_id);

        return response()->json(['success' => true]);
    }

    public function getControlTestAuditTypes($control_test_id)
    {
        $audit_types = $this->test_audit_type->findByField('control_test_id', $control_test_id);

        return Datatables::of($audit_types)->make(true);
    }

    public function postStoreControlTestProcedureAuditType($test_proc_id, Request $request)
    {
        $this->validate($request, [
            'audit_type' => 'required|unique:controls_test_audit_types,audit_type,null,control_test_id,control_test_id,'.$test_proc_id,
        ]);
        $test_proc = $this->control_test->find($test_proc_id);

        $attributes = [
            'audit_type' => $request->get('audit_type'),
        ];

        $test_proc->auditTypes()->create($attributes);

        $request->session()->flash('message', 'New audit type has been added to control test process!');

        return response()->json(['success' => true]);
    }

    public function destroyControlTestProcedureAuditType($test_proc_id, Request $request)
    {
        $test_proc = $this->control_test->find($test_proc_id);

        $test_proc->auditTypes()
                  ->where('audit_type', $request->get('type'))
                  ->delete();

        $request->session()->flash('message', 'Audit type has been deleted!');

        return response()->json(['success' => true]);
    }

    public function putUpdateAdditionalInformation($id, AdditionalInfoService $additional_info)
    {
        $control = $this->repository->find($id);
        $additional_info->saveAdditionalInfo($control);

        return response()->json(['success' => true]);
    }

    public function putUpdateCosoComponents(Request $request)
    {
        $attributes = [
            'component_code' => json_encode($request->get('components')),
            'principle_code' => $request->get('sel_pr'),
            'focus_code' => $request->get('sel_fo'),
        ];

        $control = $this->repository->find( $request->get('control_id') );
        $control->update($attributes);

        return response()->json(['success' => true]);
    }
}
