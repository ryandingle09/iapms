<?php

namespace App\Http\Controllers;

use App\Criteria\BpObjectiveCriteria;
use App\Criteria\JoinLookupValuesCriteria;
use App\Http\Requests;
use App\Http\Requests\BpStepRiskRequest;
use App\Http\Requests\BusinessProcessRequest;
use App\Http\Requests\BusinessProcessStepRequest;
use App\Repositories\BpObjectiveRepository;
use App\Repositories\BusinessProcessRepository;
use App\Repositories\BusinessProcessesStepRepository;
use App\Repositories\RiskRepository;
use App\Services\AdditionalInfoService;
use App\Services\BusinessProcessService;
use App\Services\LookupService;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class BusinessProcessController extends Controller
{
    const VIEW_PATH = 'business_process';
    private $repository;
    private $bp_steps;
    private $lookup_codes;

    public function __construct(BusinessProcessRepository $business_process, BusinessProcessesStepRepository $bp_steps)
    {
        $this->repository = $business_process;
        $this->bp_steps = $bp_steps;
        $this->lookup_codes = [
            'business_processes.bp_code' => config('iapms.lookups.business_process'),
            'business_processes.source_type' => config('iapms.lookups.source_type'),
        ];
    }

    public function index()
    {
        return view( self::VIEW_PATH.'.index' );
    }

    public function create()
    {
        $lookup = app(LookupService::class);
        $objective_categories = $lookup->getLookupValueByType(config('iapms.lookups.objective.category'))->pluck('meaning');

        return view( self::VIEW_PATH.'.form', compact('objective_categories') );
    }

    public function store(BusinessProcessRequest $request)
    {
        $attributes = [
            'bp_name' => $request->get('bp_name'),
            'bp_code' => $request->get('bp_code'),
            'source_type' => $request->get('source_type'),
            'source_ref' => $request->get('source_ref'),
            'bp_remarks' => $request->get('bp_remarks'),
        ];

        $bp = $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'redirect' => route('business_process.edit', ['bp_id' => $bp->bp_id]),
        ]);
    }

    public function edit($bp_id)
    {
        $lookup = app(LookupService::class);
        $objective_categories = $lookup->getLookupValueByType(config('iapms.lookups.objective.category'))->pluck('meaning');
        $this->repository->pushCriteria(new JoinLookupValuesCriteria( $this->lookup_codes ));
        $details = $this->repository->with(['bpObjectives' => function($w){
            $w->select('bp_objectives.*')
              ->lookupDetails(config('iapms.lookups.objective.category'), 'lv_objective_category', 'bp_objectives.objective_category');
        }])->find($bp_id);

        return view( self::VIEW_PATH.'.form', compact('details', 'objective_categories') );
    }

    public function update($bp_id, BusinessProcessRequest $request)
    {
        $attributes = [
            'objective_category' => $request->get('obj_cat'),
            'source_type' => $request->get('source_type'),
            'source_ref' => $request->get('source_ref'),
            'bp_remarks' => $request->get('bp_remarks'),
        ];

        $this->repository->update($attributes, $bp_id);

        $request->session()->flash('message', 'Business process has been updated!');

        return response()->json([
            'success' => true,
        ]);
    }

    public function destroy($bp_id)
    {
        $this->repository->delete($bp_id);

        return response()->json([
            'success' => true,
        ]);
    }

    public function getProcesses()
    {
        if( \Request::get('transform') ) {
            $this->repository->setPresenter("App\Presenters\BusinessProcessPresenter");
        }
        $this->repository->pushCriteria(new JoinLookupValuesCriteria( $this->lookup_codes ));
        return $this->repository->all();
    }

    public function lists()
    {
        $process = $this->getProcesses();

        return Datatables::of($process)->make(true);
    }

    public function getObjectives($bp_id = '')
    {
        $objective = app(BpObjectiveRepository::class);
        $objective->pushCriteria(new BpObjectiveCriteria($bp_id));
        return $objective->all();
    }

    public function getObjectiveRisks($objective_id, BpObjectiveRepository $objective)
    {
        return $objective->getObjectiveRisks($objective_id);
    }

    public function getSteps($objective_id)
    {
        return $this->bp_steps->findByField('bp_objective_id', $objective_id);
    }

    /**
     * Create business process step
     * @param  integer                      $bp_id   business process ID
     * @param  BusinessProcessStepRequest $request
     * @return json
     */
    public function postStoreStep($objective_id, BusinessProcessStepRequest $request, BpObjectiveRepository $objective)
    {
        $bp_obj = $objective->find($objective_id);
        $attributes = [
            'bp_steps_seq' => $request->get('sequence'),
            'activity_narrative' => $request->get('activity_narrative'),
        ];
        $bp_obj->bpSteps()->create($attributes); // create business process step through relation

        return response()->json(['success' => true]);
    }

    public function putUpdateStep($bp_step_id, BusinessProcessStepRequest $request)
    {
        $bp_step = $this->bp_steps->find($bp_step_id);
        $attributes = [
            'bp_steps_seq' => $request->get('sequence'),
            'activity_narrative' => $request->get('activity_narrative'),
        ];

        $bp_step->update($attributes);

        return response()->json(['success' => true]);
    }

    public function destroyStep($bp_step_id)
    {
        $this->bp_steps->delete($bp_step_id);

        return response()->json([
            'success' => true,
        ]);
    }

    public function getStepRisks($bp_id, $bp_step_id, RiskRepository $risk)
    {
        $bp = $this->repository->find($bp_id);
        $details = $this->bp_steps->find($bp_step_id);

        $risk->setPresenter('App\Presenters\RiskPresenter');
        $risk->pushCriteria(new JoinLookupValuesCriteria(['risks.risk_code' => config('iapms.lookups.risk.risks')]));
        $risks = $risk->all();

        return view( self::VIEW_PATH.'.steps', compact('details', 'risks', 'bp') );
    }

    public function getStepRiskList($bp_step_id)
    {
        $step = $this->bp_steps->find($bp_step_id);
        $risks = $step->bpStepRisk()
        ->leftJoin(\DB::raw('ia_lookup_values as lv_risk'), function($j){
            $j->on(\DB::raw('lv_risk.lookup_code'), '=', 'risk_code')
              ->where(\DB::raw('lv_risk.lookup_type'), '=', config('iapms.lookups.risk.risks'));
        })
        ->addSelect(\DB::raw('ia_risks.*, lv_risk.meaning as lv_risk_meaning, lv_risk.description as lv_risk_desc'))
        ->get();
        return Datatables::of($risks)->make(true);
    }

    public function postStoreStepRisk($bp_step_id, BpStepRiskRequest $request)
    {
        $step = $this->bp_steps->find($bp_step_id);
        $step->bpStepRisk()->attach($request->get('risk'), ['bp_step_risk_seq' => $request->get('sequence')]);

        return response()->json(['success' => true]);
    }

    public function destroyStepRisk($bp_step_id, $risk_id)
    {
        $step = $this->bp_steps->find($bp_step_id);
        $step->bpStepRisk()->detach($risk_id);

        return response()->json([
            'success' => true,
        ]);
    }

    public function postStoreObjective($bp_id, Requests\BpObjectiveRequest $request)
    {
        $bp = $this->repository->find($bp_id);

        $attributes = $request->only([ 'objective_name', 'objective_narrative', 'objective_category', ]);
        $bp->bpObjectives()->create($attributes);

        return response()->json([
            'success' => true,
        ]);
    }

    public function putUpdateObjective($bp_obj_id, Requests\BpObjectiveRequest $request, BpObjectiveRepository $objective)
    {
        $bp_obj = $objective->find($bp_obj_id);

        $attributes = $request->only([ 'objective_name', 'objective_narrative', 'objective_category', ]);
        $bp_obj->update($attributes);

        return response()->json([
            'success' => true,
        ]);
    }

    public function destroyObjective($bp_obj_id, BpObjectiveRepository $objective)
    {
        $bp_obj = $objective->find($bp_obj_id);
        $bp_obj->delete();

        return response()->json([
            'success' => true,
        ]);
    }

    public function putUpdateAdditionalInformation($id, AdditionalInfoService $additional_info)
    {
        $bp = $this->repository->find($id);
        $additional_info->saveAdditionalInfo($bp);

        return response()->json(['success' => true]);
    }

    public function putUpdateStepAdditionalInformation($id, AdditionalInfoService $additional_info)
    {
        $bp_step = $this->bp_steps->find($id);
        $additional_info->saveAdditionalInfo($bp_step);

        return response()->json(['success' => true]);
    }

    public function getBusinessProcessTestProcMandays($bp_id, BusinessProcessService $bp_service)
    {
        $response = $bp_service->getBudgetedMandays($bp_id)[0];
        return response()->json( ['success' => true, 'data' => !is_null($response) ? $response : 0] );
    }
}
