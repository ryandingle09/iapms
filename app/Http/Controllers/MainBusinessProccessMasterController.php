<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AeMbpRepository;
use App\Http\Requests;

class MainBusinessProccessMasterController extends Controller
{
    protected $repository;

	public function __construct(
		AeMbpRepository $repository
	){
		$this->repository = $repository;
	}

	public function index($ae_id)
	{
		$data = $this->repository->findWhere([ 'auditable_entity_id' => $ae_id ]);
		
		return response()->json([
            'success' => true,
            'message' => 'MBP list.',
            'data' => $data
        ]);
	}

	public function show($ae_id, $id){
		$data = $this->repository->find( $id );
		return response()->json([
            'success' => true,
            'message' => 'MBP list.',
            'data' => $data
        ]);
	}

    public function list($ae_id, Request $request)
    {
    	
    }
}
