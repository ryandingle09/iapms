<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectScopeApgApprovalRequest;
use App\Repositories\ProjectScopeApgApprovalRepository;
use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Auth;

class ProjectScopeApgApprovalsController extends Controller
{
    protected $repository;

    public function __construct(
        ProjectScopeApgApprovalRepository $repository
        )
    {
        $this->repository = $repository;
    }

    public function store($apgId, ProjectScopeApgApprovalRequest  $request){
        $list = $this->repository->findWhere([ 'apg_id' => $apgId ])->count();

        $attributes = $request->except(['_token']);
        $attributes['apg_id'] = $apgId;
        $attributes['approval_seq'] = ($list + 1);
        $attributes['auditor_id'] = Auth::user()->user_id;
        $attributes['status_date'] = date('Y-m-d');

        $approval = $this->repository->create($attributes);

        $approval->apg->status = $request->status_to;
        $approval->apg->save();

        return response()->json([
            'success' => true,
            'message' => 'Project Scope APG Approval has been saved.'
        ]);
    }

    public function list($apgId){
        $apg_approvals = $this->repository->findWhere([ 'apg_id' => $apgId ]);
        return Datatables::of($apg_approvals)->make(true); 
    }
}
