<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\AuditableEntitiesActualRepository;
use App\Repositories\AeMbpActualRepository;
use App\Repositories\PlanRepository;
use App\Repositories\PlanProjectRepository;
use App\Criteria\EntityFullSearchGroupByCriteria;
use App\Services\AdjustedSeverityValueService;
use App\Http\Requests;
use App\Http\Requests\CheckPlanGuideRequest;
use Yajra\Datatables\Datatables;

class PlanGuideController extends Controller
{

    protected $ae_actual_repository;
    protected $plan_repository;
    protected $plan_project_repository;
    protected $asv_service;
	protected $mbpa_repository;


    public function __construct(
        AuditableEntitiesActualRepository $ae_actual_repository,
        PlanRepository $plan_repository,
        PlanProjectRepository $plan_project_repository,
    	AdjustedSeverityValueService $asv_service,
        AeMbpActualRepository $mbpa_repository
    ){
        $this->ae_actual_repository = $ae_actual_repository;
        $this->plan_repository = $plan_repository;
        $this->plan_project_repository = $plan_project_repository;
        $this->asv_service = $asv_service;
    	$this->mbpa_repository = $mbpa_repository;
    }

    public function index(){
    	return view( 'plan_guide.index' ); 
    }

    public function checkPlan(CheckPlanGuideRequest $request){
        $data = $this->plan_repository->findWhere([ "plan_year" => $request->audit_year ])->first();
        return response()->json([
            'success' => true,
            'message' => 'No Error.',
            'data' => $data
        ]);
    }


    public function searchData(Request $request){

        $this->ae_actual_repository->pushCriteria(new EntityFullSearchGroupByCriteria() );
    	$data = $this->ae_actual_repository->fullSearch();

        $prev_conditions = [];
        $prev_asv = 0;
        foreach ($data as $item) {
            $conditions = [];

            if( array_key_exists( 'auditable_entity_id' , $item->toArray() ) ){
                $conditions['auditable_entity_id'] = $item->auditable_entity_id;
            }

            if( array_key_exists( 'ae_mbp_id' , $item->toArray() ) ){
                $conditions['ae_mbp_id'] = $item->ae_mbp_id;
            }

            if( array_key_exists( 'risk_id' , $item->toArray() ) ){
                $conditions['risk_id'] = $item->risk_id;
            }

            if( $prev_conditions === $conditions ){
                $item->severity_value = $prev_asv;
            }else{
                $prev_conditions = $conditions;
                $prev_asv = $this->asv_service->getAdjustedSeverityValue($conditions);
                $item->severity_value =   $prev_asv;
            }
        }
        // dd($data->toArray());
        return response()->json([
            'success' => true,
            'message' => 'Completed.',
            'data' => $data
        ]);
    }

    public function addToProject(Request $request){

        $service  = app('App\Services\MandaysService');
        $selected_mbpa =  collect( json_decode($request->cart_data) ) ;

        $plan_project = $this->plan_project_repository->find( $request->plan_project_id );
        $count = $plan_project->scopes->count();

        $selected_mbpa->each(function( $value ,$key ) use ( $count , $plan_project, $service ) {

            $mbpa = \App\Models\AeMbpActual::find( $value->mbpa_id );

            $num = $service->getMbpBudgetedMandays( $mbpa->master_ae_mbp_id , $value->audit_type);

            $check_if_exist =  $plan_project->scopes
                            ->where('auditable_entity_id', $mbpa->auditable_entity_id )
                            ->where('mbp_id',$mbpa->ae_mbp_id )
                            ->count();

            if(!$check_if_exist){

                $scope = new \App\Models\PlanProjectScope([
                    'scope_sequence' => $count + $key + 1,
                    'auditable_entity_id' => $mbpa->auditable_entity_id,
                    'mbp_id' => $mbpa->ae_mbp_id,
                    'audit_location' => "",
                    'budgeted_mandays' => $mbpa->additional_budgeted_mandays_total + $num,
                ]);

                $plan_project->scopes()->save($scope);
            }
        });
        
        return response()->json([
            'success' => true,
            'message' =>  "Scopes has been inserted to project.",
        ]);
    }

    public function cartData(Request $request){

        $service  = app('App\Services\MandaysService');
        $selected_mbpa =  collect( json_decode($request->cart_data) ) ;
        $data = [];

        foreach ($selected_mbpa as $key => $value) {
            $mbpa  = $this->mbpa_repository->with('auditableEntity')->find( $value->mbpa_id );
            $num = $service->getTestProcedureBudgetedMandays( $mbpa->master_ae_mbp_id , $value->audit_type );
            array_push($data, collect( [
                'mbpa_id' => $value->mbpa_id ,
                'auditable_entity_name' => $mbpa->auditableEntity->auditable_entity_name,
                'main_bp_name' => $mbpa->main_bp_name,
                'audit_type' => $value->audit_type ,
                'adjusted_severity_value' => $mbpa->adjusted_severity_value,
                'budgeted_mandays' => $mbpa->additional_budgeted_mandays_total + $num
            ]) );
        }

        return response()->json([
            'success' => true,
            'message' => 'Completed.',
            'data' => $data
        ]);
    }

    public function pasv(Request $request){    
        // $this->asv_service->setYear($request->year);
        // $proccess = $this->asv_service->populateAnnualAsv($request->ae_id);
        // abort(401);
        return response()->json([
            'success' => true,
            'message' =>  "PASV has been executed.",
            'temp' => $proccess
        ]);
    }

    public function projectList(Request $request){
        if(auth()->user()->user_tag == "Auditor" ){
            $project_ids = \App\Models\PlanProjectAuditor::where('auditor_id', auth()->user()->user_tag_ref_id)->pluck('plan_project_id');
            $projects = \App\Models\PlanProject::whereIn('plan_project_id',$project_ids)
                            ->get();
            return Datatables::of($projects)->make(true);
        }else{
            return null;
        }
    }

    public function test(Request $request){
        if(auth()->user()->user_tag == "Auditor" && false){
            $project_ids = \App\Models\PlanProjectAuditor::where('auditor_id', auth()->user()->user_tag_ref_id)->pluck('plan_project_id');
            $projects = \App\Models\PlanProject::whereIn('plan_project_id',$project_ids)->get();
            return Datatables::of($projects)->make(true);
        }else{
            return null;
        }
    }
}
