<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProjectScopeDarRepository;
use App\Models\ProjectScopeDar;
use App\Http\Requests;
use App\Http\Requests\ProjectScopeDarRequest;
use Yajra\Datatables\Datatables;

class ProjectScopeDarController extends Controller
{
    const VIEW_PATH = 'dar';
    private $repository;

    public function __construct(
        ProjectScopeDarRepository $repository
    	)
    {
        $this->repository = $repository;
    }

    public function index($scopeId){

    	$details = $this->repository->findWhere([ 'project_scope_id' => $scopeId]);
        $details = $details->first();

        return view( self::VIEW_PATH.'.index', compact('details' , 'scopeId' ) );
    }

    public function store($scopeId, ProjectScopeDarRequest $request){

        $attributes = $request->except(['_token']);
        $attributes['project_scope_id'] = $scopeId;
        

        //Store or Update
        $find = $this->repository->findWhere([ 'project_scope_id' => $scopeId ]);
        if($find->count()){
            $find = $find->first();
            $this->repository->update( $attributes, $find->project_scope_dar_id);
        }else{
            $this->repository->create($attributes);
        }

        return response()->json([
            'success' => true,
            'message' => 'DAR has been saved.'
        ]);
    }

    public function layout($scopeId){

        $dar = $this->repository->findWhere([ 'project_scope_id' => $scopeId ])->first();
        $html = view('dar.includes.dar_layout',compact( 'dar' ))->render();
        
        // return view('dar.includes.dar_layout',$data);
        return response()->json([ 'success' => true , 'html' => $html ]);
    }

    public function send($scopeId){

        return response()->json([ 'success' => true , 'message' => 'DAR has been sent.' ]);

    }
}
