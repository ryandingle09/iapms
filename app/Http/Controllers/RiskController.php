<?php

namespace App\Http\Controllers;

use App\Criteria\JoinLookupValuesCriteria;
use App\Criteria\RiskJoinControlsCriteria;
use App\Http\Requests;
use App\Http\Requests\RiskControlRequest;
use App\Http\Requests\RiskRequest;
use App\Repositories\ControlsMasterRepository;
use App\Repositories\RiskRepository;
use App\Repositories\RisksControlRepository;
use App\Services\AdditionalInfoService;
use App\Services\CosoService;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class RiskController extends Controller
{
    const VIEW_PATH = 'risks';
    private $repository;
    private $risk_control;
    private $lookup_codes;

    public function __construct(RiskRepository $risk, RisksControlRepository $risk_control)
    {
        $this->repository = $risk;
        $this->risk_control = $risk_control;
        $this->lookup_codes = [
            'risks.risk_code' => config('iapms.lookups.risk.risks'),
            'risks.risk_type' => config('iapms.lookups.risk.type'),
            'risks.risk_source_type' => config('iapms.lookups.risk.source'),
            'risks.risk_response_type' => config('iapms.lookups.risk.response'),
        ];
    }

    public function index()
    {
        return view(self::VIEW_PATH.'.index');
    }

    public function create()
    {
        return view(self::VIEW_PATH.'.form');
    }

    public function store(RiskRequest $request)
    {
        $attributes = [
            'risk_code' => $request->get('risk'),
            'risk_type' => $request->get('risk_type'),
            'risk_remarks' => $request->get('risk_remarks'),
            'risk_source_type' => $request->get('source_type'),
            'risk_response_type' => $request->get('response_type'),
            'def_inherent_impact_rate' => $request->get('def_in_rating_impact'),
            'def_inherent_likelihood_rate' => $request->get('def_in_rating_likelihood'),
            'def_residual_impact_rate' => $request->get('def_res_rating_impact'),
            'def_residual_likelihood_rate' => $request->get('def_res_rating_likelihood'),
            'rating_comment' => $request->get('rating_comment'),
            'enterprise_risk_id' => $request->get('enterprise_risk_id'),
        ];
        $risk = $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'redirect' => route('risks.edit', ['risk_id' => $risk->risk_id]),
        ]);
    }

    public function edit($risk_id, ControlsMasterRepository $control)
    {
        $this->repository->pushCriteria(new JoinLookupValuesCriteria($this->lookup_codes));
        $details = $this->repository->with(['enterpriseRisk' => function($d){
            $d->with(['updater.auditee' => function($a){
                $a->select('auditees.*')
                    ->lookupDetails(config('iapms.lookups.auditee.position'), 'lv_position_code', 'position_code');
                $a->with(['auditableEntity' => function($j){
                    $j->select('auditable_entities_actual.*')
                        ->lookupDetails(config('iapms.lookups.company'), 'lv_company_code', 'company_code')
                        ->lookupDetails(config('iapms.lookups.dept_code'), 'lv_department_code', 'department_code')
                        ->lookupDetails(config('iapms.lookups.branch.code'), 'lv_branch_code', 'branch_code');
                }]);
            }]);
        }])->find($risk_id);

        // get controls
        $control->pushCriteria(new JoinLookupValuesCriteria([
            'controls_master.control_code' => config('iapms.lookups.control.controls'),
            'controls_master.component_code' => config('iapms.lookups.control.component'),
            'controls_master.principle_code' => config('iapms.lookups.control.principle'),
            'controls_master.focus_code' => config('iapms.lookups.control.focus'),
        ]));
        $controls = $control->scopeQuery(function($q){
            return $q->select( \DB::raw('control_id as id, control_code, component_code, principle_code, focus_code, lv_control_code.description as lv_control_code_desc, lv_component_code.description as lv_component_code_desc, lv_principle_code.description as lv_principle_code_desc, lv_focus_code.description as lv_focus_code_desc') );
        })->all();

        $coso = app(CosoService::class);
        $coso_components = $coso->getComponents();

        return view(self::VIEW_PATH.'.form', compact('details', 'controls', 'coso_components'));
    }

    public function update($risk_id, Request $request)
    {
        $attributes = [
            'risk_type' => $request->get('risk_type'),
            'risk_remarks' => $request->get('risk_remarks'),
            'risk_source_type' => $request->get('source_type'),
            'risk_response_type' => $request->get('response_type'),
            'def_inherent_impact_rate' => $request->get('def_in_rating_impact'),
            'def_inherent_likelihood_rate' => $request->get('def_in_rating_likelihood'),
            'def_residual_impact_rate' => $request->get('def_res_rating_impact'),
            'def_residual_likelihood_rate' => $request->get('def_res_rating_likelihood'),
            'rating_comment' => $request->get('rating_comment'),
            'enterprise_risk_id' => $request->get('enterprise_risk_id'),
        ];
        $this->repository->update($attributes, $risk_id);

        return response()->json([ 'success' => true ]);
    }

    public function destroy($id, Request $request)
    {
        $this->repository->delete($id);

        return response()->json([ 'success' => true ]);
    }

    public function getRisksList()
    {
        $this->repository->pushCriteria(new JoinLookupValuesCriteria(['risks.risk_code' => config('iapms.lookups.risk.risks')]));
        $controls = $this->repository->all();
        if( \Request::get('response') == 'json' )
            return response()->json([ 'data' => $controls ]);
        else
            return Datatables::of($controls)->make(true);
    }

    public function getRiskControl($risk_id)
    {
        $this->risk_control->pushCriteria(new RiskJoinControlsCriteria());
        $this->risk_control->pushCriteria(new JoinLookupValuesCriteria([
            'controls_master.control_code' => config('iapms.lookups.control.controls'),
            'controls_master.component_code' => config('iapms.lookups.control.component'),
            'controls_master.principle_code' => config('iapms.lookups.control.principle'),
            'controls_master.focus_code' => config('iapms.lookups.control.focus'),
        ], 'risk_control_id,risk_control_seq,risk_id,'));
        $controls = $this->risk_control->findByField('risk_id', $risk_id);

        return Datatables::of($controls)->make(true);
    }

    public function postStoreRiskControl($risk_id, RiskControlRequest $request)
    {
        $risk = $this->repository->find($risk_id);
        if( is_null($risk) ) return response()->json([ 'success' => false ]); // Risk not exists

        $attributes = [
            'risk_control_seq' => $request->get('control_seq'),
        ];
        $risk->controls()->attach($request->get('controls'), $attributes);

        return response()->json([ 'success' => true ]);
    }

    public function putUpdateRiskControl($id, RiskControlRequest $request)
    {
        $control = $this->risk_control->find($id);

        $attributes = [
            'risk_control_seq' => $request->get('control_seq'),
            'control_id' => $request->get('controls'),
        ];
        $control->update($attributes);

        return response()->json([ 'success' => true ]);
    }

    public function deleteRiskControl($id)
    {
        $control = $this->risk_control->find($id);
        $control->delete();

        return response()->json([ 'success' => true ]);
    }

    public function putUpdateAdditionalInformation($id, AdditionalInfoService $additional_info)
    {
        $risk = $this->repository->find($id);

        $additional_info->saveAdditionalInfo($risk);

        return response()->json(['success' => true]);
    }
}
