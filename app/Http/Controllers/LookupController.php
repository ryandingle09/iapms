<?php

namespace App\Http\Controllers;

use App\Criteria\ExceptCodeCriteria;
use App\Http\Requests;
use App\Http\Requests\LookupRequest;
use App\Http\Requests\LookupValueRequest;
use App\Models\LookupValue;
use App\Repositories\LookupTypeRepository;
use App\Repositories\LookupValueRepository;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

class LookupController extends Controller
{
    const VIEW_PATH = 'administrator.lookup';

    private $lookup_type;
    private $lookup_value;

    function __construct(LookupTypeRepository $lookup_type, LookupValueRepository $lookup_value) {
        $this->lookup_type = $lookup_type;
        $this->lookup_value = $lookup_value;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( self::VIEW_PATH.'.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( self::VIEW_PATH.'.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  LookupRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LookupRequest $request)
    {
        $inputs = $request->except(['_token']);
        $lookup_attr = [
            'lookup_type' => $inputs['type'],
            'meaning' => $inputs['meaning'],
            'description' => $inputs['description'],
        ];
        $lookup_type = $this->lookup_type->create($lookup_attr);

        $request->session()->flash('message', 'Add codes for '.$inputs['type']);

        return response()->json(['success' => true]);
    }

    /**
     * Get lookupValue by lookupType
     * @param  lookup_type  $type
     * @param  Request $request
     * @return json
     */
    public function ajaxLookupValueByType($type, Request $request)
    {
        $lookup_type = $this->lookup_type->findByField('lookup_type', $type)->first();
        if( !is_null($lookup_type) ) {
            $except = json_decode($request->get('except'));
            if( count($except) ) {
                $this->lookup_value->pushCriteria(new ExceptCodeCriteria( $except ));
            }
            $this->lookup_value->setPresenter("App\Presenters\LookupValuePresenter");
            $lookup_value = $this->lookup_value->findByField('lookup_type', $type);

            return $lookup_value;
        }
        return response()->json([ 'data' => [] ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  str  $type
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function edit($type, Request $request)
    {
        $details = $this->lookup_type
                        ->with(['lookupValue'])
                        ->findByField('lookup_type', $type)
                        ->first();

        // check if lookup type exists
        if( ! is_null($details) ) {
            return view( self::VIEW_PATH.'.form', compact('details'));
        }
        else {
            $request->session()->flash('message', 'Lookup type '.$type.' does not exists! Create it here.');
            $request->session()->flash('lookup.populate', $type);

            return redirect(route('administrator.lookup.create'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  LookupRequest  $request
     * @param  str  $type
     * @return \Illuminate\Http\Response
     */
    public function update(LookupRequest $request, $type)
    {
        $inputs = $request->except(['_method', '_token']);
        $lookup_type = $this->lookup_type->find($inputs['lookup_id']);

        $lookup_type->update($inputs);

        $request->session()->flash('message', 'Update is successful!');

        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $this->lookup_type->delete($id);

        // $request->session()->flash('message', 'Lookup successfully deleted!');

        return response()->json(['success' => true]);
    }

    public function getLookupList()
    {
        $lookup = $this->lookup_type
                       ->with(['updatedBy' => function($q){
                            $q->select(['user_id', 'user_name']);
                       }])
                       ->all();
        return Datatables::of($lookup)->make(true);
    }

    public function getLookupValueList($type)
    {
        $lookup = $this->lookup_type->findByField('lookup_type', $type)->first();

        $values = !is_null($lookup) ? $lookup->lookupValue : collect([]);
        return Datatables::of($values)->make(true);
    }

    public function storeLookupValue(LookupValueRequest $request)
    {
        $lookup_type = $this->lookup_type->find( $request->get('lookup_type_id') );
        if( is_null($lookup_type) ) {
            return response()->json(['success' => false, 'message' => 'Lookup type does not exists.']);
        }

        $attributes = [
            'lookup_type' => $lookup_type->lookup_type,
            'lookup_code' => $request->get('code'),
            'meaning' => $request->get('code_meaning'),
            'description' => $request->get('code_description'),
            'effective_start_date' => $request->get('start_date'),
            'effective_end_date' => $request->get('end_date'),
        ];
        $lookup_type->lookupValue()->create($attributes);

        return response()->json(['success' => true]);
    }

    public function putUpdateLookupValue($code, LookupValueRequest $request)
    {
        $lookup_value = LookupValue::where('lookup_code', $request->get('code'))
                                   ->where('lookup_type_id', $request->get('lookup_type_id'));
        if( !$lookup_value->count() ) {
            return response()->json(['success' => false, 'message' => 'Lookup value does not exists.']);
        }

        $attributes = [
            'meaning' => $request->get('code_meaning'),
            'description' => $request->get('code_description'),
            'effective_start_date' => date('Y-m-d', strtotime($request->get('start_date'))),
            'effective_end_date' => date('Y-m-d', strtotime($request->get('end_date'))),
        ];
        $lookup_value->update($attributes);

        return response()->json(['success' => true]);
    }

    public function destroyLookupValue($type, $code, Request $request)
    {
        LookupValue::where('lookup_code', $code)
                   ->where('lookup_type_id', $type)
                   ->delete();

        return response()->json(['success' => true]);
    }
}
