<?php

namespace App\Http\Controllers;

use App\Criteria\ExceptFullNameCriteria;
use App\Criteria\JoinLookupValuesCriteria;
use App\Criteria\SameCompanyCriteria;
use App\Criteria\SameEntityCriteria;
use App\Http\Requests;
use App\Http\Requests\AuditeeRequest;
use App\Repositories\AuditableEntityRepository;
use App\Repositories\AuditeeRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AuditeeController extends Controller
{
    const VIEW_PATH = 'auditee';
    private $repository;

    function __construct(AuditeeRepository $auditee)
    {
        $this->repository = $auditee;
    }

    public function index()
    {
        $this->repository->setPresenter("App\Presenters\AuditeePresenter");
        $data = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'success' => true,
                'message' => 'Auditee has been retrieved.',
                'data' => $data['data']
            ]);
        }

        return view(self::VIEW_PATH.'.index');
    }

    private function setAttributes()
    {
        return [
            'first_name' => \Request::get('first_name'),
            'middle_name' => \Request::get('middle_name'),
            'last_name' => \Request::get('last_name'),
            'employee_id' => \Request::get('employee_id'),
            'auditable_entity_id' => \Request::get('auditable_entity'),
            'position_code' => \Request::get('position'),
            'email_address' => \Request::get('email_address'),
            'supervisor_id' => \Request::get('supervisor'),
            'department_code' => \Request::get('department_code'),
            'effective_start_date' => \Request::get('start_date'),
            'effective_end_date' => \Request::get('end_date'),
        ];
    }

    public function create()
    {
        return view(self::VIEW_PATH.'.form');
    }

    public function store(AuditeeRequest $request)
    {
        $attributes = $this->setAttributes();
        $this->repository->create($attributes);
        $request->session()->flash('message', 'New auditee has been created!');

        return response()->json(['success' => true]);
    }

    public function edit($id)
    {
        $this->repository->pushCriteria(new JoinLookupValuesCriteria( ['position_code' => config('iapms.lookups.auditee.position')], '', false));
        $details = $this->repository->scopeQuery(function ($q){
            return $q->addSelect('auditees.*');
        })
        ->with(['supervisor', 'auditableEntity'])
        ->find($id);

        return view(self::VIEW_PATH.'.form', compact('details' ));
    }

    public function update($id, AuditeeRequest $request)
    {
        $details = $this->repository->find($id);

        $attributes = $this->setAttributes();
        $details->update($attributes);
        $request->session()->flash('message', 'Auditee details has been updated!');

        return response()->json(['success' => true]);
    }

    /**
     * Get auditee by name
     * @param  Request $request [description]
     * @return json
     */
    public function show(Request $request)
    {
        $search = trim($request->get('search'));
        $except = explode(',', trim($request->get('except')));

        // transform result to specific format.
        $this->repository->setPresenter("App\Presenters\AuditeePresenter");
        $this->repository->pushCriteria(new ExceptFullNameCriteria($except));
        $this->repository->pushCriteria(new SameCompanyCriteria($request->get('company')));
        $result = $this->repository->all();
        return $result;
    }

    public function destroy($id, Request $request)
    {
        $this->repository->delete($id);
        $request->session()->flash('message', 'Auditee successfully deleted!');

        return response()->json(['success' => true]);
    }

    public function getAuditeeList(Request $request)
    {
        $this->repository->popCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $this->repository->pushCriteria(new JoinLookupValuesCriteria( ['position_code' => config('iapms.lookups.auditee.position')], '', false));
        $auditees = $this->repository
                         ->scopeQuery(function ($q){
                             return $q->addSelect('auditees.*');
                         })
                         ->with(['supervisor', 'auditableEntity'])
                         ->all();
        if($request->format == 'json'){
            return response()->json([ 'data' => $auditees ]);
        }
        return Datatables::of($auditees)->make(true);
    }
}
