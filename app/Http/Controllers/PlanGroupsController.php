<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PlanGroupRepository;
use App\Http\Requests\PlanGroupRequest;
use App\Criteria\CurrentUserGroupCriteria;
use App\Http\Requests;
use Yajra\Datatables\Datatables;
use Auth;

class PlanGroupsController extends Controller
{
    protected $repository;

    public function __construct(
        PlanGroupRepository $repository
    ){
        $this->repository = $repository;
    }

    public function index($plan_id){
        
        $this->repository->pushCriteria(new CurrentUserGroupCriteria($plan_id));
        $groups = $this->repository->with('groupHeadDetails')->all();

        // $group = $this->repository->findWhere([ 'plan_id' => $plan_id ]);
        return response()->json([
            'success' => true,
            'data' => $groups
        ]);
    }

    public function store($plan_id, PlanGroupRequest $request){
        $attributes = [
            'plan_id' => $plan_id,
            'plan_group_name' => $request->plan_group_name,
            'description' => $request->description,
            'group_head' => $request->group_head
        ];
        $group = $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Plan Group has been created.',
            'data' => $group
        ]);
    }

    public function show($plan_id, $id){
        $details = $this->repository->with(['groupHeadDetails'])->find($id);
        return response()->json([
            'success' => true,
            'data' => $details
        ]);
    }

    public function update($plan_id, $id, PlanGroupRequest $request)
    {
        $attributes = [
            'plan_group_name' => $request->plan_group_name,
            'description' => $request->description,
            'group_head' => $request->group_head
        ];
        $group = $this->repository->update( $attributes, $id);

        return response()->json([
            'success' => true,
            'message' => 'Group has been updated.',
            'data' => $group
        ]);
    }

    public function destroy($plan_id , $id, Request $request)
    {
        $this->repository->delete($id);

        return response()->json([
            'success' => true,
            'message' => 'Group has been removed.'
        ]);
    }
    
    public function list($plan_id){
        $this->repository->pushCriteria(new CurrentUserGroupCriteria($plan_id));
        $groups = $this->repository->with('groupHeadDetails')->all();
        return Datatables::of($groups)->make(true);
    }
}
