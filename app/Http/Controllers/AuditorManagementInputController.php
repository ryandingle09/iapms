<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AuditorManagementInputRequest;
use App\Repositories\AuditorManagementInputRepository;
use App\Repositories\AuditorManagementInputProjectRepository;
use Yajra\Datatables\Datatables;

class AuditorManagementInputController extends Controller
{
    const VIEW = 'auditor_management.management_input.';

    private $repository;
    private $projectRepository;

    public function __construct(AuditorManagementInputRepository $auditorManagementInput, AuditorManagementInputProjectRepository $auditorManagementInputProject)
    {
        $this->repository = $auditorManagementInput;
        $this->projectRepository = $auditorManagementInputProject;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(self::VIEW.'index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuditorManagementInputRequest $request)
    {
        $attributes = [
            'audit_year'    => $request->audit_year,
            'auditor_id'    => $request->auditor,
            'template_name' => $request->template_name,
        ];

        // check if exists auditor in same year
        $check_if_exist_auditor_and_year = $this->repository->findWhere(
            ['audit_year' => $request->audit_year,'auditor_id' => $request->auditor,], 
            ['wbs_header_id'] //select at least one column for fast query just for count porpose
        );

        if(sizeof($check_if_exist_auditor_and_year) !== 0)
            return response()->json(['auditor_already_exists' => ['Auditor already exists in the given year.']], 422);

        $auditor = $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Auditor has been created.'
        ]);
    }

    public function list(Request $request)
    {
        $auditors = $this->repository->with('auditor')->all();
        return Datatables::of($auditors)->make(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($wbs_header_id)
    {
        $auditor = $this->repository->delete($wbs_header_id);
        return response()->json([
            'success' => true,
            'message' => 'Project Auditor has been removed.'
        ]);
    }
}
