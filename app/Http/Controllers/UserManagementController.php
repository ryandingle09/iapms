<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artesaos\Defender\Role;
use Yajra\Datatables\Datatables;
use App\Repositories\ResponsibilityRepository;
use App\Repositories\UserResponsibilityAssignmentRepository;
use App\User;
use App\Http\Requests\UserManagementRequest;
use App\Http\Requests;

class UserManagementController extends Controller
{
    const VIEW_PATH = 'administrator.user_management';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view(self::VIEW_PATH.'.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::with('permissions')->get();

        return view(self::VIEW_PATH.'.forms.user', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserManagementRequest $request, UserResponsibilityAssignmentRepository $user_responsibility)
    {
        $attributes = [
            'user_name' => $request->get('username'),
            'employee_id' => $request->get('emp_id'),
            'password' => $request->get('passw'),
            'description' => $request->get('description'),
            'effective_start_date' => $request->get('start_date'),
            'effective_end_date' => $request->get('end_date'),
        ];
        $new_user = User::create($attributes);

        # Attach roles
        $roles = $request->get('roles');
        $new_user->syncRoles($roles);

        $request->session()->flash('message', 'New user has been created!');

        return response()->json(['success' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  str  $type
     * @return \Illuminate\Http\Response
     */
    public function edit($username, ResponsibilityRepository $responsibility)
    {
        $roles = Role::select('id', 'name')
                     ->with(['permissions' => function($q){
                        $q->select('id', 'name', 'readable_name');
                     }])
                     ->get();
        $details = User::where('user_name', $username)->first();

        // gets the attached roles and permissions
        $permissions = $details->permissions->pluck('id');
        $attached_roles = $details->roles->pluck('id');

        return view(self::VIEW_PATH.'.forms.user', compact('details', 'roles', 'attached_roles', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  str  $username
     * @return \Illuminate\Http\Response
     */
    public function update($username, UserManagementRequest $request, UserResponsibilityAssignmentRepository $user_responsibility)
    {
        $user = User::where('user_name', $username)->first();
        if( !is_null($user) ) {
            $user_attr = [
                'user_name' => $request->get('username'),
                'employee_id' => $request->get('emp_id'),
                'password' => $request->get('passw') != '' ? $request->get('passw') : $user->password,
                'description' => $request->get('description'),
                'effective_start_date' => $request->get('start_date'),
                'effective_end_date' => $request->get('end_date'),
            ];
            $user->update($user_attr);

            /** Defender implementation */
            # Attach roles
            $roles = $request->get('roles');
            $user->roles()->sync($roles);

            $request->session()->flash('message', $username.' details is successfully updated!');

            return response()->json(['success' => true]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $user = User::find($id);
        $user_name = $user->user_name;
        $user->delete();

        $request->session()->flash('message', $user_name.' has been deleted!');

        return response()->json(['success' => true]);
    }

    /**
     * Get all the users
     * @return Datatables
     */
    public function getUserList()
    {
        $users = User::with(['updater', 'roles' => function($r){
            $r->select(\DB::raw('group_concat(name SEPARATOR " | ") as roles'))->groupBy('user_id');
        }])
        ->get();

        if( \Request::get('format') == 'json' )
            return response()->json($users);
        else
            return Datatables::of($users)->make(true);
    }
}
