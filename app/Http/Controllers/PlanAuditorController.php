<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PlanAuditorRepository;
use App\Http\Requests\PlanAuditorRequest;
use Yajra\Datatables\Datatables;
use App\Http\Requests;
use App\Services\MandaysService;
use App\Services\ObjectMapperService;

class PlanAuditorController extends Controller
{
    protected $repository;
    protected $mandays;

    public function __construct(
        PlanAuditorRepository $repository,
        MandaysService $mandays_service
    ){
        $this->repository = $repository;
        $this->mandays = $mandays_service;
    }

    public function index($planId, ObjectMapperService $mapper)
    {

        $auditors = $this->repository->with('auditor')->findWhere([
            'plan_id' => $planId
        ]);

        $auditors = $mapper->setTemplate('AuditorFromWith')->map( $auditors);

        return response()->json([
            'success' => true,
            'message' => 'Plan Auditor list.',
            'data' => $auditors
        ]);
    }


    public function store($planId,PlanAuditorRequest $request)
    {
    	$attributes = [
            'plan_id' => $planId,
            'auditor_id' => $request->auditor
        ];
        $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Plan Auditor has been added.',
            'data' => $attributes
        ]);
    }

    public function list($planId)
    {
        $auditors = $this->repository->with('auditor')->findWhere([
            'plan_id' => $planId
            ]);
        return Datatables::of($auditors)->make(true);
    }

    public function destroy($planId,$id){
        $projects = $this->repository->delete($id);

        // dispatch( new PlanAuditorDeletedCascade( $planId,$id ) );
        
        return response()->json([
            'success' => true,
            'message' => 'Plan Auditor has been removed.'
        ]);
    }

    public function annualMandays($planId, Request $request){
        $year = $request->year ? : date('Y');
        $total_working_days  = $this->mandays->getTotalWorkingDay($year);
        $rate = global_config('auditor_mandays_factor_rate');
        $plan_auditor = $this->repository->findWhere([
            'plan_id' => $planId
        ]);

        foreach ($plan_auditor as $auditor) {
            $leave = $auditor->auditor->leave->sum('total_leave');
            $annual_mandays = ($total_working_days - $leave ) * ( $rate / 100);
            $auditor->annual_mandays = round($annual_mandays);
            $auditor->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'Plan Auditor Annual Mandays has been updated.'
        ]);
    }

    public function mandaysBreakdown($plan_id,$id){
        $data = $this->mandays->getMandaysBreakdown($plan_id,$id);
        return response()->json([
            'success' => true,
            'message' => 'Plan Auditor Breakdown has been retrieved.',
            'data' => $data
        ]);
    }
}
