<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PlanProjectScopeRequest ;
use App\Repositories\PlanProjectScopeRepository;
use App\Jobs\PopulateProjectScopeApg;
use Yajra\Datatables\Datatables;
use App\Http\Requests;
use Auth;

class PlanProjectScopeController extends Controller
{
    protected $repository;

    public function __construct(
        PlanProjectScopeRepository $repository
    ){
        $this->repository = $repository;
    }

    public function store($projectId, PlanProjectScopeRequest $request)
    {
    	$count = $this->repository->findWhere([ 'plan_project_id' => $projectId ])->count();

        $attributes = [
            'plan_project_id' => $projectId,
            'scope_sequence' => $count + 1,
            'auditable_entity_id' => $request->get('auditable_entity'),
            'mbp_id' => $request->get('main_business_process'),
            'audit_location' => $request->get('location'),
            'budgeted_mandays' => $request->get('budgeted_mandays'),
        ];
        $scope = $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Project Scope has been created.'
        ]);
    }

    public function show($projectId, $id){
        $details = $this->repository->with('iom')->find($id);
        return response()->json([
            'success' => true,
            'data' => $details
        ]);
    }

    public function destroy($projectId, $id)
    {
        $this->repository->delete($id);

        // Update sequence of remaining resources
        $left = $this->repository->findWhere([ 'plan_project_id' => $projectId ]);
        foreach ($left as $key => $value) {
           $value->scope_sequence =  $key + 1;
           $value->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'Project Scope has been removed.'
        ]);
    }

    public function list($projectId)
    {
        $scopes = $this->repository
                       ->with(['auditableEntity','mainBp'])
                       ->findWhere(['plan_project_id' => $projectId ]);
        if($projectId == 0){
            $scopes = $this->repository->with(['auditableEntity','mainBp','project','iom'])->all();
        }

        return Datatables::of($scopes)->make(true);
    }

    public function suggestedBudget($project_id, $scope_id)
    {
        $data = $this->repository->find($scope_id);
        $reviewer_mandays = $data->mainBp->reviewer_mandays;
        $return = [
            'Lead' => ( $data->budgeted_mandays - $reviewer_mandays ) ,
            'Reviewer' => $reviewer_mandays
        ];
        return response()->json([
            'success' => true,
            'message' => 'calculated auditors default alloted mandays.',
            'data' => $return
        ]);
    }
}
