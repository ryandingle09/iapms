<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectScopeApprovalRequest;
use App\Repositories\ProjectScopeApprovalRepository;
use App\Http\Requests;
use Auth;

class ProjectScopeApprovalsController extends Controller
{
    protected $repository;

    public function __construct(
        ProjectScopeApprovalRepository $repository
        )
    {
        $this->repository = $repository;
    }

    public function store($scopeId, ProjectScopeApprovalRequest  $request){
        $attributes = $request->except(["_token"]);
        $attributes = [
            'project_scope_id' =>  $scopeId,
            'approval_seq' => 1,
            'status_from' => "New",
            'status_to' => "Approved",
            'auditor_id' => Auth::user()->user_id,
            'remarks' => $request->remarks,
            'role' => "Approver",
            'status_date' => date('Y-m-d')
        ];
        $approvals = $this->repository->create($attributes);

        $approvals->project_scope->project_scope_status = "Approved";
        $approvals->project_scope->save();

        return response()->json([
            'success' => true,
            'message' => 'Project Scope has been approved.',
            'data' => $approvals->project_scope
        ]);
    }

    public function index($scopeId){
        $approval = $this->repository->with('auditor')->findWhere([ 'project_scope_id' => $scopeId]);
        return response()->json([
            'success' => true,
            'data' => $approval->first()
        ]);
    }
}
