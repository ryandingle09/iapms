<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Repositories\UserRepository;
use App\Http\Requests;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
	private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
    	return view('user.index');
    }

    public function create()
    {
    	$roles  = \App\Models\Role::all();
        return view('user.form', compact('roles') );
    }

    public function store(UserCreateRequest $request)
    {
        $attribute = $request->all();
        $attribute['user_tag_ref_id'] = $request->user_tag == "Auditor" ? $request->auditor : $request->auditee;
        $user = $this->repository->create($attribute);

        $roles = $request->get('roles') ? $request->get('roles') : [];
        $user->roles()->sync($roles);

        return response()->json([
            'success' => true,
            'message' => 'User has been created.',
            'data' => $user
        ]);

    }

    public function edit($id)
    {
        $details = $this->repository->find($id);
        $roles  = \App\Models\Role::all();
        return view('user.form', compact('details' , 'roles') );
    }

    public function update($id, UserUpdateRequest $request)
    {
        $attribute = $request->except(['password']);
        $attribute['user_tag_ref_id'] = $request->user_tag == "Auditor" ? $request->auditor : $request->auditee;

        if($request->password){
            $attribute['password'] = $request->password;
        }

        $user = $this->repository->update($attribute, $id);

        $roles = $request->get('roles') ? $request->get('roles') : [];
        $user->roles()->sync($roles);

        return response()->json([
            'success' => true,
            'message' => 'User has been updated.',
            'data' => $user
        ]);

    }

    public function destroy($id)
    {
        $this->repository->delete($id);

        return response()->json([
            'success' => true,
            'message' => 'User has been removed.'
        ]);
    }

    public function list(){
        $users = $this->repository->all();
        return Datatables::of($users)->make(true);
    }
}
