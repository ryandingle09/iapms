<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProjectScopeDarSofRepository;
use App\Http\Requests\ProjectScopeDarFindingsRequest;
use App\Http\Requests;
use Yajra\Datatables\Datatables;

class ProjectScopeDarFindingsController extends Controller
{
    private $repository;

    public function __construct(
        ProjectScopeDarSofRepository $repository
    	)
    {
        $this->repository = $repository;
    }

    public function store($scopeId, ProjectScopeDarFindingsRequest $request){
        $list = $this->repository->findWhere([ 'project_scope_id' => $scopeId ])->count();

        $attributes = $request->except(['_token']);
        $attributes['findings_seq'] = $list + 1;
        $attributes['project_scope_id'] = $scopeId;
        $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'DAR Findings has been saved.'
        ]);
    }

    public function list($scopeId){
        $findings = $this->repository->findWhere([ 'project_scope_id' => $scopeId ]);
        return Datatables::of($findings)->make(true); 
    }

    public function destroy($scopeId, $id){
    	
        $finding = $this->repository->delete($id);

        $left = $this->repository->findWhere([ 'project_scope_id' => $scopeId ]);
        foreach ($left as $key => $value) {
           $value->findings_seq =  $key + 1;
           $value->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'Findings has been removed.'
        ]);
    }
}
