<?php

namespace App\Http\Controllers;

use App\Criteria\AuditableEntityExcludeCompanyBranchCriteria;
use App\Criteria\CompanyProfileCriteria;
use App\Criteria\JoinCompanyBranchCriteria;
use App\Criteria\JoinLookupValuesCriteria;
use App\Events\EntityMainBpWasCreated;
use App\Events\EntityMainBpWasDeleted;
use App\Events\EntitySubBpWasCreated;
use App\Events\EntitySubBpWasDeleted;
use App\Events\MasterEntityDepartmentWasCreated;
use App\Facades\Iapms;
use App\Repositories\AeMbpRepository;
use App\Repositories\AeMbpSbpRepository;
use App\Repositories\AuditableEntitiesActualRepository;
use App\Repositories\AuditableEntitiesBpRepository;
use App\Repositories\AuditableEntityRepository;
use App\Services\AuditableEntityService;
use App\Transformers\CustomMeasureTransformer;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Repository\Criteria\RequestCriteria;
use Yajra\Datatables\Datatables;

class AuditableEntityMasterController extends Controller
{
    const VIEW_PATH = 'auditable_entities';
    const ENTITY_TYPE = 'Master';

    private $repository;
    private $entity_bp;
    private $lookup;

    public function __construct(AuditableEntityRepository $entity, AuditableEntitiesBpRepository $entity_bp)
    {
        $this->repository = $entity;
        $this->entity_bp = $entity_bp;
        $this->lookup = [
            'auditable_entities.company_code' => config('iapms.lookups.master_company'),
            'auditable_entities.branch_code' => config('iapms.lookups.branch.code'),
            'auditable_entities.department_code' => config('iapms.lookups.dept_code'),
        ];
    }

    public function index()
    {
        $data = $this->repository->all();
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }
        return view(self::VIEW_PATH.'.index')->with(['type' => self::ENTITY_TYPE]);
    }

    public function store(Requests\AuditableEntityRequest $request)
    {
        $attributes = [
            'auditable_entity_name' => $request->get('name'),
            'company_code' => $request->get('company'),
            'branch_code' => $request->get('branch'),
            'department_code' => $request->get('department_code'),
            'contact_name' => $request->get('contact_name'),
            'entity_head_name' => $request->get('head_name'),
            'group_type' => $request->get('group_type'),
            'business_type' => $request->get('business_type'),
            'entity_class' => $request->get('entity_class'),
        ];
        try {
            $auditable_entity = $this->repository->create($attributes);
            event(new MasterEntityDepartmentWasCreated($auditable_entity));
            $response = response()->json([
                'success' => true,
                'redirect' => route('auditable_entities.master.edit', ['id' => $auditable_entity->auditable_entity_id]),
            ]);
        }
        catch (\Exception $e) {
            $response = response()->json([
                'success' => false,
                'message' => 'Oops! System has encountered some problem while creating master entity. Please contact support immediately. '.$e->getMessage(),
            ])->setStatusCode(500);
        }

        return $response;
    }

    public function edit($id, AuditableEntityService $service)
    {
        $this->repository->pushCriteria(new JoinLookupValuesCriteria( $this->lookup, '', true ));
        $details = $this->repository->find($id);

        $bp = $details->bps()->PrcessDetails()->get();
        $master_companies = $service->setMasterCompanies()->master_companies;

        return view(self::VIEW_PATH.'.form', compact('details', 'bp', 'master_companies'))->with(['type' => self::ENTITY_TYPE]);
    }

    public function update(Request $request)
    {
        $id = $request->get('id');
        $attributes = [
            'contact_name' => $request->get('contact_name'),
            'entity_head_name' => $request->get('head_name'),
        ];
        $entity = $this->repository->find($id);
        $entity->update($attributes);

        $request->session()->flash('message', 'Auditable entity is successfully updated.');

        return response()->json([ 'success' => true ]);
    }

    public function lists(Request $request)
    {
        $entities = [];
        $search_query = array('search', 'entity_search');
        if( count(array_intersect($search_query, array_keys($request->all()))) ) {
//            $this->repository->pushCriteria(new AuditableEntityExcludeCompanyBranchCriteria());
            $entities = $this->repository->all();
        }
        return response()->json($entities);
    }

    public function destroy($id)
    {
        $this->repository->delete($id);

        return response()->json(['success' => true]);
    }

    /**
     * Get main business process with sub processes
     * @param $id
     * @return mixed
     */
    public function getEntityMainBp($id)
    {
        if( \Request::get('json') == '' ) $this->repository->popCriteria(app(RequestCriteria::class));
        $entity = $this->repository->find($id);
        $main_bp = $this->entity_bp->getMainBusinessProcess($entity);

        if( \Request::get('json') == true ) {
            $resp = !is_null($main_bp) ? $main_bp->get() : [];
            return response()->json($resp);
        }

        return Datatables::of($main_bp)->make(true);
    }

    /**
     * Save main business process and sub processes
     * @param $entity_id
     * @param Requests\MainBpRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStoreEntityMainBp($entity_id, Requests\MainBpRequest $request)
    {
        $entity = $this->repository->find($entity_id);
        $attributes = ['main_bp_name' => $request->get('main_bp')];
        $custom_measures = $request->get('custom_measure');
        for ($i=1; $i < 11; $i++) {
            $name = 'cm_'.$i.'_name';
            $high_score = 'cm_'.$i.'_high_score_type';
            $attributes[$name] = isset($custom_measures[$i]['name']) ? $custom_measures[$i]['name'] : '';
            $attributes[$high_score] = isset($custom_measures[$i]['value']) ? $custom_measures[$i]['value'] : '';
        }

        $master_bp = $entity->masterBp()->create($attributes);
//        trigger event to cascade main bp to actual entities.
        event(new EntityMainBpWasCreated($master_bp));

        return response()->json(['success' => true]);
    }

    /**
     * @param $main_bp_id
     * @param Requests\SubBusinessProcessRequest $request
     * @param AuditableEntitiesBpRepository $entity_bp
     * @param AeMbpRepository $main_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function postStoreSubProcess($main_bp_id, Requests\SubBusinessProcessRequest $request, AuditableEntitiesBpRepository $entity_bp, AeMbpRepository $main_bp)
    {
        $mainbp = $main_bp->find($main_bp_id);
        $attributes = [
            'mbp_bp_seq' => $request->get('sp_seq'),
            'bp_id' => $request->get('sub_bp'),
        ];
        $entity_bp->createMainBusinessProcess($mainbp, $attributes);

        event(new EntitySubBpWasCreated($mainbp->auditableEntity));

        return response()->json(['success' => true]);
    }

    /**
     * @param $main_bp_id
     * @param Request $request
     * @param AeMbpRepository $main_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function putUpdateMainBp($main_bp_id, Request $request, AeMbpRepository $main_bp)
    {
        $mainbp = $main_bp->find($main_bp_id);

        $custom_measures = $request->get('custom_measure');
        for ($i=1; $i < 11; $i++) {
            $name = 'cm_'.$i.'_name';
            $high_score = 'cm_'.$i.'_high_score_type';
            $mainbp->{$name} = isset($custom_measures[$i]['name']) ? $custom_measures[$i]['name'] : '';
            $mainbp->{$high_score} = isset($custom_measures[$i]['value']) ? $custom_measures[$i]['value'] : '';
        }
        $mainbp->save();

        return response()->json(['success' => true]);
    }

    /**
     * Delete main business process
     * @param Request $request
     * @param AeMbpRepository $main_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteMainBusinessProcess(Request $request, AeMbpRepository $main_bp)
    {
        $id = $request->get('id');
        $master_bp = $main_bp->find($id);
        event(new EntityMainBpWasDeleted($master_bp));

        $master_bp->delete();

        return response()->json(['success' => true]);
    }

    /**
     * Delete sub process
     * @param Request $request
     * @param AeMbpSbpRepository $sub_bp
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteSubBusinessProcess(Request $request, AeMbpSbpRepository $sub_bp)
    {
        $id = $request->get('id');
        $sub = $sub_bp->find($id);
        $entity = $sub->masterBp->auditableEntity;
        $sub->delete();

        event(new EntitySubBpWasDeleted($entity, $sub->bp_id));

        return response()->json(['success' => true]);
    }

    public function getCustomMeasure($main_bp, $entity_id, AeMbpRepository $main_bp_repo)
    {
        $data = $main_bp_repo->with(['actual' => function($a) use($entity_id){
            $a->where('auditable_entity_id', $entity_id);
        }])->find($main_bp);

        $response = Iapms::setIncludes('actual')
                         ->transformItem($data, new CustomMeasureTransformer());

        return response()->json($response);
    }

    public function getCustomMeasurePercentile($main_bp, $entity_id, AeMbpRepository $main_bp_repo)
    {
        $data = $main_bp_repo->with(['actual' => function($a) use($entity_id){
            $a->where('auditable_entity_id', $entity_id);
        }])->find($main_bp);

        $response = Iapms::setIncludes('actual')
                         ->transformItem($data, new CustomMeasureTransformer());

        return response()->json($response);
    }
}
