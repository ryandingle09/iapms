<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProjectScopeIomRepository;
use App\Repositories\AuditorRepository;
use App\Models\IomDistList;
use App\Http\Requests\ProjectScopeIomRequest;
use App\Http\Requests;

class ProjectScopeIomController extends Controller
{
    protected $repository;
    protected $auditor_repo;

    public function __construct(
        ProjectScopeIomRepository $repository,
        AuditorRepository $auditor_repo
    	)
    {
        $this->repository = $repository;
        $this->auditor_repo = $auditor_repo;
    }

    public function save($scopeId, ProjectScopeIomRequest $request)
    {
    	$year = date('Y');
        $count = $this->repository->findWhere(
            [
                'project_scope_id' => $scopeId,
                [ 'created_date', '>=' , date('Y').'-01-01' ],
                [ 'created_date', '<=' , (date('Y') + 1 ).'-01-01' ],
            ])->count();

        $iom =  $this->repository->findWhere(['project_scope_id' => $scopeId])->first();
        if($iom){
            $iom->start_date = date('Y-m-d',strtotime($request->start_date));
            $iom->subject = $request->subject;
            $iom->introduction = $request->introduction;
            $iom->background = $request->background;
            $iom->objectives = $request->objectives;
            $iom->auditee_assistance = $request->auditee_assistance;
            $iom->staffing = $request->staffing;
            $iom->save();
        }else{
            $attribute = $request->except(['_token']);
            $attribute['start_date'] = date('Y-m-d',strtotime($request->start_date));
            $attribute['project_scope_id'] = $scopeId;
            $attribute['ref_no'] = "AUD-M-".$year.'-'.($count + 1);
            $memos = $this->repository->create($attribute);
        }
        
        return response()->json(['success' => true , 'message' => 'Inter Office Memo has been saved.' ]);
    }

    public function show($id)
    {
        $details = $this->repository->findWhere([ 'project_scope_id' => $id ]);
        $details = isset($details[0]) ? $details[0] : null ;
        return response()->json(['success' => true , 'data' => $details ]);
    }

    public function layout($scopeId){
    	$data['head'] = $this->auditor_repo->findWhere(['position_code' => "Head"])->first();
        $data['iom'] = $this->repository->findWhere([ 'project_scope_id' => $scopeId ])[0];
        $data['dist_list'] = IomDistList::with([
            'auditee' => function($q) {
                $q->select('auditees.*')
                    ->lookupDetails(config('iapms.lookups.auditee.position'), 'lv_position_code', 'position_code');
            }
        ])->where('project_scope_id',$scopeId)->get();
        $html = view('inter_office_memo.includes.pdf_layout',$data)->render();
        // return view('inter_office_memo.includes.pdf_layout',$data);
        return response()->json([ 'success' => true , 'html' => $html ]);
    }

    public function send($scopeId){
    	// dispatch(new SendMemoEmail($scopeId));

        // $data['head'] = $this->auditor_repo->findWhere(['position_code' => "Head"])->first();
        // $data['iom'] = $this->repository->findWhere([ 'project_scope_id' => $scopeId ])[0];
        // $data['dist_list'] = $this->dist_model->with([
        //         'auditee' => function($q) {
        //             $q->select('auditees.*')
        //                 ->lookupDetails(config('iapms.lookups.auditee.position'), 'lv_position_code', 'position_code');
        //         }
        //     ])->where('project_scope_id',$scopeId)->get();
        // $pdf =  PDF::loadView('inter_office_memo.includes.pdf_layout',$data);
        // return view('inter_office_memo.includes.pdf_layout',$data);
        // return $pdf->download(time().'_iom.pdf');
        // return $pdf->stream(time().'_iom.pdf');

        return response()->json([ 'success' => true , 'message' => 'Inter Office Memo has been sent.' ]);
    }
}
