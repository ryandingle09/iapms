<?php

namespace App\Http\Controllers;

use App\Repositories\TestProcRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Yajra\Datatables\Datatables;

class TestProcedureController extends Controller
{
    Const VIEW_PATH = 'test_proc';
    private $repository;

    public function __construct(TestProcRepository $test_proc)
    {
        $this->repository = $test_proc;
    }

    public function index()
    {
        return view(self::VIEW_PATH.'.index');
    }

    public function store(Requests\TestProceduresRequest $request)
    {
        $attributes = $request->only([ 'test_proc_name', 'test_proc_narrative', 'audit_objective', 'budgeted_mandays', ]);
        $this->repository->create($attributes);

        return response()->json(['success' => true]);
    }

    public function update(Requests\TestProceduresRequest $request)
    {
        $attributes = $request->only([ 'test_proc_name', 'test_proc_narrative', 'audit_objective', 'budgeted_mandays', ]);
        $procedure = $this->repository->find( $request->get('id') );
        $procedure->update($attributes);

        return response()->json(['success' => true]);
    }

    public function destroy(Request $request)
    {
        $procedure = $this->repository->find( $request->get('id') );
        $procedure->delete();

        return response()->json(['success' => true]);
    }

    public function lists()
    {
        $test_proc = $this->repository->all();
        if( \Request::get('format') == 'json' )
            return $test_proc;
        else
            return Datatables::of($test_proc)->make(true);
    }
}
