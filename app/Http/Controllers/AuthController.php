<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests;
use Auth;

class AuthController extends Controller
{
	public function __construct(){

	}

	public function index()
	{
		return view('auth.login');
	}

    public function login(LoginRequest $request)
    {
    	$credentials = $request->only('user_name', 'password');

    	if( Auth::attempt( $credentials, $request->has('remember') ) ){
            $user = Auth::getLastAttempted();
    		$now = time();

            if ( strtotime($user->effective_start_date) < $now && strtotime($user->effective_end_date) > $now) {
                Auth::login($user, $request->has('remember'));
                return redirect()->intended("/");
            } else {
            	Auth::logout();
                $array["user_name"] = ["Account expired/locked." ];
            	return back()->withErrors( $array, 'default')->withInput($request->all());
            }
        }else{
            $array["user_name"] = ["Invalid combination." ];
            return back()->withErrors( $array, 'default')->withInput($request->all());
        }
    }

    public function logout(){
    	Auth::logout();
    	return redirect()->route("auth.index")->with('success_message',"You have logged out.");
    }
}
