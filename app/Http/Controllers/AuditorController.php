<?php

namespace App\Http\Controllers;

use App\Criteria\AuditorActiveCriteria;
use App\Criteria\ExceptFullNameCriteria;
use App\Http\Requests;
use App\Http\Requests\AuditorGradingMatrixRequest;
use App\Http\Requests\AuditorRequest;
use App\Models\Auditor;
use App\Models\AuditorGradingMatrix;
use App\Repositories\AuditorGradingMatrixRepository;
use App\Repositories\AuditorLcmRepository;
use App\Repositories\AuditorProficiencyRepository;
use App\Repositories\AuditorRepository;
use App\Repositories\AuditorTrainingRepository;
use App\Services\LookupService;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AuditorController extends Controller
{
    const VIEW_PATH = 'auditor';
    private $repository;

    public function __construct(AuditorRepository $auditor)
    {
        $this->repository = $auditor;
    }

    public function index()
    {
        return view( self::VIEW_PATH.'.index' );
    }

    public function create(LookupService $lookup)
    {
        return view(self::VIEW_PATH.'.form');
    }

    public function store(AuditorRequest $request)
    {
        
        $attributes = [
            'first_name' => $request->get('first_name'),
            'middle_name' => $request->get('middle_name'),
            'last_name' => $request->get('last_name'),
            'nickname' => $request->get('nickname'),
            'employee_number' => $request->get('employee_number'),
            'email_address' => $request->get('email_address'),
            'gender' => $request->get('gender'),
            'position_code' => $request->get('position'),
            'mobile_number' => $request->get('mobile_number'),
            'supervisor_id' => $request->get('supervisor'),
            'present_address' => $request->get('present_address'),
            'provincial_address' => $request->get('provincial_address'),
            'effective_start_date' => $request->get('start_date'),
            'effective_end_date' => $request->get('end_date'),
        ];
        $auditor = $this->repository->create($attributes);

        return response()->json([ 
            'success' => true , 
            'message' => 'Auditor has been created.' ,
            'data' => $auditor
        ]);
    }

    public function edit($id, LookupService $lookup)
    {
        $details = $this->repository
        ->with([
            'supervisor',
            'proficiency' => function($q) {
                $q->select( 'auditor_proficiency.*' )
                  ->lookupDetails(config('iapms.lookups.auditor.proficiency'), 'lv_proficiency', 'auditor_proficiency.proficiency_type');
            },
            'lcm' => function($q) {
                $q->select( 'auditor_lcm.*' )
                  ->lookupDetails(config('iapms.lookups.auditor.lcm'), 'lv_lcm_type', 'auditor_lcm.lcm_type');
            },
            'training' => function($q) {
                $q->select( 'auditor_training.*' )
                  ->lookupDetails(config('iapms.lookups.auditor.training'), 'lv_training_type', 'auditor_training.training_type');
            }
        ])
        ->find($id);

        $proficiencies = $lookup->getLookupValueByType( config('iapms.lookups.auditor.proficiency') );
        $lcm = $lookup->getLookupValueByType( config('iapms.lookups.auditor.lcm') );
        $trainings = $lookup->getLookupValueByType( config('iapms.lookups.auditor.training') );

        return view(self::VIEW_PATH.'.form', compact('details', 'proficiencies', 'lcm', 'trainings'));
    }

    public function update($id, AuditorRequest $request)
    {
        $attributes = [
            'first_name' => $request->get('first_name'),
            'middle_name' => $request->get('middle_name'),
            'last_name' => $request->get('last_name'),
            'nickname' => $request->get('nickname'),
            'employee_number' => $request->get('employee_number'),
            'email_address' => $request->get('email_address'),
            'gender' => $request->get('gender'),
            'position_code' => $request->get('position'),
            'mobile_number' => $request->get('mobile_number'),
            'supervisor_id' => $request->get('supervisor'),
            'present_address' => $request->get('present_address'),
            'provincial_address' => $request->get('provincial_address'),
            'effective_start_date' => $request->get('start_date'),
            'effective_end_date' => $request->get('end_date'),
        ];
        $this->repository->update($attributes, $id);

        return response()->json([ 
            'success' => true , 
            'message' => 'Auditor has been updated.' 
        ]);
    }

    public function show(Request $request)
    {
        $search = trim($request->get('search'));
        $except = explode(',', trim($request->get('except')));
        // transform result to specific format.
        $this->repository->setPresenter("App\Presenters\AuditorPresenter");
        $this->repository->pushCriteria(new ExceptFullNameCriteria($except));
        $result = $this->repository->all();
        return $result;
    }

    public function destroy($id, Request $request)
    {
        $this->repository->delete($id);
        $request->session()->flash('message', 'Auditor successfully deleted!');

        return response()->json(['success' => true]);
    }

    public function getAuditorList()
    {
        $this->repository->pushCriteria(new AuditorActiveCriteria);
        $auditors = $this->repository->with('supervisor')->skipCriteria()->all();
        // dd($auditors->first()->toArray());
        if( \Request::get('json') != '' && \Request::get('json') == true )
            return response()->json(['success' => true, 'auditors' => $auditors]); // return plain json data
        else
            return Datatables::of($auditors)->make(true); // return datatable json data
    }

    public function getGradingFactor()
    {
        return view(self::VIEW_PATH.'.grading_matrix.index');
    }

    public function postStoreGradingFactor(AuditorGradingMatrixRequest $request, AuditorGradingMatrixRepository $grading_matrix)
    {
        $attributes = [
            'variance_from' => $request->get('variance_from'),
            'variance_to' => $request->get('variance_to'),
            'rating' => $request->get('rating'),
            'quality_max' => $request->get('quality_max'),
            'documentation_max' => $request->get('documentation_max'),
            'remarks' => $request->get('remarks'),
        ];
        $grading_matrix->create($attributes);

        $request->session()->flash('message', 'New grading matrix has been created!');

        return response()->json(['success' => true]);
    }

    public function putUpdateGradingFactor(AuditorGradingMatrixRequest $request)
    {
        $grading_matrix = AuditorGradingMatrix::find($request->get('id'));
        $from = $request->get('orig_from');
        $to = $request->get('orig_to');
        $attributes = [
            'variance_from' => $request->get('variance_from'),
            'variance_to' => $request->get('variance_to'),
            'rating' => $request->get('rating'),
            'quality_max' => $request->get('quality_max'),
            'documentation_max' => $request->get('documentation_max'),
            'remarks' => $request->get('remarks'),
        ];
        $grading_matrix->update($attributes);

        $request->session()->flash('message', 'Grading matrix has been updated!');

        return response()->json(['success' => true]);
    }

    public function getGradingFactorList(AuditorGradingMatrixRepository $grading_matrix)
    {
        $grading_factors = $grading_matrix->all();
        return Datatables::of($grading_factors)->make(true);
    }

    public function destroyGradingFactor($id, Request $request)
    {
        $grading_matrix = AuditorGradingMatrix::find($id);
        $grading_matrix->delete();

        $request->session()->flash('message', 'Grading matrix has been deleted!');

        return response()->json(['success' => true]);
    }

    public function organizationalChart(){
        $data = $this->repository->with('recursiveSubordinate')->findWhere([ 
            'supervisor_id' => 0
        ]);
        $data = $this->iterateOrgchart( $data->toArray() );

        return view( self::VIEW_PATH.'.organizational_chart' , compact('data')  );
    }

    public function iterateOrgchart($items){
        $items = array_map(function($item){
            return collect([
                "name" => $item['full_name'],
                "title" => $item['position_code'],
                "children" => $this->iterateOrgchart($item['recursive_subordinate'])
            ]);
        }, $items);
        return collect($items);
    }

}
