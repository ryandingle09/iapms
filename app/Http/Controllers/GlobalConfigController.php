<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\GlobalConfig;
use Auth;

class GlobalConfigController extends Controller
{
	const VIEW_PATH = 'global_config.';

    public function index()
    {
    	$config = GlobalConfig::first();

        return view(self::VIEW_PATH.'index', compact('config'));
    }
    /*
        $id just for consistency of url
    */
    public function show($id, Request $request){
        $config = GlobalConfig::first();
        if($request->filter){
            $filter = explode(",", $request->filter);
            $config = collect($config->toArray())->only($filter);
        }
        return response()->json([ 'status' => true , "data" => $config ]);
    }

    public function save(Request $request)
    {
    	$this->validate($request, [
            'iom_subject' => "required",
            'iom_introduction' => "required",
            'iom_background' => "required",
            'iom_objectives' => "required",
            'iom_auditee_assistance' => "required",
            'iom_staffing' => "required",
            'auditor_timeliness' => "required|numeric|min:0",
            'auditor_quality' => "required|numeric|min:0",
            'auditor_documentation' => "required|numeric|min:0",
            'auditor_caf' => "required|numeric|min:0",
            'reviewer_timeliness' => "required|numeric|min:0",
            'reviewer_quality' => "required|numeric|min:0",
            'reviewer_documentation' => "required|numeric|min:0",
            'auditor_mandays_factor_rate' => "required|numeric|min:0",
	    ]);
        $agf = $request->auditor_timeliness + $request->auditor_quality + $request->auditor_documentation + $request->auditor_caf;
        if( $agf != 100 ){
            $errors['auditor_timeliness'] = ["AGF Timeliness and other data must sum up to 100%."];
            $errors['auditor_quality'] = ["AGF Quality and other data must sum up to 100%."];
            $errors['auditor_documentation'] = ["AGF Documentation and other data must sum up to 100%."];
            $errors['auditor_caf'] = ["AGF CAF and other data must sum up to 100%."];
            return response()->json($errors,422);
        }
        $rgf = $request->reviewer_timeliness + $request->reviewer_quality + $request->reviewer_documentation;
        if( $rgf != 100 ){
            $errors['reviewer_timeliness'] = ["RGF Timeliness and other data must sum up to 100%."];
            $errors['reviewer_quality'] = ["RGF Quality and other data must sum up to 100%."];
            $errors['reviewer_documentation'] = ["RGF Documentation and other data must sum up to 100%."];
            return response()->json($errors,422);
        }
        
        $attributes = $request->except(['_method','_token']);
        $attributes['created_by'] = Auth::user()->user_id;
        $attributes['created_date'] = date('Y-m-d H:i:s');
        $attributes['last_update_by'] = Auth::user()->user_id;
        $attributes['last_update_date'] = date('Y-m-d H:i:s');

    	GlobalConfig::truncate();
    	GlobalConfig::insert($attributes);

        return response()->json([ 'status' => 1 , "message" => "Config has been updated."]);
    }
}
