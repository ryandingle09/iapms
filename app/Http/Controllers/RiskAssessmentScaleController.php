<?php

namespace App\Http\Controllers;

use App\Criteria\AuditableEntityAttachBusinessProcessCriteria;
use App\Criteria\CompanyProfileCriteria;
use App\Criteria\JoinAuditableEntityParentCriteria;
use App\Criteria\JoinBusinessProcessCriteria;
use App\Criteria\JoinBusinessProcessesStepsCriteria;
use App\Criteria\JoinCompanyBranchCriteria;
use App\Criteria\JoinLookupValuesCriteria;
use App\Criteria\RiskAssessmentJoinAeBpCriteria;
use App\Criteria\RiskAssessmentScaleCriteria;
use App\Criteria\GenerateRiskSeverityValues;
use App\Http\Requests;
use App\Http\Requests\RiskAssessmentScaleRequest;
use App\Models\AuditableEntity;
use App\Models\BpStepsRisk;
use App\Models\BusinessProcess;
use App\Models\Risk;
use App\Models\RiskAssessmentScale;
use App\Repositories\AuditableEntityRepository;
use App\Repositories\BpObjectiveRepository;
use App\Repositories\RiskAssessmentScaleRepository;
use App\Services\AdditionalInfoService;
use App\Services\AuditableEntityService;
use Illuminate\Http\Request;
use Prettus\Repository\Criteria\RequestCriteria;
use Yajra\Datatables\Datatables;

class RiskAssessmentScaleController extends Controller
{
    const VIEW_PATH = 'risk_assessment';
    private $repository;
    private $table;

    /**
     * Dependency injection
     * @param RiskAssessmentScaleRepository $risk_assessment
     */
    function __construct(RiskAssessmentScaleRepository $risk_assessment)
    {
        $this->repository = $risk_assessment;
        $this->table = 'risk_assessment_scale';
    }

    /**
     * Display a listing of the resource.
     *
     * @param AuditableEntityRepository $auditable_entity
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {   
        $type = strtolower($request->type) == 'actual' ? 'Actual' : 'Master' ; 
        return view( self::VIEW_PATH.'.index' , compact("type") );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( self::VIEW_PATH.'.form' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RiskAssessmentScaleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RiskAssessmentScaleRequest $request)
    {
        $attributes = [
            'auditable_entity_id' => $request->get('auditable_entity'),
            'bp_id' => $request->get('business_process'),
        ];
        $this->repository->create($attributes);

        $request->session()->flash('message', 'New risk assessment scale has been created.');
        return response()->json(['success' => true]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer $ae_id   AuditableEntity ID
     * @param  integer $bp_id   BusinessProcess ID
     * @return \Illuminate\Http\Response
     */
    public function edit($ae_id, $bp_id)
    {
        $details = RiskAssessmentScale::with([
            'auditableEntity',
            'businessProcess'
        ])
        ->where('auditable_entity_id', $ae_id)
        ->where('bp_id', $bp_id)
        ->first();

        if( is_null($details) ) return abort(404); // show 404 page if risk assessment does not exists

        return view( self::VIEW_PATH.'.form', compact('details'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RiskAssessmentScaleRequest $request, $id)
    {
        $attributes = $request->all();

        $ras = $this->repository->update($attributes, $id);

        return response()->json([
            'success' => true,
            'message' => 'Risk Assessment Scale has been updated.',
            'data' => $ras
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $ae_id  auditable entity ID
     * @param  int  $bp_id business process ID
     * @return \Illuminate\Http\Response
     */
    public function destroy($ae_id, $bp_id)
    {
        RiskAssessmentScale::where('auditable_entity_id', $ae_id)
                                              ->where('bp_id', $bp_id)
                                              ->delete();

        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteRisk($ae_id, $bp_id, $risk_id)
    {
        $risk_assessment = RiskAssessmentScale::where('auditable_entity_id', $ae_id)
                                              ->where('bp_id', $bp_id)
                                              ->where('risk_id', $risk_id)
                                              ->delete();
        // Remove business process steps risk
        BpStepsRisk::join('business_processes_steps', 'business_processes_steps.bp_steps_id', '=', 'bp_steps_risks.bp_step_id')
                   ->join('bp_objectives', 'bp_objectives.bp_objective_id', '=', 'business_processes_steps.bp_objective_id')
                   ->where('bp_objectives.bp_id', $bp_id)
                   ->where('risk_id', $risk_id)
                   ->delete();

        return response()->json(['success' => true]);
    }

    /**
     * Get all Risk Assessment Scale
     * @param Request $request
     * @return JSON  Shows Auditable Entity and Business Process details
     */
    public function list(Request $request)
    {

        $data = $this->repository
                ->with([
                    'auditableEntity',
                    'mbp',
                    'auditableEntityActual',
                    'mbpa',
                    'risk',
                    'businessProcess',
                    'bpObjective'
                ])
                ->all();

        return Datatables::of($data)->make(true);
    }

    /**
     * Get risks list for specific Auditable Entity and Business Process
     * @param  integer $bp_id   BusinessProcess ID
     * @param  integer $ae_id   AuditableEntity ID
     * @return mixed
     */
    public function getRiskList($ae_id, $bp_id, BpObjectiveRepository $bp_objective)
    {
        $risks = $bp_objective->getRisksAssessmentByEntityBp($ae_id, $bp_id);

        return Datatables::of($risks)->make(true);
    }

    public function putUpdateRisk($ae_id, $bp_id, $risk_id, Request $request)
    {
        // $allowed_attr = ['impact_value', 'likelihood_value', 'remarks']; // update restriction
        $field = $request->get('data_type');

        $attributes = [
            $field => $request->get('data_value') // dynamic $field
        ];
        $risk_assessment = RiskAssessmentScale::where('auditable_entity_id', $ae_id)
                                              ->where('bp_id', $bp_id)
                                              ->where('risk_id', $risk_id);
        if( $risk_assessment->count() ) {
            $risk_assessment->update($attributes);
        }

        return response()->json(['success' => true]);
    }
}
