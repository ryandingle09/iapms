<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Log;

class AdministratorController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function getMigration()
    {
        return view('migration.index');
    }

    /**
     * For data migration purposes only - CSV to Database
     * @param  Request $request
     * @return JSON
     */
    public function postMigration(Request $request)
    {
        $destination_path = public_path('uploads/csv'); // upload path
        $file = $request->file('file');
        $file_name = $file->getClientOriginalName();
        $table = str_replace('.csv', '', $file_name); // remove file extension to get the table name
        $table = env('DB_PREFIX').$table; // attach DB prefix to the table
        try {
            $file->move($destination_path, $file_name); // save the file to local disk
            $path = str_replace('\\', '/', $destination_path."/".$file_name); // for windows: replace the path separator from "\" to "/"
            $handle = fopen($path, 'r'); // read and get the first line of the csv file which is the fields of the table
            if( $handle ) {
                $fields = str_replace("\r\n", '', fgets($handle, 4096) ); // get the first line and replace unnecessary strings to get the actual fields
                fclose($handle); // close file
            }
            else {
                // Failed to open the file
                return response()->json(['success' => false, 'message' => 'Unable to process csv file. There\'s a problem with the file, please check.']);
            }
            // Insert the csv file to database
            \DB::connection()->getPdo()->exec("LOAD DATA LOCAL INFILE '".$path."' INTO TABLE ".$table." FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\\r\\n' IGNORE 1 ROWS (".$fields.")");
            Log::info('[Migration] Upload success');

            return response()->json(['success' => true, 'message' => 'File successfully uploaded.']);
        }
        catch(Exception $e) {
            Log::error('[Migration] '.$e->getMessage());

            return response()->json(['success' => false, 'message' => 'File upload failed.']);
        }
    }
}
