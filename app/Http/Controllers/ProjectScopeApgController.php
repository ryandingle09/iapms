<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProjectScopeApgRepository;
use App\Repositories\TestProcRepository;
use App\Http\Requests\ProjectScopeApgRequest;
use Yajra\Datatables\Datatables;
use App\Http\Requests;


class ProjectScopeApgController extends Controller
{

    protected $repository;
	protected $test_proc_repository;

    public function __construct(
        ProjectScopeApgRepository $repository ,
        TestProcRepository $test_proc_repository
    ){
        $this->repository = $repository;
        $this->test_proc_repository = $test_proc_repository;
    }

    public function store($scope_id ,ProjectScopeApgRequest $request){
        $attributes = [
            'test_proc_name' => $request->test_proc_narrative ,
            'test_proc_narrative' => $request->test_proc_narrative ,
            'audit_objective' => $request->audit_objective ,
            'budgeted_mandays' => $request->allotted_mandays
        ];
        $test_proc = $this->test_proc_repository->create($attributes);

        $attribute = [
            'project_scope_id' => $scope_id,
            'apg_seq' => 1, // TODO
            'main_bp_name' => "TEST", //TODO
            'mbp_bp_seq' => 1, //TODO
            'bp_name' => $request->bp_name,
            'objective_name' => $request->objective_name,
            'bp_steps_seq' => 1,
            'activity_narrative' => $request->activity_narrative,
            'bp_step_risk_seq' => 1, // TODO
            'risk_code' => $request->risk_code,
            'control_seq' => 1, // TODO
            'control_name' => $request->control_name,
            'control_test_seq' => 1, // TODO
            'test_proc_id' => $test_proc->test_proc_id,
            'test_proc_narrative' => $request->test_proc_narrative,
            'audit_objective' => $request->audit_objective,
            'allotted_mandays' => $request->allotted_mandays,
            'auditor_id' => 1, //TODO
            'status' => "New",
            'enabled_flag' => "Y",
        ];
        $apg = $this->repository->create($attribute);

        return response()->json([
            'success' => true,
            'message' => 'Project Scope APG has been added.',
            'data' => $attributes
        ]);
    }

    public function show($scopeId,$id){
        $apg = $this->repository->find($id);
        return response()->json([
            'success' => true,
            'data' => $apg
        ]);
    }   

    public function update($scopeId,$id,ProjectScopeApgRequest $request){
        $attributes = [
            'bp_name' => $request->bp_name,
            'objective_name' => $request->objective_name,
            'activity_narrative' => $request->activity_narrative,
            'risk_code' => $request->risk_code,
            'control_name' => $request->control_name,
            'test_proc_narrative' => $request->test_proc_narrative,
            'audit_objective' => $request->audit_objective,
            'allotted_mandays' => $request->allotted_mandays,
            'auditor_id' => $request->auditor_id
        ];
        $this->repository->update($attributes,$id);
        return response()->json([
            'success' => true,
            'message' => 'Project Scope APG has been updated.',
            'data' => $attributes
        ]);
    }

    public function destroy($scopeId,$id){
        $this->repository->delete($id);
        return response()->json([
            'success' => true,
            'message' => 'Project Scope APG has been removed.'
        ]);
    }

    public function list($scopeId){
        $apgs = $this->repository
        			->with(['auditor','controlMaster','testProc'])
        			->findWhere( ['project_scope_id' => $scopeId ] );
        return Datatables::of($apgs)->make(true);
    }

    public function populate( $scope_id ){
        $count = $this->repository->populateApgs($scope_id);
        return response()->json([
            'success' => true,
            'message' => "Created $count APG(s)."
        ]);
    }

    public function partialView($scope_id, Request $request ){
        $data = $this->repository->pullQueriedApgs($scope_id , $request->groupBy, $request->keyword );
        $group_by = $request->groupBy;
        $html = view( 'engagement_planning.includes.editable_apg_recursive_template', compact('data' , 'group_by') )->render();
        return response()->json([
            'success' => true,
            'message' => "Tree view retrieved.",
            'data' => $data,
            'html' => $html
        ]);
    }

    public function fullView( $scope_id, Request $request ){
        $group_by = "bp_name";
        $flag =  null;
        $template = "";

        switch ($request->type) {
            case 'fw_enabled_apg':
                $flag = "Y";
                $template = "enabled_fw_apg_recursive_template";
                break;
            case 'enabled_apg':
                $flag = "Y";
                $template = "enabled_apg_recursive_template";
                break;
            default:
                $template = "editable_apg_recursive_template";
                break;
        }

        $data = $this->repository->recursiveQuery($scope_id, $group_by , "" , $flag );
        $html = view( 'engagement_planning.includes.' . $template , compact('data','group_by') )->render();

        return response()->json([
            'success' => true,
            'message' => "Enabled APG has been retrieved.",
            'data' => $data,
            'html' => $html
        ]);

    }

    public function recursiveDestroy($scope_id, Request $request){
        $data = $this->repository->recursiveDestroy($scope_id, $request);
        return response()->json([
            'success' => true,
            'message' => "APG has been removed.",
            'data' => $data
        ]);
    }

    public function enabledFlag($scope_id, $id, Request $request){
        $message = $request->status == "Y" ? "Enabled" : "Disabled";
        $this->repository->recursiveFlag($id, $request);
        return response()->json([
            'success' => true,
            'message' => "APG has been ".$message.".",
            'data' => $message
        ]);
    }

    public function updateHeader($scope_id, $id, Request $request){
        $data = $this->repository->recursiveUpdate( $id , $request->field, $request->value);
        return response()->json([
            'success' => true,
            'message' => "Value has been saved.",
            'data' => $data
        ]);
    }
}
