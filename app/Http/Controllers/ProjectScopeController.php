<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProjectScopeCreateRequest ;
use App\Http\Requests\ProjectScopeUpdateRequest ;
use App\Repositories\ProjectScopeRepository;
use App\Jobs\PopulateProjectScopeApg;
use Yajra\Datatables\Datatables;
use App\Http\Requests;
use Auth;

class ProjectScopeController extends Controller
{
    protected $repository;

    public function __construct(
        ProjectScopeRepository $repository
    ){
        $this->repository = $repository;
    }

    public function store($projectId, ProjectScopeCreateRequest $request)
    {
        $mandays_service = app('\App\Services\MandaysService');
    	$list = $this->repository->findWhere([ 'project_id' => $projectId ])->count();

        $attributes = [
            'project_id' => $projectId,
            'scope_sequence' => $list + 1,
            'auditable_entity_id' => $request->auditable_entity,
            'ae_mbp_id' => $request->main_business_process,
            'audit_location' => $request->location,
            'target_start_date' => $request->target_start_date,
            'target_end_date' =>  $request->target_end_date,
            'project_scope_status' => 'New' 
        ];
        $scope = $this->repository->create($attributes);

        $scope->default_misc_mandays = $scope->mainBp->additional_budgeted_mandays_total;
        $scope->default_apg_mandays = $mandays_service->mbpBudgetedMandaysApg( $scope->auditable_entity_id , $scope->project->audit_type );
        $scope->save();

        // dispatch(new PopulateProjectScopeApg(
        //     $request->get('auditable_entity'), 
        //     $request->get('main_business_process'), 
        //     $scope->project_scope_id, 
        //     Auth::user()->user_id 
        // ));

        return response()->json([
            'success' => true,
            'message' => 'Project Scope has been created.'
        ]);
    }

    public function show($projectId, $id){
        $details = $this->repository->with(['iom','auditableEntity','mainBp'])->find($id);
        return response()->json([
            'success' => true,
            'data' => $details
        ]);
    }

    public function update($projectId, $id, ProjectScopeUpdateRequest  $request)
    {
        $attributes = $request->only([ 'target_start_date' , 'target_end_date']);
        $this->repository->update($attributes, $id);
        return response()->json([
            'success' => true,
            'message' => 'Project Scope has been updated.',
            'data' => $attributes
        ]);
    }

    public function destroy($projectId, $id)
    {
        $this->repository->delete($id);

        // Update sequence of remaining resources
        $left = $this->repository->findWhere([ 'project_id' => $projectId ]);
        foreach ($left as $key => $value) {
           $value->scope_sequence =  $key + 1;
           $value->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'Project Scope has been removed.'
        ]);
    }

    public function list($project_id)
    {
        
        $scopes = $this->repository->with(['auditableEntity','mainBp','project','iom'])->all();

        if($project_id){
            $scopes = $this->repository
                       ->with(['auditableEntity','mainBp','iom'])
                       ->findWhere(['project_scope.project_id' => $project_id ]);
        }
        return Datatables::of($scopes)->make(true);
    }
}
