<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\LeaveRequest;
use App\Models\AuditorPlannedLeave;
use App\Repositories\AuditorPlannedLeaveRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class LeaveController extends Controller
{
    const VIEW_PATH = 'leave';
    private $repository;

    public function __construct(AuditorPlannedLeaveRepository $leave)
    {
        $this->repository = $leave;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( self::VIEW_PATH.'.index' );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view( self::VIEW_PATH.'.form' );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeaveRequest $request)
    {
        $attributes = [
            'auditor_id' => $request->get('auditor'),
            'leave_date_from' => $request->get('leave_date_from'),
            'leave_date_to' => $request->get('leave_date_to'),
            'leave_type' => $request->get('leave_type'),
            'remarks' => $request->get('remarks'),
        ];
        $leave = $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'messsage' => 'New leave has been created!',
            'data' => $leave
        ]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  str  $type
     * @param  date  $date
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $details = $this->repository
                        ->with('auditor')
                        ->findWhere([
                            'id' =>$id
                        ])
                        ->first();
        if(is_null($details)) return abort(404);
        return view(self::VIEW_PATH.'.form', compact('details'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  str  $type
     * @param  date  $date
     * @return \Illuminate\Http\Response
     */
    public function update($id, LeaveRequest $request)
    {
        $attributes = [
            'leave_date_from' => $request->get('leave_date_from'),
            'leave_date_to' => $request->get('leave_date_to'),
            'leave_type' => $request->get('leave_type'),
            'remarks' => $request->get('remarks'),
        ];
        $this->repository->update($attributes, $id);

        return response()->json([
            'success' => true,
            'messsage' => 'New leave has been updated!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        AuditorPlannedLeave::where('id',$id )->delete();
        $request->session()->flash('success_message', 'Leave has been removed!');
        return response()->json(['success' => true]);
    }

    public function list()
    {
        $leaves = $this->repository->with('auditor')->all();
        return Datatables::of($leaves)->make(true);
    }
}
