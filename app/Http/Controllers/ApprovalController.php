<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\ApprovalRepository;
use App\Services\ApprovalService;
use App\Http\Requests\ApprovalRequest;
use DB, Auth;

class ApprovalController extends Controller
{

    protected $repository;
	protected $service;

    public function __construct(ApprovalRepository $repository , ApprovalService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $data = $this->repository->all();

    	return response()->json([
            'success' => true,
            'message' => 'Approval history has been read.',
            'data' => $data
        ]);
    }

    /* TODO
    *  - auditor_type still hard coded 
    */

    public function store(ApprovalRequest $request)
    {
        $approver = Auth::user();

        $attributes = [
            'source_category' => $request->cat,
            'source_subcategory' => $request->subcat,
            'approver_id' => $approver->user_id,
            'status_from' => $request->status,
            'status_to' => $request->status_to,
            'approval_ref_id' => $request->ref_id,
            'auditor_type' => "Auditor",
            'approval_date' => date('Y-m-d'),
            'remarks' => $request->remarks
        ];


        $approval = $this->repository->create($attributes);

        $this->updateReferenceData($request, $approval);

        return response()->json([
            'success' => true,
            'message' => 'Change status has been saved.',
            'data' => $approval
        ]);
    }

    public function update( $id, Request $request){

        $attributes = [
            'remarks' => $request->remarks
        ];

        $approval = $this->repository->update($attributes, $id);

        return response()->json([
            'success' => true,
            'message' => 'Remarks has been updated.',
            'data' => $approval
        ]);
    }

    public function approval_info(Request $request)
    {
        $approvals = $this->repository->with('approver')->orderBy('approval_id','desc')->findWhere([
            'source_category' => $request->cat,
            'source_subcategory' => $request->subcat,
            'approval_ref_id' => $request->ref_id
        ]);
        $this->service->setCurrentStatus($request->status);
        $data['request'] = $request->all();
        $data['next_status'] = $this->service->getNextStatus($request->key , $request->ref_id);
        $data['auditor_type'] = $this->service->getAuditorType();
        $data['approvals'] = $approvals;

        return response()->json([
            'success' => true,
            'message' => 'Approval info has been read.',
            'data' => $data
        ]);   
    }

    public function updateReferenceData( Request $request, $approval )
    {   
        switch ($request->key) {
            case 'plan_project':
                $plan = \App\Models\PlanProject::find($request->ref_id);
                $plan->plan_project_status = $approval->status_to;
                $plan->save();
                break;
            case 'plan':
                $plan = \App\Models\Plan::find($request->ref_id);
                $plan->plan_status = $approval->status_to;
                $plan->save();
                break;
            
            default:
                # code...
                break;
        }
    }

}
