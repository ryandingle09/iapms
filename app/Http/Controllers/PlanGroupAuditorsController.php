<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PlanGroupAuditorRepository;
use App\Http\Requests\PlanGroupAuditorRequest;
use Yajra\Datatables\Datatables;
use App\Services\ObjectMapperService;
use App\Http\Requests;

class PlanGroupAuditorsController extends Controller
{
    protected $repository;

    public function __construct(
        PlanGroupAuditorRepository $repository
    ){
        $this->repository = $repository;
    }

    public function index($group_id, ObjectMapperService $mapper)
    {

        $auditors = $this->repository->with('auditor')->findWhere([
            'plan_group_id' => $group_id
            ]);

        $auditors = $mapper->setTemplate('AuditorFromWith')->map( $auditors);
       
        return response()->json([
            'success' => true,
            'message' => 'Plan Group Auditor list.',
            'data' => $auditors
        ]);
    }


    public function store($group_id,PlanGroupAuditorRequest $request)
    {
        $attributes = [
            'plan_group_id' => $group_id,
            'auditor_id' => $request->auditor,
        ];
        $data = $this->repository->create($attributes);

        return response()->json([
            'success' => true,
            'message' => 'Group Auditor has been added.',
            'data' => $data
        ]);
    }

    public function list($group_id){
        $auditors = $this->repository->with('auditor')->findWhere([
            'plan_group_id' => $group_id
            ]);
        return Datatables::of($auditors)->make(true);
    }

    public function destroy($group_id,$id){
        $projects = $this->repository->delete($id);
        return response()->json([
            'success' => true,
            'message' => 'Group Auditor has been removed.'
        ]);
    }
}
