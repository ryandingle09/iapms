<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\MatrixRequest;
use App\Repositories\MatrixRepository;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class MatrixDefinitionController extends Controller
{
    const VIEW_PATH = 'matrix_definition';
    private $repository;

    function __construct(MatrixRepository $matrix)
    {
        $this->repository = $matrix;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view( self::VIEW_PATH.'.index');
    }

    public function create()
    {
        return view( self::VIEW_PATH.'.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MatrixRequest $request)
    {
        $attributes = [
            'matrix_name' => $request->get('matrix_name'),
            'description' => $request->get('description'),
            'x_dimension' => $request->get('x'),
            'y_dimension' => $request->get('y'),
        ];

        $matrix = $this->repository->create($attributes);

        // Set up details
        $x_dimension = range(1, $request->get('x')); // set range 1 to N (y dimension)
        $y_dimension = range(1, $request->get('y')); // set range 1 to N (x dimension)

        // generate blank details as initial data
        for($y=0;$y < count($y_dimension); $y++) {
            for($x=0;$x < count($x_dimension); $x++) {
                $matrix->details()->create([
                    'x_sequence' => $x_dimension[$x],
                    'y_sequence' => $y_dimension[$y],
                ]);
            }
        }

        $request->session()->flash('message', 'New matrix has been created.');

        // redirect to edit form
        return response()->json([
            'success' => true,
            'redirect' => route('matrix_definition.edit', ['id' => $matrix->matrix_id])
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $details = $this->repository->findByField('matrix_id', $id)->first();
        if( is_null($details) ) return abort(404);
        // Generate matrix array for table viewing
        $matrices = $details->details;
        $matrix_table = [];

        foreach ($matrices as $matrix) {
            $matrix_table[ $matrix->y_sequence ][ $matrix->x_sequence ] = [
                'value' => $matrix->matrix_value,
                'label_x' => $matrix->x_value,
                'label_y' => $matrix->y_value,
                'color' => $matrix->color,
            ]; // store dimensions to array as Y as the key and X as value
        }

        return view(self::VIEW_PATH.'.form', compact('details', 'matrix_table'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\MatrixRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MatrixRequest $request, $id)
    {
        $attributes = [
            'matrix_name' => $request->get('matrix_name'),
            'description' => $request->get('description'),
            'x_dimension' => $request->get('x'),
            'y_dimension' => $request->get('y'),
        ];
        try {
            $matrix = $this->repository->update($attributes, $id);
            $details = $matrix->details;

            $x = array_values(array_unique($details->pluck('x_sequence')->toArray()));
            $y = array_values(array_unique($details->pluck('y_sequence')->toArray()));
            // Set up details
            $x_dimension = range(1, $request->get('x')); // set range 1 to N (y dimension)
            $y_dimension = range(1, $request->get('y')); // set range 1 to N (x dimension)

            $x_diff = count($x_dimension) - count($x);
            $y_diff = count($y_dimension) - count($y);

            $x_diff_result = $this->checkDiff($x_diff, $x_dimension, $x);
            $y_diff_result = $this->checkDiff($y_diff, $y_dimension, $y);

            // remove existing coorditanes if its less than the number of the new coordinates
            if( $x_diff < 0 && $x_diff_result !== false ) {
                $matrix->details()->whereIn('x_sequence', $x_diff_result)->delete();
                $x_remaining = array_diff($x, $x_diff_result);
            }
            if( $y_diff < 0 &&  $y_diff_result !== false ) {
                $matrix->details()->whereIn('y_sequence', $y_diff_result)->delete();
                $y_remaining = array_diff($y, $y_diff_result);
            }

            // Check if there is positive diff either in X/Y axis
            // new Y-axis and X-axis is created here
            if( ($x_diff > 0 || $y_diff > 0) && ($x_diff_result !== false || $y_diff_result !== false) ) {
                // set the x and y dimension to fit in with the existing coordinates
            //     $y_dimension = isset($y_remaining) ? $y_remaining : ($y_diff_result !== false ? $y_diff_result : $y_dimension);
            //     $x_dimension = isset($x_remaining) ? $x_remaining : ($x_diff_result !== false ? $x_diff_result : $x_dimension);
            // dd($y_dimension,$x_dimension);

                // generate axis
                for($y=0;$y < count($y_dimension); $y++) {
                    for($x=0;$x < count($x_dimension); $x++) {
                        $exists = $matrix->details()
                                         ->where('x_sequence', $x_dimension[$x])
                                         ->where('y_sequence', $y_dimension[$y])
                                         ->count();
                        if( !$exists ) {
                            $matrix->details()->create([
                                'x_sequence' => $x_dimension[$x],
                                'y_sequence' => $y_dimension[$y],
                            ]);
                        }
                    }
                }
            }
        }
        catch(Exception $e) {
            return $e;
        }

        return response()->json(['success' => true]);
    }

    /**
     * Check the diff between the new and old coordinates
     * @param  integer $diff            number of diff
     * @param  array $dimension         list of new axis
     * @param  array $last_coordinates  list of old axis
     * @return array | boolean          If diff is not 0 return array; else return false
     */
    private function checkDiff($diff, $dimension, $last_coordinates)
    {
        // get the end of each array as marker

        if( $diff < 0 ) {
            $check_point = end($dimension) + 1;
            $last_coordinate = end($last_coordinates);
            return range($check_point, $last_coordinate);
        }

        if( $diff > 0 ) {
            return array_values(array_diff($dimension, $last_coordinates)); // remove existing array
        }

        return false;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $this->repository->delete($id);

        $request->session()->flash('message', 'Matrix definition is successfully deleted.');
        return response()->json([ 'success' => true ]);
    }

    public function list()
    {
        $matrix = $this->repository->all();

        return Datatables::of($matrix)->make(true);
    }

    /**
     * Update X & Y details
     * @param  integer $id
     * @param  integer $x
     * @param  integer $y
     * @param  \Illuminate\Http\Request  $request
     * @return JSON
     */
    public function putUpdateDetails($id, Request $request)
    {
        $matrix = $this->repository->find($id);
        $x = $request->get('x');
        $y = $request->get('y');
        if( is_null($matrix) ) return abort(404);

        if( $request->get('type') ) {
            $field = str_replace('-', '_', $request->get('type'));
            $axis = explode('_', $field);
            $attributes[$field] = $request->get('value');
            $attributes[$axis[0].'_sequence'] = $request->get('axis');

            $details = $matrix->details()->where($axis[0].'_sequence', $request->get('axis'));
        }
        else {
            $attributes['matrix_value'] = $request->get('matrix');
            $attributes['color'] = $request->get('color');
            $attributes['x_sequence'] = $x;
            $attributes['y_sequence'] = $y;

        $details = $matrix->details()
                          ->where('x_sequence', $x)
                          ->where('y_sequence', $y);
        }

        if( $details->count() > 0 ) {
            // update matrix details
            $details->update($attributes);
        }
        else {
            // create matrix details
            $matrix->details()->create($attributes);
        }

        return response()->json(['success' => true]);
    }
}
