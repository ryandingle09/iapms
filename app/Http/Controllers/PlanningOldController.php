<?php

namespace App\Http\Controllers;

use App\Criteria\AuditorActiveCriteria;
use App\Criteria\AuditorMandaysCriteria;
use App\Criteria\PlanAuditorsCriteria;
use App\Criteria\PlanProjectAuditorMandaysCriteria;
use App\Criteria\ProjectAuditorMandaysCriteria;
use App\Criteria\ProjectAuditorsCriteria;
use App\Http\Requests;
use App\Http\Requests\PlanProjectAuditorRequest;
use App\Http\Requests\PlanProjectRequest;
use App\Http\Requests\PlanProjectScopeAuditorRequest;
use App\Http\Requests\PlanningRequest;
use App\Http\Requests\PlanProjectScopeRequest;
use App\Repositories\AuditorRepository;
use App\Repositories\PlanProjectRepository;
use App\Repositories\PlanProjectScopeRepository;
use App\Repositories\PlanRepository;
use App\Services\AuditableEntityService;
use App\Services\BusinessProcessService;
use App\Services\PlanningService;
use App\Transformers\AuditorTransformer;
use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class PlanningOldController extends Controller
{
    const VIEW_PATH = 'planning_old';
    private $repository;
    private $service;
    private $auditor;
    private $project;
    private $scope;

    function __construct(
        PlanRepository $plans,
        AuditorRepository $auditor,
        PlanProjectRepository $project,
        PlanProjectScopeRepository $scope,
        PlanningService $service
    )
    {
        $this->repository = $plans;
        $this->service = $service;
        $this->auditor = $auditor;
        $this->project = $project;
    	$this->scope = $scope;
    }

    public function index()
    {
        $project_status_count = null;
        $details = ['data']; // initialize details data
        $search = \Request::get('search');
        if( $search ) {
            // presenter is used to manipulate the return of the repository
            $this->repository->setPresenter('App\Presenters\PlanningPresenter');
            // repository will automatically get the search request if exists
            // the data will include the auditors and projects if plan exists
            $details = $this->repository->present(['auditors', 'projects'])->all(); // get the planning with auditors assigned
            if(!empty($details['data'])) {
                $details['data'] = $details['data'][0]; // get the first record if data is not empty
                $project_status_count = $this->project->getStatusCount($details['data']['id']); // get the plan project statuses count
            }
        }

    	return view(self::VIEW_PATH.'.index', compact('details', 'project_status_count'));
    }

    /**
     * Get available auditors per tab
     * @param  string $type     plan | project | scope
     * @param  integer $id
     * @return json
     */
    public function getAvailableAuditors($type, $id)
    {
        $this->auditor->pushCriteria(new AuditorActiveCriteria); // active audtiors criterias
        switch ($type) {
            case 'project':
                $this->auditor->pushCriteria(new PlanAuditorsCriteria($id));
                break;

            case 'scope':
                $this->auditor->pushCriteria(new ProjectAuditorsCriteria($id));
                break;

            default:
                // no additional criteria for plan tab
                break;
        }
        // auditors list
        $auditors = $this->auditor->all();

        return response()->json(['success' => true, 'data' => $auditors]);
    }

    public function getPlan(AuditableEntityService $auditable_entity)
    {
        $project_status_count = null;
        $details = ['data']; // initialize details data
        $search = \Request::get('search');
        if( $search ) {
            // presenter is used to manipulate the return of the repository
            $this->repository->setPresenter('App\Presenters\PlanningPresenter');
            // repository will automatically get the search request if exists
            // the data will include the auditors and projects if plan exists
            $details = $this->repository->present(['auditors', 'projects'])->all(); // get the planning with auditors assigned
            if(!empty($details['data'])) {
                $details['data'] = $details['data'][0]; // get the first record if data is not empty
                $project_status_count = $this->project->getStatusCount($details['data']['id']); // get the plan project statuses count
            }
        }

        // auditors list
        // $this->auditor->pushCriteria(new AuditorActiveCriteria);
        // $this->auditor->popCriteria(app('Prettus\Repository\Criteria\RequestCriteria')); // removes the request criteria to disable repository auto search
        // $auditors = $this->auditor->all();

        return response()->json([
            'success' => true,
            'details' => $details,
            'project_stat' => $project_status_count,
        ]);
    }

    public function store(PlanningRequest $request)
    {
        $year = $request->get('audit_year');
        $attributes = [
            'plan_year' => $year,
            'plan_name' => $request->get('name'),
            'remarks' => $request->get('remarks'),
            'plan_status' => 'New',
        ];
        $this->repository->create($attributes);

        $request->session()->flash('message', 'New planning for the year '.$year.' has been created.');

        return response()->json(['success' => true]);
    }

    public function putUpdatePlan($plan_id, PlanningRequest $request)
    {
        $plan = $this->repository->find($plan_id);
        $year = $request->get('audit_year');
        $redirect = $plan->plan_year != $year ? route('planning').'?search='.$year : false;

        $attributes = [
            'plan_year' => $year,
            'plan_name' => $request->get('name'),
            'remarks' => $request->get('remarks'),
        ];

        $plan->update($attributes);

        $request->session()->flash('message', 'Planning for the year '.$year.' has been updated.');

        return response()->json(['success' => true, 'redirect' => $redirect]);
    }

    public function getPlanAuditorMandays($plan_id, $auditor_id) {
        $auditor = $this->auditor->find($auditor_id);
        $response = [
            'proficiencies' => $auditor->proficiency->pluck('proficiency_rate', 'proficiency_code'),
        ];
        $this->service
             ->getPlanProjectAuditorRunningMandays($plan_id, $auditor_id);
        $response['mandays'] = $this->service
                        ->setAuditYear()
                        ->setAuditor($auditor)
                        ->getMandays();

        return response()->json([ 'success' => true, 'data' => $response ]);
    }

    public function getPlanProjectAuditors($project_id) {
//        $this->auditor->pushCriteria(new AuditorMandaysCriteria);

        $this->project->pushCriteria(new PlanProjectAuditorMandaysCriteria($project_id));
        $project = $this->project->find($project_id);
        dd($project);
    }

    public function getPlanProjectAuditorMandays($project_id, $auditor_id = null) {
//        $this->auditor->pushCriteria(new AuditorMandaysCriteria);
        $project = $this->project->find($project_id);
        dd($project);
    }

    public function getProjectScopeAuditorMandays($scope_id) {
        $this->auditor->pushCriteria(new AuditorMandaysCriteria);

    }

    public function postStorePlanAuditor($plan_id, Request $request)
    {
        $this->validate($request, [
            'auditor_name' => 'required|unique:plan_auditors,auditor_id,null,plan_id,plan_id,'.$plan_id,
            'auditor_type' => 'required',
        ]);

        $plan = $this->repository->find($plan_id);
        $attributes = [
            'auditor_type' => $request->auditor_type,
        ];
        $plan->auditor()->attach($request->get('auditor_name'), $attributes);
        $auditors = $this->service->getAuditors($plan->auditor, $plan->plan_id, $plan->plan_year);

        return response()->json(array_merge(['success'=> true], $auditors));
    }

    public function deletePlanAuditor($plan_id, $auditor_id, Request $request)
    {
        $plan = $this->repository->find($plan_id);
        $plan->auditor()->detach($auditor_id); // remove

        return response()->json(['success' => true]);
    }

    public function postStorePlanProject($plan_id, PlanProjectRequest $request)
    {
        $plan = $this->repository->find($plan_id);
        $attributes = [
            'plan_project_name' => $request->get('project_name'),
            'audit_type' => $request->get('audit_type'),
            'target_start_date' => $request->get('start_date'),
            'target_end_date' => $request->get('end_date'),
            'plan_project_remarks' => $request->get('remarks'),
            'project_head' => $request->get('project_head'),
            'plan_project_status' => 'New',
        ];
        $plan->projects()->create($attributes);
        $response = $this->service->getPlanProjects($plan->projects);

        return response()->json(array_merge(['success' => true], $response));
    }

    public function putUpdatePlanProject($plan_id, PlanProjectRequest $request)
    {
        $project = $this->project->find($request->get('project_id'));
        $attributes = [
            'plan_project_name'     => $request->get('project_name'),
            'audit_type'            => $request->get('audit_type'),
            'target_start_date'     => $request->get('start_date'),
            'target_end_date'       => $request->get('end_date'),
            'plan_project_remarks'  => $request->get('remarks'),
            'project_head'          => $request->get('project_head'),
        ];
        $project->update($attributes);

        $request->session()->flash('message', 'Plan project is successfully updated.');

        return response()->json(['success' => true]);
    }

    public function putUpdateProjectStatus($project_id, Request $request)
    {
        $project = $this->project->find($project_id);
        $status = $request->get('status');
        $attributes = [
            'plan_project_status' => $status,
        ];
        $this->service->updatePlanning($status, $project, $attributes);
    }

    public function getProjectHeads($plan_id, $project_id)
    {
        $response = ['data' => []]; // initial response value
        $this->repository->popCriteria(app('Prettus\Repository\Criteria\RequestCriteria')); // removes the request criteria to disable repository auto search
        $plan = $this->repository->find($plan_id);

        $project = $plan->projects()
                        ->where('plan_project_id', $project_id)
                        ->first();
        if( !is_null($project) ){
            $auditors = $project->projectAuditor()
                                ->posAuditor()
                                ->active()
                                ->get();
            $fractal = new Manager();
            $data = new Collection($auditors, new AuditorTransformer);
            $response = $fractal->createData($data)->toJson();
        }

        return $response;
    }

    public function getProjectAuditors($project_id)
    {
        $project = $this->project->find($project_id);
        $auditors = $project->projectAuditor;
        $response = $this->service->getAuditors($auditors, $project->plan_project_id, $project->plan->plan_year);

        return response()->json($response);
    }

    public function postStoreProjectAuditor($project_id, PlanProjectAuditorRequest $request)
    {
        $project = $this->project->find($project_id);
        $attributes = [
            'auditor_type' => $request->get('auditor_type'),
            'allotted_mandays' => $request->get('allotted_mandays'),
        ];

        $project->projectAuditor()->attach($request->get('auditor_name'), $attributes);
        $auditors = $project->projectAuditor;
        $response = $this->service->getAuditors($auditors, $project->plan_project_id, $project->plan->plan_year);

        return response()->json( array_merge(['success' => true], $response) );
    }

    public function deleteProjectAuditor($project_id, $auditor_id, Request $request)
    {
        $project = $this->project->find($project_id);
        $project->projectAuditor()->detach($auditor_id); // remove

        return response()->json(['success' => true]);
    }

    public function postStoreProjectScopes($project_id, PlanProjectScopeRequest $request)
    {
        $project = $this->project->find($project_id);
        $attributes = [
            'scope_sequence' => $request->get('sequence'),
            'auditable_entity_id' => $request->get('auditable_entity'),
            'bp_id' => $request->get('business_process'),
            'audit_location' => $request->get('location'),
            'budgeted_mandays' => $request->get('budgeted_mandays'),
        ];
        $project->scopes()->create($attributes);

        $response = $this->service->getPlanProjectScopes($project->scopes);

        return response()->json( array_merge(['success' => true], $response) );
    }

    public function getScopeAuditors($scope_id)
    {
        $scope = $this->scope->find($scope_id);
        $auditors = $scope->auditor;
        $response = $this->service->getAuditors($auditors, $scope_id, $scope->project->plan->plan_year);

        return response()->json($response);
    }

    public function postStoreProjectScopeAuditor($scope_id, PlanProjectScopeAuditorRequest $request)
    {
        $attributes = [
            'auditor_type' => $request->get('auditor_type'),
            'allotted_mandays' => $request->get('allotted_mandays'),
        ];
        $scope = $this->scope->find($scope_id);
        $scope->auditor()->attach($request->get('auditor_name'), $attributes);
        $auditors = $scope->auditor;
        $response = $this->service->getAuditors($auditors, $scope_id, $scope->project->plan->plan_year);

        return response()->json(array_merge(['success'=> true], $response));
    }

    public function deleteScopeAuditor($scope_id, $auditor_id, Request $request)
    {
        $scope = $this->scope->find($scope_id);
        $scope->auditor()->detach($auditor_id); // remove

        return response()->json(['success' => true]);
    }
}
