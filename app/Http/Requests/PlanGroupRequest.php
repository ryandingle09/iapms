<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PlanGroupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plan_group_name' => "required",
            // 'description' => "required",
            'group_head' => "required|unique:plan_groups,group_head,".$this->segment(4).",plan_group_id",
        ];
    }
}
