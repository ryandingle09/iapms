<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AuditorManagementInputProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_name'      => 'required',
            'project_head'      => 'required',
            'audit_type'        => 'required',
            'status'            => 'required',
            'start_date' => 'required|date',
            'end_date'   => 'required|date',
        ];
    }

    /*public function messages()
    {
        return [
            'auditor.required' => 'The auditor field is required.',
        ];
    }*/
}
