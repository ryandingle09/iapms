<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class ProjectScopeApgRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "bp_name" => "required" , 
            "objective_name" => "required" , 
            "activity_narrative" => "required" , 
            "risk_code" => "required" , 
            "control_name" => "required" , 
            "test_proc_narrative" => "required" , 
            "audit_objective" => "required" , 
            "auditor_id" => "required" , 
            "allotted_mandays" => "required|numeric" , 
            
        ];
    }
}
