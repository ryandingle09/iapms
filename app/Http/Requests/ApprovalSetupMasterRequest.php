<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ApprovalSetupMasterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'source_category' => 'required',
            'source_subcategory' => 'required',
            'source_table' => 'required',   
            // 'start_status' => 'required',
            // 'finish_status' => 'required',
            // 'supersede_allowed_flag' => 'required',     
        ];
    }
}
