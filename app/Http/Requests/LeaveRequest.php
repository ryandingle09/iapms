<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LeaveRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ignore_id = $this->segment(5) ? $this->segment(5) : 'NULL';
        return [
            'auditor' => 'required|no_conflict_leave:leave_date_from,leave_date_to,'.$ignore_id,
            'leave_type' => 'required',
            'leave_date_from' => 'required|date|before_equal:leave_date_to',
            'leave_date_to' => 'required|date',
        ];
    }

    public function messages()
    {
        return [
            'auditor.no_conflict_leave' => 'Auditor already have leave for the selected date.',
            'leave_date_from.before_equal' => 'Leave date start must be earlier than the end date.',
        ];
    }
}
