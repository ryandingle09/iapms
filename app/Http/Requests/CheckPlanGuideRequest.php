<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CheckPlanGuideRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'audit_year' => "required|exists:plan,plan_year",
            'group_by' => "required",
            // 'audit_type' => "required",
        ];
    }
}
