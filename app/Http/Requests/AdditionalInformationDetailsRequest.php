<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdditionalInformationDetailsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'sequence' => 'required|integer|min:1',
            'prompt' => 'required|unique:additional_info_details,prompt,'.$this->segment(4),
            'attribute' => 'required',
            'data_type' => 'required',
            'required' => 'required',
        ];
    }
}
