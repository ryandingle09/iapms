<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PlanProjectAuditorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "auditor" => "required|unique:plan_project_auditors,auditor_id,NULL,plan_project_id,plan_project_id,".$this->segment(2),
            // "auditor_type" => "required",
            // "allotted_mandays" => "required|integer|min:1",
        ];
    }

    public function messages()
    {
        return [
            'auditor_name.unique' => 'Auditor already exists in the project.',
        ];
    }
}
