<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BpStepRiskRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sequence' => 'required|integer|min:1|unique:bp_steps_risks,bp_step_risk_seq,null,bp_step_id,bp_step_id,'.$this->get('id'),
            'risk' => 'required|unique:bp_steps_risks,risk_id,null,bp_step_id,risk_id,'.$this->get('risk').',bp_step_id,'.$this->get('id'),
        ];
    }
}
