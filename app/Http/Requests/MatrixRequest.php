<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MatrixRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'matrix_name' => 'required|unique:matrix,matrix_name,'.$this->get('id').',matrix_id',
            'x' => 'required|integer|min:1',
            'y' => 'required|integer|min:1',
        ];
    }
}
