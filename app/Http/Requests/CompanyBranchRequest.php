<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CompanyBranchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'entity_class' => 'required',
            'branch_opening_date' => 'date',
            'active' => 'required',
        ];
        if( $this->get('_method') == 'post' ) {
            $rules['branch_code'] = 'required|unique:company_branches,branch_code,null,company_id,company_id,'.$this->get('company_id').',branch_code,'.$this->get('branch_code');
        }

        return $rules;
    }
}
