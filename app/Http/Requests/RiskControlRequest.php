<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RiskControlRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'control_seq' => 'required|integer|min:1|unique:risks_control,risk_control_seq,'.$this->get('id').',risk_control_id,risk_id,'.$this->get('risk_id'),
            'controls' => 'required|unique:risks_control,control_id,'.$this->get('id').',risk_control_id,risk_id,'.$this->get('risk_id'),
        ];
    }

    public function messages()
    {
        return [
            'control_seq.required' => 'Control sequence field is required.',
            'control_seq.number' => 'Control sequence must be a number.',
            'control_seq.min' => 'Control sequence must be atleast 1.',
        ];
    }
}
