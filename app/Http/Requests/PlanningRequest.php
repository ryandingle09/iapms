<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PlanningRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $current_year = date('Y');
        return [
            'plan_year' => 'required|numeric|min:'.$current_year.'|unique:plan,plan_year,'.$this->get('id').',plan_id',
            'plan_name' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'audit_year.unique' => 'The audit year '.$this->get('audit_year').' is already exists.',
        ];
    }
}
