<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PlanProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
            'plan_project_name' => 'required|unique:plan_projects,plan_project_name,'.$this->segment(4).",plan_project_id",
            'audit_type' => 'required',
            'plan_project_status' => 'required',
            'target_start_date' => 'required',
            'target_end_date' => 'required|after:'.$this->get('target_start_date'),
            // 'project_head' => 'required',
            // 'plan_project_remarks' => 'required'
        ];
        if( !$this->segment(2) ){
            $rules['group'] = "required"; 
        }
    
        return $rules;   
    }

    public function messages()
    {
        return [
            'start_date.before_equal' => 'Project start date must be less than or equal end date.',
        ];
    }
}
