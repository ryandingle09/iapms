<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LookupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'type' => 'required|unique:lookup_types,lookup_type,'.$this->get('lookup_id'),
            'meaning' => 'required',
            // 'code.meaning.*' => 'required',
            // 'code.start_date.*' => 'required|date',
            // 'code.end_date.*' => 'required|date',
        ];

        // $codes = $this->get('code');
        // if( isset($codes['value']) ) {
        //     for ($i = 0; $i < count($codes['value']); $i++) {
        //         $rules['code.value.'.$i] = 'required|unique:lookup_values,lookup_code,null,lookup_code,lookup_type_id,'.$codes['value'][$i].',lookup_type_id,'.$this->get('lookup_id');
        //     }
        // }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'type.unique' => ':attribute is already exists',
            // 'code.value.*.unique' => 'Lookup code value already exists.',
            // 'code.value.*.required' => 'The lookup code value is required.',
            // 'code.meaning.*.required' => 'The code meaning is required.',
            // 'code.start_date.*.required' => 'Start date is required.',
            // 'code.end_date.*.required' => 'End date is required.',
            // 'code.start_date.*.date' => 'Start date must be date.',
            // 'code.end_date.*.date' => 'End date must be date.',
            // 'code.start_date.*.before' => 'Start date must be less than the end date.',
            // 'code.end_date.*.after' => 'End date must be greater than the start date.',
        ];
    }
}
