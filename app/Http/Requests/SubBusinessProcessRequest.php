<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SubBusinessProcessRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sp_seq' => 'required|integer',
            'sub_bp' => 'required|unique:ae_mbp_sbp,bp_id,null,ae_mbp_bp_id,ae_mbp_id,'.$this->get('mbp_id'),
        ];
    }

    public function messages()
    {
        return [
            'sp_seq.required' => 'Sequence is required.',
            'sub_bp.integer' => 'Sequence must be a number.',
            'sub_bp.required' => 'Sub business process is required.',
            'sub_bp.unique' => 'Sub business process already exists.',
        ];
    }
}
