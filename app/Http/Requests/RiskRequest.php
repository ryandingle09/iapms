<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RiskRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'risk' => 'required|unique:risks,risk_code,null,risk_id',
            'risk_type' => 'required',
            'source_type' => 'required',
            'response_type' => 'required',
            'def_in_rating_impact' => 'required',
            'def_in_rating_likelihood' => 'required',
            'def_res_rating_impact' => 'required',
            'def_res_rating_likelihood' => 'required',
        ];
        if( $this->segment(1) == 'update' ) $rules['risk'] = 'required|unique:risks,risk_code,'.$this->segment(2).',risk_id';

        if( $this->get('source_type') == 'Enterprise' ) $rules['enterprise_risk_id'] = 'required';
        
        return $rules;
    }

    public function messages()
    {
        return [
            'def_in_rating_impact.required' => 'Default inherent impact rating field is required.',
            'def_in_rating_likelihood.required' => 'Default inherent likelihood rating field is required.',
            'def_res_rating_impact.required' => 'Default residual impact rating field is required.',
            'def_res_rating_likelihood.required' => 'Default residual likelihood rating field is required.',
            'enterprise_risk_id.required' => 'Enterprise risk title is required.',
        ];
    }
}
