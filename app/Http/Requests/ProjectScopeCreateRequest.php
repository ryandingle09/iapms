<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class ProjectScopeCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "auditable_entity" => "required",
            "main_business_process" => "required",
            "target_start_date" => "required|date",
            "target_end_date" => "required|date|after:target_start_date",
            "location" => "required",
        ];
    }
}
