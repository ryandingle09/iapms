<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AuditorLcmRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $segments = $this->segments();
        $rules = [
            'lcm_type' => 'required|unique:auditor_lcm,lcm_type,'.$this->segment(4).',auditor_lcm_id,auditor_id,'.$segments[1],
            'ref_no' => 'required',
            'issued_date' => 'required|date|before:expiry_date',
            'expiry_date' => 'required|date',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'lcm_type.required' => 'Type is required.',
            'ref_no.required' => 'License / Certification Number is required',
        ];
    }
}
