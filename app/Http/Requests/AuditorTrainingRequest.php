<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AuditorTrainingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'training_type' => 'required|unique:auditor_training,training_type,'.$this->segment(4).',auditor_training_id,auditor_id,'.$this->segment(2),
            'description' => 'required',
            'venue' => 'required',
            'completion_date' => 'required|date',
        ];

        return $rules;
    }
}
