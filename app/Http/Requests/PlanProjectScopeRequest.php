<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PlanProjectScopeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $except = "NULL";
        return [
            "auditable_entity" => "required",
            "main_business_process" => "required|unique:plan_project_scope,mbp_id,".$except.",plan_project_scope_id,auditable_entity_id,".$this->get('auditable_entity').",plan_project_id,".$this->segment(2),
            "budgeted_mandays" => "required|min:0|max:999|numeric",
            // "location" => "required",
        ];
    }
}
