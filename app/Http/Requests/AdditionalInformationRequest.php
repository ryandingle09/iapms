<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdditionalInformationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100|unique:additional_info_master,additional_info_name,'.$this->segment(2).',additional_info_id',
            'table' => 'required|max:80|unique:additional_info_master,table_name,'.$this->segment(2).',additional_info_id',
        ];
    }
}
