<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ControlTestProcedureRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'sequence' => 'required|integer|min:1',
            'test_proc' => 'required|unique:controls_test_proc,test_proc_id,null,control_test_id,control_id,'.$this->get('control_id'),
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'test_proc.required' => 'Test procedure narrative is required.',
            'test_proc.unique' => 'Test procedure already exists.',
        ];
    }
}
