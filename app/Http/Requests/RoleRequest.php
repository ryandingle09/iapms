<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RoleRequest  extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = 'required|unique:roles,name';
        if($this->get('rid')){
            $rules .= ",".$this->get('rid');
        }
        return ['name' => $rules, ]; 
    }

    public function messages()
    {
        return [
            'name.required' => 'Name field is required.',
        ];
    }

    /**
     * Adding flash message to form validator 
     * @source https://github.com/laravel/framework/blob/master/src/Illuminate/Foundation/Http/FormRequest.php
     * @return void
     */
    // public function response()
    // {
    //     session()->flash( 'warning_message', 'Please fill up the form correctly.' ) );
    // }
}
