<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AuditeeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|unique:auditees,first_name,'.$this->get('id').',auditee_id,middle_name,'.$this->get('middle_name').',last_name,'.$this->get('last_name'),
            'last_name' => 'required',
            'employee_id' => 'required',
            'auditable_entity' => 'required',
            'email_address' => 'required|email',
            'position' => 'required',
            'department_code' => 'required',
            'start_date' => 'required|date|before_equal:end_date',
            'end_date' => 'date',
        ];
    }

    public function messages()
    {
        return [
            'first_name.unique' => 'Auditee already exists.',
            'start_date.before_equal' => 'Start date must be less than or equal End date.',
        ];
    }
}
