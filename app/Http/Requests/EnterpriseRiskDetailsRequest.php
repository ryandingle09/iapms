<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EnterpriseRiskDetailsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $ent_id = $this->segment(4) == 'update' ? $this->segment(3) : $this->segment(2);
        $rules = [
            'risk' => 'required|unique:enterprise_risk_details,risk_name,',
            'risk_type' => 'required',
            'response_type' => 'required',
            'risk_class' => 'required',
            'risk_area' => 'required',
            'response_type' => 'required',
            'response_status' => 'required',
            'inherent_impact_rate' => 'required',
            'inherent_likelihood_rate' => 'required',
            'residual_impact_rate' => 'required',
            'residual_likelihood_rate' => 'required',
        ];
        $rules['risk'] .= ($this->segment(4) == 'update' ? $this->segment(3) : 'null').',enterprise_risk_det_id,enterprise_risk_id,'.$ent_id;

        return $rules;
    }

    public function messages()
    {
        return [
            'risk.unique' => 'Risk name already exists.',
        ];
    }
}
