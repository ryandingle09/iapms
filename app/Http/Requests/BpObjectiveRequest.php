<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BpObjectiveRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'objective_name' => 'required|unique:bp_objectives,objective_name,'.$this->get('id').',bp_objective_id,bp_id,'.$this->segment(2),
            'objective_narrative' => 'required',
            'objective_category_check' => 'required',
        ];

        return $rules;
    }
}
