<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PlanAuditorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'auditor' => 'required|unique:plan_auditors,auditor_id,NULL,plan_auditor_id,plan_id,'.$this->segment(2),
        ];
    }
}
