<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TestProceduresRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'test_proc_name' => 'required|unique:test_proc,test_proc_name,'.$this->get('id').',test_proc_id',
            'budgeted_mandays' => 'required|regex:/[0-9][.]?[0-9]*/',
        ];
    }
}
