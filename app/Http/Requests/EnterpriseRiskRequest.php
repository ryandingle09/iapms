<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EnterpriseRiskRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'auditable_entity' => 'required|unique:enterprise_risks,auditable_entity_id,null,enterprise_risk_id,bp_id,'.$this->get('business_process').',ae_mbp_id,'.$this->get('main_business_process'), for ent risk overall validation
            'auditable_entity' => 'required|unique:enterprise_risks,auditable_entity_id,null,enterprise_risk_id,bp_id,'.$this->get('business_process'),
            'main_business_process' => 'required',
            'business_process' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'auditable_entity.unique' => 'Selected auditable entity and business process already exists.',
        ];
    }
}
