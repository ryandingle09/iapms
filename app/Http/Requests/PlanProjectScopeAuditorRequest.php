<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PlanProjectScopeAuditorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $except = $this->segment(4) ? $this->segment(4) : "NULL";
        return [
            "auditor" => "required|unique:plan_project_scope_auditors,auditor_id,".$except.",plan_project_scope_auditor_id,plan_project_scope_id,".$this->segment(2),
            'auditor_type' => 'required',
            'allotted_mandays' => "required|min:0|max:999|numeric",
        ];
    }

    public function messages()
    {
        return [
        ];
    }
}
