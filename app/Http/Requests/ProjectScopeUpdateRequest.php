<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class ProjectScopeUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // "auditable_entity" => "required",
            // "main_business_process" => "required",
            "target_start_date" => "required|date",
            "target_end_date" => 'required|date|after:target_start_date',
            // "budgeted_mandays" => "required|integer|min:1",
            // "location" => "required",
        ];
    }
}
