<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ValueSetRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:value_sets,value_set_name,'.$this->get('id').',value_set_id',
            'code' => 'required',
            'meaning' => 'required',
            'from_clause' => 'required',
            'where_clause' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Value set name is required.',
            'name.uniquer' => 'Value set name already exists.',
        ];
    }
}
