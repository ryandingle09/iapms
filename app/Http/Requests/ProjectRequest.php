<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class ProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'project_name' => 'required',
            'audit_type' => 'required',
            'target_start_date' => 'required',
            'target_end_date' => 'required|after:'.$this->get('target_start_date'),
            'project_head' => 'required',
            'project_remarks' => 'required',
            'audit_year' => 'required|numeric',
        ];
        return $rules;   
    }
}
