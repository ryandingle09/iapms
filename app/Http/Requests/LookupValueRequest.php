<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LookupValueRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'code_meaning' => 'required',
            'start_date' => 'required|date|before_equal:end_date',
            'end_date' => 'date',
        ];

        if( $this->get('_method') != 'put' ) {
            $rules['code'] = 'required|unique:lookup_values,lookup_code,null,lookup_code,lookup_type_id,'.$this->get('code').',lookup_type_id,'.$this->get('lookup_type_id');
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'start_date.before_equal' => 'Start date must be less than or equal to end date',
        ];
    }
}
