<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Validator,DB;

class ProjectScopeFreezeCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        Validator::extend('no_conflict_freeze', function($attribute, $value, $parameters, $validator) {
            $project_scope_id = $parameters[0];
            $start_date = $value;
            $end_date = array_get( $validator->getData() , $parameters[1] );
            $ignore_id = isset($parameters[2]) ? $parameters[2] : null ;

            
            $find = collect( DB::table('project_scope_freeze')
                        ->where(function($q) use ($project_scope_id, $ignore_id){
                            $q->where('project_scope_id',$project_scope_id)->whereNull('effective_date_to');

                            if($ignore_id != "NULL" && $ignore_id != null){
                                $q->where('project_scope_freeze_id', "!=", $ignore_id);
                            }

                        })
                        ->orWhere(function($q) use ($project_scope_id, $start_date, $end_date, $ignore_id) {
                            if($end_date){
                                $q->where('project_scope_id',$project_scope_id)
                                    ->where('effective_date_from','<=', ymd($end_date) )
                                    ->where('effective_date_to','>=',ymd( $start_date ) );
                            }else{
                                $q->where('project_scope_id',$project_scope_id)
                                    ->where('effective_date_to','>=',ymd( $start_date ) );
                            }

                            if($ignore_id != "NULL" && $ignore_id != null){
                                $q->where('project_scope_freeze_id', "!=", $ignore_id);
                            }

                        })->get()
                    );

            return $find->count() ? false : true ;
        });

        $rules = [
            "effective_date_from" => "required|no_conflict_freeze:".$this->segment(2).",effective_date_to,".$this->segment(4) ,
            "remarks" => "required",
        ];

        if( $this->get('effective_date_to') ){
            $rules['effective_date_to'] = "required|date|after:effective_date_from";
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'effective_date_from.no_conflict_freeze' => "There is a conflict of schedule or a record with TBA on end date."
        ];
    }
}
