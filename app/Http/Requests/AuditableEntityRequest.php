<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AuditableEntityRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $table = 'auditable_entities';
        $table .= $this->get('_type') == 'actual' ? '_actual' : ''; //department_code
        $rules =  [
            'name' => 'required|unique:'.$table.',auditable_entity_name,'.$this->get('id').',auditable_entity_id',
            'company' => 'required',
        ];

        if($this->get('department_code')) $rules['branch']  = 'required';
        if($this->get('branch')) $rules['department_code']  = 'required';

        return $rules;
    }

    public function messages()
    {
        return [
            'name.unique' => 'Auditable entity already exists.',
        ];
    }
}
