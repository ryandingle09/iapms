<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AuditorGradingMatrixRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'variance_from' => 'required|numeric|max:'.($this->get('variance_to') - 1).'|unique_range:variance_to,'.$this->segment(2),
            'variance_to' => 'required|numeric|min:'.($this->get('variance_from') + 1),
            'rating' => 'required|regex:/[0-9][.]?[0-9]*/|numeric|min:0',
            'quality_max' => 'required|regex:/[0-9][.]?[0-9]*/|numeric|min:0',
            'documentation_max' => 'required|regex:/[0-9][.]?[0-9]*/|numeric|min:0',
        ];
    }

    public function messages()
    {
        return [
            'variance_from.max' => 'The :attribute must be less than to variance to',
            'variance_from.unique_range' => 'Variance range exists.',
            'variance_to.min' => 'The :attribute must be greater than to variance from',
        ];
    }
}
