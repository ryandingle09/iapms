<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ApprovalRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cat' => 'required', 
            'subcat' => 'required', 
            // 'user_id' => 'required', 
            'status' => 'required', 
            'ref_id' => 'required', 
            // 'auditor_type' => 'required',
            // 'remarks' => 'required'
            'status_to' => 'required', 
        ];
    }
}
