<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class ProjectScopeAuditorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if( $this->segment(4) ){
            return [
                "allotted_misc_mandays" => "required|min:0|max:999|numeric",
            ];
        }
        return [
            "auditor" => "required|unique:project_scope_auditors,auditor_id,NULL,project_scope_auditor_id,project_scope_id,".$this->segment(2),
            'role' => 'required',
            'allotted_misc_mandays' => "required|min:0|max:999|numeric",
        ];
    }
}
