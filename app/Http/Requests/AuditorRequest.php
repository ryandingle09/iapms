<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AuditorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required|unique:auditors,first_name,'.$this->segment(2).',auditor_id,middle_name,'.$this->get('middle_name').',last_name,'.$this->get('last_name'),
            'last_name' => 'required',
            'employee_number' => 'required',
            'email_address' => 'required|email',
            'gender' => 'required',
            'position' => 'required',
            'supervisor' => 'required',
            // proficiency code
            'code.rate.*' => 'required|integer',
        ];

        if( $this->get('start_date') ){
            $rules['start_date'] = 'required|date';
        }

        if( $this->get('end_date') ){
            $rules['end_date'] = 'required|date|after:start_date';
        }

        $codes = $this->get('code');
        if( !is_null($codes) ) {
            for( $x=0; $x < count($codes['value']); $x++ ) {
                $rules['code.start_date.'.$x] = 'required|date|before_equal:code.end_date.'.$x;
                $rules['code.end_date.'.$x] = 'required|date';
                for( $i=0; $i < count($codes['value']); $i++ ) {
                    if( $x !== $i ) $rules['code.value.'.$x] = 'required|different:code.value.'.$i;
                }
            }
        }

        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.unique' => 'Auditor already exists.',
            'start_date.before_equal' => 'Start date must be less than or equal to End date.',
            'code.value.*.unique' => 'Proficiency code already exists.',
            'code.value.*.different' => 'Proficiency code must be unique.',
            'code.value.*.required' => 'The proficiency code is required.',
            'code.rate.*.required' => 'The proficiency rate is required.',
            'code.rate.*.integer' => 'The proficiency rate must be an integer.',
            'code.start_date.*.required' => 'Proficiency start date is required.',
            'code.end_date.*.required' => 'Proficiency end date is required.',
            'code.start_date.*.date' => 'Proficiency start date must be date.',
            'code.end_date.*.date' => 'Proficiency end date must be date.',
            'code.start_date.*.before_equal' => 'Proficiency start date must be less than or equal end data.',
        ];
    }
}
