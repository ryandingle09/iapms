<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BpMandaysRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'walk_thru_mandays' => 'regex:/[0-9][.]?[0-9]*/',
            'planning_mandays' => 'regex:/[0-9][.]?[0-9]*/',
            'followup_mandays' => 'regex:/[0-9][.]?[0-9]*/',
            'discussion_mandays' => 'regex:/[0-9][.]?[0-9]*/',
            'draft_dar_mandays' => 'regex:/[0-9][.]?[0-9]*/',
            'final_report_mandays' => 'regex:/[0-9][.]?[0-9]*/',
            'wrap_up_mandays' => 'regex:/[0-9][.]?[0-9]*/',
            'reviewer_mandays' => 'regex:/[0-9][.]?[0-9]*/',
        ];
    }
}
