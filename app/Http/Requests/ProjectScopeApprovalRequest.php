<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjectScopeApprovalRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'project_scope_id' => "required",
            // 'approval_seq' => "required",
            // 'status_from' => "required",
            // 'status_to' => "required",
            // 'auditor' => "required",
            'remarks' => "required",
            // 'role' => "required",
            // 'status_date' => "required"
        ];
    }
}
