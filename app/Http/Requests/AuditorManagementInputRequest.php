<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AuditorManagementInputRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'audit_year'    => 'required|numeric',
            'auditor'       => 'required|numeric',
            'template_name' => 'required',
        ];
    }

    /*public function messages()
    {
        return [
            'auditor.required' => 'The auditor field is required.',
        ];
    }*/
}
