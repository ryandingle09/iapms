<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class QuestionnaireRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $questionnaire = $this->get('questionnaire');
        $rules = [
            'name'              => 'required|unique:questionnaires,questionnaire_name,'.$this->get('id').',questionnaire_id',
            'question_type'     => 'required',
            // details
            'details' => 'required',
            'questionnaire.*.question'  => 'required',
            'questionnaire.*.required'  => 'required',
            'questionnaire.*.page_break_after'  => 'required',
            'questionnaire.*.optional_remarks'  => 'required',
            'questionnaire.*.response'  => 'required',
            'questionnaire.*.datatype'  => 'required',
        ];

        if(!empty($questionnaire)) {
            for($r=0; $r < count($questionnaire); $r++) {
                if( isset($questionnaire[$r]['range']) ) {
                    $temp = $questionnaire[$r]['range']['from'];
                    $min = ( is_numeric($temp) ? $temp + 1 :  1);
                    $rules['questionnaire.'.$r.'.range.from'] = 'required|integer';
                    $rules['questionnaire.'.$r.'.range.to'] = 'required|integer|min:'.$min;
                }

                if( isset($questionnaire[$r]['choices']) ) {
                    $rules['questionnaire.'.$r.'.choices'] = 'required';
                }
            }
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'questionnaire.*.question.required'  => 'Question field is required.',
            'questionnaire.*.required.required'  => 'Response Required field is required.',
            'questionnaire.*.page_break_after.required'  => 'Page Break After field is required.',
            'questionnaire.*.optional_remarks.required'  => 'Optional Remarks field is required.',
            'questionnaire.*.datatype.required'  => 'Data type field is required.',
            'questionnaire.*.response.required'  => 'Response Type field is required.',
            'questionnaire.*.response.required'  => 'Response Type field is required.',
            'questionnaire.*.range.from.required'  => 'Range From field is required.',
            'questionnaire.*.range.to.required'  => 'Range To field is required.',
            'questionnaire.*.range.from.integer'  => 'Range From field must be a number.',
            'questionnaire.*.range.to.integer'  => 'Range To field  must be a number.',
            'questionnaire.*.range.from.max'  => 'Range From field must be less than Range To field.',
            'questionnaire.*.range.to.min'  => 'Range To field must be greater than Range From field.',
            'questionnaire.*.choices.required'  => 'Choices Name field is required.',
            'details.required'  => 'Please enter atleast 1 question.',
        ];
    }
}
