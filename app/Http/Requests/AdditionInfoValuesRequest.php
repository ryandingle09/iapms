<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdditionInfoValuesRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'additional_info.value.text.*' => 'required',
            'additional_info.value.number.*' => 'required|integer',
            'additional_info.value.date.*' => 'required|date',
        ];
    }

    public function messages()
    {
        return [
            'additional_info.value.text.*.required' => 'Additional Info value is required.',
            'additional_info.value.number.*.required' => 'Additional Info value is required.',
            'additional_info.value.number.*.integer' => 'Additional Info value must be a number.',
            'additional_info.value.date.*.required' => 'Additional Info value is required.',
            'additional_info.value.date.*.date' => 'Additional Info value must be a date',
        ];
    }
}
