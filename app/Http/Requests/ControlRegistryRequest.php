<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ControlRegistryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            '_assertions' => 'integer|min:1',
            'assertions.*' => 'required',
            '_types' => 'integer|min:1',
            'control_types.*' => 'required',
            'frequency' => 'required',
            'mode' => 'required',
            'control_source' => 'required',
        ];
        if( $this->get('id') == '' ) {
            $rules['control_code'] = 'required|unique:controls_master,control_code,null,control_id';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            '_assertions.min' => 'Select atleast 1 assertion.',
            '_types.min' => 'Select atleast 1 control type.',
        ];
    }
}
