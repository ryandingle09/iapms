<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HolidayRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $ignore_id = $this->segment(5) ? $this->segment(5) : 'NULL';
        return [
            'holiday_date' => 'required|date|unique:holidays,holiday_date,'.$ignore_id.'|recurring_date',
            'holiday_type' => 'required|unique_holiday:fixed_date,holiday_date,'.$ignore_id,
        ];
    }
     /**
     * Overide all() method to convert input before submitting to validation.
     * Source : https://stackoverflow.com/questions/39689404/change-date-format-before-validation
     * @return array
     */
    public function all()
    {
        $holiday_date = $this->input('holiday_date') ? date('Y-m-d', strtotime($this->input('holiday_date'))) : '';
        $this->merge(array('holiday_date' => $holiday_date ));
        $attributes = parent::all();
        return $attributes;
    }

    public function messages()
    {
        return [
            'unique_holiday' => 'Holiday already exists.',
            'holiday_date.recurring_date' => 'Holiday date exists as recurring holiday.',
        ];
    }
}
