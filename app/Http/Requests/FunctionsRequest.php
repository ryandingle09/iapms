<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FunctionsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:functions,function_name,'.$this->get('fid').',function_id',
            'exec_name' => 'required',
            'exec_type' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
        ];
    }

    public function messages()
    {
        return [
            'exec_name.required' => 'Executable name is required',
            'exec_type.required' => 'Executable type is required',
        ];
    }
}
