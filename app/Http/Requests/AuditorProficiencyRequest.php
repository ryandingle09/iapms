<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AuditorProficiencyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $segments = $this->segments();
        $rules = [
            'proficiency_rate' => 'required',
            'proficiency_type' => 'required|unique:auditor_proficiency,proficiency_type,'.$this->get('proficiency_id').',auditor_proficiency_id,auditor_id,'.$segments[1],
        ];

        return $rules;
    }
}
