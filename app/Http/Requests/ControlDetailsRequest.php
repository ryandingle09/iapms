<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ControlDetailsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sequence' => 'required|integer|min:1',
            'control_code' => 'required|unique:controls_detail,control_code,'.$this->get('control_det_id').',control_det_id,control_id,'.$this->get('control_id'),
        ];
    }
}
