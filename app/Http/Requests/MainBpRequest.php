<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MainBpRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'main_bp' => 'required|unique:ae_mbp,main_bp_name,null,ae_mbp_id,auditable_entity_id,'.$this->get('entity_id'),
        ];
    }
}
