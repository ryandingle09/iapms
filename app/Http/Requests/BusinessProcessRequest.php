<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BusinessProcessRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if( $this->get('_method') != 'PUT' ) {
            $rules = [
                'bp_name' => 'required|unique:business_processes,bp_name,null,bp_id',
                'bp_code' => 'required',
            ];
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'bp_name.required' => 'Business process name field is required!',
            'bp_name.unique' => 'Business process name already exists!',
            'bp_code.required' => 'Business process code field is required!',
        ];
    }
}
