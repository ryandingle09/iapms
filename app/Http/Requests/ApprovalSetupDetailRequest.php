<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ApprovalSetupDetailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'approval_setup_master_id' => 'required',
            'approval_sequence' => 'required|numeric',
            'auditor_type' => 'required',
            // 'approver_flag' => 'required',
        ];
    }
}
