<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjectScopeApgFindingsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'project_scope_apg_id' => 'required',
            // 'non_issue' => 'required',
            // 'highlight' => 'required',
            'issues' => 'required',
            'conclusions' => 'required',
            'recommendations' => 'required',
            'auditee_actions_needed' => 'required',
            'auditee_action_due_date' => 'required',
            'auditee_actions_taken' => 'required',
            'auditor_remarks' => 'required',
            // 'auditor_id' => 'required',
            // 'auditee_id' => 'required',
        ];
    }
}
