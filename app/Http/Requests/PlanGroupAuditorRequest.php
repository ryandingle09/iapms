<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PlanGroupAuditorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'auditor' => 'required|unique:plan_group_auditors,auditor_id,NULL,plan_group_auditor_id,plan_group_id,'.$this->segment(2)
        ];
    }
}
