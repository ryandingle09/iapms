<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BusinessProcessStepRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sequence' => 'required|integer|min:1|unique:business_processes_steps,bp_steps_seq,'.$this->get('bp_steps_id').',bp_steps_id,bp_objective_id,'.$this->get('bp_obj_id'),
            'activity_narrative' => 'required',
        ];
    }
}
