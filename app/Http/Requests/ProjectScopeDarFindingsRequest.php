<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProjectScopeDarFindingsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "business_process_step" => 'required',
            "risk" => 'required',
            "recommendations" => 'required',
            "auditee_actions_taken" => 'required'
        ];
    }
}
