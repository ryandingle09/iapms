<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PlanRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $plan_id = $this->segment(2) ? : 'NULL';
        $current_year = date('Y');

        $rules = [
            'plan_year' => 'required|numeric|min:'.$current_year.'|unique:plan,plan_year,'.$plan_id.',plan_id',
            'plan_name' => 'required',
            'plan_status' => 'required',
        ];

        return $rules;
    }

    public function messages()
    {
        return [
            'audit_year.unique' => 'The audit year '.$this->get('audit_year').' is already exists.',
        ];
    }
}
