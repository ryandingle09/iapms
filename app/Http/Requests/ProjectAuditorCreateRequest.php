<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class ProjectAuditorCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "auditor" => "required|unique:project_auditors,auditor_id,NULL,project_id,project_id,".$this->segment(2),
            "role" => "required"
        ];
    }
}
