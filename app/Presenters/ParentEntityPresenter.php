<?php

namespace App\Presenters;

use App\Transformers\ParentEntityTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ParentEntityPresenter
 *
 * @package namespace App\Presenters;
 */
class ParentEntityPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ParentEntityTransformer();
    }
}
