<?php

namespace App\Presenters;

use App\Transformers\ProjectAuditorTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ProjectAuditorPresenter
 *
 * @package namespace App\Presenters;
 */
class ProjectAuditorPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ProjectAuditorTransformer();
    }
}
