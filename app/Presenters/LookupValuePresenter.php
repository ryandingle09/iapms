<?php

namespace App\Presenters;

use App\Transformers\LookupValueTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class LookupValuePresenter
 *
 * @package namespace App\Presenters;
 */
class LookupValuePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new LookupValueTransformer();
    }
}
