<?php

namespace App\Presenters;

use App\Transformers\AuditorSelectizePresenterTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AuditorSelectizePresenter
 *
 * @package namespace App\Presenters;
 */
class AuditorSelectizePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AuditorSelectizePresenterTransformer();
    }
}
