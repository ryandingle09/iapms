<?php

namespace App\Presenters;

use App\Transformers\AuditableEntityTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AuditableEntityPresenter
 *
 * @package namespace App\Presenters;
 */
class AuditableEntityPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AuditableEntityTransformer();
    }
}
