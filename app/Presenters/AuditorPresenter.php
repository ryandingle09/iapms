<?php

namespace App\Presenters;

use App\Transformers\AuditorTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AuditorPresenter
 *
 * @package namespace App\Presenters;
 */
class AuditorPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AuditorTransformer();
    }
}
