<?php

namespace App\Presenters;

use App\Transformers\PlanProjectTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PlanProjectPresenter
 *
 * @package namespace App\Presenters;
 */
class PlanProjectPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PlanProjectTransformer();
    }
}
