<?php

namespace App\Presenters;

use App\Transformers\RiskAssessmentScaleTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RiskAssessmentScalePresenter
 *
 * @package namespace App\Presenters;
 */
class RiskAssessmentScalePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RiskAssessmentScaleTransformer();
    }
}
