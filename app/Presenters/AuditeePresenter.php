<?php

namespace App\Presenters;

use App\Transformers\AuditeeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AuditeePresenter
 *
 * @package namespace App\Presenters;
 */
class AuditeePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AuditeeTransformer();
    }
}
