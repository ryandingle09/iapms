<?php

namespace App\Presenters;

use App\Transformers\BusinessProcessInquiryTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BusinessProcessInquiryPresenter
 *
 * @package namespace App\Presenters;
 */
class BusinessProcessInquiryPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BusinessProcessInquiryTransformer();
    }
}
