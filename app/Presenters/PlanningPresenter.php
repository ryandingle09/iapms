<?php

namespace App\Presenters;

use App\Abstracts\FractalRelationshipsPresenter;
use App\Transformers\PlanningTransformer;

/**
 * Class PlanningPresenter
 *
 * @package namespace App\Presenters;
 */
class PlanningPresenter extends FractalRelationshipsPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
    	return new PlanningTransformer();
    }
}
