<?php

namespace App\Presenters;

use App\Transformers\BusinessProcessesStepTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BusinessProcessesStepPresenter
 *
 * @package namespace App\Presenters;
 */
class BusinessProcessesStepPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BusinessProcessesStepTransformer();
    }
}
