<?php

namespace App\Presenters;

use App\Transformers\LookupTypeValueTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class LookupTypeValuePresenter
 *
 * @package namespace App\Presenters;
 */
class LookupTypeValuePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new LookupTypeValueTransformer();
    }
}
