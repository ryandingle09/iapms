<?php

namespace App\Presenters;

use App\Transformers\BpObjectivesTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BpObjectivesPresenter
 *
 * @package namespace App\Presenters;
 */
class BpObjectivesPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BpObjectivesTransformer();
    }
}
