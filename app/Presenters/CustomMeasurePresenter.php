<?php

namespace App\Presenters;

use App\Abstracts\FractalRelationshipsPresenter;
use App\Transformers\CustomMeasureTransformer;

/**
 * Class CustomMeasurePresenter
 *
 * @package namespace App\Presenters;
 */
class CustomMeasurePresenter extends FractalRelationshipsPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CustomMeasureTransformer();
    }
}
