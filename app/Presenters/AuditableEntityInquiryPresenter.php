<?php

namespace App\Presenters;

use App\Transformers\AuditableEntityInquiryTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AuditableEntityInquiryPresenter
 *
 * @package namespace App\Presenters;
 */
class AuditableEntityInquiryPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AuditableEntityInquiryTransformer();
    }
}
