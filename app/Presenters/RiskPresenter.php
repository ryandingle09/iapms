<?php

namespace App\Presenters;

use App\Transformers\RiskTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RiskPresenter
 *
 * @package namespace App\Presenters;
 */
class RiskPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RiskTransformer();
    }
}
