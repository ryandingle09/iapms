<?php

namespace App\Presenters;

use App\Transformers\ValueSetTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ValueSetPresenter
 *
 * @package namespace App\Presenters;
 */
class ValueSetPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ValueSetTransformer();
    }
}
