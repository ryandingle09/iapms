<?php

namespace App\Presenters;

use App\Transformers\PlanProjectScopeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PlanProjectScopePresenter
 *
 * @package namespace App\Presenters;
 */
class PlanProjectScopePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PlanProjectScopeTransformer();
    }
}
