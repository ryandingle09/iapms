<?php

namespace App\Presenters;

use App\Transformers\CustomMeasureActualTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class CustomMeasureActualPresenter
 *
 * @package namespace App\Presenters;
 */
class CustomMeasureActualPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CustomMeasureActualTransformer();
    }
}
