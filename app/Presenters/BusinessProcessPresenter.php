<?php

namespace App\Presenters;

use App\Transformers\BusinessProcessTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class BusinessProcessPresenter
 *
 * @package namespace App\Presenters;
 */
class BusinessProcessPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new BusinessProcessTransformer();
    }
}
