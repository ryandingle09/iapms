<?php

namespace App\Presenters;

use App\Abstracts\FractalRelationshipsPresenter;
use App\Transformers\PlanningAuditorTransformer;

/**
 * Class PlanningAuditorPresenter
 *
 * @package namespace App\Presenters;
 */
class PlanningAuditorPresenter extends FractalRelationshipsPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PlanningAuditorTransformer();
    }
}
