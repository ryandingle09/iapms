<?php
namespace App\Abstracts;

use Prettus\Repository\Eloquent\BaseRepository;

abstract class PresentableRepository extends BaseRepository
{
    public function present($relations)
    {
        $this->presenter->parseIncludes($relations);
        return $this;
    }

}