<?php

namespace App;

use App\Traits\Authorable;
use App\Traits\Updater;
use Artesaos\Defender\Traits\HasDefender;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasDefender, Updater, Authorable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_name',
        'employee_id',
        'user_tag',
        'user_tag_ref_id',
        'password',
        'description',
        'display_name',
        'effective_start_date',
        'effective_end_date',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $appends = [
        'full_name',
        'user_tag_info'
    ];

    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updater()
    {
        return $this->belongsTo('App\User', 'last_update_by');
    }

    public function createdLookupType()
    {
        return $this->hasMany('App\Models\LookupType', 'created_by');
    }

    public function updatedLookupType()
    {
        return $this->belongsTo('App\Models\LookupType', 'last_update_by');
    }

    public function createdLookupValue()
    {
        return $this->hasMany('App\Models\LookupValue', 'created_by');
    }

    public function updatedLookupValue()
    {
        return $this->belongsTo('App\Models\LookupValue', 'last_update_by');
    }

    public function responsibilityAssignment()
    {
        return $this->hasMany('App\Models\UserResponsibilityAssignment');
    }

    public function userResponsibility()
    {
        return $this->belongsToMany('App\Models\Responsibility', 'user_resp_assignments');
    }

    public function createdResponsibility()
    {
        return $this->hasMany('App\Models\Responsibility', 'created_by');
    }

    public function updatedResponsibility()
    {
        return $this->hasMany('App\Models\Responsibility', 'last_update_by');
    }

    public function getLastUpdateDateAttribute($value)
    {
        return strtotime($value) > 0 ? date('d-M-Y', strtotime($value) ) : null;
    }

    public function getLastLoginDateAttribute($value)
    {
        return strtotime($value) > 0 ? date('d-M-Y', strtotime($value) ) : null;
    }

    public function setEffectiveStartDateAttribute($value)
    {
        $this->attributes['effective_start_date'] = date('Y-m-d', strtotime($value) );
    }

    public function setEffectiveEndDateAttribute($value)
    {
        $this->attributes['effective_end_date'] = date('Y-m-d', strtotime($value) );
    }

    public function getEffectiveStartDateAttribute($value)
    {
        return strtotime($value) > 0 ? date_display($value)  : null;
    }

    public function getEffectiveEndDateAttribute($value)
    {
        return strtotime($value) > 0 ? date_display($value)  : null;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function auditee()
    {
        return $this->hasOne('App\Models\Auditee', 'auditee_id', 'user_tag_ref_id');
    }

    public function auditor()
    {
        return $this->hasOne('App\Models\Auditor', 'auditor_id', 'user_tag_ref_id');
    }

    public function getUserTagInfoAttribute(){
        if($this->user_tag == 'Auditee'){
            return $this->auditee;
        }else if($this->user_tag == 'Auditor'){
            return $this->auditor;
        }else{
            return null;
        }
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role','user_has_roles');
    }

    public function allowedMenu(){
        $merged_allowed_menus = [];
        foreach ($this->roles as $key => $value) {
            $array = array_pluck($value->menus->toArray(), 'id');
            $merged_allowed_menus = array_merge($merged_allowed_menus,$array);
        }
        return array_unique($merged_allowed_menus);
    }

    public function getFullNameAttribute(){
        if($this->user_tag == 'Auditee'){
            return $this->auditee->full_name;
        }else if($this->user_tag == 'Auditor'){
            return $this->auditor->full_name;
        }else{
            return $this->display_name;
        }
    }

    // public function groupHead(){
    //     if($this->user_tag == 'Auditor'){
    //         $auditor_service = app('App\Services\AuditorService');
    //         return $auditor_service->immediate("",)
    //     }
    //     return 0;
    // }
}
