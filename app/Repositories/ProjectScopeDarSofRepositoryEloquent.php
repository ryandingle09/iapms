<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectScopeDarSofRepository;
use App\Models\ProjectScopeDarSof;
use App\Validators\ProjectScopeDarSofValidator;

/**
 * Class ProjectScopeDarSofRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectScopeDarSofRepositoryEloquent extends BaseRepository implements ProjectScopeDarSofRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectScopeDarSof::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
