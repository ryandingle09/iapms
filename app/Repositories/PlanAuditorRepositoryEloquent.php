<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PlanAuditorRepository;
use App\Models\PlanAuditor;
use App\Validators\PlanAuditorValidator;

/**
 * Class PlanAuditorRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PlanAuditorRepositoryEloquent extends BaseRepository implements PlanAuditorRepository
{
    protected $fieldSearchable = [
        'auditor.first_name' => "like",
        'auditor.last_name' => "like",
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PlanAuditor::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
