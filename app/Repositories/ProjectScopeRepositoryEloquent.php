<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectScopeRepository;
use App\Models\ProjectScope;
use App\Validators\ProjectScopeValidator;
use App\Criteria\CurrentUserProjectScopesCriteria;

/**
 * Class ProjectScopeRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectScopeRepositoryEloquent extends BaseRepository implements ProjectScopeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectScope::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(app(CurrentUserProjectScopesCriteria::class));
    }
}
