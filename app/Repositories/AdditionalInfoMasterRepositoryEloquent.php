<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AdditionalInfoMasterRepository;
use App\Models\AdditionalInfoMaster;
use App\Validators\AdditionalInfoMasterValidator;

/**
 * Class AdditionalInfoMasterRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AdditionalInfoMasterRepositoryEloquent extends BaseRepository implements AdditionalInfoMasterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AdditionalInfoMaster::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
