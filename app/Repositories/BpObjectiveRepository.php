<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BpObjectiveRepository
 * @package namespace App\Repositories;
 */
interface BpObjectiveRepository extends RepositoryInterface
{
    public function getRisksAssessmentByEntityBp($entity_id, $bp_id);

    public function getObjectiveRisks($objective_id);
}
