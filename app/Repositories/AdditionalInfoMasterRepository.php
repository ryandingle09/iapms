<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AdditionalInfoMasterRepository
 * @package namespace App\Repositories;
 */
interface AdditionalInfoMasterRepository extends RepositoryInterface
{
    //
}
