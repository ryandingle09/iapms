<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HolidayRepository
 * @package namespace App\Repositories;
 */
interface HolidayRepository extends RepositoryInterface
{
    //
}
