<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EnterpriseRiskDetailRepository
 * @package namespace App\Repositories;
 */
interface EnterpriseRiskDetailRepository extends RepositoryInterface
{
    public function getAll();
}
