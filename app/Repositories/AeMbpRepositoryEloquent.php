<?php

namespace App\Repositories;

use App\Abstracts\PresentableRepository;
use App\Models\AuditableEntitiesActual;
use App\Models\AuditableEntity;
use App\Models\CompanyProfile;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\AeMbp;
use Log;

/**
 * Class AeMbpRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AeMbpRepositoryEloquent extends PresentableRepository implements AeMbpRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AeMbp::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param AeMbp $main_bp
     * @param Bool $delete
     */
    public function applyToActualEntities(AeMbp $main_bp, $delete = false)
    {
        $entity = $main_bp->auditableEntity;
        $related_companies = $this->getRelatedCompanies($entity);

        foreach ($related_companies as $company) {
            if( $company->branches->count() ) {
                $branch_codes = $company->branches->pluck('branch_code');
                # Get all related actual entities based on company_code, branch_codes & department_code
                $actual_entities = AuditableEntitiesActual::whereIn('branch_code', $branch_codes)
                ->relatedCompanyDepartment($company->company_code, $entity->department_code)
                ->get();

                foreach ($actual_entities as $entity) {
                    if( $delete ) {
                        $main_bp->actual()->delete();
                        Log::info('[Method::applyToActualEntities] Actual Entity Main BP deleted.');
                    }
                    else {
                        $main_bp->actual()->create([
                            'auditable_entity_id' => $entity->auditable_entity_id,
                            'main_bp_name' => $main_bp->main_bp_name,
                        ]);
                        Log::info('[Method::applyToActualEntities] Actual Entity Main BP created. [entity => '.$entity->auditable_entity_id.', main_bp_name => '.$main_bp->main_bp_name.']');
                    }
                }
            }
        }
    }

    /**
     * Get related companies with branches based on business_type, group_type & branch entity_class
     * @param AuditableEntity $entity
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getRelatedCompanies(AuditableEntity $entity)
    {
        $companies = CompanyProfile::with(['branches' => function($b) use($entity){
            $b->where('entity_class', $entity->entity_class);
        }])
        ->where('business_type', $entity->company_code) // In master table, company_code = actual business_type
        ->where('group_type', $entity->group_type)
        ->get();

        return $companies;
    }

    public function averageCustomMeasures($conditions){
        $query = $this->model;
        // $query = $query->join('')
        return $query;
    }
}
