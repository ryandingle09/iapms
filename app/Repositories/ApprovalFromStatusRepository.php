<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ApprovalFromStatusRepository
 * @package namespace App\Repositories;
 */
interface ApprovalFromStatusRepository extends RepositoryInterface
{
    //
}
