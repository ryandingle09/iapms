<?php

namespace App\Repositories;

use App\Models\LookupType;
use App\Repositories\LookupTypeRepository;
use App\Validators\LookupTypeValidator;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class LookupTypeRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LookupTypeRepositoryEloquent extends BaseRepository implements LookupTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LookupType::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
