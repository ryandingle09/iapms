<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MatrixDetailRepository
 * @package namespace App\Repositories;
 */
interface MatrixDetailRepository extends RepositoryInterface
{
    //
}
