<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditorTrainingRepository
 * @package namespace App\Repositories;
 */
interface AuditorTrainingRepository extends RepositoryInterface
{
    //
}
