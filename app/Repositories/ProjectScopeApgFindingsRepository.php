<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectScopeApgFindingsRepository
 * @package namespace App\Repositories;
 */
interface ProjectScopeApgFindingsRepository extends RepositoryInterface
{
    //
}
