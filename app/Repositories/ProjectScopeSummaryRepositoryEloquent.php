<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectScopeSummaryRepository;
use App\Models\ProjectScopeSummary;
use App\Validators\ProjectScopeSummaryValidator;

/**
 * Class ProjectScopeSummaryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectScopeSummaryRepositoryEloquent extends BaseRepository implements ProjectScopeSummaryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectScopeSummary::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
