<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\FunctionsRepository;
use App\Models\Functions;
use App\Validators\FunctionValidator;

/**
 * Class FunctionsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class FunctionsRepositoryEloquent extends BaseRepository implements FunctionsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Functions::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
