<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface IomDistListRepository
 * @package namespace App\Repositories;
 */
interface IomDistListRepository extends RepositoryInterface
{
    //
}
