<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditorManagementInputRepository
 * @package namespace App\Repositories;
 */
interface AuditorManagementInputRepository extends RepositoryInterface
{
    //
}
