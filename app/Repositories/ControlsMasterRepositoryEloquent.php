<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ControlsMasterRepository;
use App\Models\ControlsMaster;
use App\Validators\ControlsMasterValidator;

/**
 * Class ControlsMasterRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ControlsMasterRepositoryEloquent extends BaseRepository implements ControlsMasterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ControlsMaster::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
