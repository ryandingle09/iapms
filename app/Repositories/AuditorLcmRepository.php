<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditorLcmRepository
 * @package namespace App\Repositories;
 */
interface AuditorLcmRepository extends RepositoryInterface
{
    //
}
