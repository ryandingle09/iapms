<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AuditorTrainingRepository;
use App\Models\AuditorTraining;
use App\Validators\AuditorTrainingValidator;

/**
 * Class AuditorTrainingRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditorTrainingRepositoryEloquent extends BaseRepository implements AuditorTrainingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AuditorTraining::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
