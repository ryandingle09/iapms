<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RiskAssessmentScaleRepository
 * @package namespace App\Repositories;
 */
interface RiskAssessmentScaleRepository extends RepositoryInterface
{
    public function getAll($type = 'master');

    public function withFullAttributes();
}
