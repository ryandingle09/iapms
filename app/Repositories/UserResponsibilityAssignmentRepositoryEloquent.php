<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\UserResponsibilityAssignmentRepository;
use App\Models\UserResponsibilityAssignment;
use App\Validators\UserResponsibilityAssignmentValidator;

/**
 * Class UserResponsibilityAssignmentRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserResponsibilityAssignmentRepositoryEloquent extends BaseRepository implements UserResponsibilityAssignmentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserResponsibilityAssignment::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
