<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BpStepsRiskRepository
 * @package namespace App\Repositories;
 */
interface BpStepsRiskRepository extends RepositoryInterface
{
    //
}
