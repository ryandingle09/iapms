<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectScopeFreezeRepository
 * @package namespace App\Repositories;
 */
interface ProjectScopeFreezeRepository extends RepositoryInterface
{
    //
}
