<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ApprovalToStatusRepository;
use App\Models\ApprovalToStatus;
use App\Validators\ApprovalToStatusValidator;

/**
 * Class ApprovalToStatusRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ApprovalToStatusRepositoryEloquent extends BaseRepository implements ApprovalToStatusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ApprovalToStatus::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
