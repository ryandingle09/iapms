<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ControlsTestAuditTypeRepository
 * @package namespace App\Repositories;
 */
interface ControlsTestAuditTypeRepository extends RepositoryInterface
{
    //
}
