<?php

namespace App\Repositories;

use App\Criteria\RiskAssessmentScaleCriteria;
use App\Models\RiskAssessmentScale;
use App\Repositories\RiskAssessmentScaleRepository;
use App\Validators\RiskAssessmentScaleValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
/**
 * Class RiskAssessmentScaleRepositoryEloquent
 * @package namespace App\Repositories;
 */
class RiskAssessmentScaleRepositoryEloquent extends BaseRepository implements RiskAssessmentScaleRepository
{
    protected $fieldSearchable = [
        'auditable_entity_id',
        'ae_mbp_id',
        'bp_id',
        'bp_objective_id',
        'risk_id',
        'type'
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RiskAssessmentScale::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(app(RiskAssessmentScaleCriteria::class));
    }

    public function getAll($type = 'master')
    {
        $this->applyCriteria();
        $table = 'auditable_entities';
        $table .= $type == 'actual' ? '_'.$type : '';
        $ras = $this->model->select('ae.*', 'mbp.main_bp_name',  'bp.*', 'bpo.bp_objective_id', 'bpo.objective_name', 'bpo.objective_category', 'bpo.objective_narrative', 'r.*', 'risk_assessment_scale.*')
        ->joinEntity($type)
        ->joinBp()
        ->leftJoin('bp_objectives AS bpo', 'bpo.bp_id', '=', 'bp.bp_id')
        ->leftJoin('business_processes_steps AS bps', 'bpo.bp_objective_id', '=', 'bps.bp_objective_id')
        ->leftJoin('bp_steps_risks AS bpsr', 'bpsr.bp_step_id', '=', 'bps.bp_steps_id')
        ->joinRisk()
        ->whereType($type)
        ->groupByEntityBpRisk();
        if( $type == 'actual' ) {
            $ras->leftJoin('ae_mbp AS mbp', 'mbp.auditable_entity_id', '=', 'ae.master_auditable_entity_id')->companyDetails($table, 'ae')->branchDetails(false, $table);
        }
        else {
            $ras->leftJoin('ae_mbp AS mbp', 'mbp.auditable_entity_id', '=', 'ae.auditable_entity_id');
        }

        return $ras->get();
    }


    /**
    * Get the auditable entity and business process combination adjusted severity value
    * it can also be used to get individual values for the risk assessment scale 
    *
    * @author Romnick John M. Susa
    * @link   Author used database/queries/ras_full_attribute.sql to create this function.
    *         Please update the file if you are going to update the function below.
    * @param  (array) $params
    * @return \Collections
    **/

    public function withFullAttributes($conditions = []){
        $year = isset($combinations['year']) ? $combinations['year'] : date('Y');
        $tla_arp_percent = ".75";
        $query = $this->model;

        $query = $query->from('risk_assessment_scale as ras')
                       ->leftJoin('matrix as m_ri','m_ri.matrix_name','=', DB::raw("'Risk Index'") )
                       ->leftJoin('matrix_details as md_ri',function($q){
                            $q->on('md_ri.x_sequence', '=', 'inherent_impact_value');
                            $q->on('md_ri.y_sequence', '=', 'inherent_likelihood_value');
                            $q->on('md_ri.matrix_id', '=', 'm_ri.matrix_id');
                       })
                       ->leftJoin('matrix as m_sw','m_sw.matrix_name','=', DB::raw("'Severity Weight'") )
                       ->leftJoin('matrix_details as md_sw',function($q){
                            $q->on('md_sw.color', '=', 'md_ri.color');
                            $q->on('md_sw.matrix_id', '=', 'm_sw.matrix_id');
                       })
                       ->leftJoin(DB::raw('(SELECT grade, planned_start_date AS last_audit_date, auditable_entity_id, ae_mbp_id FROM ia_project_scope_summary t1 WHERE planned_start_date = (SELECT MAX(t2.planned_start_date) FROM ia_project_scope_summary t2 WHERE t2.auditable_entity_id = t1.auditable_entity_id AND t2.ae_mbp_id = t1.ae_mbp_id) GROUP BY auditable_entity_id, ae_mbp_id) as ia_psm') , function($q){ $q->on('psm.auditable_entity_id', '=', 'ras.auditable_entity_id');
                            $q->on('psm.ae_mbp_id', '=', 'ras.ae_mbp_id');
                       })
                       ->leftJoin('matrix as m_pai',function($q){
                            $q->on('m_pai.matrix_name', '=', DB::raw("'Past Audit Index'") );
                       })
                       ->leftJoin('matrix_details as md_pai',function($q) use ($year){
                            $q->on('md_pai.matrix_id', '=', 'm_pai.matrix_id' );
                            $q->on('md_pai.x_value', '=', 'psm.grade' );
                            $q->on('md_pai.y_sequence' , '=', DB::raw("IF ( $year - CAST(YEAR(STR_TO_DATE(ia_psm.last_audit_date, '%Y-%m-%d') ) AS UNSIGNED ) > 3, 3, $year - CAST(YEAR(STR_TO_DATE(ia_psm.last_audit_date, '%Y-%m-%d') ) AS UNSIGNED ) )") );
                       });
        foreach($conditions as $key => $value){
            if($key != "year" && $value != null){
                $query = $query->where( $key, '=', $value );
                $query = $query->addSelect( $key );
            }
        }

        $query = $query->addSelect(
            'ras.inherent_impact_value',
            'ras.inherent_likelihood_value',
            DB::raw('( inherent_impact_value * inherent_likelihood_value ) risk_score'),
            'md_ri.matrix_value as risk_index',
            'md_ri.color as risk_index_color',
            DB::raw('ia_md_sw.matrix_value as severity_weight'),
            DB::raw('( (ia_ras.inherent_impact_value * ia_ras.inherent_likelihood_value ) * ia_md_sw.matrix_value ) as severity_value'),
            DB::raw("IF ( $year - CAST(YEAR(STR_TO_DATE(ia_psm.last_audit_date, '%Y-%m-%d') ) AS UNSIGNED ) > 3, 3, $year - CAST(YEAR(STR_TO_DATE(ia_psm.last_audit_date, '%Y-%m-%d') ) AS UNSIGNED ) ) tla") ,
            'psm.grade',
            DB::raw('(ia_md_pai.matrix_value * 100) arp'),
            DB::raw("(((ia_ras.inherent_impact_value * ia_ras.inherent_likelihood_value ) * ia_md_sw.matrix_value ) * (ia_md_pai.matrix_value) ) as adjusted_severity_value") 
        );

        return $query;
    }
}
