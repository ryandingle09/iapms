<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\AeMbpSbp;
use App\Validators\AeMbpSbpValidator;
use Log;

/**
 * Class AeMbpSbpRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AeMbpSbpRepositoryEloquent extends BaseRepository implements AeMbpSbpRepository
{
    protected $fieldSearchable = [
        'businessProcess.bp_name' => "like",
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AeMbpSbp::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getBpRisks(AeMbpSbp $sub_bp)
    {
        return $sub_bp->join('bp_objectives', 'bp_objectives.bp_id', '=', 'ae_mbp_sbp.bp_id')
        ->join('business_processes_steps', 'business_processes_steps.bp_objective_id', '=', 'bp_objectives.bp_objective_id')
        ->join('bp_steps_risks', 'bp_steps_risks.bp_step_id', '=', 'business_processes_steps.bp_steps_id')
        ->groupBy('risk_id');
    }

    public function generateRiskAssessmentScale(Model $entity, $type = 'master')
    {
        // optimize this
        $entity->load('masterBp.subBp.objectives.risks');
        $attributes = [];
        $attributes['type'] = $type;
        foreach ($entity->masterBp as $main_bp) {
            if( $main_bp->subBp->count() ) {
                foreach ($main_bp->subBp as $sub_bp) {
                    $attributes['bp_id'] = $sub_bp->bp_id;
                    if( $sub_bp->objectives->count() ) {
                        foreach ($sub_bp->objectives as $objectives) {
                            if( $objectives->risks->count() ) {
                                foreach ($objectives->risks as $risks) {
                                    $attributes['risk_id'] = $risks->risk_id;
                                    $exists = $entity->riskAssessment()->where('bp_id', $sub_bp->bp_id)->where('risk_id', $risks->risk_id)->count();
                                    if( !$exists ) {
                                        $entity->riskAssessment()->create($attributes);
                                        Log::info('[Risk Assessment Scale] Successfully created: {bp_id => '.$sub_bp->bp_id.', risk_id => '.$risks->risk_id.'}');
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
