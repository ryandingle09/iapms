<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ApprovalSetupMasterRepository
 * @package namespace App\Repositories;
 */
interface ApprovalSetupMasterRepository extends RepositoryInterface
{
    //
}
