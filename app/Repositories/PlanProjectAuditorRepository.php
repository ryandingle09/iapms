<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlanProjectAuditorRepository
 * @package namespace App\Repositories;
 */
interface PlanProjectAuditorRepository extends RepositoryInterface
{
    //
}
