<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\BusinessProcessesStepRepository;
use App\Models\BusinessProcessesStep;
use App\Validators\BusinessProcessesStepValidator;

/**
 * Class BusinessProcessesStepRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BusinessProcessesStepRepositoryEloquent extends BaseRepository implements BusinessProcessesStepRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BusinessProcessesStep::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
