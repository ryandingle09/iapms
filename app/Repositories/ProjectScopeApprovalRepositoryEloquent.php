<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectScopeApprovalRepository;
use App\Models\ProjectScopeApproval;
use App\Validators\ProjectScopeApprovalValidator;

/**
 * Class ProjectScopeApprovalRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectScopeApprovalRepositoryEloquent extends BaseRepository implements ProjectScopeApprovalRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectScopeApproval::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
