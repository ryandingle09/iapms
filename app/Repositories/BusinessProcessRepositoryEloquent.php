<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\BusinessProcessRepository;
use App\Models\BusinessProcess;
use App\Validators\BusinessProcessValidator;

/**
 * Class BusinessProcessRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BusinessProcessRepositoryEloquent extends BaseRepository implements BusinessProcessRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BusinessProcess::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Get all the risk associated in the business process
     * @param  integer $bp_id Business process Id
     * @return Collection
     */
    public function getBpRisks($bp_id) {
        $risks = $this->model->select(\DB::raw('ia_risks.*, ia_lookup_values.description as risk_desc, ia_business_processes.bp_id, ia_business_processes.bp_name, ia_business_processes.bp_code'))
        ->leftJoin('bp_objectives', 'bp_objectives.bp_id', '=', 'business_processes.bp_id')
        ->leftJoin('business_processes_steps', 'business_processes_steps.bp_objective_id', '=', 'bp_objectives.bp_objective_id')
        ->leftJoin('bp_steps_risks', 'bp_steps_risks.bp_step_id', '=', 'business_processes_steps.bp_steps_id')
        ->leftJoin('risks', 'risks.risk_id', '=', 'bp_steps_risks.risk_id')
        ->leftJoin('lookup_values', 'lookup_values.lookup_code', '=', 'risks.risk_code')
        ->where('business_processes.bp_id', $bp_id)
        ->groupBy('risks.risk_id');

        return $risks->get();
    }
}
