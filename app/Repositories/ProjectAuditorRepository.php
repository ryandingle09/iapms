<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectAuditorRepository
 * @package namespace App\Repositories;
 */
interface ProjectAuditorRepository extends RepositoryInterface
{
    //
}
