<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\HolidayRepository;
use App\Models\Holiday;
use App\Validators\HolidayValidator;

/**
 * Class HolidayRepositoryEloquent
 * @package namespace App\Repositories;
 */
class HolidayRepositoryEloquent extends BaseRepository implements HolidayRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Holiday::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
