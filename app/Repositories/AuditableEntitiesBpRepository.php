<?php

namespace App\Repositories;

use App\Models\AeMbp;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditableEntitiesBpRepository
 * @package namespace App\Repositories;
 */
interface AuditableEntitiesBpRepository extends RepositoryInterface
{
    public function getBusinessProcessPercentile($entity_id, $bp_id, $value = null);

    public function getEntityBusinessProcess(Model $entity);

    public function getMainBusinessProcess(Model $entity);

    public function getActualBusinessProcesses($entity_id);

    public function createMainBusinessProcess(AeMbp $main_bp, $subs = array());
}
