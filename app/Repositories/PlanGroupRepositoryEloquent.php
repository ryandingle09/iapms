<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PlanGroupRepository;
use App\Models\PlanGroup;
use App\Validators\PlanGroupValidator;

/**
 * Class PlanGroupRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PlanGroupRepositoryEloquent extends BaseRepository implements PlanGroupRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PlanGroup::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
