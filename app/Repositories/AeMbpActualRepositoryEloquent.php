<?php

namespace App\Repositories;

use App\Models\AeMbpActual;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Validators\AeMbpActualValidator;

/**
 * Class AeMbpActualRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AeMbpActualRepositoryEloquent extends BaseRepository implements AeMbpActualRepository
{

    protected $fieldSearchable = [
        'main_bp_name'=>'like',
        'auditable_entity_id',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AeMbpActual::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getAverageCustomMeasures($conditions){
        $mbp_builder = $this->getAllMbpaByMaster( $conditions['master_ae_mbp_id'] );
        $all_mbpas = $mbp_builder->get();
        $selected_mbpa = $mbp_builder->where('ae_mbp_actual.auditable_entity_id',$conditions['auditable_entity_id'])->get()->first()->toArray();

        $query = $this->model;
        $arr = [];
        $ctr = 0;
        for ($i=1; $i < 11 ; $i++) { 

            $hs_type = $selected_mbpa['cm_'.$i.'_high_score_type'];

            if($hs_type == null || $hs_type == ""){
                break;
            }

            $group = $all_mbpas->pluck('cm_'.$i.'_value')->toArray();

            $percentile = percentile_rank( $group , $selected_mbpa['cm_'.$i.'_value'], $hs_type);

            array_push($arr,  round($percentile));
        }
        return  collect($arr)->avg();
    }

    public function getAllMbpaByMaster($master_mbp_id){
        return $this->model->join('ae_mbp','ae_mbp.ae_mbp_id','=','ae_mbp_actual.master_ae_mbp_id')
                           ->where('master_ae_mbp_id',$master_mbp_id)
                           ->addSelect(
                            // '*',
                            'ae_mbp_actual.auditable_entity_id as aea_id',
                            'cm_1_value',
                            'cm_1_high_score_type',
                            'cm_2_value',
                            'cm_2_high_score_type',
                            'cm_3_value',
                            'cm_3_high_score_type',
                            'cm_4_value',
                            'cm_4_high_score_type',
                            'cm_5_value',
                            'cm_5_high_score_type',
                            'cm_6_value',
                            'cm_6_high_score_type',
                            'cm_7_value',
                            'cm_7_high_score_type',
                            'cm_8_value',
                            'cm_8_high_score_type',
                            'cm_9_value',
                            'cm_9_high_score_type',
                            'cm_10_value',
                            'cm_10_high_score_type'
                           );
    }

    public function findBudgetedMandays($mbp_id, $audit_type){
        $query = $this->model
                    ->where('ae_mbp_id',$mbp_id)
                    ->addSelect(
                        'master_ae_mbp_id',
                        \DB::raw('( IFNULL(walk_thru_mandays,0) + IFNULL(planning_mandays,0) + IFNULL(reviewer_mandays,0) + IFNULL(followup_mandays,0) + IFNULL(discussion_mandays,0) + IFNULL(draft_dar_mandays,0) + IFNULL(final_report_mandays,0) + IFNULL(wrap_up_mandays,0) ) AS additional_budgeted_mandays')
                    )->first();

        $master_ae_mbp_id = $query ? $query->master_ae_mbp_id : 0;
        $additional_budgeted_mandays = $query ? $query->additional_budgeted_mandays : 0;

        $test_proc =  collect(\DB::select("
            SELECT 
            ctp.test_proc_id, 
            max(ctp.budgeted_mandays) budgeted_mandays 
            from ia_ae_mbp aembp, 
            ia_ae_mbp_sbp aembpsbp,
            ia_business_processes sbp, 
            ia_bp_objectives sbpo, 
            ia_business_processes_steps sbps,
            ia_bp_steps_risks sbpsr, 
            ia_risks r, 
            ia_risks_control rc, 
            ia_controls_master cm, 
            ia_controls_test_proc ctp, 
            ia_controls_test_audit_types ctat 
            WHERE aembp.ae_mbp_id = $master_ae_mbp_id 
            AND aembpsbp.ae_mbp_id = aembp.ae_mbp_id 
            AND sbp.bp_id = aembpsbp.bp_id 
            AND sbpo.bp_id = sbp.bp_id 
            AND sbps.bp_objective_id = sbpo.bp_objective_id 
            AND sbpsr.bp_step_id = sbps.bp_steps_id 
            AND r.risk_id = sbpsr.risk_id 
            AND rc.risk_id = r.risk_id 
            AND cm.control_id = rc.control_id 
            AND ctp.control_id = cm.control_id 
            AND ctat.control_test_id = ctp.control_test_id 
            AND ctat.audit_type = '$audit_type' 
            GROUP BY ctp.test_proc_id"))->sum('budgeted_mandays'); 
        return $additional_budgeted_mandays + $test_proc;
    }
}
