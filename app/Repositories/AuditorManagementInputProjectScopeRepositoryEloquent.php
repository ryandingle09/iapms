<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AuditorManagementInputProjectScopeRepository;
use App\Models\AuditorManagementInputProjectScope;
use App\Validators\AuditorManagementInputProjectScopeValidator;

/**
 * Class AuditorManagementInputProjectScopeRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditorManagementInputProjectScopeRepositoryEloquent extends BaseRepository implements AuditorManagementInputProjectScopeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AuditorManagementInputProjectScope::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
