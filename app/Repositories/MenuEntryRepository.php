<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MenuEntryRepository
 * @package namespace App\Repositories;
 */
interface MenuEntryRepository extends RepositoryInterface
{
    //
}
