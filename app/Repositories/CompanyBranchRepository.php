<?php

namespace App\Repositories;

use App\Models\AuditableEntity;
use App\Models\CompanyProfile;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CompanyBranchRepository
 * @package namespace App\Repositories;
 */
interface CompanyBranchRepository extends RepositoryInterface
{
    public function createDefaultBranch(CompanyProfile $company);

    public function updateStatus($company_id, $branch_code, $status);

    public function getMasterRelatedCompanyBranches(AuditableEntity $entity);
}
