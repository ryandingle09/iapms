<?php

namespace App\Repositories;

use App\Models\AuditableEntitiesActual;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EnterpriseRiskRepository
 * @package namespace App\Repositories;
 */
interface EnterpriseRiskRepository extends RepositoryInterface
{
    public function getAuditeeEntity(AuditableEntitiesActual $entity);
}
