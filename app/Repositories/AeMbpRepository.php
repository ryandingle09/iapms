<?php

namespace App\Repositories;

use App\Models\AeMbp;
use App\Models\AuditableEntity;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AeMbpRepository
 * @package namespace App\Repositories;
 */
interface AeMbpRepository extends RepositoryInterface
{
    public function applyToActualEntities(AeMbp $main_bp, $delete = false);

    public function getRelatedCompanies(AuditableEntity $entity);

    public function averageCustomMeasures($conditions);
}
