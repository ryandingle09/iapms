<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FunctionsRepository
 * @package namespace App\Repositories;
 */
interface FunctionsRepository extends RepositoryInterface
{
    //
}
