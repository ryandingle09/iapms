<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CompanyProfileRepository;
use App\Models\CompanyProfile;
use App\Validators\CompanyProfileValidator;

/**
 * Class CompanyProfileRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CompanyProfileRepositoryEloquent extends BaseRepository implements CompanyProfileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CompanyProfile::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
