<?php

namespace App\Repositories;

use App\Models\AuditableEntitiesActual;
use App\Models\AuditableEntity;
use App\Models\CompanyBranch;
use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditableEntitiesActualRepository
 * @package namespace App\Repositories;
 */
interface AuditableEntitiesActualRepository extends RepositoryInterface
{
    public function getCustomMeasurePercentile(AuditableEntitiesActual $entity, $values = array());

    public function makeBranchFromCompany(AuditableEntitiesActual $entity, $branches = array());

    public function getMasterEntities($business, $entity);

    public function createEntity($attributes = array());

    public function getMasterEntity($group, $business, $entity, $branch, $dept);

    public function applyMasterBp(AuditableEntitiesActual $entity);

    public function createActualFromMaster(AuditableEntity $master, Collection $companies);

    public function createCompanyEntity(AuditableEntity $master, CompanyBranch $company);

    public function createBranchEntity(AuditableEntity $master, CompanyBranch $company);

    public function checkExists(AuditableEntity $master, $entity_name);

    /**
     * Get company level entities
     * @param $branch_code
     * @return mixed
     */
    public function getBranchCompanies($branch_code);

    public function fullSearch();
}
