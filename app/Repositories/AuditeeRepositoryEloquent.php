<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AuditeeRepository;
use App\Models\Auditee;
use App\Validators\AuditeeValidator;

/**
 * Class AuditeeRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditeeRepositoryEloquent extends BaseRepository implements AuditeeRepository
{
    protected $fieldSearchable = [
        'first_name' => 'like',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Auditee::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
