<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RiskAssessmentScaleAnnualAsvRepository;
use App\Models\RiskAssessmentScaleAnnualAsv;
use App\Validators\RiskAssessmentScaleAnnualAsvValidator;

/**
 * Class RiskAssessmentScaleAnnualAsvRepositoryEloquent
 * @package namespace App\Repositories;
 */
class RiskAssessmentScaleAnnualAsvRepositoryEloquent extends BaseRepository implements RiskAssessmentScaleAnnualAsvRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RiskAssessmentScaleAnnualAsv::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
