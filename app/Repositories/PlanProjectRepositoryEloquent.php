<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PlanProjectRepository;
use App\Models\PlanProject;
use App\Validators\PlanProjectValidator;

/**
 * Class PlanProjectRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PlanProjectRepositoryEloquent extends BaseRepository implements PlanProjectRepository
{

    protected $fieldSearchable = [
        'audit_type'
    ];
    
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PlanProject::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Get the status count
     * @param $plan_id
     * @return mixed
     */
    public function getStatusCount($plan_id)
    {
        $plan_project = $this->model->select( DB::raw('COUNT(*) AS total_count') )
        ->addSelect( \DB::raw('COUNT(CASE WHEN plan_project_status = \''.PlanProject::STATUS_NEW.'\' THEN 1 END) AS new_project') )
        ->addSelect( \DB::raw('COUNT(CASE WHEN plan_project_status = \''.PlanProject::STATUS_REVIEW.'\' THEN 1 END) AS for_review') )
        ->addSelect( \DB::raw('COUNT(CASE WHEN plan_project_status = \''.PlanProject::STATUS_REVIEWED.'\' THEN 1 END) AS reviewed') )
        ->addSelect( \DB::raw('COUNT(CASE WHEN plan_project_status = \''.PlanProject::STATUS_APPROVAL.'\' THEN 1 END) AS for_approval') )
        ->addSelect( \DB::raw('COUNT(CASE WHEN plan_project_status = \''.PlanProject::STATUS_APPROVED.'\' THEN 1 END) AS approved') )
        ->addSelect( \DB::raw('COUNT(CASE WHEN plan_project_status = \''.PlanProject::STATUS_CLOSED.'\' THEN 1 END) AS closed') )
        ->where('plan_id', $plan_id)
        ->first();

        return $plan_project;
    }
}
