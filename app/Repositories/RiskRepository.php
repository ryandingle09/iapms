<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RiskRepository
 * @package namespace App\Repositories;
 */
interface RiskRepository extends RepositoryInterface
{
    //
}
