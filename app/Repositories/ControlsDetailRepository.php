<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ControlsDetailRepository
 * @package namespace App\Repositories;
 */
interface ControlsDetailRepository extends RepositoryInterface
{
    //
}
