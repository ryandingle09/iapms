<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface QuestionnaireRepository
 * @package namespace App\Repositories;
 */
interface QuestionnaireRepository extends RepositoryInterface
{
    //
}
