<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectScopeApgApprovalRepository;
use App\Models\ProjectScopeApgApproval;
use App\Validators\ProjectScopeApgApprovalValidator;

/**
 * Class ProjectScopeApgApprovalRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectScopeApgApprovalRepositoryEloquent extends BaseRepository implements ProjectScopeApgApprovalRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectScopeApgApproval::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
