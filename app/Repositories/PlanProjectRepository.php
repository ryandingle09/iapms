<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlanProjectRepository
 * @package namespace App\Repositories;
 */
interface PlanProjectRepository extends RepositoryInterface
{
    public function getStatusCount($plan_id);
}
