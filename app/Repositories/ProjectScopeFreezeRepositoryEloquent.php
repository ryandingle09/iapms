<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectScopeFreezeRepository;
use App\Models\ProjectScopeFreeze;
use App\Validators\ProjectScopeFreezeValidator;

/**
 * Class ProjectScopeFreezeRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectScopeFreezeRepositoryEloquent extends BaseRepository implements ProjectScopeFreezeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectScopeFreeze::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
