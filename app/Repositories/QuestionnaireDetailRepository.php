<?php

namespace App\Repositories;

use App\Models\Questionnaire;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface QuestionnaireDetailRepository
 * @package namespace App\Repositories;
 */
interface QuestionnaireDetailRepository extends RepositoryInterface
{
    public function attachToQuestionnaire(Questionnaire $questionnaire);
    public function detachToQuestionnaire(Questionnaire $questionnaire, $seq);
    public function updateAttachment(Questionnaire $questionnaire, $seq);
    public function setAttributes($seq = 0, $question = array());
}
