<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RiskRepository;
use App\Models\Risk;
use App\Validators\RiskValidator;

/**
 * Class RiskRepositoryEloquent
 * @package namespace App\Repositories;
 */
class RiskRepositoryEloquent extends BaseRepository implements RiskRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Risk::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
