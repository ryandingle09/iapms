<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ApprovalFromStatusRepository;
use App\Models\ApprovalFromStatus;
use App\Validators\ApprovalFromStatusValidator;

/**
 * Class ApprovalFromStatusRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ApprovalFromStatusRepositoryEloquent extends BaseRepository implements ApprovalFromStatusRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ApprovalFromStatus::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
