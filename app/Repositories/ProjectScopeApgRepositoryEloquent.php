<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectScopeApgRepository;
use App\Models\ProjectScopeApg;
use App\Validators\ProjectScopeApgValidator;
use App\Criteria\ApgFilterColumnByFieldCriteria;

/**
 * Class ProjectScopeApgRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectScopeApgRepositoryEloquent extends BaseRepository implements ProjectScopeApgRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectScopeApg::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria( app(ApgFilterColumnByFieldCriteria::class) );
    }

    /*
    * Copy from populate_apg() stored procedure 
    * Procedure to populate the project scope apg
    */

    public function populateApgs($scope_id)
    {
        $scope = \App\Models\ProjectScope::find($scope_id);
        $auditable_entity_id = $scope->auditable_entity_id;
        $ae_mbp_id = $scope->ae_mbp_id;
        $audit_type = "FAS Audit";
        $user_id = auth()->user()->user_id;
        $result = collect(\DB::select("SELECT 
                  $scope_id AS project_scope_id,
                  main.rn apg_seq,
                  -- main.auditable_entity_name,
                  main.main_bp_name,
                  main.mbp_bp_seq,
                  main.bp_name,
                  main.objective_name,
                  main.bp_steps_seq,
                  main.activity_narrative,
                  main.bp_step_risk_seq,
                  main.risk_code,
                  1 AS control_seq,
                  main.control_name,
                  main.control_test_seq,
                  main.test_proc_id,
                  main.test_proc_narrative,
                  -- main.test_proc_name,
                  main.audit_objective,
                  main.budgeted_mandays AS allotted_mandays,
                  NULL AS auditor_id,
                  NULL AS status,
                  'Y' AS enabled_flag,
                  $user_id AS created_by,
                  NOW() AS created_date,
                  $user_id AS last_update_by,
                  NOW() AS last_update_date
                    FROM
                      (SELECT 
                        s1.test_proc_name,
                        MIN(s1.s1rn) s2rn 
                      FROM
                        (SELECT 
                          aea.auditable_entity_name,
                          aembpa.main_bp_name,
                          aembpsbp.mbp_bp_seq,
                          sbp.bp_name,
                          sbpo.objective_name,
                          sbps.bp_steps_seq,
                          sbps.activity_narrative,
                          sbpsr.bp_step_risk_seq,
                          r.risk_code,
                          cm.control_name,
                          ctp.control_test_seq,
                          @rownum1 := IFNULL(@rownum1, 0) + 1 s1rn,
                          tp.test_proc_id,
                          tp.test_proc_narrative,
                          tp.audit_objective,
                          tp.test_proc_name,
                          ctp.budgeted_mandays 
                        FROM
                          ia_auditable_entities_actual aea,
                          ia_ae_mbp_actual aembpa,
                          ia_ae_mbp aembp,
                          ia_ae_mbp_sbp aembpsbp,
                          --
                          ia_business_processes sbp,
                          ia_bp_objectives sbpo,
                          ia_business_processes_steps sbps,
                          ia_bp_steps_risks sbpsr,
                          --
                          ia_risks r,
                          ia_risks_control rc,
                          --
                          ia_controls_master cm,
                          ia_controls_test_proc ctp,
                          ia_test_proc tp,
                          ia_controls_test_audit_types ctat,
                          (SELECT 
                            @rownum1 := 0) r 
                        WHERE aea.auditable_entity_id = $auditable_entity_id 
                          AND aembpa.auditable_entity_id = aea.auditable_entity_id 
                          AND aembpa.ae_mbp_id = $ae_mbp_id 
                          AND aembp.ae_mbp_id = aembpa.master_ae_mbp_id 
                          AND aembpsbp.ae_mbp_id = aembp.ae_mbp_id 
                          AND sbp.bp_id = aembpsbp.bp_id 
                          AND sbpo.bp_id = sbp.bp_id 
                          AND sbps.bp_objective_id = sbpo.bp_objective_id 
                          AND sbpsr.bp_step_id = sbps.bp_steps_id 
                          AND r.risk_id = sbpsr.risk_id 
                          AND rc.risk_id = r.risk_id 
                          AND cm.control_id = rc.control_id 
                          AND ctp.control_id = cm.control_id 
                          AND tp.test_proc_id = ctp.test_proc_id 
                          AND ctat.control_test_id = ctp.control_test_id 
                          AND ctat.audit_type = '$audit_type' 
                        ORDER BY 3,
                          6,
                          11) s1 
                      GROUP BY s1.test_proc_name 
                      ORDER BY s2rn) s2,
                      --
                      (SELECT 
                        aea.auditable_entity_name,
                        aembpa.main_bp_name,
                        aembpsbp.mbp_bp_seq,
                        sbp.bp_name,
                        sbpo.objective_name,
                        sbps.bp_steps_seq,
                        sbps.activity_narrative,
                        sbpsr.bp_step_risk_seq,
                        r.risk_code,
                        cm.control_name,
                        ctp.control_test_seq,
                        @rownum2 := IFNULL(@rownum2, 0) + 1 rn,
                        tp.test_proc_id,
                        tp.test_proc_narrative,
                        tp.audit_objective,
                        tp.test_proc_name,
                        ctp.budgeted_mandays 
                      FROM
                        ia_auditable_entities_actual aea,
                        ia_ae_mbp_actual aembpa,
                        ia_ae_mbp aembp,
                        ia_ae_mbp_sbp aembpsbp,
                        --
                        ia_business_processes sbp,
                        ia_bp_objectives sbpo,
                        ia_business_processes_steps sbps,
                        ia_bp_steps_risks sbpsr,
                        --
                        ia_risks r,
                        ia_risks_control rc,
                        --
                        ia_controls_master cm,
                        ia_controls_test_proc ctp,
                        ia_test_proc tp,
                        ia_controls_test_audit_types ctat,
                        (SELECT 
                          @rownum2 := 0) r 
                      WHERE aea.auditable_entity_id = $auditable_entity_id 
                        AND aembpa.auditable_entity_id = aea.auditable_entity_id 
                        AND aembpa.ae_mbp_id = $ae_mbp_id 
                        AND aembp.ae_mbp_id = aembpa.master_ae_mbp_id 
                        AND aembpsbp.ae_mbp_id = aembp.ae_mbp_id 
                        AND sbp.bp_id = aembpsbp.bp_id 
                        AND sbpo.bp_id = sbp.bp_id 
                        AND sbps.bp_objective_id = sbpo.bp_objective_id 
                        AND sbpsr.bp_step_id = sbps.bp_steps_id 
                        AND r.risk_id = sbpsr.risk_id 
                        AND rc.risk_id = r.risk_id 
                        AND cm.control_id = rc.control_id 
                        AND ctp.control_id = cm.control_id 
                        AND tp.test_proc_id = ctp.test_proc_id 
                        AND ctat.control_test_id = ctp.control_test_id 
                        AND ctat.audit_type = '$audit_type' 
                      ORDER BY 3,
                        6,
                        11) main 
                    WHERE main.rn = s2.s2rn 
                    ORDER BY 3,
                      6,
                      11"
        ));
        $bulk_data = $result->map(function($item){
                            return (array) $item;
                        })->toArray() ;
        $this->model->where('project_scope_id',$scope_id)->delete();
        $this->model->insert($bulk_data);
        return count($bulk_data) ;
    }

    public function pullQueriedApgs($scope_id, $group_by, $keyword,$flag = null )
    { 
        $query = $this->model->where( 'project_scope_id', $scope_id );
        if($flag != null){
          $query = $query->where('enabled_flag', $flag);
        }
        switch ( $group_by ) {
            case 'bp_name':
                return $query->groupBy('bp_name')
                                ->addSelect(
                                    'project_scope_apg_id',
                                    \DB::raw('bp_name AS title'),
                                    \DB::raw('"bp_name" AS field'),
                                    \DB::raw('"objective_name" AS next_field'),
                                    'enabled_flag',
                                    \DB::raw('SUM(CASE WHEN enabled_flag = "Y" THEN 1 ELSE 0 END) AS ycount')
                                )
                                ->get();
                break;
            case 'objective_name':
                return $query->where('bp_name', $keyword )
                                ->groupBy('objective_name')
                                ->addSelect(
                                    'project_scope_apg_id',
                                    \DB::raw('objective_name AS title'),
                                    \DB::raw('"objective_name" AS field'),
                                    \DB::raw('"activity_narrative" AS next_field'),
                                    'enabled_flag',
                                    \DB::raw('SUM(CASE WHEN enabled_flag = "Y" THEN 1 ELSE 0 END) AS ycount')
                                )
                                ->get();
                break;
            case 'activity_narrative':
                return $query->where('objective_name', $keyword )
                                ->groupBy('activity_narrative')
                                ->addSelect(
                                    'project_scope_apg_id',
                                    \DB::raw('activity_narrative AS title'),
                                    \DB::raw('"activity_narrative" AS field'),
                                    \DB::raw('"risk_code" AS next_field'),
                                    'enabled_flag',
                                    \DB::raw('SUM(CASE WHEN enabled_flag = "Y" THEN 1 ELSE 0 END) AS ycount')
                                )
                                ->get();
                break;
            case 'risk_code':
                return $query->where('activity_narrative', $keyword )
                                ->groupBy('risk_code')
                                ->addSelect(
                                    'project_scope_apg_id',
                                    \DB::raw('risk_code AS title'),
                                    \DB::raw('"risk_code" AS field'),
                                    \DB::raw('"control_name" AS next_field'),
                                    'enabled_flag',
                                    \DB::raw('SUM(CASE WHEN enabled_flag = "Y" THEN 1 ELSE 0 END) AS ycount')
                                )
                                ->get();
                break;
            case 'control_name':
                return $query->where('risk_code', $keyword )
                                ->groupBy('control_name')
                                ->addSelect(
                                    'project_scope_apg_id',
                                    \DB::raw('control_name AS title'),
                                    \DB::raw('"control_name" AS field'),
                                    \DB::raw('"test_proc_table" AS next_field'),
                                    'enabled_flag',
                                    \DB::raw('SUM(CASE WHEN enabled_flag = "Y" THEN 1 ELSE 0 END) AS ycount')
                                )
                                ->get();
                break;
            case 'test_proc_table' :
                return $query->leftJoin('auditors','auditors.auditor_id','=','project_scope_apg.auditor_id')
                                ->where('control_name', $keyword )
                                ->addSelect(
                                    'project_scope_apg_id',
                                    'test_proc_id',
                                    'test_proc_narrative',
                                    'control_test_seq',
                                    'audit_objective',
                                    'allotted_mandays',
                                    'project_scope_apg.auditor_id',
                                    'enabled_flag',
                                    \DB::raw('"1" AS ycount'),
                                    \DB::raw('CONCAT(first_name," ",last_name) AS auditor_full_name')
                                )
                                ->get();
                break;
            default:
                return [];
                break;
        }        
    }

    public function recursiveDestroy($scope_id, $request)
    {
        $find = $this->model->where( 'project_scope_apg_id', $request->id )->first();  
        switch ($request->field) {
            case 'bp_name':
                return $this->model->where('bp_name', $find->bp_name)
                                    ->delete();
                break;
            case 'objective_name':
                return $this->model->where('bp_name', $find->bp_name)
                                    ->where('objective_name', $find->objective_name)
                                    ->delete();
                break;
            case 'activity_narrative':
                return $this->model->where('bp_name', $find->bp_name)
                                    ->where('objective_name', $find->objective_name)
                                    ->where('activity_narrative', $find->activity_narrative)
                                    ->delete();
                break;
            case 'risk_code':
                return $this->model->where('bp_name', $find->bp_name)
                                    ->where('objective_name', $find->objective_name)
                                    ->where('activity_narrative', $find->activity_narrative)
                                    ->where('risk_code', $find->risk_code)
                                    ->delete();
                break;
            case 'control_name':
                return $this->model->where('bp_name', $find->bp_name)
                                    ->where('objective_name', $find->objective_name)
                                    ->where('activity_narrative', $find->activity_narrative)
                                    ->where('risk_code', $find->risk_code)
                                    ->where('control_name', $find->control_name)
                                    ->delete();
                break;
            case 'test_proc_table':
                return null;
                break;
            default:
                return null;
                break;
        }
    }

    public function recursiveFlag($id, $request)
    {
      $find = $this->model->where( 'project_scope_apg_id', $id )->first();
      switch ($request->field) {
        case 'bp_name':
            return $this->model->where('bp_name', $find->bp_name)
                                ->update([
                                  'enabled_flag' => $request->status
                                ]);
            break;
        case 'objective_name':
            return $this->model->where('bp_name', $find->bp_name)
                                ->where('objective_name', $find->objective_name)
                                ->update([
                                  'enabled_flag' => $request->status
                                ]);
            break;
        case 'activity_narrative':
            return $this->model->where('bp_name', $find->bp_name)
                                ->where('objective_name', $find->objective_name)
                                ->where('activity_narrative', $find->activity_narrative)
                                ->update([
                                  'enabled_flag' => $request->status
                                ]);
            break;
        case 'risk_code':
            return $this->model->where('bp_name', $find->bp_name)
                                ->where('objective_name', $find->objective_name)
                                ->where('activity_narrative', $find->activity_narrative)
                                ->where('risk_code', $find->risk_code)
                                ->update([
                                  'enabled_flag' => $request->status
                                ]);
            break;
        case 'control_name':
            return $this->model->where('bp_name', $find->bp_name)
                                ->where('objective_name', $find->objective_name)
                                ->where('activity_narrative', $find->activity_narrative)
                                ->where('risk_code', $find->risk_code)
                                ->where('control_name', $find->control_name)
                                ->update([
                                  'enabled_flag' => $request->status
                                ]);
            break;
        default:
            return $this->model->where('project_scope_apg_id', $id)->update([
                                  'enabled_flag' => $request->status
                                ]);
            break;
      }
    }

    // public function fullTreeApg($scope_id ,$flag){
    //   $data = $this->recursiveQuery($scope_id, 'bp_name', "" , "Y" );
    //   return $data;
    // }

    public function recursiveQuery($scope_id, $group_by, $keyword, $flag){
      $data = $this->pullQueriedApgs($scope_id, $group_by ,$keyword, $flag)
                ->map(function($item) use  ($scope_id ,$flag ) {
                    $temp = (object) [
                        "project_scope_apg_id" => $item->project_scope_apg_id,
                        "title" => $item->title,
                        "field" => $item->field,
                        "next_field" => $item->next_field,
                        "enabled_flag" => $flag,
                        "ycount" => $item->ycount
                    ];
                    if( $item->next_field != "test_proc_table"){
                        $temp->children = $this->recursiveQuery($scope_id, $item->next_field, $item->title , $flag);
                        return $temp;
                    }else{
                        $temp->children = $this->pullQueriedApgs($scope_id, $item->next_field , $item->title  , $flag);
                        return $temp;
                    }
                });

      return $data;
    }

    public function recursiveUpdate( $apg_id , $field, $value )
    {
        $find = $this->model->where( 'project_scope_apg_id', $apg_id )->first();
        switch ($field) {
          case 'bp_name':
              return $this->model->where('bp_name', $find->bp_name )
                                  ->update([
                                    'bp_name' => $value
                                  ]);
              break;
          case 'objective_name':
              return $this->model->where('bp_name', $find->bp_name)
                                  ->where('objective_name', $find->objective_name)
                                  ->update([
                                    'objective_name' => $value
                                  ]);
              break;
          case 'activity_narrative':
              return $this->model->where('bp_name', $find->bp_name)
                                  ->where('objective_name', $find->objective_name)
                                  ->where('activity_narrative', $find->activity_narrative)
                                  ->update([
                                    'activity_narrative' => $value
                                  ]);
              break;
          case 'risk_code':
              return $this->model->where('bp_name', $find->bp_name)
                                  ->where('objective_name', $find->objective_name)
                                  ->where('activity_narrative', $find->activity_narrative)
                                  ->where('risk_code', $find->risk_code)
                                  ->update([
                                    'risk_code' => $value
                                  ]);
              break;
          case 'control_name':
              return $this->model->where('bp_name', $find->bp_name)
                                  ->where('objective_name', $find->objective_name)
                                  ->where('activity_narrative', $find->activity_narrative)
                                  ->where('risk_code', $find->risk_code)
                                  ->where('control_name', $find->control_name)
                                  ->update([
                                    'control_name' => $value
                                  ]);
              break;
          default:
              return 101;
              break;
        }
    }
}
