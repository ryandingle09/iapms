<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlanProjectScopeAuditorRepository
 * @package namespace App\Repositories;
 */
interface PlanProjectScopeAuditorRepository extends RepositoryInterface
{
    //
}
