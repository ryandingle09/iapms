<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\QuestionnaireRepository;
use App\Models\Questionnaire;
use App\Validators\QuestionnaireValidator;

/**
 * Class QuestionnaireRepositoryEloquent
 * @package namespace App\Repositories;
 */
class QuestionnaireRepositoryEloquent extends BaseRepository implements QuestionnaireRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Questionnaire::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
