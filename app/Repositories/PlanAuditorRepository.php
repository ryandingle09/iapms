<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlanAuditorRepository
 * @package namespace App\Repositories;
 */
interface PlanAuditorRepository extends RepositoryInterface
{
    //
}
