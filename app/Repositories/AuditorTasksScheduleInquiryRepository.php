<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditorTasksScheduleInquiryRepository
 * @package namespace App\Repositories;
 */
interface AuditorTasksScheduleInquiryRepository extends RepositoryInterface
{
    //
}
