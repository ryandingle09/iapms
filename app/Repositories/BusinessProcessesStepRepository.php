<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BusinessProcessesStepRepository
 * @package namespace App\Repositories;
 */
interface BusinessProcessesStepRepository extends RepositoryInterface
{
    //
}
