<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AuditorGradingMatrixRepository;
use App\Models\AuditorGradingMatrix;
use App\Validators\AuditorGradingMatrixValidator;

/**
 * Class AuditorGradingMatrixRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditorGradingMatrixRepositoryEloquent extends BaseRepository implements AuditorGradingMatrixRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AuditorGradingMatrix::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
