<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MatrixRepository
 * @package namespace App\Repositories;
 */
interface MatrixRepository extends RepositoryInterface
{
    //
}
