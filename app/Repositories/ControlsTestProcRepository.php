<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ControlsTestProcRepository
 * @package namespace App\Repositories;
 */
interface ControlsTestProcRepository extends RepositoryInterface
{
    //
}
