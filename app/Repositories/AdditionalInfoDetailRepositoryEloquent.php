<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AdditionalInfoDetailRepository;
use App\Models\AdditionalInfoDetail;
use App\Validators\AdditionalInfoDetailValidator;

/**
 * Class AdditionalInfoDetailRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AdditionalInfoDetailRepositoryEloquent extends BaseRepository implements AdditionalInfoDetailRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AdditionalInfoDetail::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
