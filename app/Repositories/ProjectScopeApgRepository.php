<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectScopeApgRepository
 * @package namespace App\Repositories;
 */
interface ProjectScopeApgRepository extends RepositoryInterface
{
    //
}
