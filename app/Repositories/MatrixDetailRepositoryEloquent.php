<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MatrixDetailRepository;
use App\Models\MatrixDetail;
use App\Validators\MatrixDetailValidator;

/**
 * Class MatrixDetailRepositoryEloquent
 * @package namespace App\Repositories;
 */
class MatrixDetailRepositoryEloquent extends BaseRepository implements MatrixDetailRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MatrixDetail::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
