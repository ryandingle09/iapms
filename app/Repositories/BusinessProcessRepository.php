<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BusinessProcessRepository
 * @package namespace App\Repositories;
 */
interface BusinessProcessRepository extends RepositoryInterface
{
	/**
	 * Get all the risk associated in the business process
	 * @param  integer $bp_id Business process Id
	 * @return Collection
	 */
    public function getBpRisks($bp_id);
}
