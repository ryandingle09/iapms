<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MenuEntryRepository;
use App\Models\MenuEntry;
use App\Validators\MenuEntryValidator;

/**
 * Class MenuEntryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class MenuEntryRepositoryEloquent extends BaseRepository implements MenuEntryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MenuEntry::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
