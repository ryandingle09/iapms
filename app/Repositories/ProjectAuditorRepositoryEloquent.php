<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectAuditorRepository;
use App\Models\ProjectAuditor;
use App\Validators\ProjectAuditorValidator;

/**
 * Class ProjectAuditorRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectAuditorRepositoryEloquent extends BaseRepository implements ProjectAuditorRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectAuditor::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
