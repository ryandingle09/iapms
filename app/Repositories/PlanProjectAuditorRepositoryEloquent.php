<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PlanProjectAuditorRepository;
use App\Models\PlanProjectAuditor;
use App\Validators\PlanProjectAuditorValidator;

/**
 * Class PlanProjectAuditorRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PlanProjectAuditorRepositoryEloquent extends BaseRepository implements PlanProjectAuditorRepository
{
    protected $fieldSearchable = [
        'auditor.first_name' => "like",
        'auditor.last_name' => "like"
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PlanProjectAuditor::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
