<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TestProcRepository
 * @package namespace App\Repositories;
 */
interface TestProcRepository extends RepositoryInterface
{
    //
}
