<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ApprovalSetupDetailRepository;
use App\Models\ApprovalSetupDetail;
use App\Validators\ApprovalSetupDetailValidator;

/**
 * Class ApprovalSetupDetailRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ApprovalSetupDetailRepositoryEloquent extends BaseRepository implements ApprovalSetupDetailRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ApprovalSetupDetail::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
