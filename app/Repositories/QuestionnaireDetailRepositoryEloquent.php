<?php

namespace App\Repositories;

use App\Models\Questionnaire;
use App\Models\QuestionnaireDetail;
use App\Repositories\QuestionnaireDetailRepository;
use App\Validators\QuestionnaireDetailValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class QuestionnaireDetailRepositoryEloquent
 * @package namespace App\Repositories;
 */
class QuestionnaireDetailRepositoryEloquent extends BaseRepository implements QuestionnaireDetailRepository
{
    public $attributes = [];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return QuestionnaireDetail::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function attachToQuestionnaire(Questionnaire $questionnaire){
        return $questionnaire->details()
                             ->create($this->attributes);
    }

    public function detachToQuestionnaire(Questionnaire $questionnaire, $seq) {
        return $questionnaire->details()
                             ->where('question_sequence', $seq)
                             ->delete();
    }

    public function updateAttachment(Questionnaire $questionnaire, $seq){
        return $questionnaire->details()
                             ->where('question_sequence', $seq)
                             ->update($this->attributes);
    }

    public function setAttributes($seq = 0, $question = array())
    {
        if( !count($question) ) return false;

        // set details attributes
        $this->attributes = [
            'question_sequence' => $seq,
            'question'          => $question['question'],
            'response_required' => $question['required'],
            'response_required' => $question['required'],
            'response_type'     => $question['response'],
            'page_break_after'  => $question['page_break_after'],
            'optional_remarks'  => $question['optional_remarks'],
            'data_type'  => $question['datatype'],
        ];

        // check response type and assign to respective field
        $response_type = strtolower($question['response']);

        if( $response_type == 'choices' ) {
            $this->attributes['choices_name'] = $question['choices'];
        }
        elseif( $response_type == 'range' ) {
            $this->attributes['range_from'] = $question['range']['from'];
            $this->attributes['range_to'] = $question['range']['to'];
        }

        return $this;
    }
}
