<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AuditorRepository;
use App\Models\Auditor;
use App\Validators\AuditorValidator;

/**
 * Class AuditorRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditorRepositoryEloquent extends BaseRepository implements AuditorRepository
{
    protected $fieldSearchable = [
        'first_name' => 'like',
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Auditor::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
