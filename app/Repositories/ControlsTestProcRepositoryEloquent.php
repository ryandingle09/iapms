<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ControlsTestProcRepository;
use App\Models\ControlsTestProc;
use App\Validators\ControlsTestProcValidator;

/**
 * Class ControlsTestProcRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ControlsTestProcRepositoryEloquent extends BaseRepository implements ControlsTestProcRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ControlsTestProc::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
