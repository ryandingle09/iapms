<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectScopeIomRepository;
use App\Models\ProjectScopeIom;
use App\Validators\ProjectScopeIomValidator;

/**
 * Class ProjectScopeIomRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectScopeIomRepositoryEloquent extends BaseRepository implements ProjectScopeIomRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectScopeIom::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
