<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RisksControlRepository;
use App\Models\RisksControl;
use App\Validators\RisksControlValidator;

/**
 * Class RisksControlRepositoryEloquent
 * @package namespace App\Repositories;
 */
class RisksControlRepositoryEloquent extends BaseRepository implements RisksControlRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return RisksControl::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
