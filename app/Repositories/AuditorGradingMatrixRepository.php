<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditorGradingMatrixRepository
 * @package namespace App\Repositories;
 */
interface AuditorGradingMatrixRepository extends RepositoryInterface
{
    //
}
