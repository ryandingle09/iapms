<?php

namespace App\Repositories;

use App\Criteria\LookupExcludeCriteria;
use App\Criteria\RequestLimitCriteria;
use App\Models\LookupType;
use App\Models\LookupValue;
use App\Repositories\LookupValueRepository;
use App\Validators\LookupValueValidator;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Traits\CacheableRepository;

/**
 * Class LookupValueRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LookupValueRepositoryEloquent extends BaseRepository implements LookupValueRepository
{
    // use CacheableRepository;

    protected $fieldSearchable = [
        'lookup_code' => 'like',
    ];

    protected $cacheMinutes = 90;
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LookupValue::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(app(LookupExcludeCriteria::class));
        $this->pushCriteria(app(RequestLimitCriteria::class));
    }
}
