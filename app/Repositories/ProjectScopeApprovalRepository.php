<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectScopeApprovalRepository
 * @package namespace App\Repositories;
 */
interface ProjectScopeApprovalRepository extends RepositoryInterface
{
    //
}
