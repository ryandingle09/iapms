<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditeeRepository
 * @package namespace App\Repositories;
 */
interface AuditeeRepository extends RepositoryInterface
{
    //
}
