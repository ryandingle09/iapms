<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectScopeDarRepository;
use App\Models\ProjectScopeDar;
use App\Validators\ProjectScopeDarValidator;

/**
 * Class ProjectScopeDarRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectScopeDarRepositoryEloquent extends BaseRepository implements ProjectScopeDarRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectScopeDar::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
