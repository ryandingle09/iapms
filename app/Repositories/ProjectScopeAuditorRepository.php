<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectScopeAuditorRepository
 * @package namespace App\Repositories;
 */
interface ProjectScopeAuditorRepository extends RepositoryInterface
{
    //
}
