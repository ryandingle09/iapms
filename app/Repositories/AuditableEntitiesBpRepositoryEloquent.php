<?php

namespace App\Repositories;

use App\Criteria\JoinSubBpToBusinessProcessCriteria;
use App\Models\AeMbp;
use App\Models\AeMbpActual;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\AuditableEntitiesBp;
use App\Validators\AuditableEntitiesBpValidator;

/**
 * Class AuditableEntitiesBpRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditableEntitiesBpRepositoryEloquent extends BaseRepository implements AuditableEntitiesBpRepository
{
    private $custom_measures;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AuditableEntitiesBp::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getBusinessProcessPercentile($entity_id, $bp_id, $value = null)
    {
        $ae = $this->model
                   ->join('business_processes', 'business_processes.bp_id', '=', 'auditable_entities_bp.bp_id')
                   ->where('auditable_entity_id', $entity_id);
                   // ->where('bp_id', $bp_id);

        $cm_range = $this->custom_measures;
        for( $i=0; $i < count($cm_range); $i++ ) {
            if(isset($cm_range[$i]['index'])) {
                $num = $cm_range[$i]['index'];
                $field = 'cm_'.$num.'_value';
                $value = $cm_range[$i]['value'];
            }
            else {
                $num = $cm_range[$i];
                $value = $field = 'cm_'.$cm_range[$i].'_value';
            }

            $score_type = 'cm_'.$num.'_high_score_type';

            // 100 * ( MIN(custom_measure) - 1 ) / count_of_custom_measures
            // TRUNCATE(100 * ((select count(low.'.$field.') from ia_auditable_entities_bp as low where low.bp_id = 6 and low.'.$field.' < ia_auditable_entities_bp.'.$field.') + ((select count(equal.'.$field.') from ia_auditable_entities_bp as equal where equal.bp_id = 6 and equal.'.$field.' = ia_auditable_entities_bp.'.$field.')*.5) ) / COUNT(IF(ia_auditable_entities_bp.'.$field.' IS NULL, 1, 1)), 1)
            $ae->addSelect(\DB::raw('TRUNCATE(100 * ((select count(low.'.$field.') from ia_auditable_entities_bp as low where low.bp_id = '.$bp_id.' and low.'.$field.' < ia_auditable_entities_bp.'.$field.') + ((select count(equal.'.$field.') from ia_auditable_entities_bp as equal where equal.bp_id = '.$bp_id.' and equal.'.$field.' = ia_auditable_entities_bp.'.$field.')*.5) ) / COUNT(IF(ia_auditable_entities_bp.'.$field.' IS NULL, 1, 1)), 1) as cm_'.$num.'_percentile'));
        }

        return $ae->first();
    }

    public function getEntityBusinessProcess(Model $entity)
    {
        return $entity->businessProcess()
                      ->select('business_processes.*')
                      ->lookupDetails(config('iapms.lookups.business_process'), 'lv_bp_code', 'business_processes.bp_code')
                      ->lookupDetails(config('iapms.lookups.source_type'), 'lv_source_type', 'business_processes.source_type')
                      ->get();
    }

    /**
     * Get master main business process and sub process
     * @param Model $entity
     * @return mixed
     */
    public function getMainBusinessProcess(Model $entity)
    {
        $master_bp = $entity->masterBp;
        if( !$master_bp->count() ) return $master_bp;

        return $entity->masterBp()->with(['subBpDetails'])
        ->select('ae_mbp.*')
        ->lookupDetails(config('iapms.lookups.main_bp'), 'lv_main_bp_name', 'ae_mbp.main_bp_name', true);
    }

    /**
     * Get actual main business process and sub process
     * @param $entity_id
     * @return mixed
     */
    public function getActualBusinessProcesses($entity_id)
    {
        $actual = AeMbpActual::where('auditable_entity_id', $entity_id)->get();
//        $actual = AeMbpActual::with('subBpDetails')->where('auditable_entity_id', $entity_id)->get();
        $bp = collect([]);
        if( !is_null($actual) ) {
            foreach ($actual as $item)
                $bp->push($this->getMainBusinessProcess($item)->first());
        }

        return $bp;
    }

    public function attachBusinessProcesses()
    {

    }

    public function getSubBusinessProcesses($mbp_id)
    {
        $sub_bp = app( AeMbpSbpRepository::class );
        $sub_bp->pushCriteria(new JoinSubBpToBusinessProcessCriteria());
        return $sub_bp->findByField('ae_mbp_id', $mbp_id)->all();
    }

    /**
     * @param AeMbp $main_bp
     * @param array $attributes
     * @return Model
     */
    public function createMainBusinessProcess(AeMbp $main_bp, $attributes = array())
    {
        return $main_bp->subBp()->create($attributes);
    }
}
