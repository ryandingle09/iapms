<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EnterpriseRiskDetailRepository;
use App\Models\EnterpriseRiskDetail;
use App\Validators\EnterpriseRiskDetailValidator;

/**
 * Class EnterpriseRiskDetailRepositoryEloquent
 * @package namespace App\Repositories;
 */
class EnterpriseRiskDetailRepositoryEloquent extends BaseRepository implements EnterpriseRiskDetailRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EnterpriseRiskDetail::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getAll()
    {
        $this->applyCriteria();
        $ent_risk = $this->model->with(['updater.auditee' => function($a){
            $a->select('auditees.*')
                ->lookupDetails(config('iapms.lookups.auditee.position'), 'lv_position_code', 'position_code');
            $a->with(['auditableEntity' => function($j){
                $j->select('auditable_entities_actual.*')
                    ->lookupDetails(config('iapms.lookups.company'), 'lv_company_code', 'company_code')
                    ->lookupDetails(config('iapms.lookups.dept_code'), 'lv_department_code', 'department_code')
                    ->lookupDetails(config('iapms.lookups.branch.code'), 'lv_branch_code', 'branch_code');
            }]);
        }])
        ->groupBy('enterprise_risk_details.risk_name');

        return $ent_risk->get();
    }
}
