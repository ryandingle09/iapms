<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LookupTypeRepository
 * @package namespace App\Repositories;
 */
interface LookupTypeRepository extends RepositoryInterface
{

}