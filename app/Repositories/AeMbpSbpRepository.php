<?php

namespace App\Repositories;

use App\Models\AeMbpSbp;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AeMbpSbpRepository
 * @package namespace App\Repositories;
 */
interface AeMbpSbpRepository extends RepositoryInterface
{
    public function getBpRisks(AeMbpSbp $sub_bp);

    public function generateRiskAssessmentScale(Model $master_bps, $type = 'master');
}
