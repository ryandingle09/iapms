<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlanGroupRepository
 * @package namespace App\Repositories;
 */
interface PlanGroupRepository extends RepositoryInterface
{
    //
}
