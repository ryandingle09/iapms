<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlanProjectScopeRepository
 * @package namespace App\Repositories;
 */
interface PlanProjectScopeRepository extends RepositoryInterface
{
    //
}
