<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PlanProjectScopeAuditorRepository;
use App\Models\PlanProjectScopeAuditor;
use App\Validators\PlanProjectScopeAuditorValidator;

/**
 * Class PlanProjectScopeAuditorRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PlanProjectScopeAuditorRepositoryEloquent extends BaseRepository implements PlanProjectScopeAuditorRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PlanProjectScopeAuditor::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
