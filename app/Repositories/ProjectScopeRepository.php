<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectScopeRepository
 * @package namespace App\Repositories;
 */
interface ProjectScopeRepository extends RepositoryInterface
{
    //
}
