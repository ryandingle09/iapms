<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\BpStepsRiskRepository;
use App\Models\BpStepsRisk;
use App\Validators\BpStepsRiskValidator;

/**
 * Class BpStepsRiskRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BpStepsRiskRepositoryEloquent extends BaseRepository implements BpStepsRiskRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BpStepsRisk::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
