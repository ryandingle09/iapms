<?php

namespace App\Repositories;

use App\Criteria\AuditableEntitySearchCriteria;
use App\Criteria\JoinRiskCriteria;
use App\Criteria\RiskInquiryCriteria;
use App\Facades\Iapms;
use App\Models\AeMbp;
use App\Models\AeMbpActual;
use App\Models\AeMbpSbp;
use App\Models\AuditableEntitiesActual;
use App\Models\AuditableEntity;
use App\Models\BpObjective;
use App\Models\BpStepsRisk;
use App\Models\BusinessProcess;
use App\Models\BusinessProcessesStep;
use App\Models\ControlsMaster;
use App\Models\Risk;
use App\Transformers\AuditableEntityInquiryTransformer;
use App\Validators\AuditableEntityValidator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection as FractalCollection;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class AuditableEntityRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditableEntityRepositoryEloquent extends BaseRepository implements AuditableEntityRepository
{
    protected $fieldSearchable = [
        'auditable_entity_name',
    ];

    // Inquiry attributes
    private $id_field;
    private $text;
    private $inquiry_type;
    private $lazy;
    private $inquiry_details;
    private $meta;
    private $request;
    private $filter;
    private $icon;
    private $company_code;
    private $branch_code;
    private $dept_code;
    private $starting_point;
    private $next_inquiry;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AuditableEntity::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(AuditableEntitySearchCriteria::class));
        $this->pushCriteria(app(RequestCriteria::class));

        $this->request = \Request::all();
        $this->filter = \Request::get('filter');
        $this->company_code = isset($this->filter['company']) ? $this->filter['company'] : '';
        $this->branch_code = isset($this->filter['branch']) ? $this->filter['branch'] : '';
        $this->dept_code = isset($this->filter['department']) ? $this->filter['department'] : '';
    }

    /**
     * Process inquiry requests
     * @return array
     */
    public function processInquiry()
    {
        $resp = collect(['data' => []]);
        $method = isset($this->request['_type']) ? 'get'.ucfirst($this->request['_type']).'Inquiry' : 'getEntityInquiry';

        if( method_exists($this, $method) )
        {
            $data = $this->{$method}();
            $this->getNextInquiry();
            if( !is_null($data) ) {
                $transform = 'transformCollection';
                if (!$data instanceof Collection) $transform = 'transformCollectionArray';

                $resp = Iapms::{$transform}($data, new AuditableEntityInquiryTransformer($this->id_field, $this->text, $this->inquiry_type, $this->lazy, $this->inquiry_details, $this->icon, $this->meta));
            }
        }

        return $resp;
    }

    public function getActualEntities($company_code = '', $master = 'company')
    {
        $starting = $this->request['subtreeview']; #TODO: Add set treeview method to set treeview attribute. Remove all duplicate implementation
        $entity_point = ['all', 'branch', 'dept', 'entity']; // duplicate array on line 549
        if( in_array($starting, $entity_point) ) {
            if( isset($this->request['id']) ) {
                $entity = AuditableEntitiesActual::find($this->request['id']);
            }

            $company_code = $this->company_code != '' ? $this->company_code : $company_code;
        }

        $entities = AuditableEntitiesActual::select('auditable_entities_actual.*')->filter($company_code, $this->branch_code, $this->dept_code);

        if( $master != '' && $master != 'branch' )
            $entities = $entities->isParentCompany('Y');
        else
            $entities = $entities->noDefault(); // for departments

        $entities = $entities->companyDetails()
        ->branchDetails()
        ->lookupDetails(config('iapms.lookups.branch.code'), 'lv_branch_code', 'auditable_entities_actual.branch_code')
        ->groupByCompany()
        ->orderBy('auditable_entity_name');

        #TODO: Optimize this, create new method
        if(isset($this->filter['root_name'])) $this->setMeta(['root_name' => $this->filter['root_name']]);
        if($starting == 'mainbp') {
            $mainbp = AeMbpActual::where('master_ae_mbp_id', $this->request['id'])->first();
            $entities->join('ae_mbp_actual', 'ae_mbp_actual.auditable_entity_id', '=', 'auditable_entities_actual.auditable_entity_id')
            ->where('main_bp_name', $mainbp->main_bp_name);
            $this->setMeta(['root_id' => $this->request['id']]);
        }
        if($starting == 'subbp') {
            $this->setMeta(['root_id' => $this->request['id']]);
            $entities->join('ae_mbp_actual', 'ae_mbp_actual.auditable_entity_id', '=', 'auditable_entities_actual.auditable_entity_id')
            ->join('ae_mbp_sbp', 'ae_mbp_sbp.ae_mbp_id', '=', 'ae_mbp_actual.master_ae_mbp_id')
            ->where('ae_mbp_sbp.bp_id', $this->request['id']);
        }
        if($starting == 'objective') {
            $this->setMeta(['root_id' => $this->request['id']]);
            $entities->join('ae_mbp_actual', 'ae_mbp_actual.auditable_entity_id', '=', 'auditable_entities_actual.auditable_entity_id')
            ->join('ae_mbp_sbp', 'ae_mbp_sbp.ae_mbp_id', '=', 'ae_mbp_actual.master_ae_mbp_id')
            ->join('bp_objectives', 'bp_objectives.bp_id', '=', 'ae_mbp_sbp.bp_id')
            ->where('bp_objectives.bp_objective_id', $this->request['id']);
        }
        if($starting == 'risk') {
            $this->setMeta(['root_id' => $this->request['id']]);
            $entities->join('ae_mbp_actual', 'ae_mbp_actual.auditable_entity_id', '=', 'auditable_entities_actual.auditable_entity_id')
            ->join('ae_mbp_sbp', 'ae_mbp_sbp.ae_mbp_id', '=', 'ae_mbp_actual.master_ae_mbp_id')
            ->join('bp_objectives', 'bp_objectives.bp_id', '=', 'ae_mbp_sbp.bp_id')
            ->join('business_processes_steps', 'business_processes_steps.bp_objective_id', '=', 'bp_objectives.bp_objective_id')
            ->join('bp_steps_risks', 'bp_steps_risks.bp_step_id', '=', 'business_processes_steps.bp_steps_id')
            ->where('bp_steps_risks.risk_id', $this->request['id']);
        }
        if($starting == 'control') {
            $this->setMeta(['root_id' => $this->request['id']]);
            $entities->join('ae_mbp_actual', 'ae_mbp_actual.auditable_entity_id', '=', 'auditable_entities_actual.auditable_entity_id')
            ->join('ae_mbp_sbp', 'ae_mbp_sbp.ae_mbp_id', '=', 'ae_mbp_actual.master_ae_mbp_id')
            ->join('bp_objectives', 'bp_objectives.bp_id', '=', 'ae_mbp_sbp.bp_id')
            ->join('business_processes_steps', 'business_processes_steps.bp_objective_id', '=', 'bp_objectives.bp_objective_id')
            ->join('bp_steps_risks', 'bp_steps_risks.bp_step_id', '=', 'business_processes_steps.bp_steps_id')
            ->join('risks_control', 'risks_control.risk_id', '=', 'bp_steps_risks.risk_id')
            ->where('risks_control.control_id', $this->request['id']);
        }
        # End optimize
//dd($entities->get());
        return $entities->get();
    }

    public function getMainProcess($entity_id = '')
    {
        $starting = $this->request['subtreeview']; // duplicate
        $roots = ['subbp', 'objective', 'risk', 'control'];
        $entity_id = isset($this->request['id']) ? $this->request['id']: $entity_id;
        if( $entity_id ) {
            $main = AeMbpActual::where('auditable_entity_id', $entity_id);
            $this->setIdField('master_ae_mbp_id');
        }
        else {
            $this->setIdField('ae_mbp_id');
            $main = AeMbp::groupBy('main_bp_name');
        }

        if( isset($this->filter['main_bp']) && $this->filter['main_bp'] != '' ) {
            $main->where('main_bp_name', $this->filter['main_bp']);

            $this->setMeta(['root_name' => $this->filter['main_bp']]);
        }

        if( in_array($starting, $roots) ) {
            $this->setMeta(['root_id' => $this->filter['root_id']]);
        }
        return $main->get();
    }

    public function getSubProcess($mbp_id = '')
    {
        $starting = $this->request['subtreeview'];
        $entity_point = ['all', 'branch', 'dept', 'entity']; // duplicate array on line 549,108
        $mbp_id = isset($this->request['id']) ? $this->request['id']: $mbp_id;
        if( $starting == 'objective' ) {
            $this->setMeta(['root_id' => $this->filter['root_id']]);
            $bp_obj = BpObjective::find($this->filter['root_id']);
            $mbp_id = $bp_obj->bp_id; // override $mbp_id with root mbp id
        }
        if( $starting == 'risk' ) {
            $this->setMeta(['root_id' => $this->filter['root_id']]);
            $bp_obj = BpStepsRisk::leftJoin('business_processes_steps as bps', 'bps.bp_steps_id', '=', 'bp_step_id')
            ->leftJoin('bp_objectives as bpo', 'bps.bp_objective_id', '=', 'bpo.bp_objective_id')
            ->where('risk_id', $this->filter['root_id'])
            ->groupBy('bp_id')
            ->get();
            $mbp_id = $bp_obj->pluck('bp_id')->toArray(); // override $mbp_id with root mbp id
        }
        if( $starting == 'control' ) {
            $controls = BpStepsRisk::leftJoin('risks', 'risks.risk_id', '=', 'bp_steps_risks.risk_id')
            ->leftJoin('business_processes_steps as bps', 'bps.bp_steps_id', '=', 'bp_step_id')
            ->leftJoin('bp_objectives as bpo', 'bps.bp_objective_id', '=', 'bpo.bp_objective_id')
            ->leftJoin('risks_control as rc', 'rc.risk_id', '=', 'risks.risk_id')
            ->leftJoin('controls_master as cm', 'cm.control_id', '=', 'rc.control_id')
            ->where('cm.control_id', $this->filter['root_id'])
            ->get();
            $mbp_id = $controls->pluck('bp_id')->toArray(); // override $mbp_id with root mbp id
        }
        if( $mbp_id ) {
            $sub_bp = AeMbpSbp::join('business_processes', 'business_processes.bp_id', '=', 'ae_mbp_sbp.bp_id')->groupBy('ae_mbp_sbp.bp_id');
            if(isset($this->filter['root_name'])) $sub_bp->where('main_bp_name', $this->filter['root_name']);
            if( !in_array($starting, $entity_point) && $starting != 'mainbp' ) {
                if( is_array($mbp_id) )
                    $sub_bp->whereIn('ae_mbp_sbp.bp_id', $mbp_id);
                else
                    $sub_bp->where('ae_mbp_sbp.ae_mbp_id', $mbp_id);
            }
        }
        else {
            $sub_bp = BusinessProcess::query();
        }

        if( isset($this->filter['sub_bp']) && $this->filter['sub_bp'] != '' ) $sub_bp->where('bp_code', $this->filter['sub_bp']);

        // Optimize this
        if( in_array($starting, $entity_point) ) {
            $sub_bp->leftJoin('ae_mbp_actual', 'ae_mbp_actual.master_ae_mbp_id', '=', 'ae_mbp_sbp.ae_mbp_id')
            ->where('ae_mbp_actual.master_ae_mbp_id', $this->request['id']);
        }
        if($starting == 'mainbp') {
            $sub_bp->leftJoin('ae_mbp_actual', 'ae_mbp_actual.master_ae_mbp_id', '=', 'ae_mbp_sbp.ae_mbp_id')
            ->where('ae_mbp_actual.auditable_entity_id', $this->request['id']);
        }

        return $sub_bp->get();
    }

    public function getProcessObjectives($bp_id = '')
    {
        $starting = $this->request['subtreeview'];
        $bp_id = isset($this->request['id']) ? $this->request['id']: $bp_id;
        if( $starting == 'subbp' ) $bp_id = $this->filter['root_id']; // override $bp_id with root bp id
        if( $bp_id ) {
            $objective = BpObjective::where('bp_id', $bp_id);
        }
        else {
            $objective = AeMbpSbp::join('bp_objectives', 'bp_objectives.bp_id', '=', 'ae_mbp_sbp.bp_id')
            ->groupBy('ae_mbp_sbp.bp_id', 'bp_objectives.bp_objective_id');
        }

        if( isset($this->filter['objective']) && $this->filter['objective'] != '' ) $objective->where('bp_objective_id', $this->filter['objective']);

        if( $starting == 'risk' ) $this->setMeta(['root_id' => $this->filter['root_id']]);
        return $objective->get();
    }

    public function getObjectiveSteps($objective_id = '')
    {
        $starting = $this->request['subtreeview'];
        $objective_id = isset($this->request['id']) ? $this->request['id'] : $objective_id;
        if( $starting != 'risk' && isset($this->filter['root_id']) && $this->filter['root_id'] != '' ) $objective_id = $this->filter['root_id'];
        $process_step = app(BusinessProcessesStepRepository::class );
        $process_step->popCriteria(app(RequestCriteria::class));
        $steps = $process_step->findByField('bp_objective_id', $objective_id);

        if( $starting == 'risk' ) $this->setMeta(['root_id' => $this->filter['root_id']]);

        return $steps;
    }

    public function getStepRisks($step_id = '')
    {
        $step_id = isset($this->request['id']) ? $this->request['id'] : $step_id;
        $risks = app(BpStepsRiskRepository::class);
        $risks->popCriteria(app(RequestCriteria::class));
        $risks->pushCriteria(new JoinRiskCriteria());
        $risks->pushCriteria(new RiskInquiryCriteria());
        if($step_id) {
            $risk = $risks->findByField('bp_step_id', $step_id);
        }
        else {
            $risk = $risks->all();
        }

        return $risk;
    }

    public function getRiskControls($risk_id = '')
    {
        $starting = $this->request['subtreeview'];
        $risk_id = isset($this->request['id']) ? $this->request['id'] : $risk_id;
        if( $starting == 'risk' ) $risk_id = $this->filter['root_id'];
        if( $risk_id ) {
            $control = BusinessProcessesStep::leftJoin('bp_steps_risks as bpsr', 'bp_steps_id', '=', 'bp_step_id')
            ->leftJoin('risks_control as rc', 'bpsr.risk_id', '=', 'rc.risk_id')
            ->leftJoin('controls_master as cm', 'cm.control_id', '=', 'rc.control_id')
            ->where('rc.risk_id', $risk_id);
            if( $starting == 'risk' ) $control->groupBy('rc.control_id');
        }
        else {
            $control = ControlsMaster::query();
        }

        if( isset( $this->filter['control']) && $this->filter['control'] != '' ) $control->where('control_code', $this->filter['control']);

        return $control->get();
    }

    public function getRelatedEntity($group, $business, $entity, $branch_code, $department, $parent = 'N')
    {
        return $this->model
                    ->where('parent_entity', $parent)
                    ->where('group_type', $group)
                    ->where('business_type', $business)
                    ->where('entity_class', $entity)
                    ->where('branch_code', $branch_code)
                    ->where('department_code', $department);
    }

    public function setInquiryType($type)
    {
        $this->inquiry_type = $type;
        return $this;
    }

    public function setLazy($lazy = true)
    {
        $this->lazy = $lazy;
        return $this;
    }

    public function setIdField($id_field)
    {
        $this->id_field = $id_field;
        return $this;
    }

    public function setTextLabel($text)
    {
        $this->text = $text;
        return $this;
    }

    public function setInquiryDetails($details = array())
    {
        $this->inquiry_details = $details;
        return $this;
    }

    public function setIcon($icon = 'asterisk')
    {
        $this->icon = $icon;
        return $this;
    }

    public function getEntityProcesses($id)
    {
        $entity = $this->model->find($id);
        return $entity->businessProcess()
                      ->addSelect('business_processes.*')
                      ->lookupDetails(config('iapms.lookups.business_process'),'lv_bp_code', 'business_processes.bp_code')
                      ->lookupDetails(config('iapms.lookups.source_type'),'lv_source_type', 'business_processes.source_type')
                      ->lookupDetails(config('iapms.lookups.objective.category'),'lv_objective_category', 'business_processes.objective_category')
                      ->get();
    }

    public function setMeta($meta = array())
    {
        if( count($this->meta) )
            $this->meta = array_merge($this->meta, $meta);
        else
            $this->meta = $meta;

        return $this;
    }

    public function getEntityObjectives($id)
    {
        return BpObjective::whereIn('bp_id', function($query) use ($id){
            $query->select('bp_id')
                  ->from('auditable_entities_bp')
                  ->where('auditable_entity_id', $id)
                  ->groupBy('bp_id');
        })->get();
    }

    public function getEntityBpSteps($id)
    {
        return BusinessProcessesStep::whereIn('bp_objective_id', function($query) use ($id){
            $query->select('bp_objective_id')
                  ->from('bp_objectives')
                  ->whereIn('bp_objective_id', function($q) use ($id){
                        $q->select('bp_id')
                              ->from('auditable_entities_bp')
                              ->where('auditable_entity_id', $id)
                              ->groupBy('bp_id');
                    })
                    ->groupBy('bp_objective_id');
        })->get();
    }

    public function getEntityRisks($id)
    {
        return Risk::whereIn('risk_id', function($query) use ($id){
            $query->select('risk_id')
                  ->from('bp_steps_risks')
                  ->whereIn('bp_step_id', function($q) use ($id){
                        $q->select('bp_objective_id')
                              ->from('bp_objectives')
                              ->whereIn('bp_objective_id', function($q2) use ($id){
                                    $q2->select('bp_id')
                                          ->from('auditable_entities_bp')
                                          ->where('auditable_entity_id', $id)
                                          ->groupBy('bp_id');
                                })
                              ->groupBy('bp_objective_id');
                    })
                    ->groupBy('risk_id');
        })->get();
    }

    public function getEntityControls($id)
    {
        return ControlsMaster::whereIn('control_id', function($query) use ($id){
            $query->select('control_id')
                  ->from('risks_control')
                  ->whereIn('risk_id', function($q) use ($id){
                        $q->select('risk_id')
                          ->from('bp_steps_risks')
                          ->whereIn('bp_step_id', function($q) use ($id){
                                $q->select('bp_objective_id')
                                  ->from('bp_objectives')
                                  ->whereIn('bp_objective_id', function($q2) use ($id){
                                        $q2->select('bp_id')
                                              ->from('auditable_entities_bp')
                                              ->where('auditable_entity_id', $id)
                                              ->groupBy('bp_id');
                                    })
                                  ->groupBy('bp_objective_id');
                          })
                          ->groupBy('risk_id');
                    })
                    ->groupBy('control_id');
        })->get();
    }

    public function getEntityRisksWithAssessment($company_code)
    {
        return $this->model->select(\DB::raw("ia_risks.risk_code, ia_risks.risk_response_type, ia_risks.risk_source_type, if(ia_risk_assessment_scale.inherent_impact_value > 0, ia_risk_assessment_scale.inherent_impact_value, ia_risks.def_inherent_impact_rate) as impact, if(ia_risk_assessment_scale.inherent_likelihood_value > 0, ia_risk_assessment_scale.inherent_likelihood_value, ia_risks.def_inherent_likelihood_rate) as likelihood, if(ia_risk_assessment_scale.inherent_remarks <> '', ia_risk_assessment_scale.inherent_remarks, ia_risks.risk_remarks) as remarks, (select matrix_value from ia_matrix inner join ia_matrix_details on `ia_matrix`.`matrix_id` = `ia_matrix_details`.`matrix_id` where `ia_matrix`.`matrix_name` = 'Risk Index' and x_sequence = if(ia_risk_assessment_scale.inherent_impact_value > 0, ia_risk_assessment_scale.inherent_impact_value, ia_risks.def_inherent_impact_rate) and y_sequence = if(ia_risk_assessment_scale.inherent_likelihood_value > 0, ia_risk_assessment_scale.inherent_likelihood_value, ia_risks.def_inherent_likelihood_rate)) as matrix_value"))
        ->join('auditable_entities_bp', 'auditable_entities_bp.auditable_entity_id', '=', 'auditable_entities.auditable_entity_id')
        ->join('bp_objectives', 'bp_objectives.bp_id', '=', 'auditable_entities_bp.bp_id')
        ->join('business_processes_steps', 'business_processes_steps.bp_objective_id', '=', 'bp_objectives.bp_objective_id')
        ->join('bp_steps_risks', 'bp_steps_risks.bp_step_id', '=', 'business_processes_steps.bp_steps_id')
        ->join('risks', 'risks.risk_id', '=', 'bp_steps_risks.risk_id')
        ->leftJoin('risk_assessment_scale', function($ras){
            $ras->on('auditable_entities.auditable_entity_id', '=', 'risk_assessment_scale.auditable_entity_id')
                ->where('risk_assessment_scale.risk_id', '=', 'bp_steps_risks.risk_id')
                ->where('risk_assessment_scale.bp_id', '=', 'auditable_entities_bp.bp_id');
        })
        ->where('company_code', $company_code)
        ->groupBy('risks.risk_id')
        ->get();
    }

    public function getEntityInquiry()
    {
        $master =  $this->request['subtreeview'] == 'all' ? 'company' : ($this->request['subtreeview'] == 'branch' ? 'branch' : '' );
        $treeview = $master == 'company' ? 'branch' : ($master == 'branch' ? 'dept' : '' );
        $type = $treeview != '' ? 'entity' : 'mainBp';
        $data = $this->setIdField('auditable_entity_id')
        ->setTextLabel('auditable_entity_name')
        ->setInquiryType($type)
        ->setInquiryDetails([
            'group_type' => 'Group Type',
            'business_type' => 'Business Type',
            'entity_class' => 'Entity Class',
            'lv_branch_code_desc' => 'Branch',
            'department_code' => 'Department',
        ])
        ->setIcon('building')
        ->setMeta(['treeview' => $treeview])
        ->setLazy(true)
        ->getActualEntities('', $master);

        return $data;
    }

    public function getMainBpInquiry()
    {
        $data = $this->setTextLabel('main_bp_name')
        ->setInquiryType('subBp')
        ->setIcon('cog')
        ->setLazy(true)
        ->getMainProcess();

        return $data;
    }

    public function getSubBpInquiry()
    {
        $data = $this->setIdField('bp_id')
        ->setTextLabel('bp_name')
        ->setInquiryType('objective')
        ->setIcon('cog')
        ->setLazy(true)
        ->getSubProcess();

        return $data;
    }

    public function getObjectiveInquiry()
    {
        $data = $this->setIdField('bp_objective_id')
        ->setTextLabel('objective_name')
        ->setInquiryType('objectiveStep')
        ->setInquiryDetails(['objective_narrative' => 'Narrative'])
        ->setIcon('crosshairs')
        ->setLazy('true')
        ->getProcessObjectives();

        return $data;
    }
    
    public function getObjectiveStepInquiry()
    {
        $data = $this->setIdField('bp_steps_id')
        ->setTextLabel('activity_narrative')
        ->setInquiryType('risk')
        ->setInquiryDetails(['bp_steps_seq' => 'Sequence'])
        ->setIcon('step-forward')
        ->setLazy('true')
        ->getObjectiveSteps();

        return $data;
    }

    public function getRiskInquiry()
    {
        $data = $this->setIdField('risk_id')
        ->setTextLabel('lv_risk_code_meaning')
        ->setInquiryType('control')
        ->setInquiryDetails([
            'risk_code' => 'Code',
            'lv_risk_code_desc' => 'Description',
            'risk_type' => 'Risk Type',
            'risk_remarks' => 'Risk Remarks',
            'risk_source_type' => 'Risk Source Type',
            'risk_response_type' => 'Risk Response Type',
            'def_inherent_impact_rate' => 'Inherent Impact Rate',
            'def_inherent_likelihood_rate' => 'Inherent Likelihood Rate',
            'def_residual_impact_rate' => 'Residual Impact Rate',
            'def_residual_likelihood_rate' => 'Residual Likelihood Rate',
            'rating_comment' => 'Rating Comment',
        ])
        ->setIcon('exclamation-triangle')
        ->setLazy(true)
        ->getStepRisks();

        if( $this->request['subtreeview'] == 'control' ) $this->setLazy(false);

        return $data;
    }

    public function getControlInquiry()
    {
        $data = $this->setIdField('control_id')
        ->setTextLabel('control_name')
        ->setLazy(false)
        ->setInquiryDetails([
            'control_code' => 'Control Code',
            'lv_control_code_desc' => 'Control Description',
            'assertions' => 'Assertions',
            'control_types' => 'Control Types',
            'application' => 'Application',
            'control_source' => 'Control Source',
            'coso_data' => ['component_code', 'principle_code', 'focus_code'],
        ])
        ->setIcon('cogs')
        ->setMeta([ 'modal' => 'data-id' ])
        ->getRiskControls();
        if( $this->request['subtreeview'] == 'control' ) $this->setLazy('true');

        return $data;
    }

    public function getNextInquiry()
    {
        $subtree = $this->request['subtreeview'];
        $level = \Request::get('level') != "" ? \Request::get('level') : 0; // set tree level count
        $entity = ['all', 'branch', 'entity']; // duplicate array on line 108
        $this->starting_point = in_array($subtree, $entity) ? 'entity' : $subtree;
        $this->next_inquiry =  strtolower(\Request::get('_type')) == strtolower($this->starting_point) && $level < 1 ? 0 : $level;
        $sequence = config('iapms.inquiry.sequence.'.$this->starting_point);
        $next_index = count($sequence) == $this->next_inquiry ? $this->next_inquiry - 1 : $this->next_inquiry;
        $this->next_inquiry = $sequence[$next_index];

        $this->setInquiryType($this->next_inquiry)->setMeta(['treeview' => $this->starting_point, 'level' => ($level + 1)]);
    }
}
