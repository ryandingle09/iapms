<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PlanGroupAuditorRepository;
use App\Models\PlanGroupAuditor;
use App\Validators\PlanGroupAuditorValidator;

/**
 * Class PlanGroupAuditorRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PlanGroupAuditorRepositoryEloquent extends BaseRepository implements PlanGroupAuditorRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PlanGroupAuditor::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
