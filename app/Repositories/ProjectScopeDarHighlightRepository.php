<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectScopeDarHighlightRepository
 * @package namespace App\Repositories;
 */
interface ProjectScopeDarHighlightRepository extends RepositoryInterface
{
    //
}
