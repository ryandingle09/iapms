<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectScopeApgFindingsRepository;
use App\Models\ProjectScopeApgFindings;
use App\Validators\ProjectScopeApgFindingsValidator;

/**
 * Class ProjectScopeApgFindingsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectScopeApgFindingsRepositoryEloquent extends BaseRepository implements ProjectScopeApgFindingsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectScopeApgFindings::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
