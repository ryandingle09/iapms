<?php

namespace App\Repositories;

use App\Models\AuditableEntity;
use App\Models\CompanyProfile;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CompanyBranchRepository;
use App\Models\CompanyBranch;
use App\Validators\CompanyBranchValidator;

/**
 * Class CompanyBranchRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CompanyBranchRepositoryEloquent extends BaseRepository implements CompanyBranchRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CompanyBranch::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function createDefaultBranch(CompanyProfile $company)
    {
        $default = [
            'branch_code' => 'default',
            'entity_class' => '',
        ];
        return $company->branches()->create($default);
    }

    public function updateStatus($company_id, $branch_code, $status)
    {
        return $this->model
                    ->where('company_id', $company_id)
                    ->where('branch_code', $branch_code)
                    ->update([ 'active' => $status ]);
    }

    /**
     * Get master related company branches
     * @param AuditableEntity $entity
     * @return mixed
     */
    public function getMasterRelatedCompanyBranches(AuditableEntity $entity)
    {
        $branches = $this->model
        ->select('company_branches.*', 'company_profile.*')
        ->rightJoin('company_profile', function ($j) use($entity){
            $j->on('company_profile.company_id', '=', 'company_branches.company_id')
            ->where('business_type', '=', $entity->business_type)
            ->where('group_type', '=', $entity->group_type);
        })
        ->lookupDetails(config('iapms.lookups.branch.code'), 'lv_branch_code', 'branch_code')
        ->activeBranch()
        ->entityClass($entity->entity_class)
        ->get();

        return $branches;
    }
}
