<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditorManagementInputProjectRepository
 * @package namespace App\Repositories;
 */
interface AuditorManagementInputProjectRepository extends RepositoryInterface
{
    //
}
