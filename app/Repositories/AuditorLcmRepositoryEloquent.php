<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AuditorLcmRepository;
use App\Models\AuditorLcm;
use App\Validators\AuditorLcmValidator;

/**
 * Class AuditorLcmRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditorLcmRepositoryEloquent extends BaseRepository implements AuditorLcmRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AuditorLcm::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
