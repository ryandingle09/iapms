<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\PlanProjectScopeRepository;
use App\Models\PlanProjectScope;
use App\Validators\PlanProjectScopeValidator;

/**
 * Class PlanProjectScopeRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PlanProjectScopeRepositoryEloquent extends BaseRepository implements PlanProjectScopeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PlanProjectScope::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
