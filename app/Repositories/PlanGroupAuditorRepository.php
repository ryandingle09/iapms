<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlanGroupAuditorRepository
 * @package namespace App\Repositories;
 */
interface PlanGroupAuditorRepository extends RepositoryInterface
{
    //
}
