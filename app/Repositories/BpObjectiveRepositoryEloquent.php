<?php

namespace App\Repositories;

use App\Models\BusinessProcessesStep;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\BpObjectiveRepository;
use App\Models\BpObjective;
use App\Validators\BpObjectiveValidator;

/**
 * Class BpObjectiveRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BpObjectiveRepositoryEloquent extends BaseRepository implements BpObjectiveRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BpObjective::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getRisksAssessmentByEntityBp($entity_id, $bp_id)
    {
        $risk_assessment = $this->model
                                ->select(\DB::raw('ia_risk_assessment_scale.impact_value, ia_risk_assessment_scale.likelihood_value ,ia_risk_assessment_scale.remarks as ra_remarks, ia_risks.*, lv_risk_code.description lv_risk_code_desc, lv_risk_type.description lv_risk_type_desc, lv_source_type.description lv_source_type_desc, lv_response_type.description lv_response_type_desc, lv_enterprise_risk_code.description lv_enterprise_risk_code_desc, lv_enterprise_risk_type.description lv_enterprise_risk_type_desc, lv_enterprise_risk_class.description lv_enterprise_risk_class_desc, lv_enterprise_risk_area.description lv_enterprise_risk_area_desc, ( SELECT matrix_value FROM ia_matrix INNER JOIN ia_matrix_details ON ia_matrix.matrix_id = ia_matrix_details.matrix_id WHERE ia_matrix.matrix_name = \'Risk Index\' AND x_sequence = IF(ia_risk_assessment_scale.impact_value > 0, ia_risk_assessment_scale.impact_value, ia_risks.def_inherent_impact_rate) AND y_sequence = IF(ia_risk_assessment_scale.likelihood_value > 0, ia_risk_assessment_scale.likelihood_value, ia_risks.def_inherent_likelihood_rate)) AS matrix_value, ( SELECT matrix_value FROM ia_matrix INNER JOIN ia_matrix_details ON ia_matrix.matrix_id = ia_matrix_details.matrix_id WHERE ia_matrix.matrix_name = \'Severity weight\' AND color = ( SELECT color FROM ia_matrix INNER JOIN ia_matrix_details ON ia_matrix.matrix_id = ia_matrix_details.matrix_id WHERE ia_matrix.matrix_name = \'Risk Index\' AND x_sequence = IF(ia_risk_assessment_scale.impact_value > 0, ia_risk_assessment_scale.impact_value, ia_risks.def_inherent_impact_rate) AND y_sequence = IF(ia_risk_assessment_scale.likelihood_value > 0, ia_risk_assessment_scale.likelihood_value, ia_risks.def_inherent_likelihood_rate) LIMIT 1) LIMIT 1) AS severity_weight'))
                                ->join('business_processes_steps', 'business_processes_steps.bp_objective_id', '=', 'bp_objectives.bp_objective_id')
                                ->join('bp_steps_risks', 'bp_steps_risks.bp_step_id', '=', 'business_processes_steps.bp_steps_id')
                                ->join('risks', 'risks.risk_id', '=', 'bp_steps_risks.risk_id')
                                ->leftJoin('risk_assessment_scale', function($j) use($entity_id, $bp_id){
                                    $j->on('risk_assessment_scale.risk_id', '=', 'risks.risk_id')
                                      ->where('risk_assessment_scale.auditable_entity_id', '=', $entity_id)
                                      ->where('risk_assessment_scale.bp_id', '=', $bp_id);
                                })
                                ->leftJoin(\DB::raw('ia_lookup_values as lv_risk_code'), function($r){
                                    $r->on(\DB::raw('lv_risk_code.lookup_code'), '=', 'risks.risk_code')
                                      ->where(\DB::raw('lv_risk_code.lookup_type'), '=', config('iapms.lookups.risk.risks'));
                                })
                                ->leftJoin(\DB::raw('ia_lookup_values as lv_risk_type'), function($r){
                                    $r->on(\DB::raw('lv_risk_type.lookup_code'), '=', 'risks.risk_type')
                                      ->where(\DB::raw('lv_risk_type.lookup_type'), '=', config('iapms.lookups.risk.type'));
                                })
                                ->leftJoin(\DB::raw('ia_lookup_values as lv_source_type'), function($r){
                                    $r->on(\DB::raw('lv_source_type.lookup_code'), '=', 'risks.risk_source_type')
                                      ->where(\DB::raw('lv_source_type.lookup_type'), '=', config('iapms.lookups.risk.source'));
                                })
                                ->leftJoin(\DB::raw('ia_lookup_values as lv_response_type'), function($r){
                                    $r->on(\DB::raw('lv_response_type.lookup_code'), '=', 'risks.risk_response_type')
                                      ->where(\DB::raw('lv_response_type.lookup_type'), '=', config('iapms.lookups.risk.response'));
                                })
                                ->leftJoin(\DB::raw('ia_lookup_values as lv_enterprise_risk_code'), function($r){
                                    $r->on(\DB::raw('lv_enterprise_risk_code.lookup_code'), '=', 'risks.enterprise_risk_code')
                                      ->where(\DB::raw('lv_enterprise_risk_code.lookup_type'), '=', config('iapms.lookups.risk.ent_code'));
                                })
                                ->leftJoin(\DB::raw('ia_lookup_values as lv_enterprise_risk_type'), function($r){
                                    $r->on(\DB::raw('lv_enterprise_risk_type.lookup_code'), '=', 'risks.enterprise_risk_type')
                                      ->where(\DB::raw('lv_enterprise_risk_type.lookup_type'), '=', config('iapms.lookups.risk.ent_type'));
                                })
                                ->leftJoin(\DB::raw('ia_lookup_values as lv_enterprise_risk_class'), function($r){
                                    $r->on(\DB::raw('lv_enterprise_risk_class.lookup_code'), '=', 'risks.enterprise_risk_class')
                                      ->where(\DB::raw('lv_enterprise_risk_class.lookup_type'), '=', config('iapms.lookups.risk.ent_class'));
                                })
                                ->leftJoin(\DB::raw('ia_lookup_values as lv_enterprise_risk_area'), function($r){
                                    $r->on(\DB::raw('lv_enterprise_risk_area.lookup_code'), '=', 'risks.enterprise_risk_area')
                                      ->where(\DB::raw('lv_enterprise_risk_area.lookup_type'), '=', config('iapms.lookups.risk.ent_area'));
                                })
                                ->where('bp_objectives.bp_id', $bp_id)
                                ->groupBy('risks.risk_id')
                                ->get();
        return $risk_assessment;
    }

    public function getObjectiveRisks($objective_id)
    {
        $obj_risks = BusinessProcessesStep::select('*')
        ->join('bp_steps_risks', 'bp_steps_risks.bp_step_id', '=', 'bp_steps_id')
        ->join('risks', 'risks.risk_id', '=', 'bp_steps_risks.risk_id')
        ->leftJoin(\DB::raw('ia_lookup_values as lv_risk_code'), function($j){
            $j->on(\DB::raw('lv_risk_code.lookup_code'), '=', 'risks.risk_code');
            $j->where(\DB::raw('lv_risk_code.lookup_type'), '=', 'Risks');
        })
        ->addSelect( \DB::raw('lv_risk_code.meaning as lv_risk_code_meaning') )
        ->where('bp_objective_id', $objective_id)
        ->get();

        return $obj_risks;
    }
}
