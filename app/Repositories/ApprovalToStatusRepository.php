<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ApprovalToStatusRepository
 * @package namespace App\Repositories;
 */
interface ApprovalToStatusRepository extends RepositoryInterface
{
    //
}
