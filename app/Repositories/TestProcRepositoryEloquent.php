<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\TestProcRepository;
use App\Models\TestProc;
use App\Validators\TestProcValidator;

/**
 * Class TestProcRepositoryEloquent
 * @package namespace App\Repositories;
 */
class TestProcRepositoryEloquent extends BaseRepository implements TestProcRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TestProc::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
