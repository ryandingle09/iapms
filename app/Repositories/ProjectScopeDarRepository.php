<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectScopeDarRepository
 * @package namespace App\Repositories;
 */
interface ProjectScopeDarRepository extends RepositoryInterface
{
    //
}
