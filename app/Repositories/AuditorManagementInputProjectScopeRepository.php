<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditorManagementInputProjectScopeRepository
 * @package namespace App\Repositories;
 */
interface AuditorManagementInputProjectScopeRepository extends RepositoryInterface
{
    //
}
