<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RiskAssessmentScaleAnnualAsvRepository
 * @package namespace App\Repositories;
 */
interface RiskAssessmentScaleAnnualAsvRepository extends RepositoryInterface
{
    //
}
