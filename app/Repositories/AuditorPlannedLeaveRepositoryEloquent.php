<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AuditorPlannedLeaveRepository;
use App\Models\AuditorPlannedLeave;
use App\Validators\AuditorPlannedLeaveValidator;

/**
 * Class AuditorPlannedLeaveRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditorPlannedLeaveRepositoryEloquent extends BaseRepository implements AuditorPlannedLeaveRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AuditorPlannedLeave::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
