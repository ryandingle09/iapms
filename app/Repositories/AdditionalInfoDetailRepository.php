<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AdditionalInfoDetailRepository
 * @package namespace App\Repositories;
 */
interface AdditionalInfoDetailRepository extends RepositoryInterface
{
    //
}
