<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AuditorProficiencyRepository;
use App\Models\AuditorProficiency;
use App\Validators\AuditorProficiencyValidator;

/**
 * Class AuditorProficiencyRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditorProficiencyRepositoryEloquent extends BaseRepository implements AuditorProficiencyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AuditorProficiency::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
