<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectScopeDarSofRepository
 * @package namespace App\Repositories;
 */
interface ProjectScopeDarSofRepository extends RepositoryInterface
{
    //
}
