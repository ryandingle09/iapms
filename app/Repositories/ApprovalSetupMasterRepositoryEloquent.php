<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ApprovalSetupMasterRepository;
use App\Models\ApprovalSetupMaster;
use App\Validators\ApprovalSetupMasterValidator;

/**
 * Class ApprovalSetupMasterRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ApprovalSetupMasterRepositoryEloquent extends BaseRepository implements ApprovalSetupMasterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ApprovalSetupMaster::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
