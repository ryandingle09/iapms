<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectScopeAuditorRepository;
use App\Models\ProjectScopeAuditor;
use App\Validators\ProjectScopeAuditorValidator;

/**
 * Class ProjectScopeAuditorRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectScopeAuditorRepositoryEloquent extends BaseRepository implements ProjectScopeAuditorRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectScopeAuditor::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
