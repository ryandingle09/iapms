<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditorProficiencyRepository
 * @package namespace App\Repositories;
 */
interface AuditorProficiencyRepository extends RepositoryInterface
{
    //
}
