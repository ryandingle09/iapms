<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\IomDistListRepository;
use App\Models\IomDistList;
use App\Validators\IomDistListValidator;

/**
 * Class IomDistListRepositoryEloquent
 * @package namespace App\Repositories;
 */
class IomDistListRepositoryEloquent extends BaseRepository implements IomDistListRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return IomDistList::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
