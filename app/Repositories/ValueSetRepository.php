<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ValueSetRepository
 * @package namespace App\Repositories;
 */
interface ValueSetRepository extends RepositoryInterface
{
    //
}
