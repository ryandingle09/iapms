<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ControlsDetailRepository;
use App\Models\ControlsDetail;
use App\Validators\ControlsDetailValidator;

/**
 * Class ControlsDetailRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ControlsDetailRepositoryEloquent extends BaseRepository implements ControlsDetailRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ControlsDetail::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
