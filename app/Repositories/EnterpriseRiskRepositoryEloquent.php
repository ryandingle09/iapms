<?php

namespace App\Repositories;

use App\Criteria\EnterpriseRiskRequestCriteria;
use App\Models\AuditableEntitiesActual;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\EnterpriseRiskRepository;
use App\Models\EnterpriseRisk;
use App\Validators\EnterpriseRiskValidator;

/**
 * Class EnterpriseRiskRepositoryEloquent
 * @package namespace App\Repositories;
 */
class EnterpriseRiskRepositoryEloquent extends BaseRepository implements EnterpriseRiskRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EnterpriseRisk::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(app(EnterpriseRiskRequestCriteria::class));
    }

    /**
     * Get assigned entity to auditee
     * @param AuditableEntitiesActual $entity
     * @return mixed
     */
    public function getAuditeeEntity(AuditableEntitiesActual $entity)
    {
        // Company entity
        if( $entity->branch_code == '' ) {
            return $entity->entitiesByCompany($entity->company_code)->get();
        }
        // branch entity
        elseif ($entity->department_code == '') {
            return $entity->entitiesByBranch($entity->branch_code)->get();
        }

        return $entity;
    }
}
