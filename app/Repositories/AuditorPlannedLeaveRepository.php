<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditorPlannedLeaveRepository
 * @package namespace App\Repositories;
 */
interface AuditorPlannedLeaveRepository extends RepositoryInterface
{
    //
}
