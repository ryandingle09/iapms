<?php

namespace App\Repositories;

use App\Abstracts\PresentableRepository;
use App\Models\Plan;
use App\Repositories\PlanRepository;
use App\Validators\PlanValidator;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class PlanRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PlanRepositoryEloquent extends PresentableRepository implements PlanRepository
{
    protected $fieldSearchable = [
        'plan_year'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Plan::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    
}
