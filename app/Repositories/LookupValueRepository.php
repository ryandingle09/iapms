<?php

namespace App\Repositories;

use App\Models\LookupType;
use App\Models\LookupValue;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LookupValueRepository
 * @package namespace App\Repositories;
 */
interface LookupValueRepository extends RepositoryInterface
{

}
