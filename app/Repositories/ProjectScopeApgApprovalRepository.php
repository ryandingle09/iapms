<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectScopeApgApprovalRepository
 * @package namespace App\Repositories;
 */
interface ProjectScopeApgApprovalRepository extends RepositoryInterface
{
    //
}
