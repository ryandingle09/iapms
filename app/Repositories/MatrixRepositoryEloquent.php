<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\MatrixRepository;
use App\Models\Matrix;
use App\Validators\MatrixValidator;

/**
 * Class MatrixRepositoryEloquent
 * @package namespace App\Repositories;
 */
class MatrixRepositoryEloquent extends BaseRepository implements MatrixRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Matrix::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
