<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AuditorTasksScheduleInquiryRepository;
use App\Models\AuditorTasksScheduleInquiry;
use App\Validators\AuditorTasksScheduleInquiryValidator;

/**
 * Class AuditorTasksScheduleInquiryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditorTasksScheduleInquiryRepositoryEloquent extends BaseRepository implements AuditorTasksScheduleInquiryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AuditorTasksScheduleInquiry::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
