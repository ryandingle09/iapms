<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ControlsTestAuditTypeRepository;
use App\Models\ControlsTestAuditType;
use App\Validators\ControlsTestAuditTypeValidator;

/**
 * Class ControlsTestAuditTypeRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ControlsTestAuditTypeRepositoryEloquent extends BaseRepository implements ControlsTestAuditTypeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ControlsTestAuditType::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
