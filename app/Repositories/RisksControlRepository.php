<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RisksControlRepository
 * @package namespace App\Repositories;
 */
interface RisksControlRepository extends RepositoryInterface
{
    //
}
