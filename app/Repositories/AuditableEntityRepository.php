<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuditableEntityRepository
 * @package namespace App\Repositories;
 */
interface AuditableEntityRepository extends RepositoryInterface
{
    public function processInquiry();

    public function getActualEntities($company_code = '', $master = 'company');

    public function getMainProcess($entity_id = '');

    public function getSubProcess($mbp_id = '');

    public function getProcessObjectives($bp_id = '');

    public function getObjectiveSteps($objective_id = '');

    public function getStepRisks($step_id = '');

    public function getRiskControls($risk_id = '');

    public function getRelatedEntity($group, $business, $entity, $branch_code, $department, $parent = 'N');

    public function setInquiryType($type);

    public function setLazy($lazy = true);

    public function setIdField($id_field);

    public function setTextLabel($text);

    public function setInquiryDetails($details = array());

    public function setIcon($icon = 'asterisk');

    public function setMeta($meta = array());

    public function getEntityProcesses($id);

    public function getEntityObjectives($id);

    public function getEntityBpSteps($id);

    public function getEntityRisks($id);

    public function getEntityControls($id);

    public function getEntityRisksWithAssessment($company_code);

    public function getEntityInquiry();

    public function getMainBpInquiry();

    public function getSubBpInquiry();

    public function getObjectiveInquiry();

    public function getObjectiveStepInquiry();

    public function getRiskInquiry();

    public function getControlInquiry();

    public function getNextInquiry();
}
