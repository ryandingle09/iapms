<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectScopeSummaryRepository
 * @package namespace App\Repositories;
 */
interface ProjectScopeSummaryRepository extends RepositoryInterface
{
    //
}
