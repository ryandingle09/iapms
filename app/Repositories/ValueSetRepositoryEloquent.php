<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ValueSetRepository;
use App\Models\ValueSet;
use App\Validators\ValueSetValidator;

/**
 * Class ValueSetRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ValueSetRepositoryEloquent extends BaseRepository implements ValueSetRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ValueSet::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
