<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AeMbpActualRepository
 * @package namespace App\Repositories;
 */
interface AeMbpActualRepository extends RepositoryInterface
{
    public function getAverageCustomMeasures($conditions);

    public function findBudgetedMandays($mbp_id, $audit_type);
}
