<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProjectScopeDarHighlightRepository;
use App\Models\ProjectScopeDarHighlight;
use App\Validators\ProjectScopeDarHighlightValidator;

/**
 * Class ProjectScopeDarHighlightRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ProjectScopeDarHighlightRepositoryEloquent extends BaseRepository implements ProjectScopeDarHighlightRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectScopeDarHighlight::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
