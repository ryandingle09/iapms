<?php

namespace App\Repositories;

use App\Criteria\AuditableEntitySearchCriteria;
use App\Criteria\EntityFullSearchGroupByCriteria;
use App\Events\AuditableEntityWasCreated;
use App\Models\AuditableEntity;
use App\Models\CompanyBranch;
use App\Models\CompanyProfile;
use App\Services\AuditableEntityService;
use App\Services\LookupService;
use Illuminate\Database\Eloquent\Collection;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Models\AuditableEntitiesActual;
use Log;

/**
 * Class AuditableEntitiesActualRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditableEntitiesActualRepositoryEloquent extends BaseRepository implements AuditableEntitiesActualRepository
{
    protected $fieldSearchable = [
        'auditable_entity_name'
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AuditableEntitiesActual::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(app(AuditableEntitySearchCriteria::class));
    }

    public function getCustomMeasurePercentile(AuditableEntitiesActual $entity, $values = array())
    {
        $response = [];
        $service = app(AuditableEntityService::class);

        for ($i = 0; $i < count($values); $i++) {
            $index = $i + 1;
            $cm_field = 'cm_' . $index . '_value';
            $custom_measure = $entity->master->masterBp()
            ->join('ae_mbp_actual', 'ae_mbp_actual.master_ae_mbp_id', '=', 'ae_mbp.ae_mbp_id')
            ->select($cm_field)
            ->whereNotNull('cm_' . $index . '_name')
            ->where('cm_' . $index . '_name', '<>', '')
            ->orderByRaw(\DB::raw("CASE cm_{$index}_high_score_type WHEN 'Floor' THEN cm_{$index}_value ELSE null END ASC, CASE cm_{$index}_high_score_type WHEN 'Ceiling' THEN cm_{$index}_value ELSE null END DESC"))
            ->get();

            $cm = $custom_measure->pluck('cm_' . $index . '_value')->toArray();
            $response[$index] = $service->calculatePercentile($cm, $values[$i]);
        }

        return $response;
    }

    public function makeBranchFromCompany(AuditableEntitiesActual $entity, $branches = array())
    {
        $existing = [];
        $created = [];
        $service = app( LookupService::class );
        $company_profile = $entity->companyProfile;
        $branches = $company_profile->branches()->whereIn('branch_code', $branches)->get();
        if ( !$branches->count() ) return ['success' => false, 'message' => 'Selected branches does not exists in company\'s profile.'];

        $company_lookup = $service->getLookupValueByCode( config('iapms.lookups.company'), $company_profile->company_code);
        foreach ($branches as $branch) {
            $branch_lookup = $service->getLookupValueByCode( config('iapms.lookups.branch.code'), $branch->branch_code);

            $entities = $this->getMasterEntities($company_profile->business_type, $branch->entity_class);
            if( !$entities ) {
                $created[] = 'There\'s no master entity for branch - '.$branch->branch_code ;
            }
            else {
                $branch_entity = $company_lookup->meaning.'.'.$branch_lookup->meaning.'.Default';
                $branch_exists = $this->model->where('auditable_entity_name', $branch_entity)->count();
                if( !$branch_exists ) {
                    $this->model->create([
                        'auditable_entity_name' => $branch_entity,
                        'company_code' => $company_profile->company_code,
                        'branch_code' => $branch->branch_code,
                        'department_code' => '',
                        'master_auditable_entity_id' => $entities[0]->auditable_entity_id,
                    ]);
                }
                foreach ( $entities as $master ) {
                    $entity_name = $company_lookup->meaning.'.'.$branch_lookup->meaning.'.'.$master->department_code;
                    $exists = $this->model->where('auditable_entity_name', $entity_name)->count();
                    if( !$exists ) {
                        $new_entity = $this->model->create([
                            'auditable_entity_name' => $company_lookup->meaning.'.'.$branch_lookup->meaning.'.'.$master->department_code,
                            'company_code' => $company_profile->company_code,
                            'branch_code' => $branch->branch_code,
                            'department_code' => $master->department_code,
                            'master_auditable_entity_id' => $master->auditable_entity_id,
                        ]);
                        $bps = $master->masterBp;
                        foreach ($bps as $bp) {
                            $new_entity->masterBp()->create([
                                'master_ae_mbp_id' => $bp->ae_mbp_id,
                                'main_bp_name' => $bp->main_bp_name,
                            ]);
                        }

                        $created[] = $entity_name;
                        event(new AuditableEntityWasCreated($new_entity, 'actual'));
                    }
                    else {
                        $existing[] = $entity_name;
                    }
                }
            }
        }

        $res_created = count($created) ? implode(', ', array_unique($created)).' is successfully created!' : '';
        $res_exists  = count($existing) ? implode(', ', array_unique($existing)).' already exists!' : '';
        $resp = $res_created.'; '.$res_exists;

        return $resp;
    }

    /**
     * @param $business
     * @param $entity
     * @return mixed
     */
    public function getMasterEntities($business, $entity)
    {
        $service = app( LookupService::class );
        $lookup = $service->getLookup( config('iapms.lookups.master_company') );
        $master_company = $lookup->lookupValue()->where('lookup_code', $business)->first();
        if( is_null($master_company) ) return false;

        $entities = AuditableEntity::with(['masterBp' => function($m){
                        $m->select('ae_mbp_id', 'main_bp_name', 'auditable_entity_id');
                    }])
                    ->where('company_code', $master_company->lookup_code)
                    ->where('entity_class', $entity)
                    ->where('branch_code', '<>', '')
                    ->where('department_code', '<>', '')
                    ->get();

        return $entities;
    }

    public function createEntity($attributes = array())
    {
        $comp_profile = CompanyProfile::where('company_code', $attributes['company_code'])->first();

        if(!is_null($comp_profile)) {
            $branch = $comp_profile->branches()->where('branch_code', $attributes['branch_code'])->first();
            if(!is_null($branch)) {
                $master = $this->getMasterEntity($comp_profile->group_type, $comp_profile->business_type, $branch->entity_class, $attributes['branch_code'], $attributes['department_code']);
            }
        }

        $attributes['master_auditable_entity_id'] = isset($master) ? $master->auditable_entity_id : '1';
        $entity = $this->model->create($attributes);
        if( isset( $master ) && !is_null( $master ) ) event(new AuditableEntityWasCreated($entity, 'actual'));

        return $entity;
    }

    public function getMasterEntity($group, $business, $entity, $branch, $dept)
    {
        return AuditableEntity::relatedEntity($group, $business, $entity, $branch, $dept)->first();
    }

    public function applyMasterBp(AuditableEntitiesActual $entity)
    {
        $master_bp = $entity->master->masterBp->pluck('main_bp_name', 'ae_mbp_id');
        foreach ($master_bp as $key => $value) {
            $entity->masterBp()->create([
                'master_ae_mbp_id' => $key,
                'main_bp_name' => $value,
            ]);
        }
    }

    public function createActualFromMaster(AuditableEntity $master, Collection $companies)
    {
        if( !$companies->count() && !is_null($master) ) return false;

        foreach ($companies as $company) {
            $this->createCompanyEntity($master, $company)->createBranchEntity($master, $company);
            $entity_name = $company->company_name.'.'.$company->lv_branch_code_meaning.'.'.$master->department_code;
            $exists = $this->checkExists($master, $entity_name);
            if( !$exists ) {
                $attributes = [
                    'auditable_entity_name' => $entity_name,
                    'company_code' => $company->company_code,
                    'branch_code' => $company->branch_code,
                    'department_code' => $master->department_code,
                ];
                $master->actual()->create($attributes); // create actual through master relationship

                Log::info('[Actual Entity Created] '.$entity_name);
            }
        }
    }

    public function createCompanyEntity(AuditableEntity $master, CompanyBranch $company)
    {
        $entity_name = $company->company_name.'.Default.Default';
        $attributes = [
            'auditable_entity_name' => $entity_name,
            'company_code' => $company->company_code,
            'branch_code' => '',
            'department_code' => '',
        ];
        $exists = $this->checkExists($master, $entity_name);
        if( !$exists ) $master->actual()->create($attributes); // create actual through master relationship

        return $this;
    }

    public function createBranchEntity(AuditableEntity $master, CompanyBranch $company)
    {
        $entity_name = $company->company_name.'.'.$company->branch_code.'.Default';
        $attributes = [
            'auditable_entity_name' => $entity_name,
            'company_code' => $company->company_code,
            'branch_code' => $company->branch_code,
            'department_code' => '',
        ];
        $exists = $this->checkExists($master, $entity_name);
        if( !$exists ) $master->actual()->create($attributes); // create actual through master relationship

        return $this;
    }

    public function checkExists(AuditableEntity $master, $entity_name)
    {
        return $master->actual()->where('auditable_entity_name', $entity_name)->count();
    }

    public function getBranchCompanies($branch_code)
    {
        $this->applyCriteria();

        $companies = $this->model->whereIn('company_code', function($q) use($branch_code){
            $q->from('auditable_entities_actual')
            ->select('company_code')
            ->distinct()
            ->where('branch_code', $branch_code);
        });

        return $companies->get();
    }

    public function fullSearch(){

        $this->applyCriteria();
        
        $query = $this->model;

        $query = $query->from('auditable_entities_actual AS aea')
                        ->join('ae_mbp_actual AS mbpa','mbpa.auditable_entity_id','=','aea.auditable_entity_id')
                        ->join('ae_mbp_sbp AS sbp','sbp.ae_mbp_id','=','mbpa.master_ae_mbp_id')
                        ->join('bp_objectives AS bpo','bpo.bp_id','=','sbp.bp_id')
                        ->join('business_processes_steps AS bps','bps.bp_objective_id','=','bpo.bp_objective_id')
                        ->join('bp_steps_risks AS bpsr','bpsr.bp_step_id','=','bps.bp_steps_id')
                        ->join('risks AS r','r.risk_id','=','bpsr.risk_id')
                        ->join('risks_control AS rc','rc.risk_id','=','r.risk_id')
                        ->join('controls_test_proc AS ctp','ctp.control_id','=','rc.control_id')
                        ->join('controls_test_audit_types AS ctat','ctat.control_test_id','=','ctp.control_test_id');

        return $query->get();
    }

}
