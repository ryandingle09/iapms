<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\AuditorManagementInputRepository;
use App\Models\AuditorManagementInput;
use App\Validators\AuditorManagementInputValidator;

/**
 * Class AuditorManagementInputRepositoryEloquent
 * @package namespace App\Repositories;
 */
class AuditorManagementInputRepositoryEloquent extends BaseRepository implements AuditorManagementInputRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AuditorManagementInput::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
