<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ControlsMasterRepository
 * @package namespace App\Repositories;
 */
interface ControlsMasterRepository extends RepositoryInterface
{
    //
}
