<?php

namespace App\Providers;

use App\Providers\RepositoryServiceProvider;
use Illuminate\Support\ServiceProvider;
use Validator,DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('before_equal', function($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            $attr = explode('.', $parameters[0]);
            if( count($attr) > 0 ) {
                $param = $data;
                for( $i=0; $i < count($attr); $i++ ) {
                    $param = $param[$attr[$i]]; // get array input data
                }
            }
            else {
                $param = $data[$parameters[0]];
            }
            if( $param == '' ) return true;

            return strtotime($param) >= strtotime($value);
        });

        Validator::extend('no_conflict_leave', function($attribute, $value, $parameters, $validator) {

            $start_date = array_get($validator->getData(), $parameters[0]);
            $end_date = array_get($validator->getData(), $parameters[1]);
            $ignore_id = isset($parameters[2]) ? $parameters[2] : null ;
            $auditor_id = $value;

            $find = DB::table('auditor_planned_leaves')
                        ->where('auditor_id',$auditor_id)
                        ->whereDate('leave_date_to','>=',$start_date)
                        ->whereDate('leave_date_from','<=',$end_date);
            if($ignore_id != "NULL" && $ignore_id != null){
                $find->where('id','!=',$ignore_id);
            }
                        
            return !$find->count() > 0;
        });

        Validator::extend('recurring_date', function($attribute, $value, $parameters, $validator) {
            $holiday_date = $value;
            $find = DB::table('holidays')
                ->where('fixed_date', '=','Y')
                ->whereMonth('holiday_date', '=', date('m',strtotime($holiday_date)))
                ->whereDay('holiday_date', '=', date('d',strtotime($holiday_date)));

            if( $find->count() ) return false; // recurring holiday

            return true;
        });

        Validator::extend('unique_holiday', function($attribute, $value, $parameters, $validator) {

            $is_fix = array_get($validator->getData(), $parameters[0]);
            $holiday_date = array_get($validator->getData(), $parameters[1]);
            $ignore_id = isset($parameters[2]) ? $parameters[2] : null ;
            $holiday_type = $value;

            $find = DB::table('holidays')->where('holiday_type',$holiday_type);
            if( $find->count() && is_null($ignore_id) ) {
                $holiday = $find->first();
                if( $holiday->fixed_date == 'Y' ) return false;
            }
            if($is_fix != 'Y') {
                $find->whereYear('holiday_date','=',date('Y',strtotime($holiday_date)));
            }
            if($ignore_id != "NULL" && $ignore_id != null){
                $find->where('id','!=',$ignore_id);
            }
            if($find->count()){
                return false;
            }
            return true;
        });

        Validator::extend('unique_range', function($attribute, $value, $parameters, $validator) {
            $ignore_id = isset($parameters[1]) ? $parameters[1] : null ;
            $from = $value;
            $to = array_get($validator->getData(), $parameters[0]);
            $range = [$from, $to];

            $find = 0;

            if($ignore_id){
                $find = DB::table('auditor_grading_matrix')
                        ->where(function($q) use ($ignore_id , $range) {
                            $q->whereBetween('variance_from', $range)
                              ->where('id','!=',$ignore_id);
                        })
                        ->where(function($q) use ($ignore_id , $range) {
                            $q->whereBetween('variance_to', $range)
                              ->where('id','!=',$ignore_id);
                        })->count();
            }else{
                $find = DB::table('auditor_grading_matrix')
                ->whereBetween('variance_from', $range)
                ->orWhereBetween('variance_to', $range)
                ->count();
            }
            if($find){
                return false;
            }
            return true;
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RepositoryServiceProvider::class);
        $this->app->bind('\App\Services\MenuService', function($app) {
            return new \App\Services\MenuService( $app->make('\App\Repositories\MenuRepository') );
        });

        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
        }
    }
}
