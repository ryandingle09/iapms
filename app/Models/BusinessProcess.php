<?php

namespace App\Models;

use App\Traits\Updater;
use App\Traits\Lookupable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class BusinessProcess extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'bp_id';
    protected $fillable = [
        'bp_name',
        'bp_code',
        'source_type',
        'source_ref',
        'bp_remarks',
        'attribute1',
        'attribute2',
        'attribute3',
        'attribute4',
        'attribute5',
        'attribute6',
        'attribute7',
        'attribute8',
        'attribute9',
        'attribute10',
        'created_by',
        'last_update_by',
    ];

    public function scopeWithbpStepRisks($query, $entity_id)
    {
        return $query->with(['bpObjectives' => function($o) use($entity_id){
            $o->with(['bpSteps' => function($s) use($entity_id) {
                $s->with(['bpStepRisk' => function($r) use($entity_id){
                    $r->with(['riskAssessment' => function($ra) use($entity_id){
                        $ra->whereEntity($entity_id);
                    }]);
                }]);
            }]);
        }]);
    }

    public function riskAssessment()
    {
        return $this->hasMany('App\Models\RiskAssessmentScale', 'bp_id');
    }

    public function enterpriseRisk()
    {
        return $this->hasMany('App\Models\EnterpriseRisk', 'bp_id');
    }

    public function projectScope()
    {
        return $this->hasMany('App\Models\PlanProjectScope', 'bp_id');
    }

    public function bpObjectives()
    {
        return $this->hasMany('App\Models\BpObjective', 'bp_id');
    }

    public function subProcess()
    {
        return $this->hasMany('App\Models\AeMbpSbp', 'bp_id');
    }

    protected function scopeBpDetails($query)
    {
        return $query->select('business_processes.*')
            ->lookupDetails(config('iapms.lookups.business_process'), 'lv_bp_code', 'business_processes.bp_code')
            ->lookupDetails(config('iapms.lookups.source_type'), 'lv_source_type', 'business_processes.source_type');
    }
}
