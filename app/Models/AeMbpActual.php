<?php

namespace App\Models;

use App\Traits\Authorable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AeMbpActual extends Model implements Transformable
{
    use TransformableTrait, Authorable, Updater, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = 'ae_mbp_actual';
    protected $primaryKey = 'ae_mbp_id';
    protected $fillable = [
        'auditable_entity_id',
        'main_bp_name',
        'master_ae_mbp_id',
        'cm_1_value',
        'cm_2_value',
        'cm_3_value',
        'cm_4_value',
        'cm_5_value',
        'cm_6_value',
        'cm_7_value',
        'cm_8_value',
        'cm_9_value',
        'cm_10_value',
        'walk_thru_mandays',
        'planning_mandays',
        'followup_mandays',
        'discussion_mandays',
        'draft_dar_mandays',
        'final_report_mandays',
        'wrap_up_mandays',
        'reviewer_mandays',
        'created_by',
        'last_update_by',
    ];

    protected $appends = [
        'adjusted_severity_value',
        'additional_budgeted_mandays_total'
    ];

    public function masterBp()
    {
        return $this->belongsTo('App\Models\AeMbp', 'master_ae_mbp_id');
    }

    public function subBp()
    {
        return $this->hasMany('App\Models\AeMbpSbp', 'ae_mbp_id', 'master_ae_mbp_id');
    }

    public function subBpDetails()
    {
        return $this->hasMany('App\Models\AeMbpActual', 'ae_mbp_id', 'master_ae_mbp_id')
            ->select('ae_mbp.*')
            ->join('ae_mbp', 'ae_mbp.ae_mbp_id', '=', 'master_ae_mbp_id')
            ->join('ae_mbp_sbp', 'ae_mbp_sbp.ae_mbp_id', '=', 'ae_mbp.ae_mbp_id')
            ->join('business_processes', 'ae_mbp_sbp.bp_id', '=', 'business_processes.bp_id')
            ->lookupDetails(config('iapms.lookups.main_bp'), 'lv_main_bp_name', 'ae_mbp.main_bp_name');

        $relation = $this->belongsToMany('App\Models\BusinessProcess', 'ae_mbp_sbp', 'ae_mbp_id', 'bp_id')
            ->withPivot([
                'mbp_bp_seq',
                'created_by',
                'last_update_by',
            ])
            ->withTimestamps()
            ->select('*')
            ->lookupDetails(config('iapms.lookups.business_process'), 'lv_bp_code', 'business_processes.bp_code')
            ->lookupDetails(config('iapms.lookups.source_type'), 'lv_source_type', 'business_processes.source_type');
        return $relation;
    }

    public function auditableEntity()
    {
        return $this->belongsTo('App\Models\AuditableEntitiesActual', 'auditable_entity_id','auditable_entity_id');
    }

    public function scopeSubBusinessProcessJoin($query)
    {
        return $query->join('ae_mbp_sbp', 'ae_mbp_sbp.ae_mbp_id', '=', 'master_ae_mbp_id')
                     ->join('business_processes', 'ae_mbp_sbp.bp_id', '=', 'business_processes.bp_id');
    }

    public function getAdditionalBudgetedMandaysTotalAttribute(){
        return $this->walk_thru_mandays +
            $this->planning_mandays +
            $this->followup_mandays +
            $this->discussion_mandays +
            $this->draft_dar_mandays +
            $this->final_report_mandays +
            $this->wrap_up_mandays +
            $this->reviewer_mandays;
    }

    public function getAdjustedSeverityValueAttribute($value){
        $asv_service = app('App\Services\AdjustedSeverityValueService');
        $total_adjusted_severity_value = $asv_service->getAdjustedSeverityValue([
            "auditable_entity_id" => $this->auditable_entity_id,
            "ae_mbp_id" => $this->ae_mbp_id
        ]);
        return  $total_adjusted_severity_value;
    }

}
