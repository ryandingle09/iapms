<?php

namespace App\Models;

use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AuditableEntitiesBp extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    public $incrementing = false;

    protected $primaryKey = null;
    protected $table = 'auditable_entities_bp';
    protected $fillable = [
        'bp_id',
        'auditable_entity_id',
        'cm_1_value',
        'cm_2_value',
        'cm_3_value',
        'cm_4_value',
        'cm_5_value',
        'created_by',
        'last_update_by',
    ];

    public function entity()
    {
        return $this->hasMany('App\Models\AuditableEntity', 'auditable_entity_id');
    }

    public function scopePrcessDetails($query)
    {
        return $query->select('business_processes.*')
        ->join('business_processes', 'business_processes.bp_id', '=', 'auditable_entities_bp.bp_id')
        ->lookupDetails(config('iapms.lookups.business_process'), 'lv_bp_code', 'business_processes.bp_code')
        ->lookupDetails(config('iapms.lookups.source_type'), 'lv_source_type', 'business_processes.source_type');
    }
}
