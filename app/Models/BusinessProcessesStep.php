<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class BusinessProcessesStep extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'bp_steps_id';
    protected $fillable = [
        'bp_objective_id',
        'bp_steps_seq',
        'activity_narrative',
        'attribute1',
        'attribute2',
        'attribute3',
        'attribute4',
        'attribute5',
        'attribute6',
        'attribute7',
        'attribute8',
        'attribute9',
        'attribute10',
    ];

    public function bpObjectives()
    {
        return $this->belongsTo('App\Models\BpObjective', 'bp_objective_id');
    }

    public function bpStepRisk()
    {
        return $this->belongsToMany('App\Models\Risk', 'bp_steps_risks', 'bp_step_id', 'risk_id')->withPivot('bp_step_risk_seq', 'created_by', 'last_update_by')->withTimestamps();
    }

    public function risks()
    {
        return $this->hasMany('App\Models\BpStepsRisk', 'bp_step_id');
    }
}
