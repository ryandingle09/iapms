<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Artesaos\Defender\Traits\Models\Permission;
use Artesaos\Defender\Contracts\Permission as PermissionInterface;

class UserPermission extends Model implements PermissionInterface
{
    use Permission;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table='permission';
    protected $dates = [
        'created_date',
        'last_update_date',
    ];

    public function child_menu()
    {
        return $this->hasOne('App\Models\Menu', 'permission_id');
    }
    
    public function entries()
    {
        return $this->belongsToMany('App\Models\Menu', 'menu_entries', 'permission_id');
    }
}
