<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

// InterOfficeMemo old class name
class ProjectScopeIom extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
        'project_scope_id',
        'ref_no',
	    'start_date',
        'subject',
        'introduction',
        'background',
        'objectives',
        'auditee_assistance',
        'staffing'
    ];

    protected $table = "project_scope_iom";

    protected $primaryKey = 'project_scope_iom_id';

    public function projectScope(){
        return $this->belongsTo('App\Models\ProjectScope','project_scope_id', 'project_scope_id');
    }

    public function getStartDateAttribute($value)
    {
        return $value ? date('d-M-Y',strtotime($value)) : $value;
    }

}
