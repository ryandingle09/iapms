<?php

namespace App\Models;

use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AuditorProficiency extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'auditor_proficiency_id';
    protected $table = 'auditor_proficiency';
    protected $fillable = [
        'auditor_id',
        'proficiency_type',
        'proficiency_rate',
        'remarks',
        'created_by',
        'last_update_by',
    ];

    public function auditor()
    {
        return $this->belongsTo('App\Models\Auditor');
    }
}
