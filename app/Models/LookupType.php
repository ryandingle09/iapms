<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class LookupType extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
	const UPDATED_AT = 'last_update_date';

	protected $fillable = [
		'lookup_type',
		'meaning',
		'description',
		'created_by',
		'last_update_by',
	];

	public function lookupValue()
	{
		return $this->hasMany('App\Models\LookupValue');
	}

	public function createdBy()
	{
		return $this->belongsTo('App\User', 'created_by');
	}

	public function updatedBy()
	{
		return $this->belongsTo('App\User', 'last_update_by');
	}

    public function getLastUpdateDateAttribute($value)
    {
        return strtotime($value) > 0 ? date('m-d-Y', strtotime($value) ) : null;
    }
}
