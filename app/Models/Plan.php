<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Plan extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    public $approval_cat = "Annual Audit Plan";
    public $approval_subcat = "Plan";

    protected $table = 'plan';
    protected $primaryKey = 'plan_id';

    protected $fillable = [
    	'plan_year',
    	'plan_name',
        'plan_status',
    	'remarks',
    ];

    protected $appends = [
        'allotted_mandays',
        'annual_mandays',
        'available_mandays',
        'approvals',
        'end_approval'
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function($plan){
            // Populate active auditors from auditor list within the year
            $year = $plan->plan_year;
            $auditors = \App\Models\Auditor::where(function($q) use ($year){
                                    $q->whereYear('effective_start_date','<=',$year)
                                        ->whereYear('effective_end_date','>=',$year);

                                })
                                ->orWhere(function($q) use ($year){
                                    $q->whereYear('effective_start_date','<=',$year)
                                        ->whereNull('effective_end_date');
                                })
                                ->get(['auditor_id']);

            $auditors->each(function($item) use ($plan){
                $auditor = new PlanAuditor([
                    'auditor_id' => $item->auditor_id
                ]);
                $plan->planAuditors()->save($auditor);
            });
        });
    }

    public function planAuditors()
    {
        return $this->hasMany('App\Models\PlanAuditor');
    }

    public function planMainAuditor()
    {
        return $this->hasOne('App\Models\PlanAuditor')->where('auditor_type','Auditor');
    }

    public function auditor()
    {
        return $this->belongsToMany('App\Models\Auditor', 'plan_auditors')
        			->withPivot('created_by', 'last_update_by', 'auditor_type')
                    ->withTimestamps();
    }

    public function getAllottedMandaysAttribute()
    {
        $mandays = app('App\Services\MandaysService');
        return $mandays->calculateAuditorProjectMandays($this->plan_id);
    }

    public function getAnnualMandaysAttribute()
    {
        return $this->planAuditors->sum('annual_mandays');
    }

    public function getAvailableMandaysAttribute(){
        return $this->annual_mandays - $this->allotted_mandays ;
    }

    public function getApprovalsAttribute()
    {
        return \App\Models\Approval::where('source_category', $this->approval_cat )
                                ->where('source_subcategory',$this->approval_subcat )
                                ->where('approval_ref_id',$this->plan_id )
                                ->orderBy( 'approval_id', 'desc')
                                ->with('approver')
                                ->get();
                                
    }

    public function getEndApprovalAttribute()
    {
        $master = \App\Models\ApprovalSetupMaster::where('source_category', $this->approval_cat )
                                ->where('source_subcategory',$this->approval_subcat )
                                ->first();

        $last_approval = $this->approvals->first();
        if( $master && $last_approval ){
            if( $master->finish_status == $last_approval->status_to ){
                return $last_approval;
            }
        }
        return null;
    }

    


}
