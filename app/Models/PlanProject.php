<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class PlanProject extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    public $approval_cat = "Annual Audit Plan";
    public $approval_subcat = "Project";

    protected $primaryKey = 'plan_project_id';
    protected $fillable = [
        'plan_group_id',
        'project_sequence',
        'plan_project_name',
        'audit_type',
        'plan_project_status',
        'target_start_date',
        'target_end_date',
        'plan_project_remarks'
    ];

    protected $appends = [
        'running_allotted_mandays',
        'mandays_to_add',
        'total_mandays',
        'approvals',
        'end_approval',
        'scopes_total_budgeted_mandays',
        'editable'
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function($plan_project){
            $auditors = $plan_project->group->auditors;

            $auditors->each(function($item) use ($plan_project){
                $auditor = new \App\Models\PlanProjectAuditor([
                    'auditor_id' => $item->auditor_id
                ]);
                $plan_project->auditors()->save($auditor);
            });
        });
    }

    public function group()
    {
        return $this->belongsTo('App\Models\PlanGroup','plan_group_id','plan_group_id');
    }

    public function auditors()
    {
        return $this->hasMany('App\Models\PlanProjectAuditor', 'plan_project_id','plan_project_id');
    }

    public function projectAuditor()
    {
        return $this->belongsToMany('App\Models\Auditor', 'plan_project_auditors', 'plan_project_id', 'auditor_id')
                    ->withPivot('created_by', 'last_update_by', 'auditor_type', 'allotted_mandays')
                    ->withTimestamps();
    }

    public function scopes()
    {
        return $this->hasMany('App\Models\PlanProjectScope', 'plan_project_id');
    }

    public function setTargetStartDateAttribute($value)
    {
        $this->attributes['target_start_date'] = date('Y-m-d', strtotime($value));
    }

    public function setTargetEndDateAttribute($value)
    {
        $this->attributes['target_end_date'] = date('Y-m-d', strtotime($value));
    }

    public function getTargetStartDateAttribute($value)
    {
        return date_display($value);
    }

    public function getTargetEndDateAttribute($value)
    {
        return  date_display($value);
    }

    public function getApprovedDateAttribute($value)
    {
        return  date_display($value);
    }

    public function getRunningAllottedMandaysAttribute($value)
    {
        $mandays = app('App\Services\MandaysService');
        return $mandays->calculateAuditorProjectMandays($this->plan_id, $this->plan_project_id, null);
    }

    public function getMandaysToAddAttribute(){
        
        return $this->scopes->sum("budgeted_mandays");
    }

    public function getTotalMandaysAttribute(){
        return $this->running_allotted_mandays + $this->mandays_to_add;
    }


    public function getApprovalsAttribute()
    {
        return \App\Models\Approval::where('source_category', $this->approval_cat )
                                ->where('source_subcategory',$this->approval_subcat )
                                ->where('approval_ref_id',$this->plan_project_id )
                                ->orderBy( 'approval_id', 'desc')
                                ->with('approver')
                                ->get();
    }

    public function getEndApprovalAttribute()
    {
        $master = \App\Models\ApprovalSetupMaster::where('source_category', $this->approval_cat )
                                ->where('source_subcategory',$this->approval_subcat )
                                ->first();

        $last_approval = $this->approvals->first();
        if( $master && $last_approval ){
            if( $master->finish_status == $last_approval->status_to ){
                return $last_approval;
            }
        }
        return null;
    }


    public function getScopesTotalBudgetedMandaysAttribute()
    {
        return $this->scopes->sum('budgeted_mandays');
    }
    
    public function getEditableAttribute()
    {
        if($this->plan_status != "CAE Approved"){
            return true;
        }else{
            return false;
        }
    }
}
