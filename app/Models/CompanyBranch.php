<?php

namespace App\Models;

use App\Traits\Authorable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class CompanyBranch extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable, Authorable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    public $incrementing = false;

    protected $primaryKey = null;
    protected $fillable = [
        'branch_code',
        'group_type',
        'business_type',
        'active',
        'branch_store_size',
        'branch_address',
        'branch_zone',
        'branch_store_class',
        'branch_class',
        'entity_class',
        'branch_opening_date',
        'created_by',
        'last_update_by',
    ];
    protected $hidden = [
        'created_date',
        'last_update_date',
    ];

    public function company()
    {
        return $this->belongsTo('App\Models\CompanyProfile', 'company_id');
    }

    public function setBranchOpeningDateAttribute($value)
    {
        $this->attributes['branch_opening_date'] = $value != '' ? date('Y-m-d', strtotime($value) ) : null;
    }

    public function getBranchOpeningDateAttribute($value)
    {
        return !is_null($value) && $value != '0000-00-00' ? date( config('app.date_display') , strtotime($value) ) : '';
    }

    public function scopeActiveBranch($query)
    {
        return $query->where('active', 'Y');
    }

    public function scopeEntityClass($query, $class)
    {
        return $query->where('entity_class', $class);
    }
}
