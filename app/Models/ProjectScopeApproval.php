<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class ProjectScopeApproval extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'project_scope_apg_approval_id';

    protected $fillable = [
		'project_scope_id',
		'approval_seq',
		'status_from',
		'status_to',
		'auditor_id',
		'remarks',
		'role',
		'status_date'
    ];

    public function getStatusDateAttribute($value){
    	return date_display($value);
    } 

    public function auditor()
    {
        return $this->hasOne('App\User','user_id','auditor_id');
    }

    public function project_scope()
    {
        return $this->belongsTo('App\Models\ProjectScope','project_scope_id','project_scope_id');
    }


}
