<?php

namespace App\Models;

use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AuditorLcm extends Model implements Transformable
{
    use TransformableTrait, Lookupable, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'auditor_lcm_id';
    protected $table = 'auditor_lcm';
    protected $fillable = [
        'auditor_id',
        'lcm_type',
        'ref_no',
        'remarks',
        'issued_date',
        'expiry_date',
    ];

    public function auditor()
    {
        return $this->belongsTo('App\Models\Auditor', 'auditor_id');
    }

    public function setIssuedDateAttribute($value)
    {
        $this->attributes['issued_date'] = date('Y-m-d', strtotime($value) );
    }

    public function setExpiryDateAttribute($value)
    {
        $this->attributes['expiry_date'] = date('Y-m-d', strtotime($value) );
    }

    public function getIssuedDateAttribute($value)
    {
        return $value != '' && $value != '0000-00-00' ? date( config('app.date_display') , strtotime($value) ) : '';
    }

    public function getExpiryDateAttribute($value)
    {
        return $value != '' && $value != '0000-00-00' ? date( config('app.date_display') , strtotime($value) ) : '';
    }
}
