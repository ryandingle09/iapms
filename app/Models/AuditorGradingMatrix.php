<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AuditorGradingMatrix extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = 'auditor_grading_matrix';
    protected $fillable = [
        'variance_from',
        'variance_to',
        'rating',
        'quality_max',
        'documentation_max',
        'remarks',
        'created_by',
        'last_update_by',
    ];

}
