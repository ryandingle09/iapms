<?php

namespace App\Models;

use App\Traits\Authorable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AeMbpSbp extends Model implements Transformable
{
    use TransformableTrait, Authorable, Updater, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = 'ae_mbp_sbp';
    protected $primaryKey = 'ae_mbp_bp_id';
    protected $fillable = [
        'mbp_bp_seq',
        'bp_id',
    ];

    public function masterBp()
    {
        return $this->belongsTo('App\Models\AeMbp', 'ae_mbp_id');
    }

    public function businessProcess()
    {
        return $this->belongsTo('App\Models\BusinessProcess', 'bp_id');
    }

    public function bpSteps()
    {
        return $this->hasManyThrough('App\Models\BusinessProcessesStep', 'App\Models\BpObjective', 'bp_id', 'bp_objective_id', 'bp_id');
    }

    public function objectives()
    {
        return $this->hasMany('App\Models\BpObjective', 'bp_id', 'bp_id');
    }

    protected function withBusinessProcessDetails($query)
    {
        return $query->select('business_processes.*')
                     ->lookupDetails(config('iapms.lookups.business_process'), 'lv_bp_code', 'business_processes.bp_code')
                     ->lookupDetails(config('iapms.lookups.source_type'), 'lv_source_type', 'business_processes.source_type');
    }

    public function scopeWithBusinessProcessDetails($query)
    {
        return $this->withBusinessProcessDetails($query);
    }

    public function scopeJoinBusinessProcess($query)
    {
        return $query->join('business_processes', 'business_processes.bp_id', '=', $this->table.'.bp_id');
    }

    public function scopeBpRisks($query)
    {
        return $query->join('bp_objectives', 'bp_objectives.bp_id', '=', 'ae_mbp_sbp.bp_id')
        ->join('business_processes_steps', 'business_processes_steps.bp_objective_id', '=', 'bp_objectives.bp_objective_id')
        ->join('bp_steps_risks', 'bp_steps_risks.bp_step_id', '=', 'business_processes_steps.bp_steps_id')
        ->groupBy('risk_id', 'ae_mbp_sbp.bp_id');
    }
}
