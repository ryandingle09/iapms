<?php

namespace App\Models;

use App\Traits\Authorable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AeMbp extends Model implements Transformable
{
    use TransformableTrait, Updater, Authorable, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = 'ae_mbp';
    protected $primaryKey = 'ae_mbp_id';
    protected $fillable = [
        'main_bp_name',
        'walk_thru_mandays',
        'planning_mandays',
        'followup_mandays',
        'discussion_mandays',
        'draft_dar_mandays',
        'final_report_mandays',
        'wrap_up_mandays',
        'reviewer_mandays',
        'cm_1_name',
        'cm_1_high_score_type',
        'cm_2_name',
        'cm_2_high_score_type',
        'cm_3_name',
        'cm_3_high_score_type',
        'cm_4_name',
        'cm_4_high_score_type',
        'cm_5_name',
        'cm_5_high_score_type',
        'cm_6_name',
        'cm_6_high_score_type',
        'cm_7_name',
        'cm_7_high_score_type',
        'cm_8_name',
        'cm_8_high_score_type',
        'cm_9_name',
        'cm_9_high_score_type',
        'cm_10_name',
        'cm_10_high_score_type',
        'created_by',
        'last_update_by',
    ];

    public function auditableEntity()
    {
        return $this->belongsTo('App\Models\AuditableEntity', 'auditable_entity_id');
    }

    /**
     * Immediate relation with sub processes
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subBp()
    {
        return $this->hasMany('App\Models\AeMbpSbp', 'ae_mbp_id');
    }

    /**
     * Sub process relation with business process details
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function subBpDetails()
    {
        return $this->belongsToMany('App\Models\BusinessProcess', 'ae_mbp_sbp', 'ae_mbp_id', 'bp_id')
            ->withPivot([
                'ae_mbp_bp_id',
                'mbp_bp_seq',
                'created_by',
                'last_update_by',
            ])
            ->withTimestamps()
            ->select('business_processes.*')
            ->lookupDetails(config('iapms.lookups.business_process'), 'lv_bp_code', 'business_processes.bp_code')
            ->lookupDetails(config('iapms.lookups.source_type'), 'lv_source_type', 'business_processes.source_type');
    }

    public function actual()
    {
        return $this->hasMany('App\Models\AeMbpActual', 'master_ae_mbp_id');
    }

    public function enterpriseRisk()
    {
        return $this->hasMany('App\Models\EnterpriseRisk', 'ae_mbp_id');
    }
}
