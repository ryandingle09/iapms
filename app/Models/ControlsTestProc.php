<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ControlsTestProc extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'control_test_id';
    protected $table = 'controls_test_proc';
    protected $fillable = [
        'control_id',
        'control_test_seq',
        'test_proc_id',
    ];

    public function control()
    {
        return $this->belongsTo('App\Models\ControlsMaster', 'control_id');
    }

    public function auditTypes()
    {
        return $this->hasMany('App\Models\ControlsTestAuditType', 'control_test_id');
    }

    public function testProc()
    {
        return $this->belongsTo('App\Models\TestProc', 'test_proc_id');
    }
}
