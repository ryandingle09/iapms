<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class RiskAssessmentScaleAnnualAsv extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';

    const UPDATED_AT = 'last_update_date';

    protected $table = "risk_assessment_scale_annual_asv";

    protected $primaryKey = 'risk_assessment_scale_annual_asv_id';

    protected $fillable = [
		'year',
		'auditable_entity_id',
		'ae_mbp_id',
		'bp_id',
		'bp_objective_id',
		'bp_steps_id',
		'risk_id',
		'adjusted_severity_value'
    ];

}
