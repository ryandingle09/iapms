<?php

namespace App\Models;

use App\Traits\Authorable;
use App\Traits\Lookupable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class Auditee extends Model implements Transformable
{
    use TransformableTrait, Updater, Authorable, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'auditee_id';

    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'employee_id',
        'auditable_entity_id',
        'supervisor_id',
        'position_code',
        'email_address',
        'department_code',
        'effective_start_date',
        'effective_end_date',
        'created_by',
        'last_update_by',
    ];

    protected $appends = [
        'full_name'
    ];

    public function supervisor()
    {
        return $this->belongsTo('App\Models\Auditee', 'supervisor_id');
    }

    public function subordinate()
    {
        return $this->hasMany('App\Models\Auditee', 'supervisor_id');
    }

    public function auditableEntity()
    {
        return $this->belongsTo('App\Models\AuditableEntitiesActual', 'auditable_entity_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'employee_id', 'employee_id');
    }

    public function setEffectiveStartDateAttribute($value)
    {
        $this->attributes['effective_start_date'] = date('Y-m-d', strtotime($value) );
    }

    public function setEffectiveEndDateAttribute($value)
    {
        $this->attributes['effective_end_date'] = $value != '' ? date('Y-m-d', strtotime($value) ) : null;
    }

    public function getEffectiveStartDateAttribute($value)
    {
        return date('m-d-Y', strtotime($value) );
    }

    public function getEffectiveEndDateAttribute($value)
    {
        return $value != '' ? date('m-d-Y', strtotime($value) ) : null;
    }

    public function getFullNameAttribute($value)
    {
        return preg_replace('/\s+/', ' ',$this->first_name.' '.$this->middle_name.' '.$this->last_name);
    }
}
