<?php

namespace App\Models;

use App\Traits\Authorable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class BpObjective extends Model implements Transformable
{
    use TransformableTrait, Updater, Authorable, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'bp_objective_id';

    protected $fillable = [
        'bp_id',
        'objective_name',
        'objective_narrative',
        'objective_category',
    ];

    public function businessProcess()
    {
        return $this->belongsTo('App\Models\BusinessProcess', 'bp_id');
    }

    public function bpSteps()
    {
        return $this->hasMany('App\Models\BusinessProcessesStep', 'bp_objective_id');
    }

    public function risks()
    {
        return $this->hasManyThrough('App\Models\BpStepsRisk', 'App\Models\BusinessProcessesStep', 'bp_objective_id', 'bp_step_id', 'bp_objective_id');
    }

    public function setObjectiveCategoryAttribute($value)
    {
        $this->attributes['objective_category'] = implode(',', $value);
    }
}
