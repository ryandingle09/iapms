<?php

namespace App\Models;

use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AuditorManagementInput extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'wbs_header_id';
    protected $table = 'wbs_header';
    protected $fillable = [
    	'audit_year',
    	'auditor_id',
    	'template_name',
    	'created_by',
    	'created_date',
    	'last_update_by',
    	'last_update_date'
    ];

    protected $appends = [
        //'full_name',
    ];

    public function scope()
    {
        return $this->hasMany('App\Models\AuditorManagementInputProjectScope','wbs_project_id','wbs_project_id');
    }

    public function project()
    {
        return $this->hasMany('App\Models\AuditorManagementInputProject','wbs_header_id','wbs_header_id');
    }

	public function auditor()
    {
    	return $this->belongsTo('App\Models\Auditor');
    }

}
