<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AdditionalInfoMaster extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = 'additional_info_master';
    protected $primaryKey = 'additional_info_id';
    protected $fillable = [
    	'additional_info_name',
    	'description',
    	'table_name',
    	'created_by',
    	'last_update_by',
    ];

    public function details()
    {
    	return $this->hasMany('App\Models\AdditionalInfoDetail', 'additional_info_id');
    }
}
