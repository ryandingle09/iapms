<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class QuestionnaireDetail extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    public $incrementing = false;

    protected $primaryKey = null;
    protected $fillable = [
        'question',
    	'question_sequence',
    	'response_type',
    	'response_required',
    	'choices_name',
    	'range_from',
    	'range_to',
    	'data_type',
    	'page_break_after',
    	'optional_remarks',
    	'created_by',
    	'last_update_by',
    ];

    public function questionnaire()
    {
    	return $this->belongsTo('App\Models\Questionnaire', 'questionnaire_id');
    }

}
