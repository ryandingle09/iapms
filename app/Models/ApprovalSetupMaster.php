<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ApprovalSetupMaster extends Model implements Transformable
{
	use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = "approval_setup_masters";
    protected $primaryKey = "approval_setup_master_id";

    protected $fillable = [
		'source_category',
		'source_subcategory',
		'source_table',
        'start_status',
        'finish_status',
        'supersede_allowed_flag',
    ];

}
