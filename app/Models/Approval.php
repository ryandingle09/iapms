<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Approval extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = "approvals";
    protected $primaryKey = "approval_id";

    protected $fillable = [
		'source_category',
		'source_subcategory',
		'approver_id',
		'status_from',
		'status_to',
		'approval_ref_id',
		'auditor_type',
		'approval_date',
		'remarks'
    ];

    public function getRemarksAttribute($value){
    	return $value ? $value : "No remarks";
    }

    public function getApprovalDateAttribute($value){
        return date_display($value);
    }

   	public function approver()
    {
        return $this->hasOne('App\User','user_id','approver_id');
    }
}
