<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class MatrixDetail extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    public $incrementing = false;

    protected $primaryKey = null;
    protected $fillable = [
    	'x_value',
    	'x_sequence',
    	'y_value',
    	'y_sequence',
    	'matrix_value',
    	'color',
    	'created_by',
    	'last_update_by',
    ];

    public function matrix()
    {
    	return $this->belongsTo('App\Models\Matrix', 'matrix_id');
    }
}
