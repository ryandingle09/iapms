<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ApprovalToStatus extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = "approval_to_status";
    protected $primaryKey = "approval_to_status_id";

    protected $fillable = [
		'approval_setup_detail_id',
		'status',
		'description',
    ];

}
