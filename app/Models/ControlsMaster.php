<?php

namespace App\Models;

use App\Traits\Authorable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ControlsMaster extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable, Authorable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'control_id';
    protected $table = 'controls_master';
    protected $fillable = [
        'control_name',
        'control_code',
        'component_code',
        'principle_code',
        'focus_code',
        'assertions',
        'control_types',
        'mode',
        'key_control_flag',
        'frequency',
        'control_source',
    ];

    public function testProcedures()
    {
        return $this->hasMany('App\Models\ControlsTestProc', 'control_id');
    }

    public function risk()
    {
        return $this->hasMany('App\Models\RisksControl', 'risk_id');
    }

    public function getAssertionsAttribute($value)
    {
        return $value != "" ? unserialize($value) : [];
    }

    public function getControlTypesAttribute($value)
    {
        return $value != "" ? unserialize($value) : [];
    }

    public function getPrincipleCodeAttribute($value)
    {
        return $value ? collect(json_decode($value)) : [];
    }

    public function getFocusCodeAttribute($value)
    {
        return $value ? collect(json_decode($value)) : [];
    }
}
