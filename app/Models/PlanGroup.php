<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class PlanGroup extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = 'plan_groups';
    protected $primaryKey = 'plan_group_id';

    protected $fillable = [
    	'plan_id',
    	'plan_group_name',
    	'description',
    	'group_head'
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function($plan_group){
            // Populate active auditors from auditor list within the year
            $group_head = \App\Models\Auditor::find($plan_group->group_head);
            $resource_pool  = $plan_group->plan->planAuditors;
            $plan_group->saveAuditorSubordinate($group_head , $resource_pool );
        });
    }

    public function projects()
    {
        return $this->hasMany('App\Models\PlanProject','plan_group_id','plan_group_id');
    }

    public function saveAuditorSubordinate($supervisor ,$resource_pool)
    {

        if( $resource_pool->where('auditor_id', $supervisor->auditor_id )->count() ){
            $auditor = new PlanGroupAuditor([
                'auditor_id' => $supervisor->auditor_id
            ]);
            $this->auditors()->save($auditor);
        }

        if($supervisor->subordinate->count()){

            $supervisor->subordinate->each(function($item) use ( $resource_pool ){
                $this->saveAuditorSubordinate($item , $resource_pool );
            }); 

        }
    }

    public function auditors()
    {
        return $this->hasMany('App\Models\PlanGroupAuditor', 'plan_group_id','plan_group_id');
    }

    public function groupHeadDetails()
    {
        return $this->belongsTo('App\Models\Auditor', 'group_head');
    }

    public function plan()
    {
        return $this->belongsTo('App\Models\Plan', 'plan_id', 'plan_id');
    }

}
