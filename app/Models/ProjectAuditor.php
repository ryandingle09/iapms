<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class ProjectAuditor extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
		'project_id' ,
		'auditor_id' ,
		'auditor_type' ,
		'allotted_mandays' 
	];

     protected $appends = [
        'plan_budgeted_mandays',
        "allotted_miscellaneous_mandays",
        "allotted_apg_mandays",
        "alotted_total_mandays"
    ];

    public static function boot()
    {
        parent::boot();

        static::deleted(function($model)
        {
            $auditor_id = $model->auditor_id;
            $model->scopes()->each(function($scope) use ($auditor_id) {
                $scope->auditors()->where('auditor_id', $auditor_id )->delete();  
            });
        });
    }

    public function scopes()
    {
        return $this->hasMany('App\Models\ProjectScope','project_id','project_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project','project_id','project_id');
    }

	public function auditor()
    {
    	return $this->belongsTo('App\Models\Auditor');
    }

    public function getPlanBudgetedMandaysAttribute()
    {
        if($this->project->annualPlanProject){
            $plan_project = $this->project->annualPlanProject;
            $mandays = app('App\Services\MandaysService');
            return $mandays->calculateAuditorProjectMandays( $plan_project->plan_id, $plan_project->plan_project_id, $this->auditor_id );
        }else{
            return 0;
        }
    }

    public function getAllottedMiscellaneousMandaysAttribute()
    {
        $mandays = app('App\Services\MandaysService');
        return $mandays->projectAllottedMiscMandays( $this->project_id , null, $this->auditor_id);
    }

    public function getAllottedApgMandaysAttribute()
    {
        $mandays = app('App\Services\MandaysService');
        return $mandays->projectAllottedMandaysApg( $this->project_id , null, $this->auditor_id );
    }

    public function getAlottedTotalMandaysAttribute()
    {
        return $this->allotted_miscellaneous_mandays + $this->allotted_apg_mandays;
    }

}
