<?php

namespace App\Models;

use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AuditorManagementInputProject extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'wbs_project_id';
    protected $table = 'wbs_project';

    protected $fillable = [
        'wbs_header_id',
        'project_name',
        'project_head_id',
        'audit_type',       
        'status',           
        'start_date', 
        'end_date',
        'created_by',   
        'created_date', 
        'last_update_by',   
        'last_update_date'   
    ];

    protected $appends = [
        //'full_name',
    ];

    public function scope()
    {
        return $this->hasMany('App\Models\AuditorManagementInputProjectScope','wbs_project_id','wbs_project_id');
    }

    public function auditor()
    {
        return $this->belongsTo('App\Models\Auditor', 'project_head_id', 'auditor_id');
    }

}
