<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class ProjectScopeDar extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';

    const UPDATED_AT = 'last_update_date';

    protected $table = "project_scope_dar";

    protected $primaryKey = 'project_scope_dar_id';

    protected $fillable = [
		'project_scope_id',
		'subject',
		'scope',
		'objectives',
		'opinion'
    ];

}
