<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Questionnaire extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'questionnaire_id';
    protected $fillable = [
    	'questionnaire_name',
    	'description',
    	'questionnaire_type',
    	'created_by',
    	'last_update_by',
    ];

    public function details()
    {
    	return $this->hasMany('App\Models\QuestionnaireDetail', 'questionnaire_id');
    }
}
