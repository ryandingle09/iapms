<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;
use App\Traits\Lookupable;

class IomDistList extends Model implements Transformable
{
    use TransformableTrait, Updater,Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
        'project_scope_id',
	    'auditee_id',
        'distribution_type',
    ];

    protected $table = "iom_dist_list";
    
    protected $primaryKey = "iom_dist_list_id";

    public function auditee()
    {
        return $this->belongsTo('App\Models\Auditee','auditee_id','auditee_id');
    }
}
