<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class ProjectScopeSummary extends Model
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';

    const UPDATED_AT = 'last_update_date';

    protected $table = "project_scope_summary";

    protected $primaryKey = 'project_scope_summary_id';

    protected $fillable = [
		'project_id',
		'scope_id',
		'auditable_entity_id',
		'ae_mbp_id',
		'planned_start_date',
		'planned_end_date',
		'actual_start_date',
		'actual_end_date',
		'grade',
		'remarks'
    ];
}
