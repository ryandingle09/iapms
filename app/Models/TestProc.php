<?php

namespace App\Models;

use App\Traits\Authorable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class TestProc extends Model implements Transformable
{
    use TransformableTrait, Authorable, Updater;
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = 'test_proc';
    protected $primaryKey = 'test_proc_id';
    protected $fillable = [
        'test_proc_name',
        'test_proc_narrative',
        'audit_objective',
        'budgeted_mandays',
    ];

    public function controlTestProc()
    {
        return $this->hasMany('App\Models\ControlsTestProc', 'test_proc_id');
    }
}
