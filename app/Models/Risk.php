<?php

namespace App\Models;

use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Risk extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'risk_id';
    protected $fillable = [
        'risk_code',
        'risk_type',
        'risk_description',
        'risk_type',
        'risk_remarks',
        'risk_source_type',
        'risk_response_type',
        'def_inherent_impact_rate',
        'def_inherent_likelihood_rate',
        'def_residual_impact_rate',
        'def_residual_likelihood_rate',
        'rating_comment',
        'enterprise_risk_id',
        'attribute1',
        'attribute2',
        'attribute3',
        'attribute4',
        'attribute5',
        'attribute6',
        'attribute7',
        'attribute8',
        'attribute9',
        'attribute10',
        'created_by',
        'last_update_by',
    ];

    protected $appends = [
        'risk_code_meaning'
    ];

    public function getRiskCodeMeaningAttribute()
    {
        return "1223";
        // $data = app(\App\Models\LookupValue::class);
        // dd($data->where('lookup_code', $this->risk_code ));
        // return ;//::where('lookup_code', $this->risk_code );
    }

    public function controls()
    {
        return $this->belongsToMany('App\Models\ControlsMaster', 'risks_control', 'risk_id', 'control_id')->withPivot('risk_control_seq');
    }

    public function riskAssessment()
    {
        return $this->hasMany('App\Models\RiskAssessmentScale');
    }

    /**
     * Risk has one Enterprise risk details
     * NOTE: EnterpriseRiskDetails has primary key(enterprise_risk_det_id)
     * that's why it is used rather than EnterpriseRisk
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function enterpriseRisk()
    {
        return $this->hasOne('App\Models\EnterpriseRiskDetail', 'enterprise_risk_det_id', 'enterprise_risk_id');
    }

    public function scopeWithLookupDetails($query){
        return $query->join('lookup_values','lookup_values.lookup_code','=',"risks.risk_code");
    }
}
