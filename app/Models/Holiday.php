<?php

namespace App\Models;

use App\Traits\Lookupable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class Holiday extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
        'holiday_date',
        'holiday_type',
        'description',
        'fixed_date',
    ];

    public function setHolidayDateAttribute($value)
    {
        $this->attributes['holiday_date'] = date('Y-m-d', strtotime($value) );
    }

    public function getHolidayDateAttribute($value)
    {
        return date('d-M-Y', strtotime($value) );
    }
}
