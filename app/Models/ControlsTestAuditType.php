<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ControlsTestAuditType extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    public $incrementing = false;

    protected $primaryKey = null;

    protected $fillable = [
    	'control_test_id',
    	'audit_type',
    ];

    public function controlTestProc()
    {
    	return $this->belongsTo('App\Models\ControlsTestProc', 'control_test_id');
    }
}
