<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class ProjectScopeApgFindings extends Model implements Transformable
{
    use TransformableTrait, Updater;

    protected $primaryKey = 'project_scope_apg_finding_id';
    protected $table = 'project_scope_apg_findings';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
    	'project_scope_apg_id',
		'non_issue',
		'highlight',
		'issues',
		'conclusions',
		'recommendations',
		'auditee_actions_needed',
		'auditee_action_due_date',
		'auditee_actions_taken',
		'auditor_remarks',
		'auditor_id',
		'auditee_id',
    ];

    public function apg()
    {
        return $this->belongsTo('App\Models\ProjectScopeApg','project_scope_apg_id','project_scope_apg_id');
    }

    public function auditor()
    {
        return $this->belongsTo('App\Models\Auditor');
    }

    public function setAuditeeActionDueDateAttribute($value)
    {
        $this->attributes['auditee_action_due_date'] = date('Y-m-d', strtotime($value));
    }

    public function getAuditeeActionDueDateAttribute($value)
    {
        return date_display($value); 
    }

}
