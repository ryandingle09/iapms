<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class PlanProjectAuditor extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'plan_project_auditor_id';

    protected $fillable = [
        'plan_project_id',
    	'auditor_id'
    ];

    protected $appends = [
        'allotted_mandays',
        'running_mandays',
        'available_mandays',
        'annual_mandays'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleted(function($model)
        {
            $auditor_id = $model->auditor_id;
            $model->scopes()->each(function($scope) use ($auditor_id) {
                $scope->auditors()->where('auditor_id', $auditor_id )->delete();  
            });
        });
    }

    public function getAllottedMandaysAttribute(){

        $mandays = app('App\Services\MandaysService');
        return $mandays->calculateAuditorProjectMandays(
            null,
            null,
            $this->plan_project_id,
            $this->auditor_id 
        );
    }

    public function getAnnualMandaysAttribute(){
        $plan_auditors = $this->project->group->plan->planAuditors;

        if($plan_auditors){
            $auditor = $plan_auditors->where('auditor_id',$this->auditor_id)->first();
            if($auditor){
                return $auditor->annual_mandays;
            }
        }
        return 0;
    }

    public function getRunningMandaysAttribute(){
        $mandays = app('App\Services\MandaysService');
        return $mandays->calculateAuditorProjectMandays(
            $this->project->group->plan->plan_id,
            null,
            null,
            $this->auditor_id
        );
    }

    public function getAvailableMandaysAttribute(){
        return $this->annual_mandays - $this->running_mandays;
    }

    public function auditor()
    {
    	return $this->belongsTo('App\Models\Auditor');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\PlanProject', 'plan_project_id');
    }

    public function projects()
    {
        return $this->hasMany('App\Models\PlanProject', 'plan_project_id');
    }

    public function scopes()
    {
        return $this->hasMany('App\Models\PlanProjectScope','plan_project_id','plan_project_id');
    }
}
