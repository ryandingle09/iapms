<?php

namespace App\Models;

use App\Traits\Auditable;
use App\Traits\Authorable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\CacheableInterface;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\CacheableRepository;
use Prettus\Repository\Traits\TransformableTrait;

class AuditableEntitiesActual extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable, Authorable, Auditable;

    const CREATED_AT = 'created_date';
    protected $fillable = [
        'auditable_entity_name',
        'company_code',
        'branch_code',
        'department_code',
        'contact_name',
        'entity_head_name',
        'master_auditable_entity_id',
        'attribute1',
        'attribute2',
        'attribute3',
        'attribute4',
        'attribute5',
        'attribute6',
        'attribute7',
        'attribute8',
        'attribute9',
        'attribute10',
        'created_by',
        'last_update_by',
    ];

    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'auditable_entity_id';
        protected $table = 'auditable_entities_actual';

    public function master()
    {
        return $this->belongsTo('App\Models\AuditableEntity', 'master_auditable_entity_id');
    }

    public function masterBp()
    {
        return $this->hasMany('App\Models\AeMbpActual', 'auditable_entity_id');
    }

    public function masterMainBp()
    {
        return $this->hasMany('App\Models\AeMbp', 'master_auditable_entity_id', 'auditable_entity_id');
    }

    public function auditee()
    {
        return $this->hasMany('App\Models\Auditablee', 'auditable_entity_id');
    }

    public function companyProfile()
    {
        return $this->belongsTo('App\Models\CompanyProfile', 'company_code', 'company_code');
    }

    public function riskAssessment()
    {
        return $this->hasMany('App\Models\RiskAssessmentScale', 'auditable_entity_id');
    }

    /**
     * Scope to get related entity by company and department
     * @param $query
     * @param $company_code
     * @param $department_code
     * @return mixed
     */
    public function scopeRelatedCompanyDepartment($query, $company_code, $department_code)
    {
        return $query->where($this->table.'.company_code', $company_code)
            ->where($this->table.'.department_code', $department_code);
    }

    /**
     * Scope to determine parent company and child company
     * @param $query
     * @param string $is_parent
     * @return mixed
     */
    public function scopeIsParentCompany($query, $is_parent = 'Y')
    {
        $op = '<>';
        $is_null = 'whereNotNull';
        if( $is_parent == 'Y' ) {
            $op = '=';
            $is_null = 'orWhereNull';
        }

        return $query->where('auditable_entities_actual.branch_code', $op, '')->{$is_null}('auditable_entities_actual.branch_code')
                     ->where('auditable_entities_actual.department_code', $op, '')->{$is_null}('auditable_entities_actual.department_code');
    }

    public function scopeExcludeParentCompany($query)
    {
        return $query->where($this->table.'.branch_code', '<>', '')
                     ->where($this->table.'.department_code', '<>', '');
    }

    public function scopeFilter($query, $company_code = '', $branch_code = '', $dept_code = '')
    {
        if( $company_code != '' )
            $query->where($this->table.'.company_code', $company_code);
        if( $branch_code != '' )
            $query->where($this->table.'.branch_code', $branch_code);
        if( $dept_code != '' )
            $query->where($this->table.'.department_code', $dept_code);

        return $query;
    }

    public function scopeNoDefault($query)
    {
        return $query->where($this->table.'.department_code', '<>', '')
        ->where($this->table.'.branch_code', '<>', '');
    }
    
}
