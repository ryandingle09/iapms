<?php

namespace App\Models;

use App\Traits\Auditable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class CompanyProfile extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable, Auditable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = 'company_profile';
    protected $primaryKey = 'company_id';
    protected $fillable = [
        'company_name',
        'company_code',
        'business_type',
        'group_type',
        'description',
        'business_desc',
        'industry_desc',
        'industry_new',
        'com_env_desc',
        'com_env_new',
        'eco_stab_desc',
        'eco_stab_new',
        'tech_dev_desc',
        'tech_dev_new',
        'growth_pot_desc',
        'growth_pot_new',
        'line_of_bus_desc',
        'line_of_bus_new',
        'products_desc',
        'products_new',
        'vol_of_op_desc',
        'vol_of_op_new',
        'cust_and_market_desc',
        'cust_and_market_new',
        'suppliers_desc',
        'suppliers_new',
        'competitors_desc',
        'competitors_new',
        'fin_rep_and_acct_desc',
        'fin_rep_and_acct_new',
        'taxation_desc',
        'taxation_new',
        'gov_pol_desc',
        'gov_pol_new',
        'legal_req_desc',
        'legal_req_new',
        'key_people_desc',
        'key_people_new',
        'board_of_dir_desc',
        'board_of_dir_new',
        'consultants_desc',
        'consultants_new',
        'third_party_rel_desc',
        'third_party_rel_new',
        'stockholders_desc',
        'stockholders_new',
        'company_fin_desc',
        'company_fin_new',
        'investment_desc',
        'investment_new',
        'obj_and_strat_desc',
        'obj_and_strat_new',
        'fin_aspect_desc',
        'fin_aspect_new',
        'op_aspect_desc',
        'op_aspect_new',
        'it_profile_desc',
        'it_profile_new',
        'created_by',
        'last_update_by',
    ];

    public function auditableEntity()
    {
        return $this->hasMany('App\Models\AuditableEntitiesActual', 'company_code','company_code');
    }

    public function masterEntity()
    {
        return $this->hasMany('App\Models\AuditableEntity', 'company_code','company_code');
    }

    public function branches()
    {
        return $this->hasMany('App\Models\CompanyBranch', 'company_id');
    }
}
