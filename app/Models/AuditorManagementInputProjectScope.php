<?php

namespace App\Models;

use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AuditorManagementInputProjectScope extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'wbs_project_scope_id';
    protected $table = 'wbs_project_scope';

    protected $fillable = [
    	'wbs_project_scope_id',
    	'wbs_header_id',
        'wbs_sequence',
        'iom_number',
        'auditable_entity_id',       
        'ae_mbp_id',         
        'budgeted_mandays',               
        'task_mode',                 
        'predecessor',                              
        'status',                 
        'start_date', 
        'end_date',
        'created_by',   
        'created_date', 
        'last_update_by',   
        'last_update_date'   
    ];

    protected $appends = [
        //'full_name',
    ];

}
