<?php

namespace App\Models;

use App\Traits\Authorable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class PlanProjectScope extends Model implements Transformable
{
    use TransformableTrait, Updater, Authorable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'plan_project_scope_id';
    protected $table = 'plan_project_scope';
    protected $fillable = [
        'plan_project_id',
        'scope_sequence',
        'auditable_entity_id',
        'mbp_id',
        'audit_location',
        'budgeted_mandays',
    ];

    protected $appends = [
        'adjusted_severity_value'
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function($scope){
            $auditors = $scope->project->auditors;

            $auditors->each(function($item) use ($scope){
                $auditor = new \App\Models\PlanProjectScopeAuditor([
                    'auditor_id' => $item->auditor_id
                ]);
                $scope->auditors()->save($auditor);
            });
        });
    }

    public function getAdjustedSeverityValueAttribute($value ){
        $asv_service = app('App\Services\AdjustedSeverityValueService');
        $total_adjusted_severity_value = $asv_service->getAdjustedSeverityValue([
            "auditable_entity_id" => $this->mainBp->auditable_entity_id,
            "ae_mbp_id" => $this->mbp_id
        ]);
        return  $total_adjusted_severity_value;
    }

    public function auditableEntity()
    {
        return $this->belongsTo('App\Models\AuditableEntitiesActual');
    }

    public function mainBp()
    {
        return $this->belongsTo('App\Models\AeMbpActual','mbp_id', 'ae_mbp_id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\PlanProject', 'plan_project_id');
    }

    public function auditor()
    {
        return $this->belongsToMany('App\Models\Auditor', 'plan_project_scope_auditors', 'plan_project_scope_id', 'auditor_id')
            ->withPivot('created_by', 'last_update_by', 'allotted_mandays', 'auditor_type')
            ->withTimestamps();
    }

    public function auditors()
    {
        return $this->hasMany('App\Models\PlanProjectScopeAuditor', 'plan_project_scope_id','plan_project_scope_id');
    }
}
