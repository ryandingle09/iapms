<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class ProjectScopeApg extends Model implements Transformable
{
    use TransformableTrait, Updater;

    protected $primaryKey = 'project_scope_apg_id';
    protected $table = 'project_scope_apg';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
        'project_scope_id',
        'apg_seq',
        'main_bp_name',
        'mbp_bp_seq',
        'bp_name',
        'objective_name',
        'bp_steps_seq',
        'activity_narrative',
        'bp_step_risk_seq',
        'risk_code',
        'control_seq',
        'control_name',
        'control_test_seq',
        'test_proc_id',
        'test_proc_narrative',
        'audit_objective',
        'allotted_mandays',
        'auditor_id',
        'status',
        'enabled_flag',
    ];

    public function auditor()
    {
        return $this->belongsTo('App\Models\Auditor');
    }

    public function controlMaster()
    {
        return $this->belongsTo('App\Models\ControlsMaster','control_id','control_id');
    }

    public function testProc()
    {
        return $this->belongsTo('App\Models\TestProc','test_proc_id','test_proc_id');
    }

    public function scope(){
        return $this->belongsTo('App\Models\ProjectScope','project_scope_id','project_scope_id');
    }

}
