<?php

namespace App\Models;

use App\Traits\Updater;
use App\Traits\Authorable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ProjectScopeFreeze extends Model implements Transformable
{
    use TransformableTrait, Updater, Authorable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = "project_scope_freeze";
    protected $primaryKey = "project_scope_freeze_id";

    protected $fillable = [
		"project_scope_id",
		"freeze_date",
		"effective_date_from",
        "effective_date_to",
        "remarks",
    ];

    protected $appends = [
        "editable",
        "effective_date_to_alt",
		"frozen_by",
    ];

    public function getEditableAttribute(){
        if( strtotime($this->attributes['effective_date_from']) <= time() ){
           return false;
        }
    	return true;
    }

    public function setEffectiveDateFromAttribute($value){
        $this->attributes['effective_date_from'] = ymd($value);
    }

    public function setEffectiveDateToAttribute($value){
        $this->attributes['effective_date_to'] = ymd($value);
    }

    public function getFreezeDateAttribute($value){
        return date_display($value);
    }

    public function getEffectiveDateFromAttribute($value){
        return date_display($value);
    }

    public function getEffectiveDateToAttribute($value){
        return date_display($value);
    }

    public function getEffectiveDateToAltAttribute(){
        $value = $this->attributes['effective_date_to'];
        return $value ? date_display($value) : "TBA" ;
    }

    public function getFrozenByAttribute($value)
    {
        return $this->creator->full_name;
    }

}
