<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Artesaos\Defender\Traits\Models\Role;
use Artesaos\Defender\Contracts\Role as RoleInterface;

class UserRole extends Model implements RoleInterface
{
    use Role;

    protected $table='roles';
}
