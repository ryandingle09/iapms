<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class ProjectScopeApgApproval extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
		'apg_id',
		'approval_seq',
		'status_from',
		'status_to',
		'auditor_id',
		'remarks',
		'role',
		'status_date',
    ];

    public function apg()
    {
        return $this->belongsTo('App\Models\ProjectScopeApg','apg_id','project_scope_apg_id');
    }

}
