<?php

namespace App\Models;

use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class RisksControl extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'risk_control_id';
    protected $table = 'risks_control';
    protected $fillable = [
        'risk_id',
        'risk_control_seq',
        'control_id',
        'created_by',
        'last_update_by',
    ];

    public function risk()
    {
        return $this->belongsTo('App\Models\Risk', 'risk_id');
    }

    public function control()
    {
        return $this->belongsTo('App\Models\ControlsMaster', 'control_id');
    }

    public function scopeJoinRiskDetails($query)
    {
        return $query->select($this->getTable().'.*')
            ->join('risks', 'risks.risk_id', '=', $this->getTable().'.risk_id')
            ->lookupDetails(config('iapms.lookups.risk.risks'), 'lv_risk_code', 'risks.risk_code');
    }
}
