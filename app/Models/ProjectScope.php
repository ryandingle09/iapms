<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class ProjectScope extends Model implements Transformable
{
    use TransformableTrait, Updater;

    protected $primaryKey = 'project_scope_id';
    protected $table = 'project_scope';
    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
		'project_id',
		'scope_sequence',
		'auditable_entity_id',
		'ae_mbp_id',
		'audit_location',
		'target_start_date',
		'target_end_date',
		'actual_start_date',
		'actual_end_date',
		'project_scope_status',
		'plan_project_scope_id',
		'default_misc_mandays',
		'default_apg_mandays'
	];

	protected $appends = [
		'plan_budgeted_mandays',
        'default_total_mandays',
        'allotted_miscellaneous_mandays',
		'allotted_apg_mandays',
		'allotted_total_mandays',
        'adjusted_severity_value',
        'freeze_status',
        'iom_ref_no'
    ];

	public function auditableEntity()
	{
		return $this->belongsTo('App\Models\AuditableEntitiesActual');
	}

	public function mainBp()
	{
		return $this->belongsTo('App\Models\AeMbpActual','ae_mbp_id', 'ae_mbp_id');
	}

	public function project(){
		return $this->belongsTo('App\Models\Project','project_id', 'project_id');
	}

	public function iom(){
		return $this->hasOne('App\Models\ProjectScopeIom','project_scope_id', 'project_scope_id');
	}

	public function auditors()
    {
        return $this->hasMany('App\Models\ProjectScopeAuditor', 'project_scope_id','project_scope_id');
    }

    public function setTargetStartDateAttribute($value)
    {
        $this->attributes['target_start_date'] = ymd($value);
    }

    public function setTargetEndDateAttribute($value)
    {
        $this->attributes['target_end_date'] = ymd($value);
    }

    public function getTargetStartDateAttribute($value)
    {
        return date_display($value);
    }

    public function getTargetEndDateAttribute($value)
    {
        return  date_display($value);
    }

    public function getPlanBudgetedMandaysAttribute()
    {

    	return $this->annualPlanProjectScope ? $this->annualPlanProjectScope->budgeted_mandays : 0;
    }

    public function getDefaultTotalMandaysAttribute()
    {
    	return $this->default_misc_mandays + $this->default_apg_mandays;
    }

    public function getAllottedMiscellaneousMandaysAttribute()
    {
        $mandays = app('App\Services\MandaysService');
    	return $mandays->projectAllottedMiscMandays( $this->project_id, $this->project_scope_id, null);
    }

    public function getAllottedApgMandaysAttribute()
    {
        $mandays = app('App\Services\MandaysService');
        return $mandays->projectAllottedMandaysApg( $this->ae_mbp_id , $this->project->audit_type );
    }

    public function getAllottedTotalMandaysAttribute()
    {
    	return $this->allotted_miscellaneous_mandays + $this->allotted_apg_mandays;
    }

    public function getAdjustedSeverityValueAttribute($value ){
        $asv_service = app('\App\Services\AdjustedSeverityValueService');
        $asv_service->getAdjustedSeverityValue([
            "auditable_entity_id" => $this->auditable_entity_id,
            "ae_mbp_id" => $this->ae_mbp_id,
            "year" => $this->project->audit_year
        ]);
        return 0;
    }

    public function getIomRefNoAttribute($value ){
        if($this->iom){
            return $this->iom->ref_no;
        }
        return null;
    }

    public function annualPlanProjectScope()
    {
        return $this->hasOne('App\Models\PlanProjectScope','plan_project_scope_id','plan_project_scope_id');
    }

    public function freeze()
    {
        return $this->hasMany('App\Models\ProjectScopeFreeze','project_scope_id','project_scope_id');
    }

    public function getFreezeStatusAttribute()
    {
        $count = \DB::table('project_scope_freeze')
                ->where(function($q){
                    $q->where('project_scope_id',$this->project_scope_id)
                    ->where('effective_date_from','<=', date('Y-m-d') )
                    ->where('effective_date_to','>=', date('Y-m-d') );
                })
                ->orWhere(function($q){
                    $q->where('project_scope_id',$this->project_scope_id)
                    ->where('effective_date_from','<=', date('Y-m-d') )
                    ->whereNull('effective_date_to');
                })
                ->count();
        return $count ? "Frozen" : "Not Frozen";
    }

}
