<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Matrix extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    const RISK_INDEX = 'Risk Index';
    const SEVERITY_WEIGHT = 'Severity Weight';
    const PAST_AUDIT_INDEX = 'Past Audit Index';

    protected $table = 'matrix';
    protected $primaryKey = 'matrix_id';
    protected $fillable = [
    	'matrix_name',
    	'description',
    	'x_dimension',
    	'y_dimension',
    	'created_by',
    	'last_update_by',
    ];

    public function details()
    {
    	return $this->hasMany('App\Models\MatrixDetail', 'matrix_id');
    }

    public static function scopeRiskIndex($query, $x , $y){
        return $query->join('matrix_details','matrix_details.matrix_id','=','matrix.matrix_id')
                     ->where('x_sequence' , $x)
                     ->where('y_sequence' , $y)
                     ->select('matrix_value','color')
                     ->first();
    }
}
