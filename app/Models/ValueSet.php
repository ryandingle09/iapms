<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ValueSet extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'value_set_id';

    protected $fillable = [
    	'value_set_name',
    	'description',
    	'code',
    	'meaning',
    	'from_clause',
    	'where_clause',
    	'created_by',
    	'last_update_by',
    ];

    public function infoDetails()
    {
        return $this->hasMany('App\Models\AdditionalInfoDetail', 'value_set_id');
    }
}
