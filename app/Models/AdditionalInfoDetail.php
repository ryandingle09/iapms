<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AdditionalInfoDetail extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
        'additional_info_id',
        'additional_info_seq',
        'prompt',
        'data_type',
        'attribute_assignment',
        'value_set_id',
        'required',
        'remarks',
        'created_by',
        'last_update_by',
    ];

    public function master()
    {
        return $this->belongsTo('App\Models\AdditionalInfoMaster', 'additional_info_id');
    }

    public function valueSet()
    {
        return $this->belongsTo('App\Models\ValueSet', 'value_set_id');
    }
}
