<?php

namespace App\Models;

use App\Traits\Auditable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class RiskAssessmentScale extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable, Auditable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = 'risk_assessment_scale';
    protected $primaryKey = "risk_assessment_scale_id";

    protected $fillable = [
        'auditable_entity_id',
        'ae_mbp_id',
        'bp_id',
        'bp_objective_id',
        'bp_steps_id',
        'risk_id',
        'inherent_impact_value',
        'inherent_likelihood_value',
        'inherent_remarks',
        'residual_impact_value',
        'residual_likelihood_value',
        'residual_remarks',
        'type'
    ];

    protected $appends = [
        // 'risk_score',
        // 'risk_index'
        // 'severity_weight',
        // 'Severity Value ',
        // 'time_last_audit',
        // 'adjusted_severity_value'
    ];

    public function auditableEntity()
    {
        return $this->belongsTo('App\Models\AuditableEntity');
    }

    public function auditableEntityActual()
    {
        return $this->belongsTo('App\Models\AuditableEntitiesActual', 'auditable_entity_id');
    }

    public function mbpa(){
        return $this->belongsTo('App\Models\AeMbpActual','ae_mbp_id','ae_mbp_id');
    }

    public function mbp(){
        return $this->belongsTo('App\Models\AeMbp','ae_mbp_id','ae_mbp_id');
    }

    public function getRiskScoreAttribute(){
        return $this->inherent_impact_value * $this->inherent_likelihood_value;
    }

    public function getRiskIndexAttributes(){
        $risk_index = \App\Models\Matrix::riskIndex( $this->inherent_impact_value , $this->inherent_likelihood_value);
        return $risk_index->matrix_value;
    }

    public function scopeWithFullAttributes($query){
        return $query;
    }

    public function businessProcess()
    {
        return $this->belongsTo('App\Models\BusinessProcess', 'bp_id');
    }

    public function bpObjective()
    {
        return $this->belongsTo('App\Models\BpObjective', 'bp_objective_id','bp_objective_id');
    }

    public function risk()
    {
        return $this->belongsTo('App\Models\Risk')->withLookupDetails();
    }

    public function scopeWhereEntity($query, $auditable_entity_id)
    {
        return $query->where('auditable_entity_id', $auditable_entity_id);
    }

    public function scopeGroupByEntityBpRisk($query)
    {
        return $query->groupBy('risk_assessment_scale.auditable_entity_id', 'r.risk_id', 'risk_assessment_scale.bp_id');
    }

    public function scopeJoinEntity($query, $type = '')
    {
        $entity_table = 'auditable_entities'.($type == 'actual' ? '_'.$type : '');
        return $query->rightJoin($entity_table.' AS ae', 'ae.auditable_entity_id', '=', 'risk_assessment_scale.auditable_entity_id');
    }

    public function scopeJoinBp($query)
    {
        return $query->rightJoin('business_processes AS bp', 'bp.bp_id', '=', 'risk_assessment_scale.bp_id')
                     ->lookupDetails(config('iapms.lookups.business_process'), 'lv_bp_code', 'bp_code');
    }

    public function scopeJoinSbp($query)
    {
        return $query->join('ae_mbp_sbp', 'ae_mbp_sbp.bp_id', '=', 'risk_assessment_scale.bp_id');
    }

    public function scopeJoinRisk($query)
    {
        return $query->addSelect('enterprise_risk_details.rating_comment as ent_rating_comment', 'r.risk_type as r_risk_type')
                     ->join('risks AS r', function ($j){
                         $j->on( 'r.risk_id', '=', 'bpsr.risk_id' )
                           ->on( 'r.risk_id', '=', 'risk_assessment_scale.risk_id' );
                     })
                     ->lookupDetails(config('iapms.lookups.risk.risks'), 'lv_risk_code', 'risk_code')
                     ->leftJoin('enterprise_risk_details', 'enterprise_risk_details.enterprise_risk_id', '=', 'r.enterprise_risk_id');
    }

    public function scopeWhereType($query, $type)
    {
        return $query->where('type', $type);
    }


}
