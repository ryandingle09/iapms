<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class PlanProjectScopeAuditor extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    
    protected $primaryKey = 'plan_project_scope_auditor_id';
    protected $table = 'plan_project_scope_auditors';
    protected $fillable = [
        'plan_project_scope_id',
        'auditor_id',
        'auditor_type',
        'allotted_mandays',
    ];

    protected $appends = [
        'annual_mandays',
        'running_mandays',
        'available_mandays',
    ];

    public function getAnnualMandaysAttribute(){
        $plan_auditors = $this->scope->project->group->plan->planAuditors;

        if($plan_auditors){
            $auditor = $plan_auditors->where('auditor_id',$this->auditor_id)->first();
            if($auditor){
                return $auditor->annual_mandays;
            }
        }
        return 0;
    }
    
    public function getRunningMandaysAttribute(){
        $mandays = app('App\Services\MandaysService');
        return $mandays->calculateAuditorProjectMandays(
            $this->scope->project->group->plan->plan_id,
            null,
            null,
            $this->auditor_id
        );
    }

    public function getAvailableMandaysAttribute(){
        return $this->annual_mandays - $this->running_mandays;
    }

    public function auditor()
    {
        return $this->belongsTo('App\Models\Auditor');
    }

    public function scope(){
        return $this->belongsTo('App\Models\PlanProjectScope','plan_project_scope_id','plan_project_scope_id');
    }
}
