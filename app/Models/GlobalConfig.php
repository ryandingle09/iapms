<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;

class GlobalConfig extends Model
{
	use  Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    public $incrementing = false;
    protected $table = 'global_config';
    protected $primaryKey = null;
    protected $fillable = [
        'auditor_mandays_factor_rate',
        'auditor_timeliness',
        'auditor_quality',
        'auditor_documentation',
        'auditor_caf',
        'reviewer_timeliness',
        'reviewer_quality',
        'reviewer_documentation',
		'iom_auditee_assist_narrative',
    ];
}
