<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class PlanAuditor extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = "plan_auditor_id";
    protected $fillable = [
    	'plan_id',
    	'auditor_id',
        // 'auditor_type',
        'annual_mandays'
    ];

    protected $appends = [
        'allotted_mandays',
        'running_mandays',
        'available_mandays',
    ];

    public static function boot()
    {
        parent::boot();

        static::deleted(function($model)
        {
            $auditor_id = $model->auditor_id;

            $model->groups->each(function($group) use ($auditor_id){
                $group->auditors()->where('auditor_id', $auditor_id )->delete();  
                $group->projects()->each(function($project) use ($auditor_id) {
                    $project->auditors()->where('auditor_id', $auditor_id )->delete();  
                    $project->scopes->each(function($scope) use ($auditor_id){
                        $scope->auditors()->where('auditor_id', $auditor_id )->delete();  
                    });
                });
            });
        });
    }

    public function groups()
    {
        return $this->hasMany('App\Models\PlanGroup','plan_id','plan_id');
    }

    public function auditor()
    {
        return $this->belongsTo('App\Models\Auditor');
    }

    public function getAllottedMandaysAttribute(){

        $mandays = app('App\Services\MandaysService');
        return $mandays->calculateAuditorProjectMandays(
            $this->plan_id,
            null,
            null,
            $this->auditor_id 
        );
    }

    public function getAnnualMandaysAttribute($value){
        return $value ? : 0;
    }

    public function getRunningMandaysAttribute(){
        $mandays = app('App\Services\MandaysService');
        return $mandays->calculateAuditorProjectMandays(
            $this->plan_id,
            null,
            null,
            $this->auditor_id
        );
    }

    public function getAvailableMandaysAttribute(){
        return $this->annual_mandays - $this->running_mandays;
    }

    public function plan()
    {
        return $this->belongsTo('App\Models\Plan');
    }

    public function projects()
    {
        return $this->hasMany('App\Models\PlanProject','plan_id','plan_id');
    }
}
