<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class ProjectScopeDarSof extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';

    const UPDATED_AT = 'last_update_date';

    protected $table = "project_scope_dar_sof";

    protected $primaryKey = 'project_scope_dar_sof_id';

    protected $fillable = [
	    'project_scope_id',
	    'findings_seq',
	    'business_process_step',
	    'risk',
	    'recommendations',
	    'auditee_actions_taken'
    ];

}
