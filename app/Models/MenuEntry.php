<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class MenuEntry extends Model implements Transformable
{
    use TransformableTrait;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = null;
    public $incrementing = false;

    protected $fillable = [
    	'menu_id',
        'permission_id',
    ];

    public function menu()
    {
    	return $this->belongsTo('App\Models\Menu');
    }
}
