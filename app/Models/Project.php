<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class Project extends Model implements Transformable
{
   	use TransformableTrait, Updater;

 	protected $primaryKey = 'project_id';

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
    	'project_name',
    	'audit_type',
    	'project_status',
    	'target_start_date',
    	'target_end_date',
    	'project_remarks',
    	'project_head',
    	'reviewed_by',
    	'reviewed_date',
    	'reviewer_remarks',
    	'approved_by',
    	'approved_date',
    	'approver_remarks',
    	'plan_project_id',
        'audit_year',
    ];

    protected $appends = [
        'plan_budgeted_mandays',
        'allotted_miscellaneous_mandays',
        'allotted_apg_mandays',
        'allotted_total_mandays',
    ];

    public function scopes()
    {
        return $this->hasMany('App\Models\ProjectScope', 'project_id');
    }

    public function reviewer()
    {
        return $this->hasOne('App\User','user_id','approved_by');
    }

    public function approver()
    {
        return $this->hasOne('App\User','user_id','approved_by');
    }

    public function setTargetStartDateAttribute($value)
    {
        $this->attributes['target_start_date'] = date('Y-m-d', strtotime($value));
    }

    public function setTargetEndDateAttribute($value)
    {
        $this->attributes['target_end_date'] = date('Y-m-d', strtotime($value));
    }

    public function getTargetStartDateAttribute($value)
    {
        return date_display($value);
    }

    public function getTargetEndDateAttribute($value)
    {
        return date_display($value);
    }

    public function getReviewedDateAttribute($value)
    {
        return $value ? date('d-M-Y',strtotime($value)) : $value;
    }

    public function getApprovedDateAttribute($value)
    {
        return $value ? date('d-M-Y',strtotime($value)) : $value;
    }

    public function auditors()
    {
        return $this->hasMany('App\Models\ProjectAuditor','project_id','project_id');
    }

    public function getPlanBudgetedMandaysAttribute()
    {
        if($this->annualPlanProject){
            $plan_project = $this->annualPlanProject;
            $mandays = app('App\Services\MandaysService');
            return $mandays->calculateAuditorProjectMandays( $plan_project->plan_id, $plan_project->plan_project_id, null );
        }else{
            return 0;
        }
    }

    public function getAllottedMiscellaneousMandaysAttribute()
    {
        $mandays = app('App\Services\MandaysService');
        return $mandays->projectAllottedMiscMandays( $this->project_id , null, null );
    }

    public function getAllottedApgMandaysAttribute()
    {
        $mandays = app('App\Services\MandaysService');
        return $mandays->projectAllottedMandaysApg( $this->project_id , null, null );
    }

    public function getAllottedTotalMandaysAttribute()
    {
        return  $this->allotted_miscellaneous_mandays + $this->allotted_apg_mandays;
    }

    public function annualPlanProject()
    {
        return $this->hasOne('App\Models\PlanProject','plan_project_id','plan_project_id');
    }
}
