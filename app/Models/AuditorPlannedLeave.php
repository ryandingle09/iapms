<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;
use Carbon\Carbon;

class AuditorPlannedLeave extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    public $incrementing = false;

    protected $fillable = [
		'auditor_id',
		'leave_date_from',
		'leave_date_to',
		'leave_type',
		'remarks',
    ];

    protected $appends = [
        'total_leave'
    ];

    public function auditor()
    {
    	return $this->belongsTo('App\Models\Auditor');
    }

	public function setLeaveDateFromAttribute($value)
    {
        $this->attributes['leave_date_from'] = date('Y-m-d', strtotime($value) );
    }

    public function getTotalLeaveAttribute($value)
    {
        $start = Carbon::parse($this->attributes['leave_date_from']);
        $end = Carbon::parse($this->attributes['leave_date_to']);
        return $start->diffInWeekdays($end);
    }

    public function getLeaveDateFromAttribute($value)
    {
        return date( config('app.date_display') , strtotime($value) );
    }

	public function setLeaveDateToAttribute($value)
    {
        $this->attributes['leave_date_to'] = date('Y-m-d', strtotime($value) );
    }

    public function getLeaveDateToAttribute($value)
    {
        return date( config('app.date_display') , strtotime($value) );
    }

    public function scopeByYear($query, $year)
    {
        return $query->where(\DB::raw('YEAR(leave_date_from)'), $year)
                     ->where(\DB::raw('YEAR(leave_date_to)'), $year);
    }

}
