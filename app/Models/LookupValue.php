<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class LookupValue extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    public $incrementing = false;

    protected $primaryKey = null;
    protected $fillable = [
        'lookup_type_id',
        'lookup_type',
        'lookup_code',
        'meaning',
        'description',
        'created_by',
        'last_update_by',
        'effective_start_date',
        'effective_end_date',
    ];

    public function lookupType()
    {
        return $this->belongsTo('App\Models\LookupType');
    }

    public function creator()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function updater()
    {
        return $this->belongsTo('App\User', 'last_update_by');
    }

    public function setEffectiveStartDateAttribute($value)
    {
        $this->attributes['effective_start_date'] = date('Y-m-d', strtotime($value) );
    }

    public function setEffectiveEndDateAttribute($value)
    {
        $this->attributes['effective_end_date'] = date('Y-m-d', strtotime($value) );
    }

    public function getEffectiveStartDateAttribute($value)
    {
        return strtotime($value) > 0 ? date('m-d-Y', strtotime($value) ) : '';
    }

    public function getEffectiveEndDateAttribute($value)
    {
        return strtotime($value) > 0 ? date('m-d-Y', strtotime($value) ) : '';
    }

    public function child()
    {
        return $this->hasMany('App\Models\LookupValue', 'lookup_type', 'lookup_code');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\LookupValue', 'lookup_code', 'lookup_type');
    }
}
