<?php

namespace App\Models;

use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AuditorTraining extends Model implements Transformable
{
    use TransformableTrait, Lookupable, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'auditor_training_id';
    protected $table = 'auditor_training';
    protected $fillable = [
        'auditor_id',
        'training_type',
        'description',
        'venue',
        'completion_date',
    ];

    public function auditor()
    {
        return $this->belongsTo('App\Models\Auditor', 'auditor_id');
    }

    public function setCompletionDateAttribute($value)
    {
        $this->attributes['completion_date'] = date('Y-m-d', strtotime($value) );
    }

    public function getCompletionDateAttribute($value)
    {
        return $value != '' && $value != '0000-00-00' ? date( config('app.date_display'), strtotime($value) ) : '';
    }
}
