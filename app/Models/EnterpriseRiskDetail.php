<?php

namespace App\Models;

use App\Traits\Authorable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class EnterpriseRiskDetail extends Model implements Transformable
{
    use TransformableTrait, Updater, Authorable, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'enterprise_risk_det_id';

    protected $fillable = [
        'auditable_entity_id',
        'risk_name',
        'risk_desc',
        'risk_remarks',
        'risk_type',
        'risk_class',
        'risk_area',
        'response_type',
        'response_status',
        'response_status_comment',
        'inherent_impact_rate',
        'inherent_likelihood_rate',
        'residual_impact_rate',
        'residual_likelihood_rate',
        'rating_comment',
        'supporting_docs_name',
        'supporting_docs_file',
        'rated_by',
        'rated_date',
    ];

    public function enterpriseRisk()
    {
        return $this->belongsTo('App\Models\EnterpriseRisk');
    }

    /**
     * Risk relationship
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function risk()
    {
        return $this->belongsTo('App\Models\Risk', 'enterprise_risk_id');
    }

    public function setRatedDateAttribute($value)
    {
        $this->attributes['rated_date'] = $value ? date('Y-m-d', strtotime($value)) : null;
    }

    public function getRatedDateAttribute($value)
    {
        return !is_null($value) ? date('d-F-Y', strtotime($value)) : '';
    }
}
