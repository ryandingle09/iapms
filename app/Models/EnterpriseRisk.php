<?php

namespace App\Models;

use App\Traits\Auditable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class EnterpriseRisk extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable, Auditable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'enterprise_risk_id';

    protected $fillable = [
        'auditable_entity_id',
        'ae_mbp_id',
        'bp_id',
        'created_by',
        'last_update_by',
    ];

    public function details()
    {
        return $this->hasOne('App\Models\EnterpriseRiskDetail');
    }

    public function auditableEntity()
    {
        return $this->belongsTo('App\Models\AuditableEntitiesActual');
    }

    public function businessProcess()
    {
        return $this->belongsTo('App\Models\BusinessProcess', 'bp_id');
    }

    public function mainBp()
    {
        return $this->belongsTo('App\Models\AeMbp', 'ae_mbp_id');
    }

    public function risk()
    {
        return $this->hasMany('App\Models\Risk', 'enterprise_risk_id');
    }

    public function scopeAuditeeOwner($query)
    {
        return $query->where('created_by', \Auth::user()->user_id);
    }
}
