<?php

namespace App\Models;

use App\Traits\Authorable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class BpStepsRisk extends Model implements Transformable
{
    use TransformableTrait, Updater, Authorable, Lookupable;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    public $incrementing = false;

    protected $primaryKey = 'null';
    protected $fillable = [
        'bp_step_id',
        'risk_id',
        'created_by',
        'last_update_by',
    ];

    public function step()
    {
        return $this->belongsTo('App\Models\BusinessProcessesStep', 'bp_step_id');
    }

    public function risk()
    {
        return $this->belongsTo('App\Models\Risk');
    }

    public function controls()
    {
        return $this->belongsTo('App\Models\RisksControl', 'risk_id', 'risk_id');
    }

    public function scopeJoinRiskDetails($query)
    {
        return $query->select($this->getTable().'.*', 'risks.*')
                     ->join('risks', 'risks.risk_id', '=', $this->getTable().'.risk_id')
                     ->lookupDetails(config('iapms.lookups.risk.risks'), 'lv_risk_code', 'risks.risk_code');
    }
}
