<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ApprovalSetupDetail extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $table = "approval_setup_details";
    protected $primaryKey = "approval_setup_detail_id";

    protected $fillable = [
		'approval_setup_master_id',
		'approval_sequence',
		'auditor_type',
		'approver_flag',
    ];

}
