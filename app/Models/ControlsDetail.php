<?php

namespace App\Models;

use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ControlsDetail extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'control_det_id';
    protected $table = 'controls_detail';
    protected $fillable = [
    	'control_seq',
    	'control_code',
    	'created_by',
    	'last_update_by',
    ];

    public function master()
    {
    	return $this->belongsTo('App\Models\ControlsMaster', 'control_id');
    }

    public function testProcedures()
    {
    	return $this->hasMany('App\Models\ControlsTestProc', 'control_det_id');
    }
}
