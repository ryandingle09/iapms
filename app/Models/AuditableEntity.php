<?php

namespace App\Models;

use App\Traits\Auditable;
use App\Traits\Authorable;
use App\Traits\Lookupable;
use App\Traits\Updater;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AuditableEntity extends Model implements Transformable
{
    use TransformableTrait, Updater, Lookupable, Authorable, Auditable;

    const CREATED_AT = 'created_date';
    protected $fillable = [
        'auditable_entity_name',
        'company_code',
        'branch_code',
        'department_code',
        'contact_name',
        'entity_head_name',
        'business_type',
        'group_type',
        'entity_class',
        'attribute1',
        'attribute2',
        'attribute3',
        'attribute4',
        'attribute5',
        'attribute6',
        'attribute7',
        'attribute8',
        'attribute9',
        'attribute10',
        'created_by',
        'last_update_by',
    ];

    const UPDATED_AT = 'last_update_date';

    protected $primaryKey = 'auditable_entity_id';

    public function businessProcess()
    {
        return $this->belongsToMany('App\Models\BusinessProcess', 'auditable_entities_bp', 'auditable_entity_id', 'bp_id')
                    ->withPivot([
                        'created_by',
                        'last_update_by',
                    ])
                    ->withTimestamps();
    }

    public function bps()
    {
        return $this->hasMany('App\Models\AuditableEntitiesBp', 'auditable_entity_id');
    }

    public function riskAssessment()
    {
        return $this->hasMany('App\Models\RiskAssessmentScale');
    }

    public function riskAssessmentScale()
    {
        return $this->belongsToMany('App\Models\BusinessProcess', 'risk_assessment_scale', 'auditable_entity_id', 'bp_id');
    }

    public function enterpriseRisk()
    {
        return $this->hasMany('App\Models\EnterpriseRisk');
    }

    public function projectScope()
    {
        return $this->hasMany('App\Models\PlanProjectScope', 'auditable_entity_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Models\CompanyProfile', 'company_code', 'company_code');
    }

    public function masterBp()
    {
        return $this->hasMany('App\Models\AeMbp', 'auditable_entity_id');
    }

    public function subBp()
    {
        return $this->hasManyThrough('App\Models\AeMbpSbp', 'App\Models\AeMbp', 'auditable_entity_id', 'ae_mbp_id', 'auditable_entity_id');
    }

    public function actual()
    {
        return $this->hasMany('App\Models\AuditableEntitiesActual', 'master_auditable_entity_id');
    }

    /**
     * To get related entity(master) by group_type, business_type, entity_class, department_code
     * @param $query
     * @param $group
     * @param $business
     * @param $entity
     * @param $branch
     * @param $dept
     * @return mixed
     */
    public function scopeRelatedEntity($query, $group, $business, $entity, $branch, $dept)
    {
        $branch = $branch != '' ? $entity : $branch;
        return $query->where('group_type', $group)
        ->where('company_code', $business) // company code is the code of business type for master
        ->where('entity_class', $entity)
        ->where('branch_code', $branch)
        ->where('department_code', $dept);
    }
}
