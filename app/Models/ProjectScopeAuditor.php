<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use App\Traits\Updater;

class ProjectScopeAuditor extends Model implements Transformable
{
    use TransformableTrait, Updater;

    const CREATED_AT = 'created_date';
    const UPDATED_AT = 'last_update_date';

    protected $fillable = [
        'project_scope_id',
        'auditor_id',
        'auditor_type',
        'allotted_misc_mandays'
    ];

    protected $appends = [
        'plan_budgeted_mandays',
        'allotted_apg_mandays',
        'allotted_total_mandays'
    ];

    public function getPlanBudgetedMandaysAttribute()
    {
        if($this->scope->annualPlanProjectScope){
            $auditor = $this->scope->annualPlanProjectScope->auditors->where('auditor_id',$this->auditor_id)->first();
            if($auditor){
                return $auditor->allotted_mandays;
            }
        }
        return 0;
    }

    public function getAllottedApgMandaysAttribute()
    {
        $mandays = app('App\Services\MandaysService');
        return $mandays->projectAllottedMandaysApg( $this->project_id , $this->project_scope_id , $this->auditor_id );

    }

    public function getAllottedTotalMandaysAttribute()
    {
        return $this->allotted_misc_mandays + $this->allotted_apg_mandays;
    }

    public function auditor()
    {
    	return $this->belongsTo('App\Models\Auditor');
    }

    public function scope(){
        return $this->belongsTo('App\Models\ProjectScope','project_scope_id','project_scope_id');
    }

}
