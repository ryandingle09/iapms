<?php

namespace App\Listeners;

use App\Events\AuditableEntityWasCreated;
use App\Repositories\AeMbpSbpRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class CreateRiskAssessmentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AuditableEntityWasCreated  $event
     * @return void
     */
    public function handle(AuditableEntityWasCreated $event)
    {
        Log::info('[Risk Assessment Scale] Event started.');

        $entity = $event->auditable_entity;
        if( !is_null($entity) ) {
            $sbp_repository = app(AeMbpSbpRepository::class);
            $sbp_repository->generateRiskAssessmentScale($entity, $event->type);
        }
    }
}
