<?php

namespace App\Listeners;

use App\Models\AuditableEntity;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class DeleteInRiskAssessmentListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param   $event
     * @return void
     */
    public function handle($event)
    {
        // check if main bp has been deleted
        if( property_exists($event, 'main_bp') ) {
            $entity = $event->main_bp->auditableEntity;
            $bps = $event->main_bp->subBp;

            if( !is_null($bps) && $bps->count() ) {
                foreach ($bps as $bp) {
                    $this->execute($entity, $bp->bp_id);
                }
            }
        }
        else {
            // sub bp
            $this->execute($event->entity, $event->bp);
        }
    }

    /**
     * Execute delete on risk assesement
     * @param AuditableEntity $entity
     * @param $bp
     */
    private function execute(AuditableEntity $entity, $bp) {
        $entity->riskAssessment()->where('bp_id', $bp)->delete();

        Log::info('[Event::DeleteInRiskAssessmentListener] Risk assessment deleted. BP ID - '.$bp);
    }
}
