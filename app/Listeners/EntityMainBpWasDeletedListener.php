<?php

namespace App\Listeners;

use App\Events\EntityMainBpWasDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class EntityMainBpWasDeletedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntityMainBpWasDeleted  $event
     * @return void
     */
    public function handle(EntityMainBpWasDeleted $event)
    {
        Log::info('[Event::EntityMainBpWasDeleted] Event initiated.');

        $main_bp = app('App\Repositories\AeMbpRepository');
        $main_bp->applyToActualEntities($event->main_bp, true);
    }
}
