<?php

namespace App\Listeners;

use App\Events\NewPermissionCreated;
use App\Models\Menu;
use App\Services\MenuService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Log;

class NewPermissionCreatedListener
{
    private $menu_service;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(MenuService $menu_service)
    {
        $this->menu_service = $menu_service;
    }

    /**
     * Handle the event.
     *
     * @param  NewPermissionCreated  $event
     * @return void
     */
    public function handle(NewPermissionCreated $event)
    {
        Log::info('New permission: '. $event->permission->name);

        /**
         *
         * Route deifinition
         * @var $route - [parent].[nth sub menu (optional)].[action]
         *
         */
        $route = explode('.', $event->permission->name);

        $this->menu_service->saveMenu($route);
    }
}
