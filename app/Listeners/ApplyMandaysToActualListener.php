<?php

namespace App\Listeners;

use App\Events\MainBpMandayWasUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class ApplyMandaysToActualListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MainBpMandayWasUpdated  $event
     * @return void
     */
    public function handle(MainBpMandayWasUpdated $event)
    {
        $main_bp = $event->main_bp;
        $actuals = $main_bp->actual;
        if( !is_null($actuals) || $actuals->count() ) {
            Log::info('[Event::MainBpMandayWasUpdated] Event initiated.');
            foreach ($actuals as $actual) {
                $actual->update($event->mandays);
            }
        }
    }
}
