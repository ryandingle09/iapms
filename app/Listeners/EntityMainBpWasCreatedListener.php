<?php

namespace App\Listeners;

use App\Events\EntityMainBpWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class EntityMainBpWasCreatedListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntityMainBpWasCreated  $event
     * @return void
     */
    public function handle(EntityMainBpWasCreated $event)
    {
        Log::info('[Event::EntityMainBpWasCreated] Event initiated.');

        $main_bp = app('App\Repositories\AeMbpRepository');
        $main_bp->applyToActualEntities($event->main_bp);
    }
}
