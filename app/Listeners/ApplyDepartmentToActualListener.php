<?php

namespace App\Listeners;

use App\Events\MasterEntityDepartmentWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class ApplyDepartmentToActualListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event when new master entity department is created.
     *
     * @param  MasterEntityDepartmentWasCreated  $event
     * @return void
     */
    public function handle(MasterEntityDepartmentWasCreated $event)
    {
        $entity = $event->entity;
        if( $entity->department_code != '' ) {
            Log::info('[Event::MasterEntityDepartmentWasCreated] Event initiated for department - '.$entity->department_code);

            // initiate repositories
            $actual_repo = app('App\Repositories\AuditableEntitiesActualRepository');
            $branch_repo = app('App\Repositories\CompanyBranchRepository');

            $companies = $branch_repo->getMasterRelatedCompanyBranches($entity);
            if( $companies->count() ) $actual_repo->createActualFromMaster($entity, $companies);
        }
    }
}
