<?php

namespace App\Listeners;

use App\Events\AuditableEntityWasCreated;
use App\Models\AuditableEntitiesActual;
use App\Repositories\AuditableEntitiesActualRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class ApplyMainBpListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event when actual entity has been create.
     * This will attach Main Business Process according to
     * its group_type, business_type and entity_class
     *
     * @param  AuditableEntityWasCreated  $event
     * @return mixed
     */
    public function handle(AuditableEntityWasCreated $event)
    {
        $entity = $event->auditable_entity;
        // check if actual entity
        if( !$entity instanceof AuditableEntitiesActual) return false;

        Log::info('[Listener::ApplyMainBpListener] Event initiated.');

        if( !is_null($entity) ) {
            $sbp_repository = app(AuditableEntitiesActualRepository::class);
            $sbp_repository->applyMasterBp($entity, $event->type);
        }
    }
}
