<?php

namespace App\Listeners;

use App\Events\EntityMainBpWasCreated;
use App\Events\EntitySubBpWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class SyncToRiskAssessmentListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntitySubBpWasCreated  $event
     * @return void
     */
    public function handle(EntitySubBpWasCreated $event)
    {
        Log::info('[Event::SyncToRiskAssessmentListener] Event initiated.');

        $main_bp = app('App\Repositories\AeMbpSbpRepository');
        $main_bp->generateRiskAssessmentScale($event->auditable_entity, $event->type);

        // generate actual risk assessment
        if( $event->type == 'master') {
            $actuals = $event->auditable_entity->actual;
            if( $actuals->count() ) {
                foreach ($actuals as $actual) {
                    $main_bp->generateRiskAssessmentScale($actual, 'actual');
                }
            }
        }
    }
}
