<?php

namespace App\Listeners;

use App\Events\EntitySubBpWasDeleted;
use App\Models\AeMbpSbp;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class DeleteSubBpInMainBpListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EntitySubBpWasDeleted  $event
     * @return void
     */
    public function handle(EntitySubBpWasDeleted $event)
    {
        $subbp = $event->entity
        ->subBp()
        ->where('bp_id', $event->bp)
        ->delete();

        Log::info('[Listener::DeleteSubBpInMainBpListener] Event initiated.');
    }
}
