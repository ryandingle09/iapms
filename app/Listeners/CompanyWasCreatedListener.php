<?php

namespace App\Listeners;

use App\Events\CompanyWasCreated;
use App\Repositories\CompanyBranchRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Log;

class CompanyWasCreatedListener
{
    private $company_branch;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(CompanyBranchRepository $company_branch)
    {
        $this->company_branch = $company_branch;
    }

    /**
     * Handle the event.
     *
     * @param  CompanyWasCreated  $event
     * @return void
     */
    public function handle(CompanyWasCreated $event)
    {
        $this->company_branch->createDefaultBranch($event->company);
        Log::info('Company was created. Default branch is created.');
    }
}
