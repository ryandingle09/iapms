<?php

namespace App\Transformers;

use App\Models\BusinessProcess;
use App\Transformers\BpObjectivesTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class BusinessProcessTransformer
 * @package namespace App\Transformers;
 */
class BusinessProcessTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'objectives',
    ];

    /**
     * Transform the \BusinessProcess entity
     * @param \BusinessProcess $model
     *
     * @return array
     */
    public function transform(BusinessProcess $model)
    {
        return [
            'id'            => (int) $model->bp_id,
            'name'          => $model->bp_name,
            'code'          => $model->bp_code,
            'code_desc'     => $model->bp_code_desc,
            'obj_cat'       => $model->objective_category,
            'obj_cat_desc'  => $model->objective_category_desc,
            // 'obj_type'      => $model->objective_type,
            // 'obj_type_desc' => $model->lv_objective_type_desc,
            'source_type'      => $model->source_type,
            'source_type_desc' => $model->source_type_desc,
            'cm_1_name'        => $model->cm_1_name,
            'cm_1_high_score_type'  => $model->cm_1_high_score_type,
            'cm_2_name'        => $model->cm_2_name,
            'cm_2_high_score_type'  => $model->cm_2_high_score_type,
            'cm_3_name'        => $model->cm_3_name,
            'cm_3_high_score_type'  => $model->cm_3_high_score_type,
            'cm_4_name'        => $model->cm_4_name,
            'cm_4_high_score_type'  => $model->cm_4_high_score_type,
            'cm_5_name'        => $model->cm_5_name,
            'cm_5_high_score_type'  => $model->cm_5_high_score_type,
        ];
    }

    public function objectives(BusinessProcess $bps)
    {
        $collection = $bps->bpObjectives;
        return $this->collection($collection, new BpObjectivesTransformer);
    }
}
