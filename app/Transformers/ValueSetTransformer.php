<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ValueSet;

/**
 * Class ValueSetTransformer
 * @package namespace App\Transformers;
 */
class ValueSetTransformer extends TransformerAbstract
{

    /**
     * Transform the \ValueSet entity
     * @param \ValueSet $model
     *
     * @return array
     */
    public function transform(ValueSet $model)
    {
        return [
            'id'         => (int) $model->value_set_id,
            'text'       => $model->value_set_name,
        ];
    }
}
