<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;

/**
 * Class AuditorSelectizePresenterTransformer
 * @package namespace App\Transformers;
 */
class AuditorSelectizePresenterTransformer extends TransformerAbstract
{

    /**
     * Transform the \AuditorSelectizePresenter entity
     * @param \AuditorSelectizePresenter $model
     *
     * @return array
     */
    public function transform(AuditorSelectizePresenter $model)
    {
        $auditor = $model->auditor;
        return [
            'id'         => (int) $auditor->auditor_id,
            'text'       => $auditor->first_name.' '.$auditor->middle_name.' '.$auditor->last_name,
            'position'   => $auditor->position_code,
        ];
    }
}
