<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ProjectAuditor;

/**
 * Class ProjectAuditorTransformer
 * @package namespace App\Transformers;
 */
class ProjectAuditorTransformer extends TransformerAbstract
{

    /**
     * Transform the \ProjectAuditor entity
     * @param \ProjectAuditor $model
     *
     * @return array
     */
    public function transform(ProjectAuditor $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
