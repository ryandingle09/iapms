<?php

namespace App\Transformers;

use App\Models\Auditor;
use League\Fractal\TransformerAbstract;

/**
 * Class PlanningAuditorTransformer
 * @package namespace App\Transformers;
 */
class PlanningAuditorTransformer extends TransformerAbstract
{
    private $mandays_type;
    private $id;
    private $param;
    private $year;

    function __construct($mandays_type, $id, $param = array(), $year = null)
    {
        $this->mandays_type = $mandays_type;
        $this->id = $id;
        $this->param = $param;
        $this->year = $year;
    }

    /**
     * Transform the \PlanningAuditor entity
     * @param Auditor $model
     *
     * @return array
     */
    public function transform(Auditor $model)
    {
        $audit_service = app('App\Services\PlanningService');
        $this->param['auditor_id'] = $model->auditor_id;
        $audit_service->setMandaysSource($this->mandays_type, $this->id, $this->param);
        $mandays = $audit_service->setAuditor($model)
                                 ->setAuditYear($this->year)
                                 ->getMandays();
        if( !is_null($model->pivot) ) {
            $mandays['alloted'] = $model->pivot->allotted_mandays;
        }
        return [
            'id'       => (int) $model->auditor_id,
            'name'     => $model->full_name,
            'type'     => $model->pivot->auditor_type,
            'mandays'  => $mandays,
            'proficiencies' => $model->proficiency->pluck('proficiency_rate', 'proficiency_code'),
        ];
    }

}
