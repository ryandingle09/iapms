<?php

namespace App\Transformers;

use App\Models\AuditableEntity;
use App\Models\BusinessProcess;
use App\Services\AuditableEntityService;
use App\Transformers\BusinessProcessTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class AuditableEntityTransformer
 * @package namespace App\Transformers;
 */
class AuditableEntityTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'businessProcess'
    ];

    /**
     * Transform the \AuditableEntity entity
     * @param AuditableEntity $model
     *
     * @return array
     */
    public function transform(AuditableEntity $model)
    {
        return [
            'id'                 => (int) $model->auditable_entity_id,
            'name'               => $model->auditable_entity_name,
            'company_code'       => $model->company_code,
            'group_type'         => $model->group_type,
            'branch_code'        => $model->branch_code,
            'entity_class'       => $model->entity_class,
            'entity_code'        => $model->entity_code,
            'business_type'      => $model->business_type,
            'parent_entity'      => $model->parent_entity,
            'parent_ae_id'       => (int) $model->parent_ae_id,
            'contact_name'       => $model->contact_name,
            'entity_head_name'   => $model->entity_head_name,
        ];
    }

    /**
     * @param AuditableEntity $entity
     * @return \League\Fractal\Resource\Item
     */
    public function includeBusinessProcess(AuditableEntity $entity)
    {
        $service = app(AuditableEntityService::class);
        $bp = $service->setMasterCompanies()->getBpOfMasterCompany($entity);

        return $this->collection($bp, new BusinessProcessTransformer);
    }
}
