<?php

namespace App\Transformers;

use App\Models\AuditableEntitiesActual;
use App\Models\BpObjective;
use App\Models\BusinessProcess;
use App\Models\BusinessProcessesStep;
use App\Models\Risk;
use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

/**
 * Class AuditableEntityInquiryTransformer
 * @package namespace App\Transformers;
 */
class AuditableEntityInquiryTransformer extends TransformerAbstract
{
    private $id;
    private $text;
    private $type;
    private $lazy;
    private $details;
    private $icon;
    private $meta;

    protected $availableIncludes = [
        'children',
        'processes',
        'objectives',
        'steps',
        'risks',
        'controls',
    ];

    function __construct($id, $text, $type = '', $lazy = true, $details = array(), $icon = 'asterisk', $meta = array()) {
        $this->id = $id;
        $this->text = $text;
        $this->type = $type;
        $this->lazy = $lazy;
        $this->details = $details;
        $this->icon = 'fa fa-'.$icon;
        $this->meta = $meta;
    }

    /**
     * Transform the \AuditableEntity entity
     * @param Model $model
     *
     * @return array
     */
    public function transform(Model $model)
    {
        $text = explode('.', $this->text);
        if( count($text) > 1 ) {
            $title = $model->{$text[0]}->{$text[1]};
        }
        else {
            $title = $model->{$text[0]};
        }
        $format = [
            'key'           => (int) $model->{$this->id},
            'title'         => $title,
            'lazy'          => $this->lazy,
            'type'          => $this->type,
            'icon'          => $this->icon,
            'meta'          => $this->meta,
        ];

        if (isset($model->company_code)) {
            $format['company'] = (int) $model->company_code;
            if( \Request::get('subtreeview') == 'branch' ) $format['branch'] = (int) $model->branch_code;
        }

        if( !empty($this->details) ) {
            $unserialize = ['assertions', 'control_types'];
            $json_data = ['coso_data'];
            foreach ($this->details as $key => $val) {
                if( in_array($key, $unserialize) ){
                    $details = is_array($model->{$key}) ? $model->{$key} : unserialize($model->{$key});
                    $details = !empty($details) || $details ? implode(', ', $details) : '--';
                }
                elseif( in_array($key, $json_data) ) {
                    for($i=0; $i < count($val); $i++) {
                        $format['meta'][$key][$val[$i]] = $model->{$val[$i]};
                    }
                }
                else
                    $details = $model->{$key};

                if( !is_array($val) ) $format['details'][$val] = $details;
            }
        }

        return $format;
    }

    public function includeChildren(AuditableEntitiesActual $entity)
    {
        $this->details['branch_code'] = 'Branch';
        $this->details['department_code'] = 'Department';
        $collection = $entity->children;
        return $collection ? $this->collection($collection, new AuditableEntityInquiryTransformer($this->id, $this->text, $this->type, false, $this->details)) : $this->null();
    }

    public function includeProcesses(AuditableEntitiesActual $entity)
    {
        $details = [
            'lv_bp_code_desc' => 'Description',
        ];
        $collection = $entity->businessProcess()->addSelect('business_processes.*')
                      ->lookupDetails(config('iapms.lookups.business_process'),'lv_bp_code', 'business_processes.bp_code')
                      ->get();
        return $collection ? $this->collection($collection, new AuditableEntityInquiryTransformer('bp_id', 'bp_code', 'bp-objectives', false, $details)) : $this->null();
    }

    public function includeObjectives(BusinessProcess $bp)
    {
        $collection = $bp->bpObjectives;
        return $collection ? $this->collection($collection, new AuditableEntityInquiryTransformer('bp_objective_id', 'objective_narrative', 'bp-steps', false)) : $this->null();
    }

    public function includeSteps(BpObjective $objective)
    {
        $details = [
            'bp_steps_seq' => 'Sequence',
        ];
        $collection = $objective->bpSteps;
        return $collection ? $this->collection($collection, new AuditableEntityInquiryTransformer('bp_steps_id', 'activity_narrative', 'risks', false, $details)) : $this->null();
    }

    public function includeRisks(BusinessProcessesStep $step)
    {
        $id_field = 'risk_id';
        $text = 'risk_code';
        $type = 'controls';
        $details = [
            'lv_risk_code_desc' => 'Description',
            'risk_type' => 'Risk Type',
            'risk_remarks' => 'Risk Remarks',
            'risk_source_type' => 'Risk Source Type',
            'risk_response_type' => 'Risk Response Type',
            'def_inherent_impact_rate' => 'Inherent Impact Rate',
            'def_inherent_likelihood_rate' => 'Inherent Likelihood Rate',
            'def_residual_impact_rate' => 'Residual Impact Rate',
            'def_residual_likelihood_rate' => 'Residual Likelihood Rate',
            'rating_comment' => 'Rating Comment',
        ];
        $collection = $step->bpStepRisk()->addSelect('risks.*')
                      ->lookupDetails(config('iapms.lookups.risk.risks'),'lv_risk_code', 'risks.risk_code')
                      ->get();
        return $collection ? $this->collection($collection, new AuditableEntityInquiryTransformer('risk_id', 'risk_code', 'controls', false, $details)) : $this->null();
    }

    public function includeControls(Risk $risk)
    {
        $details = [
            'control_code' => 'Control Code',
            'lv_control_code_desc' => 'Control Description',
            'component_code' => 'Component Code',
            'lv_component_code_desc' => 'Component Description',
            'principle_code' => 'Principle Code',
            'lv_principle_code_desc' => 'Principle Description',
            'focus_code' => 'Focus Code',
            'lv_focus_code_desc' => 'Focus Description',
            'assertions' => 'Assertions',
            'control_types' => 'Control Types',
            'application' => 'Application',
            'control_source' => 'Control Source',
        ];
        $collection = $risk->controls()->addSelect('controls_master.*')
                      ->lookupDetails(config('iapms.lookups.control.controls'),'lv_control_code', 'controls_master.control_code')
                      ->lookupDetails(config('iapms.lookups.control.component'),'lv_component_code', 'controls_master.component_code')
                      ->lookupDetails(config('iapms.lookups.control.principle'),'lv_principle_code', 'controls_master.principle_code')
                      ->lookupDetails(config('iapms.lookups.control.focus'),'lv_focus_code', 'controls_master.focus_code')
                      ->get();
        return $collection ? $this->collection($collection, new AuditableEntityInquiryTransformer('control_id', 'control_name', 'controls', false, $details)) : $this->null();
    }
}
