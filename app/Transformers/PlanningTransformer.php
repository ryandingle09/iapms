<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Plan;

/**
 * Class PlanningTransformer
 * @package namespace App\Transformers;
 */
class PlanningTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'auditors',
        'projects'
    ];

    /**
     * Transform the \Planning entity
     * @param \Planning $model
     *
     * @return array
     */
    public function transform(Plan $model)
    {
        return [
            'id'         => (int) $model->plan_id,
            'year'       => (int) $model->plan_year,
            'name'       => $model->plan_name,
            'status'     => $model->plan_status,
            'status'     => $model->plan_status,
            'remarks'    => $model->remarks,
            'reviewer'   => $model->reviewed_by,
            'review_date'   => $model->reviewed_date,
            'reviewer_remarks'   => $model->reviewer_remarks,
            'approver'   => $model->approved_by,
            'approve_date'   => $model->approved_date,
            'approver_remarks'   => $model->approved_date,
            'created_by'   => $model->created_by,
        ];
    }

    public function includeAuditors(Plan $plan)
    {
        $collection = $plan->auditor;
        return $this->collection($collection, new PlanningAuditorTransformer('plan', $plan->plan_id, array(), $plan->plan_year));
    }

    public function includeProjects(Plan $plan)
    {
        $collection = $plan->projects;
        return $this->collection($collection, new PlanProjectTransformer);
    }
}
