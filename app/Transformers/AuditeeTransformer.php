<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Auditee;

/**
 * Class AuditeeTransformer
 * @package namespace App\Transformers;
 */
class AuditeeTransformer extends TransformerAbstract
{

    /**
     * Transform the \Auditee entity
     * @param \Auditee $model
     *
     * @return array
     */
    public function transform(Auditee $model)
    {
        return [
            'id'         => (int) $model->auditee_id,
            'text'       => $model->first_name.' '.$model->middle_name.' '.$model->last_name,
            'position'       => $model->position_code,
        ];
    }
}
