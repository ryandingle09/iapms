<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Risk;

/**
 * Class RiskTransformer
 * @package namespace App\Transformers;
 */
class RiskTransformer extends TransformerAbstract
{

    /**
     * Transform the \Risk entity
     * @param \Risk $model
     *
     * @return array
     */
    public function transform(Risk $model)
    {
        return [
            'id'         => (int) $model->risk_id,
            'code'       => $model->risk_code,
            'description'  => $model->lv_risk_code_desc,
        ];
    }
}
