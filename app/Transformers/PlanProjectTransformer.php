<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\PlanProject;

/**
 * Class PlanProjectTransformer
 * @package namespace App\Transformers;
 */
class PlanProjectTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'auditors',
    ];

    protected $defaultIncludes = [
        'scopes',
    ];

    /**
     * Transform the \PlanProject entity
     * @param \PlanProject $model
     *
     * @return array
     */
    public function transform(PlanProject $model)
    {
        return [
            'id'                => (int) $model->plan_project_id,
            'sequence'          => (int) $model->project_sequence,
            'name'              => $model->plan_project_name,
            'audit_type'        => $model->audit_type,
            'status'            => $model->plan_project_status,
            'start_date'        => $model->target_start_date,
            'end_date'          => $model->target_end_date,
            'remarks'           => $model->plan_project_remarks,
            'project_head'      => !is_null($model->auditor) ? $model->auditor->full_name : '',
            'reviewer'          => $model->reviewed_by,
            'reviewed_date'     => $model->reviewed_date,
            'reviewer_remarks'  => $model->reviewer_remarks,
            'approver'          => $model->approved_by,
            'approved_date'     => $model->approved_date,
            'approver_remarks'  => $model->approver_remarks,
//            'scopes'            => $model->scopes,
        ];
    }

    public function includeAuditors(PlanProject $project)
    {
        $collection = $project->projectAuditor;
        return $this->collection($collection, new PlanningAuditorTransformer);
    }

    public function includeScopes(PlanProject $project)
    {
        $collection = $project->scopes;
        return $this->collection($collection, new PlanProjectScopeTransformer);
    }
}
