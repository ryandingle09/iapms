<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\RiskAssessmentScale;

/**
 * Class RiskAssessmentScaleTransformer
 * @package namespace App\Transformers;
 */
class RiskAssessmentScaleTransformer extends TransformerAbstract
{

    /**
     * Transform the \RiskAssessmentScale entity
     * @param \RiskAssessmentScale $model
     *
     * @return array
     */
    public function transform(RiskAssessmentScale $model)
    {
        return [
            'id'         => (int) $model->risk_id,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
