<?php

namespace App\Transformers;

use App\Models\BusinessProcessesStep;
use App\Models\Risk;
use App\Transformers\RiskTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class BusinessProcessesStepTransformer
 * @package namespace App\Transformers;
 */
class BusinessProcessesStepTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'risks',
    ];

    /**
     * Transform the \BusinessProcessesStep entity
     * @param \BusinessProcessesStep $model
     *
     * @return array
     */
    public function transform(BusinessProcessesStep $model)
    {
        $attributes = range(1, 10);
        $return = [
            'id'            => (int) $model->bp_steps_id,
            'objective_id'  => $model->bp_objective_id,
            'sequence'      => $model->bp_steps_seq,
            'narrative'     => $model->activity_narrative,
            'created_by'    => $model->creator->user_name,
            'created_at'    => $model->created_date,
            'updated_by'    => $model->updater->user_name,
            'updated_at'    => $model->last_update_date
        ];
        for($i=0; $i < count($attributes); $i++) $return['attribute'.$attributes[$i]] = $model->{'attribute'.$attributes[$i]};
        return $return;
    }

    public function risks(BusinessProcessesStep $steps)
    {
        $collection = $steps->risks;
        return $this->collection($collection, new RiskTransformer);
    }
}
