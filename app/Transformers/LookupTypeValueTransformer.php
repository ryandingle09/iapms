<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\LookupType;

/**
 * Class LookupTypeValueTransformer
 * @package namespace App\Transformers;
 */
class LookupTypeValueTransformer extends TransformerAbstract
{

    /**
     * Transform the \LookupType entity
     * @param \LookupType $model
     *
     * @return array
     */
    public function transform(LookupType $model)
    {
        return [
            'id'         => (int) $model->lookupValue->id,
            'text'       => $model->lookupValue->lookup_code,
            'meaning'    => $model->lookupValue->lookup_meaning,
        ];
    }
}
