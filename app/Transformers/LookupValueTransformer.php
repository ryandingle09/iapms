<?php

namespace App\Transformers;

use App\Models\LookupValue;
use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

/**
 * Class LookupValueTransformer
 * @package namespace App\Transformers;
 */
class LookupValueTransformer extends TransformerAbstract
{
    private $other_details;
    protected $availableIncludes = [
        'child',
    ];

    public function __construct($other_details = array())
    {
        $this->other_details = $other_details;
    }
    /**
     * Transform the \LookupValue entity
     * @param Model $model
     *
     * @return array
     */
    public function transform(Model $model)
    {
        $response = [
            'id'            => $model->lookup_code,
            'text'          => $model->lookup_code,
            'meaning'       => $model->meaning,
            'description'   => $model->description,
        ];
        if( !empty($this->other_details) ) {
            foreach ($this->other_details as $key => $value) {
                $response[$key] = $model->{$value};
            }
        }

        return $response;
    }

    public function includeChild(LookupValue $model)
    {
        return $this->collection($model->child, new LookupValueTransformer());
    }
}
