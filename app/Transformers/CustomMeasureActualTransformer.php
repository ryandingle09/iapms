<?php

namespace App\Transformers;

use App\Models\AeMbpActual;
use League\Fractal\TransformerAbstract;

/**
 * Class CustomMeasureActualTransformer
 * @package namespace App\Transformers;
 */
class CustomMeasureActualTransformer extends TransformerAbstract
{

    /**
     * Transform the CustomMeasureActual entity
     * @param AeMbpActual $model
     *
     * @return array
     */
    public function transform(AeMbpActual $model)
    {
        $format =[
            'id'         => (int) $model->ae_mbp_id,
            'entity_id'  => (int) $model->auditable_entity_id,
        ];
        for ($i=0; $i < config('iapms.custom_measure.count'); $i++) {
            $count = $i + 1;
            $format['cm_'.$count.'_value'] =  $model->{'cm_'.$count.'_value'};
        }
        return $format;
    }
}
