<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\PlanProjectScope;

/**
 * Class PlanProjectScopeTransformer
 * @package namespace App\Transformers;
 */
class PlanProjectScopeTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'auditable_entity',
        'business_process',
    ];

    /**
     * Transform the \PlanProjectScope entity
     * @param \PlanProjectScope $model
     *
     * @return array
     */
    public function transform(PlanProjectScope $model)
    {
        return [
            'id'                => (int) $model->plan_project_scope_id,
            'sequence'          => $model->scope_sequence,
            'location'          => $model->audit_location,
            'budgeted_mandays'  => $model->budgeted_mandays,

            'created_by'        => $model->creator->user_name,
            'created_at'        => $model->created_date,
            'updated_by'        => $model->updater->user_name,
            'updated_at'        => $model->last_update_date
        ];
    }

    public function includeAuditableEntity(PlanProjectScope $scope)
    {
        $item = $scope->auditableEntity;
        return $item ? $this->item($item, new AuditableEntityTransformer) : $this->null();
    }

    public function includeBusinessProcess(PlanProjectScope $scope)
    {
        $item = $scope->businessProcess;
        return $item ? $this->item($item, new BusinessProcessTransformer) : $this->null();
    }
}
