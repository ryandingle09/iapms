<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Auditor;

/**
 * Class AuditorTransformer
 * @package namespace App\Transformers;
 */
class AuditorTransformer extends TransformerAbstract
{

    /**
     * Transform the \Auditor entity
     * @param \Auditor $model
     *
     * @return array
     */
    public function transform(Auditor $model)
    {
        return [
            'id'         => (int) $model->auditor_id,
            'text'       => $model->first_name.' '.$model->middle_name.' '.$model->last_name,
            'position'    => $model->position_code,
        ];
    }
}
