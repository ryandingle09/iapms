<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\Project;

/**
 * Class ProjectTransformer
 * @package namespace App\Transformers;
 */
class ProjectTransformer extends TransformerAbstract
{

    /**
     * Transform the \Project entity
     * @param \Project $model
     *
     * @return array
     */
    public function transform(Project $model)
    {
        return [
            'project_id'         => (int) $model->project_id,
            'project_name' => $model->project_name,
            'audit_type' => $model->audit_type,
            'project_status' => $model->project_status,
            'target_start_date' => date('d-M-Y',strtotime($model->target_start_date)),
            'target_end_date' => date('d-M-Y',strtotime($model->target_end_date)),
            'project_head' => $model->project_head,
            'project_remarks' => $model->project_remarks,
            'reviewed_by' => $model->reviewed_by,
            'reviewed_date' => $model->reviewed_date ? date('d-M-Y',strtotime($model->reviewed_date)) : null,
            'reviewer_remarks' => $model->reviewer_remarks,
            'approved_by' => $model->approved_by,
            'approved_date' => $model->approved_date ? date('d-M-Y',strtotime($model->approved_date)) : null,
            'approver_remarks' => $model->approver_remarks,
            // 'created_at' => $model->created_at,
            // 'updated_at' => $model->updated_at
        ];
    }
}
