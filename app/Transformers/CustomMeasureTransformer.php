<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\AeMbp;

/**
 * Class CustomMeasureTransformer
 * @package namespace App\Transformers;
 */
class CustomMeasureTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'actual'
    ];
    /**
     * Transform the CustomMeasure entity
     * @param AeMbp $model
     *
     * @return array
     */
    public function transform(AeMbp $model)
    {
        $format = [ 'id' => (int) $model->ae_mbp_id ];
        for ($i=0; $i < config('iapms.custom_measure.count'); $i++) {
            $count = $i + 1;
            $format['cm_'.$count.'_name'] =  $model->{'cm_'.$count.'_name'};
            $format['cm_'.$count.'_score_type'] =  $model->{'cm_'.$count.'_high_score_type'};
        }
        return $format;
    }

    public function includeActual(AeMbp $model)
    {
        $collection = $model->actual;
        return $this->collection($collection, new CustomMeasureActualTransformer());
    }
}
