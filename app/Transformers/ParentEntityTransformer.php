<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\AuditableEntity;

/**
 * Class ParentEntityTransformer
 * @package namespace App\Transformers;
 */
class ParentEntityTransformer extends TransformerAbstract
{

    /**
     * Transform the \AuditableEntity entity
     * @param \AuditableEntity $model
     *
     * @return array
     */
    public function transform(AuditableEntity $model)
    {
        return [
            'id'         => (int) $model->auditable_entity_id,
            'name'       => $model->auditable_entity_name,
        ];
    }
}
