<?php

namespace App\Transformers;

use App\Models\BpObjective;
use App\Transformers\BusinessProcessesStepTransformer;
use League\Fractal\TransformerAbstract;

/**
 * Class BpObjectiveTransformer
 * @package namespace App\Transformers;
 */
class BpObjectivesTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'steps',
    ];

    /**
     * Transform the \BpObjective entity
     * @param \BpObjective $model
     *
     * @return array
     */
    public function transform(BpObjective $model)
    {
        return [
            'id'         => (int) $model->bp_objective_id,
            'narrative'  => $model->objective_narrative,
            'created_by' => $model->creator->user_name,
            'created_at' => $model->created_date,
            'updated_by' => $model->updater->user_name,
            'updated_at' => $model->last_update_date
        ];
    }

    public function steps(BpObjective $objective)
    {
        $collection = $objective->bpSteps;
        return $this->collection($collection, new BusinessProcessesStepTransformer);
    }
}
