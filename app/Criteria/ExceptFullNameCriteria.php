<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ExceptFullNameCriteria
 * @package namespace App\Criteria;
 */
class ExceptFullNameCriteria implements CriteriaInterface
{
    private $name;

    public function __construct($name = array())
    {
        $this->name = $name;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if( count($this->name) && $this->name[0] != "" ) {
            list($first, $middle, $last) = $this->name;
            $model = $model->where('first_name', '<>', $first)
            ->where('middle_name', '<>', $middle)
            ->where('last_name', '<>', $last);
        }
        return $model;
    }
}
