<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ParentEntityByGroupBusinessClassCriteria
 * @package namespace App\Criteria;
 */
class ParentEntityByGroupBusinessClassCriteria implements CriteriaInterface
{
    private $group;
    private $business;
    private $entity;
    private $id;

    public function __construct($group, $business, $entity, $id = '')
    {
        $this->group = $group;
        $this->business = $business;
        $this->entity = $entity;
        $this->id = $id;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('group_type', $this->group)
                       ->where('business_type', $this->business)
                       ->where('entity_class', $this->entity);
        if( $this->id != '' ) $model->where('auditable_entity_id', '<>', $this->id);
        return $model;
    }
}
