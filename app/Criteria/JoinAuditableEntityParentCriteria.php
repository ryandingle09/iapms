<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class JoinAuditableEntityParentCriteria
 * @package namespace App\Criteria;
 */
class JoinAuditableEntityParentCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->addSelect( \DB::raw('parent_ae.auditable_entity_name AS parent') )
                       ->leftJoin( \DB::raw('ia_auditable_entities as parent_ae'), \DB::raw('parent_ae.auditable_entity_id'), '=', 'auditable_entities.parent_ae_id' );
        return $model;
    }
}
