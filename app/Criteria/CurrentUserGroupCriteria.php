<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;
/**
 * Class CurrentUserGroupCriteria
 * @package namespace App\Criteria;
 */
class CurrentUserGroupCriteria implements CriteriaInterface
{
    private $plan_id;

    public function __construct($plan_id)
    {
        $this->plan_id = $plan_id;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $auditor_id = Auth::user()->user_tag == "Auditor" ? Auth::user()->user_tag_ref_id : 0 ;
        $plan_id = $this->plan_id;

        return $model->leftJoin('plan_group_auditors','plan_group_auditors.plan_group_id','=','plan_groups.plan_group_id')
                ->where(function($q) use ( $auditor_id , $plan_id ){
                    $q->where('plan_groups.plan_id',$plan_id)
                    ->where('plan_group_auditors.auditor_id',$auditor_id);
                })
                ->groupBy('plan_groups.plan_group_id')
                ->addSelect('plan_groups.*');
    }
}
