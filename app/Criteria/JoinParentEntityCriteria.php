<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class JoinParentEntityCriteria
 * @package namespace App\Criteria;
 */
class JoinParentEntityCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->leftJoin( \DB::raw('ia_auditable_entities as parent_ae'), \DB::raw('parent_ae.auditable_entity_id'), '=', 'auditable_entities.parent_ae_id' )
                       ->addSelect( \DB::raw('parent_ae.auditable_entity_name AS parent, ia_auditable_entities.*') );
        return $model;
    }
}
