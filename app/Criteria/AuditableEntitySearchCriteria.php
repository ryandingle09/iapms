<?php

namespace App\Criteria;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class AuditableEntityInquiryCriteria
 * @package namespace App\Criteria;
 */
class AuditableEntitySearchCriteria implements CriteriaInterface
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if( $this->request->get('entity_search') ) {
            $type = $this->request->get('entity_type');
            $model = $model->orWhere('auditable_entity_name', 'like', '%'.$this->request->get('entity_search').'%');
            if( $type == 'actual' ) {
                $table = 'auditable_entities_actual';
                $model->orWhere('company_name', 'like', '%'.$this->request->get('entity_search').'%')
                ->orWhere(\DB::raw('lv_branch_code.description'), 'like', '%'.$this->request->get('entity_search').'%');
            }
            else {
                $table = 'auditable_entities';
                $model->orWhere('company_code', 'like', '%'.$this->request->get('entity_search').'%')
                ->orWhere($table.'.branch_code', 'like', '%'.$this->request->get('entity_search').'%');
            }
            $model->orWhere($table.'.department_code', 'like', '%'.$this->request->get('entity_search').'%')
            ->orWhere('business_type', 'like', '%'.$this->request->get('entity_search').'%')
            ->orWhere('entity_class', 'like', '%'.$this->request->get('entity_search').'%');
        }
        return $model;
    }
}
