<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
/**
 * Class ApgFilterColumnByFieldCriteria
 * @package namespace App\Criteria;
 */
class ApgFilterColumnByFieldCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $field = \Request::get('field');
        switch ($field) {
            case 'bp_name':
                return $model->addSelect(
                            'bp_name'
                        );
                break;
            case 'objective_name':
                return $model->addSelect(
                            'bp_name',
                            'objective_name'
                        );
                break;
            case 'activity_narrative':
                return $model->addSelect(
                            'bp_name',
                            'objective_name',
                            'activity_narrative'
                        );
                break;
            case 'risk_code':
                return $model->addSelect(
                            'bp_name',
                            'objective_name',
                            'activity_narrative',
                            'risk_code'
                        );
                break;
            case 'control_name':
                return $model->addSelect(
                            'bp_name',
                            'objective_name',
                            'activity_narrative',
                            'risk_code',
                            'control_name'
                        );
                break;

            default:
                return $model;
                break;
        }
    }
}
