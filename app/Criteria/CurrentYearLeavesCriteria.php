<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class CurrentYearLeavesCriteria
 * @package namespace App\Criteria;
 */
class CurrentYearLeavesCriteria implements CriteriaInterface
{
    private $year;

    public function __construct($year)
    {
        $this->year = $year;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where(\DB::raw('YEAR(leave_date_from)'), $this->year)
                       ->where(\DB::raw('YEAR(leave_date_to)'), $this->year);

        return $model;
    }
}
