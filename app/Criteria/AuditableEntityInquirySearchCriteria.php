<?php

namespace App\Criteria;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class AuditableEntityInquirySearchCriteria
 * @package namespace App\Criteria;
 */
class AuditableEntityInquirySearchCriteria implements CriteriaInterface
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $filters = $request->get('filter');
        if( $filters != '' ) {
            $comp_br_de = array('all', 'branch', 'department');
            if( in_array($this->request->get('subtreeview'), $comp_br_de) ) {
//                $model->where();
            }
            else {

            }
        }
        return $model;
    }
}
