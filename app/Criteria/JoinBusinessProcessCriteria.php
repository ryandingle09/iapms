<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class JoinBusinessProcessCriteria
 * @package namespace App\Criteria;
 */
class JoinBusinessProcessCriteria implements CriteriaInterface
{
    private $table;
    private $bp_id;

    function __construct($table, $bp_id = null)
    {
        $this->table = $table;
        $this->bp_id = $bp_id;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->join('business_processes', 'business_processes.bp_id', '=', $this->table.'.bp_id')
                       ->addSelect( \DB::raw('ia_business_processes.*') );

        if( !is_null($this->bp_id) ) {
            $model->where('business_processes.bp_id', $this->bp_id);
        }
        return $model;
    }
}
