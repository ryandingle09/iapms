<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ProjectScopeAuditorMandaysCriteria
 * @package namespace App\Criteria;
 */
class ProjectScopeAuditorMandaysCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->join('plan_auditors', 'plan_auditors.plan_id', '=', 'plan.plan_id')
            ->join('plan_projects', 'plan_projects.plan_id', '=', 'plan.plan_id')
            ->join('plan_project_auditors', function($j){
                $j->on('plan_project_auditors.plan_project_id', '=', 'plan_project_auditors.auditor_id')
                    ->where('plan_auditors.auditor_id', '=', 'plan_project_auditors.auditor_id');
            })
            ->groupBy('plan_project_auditors.auditor_id');
        return $model;
    }
}
