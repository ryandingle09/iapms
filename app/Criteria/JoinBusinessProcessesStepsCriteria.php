<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class JoinBusinessProcessesStepsCriteria
 * @package namespace App\Criteria;
 */
class JoinBusinessProcessesStepsCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->leftJoin('bp_objectives', 'bp_objectives.bp_id', '=', 'business_processes.bp_id')
                       ->leftJoin('business_processes_steps', 'business_processes_steps.bp_objective_id', '=', 'bp_objectives.bp_objective_id')
                       ->leftJoin('bp_steps_risks', 'bp_steps_risks.bp_step_id', '=', 'business_processes_steps.bp_steps_id');
        return $model;
    }
}
