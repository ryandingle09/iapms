<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchMbpaThroughAuditTypeCriteria
 * @package namespace App\Criteria;
 */
class SearchMbpaThroughAuditTypeCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $filter = \Request::get('audit_type_filter');
        if($filter){
            return $model->from('ae_mbp_actual AS mbpa')
                        ->join('ae_mbp_sbp AS sbp','sbp.ae_mbp_id','=','mbpa.master_ae_mbp_id')
                        ->join('bp_objectives AS bpo','bpo.bp_id','=','sbp.bp_id')
                        ->join('business_processes_steps AS bps','bps.bp_objective_id','=','bpo.bp_objective_id')
                        ->join('bp_steps_risks AS bpsr','bpsr.bp_step_id','=','bps.bp_steps_id')
                        ->join('risks_control AS rc','rc.risk_id','=','bpsr.risk_id')
                        ->join('controls_test_proc AS ctp','ctp.control_id','=','rc.control_id')
                        ->join('controls_test_audit_types AS ctpa','ctpa.control_test_id','=','ctp.control_test_id')
                        ->where('ctpa.audit_type',$filter)
                        ->groupBy('mbpa.ae_mbp_id')
                        ->addSelect(
                            'mbpa.ae_mbp_id',
                            'mbpa.auditable_entity_id',
                            'mbpa.main_bp_name',
                            'mbpa.master_ae_mbp_id'
                        );
        }
        return $model;
    }
}
