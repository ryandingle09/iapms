<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ExceptCodeCriteria
 * @package namespace App\Criteria;
 */
class ExceptCodeCriteria implements CriteriaInterface
{
    private $codes;

    public function __construct(array $codes)
    {
        $this->codes = $codes;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereNotIn('lookup_code', $this->codes);
        return $model;
    }
}
