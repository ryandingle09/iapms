<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class JoinCompanyBranchCriteria
 * @package namespace App\Criteria;
 */
class JoinCompanyBranchCriteria implements CriteriaInterface
{
    private $with_entity;
    private $table;
    private $alias;
    private $active;

    public function __construct($with_entity = false, $table = '', $alias = '', $active = 'Y' )
    {
        $this->with_entity = $with_entity;
        $this->table = $table;
        $this->active = $active;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->branchDetails($this->with_entity, $this->table, $this->alias, $this->active);

        return $model;
    }
}
