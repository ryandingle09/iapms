<?php

namespace App\Criteria;

use function foo\func;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class AuditeeEntitiesCriteria
 * @package namespace App\Criteria;
 */
class AuditeeEntitiesCriteria implements CriteriaInterface
{
    private $alias;

    public function __construct($alias = '')
    {
        $this->alias = $alias;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $table = $this->alias != '' ? $this->alias : 'auditable_entities_actual';
        $model = $model->join('auditees', function ($j) use ($table){
            $j->on('auditees.auditable_entity_id', '=', $table.'.auditable_entity_id')
              ->where('auditees.employee_id', '=', \Auth::user()->employee_id);
        });
        return $model;
    }
}
