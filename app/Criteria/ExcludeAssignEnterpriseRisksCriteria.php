<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ExcludeAssignEnterpriseRisksCriteria
 * @package namespace App\Criteria;
 */
class ExcludeAssignEnterpriseRisksCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $current = \Request::get('current');
        $model = $model->whereNotIn('risk_name', function($q) use($current){
            $q->select('erd.risk_name')
            ->from('risks')
            ->join('enterprise_risk_details as erd', 'erd.enterprise_risk_det_id', '=', 'risks.enterprise_risk_id');
            if( $current !='' ) $q->where('erd.risk_name', '<>', $current);
        });
        return $model;
    }
}
