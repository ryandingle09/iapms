<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class RiskAssessmentJoinAeBpCriteria
 * @package namespace App\Criteria;
 */
class RiskAssessmentJoinAeBpCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->addSelect(\DB::raw('ia_auditable_entities.*'))
                       ->join('auditable_entities', 'auditable_entities.auditable_entity_id', '=', 'risk_assessment_scale.auditable_entity_id');
        return $model;
    }
}
