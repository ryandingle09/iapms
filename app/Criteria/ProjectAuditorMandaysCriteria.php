<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ProjectAuditorMandaysCriteria
 * @package namespace App\Criteria;
 */
class ProjectAuditorMandaysCriteria implements CriteriaInterface
{
    private $auditor_id;

    /**
     * PlanProjectAuditorMandaysCriteria constructor.
     * @param integer $project_id  gets the specific project mandays
     * @param null $auditor_id  gets the specific project auditor mandays if not null
     */
    function __construct($auditor_id = null)
    {
        $this->auditor_id = $auditor_id;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->join('plan_project_auditors', 'plan_projects.plan_project_id', '=', 'plan_project_auditors.plan_project_id');
        if( !is_null($this->auditor_id) ){
            $model->where('plan_project_auditors.auditor_id', $this->auditor_id)
                  ->groupBy('plan_project_auditors.auditor_id');
        }

        return $model;
    }
}
