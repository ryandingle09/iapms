<?php

namespace App\Criteria;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class RiskAssessmentScaleCriteria
 * @package namespace App\Criteria;
 */
class RiskAssessmentScaleCriteria implements CriteriaInterface
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if($this->request->get('auditable_entity') != '') {
            $model = $model->where('ae.auditable_entity_id', $this->request->get('auditable_entity'));
        }
        if($this->request->get('main_business_process') != '') {
            $model = $model->where('mbp.ae_mbp_id', $this->request->get('main_business_process'));
        }
        if($this->request->get('business_process') != '') {
            $model = $model->where('bp.bp_id', $this->request->get('business_process'));
        }
        if($this->request->get('objective') != '') {
            $model = $model->where('bpo.bp_objective_id', $this->request->get('objective'));
        }
        if($this->request->get('risk') != '') {
            $model = $model->where('r.risk_id', $this->request->get('risk'));
        }

        return $model;
    }
}
