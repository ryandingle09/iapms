<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class EnititiesByCompanyCodeCriteria
 * @package namespace App\Criteria;
 */
class EntitiesByCompanyCodeCriteria implements CriteriaInterface
{
    private $company_code;

    public function __construct($company_code = '')
    {
        $this->company_code = $company_code;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if( $this->company_code != '' ) $model = $model->entitiesByCompany($this->company_code);
        return $model;
    }
}
