<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class RiskJoinControlsCriteriaCriteria
 * @package namespace App\Criteria;
 */
class RiskJoinControlsCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->select(\DB::raw('ia_risks_control.*'))->join('controls_master', 'controls_master.control_id', '=', 'risks_control.control_id');

        return $model;
    }
}
