<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class AuditableEntityParentCriteria
 * @package namespace App\Criteria;
 */
class AuditableEntityParentCriteria implements CriteriaInterface
{
    private $is_parent;

    public function __construct($is_parent='Y')
    {
        $this->is_parent = $is_parent;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->isParentCompany($this->is_parent);

        return $model;
    }
}
