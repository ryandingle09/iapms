<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class JoinLookupValuesCriteria
 * @package namespace App\Criteria;
 */
class JoinLookupValuesCriteria implements CriteriaInterface
{
    private $code_fk;
    private $default_select;
    private $select_table;
    private $by_meaning;

    public function __construct(array $code_fk, $default_select = '', $select_table = true, $by_meaning = false)
    {
        $this->code_fk = $code_fk;
        $this->default_select = $default_select;
        $this->select_table = $select_table;
        $this->by_meaning = $by_meaning;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $selected = [];
        foreach ($this->code_fk as $key => $value) {
            $field = explode('.', $key);
            $alias = 'lv_'.end($field);
            $main_table = 'ia_'.$field[0].'.*';
            $model = $model->lookupDetails($value, $alias, $key, $this->by_meaning);
            if( $this->select_table && !in_array($main_table, $selected) )  {
                $model = $model->addSelect( \DB::raw($main_table) );
            }
            $selected[] = $main_table;
        }

        return $model;
    }
}
