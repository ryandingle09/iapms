<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class AuditorActiveCriteria
 * @package namespace App\Criteria;
 */
class AuditorActiveCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $now = date('Y-m-d');
        $model = $model->where('effective_end_date', '>=', $now);

        return $model;
    }
}
