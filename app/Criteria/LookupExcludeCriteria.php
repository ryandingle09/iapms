<?php

namespace App\Criteria;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class LookupExcludeCriteria
 * @package namespace App\Criteria;
 */
class LookupExcludeCriteria implements CriteriaInterface
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $exclude = $this->request->get('exclude');
        if( $exclude !='' ) {
            if( strpos($exclude,'|') ) {
                list($field, $value) = explode('|', $exclude);
                $model = $model->where($field, '<>', $value);
            }
        }

        return $model;
    }
}
