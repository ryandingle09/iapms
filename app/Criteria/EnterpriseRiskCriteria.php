<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class EnterpriseRiskCriteria
 * @package namespace App\Criteria;
 */
class EnterpriseRiskCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $entity_alias = 'ae';
        $model = $model->select([
            'enterprise_risks.*',
            'ae.auditable_entity_name',
            'ae.company_code',
            'ae.branch_code',
            'ae.department_code' ,
            'ae.contact_name',
            'ae.entity_head_name',
            'mbp.main_bp_name',
            'bp.bp_code',
            'bp.bp_name',
            'bp.source_type'
        ])
        ->join('auditable_entities_actual AS '.$entity_alias, $entity_alias.'.auditable_entity_id', '=', 'enterprise_risks.auditable_entity_id')
        ->join('ae_mbp AS mbp', 'mbp.ae_mbp_id', '=', 'enterprise_risks.ae_mbp_id')
        ->join('business_processes AS bp', 'bp.bp_id', '=', 'enterprise_risks.bp_id')
        ->groupBy('enterprise_risks.auditable_entity_id', 'enterprise_risks.ae_mbp_id', 'enterprise_risks.bp_id');

        return $model;
    }
}
