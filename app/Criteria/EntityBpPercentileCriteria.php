<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class EntityBpPercentileCriteria
 * @package namespace App\Criteria;
 */
class EntityBpPercentileCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $cm_range = range(1, 5);
        for( $i=0; $i < count($cm_range); $i++ ) {
            $field = 'cm_'.$cm_range[$i].'_value';
            // (custom_measure/100) * (count_of_custom_measures + 1) * 100
            $model->addSelect(\DB::raw('('.$field.'/100) * (COUNT( '.$field.' ) + 1) * 100 as cm_'.$cm_range[$i].'_percentile'));
        }

        return $model;
    }
}
