<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActualEntityGroupByCompanyCodeCriteria
 * @package namespace App\Criteria;
 */
class ActualEntityGroupByCompanyCodeCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->groupByCompany();
        if( \Request::get('limit') != '' ) $model = $model->take(\Request::get('limit'));

        return $model;
    }
}
