<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SelectTestProcudureCriteria
 * @package namespace App\Criteria;
 */
class SelectTestProcudureCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        return $model->addSelect(
                    "tp.*",
                    "rc.risk_control_seq",
                    "ctp.control_id",
                    "ctp.control_test_seq",
                    "ctp.test_proc_id"
                    )
                    ->leftJoin("ae_mbp_actual as mbp", "auditable_entities_actual.auditable_entity_id", "=", "mbp.auditable_entity_id")
                    ->leftJoin("ae_mbp_sbp as sbp", "mbp.master_ae_mbp_id", "=", "sbp.ae_mbp_id")
                    ->leftJoin("bp_objectives as bpo", "bpo.bp_id", "=", "sbp.bp_id")
                    ->leftJoin("business_processes_steps as bps","bps.bp_objective_id" ,"=", "bpo.bp_objective_id")
                    ->leftJoin("bp_steps_risks as bpsr","bpsr.bp_step_id" ,"=", "bps.bp_steps_id")
                    ->leftJoin("risks_control as rc","rc.risk_id" ,"=", "bpsr.risk_id")
                    ->leftJoin("controls_test_proc as ctp","ctp.control_id" ,"=", "rc.control_id")
                    ->leftJoin("test_proc as tp","tp.test_proc_id" ,"=", "ctp.test_proc_id");
    }
}
