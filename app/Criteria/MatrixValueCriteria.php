<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class MatrixValueCriteria
 * @package namespace App\Criteria;
 */
class MatrixValueCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->addSelect(\DB::raw('(select matrix_value from ia_matrix inner join ia_matrix_details on `ia_matrix`.`matrix_id` = `ia_matrix_details`.`matrix_id` where `ia_matrix`.`matrix_name` = \'Risk Index\' and x_sequence = if(ia_risk_assessment_scale.impact_value > 0, ia_risk_assessment_scale.impact_value, ia_risks.def_inherent_impact_rate) and y_sequence = if(ia_risk_assessment_scale.likelihood_value > 0, ia_risk_assessment_scale.likelihood_value, ia_risks.def_inherent_likelihood_rate)) as matrix_value'))
        ->join('bp_objectives', 'bp_objectives.bp_id', '=', 'auditable_entities_bp.bp_id')
        ->join('business_processes_steps', 'business_processes_steps.bp_objective_id', '=', 'bp_objectives.bp_objective_id')
        ->join('bp_steps_risks', 'bp_steps_risks.bp_step_id', '=', 'business_processes_steps.bp_steps_id')
        ->join('risks', 'risks.risk_id', '=', 'bp_steps_risks.risk_id')
        ->leftJoin('risk_assessment_scale', function($ras){
            $ras->on('auditable_entities.auditable_entity_id', '=', 'risk_assessment_scale.auditable_entity_id')
                ->on('risk_assessment_scale.risk_id', '=', 'bp_steps_risks.risk_id')
                ->on('risk_assessment_scale.bp_id', '=', 'auditable_entities_bp.bp_id');
        });
//        dd($model->toSql());
        return $model;
    }
}
