<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class EnititiesByBranchCodeCriteria
 * @package namespace App\Criteria;
 */
class EntitiesByBranchCodeCriteria implements CriteriaInterface
{
    private $branch_code;

    public function __construct($branch_code = '')
    {
        $this->branch_code = $branch_code;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if( $this->branch_code != '' ) $model = $model->entitiesByBranch($this->branch_code);
        return $model;
    }
}
