<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class AuditableEntityExcludeCompanyBranchCriteria
 * @package namespace App\Criteria;
 */
class AuditableEntityExcludeCompanyBranchCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $table = 'auditable_entities';
        $table .= (\Request::get('entity_type') == 'actual' ? '_actual' : '');
        $model = $model->where($table.'.branch_code', '<>', '')->where($table.'.department_code', '<>', '');
        return $model;
    }
}
