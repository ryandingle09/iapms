<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class JoinAuditableEntityCriteria
 * @package namespace App\Criteria;
 */
class JoinAuditableEntityCriteria implements CriteriaInterface
{
    private $table;

    function __construct($table)
    {
        $this->table = $table;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->join('auditable_entities', 'auditable_entities.auditable_entity_id', '=', $this->table.'.auditable_entity_id');
        return $model;
    }
}
