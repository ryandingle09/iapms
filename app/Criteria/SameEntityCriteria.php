<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SameEntityCriteria
 * @package namespace App\Criteria;
 */
class SameEntityCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $entity = \Request::get('entity');
        if($entity != '') {
            $model = $model->where('auditable_entity_id', $entity);
        }

        return $model;
    }
}
