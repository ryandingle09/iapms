<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class RiskInquiryCriteria
 * @package namespace App\Criteria;
 */
class RiskInquiryCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $filter = \Request::get('filter');
        if( isset($filter['risk']) && $filter['risk'] != '' ) {
            $model = $model->where('risk_code', $filter['risk'])->groupBy('risks.risk_id');
        }
        return $model;
    }
}
