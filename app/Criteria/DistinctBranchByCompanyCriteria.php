<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class DistinctBranchByCompanyCriteria
 * @package namespace App\Criteria;
 */
class DistinctBranchByCompanyCriteria implements CriteriaInterface
{
    private $company_code;

    function __construct($company_code) {
        $this->company_code = $company_code;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->addSelect('auditable_entities_actual.branch_code AS text', \DB::raw('lv_branch_code.meaning as meaning'))
                       ->where('company_code', $this->company_code)
                       ->where('branch_code', '<>', '')
                       ->groupBy('branch_code');
        return $model;
    }
}
