<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class JoinAuditableEntityBPCriteria
 * @package namespace App\Criteria;
 */
class JoinAuditableEntityBPCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->join('auditable_entities_bp', 'auditable_entities_bp.auditable_entity_id', '=', 'auditable_entities.auditable_entity_id');
        return $model;
    }
}
