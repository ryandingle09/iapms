<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class AuditableEntityAttachBusinessProcessCriteria
 * @package namespace App\Criteria;
 */
class AuditableEntityAttachBusinessProcessCriteria implements CriteriaInterface
{
    private $bp_name;

    public function __construct($bp_name = '')
    {
        $this->bp_name = $bp_name;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->leftJoin('auditable_entities_bp', function($j){
                           $j->on('auditable_entities_bp.auditable_entity_id', '=', 'risk_assessment_scale.auditable_entity_id')
                             ->on('auditable_entities_bp.bp_id', '=', 'risk_assessment_scale.bp_id');
                       })
                       ->leftJoin('business_processes', function($j){
                            $j->on('auditable_entities_bp.bp_id', '=', 'business_processes.bp_id');

                            if( $this->bp_name != '' ) $j->where('bp_name', 'like', '%'.$this->bp_name.'%');
                       })
                       ->groupBy('risk_assessment_scale.auditable_entity_id', 'risk_assessment_scale.bp_id');
        return $model;
    }
}
