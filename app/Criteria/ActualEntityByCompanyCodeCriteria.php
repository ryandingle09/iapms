<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ActualEntityByCompanyCodeCriteria
 * @package namespace App\Criteria;
 */
class ActualEntityByCompanyCodeCriteria implements CriteriaInterface
{
    private $company_code;

    public function __construct($company)
    {
        $this->company_code = $company;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('auditable_entities_actual.company_code', $this->company_code);
        return $model;
    }
}
