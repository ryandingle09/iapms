<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PlanAuditorsCriteria
 * @package namespace App\Criteria;
 */
class PlanAuditorsCriteria implements CriteriaInterface
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->join('plan_auditors', 'plan_auditors.auditor_id', '=', 'auditors.auditor_id')
                       ->where('plan_auditors.plan_id', $this->id);

        return $model;
    }
}
