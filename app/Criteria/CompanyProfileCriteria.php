<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class CompanyProfileCriteria
 * @package namespace App\Criteria;
 */
class CompanyProfileCriteria implements CriteriaInterface
{
    private $table;
    private $alias;

    public function __construct($table = '', $alias = '')
    {
        $this->table = $table;
        $this->alias = $alias;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->companyDetails($this->table, $this->alias);
        return $model;
    }
}
