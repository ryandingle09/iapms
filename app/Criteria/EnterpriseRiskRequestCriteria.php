<?php

namespace App\Criteria;

use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class EnterpriseRiskRequestCriteria
 * @package namespace App\Criteria;
 */
class EnterpriseRiskRequestCriteria implements CriteriaInterface
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if( $this->request->get('search_auditable_entity') ) {
            $model = $model->where('enterprise_risks.auditable_entity_id', $this->request['search_auditable_entity']);
        }
        if( $this->request->get('search_main_business_process') ) {
            $model = $model->where('enterprise_risks.ae_mbp_id', $this->request['search_main_business_process']);
        }
        if( $this->request->get('search_business_process') ) {
            $model = $model->where('enterprise_risks.bp_id', $this->request['search_business_process']);
        }

        return $model;
    }
}
