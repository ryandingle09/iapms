<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PlanningAuditorCriteria
 * @package namespace App\Criteria;
 */
class PlanningAuditorCriteria implements CriteriaInterface
{
    private $table;
    private $id;

    function __construct($id, $table = '')
    {
        $this->id = $id;
        $this->table = $table;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if( !is_null($this->id) ) {
            $field = $this->table != '' ? $this->table.'.auditor_id' : 'auditor_id';
            $model = $model->where($field, $this->id);
        }

        return $model;
    }
}
