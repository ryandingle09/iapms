<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class EntityMasterCompanyCriteria
 * @package namespace App\Criteria;
 *
 * This will set the criteria to get business processes
 * in the MASTER COMPANY
 */
class EntityMasterCompanyCriteria implements CriteriaInterface
{
    private $group_type;
    private $entity_class;
    private $department;
    private $business;

    /**
     *
     * @param string $group_type
     * @param string $entity_class
     * @param string $department
     * @param string $branch
     * @param string $business
     */
    public function __construct($group_type, $entity_class, $department, $business)
    {
        $this->group_type = $group_type;
        $this->entity_class = $entity_class;
        $this->department = $department;
        $this->business = $business;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('group_type', $this->group_type)
        ->where('entity_class', $this->entity_class)
        ->where('department_code', $this->department)
        ->where('business_type', $this->business)
        ->where('parent_entity', 'N')
        ->where(\DB::raw("concat('',company_code * 1)"), '=', 0);

        return $model;
    }
}
