<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;
/**
 * Class CurrentUserPlanProjectsCriteria
 * @package namespace App\Criteria;
 */
class CurrentUserPlanProjectsCriteria implements CriteriaInterface
{
    private $group_id;

    public function __construct($group_id)
    {
        $this->group_id = $group_id;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $auditor_id = Auth::user()->user_tag == "Auditor" ? Auth::user()->user_tag_ref_id : 0 ;
        $group_id = $this->group_id;

        return $model->leftJoin('plan_project_auditors','plan_project_auditors.plan_project_id','=','plan_projects.plan_project_id')
                ->where(function($q) use ( $auditor_id , $group_id ){
                    $q->where('plan_group_id',$group_id)
                    ->where('plan_project_auditors.auditor_id',$auditor_id);
                })
                // ->orWhere(function($q) use ( $auditor_id , $group_id ){
                //    $q->where('group_id',$group_id)
                //     ->where('plan_projects.project_head',$auditor_id);
                // })
                ->groupBy('plan_projects.plan_project_id')
                ->addSelect('plan_projects.*');
    }
}
