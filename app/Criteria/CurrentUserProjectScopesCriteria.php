<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Auth;
/**
 * Class CurrentUserProjectScopesCriteriaCriteria
 * @package namespace App\Criteria;
 */
class CurrentUserProjectScopesCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $trigger = \Request::get('userfilter');
        if($trigger){
            $auditor_id = Auth::user()->user_tag == "Auditor" ? Auth::user()->user_tag_ref_id : 0 ;
            return $model->leftJoin('projects','projects.project_id','=','project_scope.project_id')
                        ->leftJoin('project_auditors','project_auditors.project_id','=','projects.project_id')
                ->where(function($q) use ( $auditor_id  ){
                    $q->where('project_auditors.auditor_id',$auditor_id);
                })
                ->orWhere(function($q) use ( $auditor_id  ){
                   $q->where('projects.project_head',$auditor_id);
                })
                ->groupBy('project_scope.project_scope_id')
                ->addSelect('project_scope.*');
        }
        return $model;
    }
}
