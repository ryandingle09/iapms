<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class BpObjectiveCriteria
 * @package namespace App\Criteria;
 */
class BpObjectiveCriteria implements CriteriaInterface
{
    private $bp_id;

    public function __construct($bp_id = '')
    {
        $this->bp_id = $bp_id;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->groupBy('objective_name');
        if( $this->bp_id != '' ) $model->where('bp_id', $this->bp_id);

        return $model;
    }
}
