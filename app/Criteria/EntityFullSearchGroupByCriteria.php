<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class EntityFullSearchGroupByCriteria
 * @package namespace App\Criteria;
 * @author Romnick John M. Susa
 * @link www.beautifulsyntax.com
 */
class EntityFullSearchGroupByCriteria implements CriteriaInterface
{

    protected $fields = [
        'auditable_entity_id' => 'aea.auditable_entity_id',
        'auditable_entity_name' => 'aea.auditable_entity_name',
        'master_ae_mbp_id' => 'mbpa.master_ae_mbp_id',
        'mbpa_id' => 'mbpa.ae_mbp_id',
        'mbpa_name' => 'mbpa.main_bp_name',
        'risk_id' => 'r.risk_id',
        'risk_code' => 'r.risk_code',
        'audit_type' => 'ctat.audit_type'
    ];

    protected $searchableFields = [
        'auditable_entity_id' => "=",
        'auditable_entity_name' => 'like',
        'master_ae_mbp_id' => '=',
        'mbpa_id' => '=',
        'mbpa_name' => 'like',
        'risk_id' => '=',
        'risk_code' => 'like',
        'audit_type' => "="
    ];
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {   
        $fgroup_by = \Request::get('fgroupBy') ? \Request::get('fgroupBy') : null;
        $ffilter = \Request::get('ffilter') ? \Request::get('ffilter') : null;
        $fsearch = \Request::get('fsearch') ? \Request::get('fsearch') : null;

        if( $fgroup_by ){
            $group_bys = explode(";", $fgroup_by);
            foreach ($group_bys as $key => $value) {
                if( array_key_exists( $value , $this->fields ) ){
                    $model = $model->groupBy( $this->fields[$value] );
                }
            }
        }

        if( $ffilter ){
            $filter_array = [];
            $filters = explode(";", $ffilter);

            foreach ($filters as $filter) {

                if( array_key_exists( $filter , $this->fields ) ){

                    array_push( $filter_array , $this->fields[$filter]);
                }

            }
            if( count($filter_array) ){

                $model =  $model->addSelect( $filter_array );
            }else{
                $model =  $model->addSelect( array_divide($this->fields)[1] );
            }
        }else{
            $model =  $model->addSelect( array_divide($this->fields)[1] );
        }

        if($fsearch){
            $search = explode(";", $fsearch );
            if(count($search)){
                foreach ($search as $key => $value) {
                    $keyval = explode(":", $value );
                    if(array_key_exists( $keyval[0] , $this->searchableFields )){
                        $full_keyword = $this->searchableFields[  $keyval[0] ] == "like" ? '%'.$keyval[1].'%' : $keyval[1];
                        $model = $model->where( 
                            $this->fields[ $keyval[0] ],
                            $this->searchableFields[  $keyval[0] ],
                            $full_keyword
                        );
                    }
                }
                
            }
        }




        return $model;
    }

    //USAGE
    // DOMAIN/plan-guide/search-data
    // ?auditable_entity_id=9075
    // &fsearch=auditable_entity_name:adfas
    // &groupBy=auditable_entity_id
}
