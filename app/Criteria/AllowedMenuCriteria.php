<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class AllowedMenuCriteria
 * @package namespace App\Criteria;
 */
class AllowedMenuCriteria implements CriteriaInterface
{
    private $allowed;

    public function __construct($allowed = array())
    {
        $this->allowed = $allowed;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereIn('menu_entries.permission_id', $this->allowed);
        return $model;
    }
}
