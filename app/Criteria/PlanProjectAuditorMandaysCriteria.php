<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class PlanProjectAuditorMandaysCriteria
 * @package namespace App\Criteria;
 */
class PlanProjectAuditorMandaysCriteria implements CriteriaInterface
{
    private $project_id;

    /**
     * PlanProjectAuditorMandaysCriteria constructor.
     * @param null $project_id  gets the specific project mandays if not null
     */
    function __construct($project_id = null)
    {
        $this->project_id = $project_id;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->join('plan_auditors', 'plan_auditors.plan_id', '=', 'plan.plan_id')
                       ->join('plan_projects', 'plan_projects.plan_id', '=', 'plan.plan_id')
                       ->join('plan_project_auditors', function($j){
                            $j->on('plan_project_auditors.auditor_id', '=', 'plan_auditors.auditor_id');

                           if( !is_null($this->project_id) ) {
                               $j->where('plan_project_auditors.plan_project_id', '=', $this->project_id );
                           }
                           else {
                               $j->on('plan_projects.plan_project_id', '=', 'plan_project_auditors.plan_project_id' );
                           }
                       })
                       ->groupBy('plan_project_auditors.auditor_id');
        return $model;
    }
}
