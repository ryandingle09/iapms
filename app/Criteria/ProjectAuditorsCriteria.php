<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class ProjectAuditorsCriteria
 * @package namespace App\Criteria;
 */
class ProjectAuditorsCriteria implements CriteriaInterface
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->join('plan_project_auditors', 'plan_project_auditors.auditor_id', '=', 'auditors.auditor_id')
                       ->where('plan_project_auditors.plan_project_id', $this->id);
        return $model;
    }
}
