<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class JoinTestControlDetailsCriteriaCriteria
 * @package namespace App\Criteria;
 */
class JoinTestControlDetailsCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->join('test_proc', 'test_proc.test_proc_id', '=', 'controls_test_proc.test_proc_id');
        return $model;
    }
}
