<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class DistinctDepartmentByBranchCriteria
 * @package namespace App\Criteria;
 */
class DistinctDepartmentByBranchCriteria implements CriteriaInterface
{
    private $branch_code;

    function __construct($branch_code) {
        $this->branch_code = $branch_code;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('branch_code', $this->branch_code)
                       ->groupBy('department_code');
        return $model;
    }
}
