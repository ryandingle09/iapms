<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class AuditableEntityMasterDetailsCriteria
 * @package namespace App\Criteria;
 */
class AuditableEntityMasterDetailsCriteria implements CriteriaInterface
{
    private $group;
    private $entity_class;
    private $business_type;

    function __construct($group, $entity_class, $business_type)
    {
        $this->group = $group;
        $this->entity_class = $entity_class;
        $this->business_type = $business_type;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->where('group_type', $this->group)
                       ->where('entity_class', $this->entity_class)
                       ->where('business_type', $this->business_type)
                       ->where(\DB::raw('(company_code * 1)'), 0);
        return $model;
    }
}
