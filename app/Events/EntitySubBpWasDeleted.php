<?php

namespace App\Events;

use App\Events\Event;
use App\Models\AuditableEntity;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EntitySubBpWasDeleted extends Event implements ShouldQueue
{
    use SerializesModels;

    public $entity;
    public $bp;

    /**
     * Create a new event instance.
     *
     * @param AuditableEntity $entity
     * @param $bp
     */
    public function __construct(AuditableEntity $entity, $bp)
    {
        $this->entity = $entity;
        $this->bp = $bp;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
