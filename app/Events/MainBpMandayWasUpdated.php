<?php

namespace App\Events;

use App\Events\Event;
use App\Models\AeMbp;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MainBpMandayWasUpdated extends Event
{
    use SerializesModels;

    public $main_bp;
    public $mandays;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(AeMbp $main_bp, $mandays = array())
    {
        $this->main_bp = $main_bp;
        $this->mandays = $mandays;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
