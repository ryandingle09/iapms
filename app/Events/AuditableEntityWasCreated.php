<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AuditableEntityWasCreated extends Event implements ShouldQueue
{
    use SerializesModels;

    public $auditable_entity;
    public $type;

    /**
     * Create a new event instance.
     * @param Model $auditable_entity
     * @param $entity_class
     */
    public function __construct(Model $auditable_entity, $type = 'master')
    {
        $this->auditable_entity = $auditable_entity;
        $this->type = $type;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
