<?php

namespace App\Events;

use App\Events\Event;
use App\Models\AuditableEntity;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EntitySubBpWasCreated extends Event implements ShouldQueue
{
    use SerializesModels;

    public $auditable_entity;
    public $type;

    /**
     * Create a new event instance.
     *
     * @param AuditableEntity $auditable_entity
     * @param string $type master|actual
     *
     */
    public function __construct(AuditableEntity $auditable_entity, $type = 'master')
    {
        $this->auditable_entity = $auditable_entity;
        $this->type = $type;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
