<?php

namespace App\Events;

use App\Events\Event;
use App\Models\AeMbp;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EntityMainBpWasCreated extends Event
{
    use SerializesModels;

    public $main_bp;
    /**
     * Create a new event instance.
     * @param $main_bp AeMbp
     */
    public function __construct(AeMbp $main_bp)
    {
        $this->main_bp = $main_bp;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
