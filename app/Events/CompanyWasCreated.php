<?php

namespace App\Events;

use App\Events\Event;
use App\Models\CompanyBranch;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CompanyWasCreated extends Event implements ShouldQueue
{
    use SerializesModels;

    public $company;
    /**
     * Create a new event instance.
     * @param $company CompanyBranch
     */
    public function __construct(CompanyBranch $company)
    {
        $this->company = $company;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
