<?php
namespace App\Traits;

trait Authorable
{
    public function creator()
    {
        return $this->belongsTo('App\User', 'last_update_by');
    }

    public function updater()
    {
        return $this->belongsTo('App\User', 'last_update_by');
    }
}