<?php
namespace App\Traits;

trait Auditable
{
    /**
     * Scope to group entities by company, branch and department
     * @param $query
     * @return mixed
     */
    public function scopeGroupByCompany($query)
    {
        return $query->groupBy($this->getTable().'.company_code', $this->getTable().'.branch_code', $this->getTable().'.department_code');
    }

    /**
     * Scope to attach company details
     * @param $query
     * @param string $table
     * @param string $alias
     * @return mixed
     */
    public function scopeCompanyDetails($query, $table = '', $alias = '')
    {
        if( $alias != '' )
            $table = $alias;
        else
            $table = $table == '' ? $this->getTable() : $table;

        return $query->addSelect('company_profile.company_id', 'company_profile.group_type', 'company_profile.business_type', 'company_name', 'company_profile.description')
            ->leftJoin('company_profile', 'company_profile.company_code', '=', $table.'.company_code');
    }

    /**
     * Scope to attach branch details
     * @param $query
     * @param bool $with_entity - if true, gets the branch entity class
     * @param $table
     * @param $alias
     * @param string $active  Y|N
     * @return mixed
     */
    public function scopeBranchDetails($query, $with_entity = false, $table = '', $alias = '', $active = 'Y')
    {
        if( $alias != '' )
            $table = $alias;
        else
            $table = $table == '' ? $this->getTable() : $table;

        return $query->addSelect('company_branches.active', 'company_branches.branch_store_size', 'company_branches.branch_address', 'company_branches.branch_zone', 'company_branches.branch_store_class', 'company_branches.branch_class', 'company_branches.entity_class', 'company_branches.branch_opening_date')
            ->leftJoin('company_branches', function ($j) use($with_entity, $table, $active){
                $j->on('company_branches.company_id', '=', 'company_profile.company_id');
                if( $with_entity ) $j->on('company_branches.branch_code', '=', $table.'.branch_code');
                $j->where('company_branches.active', '=', $active);
            })
            ->orderBy(\DB::raw('CAST(ia_company_branches.branch_code as UNSIGNED)'), 'asc');
    }

    /**
     * Scope to get entities by company
     * @param $query
     * @param $company_code
     * @return mixed
     */
    public function scopeEntitiesByCompany($query, $company_code)
    {
        return $query->where($this->getTable().'.company_code', $company_code);
    }

    /**
     * Scope to get entities by branch
     * @param $query
     * @param $branch_code
     * @return mixed
     */
    public function scopeEntitiesByBranch($query, $branch_code)
    {
        return $query->where($this->getTable().'.branch_code', $branch_code);
    }

    /**
     * Scope to get entities by department
     * @param $query
     * @param $dept_code
     * @return mixed
     */
    public function scopeEntitiesByDepartment($query, $dept_code)
    {
        return $query->where($this->getTable().'.department_code', $dept_code);
    }

    /**
     * Scope to get only active branches
     * @param $query
     * @return mixed
     */
    public function scopeActiveBranch($query)
    {
        return $query->where('company_branches.active', 'Y');
    }
}