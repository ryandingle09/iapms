<?php
namespace App\Traits;

trait Lookupable
{

    /**
     * Scope to get lookup details
     * @param $query
     * @param $type
     * @param $lv_alias
     * @param $lv_code
     * @param bool $by_meaning
     * @return mixed
     */
    public function scopeLookupDetails($query, $type, $lv_alias, $lv_code, $by_meaning = false)
    {
        $query = $query->leftJoin(\DB::raw('ia_lookup_values as '.$lv_alias), function($j) use($lv_alias, $lv_code, $type, $by_meaning) {
                            if( !$by_meaning ) $j->on(\DB::raw($lv_alias.'.lookup_code'), '=', $lv_code);
                            else $j->on(\DB::raw($lv_alias.'.meaning'), '=', $lv_code);
                            $j->where(\DB::raw($lv_alias.'.lookup_type'), '=', $type);
                       })
                       ->addSelect( \DB::raw($lv_alias.'.description as '.$lv_alias.'_desc, '.$lv_alias.'.meaning as '.$lv_alias.'_meaning, '.$lv_alias.'.lookup_code as '.$lv_alias.'_code') );

        return $query;
    }
}