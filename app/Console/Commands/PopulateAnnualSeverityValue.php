<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artesaos\Defender\Permission;
use App\Services\AdjustedSeverityValueService;
use App\Models\RiskAssessmentScale;
use App\Models\RiskAssessmentScaleAnnualAsv;
use App\Models\AuditableEntitiesActual;


class PopulateAnnualSeverityValue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iapms:pasv 
                            {year : Year range to populate}
                            {limit=all : master_auditable_entity_id for single populate}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate annual adjusted severity value';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(AdjustedSeverityValueService $asv_service)
    {
        // Get all arguments
        $year = (int) $this->argument('year');
        $limit = $this->argument('limit');

        // Validating arguments
        if( ( date("Y") + 50 ) < $year || $year < date("Y") ){
            $this->info("Invalid year input.");
            exit;
        }

        // Fetching RAS Data
        $ras_data = null;

        if($limit == "all"){
            $ras_data = RiskAssessmentScale::where('type', 'Actual')->get();
            RiskAssessmentScaleAnnualAsv::where( 'year', $year )->delete();
        }else{
            $aea_ids = AuditableEntitiesActual::where('master_auditable_entity_id', $limit )
                    ->pluck('auditable_entity_id');
            $ras_data = RiskAssessmentScale::where('type', 'Actual')
                    ->whereIn('auditable_entity_id' ,  $aea_ids )
                    ->get();
            if(!$ras_data->count()){
                $this->info("No data found for ". $limit .".");
                exit;
            }
            RiskAssessmentScaleAnnualAsv::where( 'year', $year )
                    ->whereIn('auditable_entity_id' ,  $aea_ids )
                    ->delete();
        }
        
        $array = [];
        $date = date('Y-m-d H:i:s');

        $asv_service->setYear($year);

        $bar = $this->output->createProgressBar( count($ras_data) );

        foreach ($ras_data as $ras) {
            
            $adjusted_severity_value = $asv_service->getTotalAdjustedSevVal(
                $year,
                $ras->auditable_entity_id,
                $ras->ae_mbp_id,
                $ras->bp_id,
                $ras->bp_objective_id,
                $ras->bp_steps_id,
                $ras->risk_id,
                $ras->type
            );

            $attributes =  [
                'year' => $year,
                'auditable_entity_id' => $ras->auditable_entity_id,
                'ae_mbp_id' => $ras->ae_mbp_id,
                 'bp_id' => $ras->bp_id,
                'bp_objective_id' => $ras->bp_objective_id,
                'bp_steps_id' => $ras->bp_steps_id,
                'risk_id' => $ras->risk_id,
                'adjusted_severity_value' => $adjusted_severity_value,
                'created_by' => 1,
                'created_date' => $date,
                'last_update_by' => 1,
                'last_update_date' => $date
            ]; 

            RiskAssessmentScaleAnnualAsv::insert($attributes);   

            $bar->advance();
        }

        $bar->finish();

        $this->info("\n\nProccess complete.");
    }

   
}
