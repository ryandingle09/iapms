<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Artesaos\Defender\Permission;
use App\Services\AdjustedSeverityValueService;
use App\Models\RiskAssessmentScale;
use App\Models\RiskAssessmentScaleAnnualAsv;
use App\Models\AuditableEntitiesActual;


class PopulateMainBusinessProccessActual extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'iapms:cascade-mbpa
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cascade main business proccess actual from master';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(AdjustedSeverityValueService $asv_service)
    {
        
        $this->info("\n\nProccess complete.");
    }

   
}
