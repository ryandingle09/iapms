<?php

namespace App\Exceptions;

use Artesaos\Defender\Exceptions\ForbiddenException;
use Artesaos\Defender\Contracts\ForbiddenHandler as ForbiddenHandlerContract;

class DefenderHandler implements ForbiddenHandlerContract
{
    /**
     * Handle Forbidden by Defender
     * @return page
     */
    public function handle()
    {
        // throw new ForbiddenException;
        return response()->view('errors.404');
    }
}
