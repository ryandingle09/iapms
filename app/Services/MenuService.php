<?php

namespace App\Services;

use App\Models\Menu;
use App\Repositories\MenuRepository;
use Auth, Cache;

class MenuService
{

	private $cache_list_name = "sidemenu_cache_list";
	private $repository;

	function __construct( MenuRepository $menu ){
    	$this->repository = $menu;
    }

	/**
	 * Get the menus
	 * @param  array  $allow (Optional) List of allowed menu
	 * @return object
	 */
	public function getMenu($allow = array())
	{
		$menu = Menu::tree([25]);
    	return $menu;
	}


	/**
	 * Gets sidebar menu of logged on user based on his roles
	 * @return view
	 */
	public function getUserMenu(){
		$user_roles = Auth::user()->roles->lists('id')->toArray();
		sort($user_roles);
		$cache_key = implode("_", $user_roles );
		if(!Cache::has($cache_key)){
			$allowed = Auth::user()->allowedMenu();
			$allowed = $allowed ? $allowed : [0] ;
			$sidemenu = $this->getMenuNestableTemplate('includes.nested-sidemenu',[],[],$allowed)->render();
			Cache::put($cache_key, $sidemenu , 120 );
			$this->registerKey($cache_key);
		}
		return Cache::get($cache_key);
	}

	/**
	 * Add new cache key to  cache list
	 * @return void
	*/
	public function getMenuNestableTemplate($template,$childrens = [],$params =[],$allowed = []){
		$menus = Menu::tree($allowed);
		if($childrens){
			$menus = $childrens;
		};
		return view($template, compact("menus",'params','allowed'));		
	}

	/**
	 * Add new cache key to  cache list
	 * @return void
	*/
	public function registerKey($string){
		$list = $this->allKey();
		array_push($list,$string);
		Cache::put( $this->cache_list_name  , implode(",", $list) , 120 ) ;
	}

	/**
	 * Destroy all cache related to menu
	 * @return void
	*/
	public function clearCache(){
		foreach ($this->allKey() as $key => $value) {
			Cache::forget($value);
		}
		Cache::forget( $this->cache_list_name );
	}

	/**
	 * Get all key registered in menu cache
	 * @return [array] list
	*/

	public function allKey(){
		$cache_list = Cache::get( $this->cache_list_name );
		$list = [];
		if( $cache_list ) $list = explode(",", $cache_list );
		return $list;
	}
}
