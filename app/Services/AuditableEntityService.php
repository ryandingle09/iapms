<?php

namespace App\Services;

use App\Criteria\AuditableEntityMasterDetailsCriteria;
use App\Criteria\AuditableEntityParentCriteria;
use App\Criteria\EntityMasterCompanyCriteria;
use App\Criteria\JoinAuditableEntityBPCriteria;
use App\Criteria\JoinAuditableEntityParentCriteria;
use App\Criteria\JoinBusinessProcessCriteria;
use App\Criteria\JoinLookupValuesCriteria;
use App\Http\Requests\Request;
use App\Models\AuditableEntity;
use App\Repositories\AuditableEntityRepository;
use App\Services\LookupService;
use App\Transformers\AuditableEntityTransformer;
use Illuminate\Database\Eloquent\Collection;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection as FractalCollection;
use Log;
use Yajra\Datatables\Datatables;

class AuditableEntityService
{
    private $auditable_entities;
    private $lookup_service;
    private $bp_service;
    public $master_companies;
    public $lookup_codes;

    public function __construct(AuditableEntityRepository $auditable_entities, LookupService $lookup, BusinessProcessService $bp_service)
    {
        $this->auditable_entities = $auditable_entities;
        $this->lookup_service = $lookup;
        $this->bp_service = $bp_service;
        $this->master_companies = [];
        $this->lookup_codes = [
            'auditable_entities.company_code' => config('iapms.lookups.company'),
            'auditable_entities.group_type' => config('iapms.lookups.group_type'),
            'auditable_entities.branch_code' => config('iapms.lookups.branch.code'),
            'auditable_entities.entity_class' => config('iapms.lookups.entity_class'),
            'auditable_entities.department_code' => config('iapms.lookups.dept_code'),
            'auditable_entities.business_type' => config('iapms.lookups.business_type'),
        ];
    }

    public function getEntities($id = null, $descriptions = false)
    {
        if( \Request::get('draw') ) {
            // skip all criterias if the request is from datatables
            $this->auditable_entities->skipCriteria();
        }
        if(\Request::get('descriptions') == true || $descriptions) {
            $this->auditable_entities->pushCriteria(new JoinLookupValuesCriteria( $this->lookup_codes ));
        }
        if( !is_null($id) )
            $entities = $this->auditable_entities->find($id);
        else
            $entities = $this->auditable_entities->all();

        return $entities;
    }

    public function attachParent($parent = false, $parent_id = '')
    {
        $is_parent = !$parent ? 'N' : 'Y';
        $this->auditable_entities->pushCriteria(new AuditableEntityParentCriteria($parent_id, $is_parent));

        return $this;
    }

    public function syncAuditableEntityTemplate(AuditableEntity $auditable_entity, $branch_code)
    {
        $group = $auditable_entity->group_type;
        $business = $auditable_entity->business_type;
        $entity = $auditable_entity->entity_class;
        $department = $auditable_entity->department_code;

        $to_sync = $this->auditable_entities->getRelatedEntity( $group, $business, $entity, $branch_code, $department );

        if( $to_sync->count() ) {
            $business_process = $auditable_entity->bps;
            foreach ($to_sync->get() as $ae) {
                if( !is_null($ae->bps) ) $ae->bps->detach(); // remove all the existing business processes attach
                $this->replicateBusinessProcess($business_process, $ae->auditable_entity_id);
            }
        }
    }

    // replicate BusinessProcess and replace id if exists
    public function replicateBusinessProcess(Collection $collection, $ae_id = '')
    {
        foreach ($collection as $model) {
            $business_process = $model->replicate();
            $business_process->auditable_entity_id = $ae_id;
            $business_process->save();
        }
    }

    /**
     * Create copy of actual auditable entity with different branch codes
     * @param  AuditableEntity $auditable_entity
     * @param  mixed          $branch_code
     * @return void
     */
    public function replicateAuditableEntity($parent_id, $parent_name, $group_type, $business_type, $branch_code)
    {
        #TODO: Add exception
        list($company_code, $company_short, ) = explode('.', $parent_name);
        $entity_class = $this->getBranchEntityClass($branch_code);
        $branch_info = $this->lookup_service->getLookupValueByCode( config('iapms.lookups.branch.code'), $branch_code ); // get branch info from lookup

        $master = $this->getMasterDetails($group_type, $entity_class, $business_type);

        Log::info('[Auditable Entity] Replicating - '.$branch_code.' - '.$branch_info->meaning.' - '.$master->count());
        foreach ($master as $entity) {
            $entity_name = $company_code.'.'.$company_short.'.'.$branch_info->meaning.'.'.$entity->department_code; // entity name unique naming convention
            $exists = $this->auditable_entities->findByField('auditable_entity_name', $entity_name);
            if(!$exists->count()) {
                $attributes = [
                    'auditable_entity_name' => $entity_name,
                    'company_code' => $company_code,
                    'group_type' => $entity->group_type,
                    'business_type' => $entity->business_type,
                    'entity_class' => $entity->entity_class,
                    'branch_code' => $branch_code,
                    'department_code' => $entity->department_code,
                    'parent_entity' => 'N',
                    'parent_ae_id' => $parent_id,
                ];
                $bp_attr = $entity->businessProcess->pluck('bp_id')->toArray(); // get business processes attached to the master

                $new_entity = $this->auditable_entities->create($attributes);
                $new_entity->businessProcess()->attach($bp_attr); // cascades master's business processes to the newly created entity

                Log::info('[Auditable Entity] Replicated - '.$entity_name);
            }
        }

        return true;
    }

    /**
     * Get all master details
     * @param  string $group_type
     * @param  string $entity_class
     * @param  string $business_type
     * @return Collection
     */
    public function getMasterDetails($group_type, $entity_class, $business_type)
    {
        $this->auditable_entities->resetCriteria();
        $this->auditable_entities->pushCriteria( new AuditableEntityMasterDetailsCriteria($group_type, $entity_class, $business_type) ); // get the related master details
        return $this->auditable_entities->all();
    }

    public function getBranchEntityClass($branch_code)
    {
        $entity_class = 'Office';
        $head_office = $this->lookup_service->getHeadOffice();
        if( !in_array($branch_code, $head_office) ) {
            $entity_class = 'Store';
        }

        return $entity_class;
    }

    public function getTemplate($group, $business, $entity, $branch_code, $department)
    {
        return $this->auditable_entities->getRelatedEntity( $group, $business, $entity, $branch_code, $department, 'Y' )->first();
    }

    public function getEntityList()
    {
        $auditable_entities = $this->getEntities(null, true);
        if(\Request::get('transform') == true) {
            $fractal = new Manager();
            $data = new FractalCollection($auditable_entities, new AuditableEntityTransformer);
            if( \Request::get('include') != '' ) $fractal->parseIncludes(\Request::get('include'));
            $auditable_entities = $fractal->createData($data)->toArray();
        }
        if(\Request::get('json')  == true) {
            return response()->json([ 'success' => true, 'data' => $auditable_entities]);
        }
        else
            return Datatables::of($auditable_entities)->make(true);
    }

    public function getEntityBusinessProcesses($ae_id = null, $bp_id = null)
    {
        $this->auditable_entities->pushCriteria(new JoinAuditableEntityParentCriteria());
        $this->auditable_entities->pushCriteria(new JoinAuditableEntityBPCriteria());
        $this->auditable_entities->pushCriteria(new JoinBusinessProcessCriteria( 'auditable_entities_bp', $bp_id )); // join to business process
        $this->auditable_entities->pushCriteria(new JoinLookupValuesCriteria( [
            'business_processes.bp_code' => config('iapms.lookups.business_process'),
            'business_processes.objective_category' => config('iapms.lookups.objective.category'),
            'business_processes.source_type' => config('iapms.lookups.source_type'),
        ] ));
        return $this->getEntities($ae_id, true);
    }

    public function getMasterCompany($group_type, $entity_class, $department, $business)
    {
        $this->auditable_entities->pushCriteria(new EntityMasterCompanyCriteria($group_type, $entity_class, $department, $business));
        return $this->auditable_entities->first();
    }

    /**
     * Get the master companies from Lookup
     * @param boolean $code_only
     * @return array
     */
    function getMasterCompanyFromLookup($code_only = true) {
        $master_company = config('iapms.lookups.master_company');
        $lookup = $this->lookup_service->getLookup( $master_company );

        if( $code_only )
            return !is_null($lookup) ? $lookup->lookupValue->pluck('lookup_code')->toArray() : [];
        else
            return $lookup->lookupValue;
    }

    /**
     * Calculates the percentile rank
     * Formula: (score position / total count) * 100
     * @param  array $data
     * @param  integer $score
     * @return integer
     */
    public function calculatePercentile($data, $score)
    {
        $count = count($data);
        $position = array_search($score, $data);
        if( $position !== false && (is_null($score) || $score > 0) ) return number_format((($position + 1) / $count) * 100, 1); // $position + 1 to get actual, because the position is 0 based index

        return 0; // default percentile
    }

    public function setMasterCompanies()
    {
        $this->master_companies = $this->getMasterCompanyFromLookup();
        return $this;
    }

    function getBpOfMasterCompany(AuditableEntity $entity) {
        $bp = [];
        if( $entity->parent_entity != 'Y' || in_array($entity->company_code, $this->master_companies) ) {
            if(in_array($entity->company_code, $this->master_companies)) {
                $master_company = $entity;
            }
            else{
                $master_company = $this->getMasterCompany($entity->group_type, $entity->entity_class, $entity->department_code, $entity->business_type);
            }

            if( !is_null($master_company) ) {
                $bp = $this->bp_service->getAuditableEntityBp($master_company);
            }
        }
        return $bp;
    }

    public function getEntityBusinessProcessCustomMeasures(AuditableEntity $entity, $bp_id)
    {
        // $custom_measures = $entity->businessProcess()->where('auditable_entities_bp.bp_id', $bp_id)->first();
        $custom_measures = $this->setMasterCompanies()->getBpOfMasterCompany($entity)->find($bp_id);

        return $custom_measures;
    }
}