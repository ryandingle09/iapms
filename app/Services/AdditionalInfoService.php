<?php

namespace App\Services;

use App\Models\AdditionalInfoMaster;
use App\Models\LookupType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Log;

class AdditionalInfoService
{
    public $from;
    public $where;
    public $attributes;

    public function __construct()
    {
        $this->from = '';
        $this->where = '';
        $this->attributes = array();
    }

    public function testQuery()
    {
        $query = $this->runQuery();

        if( !$query )
            return ['success' => false, 'message' => 'You have an error on your query, please check.'];
        else
            return ['success' => true, 'message' => count($query)];
    }

    public function runQuery()
    {
        try {
            $select = 'SELECT '.$this->attributes.' FROM '.$this->from.' '.$this->where;
            $query = \DB::select( \DB::raw($select) );

            return $query;
        }
        catch( \Exception $e ) {
            Log::error('[ValueSet][Test]: '.$e->getMessage());
            return false;
        }
    }

    public function setFrom($from)
    {
        $this->from = env('DB_PREFIX').$from;
        return $this;
    }

    public function setWhere($where)
    {
        $this->where = $where;
        return $this;
    }

    public function setAttributes($attributes = array())
    {
        $this->attributes = count($attributes) ? implode(',', $attributes) : '*';;
        return $this;
    }

    public function generateAdditionalInfo(AdditionalInfoMaster $additional_info, $table = null, $id = null)
    {
        if( $additional_info->details->count() ) {
            $values = [];
            $cnt = 1;
            $index = 0;
            if( !is_null($table) ) {
                $e_table = 'auditable_entities';
                $orig_table = $table;
                $entity = [$e_table, $e_table.'_actual'];
                $lookup = LookupType::where('lookup_type', config('iapms.lookups.primary_keys'))->first();

                if( in_array($table, $entity) ) $table = $e_table; // entity master and actual has the same ID

                $primary_key = $lookup->lookupValue()->where('lookup_code', $table)->first();
                if( !is_null($primary_key) ) {
                    $values = \DB::table($orig_table)
                                 ->where($primary_key->meaning, $id)
                                 ->first();
                }
            }
            $html = '<table class="table table-striped table-bordered"><tbody>';
            foreach ($additional_info->details as $details) {
                $required = $details->required == 'Y' ? 'required' : '';
                $html .= '<tr><th>'.$details->prompt.'</th>';
                if( $details->value_set_id != 0 ) {
                    $value_set = $details->valueSet;
                    $attributes = [ $value_set->code, $value_set->meaning ];
                    $value_set_data = $this->setFrom($value_set->from_clause)
                                           ->setWhere($value_set->where_clause)
                                           ->setAttributes($attributes)
                                           ->runQuery();
                       $html .= '<td><select name="additional_info[value]['.$index.']" class="form-control input-sm basic-info" '.$required.'>';
                       $html .= '<option value="">---</option>';

                    foreach($value_set_data as $data){
                         // check if there is table included in the field
                        $code = strpos($value_set->code, '.') !== false ? explode('.', $value_set->code) : $value_set->code;
                        $meaning = strpos($value_set->meaning, '.') !== false ? explode('.', $value_set->meaning) : $value_set->meaning;
                        $code = is_array($code) ? $code[1] : $code;
                        $meaning = is_array($meaning) ? $meaning[1] : $meaning;
                        $selected = !empty($values) ? ($values->{'attribute'.$cnt} == $data->{$code} ? 'selected' : '') : '';
                        $html .= '<option value="'.$data->{$code}.'" '.$selected.'>'.$data->{$meaning}.'</option>';
                    };
                   $html .= '</select></td></tr>';
                }
                else {
                    $class = '';
                    $type = '';
                    $attr = '';
                    $name = '';
                    switch ($details->data_type) {
                        case 'integer':
                            $type = 'number';
                            $name = 'name="additional_info[value]['.$index.']"';
                            break;
                        case 'date':
                            $class = 'date-picker';
                            $attr = 'data-date-format="dd-M-yyyy"';
                            $name = 'name="additional_info[value]['.$index.']"';
                            break;
                        default:
                            $type = 'text';
                            $name = 'name="additional_info[value]['.$index.']"';
                            break;
                    }
                    $value = !empty($values) ? $values->{'attribute'.$cnt} : '';

                    $html .= '<td><input '.$name.' type="'.$type.'" class="form-control input-sm basic-info '.$class.'" value="'.$value.'" '.$attr.' '.$required.'></td></tr>';
                }

                $cnt++;
                $index++;
            }
            $html .= '</tbody></table>';
        }

        else {
            $html = '<div class="alert alert-info"><strong><i class="ace-icon fa fa-info"></i></strong> No additional info available</div>';
        }

        return $html;
    }

    public function saveAdditionalInfo(Model $model)
    {
        $infos = \Request::get('additional_info'); // get all the info set by the user
        if( count($infos['value']) ) {
            for ($i=0; $i < count($infos['value']); $i++) {
                $field = 'attribute'.($i+1);
                $model->{$field} = $infos['value'][$i]; // set the new additional info to respective model
            }
            return $model->save();
        }
        return false;
    }
}