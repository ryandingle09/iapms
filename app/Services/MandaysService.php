<?php
namespace App\Services;
use App\Repositories\HolidayRepository;
use App\Criteria\CurrentYearHolidayCriteria;
use Carbon\Carbon;

class MandaysService
{
	protected $audit_year;
	protected $working_days;
	protected $holiday;

	public function __construct(
		HolidayRepository $holiday
	){
		$this->holiday = $holiday;
	}

	public function setAuditYear($year = null)
    {
        if( is_null($year) ) $year = date('Y'); // set current year if parameter is null

        $this->audit_year = $year;
        return $this;
    }

	public function getWorkingDays($audit_year)
    {
        $start_of_year = Carbon::parse('01-01-'.$audit_year);
        $end_of_year = Carbon::parse('31-12-'.$audit_year);
        return $start_of_year->diffInWeekdays($end_of_year); // total number of weekdays in selected year
    }

    public function getHolidays($audit_year)
    {
        $total_holidays = 0;
        $this->holiday->pushCriteria(new CurrentYearHolidayCriteria($audit_year));
        $holidays = $this->holiday->all()->count();
        return $holidays;
    }

    public function getTotalWorkingDay($audit_year){
    	$working_days = $this->getWorkingDays($audit_year);
        $holidays = $this->getHolidays($audit_year);
        return $working_days - $holidays ;
    }


    public function calculateAuditorProjectMandays($plan_id, $group_id = null, $project_id = null, $auditor_id = null){
        $conditions['p.plan_id'] = $plan_id;

        if($plan_id != null) $conditions['p.plan_id'] = $plan_id;

        if($group_id) $conditions['pp.plan_group_id'] = $group_id;

        if($project_id != null) $conditions['pp.plan_project_id'] = $project_id;

        if($auditor_id) $conditions['ppsa.auditor_id'] = $auditor_id;

        $auditor_project_mandays = $this->auditorMandays($conditions)->first();

        return $auditor_project_mandays->allotted_mandays ?: 0 ;
    }

    public function auditorMandays($conditions){
        $query = \DB::table('plan as p')
                            ->join('plan_groups as pg', 'pg.plan_id', '=','p.plan_id')
                            ->join('plan_projects as pp', 'pp.plan_group_id', '=','pg.plan_group_id')
                            ->join('plan_project_scope as pps', 'pps.plan_project_id', '=','pp.plan_project_id')
                            ->join('plan_project_scope_auditors as ppsa', 'ppsa.plan_project_scope_id', '=','pps.plan_project_scope_id');

        foreach($conditions as $key => $value){
            $query = $query->where( $key, '=', $value );
        }
        $query->addSelect(
            \DB::raw('IF( SUM( ia_ppsa.allotted_mandays) , SUM(ia_ppsa.allotted_mandays) , 0) AS allotted_mandays')
        );
        return $query;
    }

    /*
    * Copy from mbp_budgeted_mandays_all() stored procedure 
    * Get the main business process budgeted mandays for all the distinct test procedures
    * irregardless of the objective type
    * modification: 02-jun-2017 - first version
    *               02-oct-2017 - made modifications to include ia_control_master and parameter.
    */

    public function mbpBudgetedMandaysAll( $type, $ae_mbp_id, $audit_type ){
        $add_budgeted_mandays = 0;
        $mbp_budgeted_mandays = 0;

        if($type == "Actual"){
            $mbpa = \App\Models\AeMbpActual::select(
                            \DB::raw('( walk_thru_mandays + 
                         planning_mandays + 
                         reviewer_mandays + followup_mandays + 
                         discussion_mandays + 
                         draft_dar_mandays + 
                         final_report_mandays + 
                         wrap_up_mandays + 
                         reviewer_mandays) as additional_budgeted_mandays')
                        )->where('ae_mbp_id',$ae_mbp_id)
                        ->first();
            if($mbpa){
                $add_budgeted_mandays = $mbpa->additional_budgeted_mandays;
            }
        }

        $tot_mbp_budgeted_mandays = $mbp_budgeted_mandays + $add_budgeted_mandays;

        return $tot_mbp_budgeted_mandays;
    }

    public function getMbpBudgetedMandays($master_ae_mbp_id, $audit_type)
    {
        $budgeted_mandays =  collect(\DB::select("
            SELECT 
            ctp.test_proc_id, 
            max(ctp.budgeted_mandays) budgeted_mandays 
            from ia_ae_mbp aembp, 
            ia_ae_mbp_sbp aembpsbp,
            ia_business_processes sbp, 
            ia_bp_objectives sbpo, 
            ia_business_processes_steps sbps,
            ia_bp_steps_risks sbpsr, 
            ia_risks r, 
            ia_risks_control rc, 
            ia_controls_master cm, 
            ia_controls_test_proc ctp, 
            ia_controls_test_audit_types ctat 
            WHERE aembp.ae_mbp_id = $master_ae_mbp_id 
            AND aembpsbp.ae_mbp_id = aembp.ae_mbp_id 
            AND sbp.bp_id = aembpsbp.bp_id 
            AND sbpo.bp_id = sbp.bp_id 
            AND sbps.bp_objective_id = sbpo.bp_objective_id 
            AND sbpsr.bp_step_id = sbps.bp_steps_id 
            AND r.risk_id = sbpsr.risk_id 
            AND rc.risk_id = r.risk_id 
            AND cm.control_id = rc.control_id 
            AND ctp.control_id = cm.control_id 
            AND ctat.control_test_id = ctp.control_test_id 
            AND ctat.audit_type = '$audit_type' 
            GROUP BY ctp.test_proc_id"))->sum('budgeted_mandays'); 

        return $budgeted_mandays;
    }

    public function getTestProcedureBudgetedMandays($master_ae_mbp_id, $audit_type)
    {
        $budgeted_mandays =  collect(\DB::select("
            SELECT 
            ctp.test_proc_id, 
            max(ctp.budgeted_mandays) budgeted_mandays 
            from ia_ae_mbp aembp, 
            ia_ae_mbp_sbp aembpsbp,
            ia_business_processes sbp, 
            ia_bp_objectives sbpo, 
            ia_business_processes_steps sbps,
            ia_bp_steps_risks sbpsr, 
            ia_risks r, 
            ia_risks_control rc, 
            ia_controls_master cm, 
            ia_controls_test_proc ctp, 
            ia_controls_test_audit_types ctat 
            WHERE aembp.ae_mbp_id = $master_ae_mbp_id 
            AND aembpsbp.ae_mbp_id = aembp.ae_mbp_id 
            AND sbp.bp_id = aembpsbp.bp_id 
            AND sbpo.bp_id = sbp.bp_id 
            AND sbps.bp_objective_id = sbpo.bp_objective_id 
            AND sbpsr.bp_step_id = sbps.bp_steps_id 
            AND r.risk_id = sbpsr.risk_id 
            AND rc.risk_id = r.risk_id 
            AND cm.control_id = rc.control_id 
            AND ctp.control_id = cm.control_id 
            AND ctat.control_test_id = ctp.control_test_id 
            AND ctat.audit_type = '$audit_type' 
            GROUP BY ctp.test_proc_id"))->sum('budgeted_mandays'); 

        return $budgeted_mandays;
    }

    public function getMandaysBreakdown($plan_id,$id){
        $data = collect(\DB::select("SELECT 
                    ppsa.auditor_type,
                    SUM(ppsa.allotted_mandays) AS allotted_mandays
                    FROM ia_plan p 
                    JOIN ia_plan_groups pg ON pg.plan_id = p.plan_id
                    JOIN ia_plan_projects pp ON pp.plan_group_id = pg.plan_group_id
                    JOIN ia_plan_project_scope pps ON pps.plan_project_id = pp.plan_project_id
                    JOIN ia_plan_project_scope_auditors ppsa ON ppsa.plan_project_scope_id = pps.plan_project_scope_id
                    WHERE p.plan_id = $plan_id 
                    AND ppsa.auditor_type IS NOT NULL
                    AND ppsa.auditor_id = $id 
                    GROUP BY ppsa.auditor_type")
                    );
        return $data;
    }
    
    /*
    * Copy from mbp_budgeted_mandays_apg() stored procedure 
    * Get the main business process apg budgeted mandays
    * modification: 19-oct-2017 - first version sir noel
    */
    public function mbpBudgetedMandaysApg( $mbpa_id, $audit_type ){
        $data = collect(\DB::select("SELECT 
                          IFNULL(SUM(tpbm.budgeted_mandays),0) AS budgeted_mandays 
                        FROM
                          (SELECT 
                            ctp.test_proc_id,
                            MAX(ctp.budgeted_mandays) budgeted_mandays 
                          FROM
                            ia_ae_mbp_actual aembpa,
                            ia_ae_mbp aembp,
                            ia_ae_mbp_sbp aembpsbp,
                            ia_business_processes sbp,
                            ia_bp_objectives sbpo,
                            ia_business_processes_steps sbps,
                            ia_bp_steps_risks sbpsr,
                            ia_risks r,
                            ia_risks_control rc,
                            ia_controls_master cm,
                            ia_controls_test_proc ctp,
                            ia_controls_test_audit_types ctat 
                          WHERE aembpa.ae_mbp_id = $mbpa_id 
                            AND aembp.ae_mbp_id = aembpa.master_ae_mbp_id 
                            AND aembpsbp.ae_mbp_id = aembp.ae_mbp_id 
                            AND sbp.bp_id = aembpsbp.bp_id 
                            AND sbpo.bp_id = sbp.bp_id 
                            AND sbps.bp_objective_id = sbpo.bp_objective_id 
                            AND sbpsr.bp_step_id = sbps.bp_steps_id 
                            AND r.risk_id = sbpsr.risk_id 
                            AND rc.risk_id = r.risk_id 
                            AND cm.control_id = rc.control_id 
                            AND ctp.control_id = cm.control_id 
                            AND ctat.control_test_id = ctp.control_test_id 
                            AND ctat.audit_type = '$audit_type' 
                          GROUP BY ctp.test_proc_id) tpbm" ) )->first();
        return $data ? $data->budgeted_mandays : 0;
    }

    /*
    * Copy from project_allotted_mandays_misc() stored procedure 
    * Get the project scope auditor's miscellaneous allotted mandays
    * modification: 19-oct-2017 - first version sir noel
    */
    public function projectAllottedMiscMandays($project_id = null, $project_scope_id = null, $auditor_id = null ) 
    {
        $query = \DB::table('projects AS p')
                    ->join('project_scope AS ps' , 'ps.project_id','=','p.project_id')
                    ->join('project_scope_auditors AS psa' , 'psa.project_scope_id','=','ps.project_scope_id');

        if($project_id != null) $query->where( 'p.project_id' , $project_id );

        if($project_scope_id != null) $query->where( 'ps.project_scope_id' , $project_scope_id );

        if($auditor_id != null) $query->where( 'psa.auditor_id' , $auditor_id );

        $data = $query->addSelect( \DB::raw('IFNULL( SUM( ia_psa.allotted_misc_mandays) , 0) AS allotted_misc_mandays') )->first();

        return $data ? $data->allotted_misc_mandays : 0;
    }

    /*
    * TODO
    * Copy from project_allotted_mandays_apg() stored procedure 
    * Get the project scope auditor's miscellaneous allotted mandays
    * modification: 19-oct-2017 - first version sir noel
    */

    public function projectAllottedMandaysApg($project_id = null, $project_scope_id = null, $auditor_id = null )
    {
        $query = \DB::table('projects AS p')
                    ->join('project_scope AS ps' , 'ps.project_id','=','p.project_id')
                    ->join('project_scope_apg AS psapg' , 'psapg.project_scope_id','=','ps.project_scope_id');

        if($project_id != null) $query->where( 'p.project_id' , $project_id );

        if($project_scope_id != null) $query->where( 'ps.project_scope_id' , $project_scope_id );

        if($auditor_id != null) $query->where( 'psapg.auditor_id' , $auditor_id );

        $data = $query->addSelect( \DB::raw('IFNULL( SUM( ia_psapg.allotted_mandays) , 0) AS allotted_mandays') )->first();

        return $data ? $data->allotted_mandays : 0;
    }
}

