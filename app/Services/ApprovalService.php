<?php

namespace App\Services;
use Auth,DB;

class ApprovalService
{
	protected $user;

	public $auditor_type;

	public $current_status;

	public $ref_id;

	public function __construct()
	{
		$this->user = Auth::user();
	}

	public function setCurrentStatus($status)
	{
		$this->current_status = $status;
	}

	public function getNextStatus($key , $ref_id)
	{
		$this->ref_id = $ref_id;
		$next_status = [];
		switch($key){
			case 'plan_project' :
				$next_status = $this->getNextStatusByPlanProject();
				break;
			case 'plan' :
				$next_status = $this->getNextStatusByPlan();
				break;
			default:
				break;
		}
		return $next_status;
	}

	public function getNextStatusByPlanProject(){
		$statuses = [];
		if($this->user->user_tag == "Auditor"){
			$auditor_id = $this->user->auditor->auditor_id;
			$plan_project_auditor = \App\Models\PlanProjectAuditor::where( 'auditor_id', $auditor_id )
												->where( 'plan_project_id', $this->ref_id )
												->first();
			if($plan_project_auditor){
				$this->auditor_type = $plan_project_auditor->auditor_type;
				$statuses = $this->getNextStatusQuery('Annual Audit Plan', 'Project' , $this->auditor_type , $this->current_status );
			}
		}
		return $statuses;
	}

	public function getNextStatusByPlan(){
		$statuses = [];
		if($this->user->user_tag == "Auditor"){
			$this->auditor_type = $this->user->auditor->position_code;
			$statuses = $this->getNextStatusQuery('Annual Audit Plan', 'Plan' , $this->auditor_type , $this->current_status );
		}
		return $statuses;
	}

	public function getNextStatusQuery( $category = null ,$subcategory = null , $auditor_type = null , $current_status = null ){
		$results = DB::select("
					SELECT 
					  source_category,
					  source_subcategory,
					  auditor_type,
					  ats.status
					FROM
					  ia_approval_setup_masters am
					  JOIN ia_approval_setup_details ad ON ad.approval_setup_master_id = am.approval_setup_master_id
					  JOIN ia_approval_from_status afs ON afs.approval_setup_detail_id = ad.approval_setup_detail_id
					  JOIN ia_approval_to_status ats ON ats.approval_setup_detail_id = afs.approval_setup_detail_id
					  WHERE source_category = '$category'
					  AND source_subcategory = '$subcategory'
					  AND auditor_type = '$auditor_type'
					  AND afs.status = '$current_status'

				");
		return $results ;
	}

	public function getAuditorType(){
		return $this->auditor_type;
	}
}