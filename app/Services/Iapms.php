<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Support\Collection as CollectionArray;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection as FractalCollection;
use League\Fractal\Resource\Item as FractalItem;
use League\Fractal\TransformerAbstract;

class Iapms
{
    private $manager;
    private $includes;

    public function __construct()
    {
        $this->manager = new Manager();
    }

    public function transformCollectionArray(CollectionArray $data, TransformerAbstract $transformer)
    {
        if( !$data->count() ) return null;
        $response = [];
        foreach ($data as $item) {
            $trans = $this->transformItem($item, $transformer)['data'];
            $response[] = $trans;
        }

        return ['data' => $response];
    }


    public function transformCollection(Collection $data, TransformerAbstract $transformer)
    {
        $data = new FractalCollection($data, $transformer);
        $this->parseIncludes();
        return $this->manager->createData($data)->toArray();
    }

    public function transformItem(Model $model, TransformerAbstract $transformer)
    {
        $data = new FractalItem($model, $transformer);
        $this->parseIncludes();
        return $this->manager->createData($data)->toArray();
    }

    private function parseIncludes()
    {
        $includes = \Request::get('include') != '' ? \Request::get('include') : $this->includes;
        if( $includes ) $this->manager->parseIncludes($includes);
    }

    public function setIncludes($include = '')
    {
        $this->includes = $include;
        return $this;
    }
}
