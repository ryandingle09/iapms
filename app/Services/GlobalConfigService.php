<?php 
namespace App\Services;

use App\Models\GlobalConfig;
use Schema;
class GlobalConfigService
{
	private $model;

	function __construct( GlobalConfig $model )
    {
    	$this->model = $model;
    }

    function getData($column){
    	$data = $this->model->first();
    	if($column && $data){
    		return isset($data->$column) ? $data->$column : "";
    	}else{
    		return "";
    	}
    }

}