<?php

namespace App\Services;

class ObjectMapperService
{
	public $template ;

	public function __construct()
	{
		$this->template = "default";
	}

	public function setTemplate($template)
	{
		$this->template = $template;
		return $this;
	}

	public function map( $data )
	{
		if($data->count()){
			return array_map(function($item){
				return $this->transformer($item);
			}, $data->toArray() );
		}
	}

	public function transformer($item)
	{
		switch ($this->template) {
			case 'BPFromWith':
				return [
		            'id'         => $item['business_process']['bp_id'],
		            'text'       => $item['business_process']['bp_name'],
		            'code'       => $item['business_process']['bp_code']
		        ];
				break;

			case 'AuditorFromWith':
				return [
		            'id'         => $item['auditor']['auditor_id'],
		            'text'       => $item['auditor']['full_name'],
		            'position'   => $item['auditor']['position_code']
		        ];
				break;
			
			default:
				return [
		            'id'         => $item->id,
		            'text'       => $item->text,
		            'position'   => $item->position,
		        ];
				break;
		}
	}
}