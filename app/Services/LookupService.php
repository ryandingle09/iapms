<?php

namespace App\Services;

use App\Repositories\LookupTypeRepository;
use App\Repositories\LookupValueRepository;

class LookupService
{

    private $repository;
	private $lookup_value;

	function __construct( LookupTypeRepository $lookup, LookupValueRepository $lookup_value )
    {
        $this->repository = $lookup;
    	$this->lookup_value = $lookup_value;
    }

    public function getLookup($type)
    {
        $this->repository->skipCriteria(true);
        $this->repository->skipPresenter(true);
        return $this->repository
                    ->with('lookupValue')
                    ->findByField('lookup_type', $type)
                    ->first();
    }

    /**
     * Get head office codes define in the lookup
     * @return array
     */
    public function getHeadOffice()
    {
        $head_office = $this->getLookup( config('iapms.lookups.head_office') );

        if( !is_null($head_office) ) {
        	return $this->getLookup( config('iapms.lookups.head_office') )
                        ->lookupValue
                        ->pluck('lookup_code')
                        ->toArray();
        }

        return [];
    }

    public function getLookupValueByCode($type, $code)
    {
        $this->lookup_value->skipCriteria(true);
        $this->lookup_value->skipPresenter(true);
        return $this->lookup_value
                    ->findWhere([
                        'lookup_type' => $type,
                        'lookup_code' => $code,
                    ])
                    ->first();
    }

    public function getLookupValueByType($type)
    {
        $lookup = $this->getLookup($type);
        return !is_null($lookup) ? $lookup->lookupValue : [];
    }
}
