<?php
namespace App\Services;

use Session;

class PlanGuideCartService
{

	protected $tagname = 'plan_guide.data';

	protected function getData(){
		$data = Session::get( $this->tagname );
		if( ! $data ){
			$data = [];
		}
		return $data;
	}

	public function data(){
		return $this->getData();
	}

	public function insert($mbpa){
		$data = $this->getData();
		if( !in_array($mbpa, $data) ){
			array_push($data, $mbpa);
			$this->propData($data);
			return true;
		}else{
			return false;
		}
	}

	public function remove($mbpa){
		$data = $this->getData();
		if( false !== $key = array_search ($mbpa, $data) ){
        	unset($data[$key]);
        	$data = array_values($data);
			$this->propData($data);
			return true;
		}else{
			return false;
		}
	}

	public function destroy(){
		Session::forget( $this->tagname );
	}

	protected function propData($data){
		Session::put( $this->tagname, $data );
		Session::save();
	}


	
}