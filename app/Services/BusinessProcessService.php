<?php

namespace App\Services;

use App\Models\AuditableEntity;
use App\Models\BusinessProcessesStep;
use App\Repositories\BusinessProcessRepository;

class BusinessProcessService
{
    private $business_process;
    protected $lookup_codes;

    public function __construct(BusinessProcessRepository $business_process)
    {
        $this->business_process = $business_process;
        $this->lookup_codes = [
            'business_processes.bp_code' => config('iapms.lookups.business_process'),
            'business_processes.objective_category' => config('iapms.lookups.objective.category'),
            'business_processes.source_type' => config('iapms.lookups.source_type'),
        ];
    }

    public function getAuditableEntityBp(AuditableEntity $auditable_entity)
    {
        #TODO: Apply JoinLookupValues Criteria here
        return $auditable_entity->businessProcess()
                                ->select(\DB::raw('ia_business_processes.*, lv_bp_code.description bp_code_desc, lv_objective_category.description objective_category_desc, lv_source_type.description source_type_desc'))
                                ->leftJoin(\DB::raw('ia_lookup_values as lv_bp_code'), function($j){
                                    $j->on(\DB::raw('lv_bp_code.meaning'), '=', 'business_processes.bp_code')
                                      ->where(\DB::raw('lv_bp_code.lookup_type'), '=', $this->lookup_codes['business_processes.bp_code']);
                                })
                                ->leftJoin(\DB::raw('ia_lookup_values as lv_source_type'), function($j){
                                    $j->on(\DB::raw('lv_source_type.meaning'), '=', 'business_processes.source_type')
                                      ->where(\DB::raw('lv_source_type.lookup_type'), '=', $this->lookup_codes['business_processes.source_type']);
                                })
                                ->leftJoin(\DB::raw('ia_lookup_values as lv_objective_category'), function($j){
                                    $j->on(\DB::raw('lv_objective_category.meaning'), '=', 'business_processes.objective_category')
                                      ->where(\DB::raw('lv_objective_category.lookup_type'), '=', $this->lookup_codes['business_processes.objective_category']);
                                })
                                ->get();
    }

    public function getBusinessProcessStep($id = null)
    {
        if( !is_null($id) ) {
            $step = BusinessProcessesStep::with(['bpStepRisk' => function($j){
                $j->select(\DB::raw('ia_risks.*, lv_risk_code.description lv_risk_code_desc'))
                  ->join(\DB::raw('ia_lookup_values as lv_risk_code'), \DB::raw('lv_risk_code.meaning'), '=', 'risks.risk_code');
            }])->find($id);
        }
        else {
            $step = BusinessProcessesStep::get();
        }

        return $step;
    }

    public function attachCriteria()
    {
        $this->business_process->pushCriteria(new JoinLookupValuesCriteria( $this->lookup_codes ));
    }

    public function getBudgetedMandays($bp_id)
    {
        return \DB::select('select sum(default_mandays) mandays from (select sum(ictp.budgeted_mandays) default_mandays from
        ia_bp_objectives bpo
        left join ia_business_processes_steps bps
        on bpo.bp_objective_id = bps.bp_objective_id
        left join ia_bp_steps_risks bpsr
        on bps.bp_steps_id = bpsr.bp_step_id
        left join ia_risks_control irc
        on bpsr.risk_id = irc.risk_id
        left join ia_controls_test_proc ictp
        on irc.control_id = ictp.control_id
        where bpo.bp_id = '.$bp_id.' and irc.risk_id is not null
        group by bpo.bp_objective_id, irc.control_id, ictp.control_test_id) as default_budgeted_mandays');
    }
}
