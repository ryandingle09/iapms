<?php 
namespace App\Services;

use App\Models\GlobalConfig;
use App\Transformers\LookupValueTransformer;
use Schema;
class CosoService
{
    function getComponents(){
        $lookup = app(LookupService::class);
        $coso_lookup = $lookup->getLookupValueByType(config('iapms.lookups.control.component'));

        return \Iapms::setIncludes('child.child')->transformCollection($coso_lookup, new LookupValueTransformer());
    }

}