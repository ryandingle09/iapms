<?php

namespace App\Services;

use App\Criteria\AuditorMandaysCriteria;
use App\Criteria\CurrentYearHolidayCriteria;
use App\Criteria\PlanProjectAuditorMandaysCriteria;
use App\Criteria\PlanningAuditorCriteria;
use App\Criteria\ProjectAuditorMandaysCriteria;
use App\Models\Auditor;
use App\Models\global_config;
use App\Models\Plan;
use App\Models\PlanProject;
use App\Repositories\HolidayRepository;
use App\Repositories\PlanProjectRepository;
use App\Repositories\PlanRepository;
use App\Transformers\PlanProjectScopeTransformer;
use App\Transformers\PlanProjectTransformer;
use App\Transformers\PlanningAuditorTransformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection as EloqCollection;
use Illuminate\Database\Eloquent\Model;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Log;
use Prettus\Repository\Contracts\RepositoryInterface;

class PlanningService
{
    const SEC_PER_DAY = 86400;
    const DATE_INTERVAL = 1;

    public $audit_year;
    public $auditor_leaves;
    public $working_days;
    public $auditor;

    private $holiday;
    private $plans;
    private $project;
    private $running;
    private $mandays_relation;
    private $mandays_field;
    private $mandays_criteria;

    public function __construct( HolidayRepository $holiday, PlanRepository $plans, PlanProjectRepository $project )
    {
        $this->holiday = $holiday;
        $this->plans = $plans;
        $this->project = $project;
        $this->audit_year = '';
        $this->auditor_leaves = null;
        $this->working_days = null;
    }

    public function setAuditYear($year = null)
    {
        if( is_null($year) ) $year = date('Y'); // set current year if parameter is null

        $this->audit_year = $year;
        return $this;
    }

    public function setAuditor(Auditor $auditor)
    {
        $this->auditor = $auditor;
        return $this;
    }

    public function calculateWorkingdays()
    {
        $start_of_year = Carbon::parse('01-01-'.$this->audit_year);
        $end_of_year = Carbon::parse('31-12-'.$this->audit_year);
        $this->working_days = $start_of_year->diffInWeekdays($end_of_year); // total number of weekdays in selected year

        return $this;
    }

    public function calculateWithHolidays()
    {
        $total_holidays = 0;
        $this->holiday->pushCriteria(new CurrentYearHolidayCriteria($this->audit_year));
        $holidays = $this->holiday->all();
        if( !is_null($holidays) ) {
            foreach($holidays as $holiday){
                // NOTE: Need to confirm if special is included in the rest day holiday
                $_holiday = Carbon::parse($holiday->holiday_date);
                if( $_holiday->isWeekend() ) $total_holidays++; // add the holiday to rest day if not weekend
            }
        }
        $this->working_days -= $total_holidays; // minus the holiday to working days

        return $this;
    }

    public function calculateWithAuditorPlannedLeaves()
    {
        // get auditor's current year leaves
        $total_days = 0;
        $leaves = $this->auditor->leave()->byYear($this->audit_year)->get();
        if( !is_null($leaves) ) {
            foreach($leaves as $leave){
                // format the leave to time to generate range
                $from = Carbon::parse($leave->leave_date_from);
                $to = Carbon::parse($leave->leave_date_to);
                $total_days += $from->diffInWeekdays($to) + 1; // +1 to include the start of the leave
            }
        }
        $this->working_days -= $total_days;  // minus the leave to working days
        return $this;
    }

    public function calculateAuditorRunningMandays()
    {
        $project_mandays = $this->auditor->totalProjectMandays()->join('plan', 'plan.plan_id', '=', 'plan_projects.plan_id')->where('plan_year', $this->audit_year)->first(); // Gets the auditor project mandays
        $scope_mandays = $this->auditor
        ->totalProjectScopeMandays()
        ->join('plan_projects', 'plan_projects.plan_project_id', '=', 'plan_project_scope.plan_project_id')
        ->join('plan', 'plan.plan_id', '=', 'plan_projects.plan_id')
        ->where('plan_year', $this->audit_year)
        ->first(); // Gets the auditor project scope mandays
        $total_project = !is_null($project_mandays) ? $project_mandays->running_mandays : 0;
        $total_scope = !is_null($scope_mandays) ? $scope_mandays->running_mandays : 0;

        $this->running = $total_project + $total_scope;

        return $this;
    }

    public function getglobal_config()
    {
        return global_config::first();
    }

    public function getMandays()
    {
        // calculate annual mandays
        $annual = $this->calculateWorkingdays()
                       ->calculateWithHolidays()
                       ->calculateWithAuditorPlannedLeaves()
                       ->calculateAuditorRunningMandays()
                       ->calculateAnnualMandays();
        $available = $annual - $this->running;

        return [
            'annual' => $annual,
            'running' => $this->running,
            'available' => $available,
        ];
    }

    public function getPlanProjectAuditorRunningMandays($plan_id, $auditor_id, $project_id = null)
    {
        $table = 'plan_project_auditors';
        // push criteria for plan / project auditor
        // to get the total running mandays
        $this->attachAuditorMandaysCriterias($this->plans, $auditor_id, $table)
             ->pushCriteria(new PlanProjectAuditorMandaysCriteria($project_id));
        $plan = $this->plans->findByField('plan.plan_id', $plan_id)->first();

        $this->running = !is_null($plan) ? (int) $plan->running_mandays : 0;
    }

    public function getProjectAuditorRunningMandays($project_id, $auditor_id = null)
    {
        // push criteria for plan / project auditor
        // to get the total running mandays
        $this->attachAuditorMandaysCriterias($this->project, $auditor_id)
            ->pushCriteria(new ProjectAuditorMandaysCriteria($auditor_id));
        $project = $this->project->findByField('plan_projects.plan_project_id', $project_id)->first();

        $this->running = !is_null($project) ? (int) $project->running_mandays : 0;
    }

    /**
     * Calculates the auditor annual mandays
     * by multiplying the working days to mandays factor rate
     * from global config divided by 100
     * @return float
     */
    public function calculateAnnualMandays()
    {
        return ($this->working_days * $this->getglobal_config()->auditor_mandays_factor_rate) / 100;
    }

    public function attachAuditorMandaysCriterias(RepositoryInterface $repo, $auditor_id, $table = '')
    {
        $repo->pushCriteria(new PlanningAuditorCriteria($auditor_id, $table))->pushCriteria(new AuditorMandaysCriteria);
        return $repo;
    }

    public function setMandaysSource($type, $id, $param = array())
    {
        switch ($type) {
            case 'plan' :
                $this->getPlanProjectAuditorRunningMandays($id, $param['auditor_id'], (isset($param['project_id']) ? $param['project_id'] : null) );
                break;
            case 'scope' :
            case 'project' :
                $this->getProjectAuditorRunningMandays($id, $param['auditor_id']);
                break;
            default:
                break;
        }
    }

    public function updatePlanning($status, Model $model, $attributes)
    {

        $attributes = $this->setProcessAttributes($status, $attributes);
        return $model->update($attributes);
    }

    public function setProcessAttributes($status, $attributes)
    {
        $now = date('Y-m-d');
        $user = \Auth::user()->user_id;
        if( $status == 'Reviewed' ) {
            $attributes['reviewed_by'] = $user;
            $attributes['reviewed_date'] = $now;
        }
        if( $status == 'Approved' ) {
            $attributes['approved_by'] = $user;
            $attributes['approved_date'] = $now;
        }

        return $attributes;
    }

    public function getAuditors(EloqCollection $auditors, $id, $year, $param = array())
    {
        if( is_null($auditors) ) return [];

        $fractal = new Manager();
        $data = new Collection($auditors, new PlanningAuditorTransformer('plan', $id, $param, $year));
        return $fractal->createData($data)->toArray();
    }

    public function getPlanProjects(EloqCollection $projects)
    {
        if( is_null($projects) ) return [];

        $fractal = new Manager();
        $data = new Collection($projects, new PlanProjectTransformer);
        return $fractal->createData($data)->toArray();
    }

    public function getPlanProjectScopes(EloqCollection $scopes)
    {
        if( is_null($scopes) ) return [];

        $fractal = new Manager();
        $data = new Collection($scopes, new PlanProjectScopeTransformer);
        return $fractal->createData($data)->toArray();
    }
}