<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class SendMemoEmail extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $project_scope_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($project_scope_id)
    {
        $this->project_scope_id = $project_scope_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()

    {
        $data['project_scope_id'] = $this->project_scope_id;
        $email = 'romnickjohn.m.susa@smretail.com';
        $test =Mail::send('email.send_memo_email', $data, function ($mail) use ($email) {
            $mail->from('darknickz08@gmail.com', 'Your Application');
            $mail->to($email, "Romnick John Susa")->subject('Your Reminder!');
        });
    }
}
