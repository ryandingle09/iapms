<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CopyFromAnnualAuditPlan extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    public $plan_project_id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($plan_project_id)
    {
        $this->plan_project_id = $plan_project_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    /*
    * Copy from populate_proj_engagement() stored procedure 
    * populate project engagement tables from the annual audit plan
    */

    public function handle()
    {
        // Models
        $plan_project_model = app('\App\Models\PlanProject');
        $project_model = app('\App\Models\Project');
        $mandays_service = app('\App\Services\MandaysService');

        $plan_project = $plan_project_model->find($this->plan_project_id); 

        if($plan_project){
            // Copy plan project to project
            $project = new $project_model;
            $project->project_name = $plan_project->plan_project_name;
            $project->audit_type = $plan_project->audit_type;
            $project->project_status = "New";
            $project->target_start_date = $plan_project->target_start_date;
            $project->target_end_date = $plan_project->target_end_date;
            $project->project_remarks = $plan_project->project_remarks;
            $project->project_head = $plan_project->project_head;
            $project->plan_project_id = $plan_project->plan_project_id;
            $project->audit_year = $plan_project->plan->plan_year;
            $project->save();

            // Copy each plan project auditors to project auditors

            $plan_project->auditors->each( function($plan_project_auditor) use ($project){
                $project->auditors()->create( [
                    "auditor_id" => $plan_project_auditor->auditor_id,
                    "auditor_type" => $plan_project_auditor->auditor_type
                ]);
            });

            // Copy each plan project scopes to project scopes

            $plan_project->scopes->each( function($plan_project_scope) use ($project, $mandays_service ){

                $project_scope = $project->scopes()->create( [
                    'scope_sequence' => $plan_project_scope->scope_sequence,
                    'auditable_entity_id' => $plan_project_scope->auditable_entity_id,
                    'ae_mbp_id' => $plan_project_scope->mbp_id,
                    'audit_location' => $plan_project_scope->audit_location,
                    'target_start_date' => $plan_project_scope->project->target_start_date,
                    'target_end_date'  =>$plan_project_scope->project->target_end_date,
                    // 'actual_start_date',
                    // 'actual_end_date',
                    'project_scope_status' => 'New',
                    // 'freeze_status',
                    'plan_project_scope_id' => $plan_project_scope->plan_project_scope_id,
                    'default_misc_mandays' => $plan_project_scope->mainBp->additional_budgeted_mandays_total ,
                    'default_apg_mandays' => $mandays_service->mbpBudgetedMandaysApg( $plan_project_scope->auditable_entity_id , $project->audit_type ) 
                ]);

                // Copy each plan project scopes auditor to project scope auditor

                $plan_project_scope->auditors->each( function($plan_project_scope_auditor) use ($project_scope){
                    $project_scope->auditors()->create([
                        "auditor_id" => $plan_project_scope_auditor->auditor_id,
                        "auditor_type" => $plan_project_scope_auditor->auditor_type
                    ]);
                });

            });

        }
    }
}
