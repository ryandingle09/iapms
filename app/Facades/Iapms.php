<?php

namespace App\Facades;
use Illuminate\Support\Facades\Facade;

class Iapms extends Facade{
    protected static function getFacadeAccessor() { return 'iapms'; }
}